local _, addon = ...

local L = setmetatable({}, {
	__index = function(self, key)
		if key then
			rawset(self, key, tostring(key))
		end
		return tostring(key)
	end,
})
addon.L = L

local locale = GetLocale()

------------------------------------ ruRU ------------------------------------
if locale == "ruRU" then

------------------------------------ deDE ------------------------------------
elseif locale == "deDE" then

------------------------------------ itIT ------------------------------------
elseif locale == "itIT" then

------------------------------------ frFR ------------------------------------
elseif locale == "frFR" then

------------------------------------ koKR ------------------------------------
elseif locale == "koKR" then

------------------------------------ zhCN ------------------------------------
elseif locale == "zhCN" then

------------------------------------ ptBR ------------------------------------
elseif locale == "ptBR" then

------------------------------------ zhTW ------------------------------------
elseif locale == "zhTW" then

------------------------------------ esES ------------------------------------
elseif locale == "esES" then

------------------------------------ esMX ------------------------------------
elseif locale == "esMX" then

------------------------------------ enUS ------------------------------------
else

end

-- Replace remaining true values by their key
for k,v in pairs(L) do
	if v == true then
		L[k] = k
	end
end
