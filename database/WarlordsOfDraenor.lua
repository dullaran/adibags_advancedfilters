local _, addon = ...

addon.ITEM_DATABASE[addon.CONS.WARLORDS_OF_DRAENOR_ID] = {
    [addon.CONS.CONTAINERS_ID] = {
        118235, -- Ogre Diving Cap
        117445, -- Clefthoof Hide Satchel
        113094, -- Gronnskin Bag
        114821, -- Hexweave Bag
        130943, -- Reusable Tote Bag
        116261, -- Burnished Inscription Bag
        116259, -- Burnished Leather Bag
        116260  -- Burnished Mining Bag
    },
    [addon.CONS.CONSUMABLES_ID] = {
        [addon.CONS.C_BANDAGES_ID] = {
            111603, -- Antiseptic Bandage
            115497  -- Ashran Bandage
        },
        [addon.CONS.C_CONSUMABLES_ID] = {
            118512, -- Savory Savage Delight
            118007, -- Mecha-Blast Rocket
            109252, -- Engineer's Gnomish Army Knife
            116278, -- Oil of Immolation
            118564, -- Small Savage Piranha
            118566, -- Enormous Savage Piranha
            108422, -- Blackrock Crucible
            108597, -- Target Dummy Mark I
            109644, -- Walter
            112021, -- Blueprints: Workshop
            112200, -- Sacred Text of the Ravenspeakers
            114832, -- Creeping Carpet
            115370, -- Iron Horde Munitions
            115481, -- Bat Collar
            118579, -- Treasure Card: Ruby
            118588, -- Treasure Card: Emerald
            118590, -- Treasure Card: Diamond
            118591, -- Treasure Card: Gold Ingot
            119002, -- Resource Card: Leftovers
            115511, -- Bizmo's Big Bang Boom Bomb
            115512, -- Gazlowe's Gargantuan Grenade
            109253, -- Ultimate Gnomish Army Knife
            109167, -- Findle's Loot-A-Rang
            127655, -- Sassy Imp
            127666, -- Vial of Red Goo
            114943, -- Ultimate Gnomish Army Knife
            108745, -- Personal Hologram
            109183, -- World Shrinker
            109574, -- GUMM-E
            111820, -- Swapblaster
            122187, -- Tune-o-tron Micro
            122477, -- My Special Pet
            111821  -- Blingtron 5000
        },
        [addon.CONS.C_ELIXIRS_ID] = {
            116271, -- Draenic Water Breathing Elixir
            118711, -- Draenic Water Walking Elixir
            116274, -- Elixir of Detect Demon
            116273, -- Elixir of Dream Vision
            116272, -- Elixir of Detect Undead
            116270, -- Catseye Elixir
            116269, -- Elixir of Detect Lesser Invisibility
            124642, -- Darkmoon Draught of Supremacy
            124645, -- Darkmoon Draught of Precision
            124646, -- Darkmoon Draught of Flexibility
            124647, -- Darkmoon Draught of Alacrity
            124648, -- Darkmoon Draught of Divergence
            124649, -- Darkmoon Draught of Defense
            124650, -- Darkmoon Draught of Deftness
            124651, -- Darkmoon Draught of Deflection
            124652, -- Darkmoon Tincture of Deflection
            124653, -- Darkmoon Tincture of Deftness
            124654, -- Darkmoon Tincture of Defense
            124655, -- Darkmoon Tincture of Divergence
            124656, -- Darkmoon Tincture of Alacrity
            124657, -- Darkmoon Tincture of Precision
            124658, -- Darkmoon Tincture of Flexibility
            124659  -- Darkmoon Tincture of Supremacy
        },
        [addon.CONS.C_FLASKS_ID] = {
            109145, -- Draenic Agility Flask
            109147, -- Draenic Intellect Flask
            109148, -- Draenic Strength Flask
            109152, -- Draenic Stamina Flask
            109153, -- Greater Draenic Agility Flask
            109155, -- Greater Draenic Intellect Flask
            109156, -- Greater Draenic Strength Flask
            109160  -- Greater Draenic Stamina Flask
        },
        [addon.CONS.C_FOOD_DRINKS_ID] = {
            111431, -- Hearty Elekk Steak
            111433, -- Blackrock Ham
            111434, -- Pan-Seared Talbuk
            111436, -- Braised Riverbeast
            111437, -- Rylak Crepes
            111438, -- Clefthoof Sausages
            111439, -- Steamed Scorpion
            111441, -- Grilled Gulper
            111442, -- Sturgeon Stew
            111444, -- Fat Sleeper Cakes
            111445, -- Fiery Calamari
            111446, -- Skulker Chowder
            111447, -- Talador Surf and Turf
            111449, -- Blackrock Barbecue
            111450, -- Frosty Stew
            111452, -- Sleeper Surprise
            111453, -- Calamari Crepes
            111454, -- Gorgrond Chowder
            111455, -- Saberfish Broth
            111456, -- Grilled Saberfish
            111457, -- Feast of Blood
            111458, -- Feast of the Waters
            111544, -- Frostboar Jerky
            113509, -- Conjured Mana Bun
            114238, -- Spiced Barbed Trout
            115351, -- "Rylak Claws"
            115352, -- Telmor-Aruuna Hard Cheese
            115353, -- Tanaan Sweetmelon
            115354, -- Sliced Zangar Buttons
            115355, -- Marbled Clefthoof Steak
            117439, -- "Da Bruisery" Hot & Wroth
            117440, -- Peglegger's Porter
            117442, -- Thunderbelly Mead
            117452, -- Gorgrond Mineral Water
            117454, -- Gorgrond Grapes
            117457, -- Blood Apples
            117469, -- Sugar Dusted Choux Twist
            117470, -- Thirteen Grain Loaf
            117471, -- Cocoa Flatcakes
            117472, -- Grilled Gorgrond Surprise
            117473, -- Pickled Elekk Hooves
            117474, -- Rylak Sausages
            117475, -- Clefthoof Milk
            118050, -- Enchanted Apple
            118051, -- Enchanted Orange
            118268, -- Fuzzy Pear
            118269, -- Greenskin Apple
            118270, -- O'ruk Orange
            118271, -- Ironpeel Plantain
            118272, -- Giant Nagrand Cherry
            118275, -- Perfect Nagrand Cherry
            118276, -- Perfect Greenskin Apple
            118277, -- Perfect Ironpeel Plantain
            118416, -- Fish Roe
            118424, -- Blind Palefish
            118428, -- Legion Chili
            118576, -- Savage Feast
            119022, -- Shadowmoon Sugar Pear Cider
            119157, -- Saberon Cat-Sip
            119324, -- Savage Remedy
            120168, -- Pre-Mixed Pot of Noodles
            120293, -- Lukewarm Yak Roast Broth
            120959, -- Pinchwhistle "Nitro Fuel"
            122343, -- Sleeper Sushi
            122344, -- Salty Squid Roll
            122345, -- Pickled Eel
            122346, -- Jumbo Sea Dog
            122347, -- Whiptail Fillet
            122348, -- Buttered Sturgeon
            128219, -- Fel-Smoked Ham
            128385, -- Elemental-Distilled Water
            128498, -- Fel Eggs and Ham
            130192, -- Potato Axebeak Stew
            116917, -- Sailor Zazzuk's 180-Proof Rum
            104196, -- Delectable Ogre Queasine
            112449, -- Iron Horde Rations
            113108, -- Ogre Moonshine
            113290, -- Spirevine Fruit
            115300, -- Marinated Elekk Steak
            117437, -- Skyreach Sunrise
            117441, -- Elekk's Neck
            117453, -- "Da Bruisery" OPA
            117568, -- Jug of Ironwine
            112095, -- Half-Eaten Banana
            111522, -- Tikari & K.A.Y.T.
            114017, -- Steamwheedle Wagon Bomb
            116405, -- Congealed Cranberry Chutney
            116406, -- Twice-Baked Sweet Potato
            116407, -- Slow-Smoked Turkey
            116408, -- Herb-Infused Stuffing
            116409, -- Gourmet Pumpkin Pie
            118900, -- Hol'bruk's Brutal Brew
            120150, -- Blackrock Coffee
            126934, -- Lemon Herb Filet
            126935, -- Fancy Darkmoon Feast
            126936, -- Sugar-Crusted Fish Feast
            116120, -- Tasty Talador Lunch
            118273, -- Perfect O'ruk Orange
            118274, -- Perfect Fuzzy Pear
            101436  -- Icemother Milk
        },
        [addon.CONS.C_ITEM_ENHANCEMENTS_ID] = {
            109120, -- Oglethorpe's Missile Splitter
            109122, -- Megawatt Filament
            110638, -- Enchant Ring - Gift of Critical Strike
            110639, -- Enchant Ring - Gift of Haste
            110640, -- Enchant Ring - Gift of Mastery
            110641, -- Enchant Ring - Gift of Mastery
            110642, -- Enchant Ring - Gift of Versatility
            110645, -- Enchant Neck - Gift of Critical Strike
            110646, -- Enchant Neck - Gift of Haste
            110647, -- Enchant Neck - Gift of Mastery
            110648, -- Enchant Neck - Gift of Haste
            110649, -- Enchant Neck - Gift of Versatility
            110652, -- Enchant Cloak - Gift of Critical Strike
            110653, -- Enchant Cloak - Gift of Haste
            110654, -- Enchant Cloak - Gift of Mastery
            110655, -- Enchant Cloak - Gift of Critical Strike
            110656, -- Enchant Cloak - Gift of Versatility
            110682, -- Enchant Weapon - Mark of the Thunderlord
            112093, -- Enchant Weapon - Mark of the Shattered Hand
            112115, -- Enchant Weapon - Mark of Shadowmoon
            112160, -- Enchant Weapon - Mark of Blackrock
            112164, -- Enchant Weapon - Mark of Warsong
            112165, -- Enchant Weapon - Mark of the Frostwolf
            115973, -- Enchant Weapon - Glory of the Thunderlord
            115975, -- Enchant Weapon - Glory of the Shadowmoon
            115976, -- Enchant Weapon - Glory of the Blackrock
            115977, -- Enchant Weapon - Glory of the Warsong
            115978, -- Enchant Weapon - Glory of the Frostwolf
            118008, -- Hemet's Heartseeker
            118015, -- Enchant Weapon - Mark of Bleeding Hollow
            116117, -- Rook's Lucky Fishin' Line
            128159, -- Elemental Distillate
            128158, -- Wildswater
            118391, -- Worm Supreme
            124674  -- Day-Old Darkmoon Doughnut
        },
        [addon.CONS.C_POTIONS_ID] = {
            109217, -- Draenic Agility Potion
            109218, -- Draenic Intellect Potion
            109219, -- Draenic Strength Potion
            109220, -- Draenic Versatility Potion
            109221, -- Draenic Channeled Mana Potion
            109222, -- Draenic Mana Potion
            109223, -- Healing Tonic
            109226, -- Draenic Rejuvenation Potion
            115498, -- Ashran Healing Tonic
            115531, -- Swirling Ashran Potion
            117415, -- Smuggled Tonic
            118006, -- Shieldtronic Shield
            118262, -- Brilliant Dreampetal
            118910, -- Brawler's Draenic Agility Potion
            118911, -- Brawler's Draenic Intellect Potion
            118912, -- Brawler's Draenic Strength Potion
            118916, -- Brawler's Healing Tonic
            122453, -- Commander's Draenic Agility Potion
            122454, -- Commander's Draenic Intellect Potion
            122455, -- Commander's Draenic Strength Potion
            122456, -- Commander's Draenic Versatility Potion
            113585, -- Iron Horde Rejuvenation Potion
            116266, -- Draenic Swiftness Potion
            116268, -- Draenic Invisibility Potion
            116276, -- Draenic Living Action Potion
            118704, -- Pure Rage Potion
            122451, -- Commander's Draenic Invisibility Potion
            122452, -- Commander's Draenic Swiftness Potion
            116277, -- Potion of Petrification
            116275, -- Mighty Rage Potion
            116267, -- Free Action Potion
            107640, -- Potion of Slow Fall
            124660, -- Darkmoon Healing Tonic
            124661, -- Gladiator's Healing Potion
            124671, -- Darkmoon Firewater
            128647, -- Fizzy Apple Cider
            118278, -- Pale Vision Potion
            118913, -- Brawler's Bottomless Draenic Agility Potion
            118914, -- Brawler's Bottomless Draenic Intellect Potion
            118915, -- Brawler's Bottomless Draenic Strength Potion
            118917, -- Brawler's Bottomless Healing Tonic
            118922  -- Oralius' Whispering Crystal
        },
        [addon.CONS.C_OTHER_ID] = {
            109184, -- Stealthman 54
            114225, -- Forgotten Apexis Trinket
            115466, -- Elemental Fragment
            115522, -- Swift Riding Crop
            115532, -- Flimsy X-Ray Goggles
            115795, -- S.O.S. Relief Flare
            116414, -- Pet Supplies
            116979, -- Blackwater Anti-Venom
            116981, -- Fire Ammonite Oil
            120321, -- Mystery Bag
            122460, -- Drained Blood Crystal
            124045, -- Smelly Musk Gland
            124093, -- Minor Blackfang Challenge Totem
            124094, -- Major Blackfang Challenge Totem
            124095, -- Prime Blackfang Challenge Totem
            116119, -- Ango'rosh Sorcerer Stone
            108683, -- Miniature Dark Portal
            110274, -- Jawless Skulker Bait
            110289, -- Fat Sleeper Bait
            110290, -- Blind Lake Sturgeon Bait
            110291, -- Fire Ammonite Bait
            110292, -- Sea Scorpion Bait
            110293, -- Abyssal Gulper Eel Bait
            110294, -- Blackwater Whiptail Bait
            122742, -- Bladebone Hook
            128229, -- Felmouth Frenzy Bait
            114677, -- Garrison Resources
            118042, -- Conqueror's Tribute
            118111, -- Garrison Resources
            118699, -- Oil of Immolation
            107224, -- Celebration Package
            98551, -- Darkspear Battle Standard
            104039, -- Blackrock Blasting Powder
            106131, -- Satchel Charge
            106950, -- Power Transformer: Dash
            106952, -- Power Transformer: Jump
            106981, -- Power Slinger
            107077, -- Bag of Transformers
            107086, -- Urgent Bronze Missive
            107272, -- Frostwolf First-Fang
            107273, -- Snow Hare's Foot
            107664, -- Blizzcon Boss Basher's Bombastic Blasters
            107799, -- Racing Regs
            107930, -- Dreadful Heart
            109076, -- Goblin Glider Kit
            109599, -- Neural Silencer
            110238, -- Oath of Shadow Hunter Rala
            110431, -- Leather Beach Ball
            110433, -- Dragonfly Ambusher
            110506, -- Parasitic Starfish
            110508, -- "Fragrant" Pheromone Fish
            110724, -- Mulverick's Offer of Service
            110905, -- Defection of Gronnstalker Rokash
            110907, -- Iron Explorer Notes
            111407, -- Waterlogged Journal
            112090, -- Transmorphic Tincture
            112107, -- Mysterious Egg
            112108, -- Cracked Egg
            112321, -- Enchanted Dust
            112499, -- Stinky Gloom Bombs
            112791, -- Sargerei Cowl
            112891, -- Sargerei Robe
            112893, -- Sargerei Slippers
            112904, -- Sargerei Disguise
            113273, -- Orb of the Soulstealer
            113274, -- Plume of Celerity
            113275, -- Ravenlord's Talon
            113276, -- Pridehunter's Fang
            113277, -- Ogreblood Potion
            113278, -- Scavenger's Eyepiece
            113405, -- Gently Squeezed Toad
            114015, -- Lavastone Pale
            114016, -- Lavastone Jack
            114244, -- GG-117 Micro-Jetpack
            114246, -- "Skyterror" Personal Delivery System
            114633, -- XD-57 "Bullseye" Guided Rocket Kit
            114744, -- Sentry Turret Dispenser
            114835, -- Rooby Reat
            114850, -- Bubblefizz Bubbly
            114924, -- Prototype Mekgineer's Chopper
            114925, -- Prototype Mechano-Hog
            114926, -- Restorative Goldcap
            114942, -- Draenic Mortar
            114974, -- Pneumatic Power Gauntlet
            114975, -- Pneumatic Power Gauntlet
            114983, -- Sticky Grenade Launcher
            115020, -- Goblin Rocket Pack
            116147, -- Alliance Firework
            116148, -- Horde Firework
            116149, -- Snake Firework
            116357, -- Poorly-Painted Egg
            116358, -- Intricately-Painted Egg
            116359, -- Magnificently-Painted Egg
            116848, -- Hallowed Wand - Slime
            116850, -- Hallowed Wand - Ghoul
            116851, -- Hallowed Wand - Abomination
            116853, -- Hallowed Wand - Geist
            116854, -- Hallowed Wand - Spider
            116978, -- Rylakinator-3000 Power Cell
            118091, -- Follower XP Muffin
            118414, -- Awesomefish
            118415, -- Grieferfish
            118511, -- Tyfish
            118705, -- Warm Goren Egg
            118706, -- Cracked Goren Egg
            119044, -- Foggy Noggin Brew
            119126, -- Partial Receipt (Random)
            119158, -- Robo-Rooster
            119159, -- Happy Fun Skull
            119209, -- Angry Brewfest Letter
            119210, -- Hearthstone Board
            119212, -- Winning Hand
            119213, -- Unnecessary Spike
            119348, -- Admiral Taylor's Garrison Log
            119433, -- Path of the Void
            119436, -- Overcharged Siege Engine
            119437, -- Overcharged Demolisher
            119809, -- Caged Boar
            119811, -- Caged Talbuk
            119812, -- Caged Elekk
            119816, -- Caged Mighty Talbuk
            119818, -- Caged Mighty Elekk
            119820, -- Caged Mighty Boar
            120142, -- Coliseum Champion's Spoils
            120170, -- Partially-Digested Bag
            120182, -- Excess Potion of Accelerated Learning
            120204, -- Forged Weapons and Armor
            120205, -- XP
            120347, -- Enchanted Crystal of Replenishment
            120348, -- Enchanted Crystal of Freezing
            120349, -- Enduring Vial of Swiftness
            122589, -- Ogre Waystone Conversions
            127408, -- Adventuring Journal
            127987, -- Celebration Package
            128505, -- Celebration Wand - Murloc
            128506, -- Celebration Wand - Gnoll
            128634, -- Mysterious Brew
            128644, -- Hallowed Wand - Wight
            128645, -- Hallowed Wand - Gargoyle
            128646, -- Hallowed Wand - Nerubian
            128768, -- Candy Cane
            128793, -- Sack of Spiders
            133688, -- Tugboat Bobber
            110426, -- Goblin Hot Potato
            118664, -- Frostwolf Elixir
            118665, -- Exarch Elixir
            118667, -- Steamwheedle Elixir
            118905, -- Sinister Spores
            119216, -- Super Sticky Glitter Bomb
            120257, -- Drums of Fury
            111357, -- Mortarer's Whistle
            110505, -- Mesmerizing Fruit Hat
            112384, -- Reflecting Prism
            112498, -- Prismatic Focusing Lens
            113143, -- Glowing Honeycomb
            115463, -- Elixir of Shadow Sight
            115464, -- Lingering Frost Essence
            115470, -- Lingering Time Bubble
            118046, -- Rubber Duck
            118054, -- Discarded Bone
            118236, -- Counterfeit Coin
            119435, -- Path of Flame
            119449, -- Shadowberry
            120322, -- Klinking Stacked Card Deck
            120324, -- Bursting Stacked Card Deck
            124506, -- Vial of Fel Cleansing
            113212, -- Treasure Map: Tanaan Jungle
            115513, -- Wrynn's Vanguard Battle Standard
            115514, -- Vol'jin's Spear Battle Standard
            115519, -- Flask of the Honorbound
            115525, -- Scary Ogre Face
            117492, -- Relic of Rukhmar
            118666, -- Arakkoa Elixir
            118668, -- Laughing Skull Elixir
            118669, -- Sha'tari Elixir
            118727, -- Frostfire Treasure Map
            118728, -- Shadowmoon Valley Treasure Map
            118729, -- Gorgrond Treasure Map
            118730, -- Talador Treasure Map
            118731, -- Spires of Arak Treasure Map
            118732, -- Nagrand Treasure Map
            118736, -- Captain's Whistle
            119134, -- Sargerei Disguise
            119144, -- Touch of the Naaru
            119145, -- Firefury Totem
            119151, -- Soulgrinder
            119156, -- Portable Shattrath Defense Crystal
            119432, -- Botani Camouflage
            120148, -- Insignia of Vol'jin's Spear
            120149, -- Insignia of Wrynn's Vanguard
            127394, -- Podling Camouflage
            127395, -- Ripened Strange Fruit
            127396, -- Strange Green Fruit
            127668, -- Jewel of Hellfire
            127669, -- Skull of the Mad Chief
            127670, -- Accursed Tome of the Sargerei
            127671, -- [addon.CONS.PH] Cipher Shard
            127709, -- Throbbing Blood Orb
            127766, -- The Perfect Blossom
            127768, -- Fel Petal
            127770, -- Brazier of Awakening
            127859, -- Dazzling Rod
            128320, -- Corrupted Primal Obelisk
            128451, -- Vol'jin's Headhunters Battle Standard
            128452, -- Hand of the Prophet Battle Standard
            128453, -- Saberstalkers Battle Standard
            128454, -- Order of the Awakened Battle Standard
            128474, -- Treasure Map: Tanaan Jungle
            113670, -- Mournful Moan of Murmur
            118222, -- Spirit of Bashiok
            112087, -- Obsidian Frostwolf Petroglyph
            113545, -- Carved Drinking Horn
            113570, -- Ancient's Bloom
            113631, -- Hypnosis Goggles
            116125, -- Klikixx's Webspinner
            118221, -- Petrification Stone
            118716, -- Goren Garb
            128649, -- Illusion: Winter's Grasp
            110425, -- Gnomish Celebration Enforcer
            110428, -- Goblin Body-Building Competition
            111408, -- Discarded Lucky Coin
            117398, -- Everbloom Seed Pouch
            118115, -- Fearsome Battle Standard
            118123, -- Fearsome Battle Standard
            118630, -- Hyper Augment Rune
            118631, -- Stout Augment Rune
            118632, -- Focus Augment Rune
            118670, -- Inspiring Battle Standard
            119035, -- Inspiring Battle Standard
            119160, -- Tickle Totem
            120323, -- Bulging Stacked Card Deck
            120325, -- Overflowing Stacked Card Deck
            122275, -- Sun-touched Feather of Rukhmar
            127887, -- Food Storage Bay
            127888, -- Automated Sky Scanner
            127889, -- Ammo Reserves
            127890, -- Sonic Amplification Field
            127891, -- Extra Quarters
            127892, -- Q-43 Noisemaker Mines
            128294, -- Trade Agreement: Arakkoa Outcasts
            128312, -- Elixir of the Rapid Mind
            128315, -- Medallion of the Legion
            128446, -- Saberstalker Teachings: Trailblazer
            128475, -- Empowered Augment Rune
            128482, -- Empowered Augment Rune
            128489, -- Equipment Blueprint: Unsinkable
            128491, -- Equipment Blueprint: Tuskarr Fishing Net
            128776, -- Red Wooden Sled
            128307, -- Draenic Weaponry
            128308, -- Draenic Armor Set
            118572, -- Illusion: Flames of Ragnaros
            118904, -- Unleashed Mania
            120286, -- Enchanter's Illusion - Glorious Tyranny
            120287, -- Enchanter's Illusion - Primal Victory
            122466, -- Heart of Oak
            122468, -- Runed Greatstone
            122472, -- Bloodied Iron Horde Banner
            122473, -- Legion Beacon
            122474, -- Arcane Highmaul Relic
            122475, -- Void Prison
            112324, -- Nightmarish Hitching Post
            113193, -- Mythical Battle-Pet Stone
            122340, -- Timeworn Heirloom Armor Casing
            122341, -- Timeworn Heirloom Scabbard
            122338, -- Ancient Heirloom Armor Casing
            122339, -- Ancient Heirloom Scabbard
            117401  -- Nat's Draenic Fishing Journal
        }
    },
    [addon.CONS.CURRENCY_ID] = {
        124099  -- Blackfang Claw
    },
    [addon.CONS.GEMS_ID] = {
        115803, -- Critical Strike Taladite
        115804, -- Haste Taladite
        115805, -- Mastery Taladite
        115806, -- Haste Taladene
        115807, -- Versatility Taladite
        115808, -- Stamina Taladite
        115809, -- Greater Critical Strike Taladite
        115811, -- Greater Haste Taladite
        115812, -- Greater Mastery Taladite
        115813, -- Greater Haste Taladene
        115814, -- Greater Versatility Taladite
        115815, -- Greater Stamina Taladite
        127414, -- Eye of Rukhmar
        127415, -- Eye of Anzu
        127416, -- Eye of Sethe
        127760, -- Immaculate Critical Strike Taladite
        127761, -- Immaculate Haste Taladite
        127762, -- Immaculate Mastery Taladite
        127763, -- Immaculate Haste Taladene
        127764, -- Immaculate Versatility Taladite
        127765  -- Immaculate Stamina Taladite
    },
    [addon.CONS.GLYPHS_ID] = {
        118061  -- Glyph of the Sun
    },
    [addon.CONS.MISCELLANEOUS_ID] = {
        [addon.CONS.M_ARMOR_TOKENS_ID] = {
            119305, -- Chest of the Iron Conqueror
            119306, -- Gauntlets of the Iron Conqueror
            119307, -- Leggings of the Iron Conqueror
            119308, -- Helm of the Iron Conqueror
            119309, -- Shoulders of the Iron Conqueror
            119311, -- Gauntlets of the Iron Vanquisher
            119312, -- Helm of the Iron Vanquisher
            119313, -- Leggings of the Iron Vanquisher
            119314, -- Shoulders of the Iron Vanquisher
            119315, -- Chest of the Iron Vanquisher
            119318, -- Chest of the Iron Protector
            119319, -- Gauntlets of the Iron Protector
            119320, -- Leggings of the Iron Protector
            119321, -- Helm of the Iron Protector
            120212, -- Chest of the Iron Conqueror
            120213, -- Gauntlets of the Iron Conqueror
            120214, -- Leggings of the Iron Conqueror
            120215, -- Helm of the Iron Conqueror
            120216, -- Shoulders of the Iron Conqueror
            120217, -- Gauntlets of the Iron Vanquisher
            120218, -- Helm of the Iron Vanquisher
            120219, -- Leggings of the Iron Vanquisher
            120220, -- Shoulders of the Iron Vanquisher
            120221, -- Chest of the Iron Vanquisher
            120222, -- Chest of the Iron Protector
            120223, -- Gauntlets of the Iron Protector
            120224, -- Leggings of the Iron Protector
            120225, -- Helm of the Iron Protector
            120226, -- Shoulders of the Iron Protector
            120227, -- Chest of the Iron Conqueror
            120228, -- Gauntlets of the Iron Conqueror
            120229, -- Leggings of the Iron Conqueror
            120230, -- Helm of the Iron Conqueror
            120231, -- Shoulders of the Iron Conqueror
            120232, -- Gauntlets of the Iron Vanquisher
            120233, -- Helm of the Iron Vanquisher
            120234, -- Leggings of the Iron Vanquisher
            120235, -- Shoulders of the Iron Vanquisher
            120236, -- Chest of the Iron Vanquisher
            120237, -- Chest of the Iron Protector
            120238, -- Gauntlets of the Iron Protector
            120239, -- Leggings of the Iron Protector
            120240, -- Helm of the Iron Protector
            120241, -- Shoulders of the Iron Protector
            120242, -- Chest of the Iron Conqueror
            120243, -- Gauntlets of the Iron Conqueror
            120244, -- Leggings of the Iron Conqueror
            120245, -- Helm of the Iron Conqueror
            120246, -- Shoulders of the Iron Conqueror
            120247, -- Gauntlets of the Iron Vanquisher
            120248, -- Helm of the Iron Vanquisher
            120249, -- Leggings of the Iron Vanquisher
            120250, -- Shoulders of the Iron Vanquisher
            120251, -- Chest of the Iron Vanquisher
            120252, -- Chest of the Iron Protector
            120253, -- Gauntlets of the Iron Protector
            120254, -- Leggings of the Iron Protector
            120255, -- Helm of the Iron Protector
            120256, -- Shoulders of the Iron Protector
            127953, -- Chest of Hellfire's Conqueror
            127954, -- Gauntlets of Hellfire's Conqueror
            127955, -- Leggings of Hellfire's Conqueror
            127956, -- Helm of Hellfire's Conqueror
            127957, -- Shoulders of Hellfire's Conqueror
            127958, -- Gauntlets of Hellfire's Vanquisher
            127959, -- Helm of Hellfire's Vanquisher
            127960, -- Leggings of Hellfire's Vanquisher
            127961, -- Shoulders of Hellfire's Vanquisher
            127962, -- Chest of Hellfire's Vanquisher
            127963, -- Chest of Hellfire's Protector
            127964, -- Gauntlets of Hellfire's Protector
            127965, -- Leggings of Hellfire's Protector
            127966, -- Helm of Hellfire's Protector
            127967, -- Shoulders of Hellfire's Protector
            127968, -- Badge of Hellfire's Vanquisher
            127969, -- Badge of Hellfire's Conqueror
            127970  -- Badge of Hellfire's Protector
        },
        [addon.CONS.M_HOLIDAY_ID] = {
            128632, -- Savage Snowball
            128648, -- Yellow Snowball
            116163, -- Massage Table
            116442, -- Vengeful Spiritshard
            116443, -- Peaceful Spiritshard
            116444, -- Forlorn Spiritshard
            116445, -- Anxious Spiritshard
            116648, -- Manufactured Love Prism
            116761, -- Winter Veil Gift
            116810, -- "Mad Alchemist" Costume
            116811, -- "Lil' Starlet" Costume
            116812, -- "Yipp-Saron" Costume
            129929, -- Ever-Shifting Mirror
            116762, -- Stolen Present
            108631, -- Crashin' Thrashin' Roller Controller
            108632, -- Crashin' Thrashin' Flamer Controller
            108633, -- Crashin' Thrashin' Cannon Controller
            108634, -- Crashin' Thrashin' Mortar Controller
            108635, -- Crashin' Thrashin' Killdozer Controller
            116763, -- Crashin' Thrashin' Shredder Controller
            116651, -- True Love Prism
            116689, -- Pineapple Lounge Cushion
            116690, -- Safari Lounge Cushion
            116691, -- Zhevra Lounge Cushion
            116692, -- Fuzzy Green Lounge Cushion
            116757, -- Steamworks Sausage Grill
            116856, -- "Blooming Rose" Contender's Costume
            116888, -- "Night Demon" Contender's Costume
            116889, -- "Purple Phantom" Contender's Costume
            116890, -- "Santo's Sun" Contender's Costume
            116891, -- "Snowy Owl" Contender's Costume
            117392, -- Loot-Filled Pumpkin
            117393, -- Keg-Shaped Treasure Chest
            117394, -- Satchel of Chilled Goods
            116828, -- Exquisite Costume Set: "The Lich King"
            128510, -- Exquisite Costume Set: "Edwin VanCleef"
            128643  -- Exquisite Costume Set: "Deathwing"
        },
        [addon.CONS.M_REAGENTS_ID] = {
            114137, -- Power Core
            117544, -- Goblin Supplies
            118373, -- Apexis Keystone
            118701, -- Talon Key
            123964, -- Felforged Cudgel
            118469, -- Black Claw of Sethe
            118470, -- Garn-Tooth Necklace
            122139  -- First Aid Bandages
        },
        [addon.CONS.M_MOUNTS_ID] = {
            113543, -- Spirit of Shinri
            119180, -- Goren "Log" Roller
            116655, -- Witherhide Cliffstomper
            116656, -- Trained Icehoof
            116657, -- Ancient Leatherhide
            116658, -- Tundra Icehoof
            116662, -- Trained Meadowstomper
            116663, -- Shadowhide Pearltusk
            116664, -- Dusty Rockhide
            116665, -- Armored Irontusk
            116666, -- Blacksteel Battleboar
            116667, -- Rocktusk Battleboar
            116668, -- Armored Frostboar
            116669, -- Armored Razorback
            116670, -- Frostplains Battleboar
            116671, -- Wild Goretusk
            116672, -- Domesticated Razorback
            116673, -- Giant Coldsnout
            116675, -- Trained Rocktusk
            116676, -- Trained Riverwallow
            116768, -- Mosshide Riverwallow
            116769, -- Mudback Riverbeast
            116772, -- Shadowmane Charger
            116774, -- Trained Silverpelt
            116775, -- Breezestrider Stallion
            116776, -- Pale Thorngrazer
            116779, -- Garn Steelmaw
            116780, -- Warsong Direfang
            116781, -- Armored Frostwolf
            116782, -- Ironside Warwolf
            116784, -- Trained Snarler
            116786, -- Smoky Direwolf
            128480, -- Bristling Hellboar
            128481, -- Bristling Hellboar
            128526, -- Deathtusk Felboar
            128527, -- Deathtusk Felboar
            129139, -- Tome of Rapid Pathfinding
            128282, -- Warmongering Gladiator's Felblood Gronnling
            108883, -- Riding Harness
            115363, -- Creeping Carpet
            123890, -- Felsteel Annihilator
            123974, -- Reins of the Corrupted Dreadwing
            128277, -- Primal Gladiator's Felblood Gronnling
            128422, -- Reins of the Grove Warden
            129922, -- Bridle of the Ironbound Wraithcharger
            118676, -- Reins of the Emerald Drake
            129923, -- Reins of the Eclipse Dragonhawk
            133543, -- Reins of the Infinite Timereaver
            116771, -- Solar Spirehawk
            115484, -- Core Hound Chain
            116383, -- Gorestrider Gronnling
            116659, -- Bloodhoof Bull
            116660, -- Ironhoof Destroyer
            116661, -- Mottled Meadowstomper
            116674, -- Great Greytusk
            116767, -- Sapphire Riverbeast
            116773, -- Swift Breezestrider
            116777, -- Vicious War Ram
            116778, -- Vicious War Raptor
            116785, -- Swift Frostwolf
            116788, -- Warlord's Deathwheel
            116789, -- Champion's Treadblade
            116792, -- Sunhide Gronnling
            116794, -- Garn Nighthowl
            118515, -- Cindermane Charger
            121815, -- Voidtalon of the Dark Star
            124089, -- Vicious War Mechanostrider
            124540, -- Vicious War Kodo
            127140, -- Infernal Direwolf
            128311, -- Coalfist Gronnling
            128671, -- Minion of Grumpus
            112326, -- Warforged Nightmare
            112327, -- Grinning Reaver
            122469, -- Mystic Runesaber
            128425, -- Reins of the Illidari Felstalker
            128706, -- Soaring Skyterror
            120968, -- Chauffeured Chopper
            122703, -- Chauffeured Chopper
            110672  -- Grimoire of the Four Winds
        },
        [addon.CONS.M_COMPANIONS_ID] = {
            116415, -- Pet Charm
            113216, -- Elekk Plushie
            116155, -- Lovebird Hatchling
            116439, -- Blazing Cindercrawler
            120050, -- Veilwatcher Hatchling
            120051, -- Kaliri Hatchling
            114834, -- Meadowstomper Calf
            116402, -- Stonegrinder
            117354, -- Ancient Nest Guardian
            117564, -- Fruit Hunter
            118104, -- Umbrafen Spore
            118106, -- Crimson Spore
            118107, -- Brilliant Spore
            119431, -- Servant of Demidos
            128770, -- Grumpling
            110684, -- Leftovers
            110721, -- Crazy Carrot
            111402, -- Mechanical Axebeak
            111660, -- Iron Starlette
            111866, -- Royal Peacock
            112057, -- Lifelike Mechanical Frostboar
            112699, -- Teroclaw Hatchling
            113554, -- Zomstrok
            113558, -- Weebomination
            113569, -- Autumnal Sproutling
            113623, -- Spectral Bell
            114919, -- Sea Calf
            114968, -- Deathwatch Hatchling
            115483, -- Sky-Bo
            116064, -- Syd the Squid
            116258, -- Mystical Spring Bouquet
            116403, -- Frightened Bush Chicken
            116756, -- Stout Alemental
            116801, -- Cursed Birman
            116804, -- Widget the Departed
            116815, -- Netherspawn, Spawn of Netherspawn
            117380, -- Frostwolf Ghostpup
            117404, -- Land Shark
            117528, -- Lanticore Spawnling
            118101, -- Zangar Spore
            118105, -- Seaborne Spore
            118207, -- Hydraling
            118516, -- Argi
            118517, -- Grommloc
            118577, -- Stormwing
            118578, -- Firewing
            118595, -- Nightshade Sproutling
            118598, -- Sun Sproutling
            118675, -- Time-Locked Box
            118709, -- Doom Bloom
            118741, -- Mechanical Scorpid
            118919, -- Red Goren Egg
            118921, -- Everbloom Peachick
            118923, -- Sentinel's Companion
            119141, -- Frostwolf Pup
            119142, -- Draenei Micro Defender
            119143, -- Son of Sethe
            119146, -- Bone Wasp
            119148, -- Indentured Albino River Calf
            119149, -- Captured Forest Sproutling
            119150, -- Sky Fry
            119170, -- Eye of Observation
            119328, -- Soul of the Forge
            119434, -- Albino Chimaeraling
            119467, -- Puddle Terror
            119468, -- Sunfire Kaliri
            120121, -- Trunks
            120309, -- Glass of Warm Milk
            122104, -- Leviathan Egg
            122105, -- Grotesque Statue
            122106, -- Shard of Supremus
            122107, -- Fragment of Anger
            122108, -- Fragment of Suffering
            122109, -- Fragment of Desire
            122110, -- Sultry Grimoire
            122111, -- Smelly Gravestone
            122112, -- Hyjal Wisp
            122113, -- Sunblade Rune of Activation
            122114, -- Void Collar
            122115, -- Servant's Bell
            122116, -- Holy Chime
            122125, -- Race MiniZep Controller
            122532, -- Bone Serpent
            122533, -- Young Talbuk
            122534, -- Slithershock Elver
            123862, -- Hogs' Studded Collar
            126925, -- Blorp's Bubble
            126926, -- Translucent Shell
            127701, -- Glowing Sporebat
            127703, -- Dusty Sporewing
            127704, -- Bloodthorn Hatchling
            127705, -- Lost Netherpup
            127748, -- Cinder Pup
            127749, -- Corrupted Nest Guardian
            127753, -- Nightmare Bell
            127754, -- Periwinkle Calf
            127856, -- Left Shark
            127868, -- Crusher
            128309, -- Shard of Cyrukh
            128423, -- Zeradar
            128424, -- Brightpaw
            128426, -- Nibbles
            128427, -- Murkidan
            128477, -- Savage Cub
            128478, -- Blazing Firehawk
            129205, -- A Tiny Infernal Collar
            129216, -- Vibrating Arcane Crystal
            129217, -- Warm Arcane Crystal
            129218, -- Glittering Arcane Crystal
            134047, -- Baby Winston
            [addon.CONS.MC_SUPPLIES_ID] = {
                122535, -- Traveler's Pet Supplies
                116062, -- Greater Darkmoon Pet Supplies
                118697, -- Big Bag of Pet Supplies
                127751  -- Fel-Touched Pet Supplies
            },
            [addon.CONS.MC_TRAINING_STONE_ID] = {
                122457, -- Ultimate Battle-Training Stone
                127755, -- Fel-Touched Battle-Training Stone
                116374, -- Beast Battle-Training Stone
                116416, -- Humanoid Battle-Training Stone
                116417, -- Mechanical Battle-Training Stone
                116418, -- Critter Battle-Training Stone
                116419, -- Dragonkin Battle-Training Stone
                116420, -- Elemental Battle-Training Stone
                116421, -- Flying Battle-Training Stone
                116422, -- Magic Battle-Training Stone
                116423, -- Undead Battle-Training Stone
                116424, -- Aquatic Battle-Training Stone
                116429  -- Flawless Battle-Training Stone
            }
        },
        [addon.CONS.M_TELEPORT_ID] = {
            [addon.CONS.MT_HEARTSTONE_ID] = {
                128353, -- Admiral's Compass
                110560, -- Garrison Hearthstone
                118663, -- Relic of Karabor
                118662, -- Bladespire Relic
                128502, -- Hunter's Seeking Crystal
                128503  -- Master Hunter's Seeking Crystal
            },
            [addon.CONS.MT_JEWELRY_ID] = {
                118907, -- Pit Fighter's Punching Ring (Bizmo's Brawlpub)
                118908  -- Pit Fighter's Punching Ring (Brawl'gar Arena)
            },
            [addon.CONS.MT_SCROLLS_ID] = {
                119183  -- Scroll of Risky Recall
            },
            [addon.CONS.MT_TOYS_ID] = {
                112059  -- Wormhole Centrifuge
            }
        },
        [addon.CONS.M_ARCHAEOLOGY_ID] = {
            [addon.CONS.MA_CRATES_ID]= {
                117386, -- Crate of Pandaren Archaeology Fragments
                117387, -- Crate of Mogu Archaeology Fragments
                117388  -- Crate of Mantid Archaeology Fragments
            },
            [addon.CONS.MA_KEY_STONES_ID] = {
                109584, -- Ogre Missive
                109585, -- Arakkoa Cipher
                108439  -- Draenor Clan Orator Cane
            },
            [addon.CONS.MA_OTHERS_ID] = {
                122606, -- Explorer's Notebook
                117389, -- Draenor Archaeologist's Lodestone
                117390  -- Draenor Archaeologist's Map
            }
        },
        [addon.CONS.M_OTHER_ID] = {
            113203, -- Punctured Breastplate
            113244, -- Soleless Treads
            113245, -- Shredded Greaves
            113295, -- Cracked Potion Vial
            113307, -- Impotent Healing Potion
            113310, -- Unstable Elixir
            113313, -- Unorganized Alchemist Notes
            113316, -- Mangled Long Sword
            113321, -- Battered Shield
            113324, -- Ritual Mask Shards
            113327, -- Weathered Bedroll
            113328, -- Torn Voodoo Doll
            113329, -- Ripped Lace Kerchief
            113332, -- Cracked Wand
            113336, -- Gnarled, Splintering Staff
            113358, -- Felled Totem
            113361, -- Tattered Scroll
            113365, -- Ruined Painting
            113367, -- Waterlogged Book
            113371, -- Torn Card
            113376, -- Faintly Magical Vellum
            113381, -- Crumbling Statue
            113384, -- Crushed Locket
            113387, -- Cracked Band
            113391, -- Crystal Shards
            113394, -- Headless Figurine
            113411, -- Bloodstained Mage Robe
            113417, -- Torn Knapsack
            113420, -- Desiccated Leather Cloak
            113423, -- Scorched Leather Cap
            113426, -- Mangled Saddle Bag
            113429, -- Cracked Hand Drum
            113452, -- Trampled Survey Bot
            113465, -- Broken Hunting Scope
            113468, -- Faulty Grenade
            113471, -- Busted Alarm Bot
            113478, -- Abandoned Medic Kit
            113483, -- Lightweight Medic Vest
            113495, -- Venom Extraction Kit
            113499, -- Notes of Natural Cures
            114028, -- Small Pouch of Coins
            114970, -- Small Pouch of Coins
            116111, -- Small Pouch of Coins
            116376, -- Small Pouch of Coins
            116764, -- Small Pouch of Coins
            119191, -- Jewelcrafting Payment
            119195, -- Jewelcrafting Payment
            119196, -- Jewelcrafting Payment
            119197, -- Jewelcrafting Payment
            119198, -- Jewelcrafting Payment
            119199, -- Jewelcrafting Payment
            119200, -- Jewelcrafting Payment
            119201, -- Jewelcrafting Payment
            128327, -- Small Pouch of Coins
            116129, -- Dessicated Orc's Coin Pouch
            106895, -- Iron-Bound Junkbox
            108740, -- Stolen Weapons
            118067, -- Bartering Chip
            112623, -- Pack of Fishing Supplies
            105914, -- Danger Detector Boots
            105915, -- Danger Detector Harness
            106201, -- Bladespire Arena Key
            107270, -- Bound Traveler's Scroll
            107271, -- Frozen Envelope
            107275, -- Dog-Eared Note
            107277, -- Fur-Lined Scroll
            108882, -- Bloodmaul Blasting Charge
            110278, -- Engorged Stomach
            110667, -- Tear-stained Letter
            111863, -- Goldmane's Cage Key
            112376, -- Target Practice Axe
            112995, -- Slimy Ring
            112996, -- Glistening Ring
            112997, -- Emerald Ring
            112998, -- Diamond Ring
            112999, -- Sapphire Ring
            114054, -- Goldtoe's Key
            114141, -- Fang-Scarred Frostwolf Axe
            114143, -- Frostwolf Ancestry Scrimshaw
            114145, -- Wolfskin Snowshoes
            114147, -- Warsinger's Drums
            114149, -- Screaming Bullroarer
            114151, -- Warsong Ceremonial Pike
            114153, -- Metalworker's Hammer
            114155, -- Elemental Bellows
            114157, -- Blackrock Razor
            114159, -- Weighted Chopping Axe
            114161, -- Hooked Dagger
            114163, -- Barbed Fishing Hook
            114165, -- Calcified Eye In a Jar
            114167, -- Ceremonial Tattoo Needles
            114169, -- Cracked Ivory Idol
            114171, -- Ancestral Talisman
            114173, -- Flask of Blazegrease
            114175, -- Gronn-Tooth Necklace
            114177, -- Doomsday Prophecy
            114179, -- Headdress of the First Shaman
            114181, -- Stonemaul Succession Stone
            114183, -- Stone Manacles
            114185, -- Ogre Figurine
            114187, -- Pictogram Carving
            114189, -- Gladiator's Shield
            114190, -- Mortar and Pestle
            114191, -- Eye of Har'gunn the Blind
            114192, -- Stone Dentures
            114193, -- Rylak Riding Harness
            114194, -- Imperial Decree Stele
            114195, -- Sorcerer-King Toe Ring
            114196, -- Warmaul of the Warmaul Chieftain
            114197, -- Dreamcatcher
            114198, -- Burial Urn
            114199, -- Decree Scrolls
            114200, -- Solar Orb
            114201, -- Sundial
            114202, -- Talonpriest Mask
            114203, -- Outcast Dreamcatcher
            114204, -- Apexis Crystal
            114205, -- Apexis Hieroglyph
            114206, -- Apexis Scroll
            114207, -- Beakbreaker of Terokk
            115002, -- Raw Beast Hide
            115467, -- Barkskin Tome
            115471, -- NC-17 Sonic "Boom" Box
            115530, -- N.U.K.U.L.A.R. Target Painter
            116020, -- An Old Key
            116172, -- Perky Blaster
            116392, -- Big Bag of Booty
            116441, -- Highly Enriched Blixtherium Shells
            116452, -- Spring-loaded Spike Trap
            116986, -- Fine Blue and Gold Tent
            116987, -- Fine Blue and Purple Tent
            116988, -- Fine Blue and Green Tent
            116989, -- Ironskin Tent
            116990, -- Outcast's Tent
            116991, -- Enchanter's Tent
            116992, -- Savage Leather Tent
            116993, -- Archmage's Tent
            116994, -- Brute's Tent
            116995, -- Sturdy Tent
            116996, -- Crusader's Tent
            116997, -- Blood Elven Tent
            116998, -- High Elven Tent
            117000, -- Deathweaver's Hovel
            117001, -- Patchwork Hut
            117002, -- Elune's Retreat
            117003, -- Orgrimmar's Reach
            117004, -- Simple Tent
            117005, -- Distressingly Furry Tent
            117006, -- Ornate Alliance Tent
            117007, -- Ornate Horde Tent
            117008, -- Voodoo Doctor's Hovel
            117009, -- Nomad's Spiked Tent
            117491, -- Ogre Waystone
            118043, -- Broken Bones
            118099, -- Gorian Artifact Fragment
            118197, -- Auction Memory Socket
            118265, -- Echoing Betrayal
            118331, -- Auction Connecting Valve
            118332, -- Auction A.D.D.O.N.S Installer
            118333, -- Universal Language Compensator
            118334, -- Universal Language Filter
            118335, -- Universal Language Repository
            118336, -- Super Cooling Regulator
            118337, -- Super Cooling Tubing
            118338, -- Super Cooling Coolant
            118339, -- Super Cooling Pump
            118340, -- Cyclical Power Converter
            118341, -- Cyclical Power Housing
            118342, -- Cyclical Power Framing
            118343, -- Cyclical Power Sequencer
            118344, -- Arcane Crystal Casing
            118345, -- Arcane Crystal Conduit
            118346, -- Arcane Crystal Amplifier
            118347, -- Arcane Crystal Focusing Lens
            118592, -- Partial Receipt: Gizmothingies
            118698, -- Wings of the Outcasts
            119094, -- Partial Receipt: Flask of Funk
            119095, -- Partial Receipt: Tailored Underwear
            119096, -- Partial Receipt: Book of Troll Poetry
            119097, -- Partial Receipt: Gently-Used Bandages
            119098, -- Partial Receipt: Druidskin Rug
            119099, -- Partial Receipt: Chainmail Socks
            119100, -- Partial Receipt: Pickled Red Herring
            119101, -- Partial Receipt: Invisible Dust
            119102, -- Partial Receipt: True Iron Door Handles
            120146, -- Smuggled Sack of Gold
            120147, -- Bloody Gold Purse
            120312, -- Bulging Sack of Coins
            120989, -- Goldspade's Journal
            121820, -- Compiled Research
            121837, -- Megacharge's Cookbook
            122101, -- Argoram's Journal
            122147, -- Grinning Tolg's Journal
            122154, -- Artificer Maatun's Journal
            122398, -- Garrison Scout Report
            122677, -- Bag of Gold
            124670, -- Sealed Darkmoon Crate
            127141, -- Bloated Thresher
            128206, -- Crackle-o-Matic XL
            128322, -- Captain's Skiff
            128513, -- Anniversary Gift
            128650, -- "Merry Munchkin" Costume
            128652, -- Gently Shaken Gift
            128653, -- Winter Veil Gift
            128658, -- Spooky Supplies
            128659, -- Merry Supplies
            128660, -- Ghoulish Guises
            128661, -- Hallow's Glow
            128662, -- Seer's Invitation
            128663, -- Witch's Brew
            128664, -- Creepy Crawlers
            128665, -- Ball of Tangled Lights
            128666, -- Imported Trees
            128667, -- Little Helpers
            128668, -- Festive Outfits
            128669, -- Old Box of Decorations
            128670, -- Savage Gift
            113258, -- Blingtron 5000 Gift Package
            116920, -- True Steel Lockbox
            119438, -- Automated Critter Defense Cannon
            120320, -- Invader's Abandoned Sack
            122195, -- Music Roll: Legends of Azeroth
            122196, -- Music Roll: The Burning Legion
            122197, -- Music Roll: Wrath of the Lich King
            122198, -- Music Roll: The Shattering
            122199, -- Music Roll: Heart of Pandaria
            122200, -- Music Roll: A Siege of Worlds
            122201, -- Music Roll: Stormwind
            122202, -- Music Roll: High Seas
            122203, -- Music Roll: Ironforge
            122204, -- Music Roll: Cold Mountain
            122205, -- Music Roll: Night Song
            122206, -- Music Roll: Gnomeregan
            122207, -- Music Roll: Tinkertown
            122208, -- Music Roll: Exodar
            122209, -- Music Roll: Curse of the Worgen
            122210, -- Music Roll: Orgrimmar
            122211, -- Music Roll: War March
            122212, -- Music Roll: Undercity
            122213, -- Music Roll: Thunder Bluff
            122214, -- Music Roll: Mulgore Plains
            122215, -- Music Roll: Zul'Gurub Voodoo
            122216, -- Music Roll: The Zandalari
            122217, -- Music Roll: Silvermoon
            122218, -- Music Roll: Rescue the Warchief
            122219, -- Music Roll: Way of the Monk
            122221, -- Music Roll: Song of Liu Lang
            122222, -- Music Roll: Angelic
            122223, -- Music Roll: Ghost
            122224, -- Music Roll: Mountains
            122226, -- Music Roll: Magic
            122228, -- Music Roll: The Black Temple
            122229, -- Music Roll: Invincible
            122231, -- Music Roll: Karazhan Opera House
            122232, -- Music Roll: The Argent Tournament
            122233, -- Music Roll: Lament of the Highborne
            122234, -- Music Roll: Faerie Dragon
            122236, -- Music Roll: Totems of the Grizzlemaw
            122237, -- Music Roll: Mountains of Thunder
            122238, -- Music Roll: Darkmoon Carousel
            122239, -- Music Roll: Shalandis Isle
            126911, -- Bronze Strongbox
            126912, -- Steel Strongbox
            126913, -- Steel Strongbox
            126916, -- Bronze Strongbox
            128215, -- Dented Ashmaul Strongbox
            128216, -- Dented Ashmaul Strongbox
            108739, -- Pretty Draenor Pearl
            112633, -- Frostdeep Minnow
            113375, -- Vindicator's Armor Polish Kit
            115503, -- Blazing Diamond Pendant
            116158, -- Lunarfall Carp
            118759, -- Alchemy Experiment
            108735, -- Arena Master's War Horn
            113000, -- Oozing Amulet
            113001, -- Sparkling Amulet
            113002, -- Ruby Amulet
            113003, -- Opal Amulet
            113130, -- Recipe: Fiona's Remedy
            116114, -- Prestige Card: The Turn
            116202, -- Pet Care Package
            116404, -- Pilgrim's Bounty
            116456, -- Scroll of Storytelling
            117573, -- Wayfarer's Bonfire
            118190, -- Blixthraz's Frightening Grudgesolver
            118375, -- Arcane Crystal Module
            118376, -- Auction Control Module
            118377, -- Universal Language Module
            118378, -- Super Cooling Module
            118379, -- Cyclical Power Module
            118593, -- Merchant Card
            119190, -- Unclaimed Payment
            120352, -- Garrison Guild Banners
            127148, -- Silas' Secret Stash
            127272, -- Rickety Glider
            127865, -- A Tiny Viking Helmet
            127867, -- A Tiny Ninja Shroud
            127869, -- A Tiny Plated Helm
            127870, -- A Tiny Pirate Hat
            128874, -- A Tiny Scarecrow Costume
            119439, -- Personal Voodoo Doll
            119440, -- Training Shoes
            119447, -- Training Wheels
            111925, -- Engineering Supplies
            115499, -- Personal Rocket Courier
            115506, -- Treessassin's Guise
            116113, -- Breath of Talador
            118191, -- Archmage Vargoth's Spare Staff
            118193, -- Mysterious Shining Lockbox
            118224, -- Ogre Brewing Kit
            118935, -- Ever-Blooming Frond
            119000, -- Highmaul Lockbox
            119036, -- Box of Storied Treasures
            119037, -- Supply of Storied Rarities
            119040, -- Cache of Mingled Treasures
            119041, -- Strongbox of Mysterious Treasures
            119042, -- Crate of Valuable Treasures
            119043, -- Trove of Smoldering Treasures
            119093, -- Aviana's Feather
            119130, -- Pile of Blackfuse Shredder Scrap
            119139, -- Sacked Caravan Plunder
            119416, -- Magnaron Heart
            119417, -- Preserved Hydra Blood
            119421, -- Sha'tari Defender's Medallion
            120319, -- Invader's Damaged Cache
            120857, -- Barrel of Bandanas
            122117, -- Cursed Feather of Ikzan
            122613, -- Stash of Dusty Music Rolls
            122618, -- Misprinted Draenic Coin
            122637, -- S.E.L.F.I.E. Camera
            122661, -- S.E.L.F.I.E. Lens Upgrade Kit
            122700, -- Portable Audiophone
            123851, -- Photo B.O.M.B.
            123857, -- Runic Pouch
            123975, -- Greater Bounty Spoils
            126910, -- Silver Strongbox
            126915, -- Silver Strongbox
            126921, -- Ashmaul Strongbox
            126922, -- Ashmaul Strongbox
            127652, -- Felflame Campfire
            127659, -- Ghostly Iron Buccaneer's Hat
            127831, -- Challenger's Strongbox
            128462, -- Karabor Councilor's Attire
            128471, -- Frostwolf Grunt's Battlegear
            128472, -- Battlegear of the Frostwolves
            128473, -- Packaged Ceremonial Karabor Finery
            128803, -- Savage Satchel of Cooperation
            128807, -- Coin of Many Faces
            118267, -- Ravenmother Offering
            108743, -- Deceptia's Smoldering Boots
            116118, -- Surplus Auchenai Weaponry
            116122, -- Burning Legion Missive
            116130, -- Draenic Crystal Fragments
            116131, -- Amethyl Crystal
            109739, -- Star Chart
            113531, -- Ashes of A'kumbo
            113542, -- Whispers of Rai'Vosh
            107645, -- Iron Horde Weapon Cache
            110424, -- Savage Safari Hat
            111476, -- Stolen Breath
            113271, -- Giant Kaliri Egg
            113540, -- Ba'ruun's Bountiful Bloom
            117550, -- Angry Beehive
            117569, -- Giant Deathweb Egg
            118244, -- Iron Buccaneer's Hat
            118710, -- Exploratron 2000 Spare Parts
            120276, -- Outrider's Bridle Chain
            122478, -- Scouting Report: Frostfire Ridge
            122479, -- Scouting Report: Shadowmoon Valley
            122480, -- Scouting Report: Gorgrond
            122481, -- Scouting Report: Talador
            122482, -- Scouting Report: Spires of Arak
            122483, -- Scouting Report: Nagrand
            128223, -- Bottomless Stygana Mushroom Brew
            114669, -- Tranquil Satchel of Helpful Goods
            124054, -- Time-Twisted Anomaly
            114662, -- Tranquil Satchel of Helpful Goods
            133511, -- Gurboggle's Gleaming Bauble
            114655, -- Scorched Satchel of Helpful Goods
            114648, -- Scorched Satchel of Helpful Goods
            129938, -- Will of Northrend
            129952, -- Hourglass of Eternity
            129965, -- Grizzlesnout's Fang
            114641, -- Icy Satchel of Helpful Goods
            114634, -- Icy Satchel of Helpful Goods
            129926, -- Mark of the Ashtongue
            122122, -- Darkmoon Tonk Controller
            110678, -- Darkmoon Ticket Fanny Pack
            111418, -- Mushroom Juice
            111474, -- Colossal Pearl
            111543, -- Pile of Frostfire Turnips
            113004, -- Locket of Dreams
            113005, -- Chain of Hopes
            113006, -- Choker of Nightmares
            113096, -- Bloodmane Charm
            113388, -- Shadowmoon Astrologer's Almanac
            114227, -- Bubble Wand
            115345, -- Alliance Supply Chest Key
            115346, -- Horde Supply Chest Key
            115468, -- Permanent Frost Essence
            115472, -- Permanent Time Bubble
            116067, -- Ring of Broken Promises
            116115, -- Blazing Wings
            116139, -- Haunting Memento
            116400, -- Silver-Plated Turkey Shooter
            116435, -- Cozy Bonfire
            116440, -- Burning Defender's Medallion
            116758, -- Brewfest Banner
            118100, -- Highmaul Relic
            118427, -- Autographed Hearthstone Card
            118695, -- Toxicfang Venom
            118924, -- Cache of Arms
            118925, -- Plundered Booty
            118926, -- Huge Pile of Skins
            118927, -- Maximillian's Laundry
            118928, -- Faintly-Sparkling Cache
            118929, -- Sack of Mined Ore
            118930, -- Bag of Everbloom Herbs
            118931, -- Leonid's Bag of Supplies
            118937, -- Gamon's Braid
            118938, -- Manastorm's Duplicator
            119001, -- Mystery Keg
            119003, -- Void Totem
            119039, -- Lilian's Warning Sign
            119083, -- Fruit Basket
            119092, -- Moroes' Famous Polish
            119163, -- Soul Inhaler
            119178, -- Black Whirlwind
            119182, -- Soul Evacuation Crystal
            119189, -- Unclaimed Payment
            119215, -- Robo-Gnomebulator
            119217, -- Alliance Flag of Victory
            119218, -- Horde Flag of Victory
            119219, -- Warlord's Flag of Victory
            119220, -- Alliance Gladiator's Banner
            119221, -- Horde Gladiator's Banner
            122119, -- Everlasting Darkmoon Firework
            122120, -- Gaze of the Darkmoon
            122121, -- Darkmoon Gazer
            122123, -- Darkmoon Ring-Flinger
            122124, -- Darkmoon Cannon
            122126, -- Attraction Sign
            122128, -- Checkered Flag
            122129, -- Fire-Eater's Vial
            122191, -- Bloody Stack of Invitations
            122274, -- Tome of Knowledge
            122283, -- Rukhmar's Sacred Memory
            122304, -- Fandral's Seed Pouch
            122342, -- Auxiliary Scouting Report
            122514, -- Mission Completion Orders
            126931, -- Seafarer's Slidewhistle
            127695, -- Spirit Wand
            127696, -- Magic Pet Mirror
            127707, -- Indestructible Bone
            127861, -- Cave Mushroom Chair
            127864, -- Personal Spotlight
            128025, -- Rattling Iron Cage
            128310, -- Burning Blade
            128313, -- Huge Ogre Cache
            128316, -- Bulging Barrel of Oil
            128319, -- Void-Shrouded Satchel
            128328, -- Skoller's Bag of Squirrel Treats
            128507, -- Inflatable Thunderfury, Blessed Blade of the Windseeker
            128636, -- Endothermic Blaster
            128794, -- Sack of Spectral Spiders
            129295, -- Spike-Toed Booterang
            115501, -- Kowalski's Music Box
            115505, -- LeBlanc's Recorder
            116396, -- LeBlanc's Recorder
            113575, -- Secretive Whistle
            116980, -- Invader's Forgotten Treasure
            117414, -- Stormwind Guard Armor Package
            118529, -- Cache of Highmaul Treasures
            118530, -- Cache of Highmaul Treasures
            118531, -- Cache of Highmaul Treasures
            119322, -- Shoulders of the Iron Protector
            122163, -- Routed Invader's Crate of Spoils
            122484, -- Blackrock Foundry Spoils
            122485, -- Blackrock Foundry Spoils
            122486, -- Blackrock Foundry Spoils
            122674, -- S.E.L.F.I.E. Camera MkII
            126909, -- Gold Strongbox
            126914, -- Gold Strongbox
            126923, -- Champion's Strongbox
            126924, -- Champion's Strongbox
            127853, -- Iron Fleet Treasure Chest
            127854, -- Iron Fleet Treasure Chest
            127855, -- Iron Fleet Treasure Chest
            128391, -- Iron Fleet Treasure Chest
            110592, -- Unclaimed Black Market Container
            113007, -- Magma-Infused War Beads
            113008, -- Glowing Ancestral Idol
            116138, -- Last Deck of Nemelex Xobeh
            116140, -- Stormshield Prison Key
            116141, -- Warspear Prison Key
            118654, -- Aogexon's Fang
            118655, -- Bergruu's Horn
            118656, -- Dekorhan's Tusk
            118657, -- Direhoof's Hide
            118658, -- Gagrog's Skull
            118659, -- Mu'gra's Head
            118660, -- Thek'talon's Talon
            118661, -- Xelganak's Stinger
            119188, -- Unclaimed Payment
            119211, -- Golden Hearthstone Card: Lord Jaraxxus
            120172, -- Vileclaw's Claw
            122293, -- Trans-Dimensional Bird Whistle
            127398, -- Locket of Precious Memories
            127400, -- Wax-Daubed Signet
            127402, -- Limited-Edition Choker
            127404, -- Limited-Edition Choker
            127406, -- Lovingly Polished Nose Ring
            127407, -- Lava Prism Ring
            127409, -- Sculpted Memorial Urn
            127995, -- Unclaimed Black Market Container
            127115, -- Tome of Chaos
            115280, -- Abrogator Stone
            115981, -- Abrogator Stone Cluster
            120334, -- Satchel of Cosmic Mysteries
            -- JUNK
            122718, -- Clinking Present
            133542, -- Tosselwrench's Mega-Accurate Simulation Viewfinder
            113574, -- Scrap of Cloth
            113576, -- Scrap of Cloth
            133545  -- "New!" Kaja'Cola
        }
    },
    [addon.CONS.QUEST_ID] = {
        107399, -- Limbflayer's Limbflayer
        107400, -- Kaz's Crazy Crate
        107401, -- Passible Provisions
        107656, -- Kaz's Disturbing Crate
        110490, -- Larry Bugged Item
        110492, -- Flamewrought Jewel
        112681, -- Strange Crystal Shard
        114977, -- Salvaged Draenic Metal
        115533, -- Vial of Refined Serum
        122399, -- Scouting Missive: Magnarok
        122400, -- Scouting Missive: Everbloom Wilds
        122401, -- Scouting Missive: Stonefury Cliffs
        122402, -- Scouting Missive: Iron Siegeworks
        122403, -- Scouting Missive: Magnarok
        122404, -- Scouting Missive: Everbloom Wilds
        122405, -- Scouting Missive: Stonefury Cliffs
        122406, -- Scouting Missive: Iron Siegeworks
        122407, -- Scouting Missive: Skettis
        122408, -- Scouting Missive: Skettis
        122409, -- Scouting Missive: Pillars of Fate
        122410, -- Scouting Missive: Shattrath Harbor
        122411, -- Scouting Missive: Pillars of Fate
        122412, -- Scouting Missive: Shattrath Harbor
        122413, -- Scouting Missive: Lost Veil Anzu
        122414, -- Scouting Missive: Lost Veil Anzu
        122415, -- Scouting Missive: Socrethar's Rise
        122416, -- Scouting Missive: Socrethar's Rise
        122417, -- Scouting Missive: Darktide Roost
        122418, -- Scouting Missive: Darktide Roost
        122419, -- Scouting Missive: Gorian Proving Grounds
        122420, -- Scouting Missive: Gorian Proving Grounds
        122421, -- Scouting Missive: Mok'gol Watchpost
        122422, -- Scouting Missive: Mok'gol Watchpost
        122423, -- Scouting Missive: Broken Precipice
        122424, -- Scouting Missive: Broken Precipice
        122458, -- Strange Stone
        122467, -- Strange Stone
        122565, -- Bass Blaster
        122566, -- Laz-Tron Disc Reader
        122567, -- Phonic Amplifier
        122568, -- S.P.R.K. Capacitor
        122569, -- Cord of Ancient Wood
        122574, -- Eye of Kilrogg
        123852, -- Map to Bleeding Hollow Killer
        127006, -- Bleeding Hollow Hunting Map
        133876, -- Scouting Missive: The Pit
        133878, -- Scouting Missive: The Pit
        133883, -- Scouting Missive: The Heart of Shattrath
        133884, -- Scouting Missive: The Heart of Shattrath
        113187, -- Dionor's Death Bloom
        112378, -- Glowing Red Pod
        112394, -- Enlarged Stomper Spore Pod
        112567, -- Spore Covered Leaves
        112568, -- Spore Covered Leaves
        112569, -- Spore Covered Leaves
        112640, -- Skulltaker Skull
        112672, -- Glowing Mushroom
        113243, -- Rangari Pouch
        113260, -- Glowing Red Pod
        113444, -- Crystalized Steam
        113445, -- Crystalized Steam
        113446, -- Globe of Dead Water
        113447, -- Globe of Dead Water
        113448, -- Chunk of Crater Lord
        113449, -- Chunk of Crater Lord
        113453, -- Precious Mushroom
        113454, -- Precious Mushroom
        113456, -- Fang of the Doomwing
        113457, -- Fang of the Doomwing
        113458, -- Ebony Feather
        113459, -- Ebony Feather
        113460, -- Shimmering Scale
        113461, -- Shimmering Scale
        113462, -- Enormous Glowing Salt Crystal
        113463, -- Enormous Glowing Salt Crystal
        113586, -- Acid-Stained Goren Tooth
        113590, -- Acid-Stained Goren Tooth
        114018, -- Worn Ogron Horn
        114019, -- Worn Ogron Horn
        114020, -- Gronnling Scale
        114021, -- Gronnling Scale
        114022, -- Gronn Eye
        114023, -- Gronn Eye
        114024, -- Botani Bloom
        114025, -- Botani Bloom
        114026, -- Orc Thorn
        114027, -- Orc Thorn
        114029, -- Ancient Branch
        114030, -- Ancient Branch
        114031, -- Ravager Claw
        114032, -- Ravager Claw
        114033, -- Wasp Stinger
        114034, -- Wasp Stinger
        114035, -- Basilisk Scale
        114036, -- Basilisk Scale
        114037, -- Elemental Crystal
        114038, -- Elemental Crystal
        115442, -- Saberon Herb Bundle
        115443, -- Saberon Heart
        115974, -- Glowing Pod
        116159, -- Writhing Green Tendril
        116160, -- Writhing Green Tendril
        109095, -- Barbed Thunderlord Spear
        109121, -- Coil of Sturdy Rope
        111907, -- Drudgeboat Salvage
        112696, -- Drudgeboat Salvage
        116748, -- Fire Ammonite Lunker
        116749, -- Blackwater Whiptail Lunker
        116750, -- Blind Lake Sturgeon Lunker
        116751, -- Abyssal Gulper Lunker
        116752, -- Jawless Skulker Lunker
        116753, -- Fat Sleeper Lunker
        116754, -- Molten Catfish
        118041, -- Arcane Trout
        126951, -- Equipment Blueprint: Bilge Pump
        128228, -- Equipment Blueprint: Trained Shark Tank
        128230, -- Equipment Blueprint: High Intensity Fog Lights
        128264, -- Equipment Blueprint: Unsinkable
        128265, -- Equipment Blueprint: Tuskarr Fishing Net
        128266, -- Equipment Blueprint: True Iron Rudder
        128269, -- Equipment Blueprint: Ice Cutter
        128270, -- Equipment Blueprint: Gyroscopic Internal Stabilizer
        128271, -- Equipment Blueprint: Ghostly Spyglass
        128272, -- Equipment Blueprint: Felsmoke Launchers
        128274, -- Equipment Blueprint: Blast Furnace
        72108, -- Stuff Tail
        92752, -- Buttonbelly Mushroom
        93396, -- Plundered Profferings
        97729, -- Scorpar Husk Fragment
        97730, -- Wooly Clefthoof Pelt
        97731, -- Thunderlord Insignia
        97732, -- Warm Frostpear Bulb
        98565, -- Clefthoof Flesh
        98598, -- Exile Wolf Bait
        100855, -- Ga'nar's Hunting Spear
        100857, -- Frostwolf Banner
        100858, -- Steaming Haunch of Meat
        100893, -- Wild Flame Torch
        100899, -- Fallen Frostwolf Artifact
        101407, -- Chuck Meat
        101677, -- Thunderlord Grapple
        101690, -- Vicious Grapple
        101691, -- Coil of Chain
        101692, -- Giantstalker Tactics
        101774, -- Drek'Thar's Fire Totem
        102091, -- Mote of Frostfire
        102149, -- Karabor Weapon
        102156, -- Thunderlord Armband
        102491, -- Chunk of Lunar Rock
        102495, -- Shadowmoon Water
        103994, -- Wooden Spear
        103997, -- Scavenged Gizmo
        103998, -- Bent Framework
        103999, -- Blasting Powder
        104042, -- Cloth Scraps
        104043, -- Leather Strips
        104057, -- Diving Helm
        104066, -- Pirate Hat
        104068, -- Orc Mask
        104069, -- Stylish Hat
        104070, -- Wizard Hat
        104071, -- Shaman Chestpiece
        104072, -- Tuxedo Top
        104073, -- Red Dress
        104083, -- Blue Vest
        104100, -- Stylish Shirt
        104125, -- Iris Crystal
        104247, -- Red Skirt
        104248, -- Leather Shoulderpads
        104249, -- Shadowmoon Kilt
        104250, -- Brewfest Skirt
        104252, -- Stylish Pants
        104350, -- Swamplighter Dust
        104354, -- Moonstone
        105716, -- Engorged Heart
        105913, -- Moonstone
        105936, -- Harvester Tooth Necklace
        105937, -- Giant Squirrel Skull
        105943, -- Elekk Foods
        105944, -- Tasty Nightcrawlers
        105957, -- Orc Mask
        106237, -- Mother of Wolves' Remains
        106238, -- Garloc's Axe
        106239, -- Kroga's Blade
        106243, -- Rakish's Hammer
        106245, -- Beakbreaker Surprise
        106941, -- Redleaf Herb
        106942, -- Head of Splorg
        106943, -- Head of Thunk
        106944, -- Head of Gullok
        106958, -- Winterwasp Antidote
        106986, -- Kurgoth's Shard
        106987, -- Sigil of Karabor
        107066, -- Mulverick's Axe
        107075, -- Turgall's Key
        107076, -- Sylene's Amulet of Illusion
        107255, -- Ogre Ragebrew
        107269, -- Pipeline Plans
        107276, -- The Blade of Kroga
        107278, -- Stormherald of Rakish
        107279, -- Frostwolf Banner
        107285, -- Felblood Sample
        107335, -- Shadowmoon Journal
        107338, -- Laughing Skull Mask
        107361, -- Frostwolf Oil
        107369, -- Spikey Shield
        107370, -- Spikey Club
        107371, -- Jagahari's Charm
        107391, -- Shadow Tome
        107609, -- Prickly Nopal
        107642, -- Skullchief Mask
        107647, -- Goliath Prototype Instruction Manual
        107648, -- Remote Firing Plunger
        107655, -- Iron Missive
        107705, -- Bundle of Orange Crop
        107790, -- Defender's Medallion
        107795, -- Iron Mace
        107797, -- Iron Horde Weapons
        107806, -- Thaelin's Spare Wrench
        107815, -- Iron Shredder Operation Manual Vol. X (Abridged)
        107833, -- Thunderlord Grapple
        107843, -- Iron Shredder Operation Manual Vol. X Page ?
        107851, -- Crystal-Shaper's Tools
        107855, -- Iron Horde Explosives
        107856, -- Melani's Doll
        107859, -- Annals of Aruuna
        107896, -- Shackles
        107898, -- Slave Belt
        107899, -- Gazlowe's Solution
        107918, -- Slave Pants
        107922, -- Crate of Rocks
        107923, -- Barum's Notes
        107934, -- High-Yield Pressure Pack
        107936, -- Spare War Machine Parts
        107937, -- Spare War Machine Parts
        107938, -- The Edge of Garloc
        107949, -- Slaver's Skeleton Key
        107957, -- Shadowmoon Rubbing
        107958, -- Shadowmoon Rubbing
        107959, -- Shadowmoon Rubbing
        107960, -- Shadowmoon Rubbing
        107966, -- Glowworm Silk
        107967, -- Razorfang Teeth
        108292, -- Message to Ner'zhul
        108293, -- Letter from the Iron Horde
        108390, -- Lifebloom Orchid
        108394, -- Riverbeast Heart
        108395, -- Swamplighter Venom
        108396, -- Riotvine
        108409, -- Moonlit Herb
        108410, -- Swamplighter Queen's Tail
        108423, -- Thaelin's Call
        108442, -- Pale Blood
        108481, -- Kuu'rat's Tusks
        108484, -- Massive Stinger
        108490, -- Maa'run's Hoof
        108492, -- Fomic Ore
        108499, -- Soothepetal Flower
        108500, -- Frog Eye
        108532, -- Precisely Severed Pale Eye
        108608, -- Abnormally Large Leaf
        108616, -- Bloodmaul Chains
        108645, -- Handful of Duskwing Dust
        108655, -- Aruunem Berries
        108656, -- Handful of Jorune Crystal Dust
        108664, -- Delicious Clefthoof Ribs
        108665, -- Iron Tide Brew
        108670, -- Raw Elekk Steak
        108678, -- Frightened Volen
        108728, -- Legion Key
        108733, -- Karab'uun
        108734, -- Stolen Axe
        108736, -- Na'Shra's Mining Pick
        108737, -- Bloodmaul Slave Collar
        108741, -- Battleboar Tenderloin
        108742, -- Goren Toenail
        108746, -- Bloodmaul Missive
        108747, -- Ricky
        108749, -- Elixir of Memories
        108774, -- Auchenai Shadow Talisman
        108775, -- Solidified Goren Spit Sample
        108776, -- Curious Gem
        108777, -- Curious Gem
        108824, -- Ahm's Heirloom
        108825, -- Ahm's Heirloom
        108826, -- Goliath Prototype Report
        108886, -- Auchenai Torch
        108896, -- Telmor Registry
        108897, -- Leafshadow
        108898, -- Ogre Tooth
        108899, -- Pendant of Brol
        108951, -- Last Will and Testament
        108952, -- Last Will and Testament
        108998, -- Pulsing Egg Sac
        109011, -- Blackrock Powder Keg
        109023, -- Serathil
        109024, -- Orders from Smokebelcher Depot
        109025, -- Blackrock Ore Sample
        109055, -- Coil of Sturdy Rope
        109056, -- Barbed Thunderlord Spear
        109080, -- Glob of Sticky Tar
        109082, -- Barbed Harpoon
        109087, -- Subdermal Tracking Device
        109089, -- Palemoon Prayer Beads
        109161, -- Thaelin's Quick Fix
        109162, -- Grimoire of Binding
        109163, -- Ango'rosh Spellbook
        109164, -- Sticky Tarbomb
        109186, -- Hagra's Shard
        109187, -- Grogal's Shard
        109196, -- Battle-Worn Frostwolf Banner
        109197, -- Auch'naaru
        109205, -- Fiery Heart
        109206, -- Burning Torch
        109224, -- Gul'var Soul Shards
        109225, -- Gul'dan's Soul Trap
        109246, -- Gul'dan's Soul Trap
        109248, -- Siege Weapon Schematics
        109259, -- Shadow Council Spellbook
        109557, -- Mutation Serum
        109566, -- Baby Rylak
        109575, -- Auch'naaru
        109591, -- Dungeon Key
        109592, -- Key to Corki's Cage
        109622, -- Auchenai Prayerbook
        109700, -- All-Seeing Ring
        109744, -- Deathweb Fang
        109747, -- Gazlowe's Payment
        110116, -- Bloodmaul Shackle Key
        110149, -- Owynn's Mace
        110150, -- The Heart of Fernus
        110225, -- Bladefury's Orders
        110226, -- Dose of Lurker Venom
        110227, -- Frostbloom Leaves
        110228, -- Frostshard
        110229, -- Owynn's Armor
        110230, -- Caravan Shipment
        110231, -- O'rok's Head
        110232, -- Owynn's Dagger
        110240, -- Big Ol' Yeti Hide
        110253, -- Arkonite Crystal
        110268, -- Rat Bait
        110270, -- Vial of "Ogron Be-Gone"
        110271, -- Arkonite Pylon
        110288, -- Aren's Thermometer
        110295, -- Heart of Gorgorek
        110313, -- Wolf Fuzz
        110375, -- Borgal's Flamebinder Totem
        110378, -- Borgal's Totem
        110379, -- Borgal's Waterbinder Totem
        110380, -- Borgal's Windbinder Totem
        110390, -- Arcane Essence
        110391, -- Unrefined Power Crystals
        110394, -- Luminescent Soul Shard
        110418, -- Crystal Giant Heart
        110437, -- Kur'ika, Hammer of Execution
        110438, -- Pile of Elemental Ash
        110443, -- Boiled Ichor
        110444, -- Fresh Orc Blood
        110447, -- Rylak Mind Controller
        110452, -- Shadowbound Orders
        110453, -- Thaelin's Notebook
        110459, -- Mysterious Ring
        110468, -- Dissipation Crystal
        110469, -- Mysterious Boots
        110470, -- Mysterious Hat
        110471, -- Mysterious Staff
        110545, -- The Heart of Fernus
        110557, -- The Staff of an Archmage
        110570, -- Drudgeboat Schedule
        110606, -- Shadow Hunter Garb
        110607, -- Shadow Hunter's Glaive
        110608, -- Shadow Hunter's Mask
        110664, -- Bloodmaul Slave Key
        110683, -- Sha'tari
        110714, -- Saberon Claw
        110719, -- Stolen Goods
        110723, -- Vial of Toad Juice
        110742, -- Hand-Axe
        110799, -- Frostflurry Focus
        110898, -- Iridium Dust
        110904, -- Blackrock Blade
        110997, -- A Gift from the Shadowmoon
        111024, -- Pristine Star Lily
        111065, -- Thulgork's Orders
        111066, -- Crulgorosh's Orders
        111308, -- Frost Iron Ingot
        111322, -- Pulsating Pustule
        111348, -- Ancient Bark
        111409, -- Lunarfall Raven Pelt
        111478, -- Goren Shards
        111485, -- Weathered Wingblades
        111524, -- Ragged Mask
        111527, -- Pendant of Krag
        111528, -- Pendant of Mol
        111529, -- Pendant of Clog
        111558, -- Throne Room Key
        111559, -- Iridescent Egg
        111575, -- Adherent Proclamation
        111577, -- Mo'mor's Holy Hammer
        111591, -- Clarity Elixir
        111619, -- Garrison Blueprints
        111637, -- Zapper Sac
        111735, -- Sack of Supplies
        111736, -- Garrison Records
        111808, -- Footman Longsword
        111809, -- Thunderbrew Keg
        111845, -- Snapdragon Frond
        111846, -- Riverhopper Eye
        111847, -- Prowler Blood
        111850, -- Feather of Syth
        111874, -- Earthen Core
        111884, -- Shadowstalker Ribs
        111885, -- Gloomshade Spore
        111888, -- Gulper Leg
        111891, -- Nagrand Antiquity
        111892, -- The Saga of Terokk
        111905, -- Irridium Dust
        111906, -- Jovite Ore
        111908, -- Draenei Bucket
        111910, -- Goren Gas Extractor
        111931, -- Wrenchwrecker's Inventory Report
        111933, -- Lady Sena's Stash of Stone
        111934, -- Ravager Grub
        111937, -- Lunarblossom
        111938, -- Lantresor's Blade
        111947, -- Blademaster's Banner
        112006, -- Gordunni "Toothpick"
        112020, -- Garrison Blueprints
        112082, -- Living Flame
        112083, -- Living Water
        112084, -- Living Earth
        112085, -- Living Wind
        112088, -- Gordunni Runebeads
        112089, -- Ember Blossom
        112091, -- Smoke Grenade
        112099, -- Major Elixir of Shadows
        112100, -- Elixir of Shadows
        112101, -- Ishaal's Orb
        112135, -- Dread Raven Egg
        112142, -- Worldbreaker Schematics
        112146, -- Worldbreaker Schematic, pp. 37-51
        112147, -- Worldbreaker Schematic, pp. 58-70
        112148, -- Worldbreaker Schematic, pp. 71-84
        112149, -- Citybreaker Schematics
        112166, -- Samedi Fetish
        112195, -- Shadow Dust
        112196, -- Arcane Nexus
        112199, -- Ravenspeaker Scroll Fragment
        112203, -- Citybreaker Schematics
        112204, -- Bloodmane Earthbinder Paw
        112207, -- Ravenspeaker Ritual Knife
        112208, -- Severed Paw of Grimalkinde
        112209, -- Severed Paw of Red-Eye
        112243, -- Ga'nar's Shackle Key
        112244, -- Scroll of Mass Teleportation
        112266, -- Frozen Plant Matter
        112307, -- Emergency Rocket Pack
        112308, -- Fury of the Forge
        112323, -- Blackrock Powder Keg
        112328, -- Youngroot
        112329, -- Fara Poison
        112331, -- "Tillik" of the Flock
        112332, -- Gathered Pebbles
        112334, -- Corrupted Lumber
        112335, -- Congealed Blood of Sethe
        112336, -- Wind Serpent Wing
        112337, -- Blackrock Weapon
        112343, -- Plume of Anzu
        112372, -- Fragment of Anguish
        112380, -- Kil'uun's Beak
        112381, -- Pile of Riverbeast Meat
        112385, -- Star Reading
        112386, -- Fragment of Anguish
        112396, -- Spirit Torch
        112444, -- Scuffed Bangle
        112504, -- Shattrath Guardian Mace
        112541, -- Stink Petal
        112542, -- Scroll of Mass Teleportation
        112543, -- Scroll of Mass Teleportation
        112620, -- Shagor's Collar
        112626, -- Blackwater Whiptail Egg
        112627, -- Abyssal Gulper Eel Egg
        112628, -- Fire Ammonite Egg
        112629, -- Blind Lake Sturgeon Egg
        112630, -- Jawless Skulker Egg
        112631, -- Fat Sleeper Egg
        112634, -- Serviceable Gear
        112635, -- DEPRECATED
        112658, -- Signal Flare
        112667, -- Scavenged Supplies
        112680, -- Heart of the Magnaron
        112682, -- Baby Goren Carrier
        112683, -- QR58 Flame Blaster
        112693, -- Frostweed Seed
        112694, -- Fireweed Seed
        112698, -- G-14 Buster Rocket
        112730, -- Shelly's Report
        112738, -- Crystallized Goren Scale
        112786, -- Goren Shell
        112905, -- Waters of Utrophis
        112906, -- Volatile Oil
        112907, -- Shadow Council Codex
        112908, -- Shaman Stone
        112909, -- Potent Pollen
        112910, -- Healing Bloom
        112911, -- Goren Crystal
        112912, -- Reaver Vine
        112957, -- Prop Rotor
        112958, -- Goren Disguise
        112962, -- Transmission Flywheel
        112963, -- Assorted Engineering Parts
        112965, -- Gorger's Hide
        112966, -- Anima's Harness
        112970, -- Enriched Seeds
        112971, -- Blacksmithing Work Order
        112972, -- Sargerei Weapons
        112990, -- Doomshot
        112993, -- Will of the Genesaur
        112994, -- Lumber
        113009, -- Quenching Waters
        113010, -- Lithic's Gift
        113036, -- Goren Egg
        113074, -- Sargerei Cowl
        113075, -- Sargerei Robe
        113076, -- Sargerei Slippers
        113077, -- Heart of the Fury
        113080, -- A Molten Core
        113081, -- Apexis Core
        113083, -- Nagrand Cherry
        113084, -- Illusion Effigies
        113085, -- Raw Windroc
        113092, -- Goren Suit
        113095, -- Bryan Finn's Schematic
        113100, -- Pair of Leatherhide Ears
        113101, -- Sky-Singer Strag's Totem
        113102, -- Breezestrider Horn
        113104, -- Tall Buck's Tail
        113105, -- Horn of Banthar
        113106, -- Spectral Essence
        113107, -- Rangari Arrow
        113109, -- Frostwolf Axe
        113112, -- Shaman Stone
        113121, -- Apexis Interface
        113122, -- Armory Cannonball
        113127, -- Armory Cannon
        113136, -- Ancient Growth Sap
        113186, -- Page from Scorchbrow's Journal
        113191, -- Lucifrium Bead
        113192, -- Volatile Spore
        113217, -- Teleportation Beacon
        113248, -- Shockscale
        113256, -- Pristine Sporebat Stinger
        113272, -- Mysterious Artifact
        113282, -- The Eye of Anzu
        113292, -- Keeho's Severed Paw
        113293, -- Spineslicer's Husk
        113294, -- Sargerei Insignia
        113320, -- Crate of Cactus Apple Surprise
        113333, -- Crate of Northshire Port
        113339, -- Goblin Battery
        113395, -- Strumner's Sword
        113396, -- Krolan's Shield
        113397, -- Keri's Mug
        113398, -- Burning Log
        113399, -- Mokrik's Battleplan
        113400, -- Rukah's Battleplan
        113401, -- Gar's Battleplan
        113404, -- Fresh Charcoal
        113436, -- Neka's Poison Flask
        113437, -- Overseer Struk's Shield
        113438, -- Ghostogrifier 12000
        113439, -- Aitokk's Axe
        113441, -- Organic Laser Apparatus
        113442, -- Tank Part
        113489, -- Shackle Key
        113492, -- Frostfire Mission Orders
        113493, -- Spires of Arak Mission Orders
        113494, -- Shadowmoon Mission Orders
        113504, -- Basilisk Meat
        113508, -- Blademaster's Banner
        113549, -- Mixed Unit Tactics
        113550, -- Warsong Command Brief
        113551, -- Warsong Outrider Orders
        113552, -- Nagrand Scouting Report
        113573, -- Assassin's Mark
        113577, -- Assassin's Mark
        113578, -- Hearty Soup Bone
        113579, -- Iron Horde Missive
        113587, -- Vial of Wracking Poison
        113594, -- Redtooth Necklace
        113597, -- Miniature Statue
        113603, -- Ogre Bust
        113615, -- Purpletooth Necklace
        113616, -- Golden Medallion
        113629, -- Thukmar's Intel
        113630, -- Poison Barrel
        113635, -- Venomshiv's Secret Formula
        114121, -- Brokyo's Ring
        114142, -- Pristine Fang-Scarred Frostwolf Axe
        114144, -- Pristine Frostwolf Ancestry Scrimshaw
        114146, -- Pristine Wolfskin Snowshoes
        114148, -- Pristine Warsinger's Drums
        114150, -- Pristine Screaming Bullroarer
        114152, -- Pristine Warsong Ceremonial Pike
        114154, -- Pristine Metalworker's Hammer
        114156, -- Pristine Elemental Bellows
        114158, -- Pristine Blackrock Razor
        114160, -- Pristine Weighted Chopping Axe
        114162, -- Pristine Hooked Dagger
        114164, -- Pristine Barbed Fishing Hook
        114166, -- Pristine Calcified Eye In a Jar
        114168, -- Pristine Ceremonial Tattoo Needles
        114170, -- Pristine Cracked Ivory Idol
        114172, -- Pristine Ancestral Talisman
        114174, -- Pristine Flask of Blazegrease
        114176, -- Pristine Gronn-Tooth Necklace
        114178, -- Pristine Doomsday Prophecy
        114180, -- Pristine Headdress of the First Shaman
        114182, -- Pristine Stonemaul Succession Stone
        114184, -- Pristine Stone Manacles
        114186, -- Pristine Ogre Figurine
        114188, -- Pristine Pictogram Carving
        114208, -- Pristine Gladiator's Shield
        114209, -- Pristine Mortar and Pestle
        114210, -- Pristine Eye of Har'gunn the Blind
        114211, -- Pristine Stone Dentures
        114212, -- Pristine Rylak Riding Harness
        114213, -- Pristine Imperial Decree Stele
        114214, -- Pristine Sorcerer-King Toe Ring
        114215, -- Pristine Dreamcatcher
        114216, -- Pristine Burial Urn
        114217, -- Pristine Decree Scrolls
        114218, -- Pristine Solar Orb
        114219, -- Pristine Sundial
        114220, -- Pristine Talonpriest Mask
        114221, -- Pristine Outcast Dreamcatcher
        114222, -- Pristine Apexis Crystal
        114223, -- Pristine Apexis Hieroglyph
        114224, -- Pristine Apexis Scroll
        114229, -- Skothwa's Eye
        114241, -- Tattered Armor Scraps
        114625, -- Zangar Eel
        114627, -- Frostwolf Fishing Journal
        114628, -- Icespine Stinger Bait
        114678, -- Stolen Inscription Papers
        114679, -- Icespine Stinger
        114779, -- Azerothian Rose
        114820, -- Spiresalve
        114827, -- Timber Sample
        114873, -- Moonshell Claw
        114874, -- Moonshell Claw Bait
        114875, -- Eventide Fishing Journal
        114876, -- Shadow Sturgeon
        114898, -- Timber Sample
        114899, -- Timber Sample
        114900, -- Shadowmoon Hides
        114901, -- Silverwing Talon
        114902, -- Neatly Bundled Goods
        114963, -- Grom'kar Dispatch
        114964, -- Kurgthuk's Task Masters
        114967, -- Torch
        114969, -- Waruk's Fractured Hammer
        114976, -- Lug'dol's Head
        114981, -- Waruk's Fractured Hammer
        114986, -- Toxic Umbrafen Herbs
        114987, -- Vial of Toxic Ink
        114988, -- Ghost-Touched Materials
        114989, -- Toxin-Resistant Satchel
        114990, -- Oru'kai's Scepter
        114996, -- Blood Stone
        115011, -- Oru'kai's Ember Ring
        115013, -- Ceremonial Shadowmoon Robes
        115018, -- Patched Shadowmoon Cloth
        115019, -- Imbued Shadowmoon Cloth
        115272, -- Research Journal
        115273, -- The Eye of Anzu
        115274, -- Anti-Fungal Boots
        115275, -- Expedition Supplies
        115276, -- Fungal Spores
        115277, -- Fungal Brain
        115279, -- Pilfered Parts
        115290, -- Oru'kai's Staff
        115344, -- Haephest's Tools
        115372, -- Oru'kai's Ember Ring
        115444, -- Apexis Interface
        115445, -- Rough Lava Diamond
        115454, -- Ameeka's Tome of Tailoring
        115461, -- Marvelous Lava Diamond
        115475, -- Vial of Untested Serum
        115476, -- Heart of Kuruk
        115477, -- Trunk of Autuk
        115478, -- Twig of Loruk
        115480, -- Roots of Hanuk
        115482, -- Worn Gavel
        115489, -- Intricate Crimson Pendant
        115516, -- Empowered Crystal
        115534, -- Penny's Flare Gun
        115590, -- Damaged Hexweave
        115591, -- Woven Fur
        115592, -- Hexweave Swatch
        115816, -- Weaponization Orders
        116063, -- Prestige Card: The Turn
        116068, -- Ring of Promises
        116069, -- Misappropriated Draenic Texts
        116072, -- Blackened Iron Key
        116152, -- Head of Alexi Barov
        116153, -- Head of Weldon Barov
        116195, -- Pristine Clefthoof Hide
        116246, -- Bloodstained Skinning Knife
        116351, -- Sootweed Pitch
        116378, -- Mark of Pet Mastery
        116430, -- Siege Cannon Parts
        116434, -- Black Iron Shell
        116645, -- Blixthraz's Tools
        116747, -- Page from Nat's Fishing Journal
        116755, -- Nat's Hookshot
        116759, -- Blixthraz's Frightening Grudgesolver
        116766, -- Apexis Keystone
        116840, -- Leatherworking Work Order
        116841, -- Tailoring Work Order
        116842, -- Alchemy Work Order
        116843, -- Jewelcrafting Work Order
        116844, -- Enchanting Work Order
        116845, -- Engineering Work Order
        116846, -- Inscription Work Order
        116908, -- Weaponization Orders
        116909, -- Weaponization Orders
        116977, -- The Eye of Anzu
        117396, -- The Eye of Anzu
        117434, -- Phylarch's Research
        117435, -- Xeri'tac's Venom Gland
        117436, -- Depleted Everbloom Seed
        117493, -- Scuffed Bangle
        117978, -- Worldbreaker Schematics
        118116, -- Human Heart
        118117, -- Gnome Heart
        118118, -- Night Elf Heart
        118119, -- Dwarf Heart
        118120, -- Pandaren Heart
        118121, -- Worgen Heart
        118122, -- Draenei Heart
        118179, -- Talbuk Lasso
        118181, -- Clefthoof Lasso
        118182, -- Wolf Lasso
        118183, -- Riverbeast Lasso
        118184, -- Elekk Lasso
        118185, -- Boar Lasso
        118283, -- Wolf Lasso
        118284, -- Talbuk Lasso
        118285, -- Riverbeast Lasso
        118286, -- Elekk Lasso
        118287, -- Clefthoof Lasso
        118288, -- Boar Lasso
        118348, -- Icehoof-In-Training Whistle
        118349, -- Meadowstomper-In-Training Whistle
        118350, -- Riverwallow-In-Training Whistle
        118351, -- Rocktusk-In-Training Whistle
        118352, -- Silverpelt-In-Training Whistle
        118353, -- Snarler-In-Training Whistle
        118356, -- Blood Elf Heart
        118357, -- Goblin Heart
        118358, -- Pandaren Heart
        118359, -- Forsaken Heart
        118360, -- Troll Heart
        118361, -- Tauren Heart
        118362, -- Orc Heart
        118417, -- Crate of Surplus Materials
        118418, -- Mug of Rousing Coffee
        118468, -- Fresh Bread
        118534, -- Giant Ogre Head
        118545, -- Tormmok's Sword
        118546, -- Light of Exodus
        118567, -- Acanthus' Remains
        118570, -- Urna's Fathomstaff
        118571, -- Leorajh's Prayer Beads
        118616, -- Olaf's Shield
        118617, -- Horribly Acidic Solution
        118618, -- Strange Brass Compass
        118619, -- Soulsever Blade
        118620, -- Soulweave Vessel
        118621, -- Sun Crystal
        118622, -- Pristine Plumage
        118623, -- Finkle's Improved Skinner
        118624, -- Shed Proto-Dragon Claw
        118625, -- Dark Parchment
        118626, -- Silver-Lined Arrow
        118627, -- Strangely-Glowing Frond
        118628, -- Overgrown Artifact
        118643, -- Huge Crate of Weapons
        118644, -- Iron Limbcleaver
        118645, -- Miniature Iron Star
        118646, -- Ogre Family Tree
        118647, -- Very Shiny Thing
        118648, -- Nightmare Bell
        118649, -- Bottled Windstorm
        118650, -- Spire Flamefly
        118651, -- Void-Gate Key
        118652, -- Tiny Peachick Hatchling
        118653, -- Iron Autocannon
        118733, -- Unstable Slag Mine
        118909, -- Sigil of Highmaul
        118943, -- Overgrown Heart
        119113, -- Arcane Bomb
        119127, -- Sargerei Battle Plans
        119153, -- Bronze Victory
        119154, -- Silver Victory
        119155, -- Gold Victory
        119184, -- Ancient Snarlpaw Skull
        119208, -- The Prophet's Arcanum
        119317, -- Curious Growth
        119441, -- Icehoof-In-Training Whistle
        119442, -- Snarler-In-Training Whistle
        119443, -- Silverpelt-In-Training Whistle
        119444, -- Rocktusk-In-Training Whistle
        119445, -- Riverwallow-In-Training Whistle
        119446, -- Meadowstomper-In-Training Whistle
        119450, -- Void Lantern
        119451, -- Crude Effigy
        119452, -- Torn Cloth
        119453, -- Ancient Bone
        119454, -- Small Fruit
        119455, -- Elder Incense
        119456, -- Wooden Bowl
        120115, -- No Helmet [Diving Helm Geoset] - HIDE EVERYTHING
        120120, -- Spirit Effigy
        120180, -- Ogre Cage Key
        120290, -- Secret Meeting Details
        120979, -- Gutrek's Hilt
        120997, -- Gutrek's Pommel
        120998, -- Gutrek's Blade
        121814, -- Gutrek's Cleaver
        121819, -- Research Notes
        121822, -- Journal Page
        121823, -- Amulet of Rukhmar
        121830, -- Precision Blasting Powder
        121831, -- High Voltage Detonator
        121832, -- Demon's Blood
        121834, -- Grimoire of the Nameless Void
        122096, -- Gronnsbane's Blade
        122098, -- Gronnsbane's Haft
        122099, -- Gronnsbane's Weight
        122102, -- Gronnsbane
        122103, -- Empowered Gronnsbane
        122132, -- Pile of Partially Digested Lobstrok Meat
        122133, -- Humming Draenite Spike
        122134, -- Iceshatter Carapace
        122144, -- Bonethorn Vine
        122145, -- Shadethistle Leaves
        122146, -- Steamcap Mushrooms
        122150, -- The Silent Skull
        122160, -- Rough Crystal Shard
        122161, -- Cracked Crystal Shard
        122162, -- Smooth Crystal Shard
        122189, -- Dream of Argus
        122429, -- Iskar's Tome of Shadows
        122463, -- Reshad's Volatile Concoction
        122515, -- Explosive Charges
        122516, -- Detonator
        122536, -- Traveler's Token
        123866, -- Drained Infernal Core
        123867, -- Roark's Shipyard Blueprints
        123877, -- Transmuted Core
        126930, -- Faded Treasure Map
        127282, -- Incomprehensible Ship Blueprint
        127283, -- Readable Ship Blueprint
        127682, -- Partially Digested Orc Arm
        128027, -- Black Market Shipment
        128346, -- Fel-Corrupted Apexis Fragment
        128428, -- Bleeding Hollow Ritual Blade
        128429, -- Iron Horde Naval Manifest
        128431, -- Vial of Fel Blood
        128432, -- Shadow Council Missive
        128433, -- Unrefined Draenic Crystal
        128434, -- Wargronn Harness
        128438, -- Tanaan Jungle Tooth
        128675, -- Spirit Bomb
        112015, -- Forsaken Brains
        112113, -- Pandaren Hide
        112119, -- Worgen Snout
        112120, -- Troll Feet
        112121, -- Draenei Tail
        112122, -- Orc Tooth
        112123, -- Tauren Hoof
        112124, -- Severed Night Elf Head
        112125, -- Goblin Nose
        112126, -- Dwarf Spine
        112127, -- Tuft of Gnome Hair
        112128, -- Blood Elf Ear
        112131, -- Human Bone Chip
        114780, -- Pure Solium Band
        112271, -- Eight of Iron
        112272, -- Seven of Iron
        112273, -- Six of Iron
        112274, -- Four of Iron
        112275, -- Five of Iron
        112276, -- Three of Iron
        112277, -- Two of Iron
        112278, -- Ace of Iron
        112279, -- Eight of Visions
        112280, -- Seven of Visions
        112281, -- Six of Visions
        112282, -- Five of Visions
        112283, -- Four of Visions
        112284, -- Three of Visions
        112285, -- Two of Visions
        112286, -- Ace of Visions
        112287, -- Eight of War
        112288, -- Seven of War
        112289, -- Six of War
        112290, -- Five of War
        112291, -- Four of War
        112292, -- Three of War
        112293, -- Two of War
        112294, -- Ace of War
        112295, -- Eight of the Moon
        112296, -- Seven of the Moon
        112297, -- Six of the Moon
        112298, -- Five of the Moon
        112299, -- Four of the Moon
        112300, -- Three of the Moon
        112301, -- Two of the Moon
        112302, -- Ace of the Moon
        122572, -- Mangled Journal
        122573, -- Mangled Journal
        127989, -- Waterlogged Manifest
        112692, -- Auchenai Spirit Shard
        109012, -- Frostwolf Ancestral Totem
        112566, -- Mysterious Flask
        112655, -- Book of Alchemical Secrets
        113103, -- Mysterious Flask
        114877, -- Dirty Note
        114965, -- Fractured Forge Hammer
        114972, -- Cryptic Tome of Tailoring
        114973, -- Frostwolf Tailoring Kit
        114984, -- Mysterious Satchel
        115008, -- Enchanted Highmaul Bracer
        115278, -- Gnomish Location Transponder
        115281, -- Enchanted Highmaul Bracer
        115287, -- Intricate Crimson Pendant
        115343, -- Haephest's Satchel
        115350, -- Enchanted Highmaul Bracer
        115507, -- Drained Crystal Fragment
        115593, -- Illegible Sootstained Notes
        116173, -- Tattered Frostwolf Shroud
        116438, -- Burned-Out Hand Cannon
        133377, -- Smoldering Timewarped Ember
        133378, -- Smoldering Timewarped Ember
        129928, -- Frigid Timewarped Prism
        129747, -- Swirling Timewarped Vial
        122190, -- Ring of Blood Invitation
        128418, -- Soul Remnant
        128421, -- Fel Essence
        120209, -- Essence of the Iron Conqueror
        120210, -- Essence of the Iron Protector
        120211, -- Essence of the Iron Vanquisher
        120283, -- Essence of the Iron Conqueror
        120284, -- Essence of the Iron Protector
        120285, -- Essence of the Iron Vanquisher
        128417, -- Soul Remnant
        128420, -- Fel Essence
        120206, -- Essence of the Iron Conqueror
        120207, -- Essence of the Iron Protector
        120208, -- Essence of the Iron Vanquisher
        120280, -- Essence of the Iron Conqueror
        120281, -- Essence of the Iron Protector
        120282, -- Essence of the Iron Vanquisher
        128416, -- Soul Remnant
        128419, -- Fel Essence
        119310, -- Essence of the Iron Conqueror
        119316, -- Essence of the Iron Vanquisher
        119323, -- Essence of the Iron Protector
        120277, -- Essence of the Iron Conqueror
        120278, -- Essence of the Iron Vanquisher
        120279, -- Essence of the Iron Protector
        118382, -- Ember of the Mountain
        118383, -- Heart of Stone
        118384, -- Sliver of Iron
        118385, -- Sliver of Iron
        118386, -- Heart of Stone
        118387, -- Ember of the Mountain
        118388, -- Sliver of Iron
        118389, -- Heart of Stone
        118390, -- Ember of the Mountain
        112303, -- Iron Deck
        112304, -- Moon Deck
        112305, -- Visions Deck
        112306, -- War Deck
        113135, -- Iron Joker
        113139, -- Visions Joker
        113140, -- War Joker
        113142, -- Moon Joker
        133762, -- Remnant of Chaos
        118587, -- Sanketsu
        118589, -- Sanketsu
        127041, -- Pearl of Farahlon
        127045, -- Soaked Ogre Decoder
        128693, -- Draenic Sea Chart
        115479, -- Heart of the Fury
        115493, -- Flamebender's Tome
        115494, -- Draenic Thaumaturgical Orb
        115509, -- Elemental Tablet
        115510, -- Elemental Rune
        115523, -- Blackhand's Severed Arm
        122155, -- Orb of Dominion
        115288, -- Felbreaker's Tome
        115289, -- Sigil of the Sorcerer King
        113682, -- Core of Flame
        114107, -- Core of Iron
        114138, -- Core of Life
        114240, -- Corrupted Blood of Teron'gor
        133150, -- Commendation of the Dragonmaw Clan
        133151, -- Commendation of the Wildhammer Clan
        133152, -- Commendation of the Guardians of Hyjal
        133154, -- Commendation of the Ramkahen
        133159, -- Commendation of The Earthen Ring
        133160  -- Commendation of Therazane
    },
    [addon.CONS.RECIPES_ID] = {
        113111, -- Warbinder's Ink
        114931,  -- Cerulean Pigment
        [addon.CONS.R_BOOKS_ID] = {
            113992, -- Scribe's Research Notes
            109586, -- Brittle Cartography Journal
            111349, -- A Treatise on Mining in Draenor
            111350, -- A Compendium of the Herbs of Draenor
            111351, -- A Guide to Skinning in Draenor
            111356, -- Fishing Guide to Draenor
            111364, -- First Aid in Draenor
            111387, -- The Joy of Draenor Cooking
            120137, -- Tome of Polymorph: Polar Bear Cub
            120138, -- Tome of Polymorph: Monkey
            120139, -- Tome of Polymorph: Penguin
            120140, -- Tome of Polymorph: Porcupine
            120327  -- Guide: Sharpshooting
        },
        [addon.CONS.R_ALCHEMY_ID] = {
            112022, -- Recipe: Mighty Shadow Protection Potion
            112023, -- Recipe: Draenic Philosopher's Stone
            112024, -- Recipe: Draenic Agility Flask
            112026, -- Recipe: Draenic Intellect Flask
            112027, -- Recipe: Draenic Strength Flask
            112030, -- Recipe: Draenic Stamina Flask
            112031, -- Recipe: Greater Draenic Agility Flask
            112033, -- Recipe: Greater Draenic Intellect Flask
            112034, -- Recipe: Greater Draenic Strength Flask
            112037, -- Recipe: Greater Draenic Stamina Flask
            112038, -- Recipe: Draenic Agility Potion
            112039, -- Recipe: Draenic Intellect Potion
            112040, -- Recipe: Draenic Strength Potion
            112041, -- Recipe: Draenic Armor Potion
            112042, -- Recipe: Draenic Channeled Mana Potion
            112043, -- Recipe: Draenic Mana Potion
            112045, -- Recipe: Draenic Rejuvenation Potion
            112047, -- Recipe: Transmorphic Tincture
            122599, -- Tome of Sorcerous Elements
            122600, -- Recipe: Savage Blood
            122605, -- Tome of the Stones
            122710, -- Recipe: Primal Alchemy
            128160, -- Recipe: Wildswater
            128161, -- Recipe: Elemental Distillate
            109558, -- A Treatise on the Alchemy of Draenor
            115356, -- Draenor Blacksmithing
            115357, -- Draenor Tailoring
            115358, -- Draenor Leatherworking
            115359  -- Draenor Jewelcrafting
        },
        [addon.CONS.R_BLACKSMITHING_ID] = {
            116726, -- Recipe: Smoldering Helm
            116727, -- Recipe: Smoldering Breastplate
            116728, -- Recipe: Smoldering Greaves
            116729, -- Recipe: Steelforged Greataxe
            116730, -- Recipe: Steelforged Saber
            116731, -- Recipe: Steelforged Dagger
            116732, -- Recipe: Steelforged Hammer
            116733, -- Recipe: Steelforged Shield
            116734, -- Recipe: Truesteel Grinder
            116735, -- Recipe: Truesteel Pauldrons
            116736, -- Recipe: Truesteel Helm
            116737, -- Recipe: Truesteel Greaves
            116738, -- Recipe: Truesteel Gauntlets
            116739, -- Recipe: Truesteel Breastplate
            116740, -- Recipe: Truesteel Armguards
            116741, -- Recipe: Truesteel Boots
            116742, -- Recipe: Truesteel Waistguard
            116743, -- Recipe: Truesteel Essence
            116745, -- Recipe: Steelforged Essence
            118044, -- Recipe: Truesteel Reshaper
            119329, -- Recipe: Soul of the Forge
            120260, -- Recipe: Steelforged Axe
            120262, -- Recipe: Steelforged Aegis
            122705, -- Recipe: Riddle of Truesteel
            127725, -- Recipe: Mighty Steelforged Essence
            127727, -- Recipe: Mighty Truesteel Essence
            127743, -- Recipe: Savage Steelforged Essence
            127745  -- Recipe: Savage Truesteel Essence
        },
        [addon.CONS.R_COOKING_ID] = {
            118310, -- Recipe Idea: Hearty Elekk Steak
            118311, -- Recipe Idea: Blackrock Ham
            118312, -- Recipe Idea: Pan-Seared Talbuk
            118313, -- Recipe Idea: Braised Riverbeast
            118314, -- Recipe Idea: Rylak Crepes
            118315, -- Recipe Idea: Clefthoof Sausages
            118316, -- Recipe Idea: Steamed Scorpion
            118317, -- Recipe Idea: Grilled Gulper
            118318, -- Recipe Idea: Sturgeon Stew
            118319, -- Recipe Idea: Fat Sleeper Cakes
            118320, -- Recipe Idea: Fiery Calamari
            118321, -- Recipe Idea: Skulker Chowder
            118322, -- Recipe Idea: Talador Surf and Turf
            118323, -- Recipe Idea: Blackrock Barbecue
            118324, -- Recipe Idea: Frosty Stew
            118325, -- Recipe Idea: Sleeper Surprise
            118326, -- Recipe Idea: Calamari Crepes
            118327, -- Recipe Idea: Gorgrond Chowder
            118328, -- Recipe Idea: Feast of Blood
            118329, -- Recipe Idea: Feast of the Waters
            122555, -- Recipe: Sleeper Sushi
            122556, -- Recipe: Buttered Sturgeon
            122557, -- Recipe: Jumbo Sea Dog
            122558, -- Recipe: Pickled Eel
            122559, -- Recipe: Salty Squid Roll
            122560, -- Recipe: Whiptail Fillet
            126928, -- Recipe: Lemon Herb Filet
            128501, -- Recipe: Fel Eggs and Ham
            126927, -- Recipe: Sugar-Crusted Fish Feast
            126929  -- Recipe: Fancy Darkmoon Feast
        },
        [addon.CONS.R_ENCHANTING_ID] = {
            118394, -- Formula: Enchant Cloak - Breath of Critical Strike
            118429, -- Formula: Enchant Cloak - Breath of Haste
            118430, -- Formula: Enchant Cloak - Breath of Mastery
            118432, -- Formula: Enchant Cloak - Breath of Versatility
            118433, -- Formula: Enchant Cloak - Gift of Critical Strike
            118434, -- Formula: Enchant Cloak - Gift of Haste
            118435, -- Formula: Enchant Cloak - Gift of Mastery
            118437, -- Formula: Enchant Cloak - Gift of Versatility
            118438, -- Formula: Enchant Neck - Breath of Critical Strike
            118439, -- Formula: Enchant Neck - Breath of Haste
            118440, -- Formula: Enchant Neck - Breath of Mastery
            118442, -- Formula: Enchant Neck - Breath of Versatility
            118443, -- Formula: Enchant Neck - Gift of Critical Strike
            118444, -- Formula: Enchant Neck - Gift of Haste
            118445, -- Formula: Enchant Neck - Gift of Mastery
            118447, -- Formula: Enchant Neck - Gift of Versatility
            118453, -- Formula: Enchant Ring - Gift of Critical Strike
            118454, -- Formula: Enchant Ring - Gift of Haste
            118455, -- Formula: Enchant Ring - Gift of Mastery
            118457, -- Formula: Enchant Ring - Gift of Versatility
            118458, -- Formula: Enchant Weapon - Mark of the Thunderlord
            118460, -- Formula: Enchant Weapon - Mark of Warsong
            118461, -- Formula: Enchant Weapon - Mark of the Frostwolf
            118462, -- Formula: Enchant Weapon - Mark of Shadowmoon
            118463, -- Formula: Enchant Weapon - Mark of Blackrock
            118467, -- Formula: Enchant Weapon - Mark of Bleeding Hollow
            122711, -- Formula: Temporal Binding
            118464, -- Formula: Temporal Shatter
            111922  -- Draenor Enchanting
        },
        [addon.CONS.R_ENGINEERING_ID] = {
            118476, -- Schematic: Shrediron's Shredder
            118477, -- Schematic: Oglethorpe's Missile Splitter
            118478, -- Schematic: Megawatt Filament
            118480, -- Schematic: Findle's Loot-a-Rang
            118481, -- Schematic: World Shrinker
            118484, -- Schematic: Mechanical Axebeak
            118485, -- Schematic: Lifelike Mechanical Frostboar
            118487, -- Schematic: Personal Hologram
            118488, -- Schematic: Wormhole Centrifuge
            118489, -- Schematic: Swapblaster
            118490, -- Schematic: Blingtron 5000
            118491, -- Schematic: Linkgrease Locksprocket
            118493, -- Schematic: Didi's Delicate Assembly
            118495, -- Schematic: Hemet's Heartseeker
            118497, -- Schematic: Cybergenetic Mechshades
            118498, -- Schematic: Night-Vision Mechshades
            118499, -- Schematic: Plasma Mechshades
            118500, -- Schematic: Razorguard Mechshades
            119177, -- Schematic: Mechanical Scorpid
            120134, -- Recipe: Secrets of Draenor Engineering
            120135, -- Recipe: Secrets of Draenor Enchanting
            120136, -- Recipe: Secrets of Draenor Inscription
            120268, -- Schematic: True Iron Trigger
            122712, -- Schematic: Primal Welding
            127721, -- Schematic: Bi-Directional Fizzle Reducer
            127729, -- Schematic: Advanced Muzzlesprocket
            127739, -- Schematic: Infrablue-Blocker Lenses
            127747, -- Schematic: Taladite Firing Pin
            116142, -- Schematic: Alliance Firework
            116144, -- Schematic: Horde Firework
            116146, -- Schematic: Snake Firework
            111921  -- Draenor Engineering
        },
        [addon.CONS.R_INSCRIPTIONS_ID] = {
            122713, -- Technique: The Spirit of War
            127723, -- Technique: Mighty Ensorcelled Tarot
            127728, -- Technique: Mighty Weapon Crystal
            127741, -- Technique: Savage Ensorcelled Tarot
            127746, -- Technique: Savage Weapon Crystal
            128409, -- Technique: Mass Mill Frostweed
            128410, -- Technique: Mass Mill Fireweed
            128411, -- Technique: Mass Mill Gorgrond Flytrap
            128412, -- Technique: Mass Mill Starflower
            128413, -- Technique: Mass Mill Nagrand Arrowbloom
            128414, -- Technique: Mass Mill Talador Orchid
            118605, -- Technique: Crystalfire Spellstaff
            118606, -- Technique: Darkmoon Card of Draenor
            118607, -- Technique: Etched-Blade Warstaff
            118610, -- Technique: Weapon Crystal
            118613, -- Technique: Shadowtome
            118614, -- Technique: Volatile Crystal
            118615, -- Technique: Warmaster's Firestick
            120265, -- Technique: Ensorcelled Tarot
            111923  -- Draenor Inscription
        },
        [addon.CONS.R_JEWELCRAFTING_ID] = {
            116078, -- Recipe: Taladite Recrystalizer
            116079, -- Recipe: Taladite Amplifier
            116081, -- Recipe: Glowing Iron Band
            116082, -- Recipe: Shifting Iron Band
            116083, -- Recipe: Whispering Iron Band
            116084, -- Recipe: Glowing Iron Choker
            116085, -- Recipe: Shifting Iron Choker
            116086, -- Recipe: Whispering Iron Choker
            116087, -- Recipe: Glowing Blackrock Band
            116088, -- Recipe: Shifting Blackrock Band
            116089, -- Recipe: Whispering Blackrock Band
            116090, -- Recipe: Glowing Taladite Ring
            116091, -- Recipe: Shifting Taladite Ring
            116092, -- Recipe: Whispering Taladite Ring
            116093, -- Recipe: Glowing Taladite Pendant
            116094, -- Recipe: Shifting Taladite Pendant
            116095, -- Recipe: Whispering Taladite Pendant
            116096, -- Recipe: Critical Strike Taladite
            116097, -- Recipe: Haste Taladite
            116098, -- Recipe: Mastery Taladite
            116100, -- Recipe: Versatility Taladite
            116101, -- Recipe: Stamina Taladite
            116102, -- Recipe: Greater Critical Strike Taladite
            116103, -- Recipe: Greater Haste Taladite
            116104, -- Recipe: Greater Mastery Taladite
            116106, -- Recipe: Greater Versatility Taladite
            116107, -- Recipe: Greater Stamina Taladite
            116108, -- Recipe: Reflecting Prism
            116109, -- Recipe: Prismatic Focusing Lens
            122714, -- Recipe: Primal Gemcutting
            127726, -- Recipe: Mighty Taladite Amplifier
            127744, -- Recipe: Savage Taladite Amplifier
            127771, -- Gemcutter Module: Critical Strike
            127772, -- Gemcutter Module: Haste
            127773, -- Gemcutter Module: Mastery
            127775  -- Gemcutter Module: Stamina
        },
        [addon.CONS.R_LEATHERWORKING_ID] = {
            116319, -- Recipe: Journeying Helm
            116320, -- Recipe: Journeying Robes
            116321, -- Recipe: Journeying Slacks
            116322, -- Recipe: Traveling Helm
            116323, -- Recipe: Traveling Tunic
            116324, -- Recipe: Traveling Leggings
            116325, -- Recipe: Leather Refurbishing Kit
            116326, -- Recipe: Powerful Burnished Cloak
            116327, -- Recipe: Nimble Burnished Cloak
            116328, -- Recipe: Brilliant Burnished Cloak
            116329, -- Recipe: Supple Shoulderguards
            116330, -- Recipe: Supple Helm
            116331, -- Recipe: Supple Leggings
            116332, -- Recipe: Supple Gloves
            116333, -- Recipe: Supple Vest
            116334, -- Recipe: Supple Bracers
            116335, -- Recipe: Supple Boots
            116336, -- Recipe: Supple Waistguard
            116337, -- Recipe: Wayfaring Shoulderguards
            116338, -- Recipe: Wayfaring Helm
            116339, -- Recipe: Wayfaring Leggings
            116340, -- Recipe: Wayfaring Gloves
            116341, -- Recipe: Wayfaring Tunic
            116342, -- Recipe: Wayfaring Bracers
            116343, -- Recipe: Wayfaring Boots
            116344, -- Recipe: Wayfaring Belt
            116345, -- Recipe: Burnished Essence
            116347, -- Recipe: Burnished Leather Bag
            116348, -- Recipe: Burnished Mining Bag
            116349, -- Recipe: Burnished Inscription Bag
            116350, -- Recipe: Riding Harness
            120258, -- Recipe: Drums of Fury
            122715, -- Recipe: Spiritual Leathercraft
            127722, -- Recipe: Mighty Burnished Essence
            127740  -- Recipe: Savage Burnished Essence
        },
        [addon.CONS.R_TAILORING_ID] = {
            114851, -- Recipe: Hexweave Cloth
            114852, -- Pattern: Hexweave Embroidery
            114853, -- Pattern: Hexweave Mantle
            114854, -- Pattern: Hexweave Cowl
            114855, -- Pattern: Hexweave Leggings
            114856, -- Pattern: Hexweave Gloves
            114857, -- Pattern: Hexweave Robe
            114858, -- Pattern: Hexweave Bracers
            114859, -- Pattern: Hexweave Slippers
            114860, -- Pattern: Hexweave Belt
            114861, -- Pattern: Powerful Hexweave Cloak
            114862, -- Pattern: Nimble Hexweave Cloak
            114863, -- Pattern: Brilliant Hexweave Cloak
            114864, -- Pattern: Hexweave Bag
            114865, -- Pattern: Elekk Plushie
            114866, -- Pattern: Creeping Carpet
            114868, -- Pattern: Sumptuous Cowl
            114869, -- Pattern: Sumptuous Robes
            114870, -- Pattern: Sumptuous Leggings
            114871, -- Recipe: Hexweave Essence
            120128, -- Recipe: Secrets of Draenor Tailoring
            120129, -- Recipe: Secrets of Draenor Blacksmithing
            120130, -- Recipe: Secrets of Draenor Leatherworking
            120131, -- Recipe: Secrets of Draenor Jewelcrafting
            120132, -- Recipe: Secrets of Draenor Alchemy
            122716, -- Pattern: Primal Weaving
            127724, -- Recipe: Mighty Hexweave Essence
            127742  -- Recipe: Savage Hexweave Essence
        }
    },
    [addon.CONS.TRADE_GOODS_ID] = {
        [addon.CONS.T_CLOTH_ID] = {
            111556, -- Hexweave Cloth
            111557  -- Sumptuous Fur
        },
        [addon.CONS.T_ELEMENTAL_ID] = {
            113261, -- Sorcerous Fire
            113262, -- Sorcerous Water
            113263, -- Sorcerous Earth
            113264, -- Sorcerous Air
            120945  -- Primal Spirit
        },
        [addon.CONS.T_ENCHANTING_ID] = {
            113588, -- Temporal Crystal
            111245, -- Luminous Shard
            109693, -- Draenic Dust
            115504, -- Fractured Temporal Crystal
            115502  -- Small Luminous Shard
        },
        [addon.CONS.T_PARTS_ID] = {
            114056, -- Didi's Delicate Assembly
            111366, -- Gearspring Parts
            119299  -- Secret of Draenor Engineering
        },
        [addon.CONS.T_FISHING_ID] = {
            111671, -- Enormous Abyssal Gulper Eel
            111664, -- Abyssal Gulper Eel
            111659, -- Small Abyssal Gulper Eel
            116818, -- Abyssal Gulper Lunker
            111670, -- Enormous Blackwater Whiptail
            111663, -- Blackwater Whiptail
            111662, -- Small Blackwater Whiptail
            116817, -- Blackwater Whiptail Lunker
            111674, -- Enormous Blind Lake Sturgeon
            111667, -- Blind Lake Sturgeon
            111652, -- Small Blind Lake Sturgeon
            116820, -- Blind Lake Lunker
            111601, -- Enormous Crescent Saberfish
            111595, -- Crescent Saberfish
            111589, -- Small Crescent Saberfish
            111675, -- Enormous Fat Sleeper
            111668, -- Fat Sleeper
            111651, -- Small Fat Sleeper
            116821, -- Fat Sleeper Lunker
            111673, -- Enormous Fire Ammonite
            111666, -- Fire Ammonite
            111656, -- Small Fire Ammonite
            116819, -- Fire Ammonite Lunker
            111676, -- Enormous Jawless Skulker
            111669, -- Jawless Skulker
            111650, -- Small Jawless Skulker
            116822, -- Jawless Skulker Lunker
            111672, -- Enormous Sea Scorpion
            111665, -- Sea Scorpion
            111658, -- Small Sea Scorpion
            122696, -- Sea Scorpion Lunker
            118565, -- Savage Piranha
            127994, -- Felmouth Frenzy Lunker
            127991  -- Felmouth Frenzy
        },
        [addon.CONS.T_HERBS_ID] = {
            109124, -- Frostweed
            109624, -- Broken Frostweed Stem
            109125, -- Fireweed
            109625, -- Broken Fireweed Stem
            109126, -- Gorgrond Flytrap
            109626, -- Gorgrond Flytrap Ichor
            109127, -- Starflower
            109627, -- Starflower Petal
            109128, -- Nagrand Arrowbloom
            109628, -- Nagrand Arrowbloom Petal
            109129, -- Talador Orchid
            109629, -- Talador Orchid Petal
            109130  -- Chameleon Lotus
        },
        [addon.CONS.T_LEATHER_ID] = {
            110611, -- Burnished Leather
            110609, -- Raw Beast Hide
            110610  -- Raw Beast Hide Scraps
        },
        [addon.CONS.T_MEAT_ID] = {
            [addon.CONS.TM_ANIMAL_ID] = {
                109131, -- Raw Clefthoof Meat
                109134, -- Raw Elekk Meat
                109135, -- Raw Riverbeast Meat
                109136, -- Raw Boar Meat
                109132, -- Raw Talbuk Meat
                128500  -- Fel Ham
            },
            [addon.CONS.TM_EGG_ID] = {
                109133, -- Rylak Egg
                128499  -- Fel Egg
            },
            [addon.CONS.TM_FISH_ID] = {
                109143, -- Abyssal Gulper Eel Flesh
                109144, -- Blackwater Whiptail Flesh
                109140, -- Blind Lake Sturgeon Flesh
                109137, -- Crescent Saberfish Flesh
                109139, -- Fat Sleeper Flesh
                109141, -- Fire Ammonite Tentacle
                109138, -- Jawless Skulker Flesh
                109142  -- Sea Scorpion Segment
            }
        },
        [addon.CONS.T_METAL_STONE_ID] = {
            108257, -- Truesteel Ingot
            109118, -- Blackrock Ore
            157516, -- Blackrock Fragment
            109119, -- True Iron Ore
            157517  -- True Iron Nugget
        },
        [addon.CONS.T_OTHER_ID] = {
            109123, -- Crescent Oil
            118700, -- Secret of Draenor Alchemy
            118720, -- Secret of Draenor Blacksmithing
            118721, -- Secret of Draenor Leatherworking
            118722, -- Secret of Draenor Tailoring
            118723, -- Secret of Draenor Jewelcrafting
            119293, -- Secret of Draenor Enchanting
            119297, -- Secret of Draenor Inscription
            118225, -- Highmaul Hops
            113340, -- Blood Card
            113341, -- Blood Card
            113342, -- Blood Card
            113343, -- Blood Card
            113344, -- Blood Card
            113345, -- Blood Card
            113355, -- Card of Omens
            108996, -- Alchemical Catalyst
            112377, -- War Paints
            115524, -- Taladite Crystal
            118472, -- Savage Blood
            108738, -- Giant Draenor Clam
            113346, -- Blood Card
            113347, -- Blood Card
            113348, -- Blood Card
            113349, -- Blood Card
            114836, -- Hexweave Embroidery
            115526, -- Taladite Recrystalizer
            116170, -- Leather Refurbishing Kit
            116428, -- Truesteel Reshaper
            116654, -- Truesteel Grinder
            112270, -- Darkmoon Card of Draenor
            127759, -- Felblight
            113350, -- Blood Card
            113351, -- Blood Card
            127730, -- Savage Burnished Essence
            127732, -- Savage Truesteel Essence
            127733, -- Savage Hexweave Essence
            127734, -- Savage Taladite Amplifier
            127736, -- Savage Ensorcelled Tarot
            127731, -- Savage Steelforged Essence
            127735, -- Savage Weapon Crystal
            127712, -- Mighty Burnished Essence
            127714, -- Mighty Truesteel Essence
            127715, -- Mighty Hexweave Essence
            127716, -- Mighty Taladite Amplifier
            127718, -- Mighty Ensorcelled Tarot
            127713, -- Mighty Steelforged Essence
            127717, -- Mighty Weapon Crystal
            122543, -- Unstable Powerful Ensorcelled Tarot
            120264, -- Unstable Greater Ensorcelled Tarot
            120263, -- Unstable Ensorcelled Tarot
            128012, -- Hexweave Essence
            128013, -- Taladite Amplifier
            128014, -- Burnished Essence
            128015, -- Truesteel Essence
            128018, -- Ensorcelled Tarot
            128010, -- Weapon Crystal
            128016, -- Steelforged Essence
            113352, -- Blood Card
            113353, -- Blood Card
            113354  -- Blood Card
        }
    },
    [addon.CONS.GARRISON_ID] = {
        116395, -- Comprehensive Outpost Construction Guide
        116394,  -- Outpost Building Assembly Notes
        [addon.CONS.G_BLUEPRINTS_ID] = {
            118215, -- Book of Garrison Blueprints
            111812, -- Garrison Blueprint: Alchemy Lab, Level 1
            111929, -- Garrison Blueprint: Alchemy Lab, Level 2
            111930, -- Garrison Blueprint: Alchemy Lab, Level 3
            111968, -- Garrison Blueprint: Barn, Level 2
            111969, -- Garrison Blueprint: Barn, Level 3
            111956, -- Garrison Blueprint: Barracks, Level 1
            111970, -- Garrison Blueprint: Barracks, Level 2
            111971, -- Garrison Blueprint: Barracks, Level 3
            111966, -- Garrison Blueprint: Dwarven Bunker, Level 2
            111967, -- Garrison Blueprint: Dwarven Bunker, Level 3
            111817, -- Garrison Blueprint: Enchanter's Study, Level 1
            111972, -- Garrison Blueprint: Enchanter's Study, Level 2
            111973, -- Garrison Blueprint: Enchanter's Study, Level 3
            109258, -- Garrison Blueprint: Engineering Works, Level 1
            109256, -- Garrison Blueprint: Engineering Works, Level 2
            109257, -- Garrison Blueprint: Engineering Works, Level 3
            109578, -- Garrison Blueprint: Fishing Shack
            111927, -- Garrison Blueprint: Fishing Shack, Level 2
            111928, -- Garrison Blueprint: Fishing Shack, Level 3
            116248, -- Garrison Blueprint: Frostwall Mines, Level 2
            116249, -- Garrison Blueprint: Frostwall Mines, Level 3
            116431, -- Garrison Blueprint: Frostwall Tavern, Level 2
            116432, -- Garrison Blueprint: Frostwall Tavern, Level 3
            111814, -- Garrison Blueprint: Gem Boutique, Level 1
            111974, -- Garrison Blueprint: Gem Boutique, Level 2
            111975, -- Garrison Blueprint: Gem Boutique, Level 3
            111980, -- Garrison Blueprint: Gladiator's Sanctum, Level 2
            111981, -- Garrison Blueprint: Gladiator's Sanctum, Level 3
            111984, -- Garrison Blueprint: Gnomish Gearworks, Level 2
            111985, -- Garrison Blueprint: Gnomish Gearworks, Level 3
            116200, -- Garrison Blueprint: Goblin Workshop, Level 2
            116201, -- Garrison Blueprint: Goblin Workshop, Level 3
            109577, -- Garrison Blueprint: Herb Garden, Level 2
            111997, -- Garrison Blueprint: Herb Garden, Level 3
            109254, -- Garrison Blueprint: Lumber Mill, Level 2
            109255, -- Garrison Blueprint: Lumber Mill, Level 3
            109576, -- Garrison Blueprint: Lunarfall Excavation, Level 2
            111996, -- Garrison Blueprint: Lunarfall Excavation, Level 3
            107694, -- Garrison Blueprint: Lunarfall Inn, Level 2
            109065, -- Garrison Blueprint: Lunarfall Inn, Level 3
            109062, -- Garrison Blueprint: Mage Tower, Level 2
            109063, -- Garrison Blueprint: Mage Tower, Level 3
            111998, -- Garrison Blueprint: Menagerie, Level 2
            111999, -- Garrison Blueprint: Menagerie, Level 3
            111957, -- Garrison Blueprint: Salvage Yard, Level 1
            111976, -- Garrison Blueprint: Salvage Yard, Level 2
            111977, -- Garrison Blueprint: Salvage Yard, Level 3
            111815, -- Garrison Blueprint: Scribe's Quarters, Level 1
            111978, -- Garrison Blueprint: Scribe's Quarters, Level 2
            111979, -- Garrison Blueprint: Scribe's Quarters, Level 3
            116196, -- Garrison Blueprint: Spirit Lodge, Level 2
            116197, -- Garrison Blueprint: Spirit Lodge, Level 3
            112002, -- Garrison Blueprint: Stables, Level 2
            112003, -- Garrison Blueprint: Stables, Level 3
            111982, -- Garrison Blueprint: Storehouse, Level 2
            111983, -- Garrison Blueprint: Storehouse, Level 3
            111816, -- Garrison Blueprint: Tailoring Emporium, Level 1
            111992, -- Garrison Blueprint: Tailoring Emporium, Level 2
            111993, -- Garrison Blueprint: Tailoring Emporium, Level 3
            111813, -- Garrison Blueprint: The Forge, Level 1
            111990, -- Garrison Blueprint: The Forge, Level 2
            111991, -- Garrison Blueprint: The Forge, Level 3
            111818, -- Garrison Blueprint: The Tannery, Level 1
            111988, -- Garrison Blueprint: The Tannery, Level 2
            111989, -- Garrison Blueprint: The Tannery, Level 3
            111986, -- Garrison Blueprint: Trading Post, Level 2
            111987, -- Garrison Blueprint: Trading Post, Level 3
            116185, -- Garrison Blueprint: War Mill, Level 2
            116186, -- Garrison Blueprint: War Mill, Level 3
            127267, -- Ship Blueprint: Carrier
            127268, -- Ship Blueprint: Transport
            127269, -- Ship Blueprint: Battleship (horde)
            127270, -- Ship Blueprint: Submarine
            126900, -- Ship Blueprint: Destroyer
            128492, -- Ship Blueprint: Battleship (alliance)
            -- Blueprints dropped by rare mobs in Tanaan Jungle
            126950, -- Equipment Blueprint: Bilge Pump
            128258, -- Equipment Blueprint: Felsmoke Launchers
            128232, -- Equipment Blueprint: High Intensity Fog Lights
            128255, -- Equipment Blueprint: Ice Cutter
            128231, -- Equipment Blueprint: Trained Shark Tank
            128252, -- Equipment Blueprint: True Iron Rudder
            128257, -- Equipment Blueprint: Ghostly Spyglass
            -- Other blueprints
            128256, -- Equipment Blueprint: Gyroscopic Internal Stabilizer
            128250, -- Equipment Blueprint: Unsinkable
            128251, -- Equipment Blueprint: Tuskarr Fishing Net
            128260, -- Equipment Blueprint: Blast Furnace
            128490, -- Blueprint: Oil Rig
            128444  -- Blueprint: Oil Rig
        },
        [addon.CONS.G_FOLLOWERS_ID] = {
            -- Armor
            120301, -- Armor Enhancement Token
            114745, -- Braced Armor Enhancement
            114808, -- Fortified Armor Enhancement
            114822, -- Heavily Reinforced Armor Enhancement
            114807, -- War Ravaged Armor Set
            114806, -- Blackrock Armor Set
            114746, -- Goredrenched Armor Set
            -- Weapon
            120302, -- Weapon Enhancement Token
            114128, -- Balanced Weapon Enhancement
            114129, -- Striking Weapon Enhancement
            114131, -- Power Overrun Weapon Enhancement
            114616, -- War Ravaged Weaponry
            114081, -- Blackrock Weaponry
            114622, -- Goredrenched Weaponry
            -- Armor & Weapon 
            120313, -- Sanketsu
            128314, -- Frozen Arms of a Hero
            -- Abilities & Traits
            118354, -- Follower Re-training Certificate
            122272, -- Follower Ability Retraining Manual
            122273, -- Follower Trait Retraining Guide
            123858, -- Follower Retraining Scroll Case
            118475, -- Hearthstone Strategy Guide
            118474, -- Supreme Manual of Dance
            122584, -- Winning with Wildlings
            122583, -- Grease Monkey Guide
            122582, -- Guide to Arakkoa Relations
            122580, -- Ogre Buddy Handbook
            -- Other enhancements
            120311, -- The Blademaster's Necklace
            122298, -- Bodyguard Miniaturization Device
            -- Contracts
            116915, -- Inactive Apexis Guardian
            114245, -- Abu'Gar's Favorite Lure
            114243, -- Abu'Gar's Finest Reel
            114242, -- Abu'Gar's Vitality
            119161, -- Contract: Karg Bloodfury
            119162, -- Contract: Cleric Maluuf
            119165, -- Contract: Professor Felblast
            119166, -- Contract: Cacklebone
            119167, -- Contract: Vindicator Heluun
            119248, -- Contract: Dawnseeker Rukaryx
            119233, -- Contract: Kaz the Shrieker
            119240, -- Contract: Lokra
            119242, -- Contract: Magister Serena
            119243, -- Contract: Magister Krelas
            119244, -- Contract: Hulda Shadowblade
            119245, -- Contract: Dark Ranger Velonara
            119252, -- Contract: Rangari Erdanii
            119253, -- Contract: Spirit of Bony Xuk
            119254, -- Contract: Pitfighter Vaandaam
            119255, -- Contract: Bruto
            119256, -- Contract: Glirin
            119257, -- Contract: Penny Clobberbottom
            119267, -- Contract: Ziri'ak
            119288, -- Contract: Daleera Moonfang
            119291, -- Contract: Artificer Andren
            119292, -- Contract: Vindicator Onaala
            119296, -- Contract: Rangari Chel
            119298, -- Contract: Ranger Kaalya
            119418, -- Contract: Morketh Bladehowl
            119420, -- Contract: Miall
            122135, -- Contract: Greatmother Geyah
            122136, -- Contract: Kal'gor the Honorable
            122137, -- Contract: Bruma Swiftstone
            122138, -- Contract: Ulna Thresher
            112737, -- Contract: Ka'la of the Frostwolves
            112848, -- Contract: Daleera Moonfang
            114825, -- Contract: Ulna Thresher
            114826, -- Contract: Bruma Swiftstone
            119164, -- Contract: Arakkoa Outcasts Follower
            119168, -- Contract: Vol'jin's Spear Follower
            119169, -- Contract: Wrynn's Vanguard Follower
            119821, -- Contract: Dawnseeker Rukaryx
            128439, -- Contract: Pallas
            128440, -- Contract: Dowser Goodwell
            128441, -- Contract: Solar Priest Vayx
            128445  -- Contract: Dowser Bigspark   
        },
        [addon.CONS.G_IRONHORDE_ID] = {
            113681, -- Iron Horde Scraps
            113823, -- Crusted Iron Horde Pauldrons
            113822, -- Ravaged Iron Horde Belt
            113821  -- Battered Iron Horde Helmet
        },
        [addon.CONS.G_MINING_ID] = {
            118897, -- Miner's Coffee
            118903  -- Preserved Mining Pick
        },
        [addon.CONS.G_SHIPYARD_ID] = {
            125787, -- Bilge Pump
            127882, -- Blast Furnace
            127884, -- Felsmoke Launcher
            127895, -- Ghostly Spyglass
            127881, -- Gyroscopic Internal Stabilizer
            127662, -- High Intensity Fog Lights
            127880, -- Ice Cutter
            127663, -- Trained Shark Tank
            127883, -- True Iron Rudder
            127894, -- Tuskarr Fishing Net
            127886, -- Unsinkable
            126952, -- Ship: Destroyer
            126983, -- Ship: Submarine
            126986, -- Ship: Carrier
            127134, -- Ship: Battleship
            127135, -- Ship: Transport
            128301, -- Ship: Submarine
            128302, -- Ship: Battleship
            128303, -- Ship: Destroyer
            127833, -- Ship: Destroyer
            128487, -- Ship: The Awakener
            128488, -- Ship: The Awakener
            128638, -- Ship: Destroyer
            128639, -- Ship: Destroyer
            128640, -- Ship: Destroyer
            128641, -- Ship: Destroyer
            128642, -- Ship: Transport
            125786  -- Ship Deed: Carrier
        },
        [addon.CONS.G_WORKORDERS_ID] = {
            140590, -- Large Crate of Salvage
            114119, -- Crate of Salvage
            114116, -- Bag of Salvaged Goods
            114120, -- Big Crate of Salvage
            118473, -- Small Sack of Salvaged Goods
            139593, -- Sack of Salvaged Goods
            114781, -- Timber
            116053, -- Draenic Seeds
            115508, -- Draenic Stone
            113991, -- Iron Trap
            115009, -- Improved Iron Trap
            115010, -- Deadly Iron Trap
            119813, -- Furry Caged Beast
            119814, -- Leathery Caged Beast
            119810, -- Meaty Caged Beast
            119819, -- Caged Mighty Clefthoof
            119817, -- Caged Mighty Riverbeast
            119815, -- Caged Mighty Wolf
            117397, -- Nat's Lucky Coin
            128373, -- Rush Order: Shipyard
            122307, -- Rush Order: Barn
            122487, -- Rush Order: Gladiator's Sanctum
            122490, -- Rush Order: Dwarven Bunker
            122491, -- Rush Order: War Mill
            122496, -- Rush Order: Garden Shipment
            122497, -- Rush Order: Garden Shipment
            122500, -- Rush Order: Gnomish Gearworks
            122501, -- Rush Order: Goblin Workshop
            122502, -- Rush Order: Mine Shipment
            122503, -- Rush Order: Mine Shipment
            122576, -- Rush Order: Alchemy Lab
            122590, -- Rush Order: Enchanter's Study
            122591, -- Rush Order: Engineering Works
            122592, -- Rush Order: Gem Boutique
            122593, -- Rush Order: Scribe's Quarters
            122594, -- Rush Order: Tailoring Emporium
            122595, -- Rush Order: The Forge
            122596  -- Rush Order: The Tannery
        }
    },
    [addon.CONS.ASHRAN_ID] = {
        114982, --  Song Flower
        111842, --  Star Root Tuber
        116410, --  Scroll of Speed
        116411, --  Scroll of Protection
        116412, --  Scroll of Mass Invisibility
        116413, --  Scroll of Town Portal
        117013, --  Wand of Lightning Shield
        117014, --  Wand of Neutralization
        117015, --  Wand of Mana Stealing
        117016, --  Wand of Arcane Imprisonment
        116999, --  Scroll of Replenishment
        117381, --  Wand of Death
        116984, --  Frost Wyrm Egg
        118425, --  Nesingwary's Lost Horn
        118426, --  Scroll of Invoke Yu'lon, the Jade Serpent
        116925, --  Vintage Free Action Potion
        114124, --  Phantom Potion
        117017, --  Ghost Truffle
        115793, --  S.O.S. Relief Flare
        115500, --  Disposable Pocket Flying Machine
        114126, --  Disposable Pocket Flying Machine
        118109, --  Disposable Pocket Flying Machine
        118110, --  Disposable Pocket Flying Machine
        116397, --  Swift Riding Crop
        116398, --  Flimsy X-Ray Goggles
        114125, --  Preserved Discombobulator Ray
        114629  --  Proximity Alarm-o-Bot 2000
    },
    [addon.CONS.A_BOOKS_ID] = {
        114846, -- Sigil of Dark Simulacrum
        118760, -- Book of Rebirth
        114842, -- Book of Flight Form
        112154, -- Guide: Disengage
        116983, -- Guide: Rogue Tracking
        114845, -- Tome of Blink
        114844, -- Scroll of Touch of Death
        112005, -- The Jailer's Libram
        111926, -- Codex of Ascension
        114843, -- Handbook: Pick Pocket
        116982, -- Handbook: Vanish
        114847, -- Tablet of Ghost Wolf
        114848, -- Grimoire Of Convert Demon
        114849  -- Manual Of Spell Reflection
    }
}