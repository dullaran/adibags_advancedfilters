local _, addon = ...


addon.ITEM_DATABASE[addon.CONS.LEGION_ID] = {
    [addon.CONS.CONTAINERS_ID] = {
        138293, -- Explorer's Pack
        123958, -- Demon Hide Satchel
        127035, -- Silkweave Satchel
        130320, -- Addie's Ink-Stained Satchel
        130156, -- Crane Bag
        140335, -- Totem Tote
        129195, -- Vile Stalkerskin Pouch
        138300, -- Madman's Luggage
        142544, -- Horadric Satchel
        142075  -- Imbued Silkweave Bag
    },
    [addon.CONS.CONSUMABLES_ID] = {
        [addon.CONS.C_BANDAGES_ID] = {
            129096, -- Battle-Mender's Dressing
            146971, -- Yseralline Poultice
            133942, -- Silkweave Splint
            136653, -- Silvery Salve
            142332, -- Feathered Luffa
            143636, -- Arcane Splint
            133940  -- Silkweave Bandage
        },
        [addon.CONS.C_CONSUMABLES_ID] = {
            132511, -- Pump-Action Bandage Gun
            141026, -- Charged Spellbomb
            141531, -- Powered Security Module
            132513, -- Gunpack
            132516, -- Gunshoes
            132510, -- Gunpowder Charge
            133929, -- Prototype Pump-Action Bandage Gun
            133930, -- Prototype Pump-Action Bandage Gun
            133931, -- Prototype Pump-Action Bandage Gun
            133932, -- Prototype Pump-Action Bandage Gun
            132509, -- Deployable Bullet Dispenser
            136851, -- Commander Domitille's Helm
            141025, -- Inert Spellbomb
            141644, -- Assorted Glyphs
            141997, -- Honor
            141998, -- Honor
            144291, -- Tadpole Gift
            145315, -- Challenge Card: A. Milton
            147334, -- Sentinax Token
            151773, -- Festival Suit
            150947, -- Alliance Peace Treaty
            150948, -- Tyrande's Moonstone
            150949, -- Brimstone Beacon
            150950, -- Old Stratholme Key
            132514, -- Auto-Hammer
            132515, -- Failure Detection Pylon
            142456, -- Oakthrush Staff
            137401, -- Illusion Bomb
            141411, -- Translocation Anomaly Neutralization Crystal
            140534, -- Soulcutter Mageblade
            132518, -- Blingtron's Circuit Design Tutorial
            132524, -- Reaves Module: Wormhole Generator Mode
            132525, -- Reaves Module: Repair Mode
            132526, -- Reaves Module: Failure Detection Mode
            132528, -- Reaves Module: Fireworks Display Mode
            132529, -- Reaves Module: Snack Distribution Mode
            132530, -- Reaves Module: Bling Mode
            132531, -- Reaves Module: Piloted Combat Mode
            130102, -- Mother's Skinning Knife
            132508, -- Kvaldir Bear Trap
            156630, -- Relinquished Armor Set
            151797, -- Death Screamers
            151820, -- Primal Ascendant's Stormcallers
            138489  -- Kargath's Sacrificed Hands
        },
        [addon.CONS.C_ELIXIRS_ID] = {
            147868, -- Elixir of Greatest Demonslaying
            151608, -- Lightblood Elixir
            151609  -- Tears of the Naaru
        },
        [addon.CONS.C_FLASKS_ID] = {
            127847, -- Flask of the Whispered Pact
            127848, -- Flask of the Seventh Demon
            127849, -- Flask of the Countless Armies
            127850, -- Flask of Ten Thousand Scars
            127858  -- Spirit Flask
        },
        [addon.CONS.C_FOOD_DRINKS_ID] = {
            128764, -- Moist Azsunian Feta
            128833, -- Kaldorei Ginger Wine
            128834, -- Bradensbrook Gorse Wine
            128835, -- Highmountain Fry Bread
            128838, -- Foxberries
            128840, -- Honey-Glazed Ham
            128841, -- Highmountain Tiswin
            128842, -- Tideskorn Mead Ale
            128844, -- Dried Mango
            128846, -- Tiramisu
            128847, -- Lovingly Crafted Carrot Cake
            128849, -- Scallion Kimchi
            128850, -- Chilled Conjured Water
            133565, -- Leybeque Ribs
            133566, -- Suramar Surf and Turf
            133567, -- Barracuda Mrglgagh
            133568, -- Koi-Scented Stormray
            133569, -- Drogbar-Style Salmon
            133570, -- The Hungry Magister
            133571, -- Azshari Salad
            133572, -- Nightborne Delicacy Platter
            133573, -- Seed-Battered Fish Plate
            133574, -- Fishbrul Special
            133575, -- Dried Mackerel Strips
            133576, -- Bear Tartare
            133577, -- Fighter Chow
            133578, -- Hearty Feast
            133579, -- Lavish Suramar Feast
            133681, -- Crispy Bacon
            136544, -- Gjetost Slice
            136545, -- Skolag Worm Dumpling
            136546, -- Fresh Crawler Salad
            136547, -- Deep Sea Queenfish Cakes
            136548, -- Dried Barracuda Chips
            136549, -- Airy Biscuits
            136550, -- Deepearth Root Straws
            136551, -- Elderhorn Jerky
            136552, -- Sablehorn Steak Tartare
            136553, -- Azsunian Raisins
            136554, -- Lingonberry Fruit Leather
            136555, -- Standard Issue Rations
            136557, -- Passionfruit Tart
            136558, -- Carefully Wrapped Cupcake
            136559, -- Imp Chip Cookie
            136560, -- Condensed Mana Sparks
            136806, -- Glass of Arcwine
            138290, -- Grilled Mini Rays
            138291, -- Tart Green Apple
            138292, -- Ley-Enriched Water
            138294, -- Sea Breeze
            138295, -- Farondis Royal Red
            138978, -- High Fiber Gouda
            138982, -- Pail of Warm Milk
            138983, -- Kurd's Soft Serve
            138986, -- Kurdos Yogurt
            139345, -- Rat Hands
            139347, -- Underjelly
            140184, -- Good Batch of Fruit
            140187, -- First Year Blue
            140188, -- Second Year Blue
            140189, -- Third Year Blue
            140204, -- 'Bottled' Ley-Enriched Water
            140205, -- 'Fresh' Moist Azsunian Feta
            140206, -- Grilled 'Wild' Mini Rays
            140207, -- 'Free-Range' Honey-Glazed Ham
            140265, -- Legendermainy Light Roast
            140266, -- Kafa Kicker
            140269, -- Iced Highmountain Refresher
            140271, -- Valarjar Java
            140272, -- Suramar Spiced Tea
            140273, -- Honey Croissant
            140275, -- Val'sharah Berry Pie
            140286, -- Nightbites
            140296, -- Gummy Wyrm
            140297, -- Shal'dorice Cream
            140298, -- Mananelle's Sparkling Cider
            140299, -- Magistrix Mix
            140300, -- Fresh Arcfruit
            140301, -- Mana Biscuit
            140302, -- Arcway Bisque
            140627, -- Rockbites
            140629, -- Bottled Maelstrom
            140631, -- Nightpear
            140679, -- NeverMelt Ice Cream
            141206, -- Slice of Night Delight
            141207, -- J'llah Suspension
            141208, -- Midnight Morel
            141209, -- Distilled Nightwine
            141210, -- Warm Nightpear Cider
            141211, -- Arcfruit Sangree
            141212, -- Mana-Poached Egg
            141213, -- Candied Sandpiper Eggs
            141214, -- Mana-Eel Eggs
            141215, -- Arcberry Juice
            142334, -- Spiced Falcosaur Omelet
            143633, -- Masterful Mana Buns
            143634, -- Critical Catfish
            143635, -- Hasty Hummus
            143681, -- Slightly Burnt Food
            151775, -- Nutrient Condensate
            152717, -- Azuremyst Water Flask
            152718, -- Thrice-Baked Ammen Loaf
            152998, -- Carefully Hidden Muffin
            153192, -- Sunglow
            128761, -- Azsunian Olives
            128763, -- Pungent Vrykul Gamalost
            128836, -- Barley Bread
            128837, -- Dried Bilberries
            128839, -- Smoked Elderhorn
            128843, -- Azsunian Grapes
            128845, -- Sweet Rice Cake
            128848, -- Roasted Maize
            128853, -- Highmountain Spring Water
            133557, -- Salt & Pepper Shank
            133561, -- Deep-Fried Mossgill
            133562, -- Pickled Stormray
            133563, -- Faronaar Fizz
            133564, -- Spiced Rib Roast
            136556, -- Legion Spoiling Ration
            136561, -- Inferno Punch
            136562, -- Deep Sea Spirit
            136563, -- Pocket Warmed Arkhi
            136564, -- Runewood Akvavit
            136565, -- Fermented Melon Juice
            136566, -- Mariner's Grog
            136567, -- Azuregale Carricante
            136568, -- Black Rook Stout
            138285, -- Blue-Tail Bites
            138972, -- Bilberry Preserves
            138973, -- Pungent and Moldy Gamalost
            138974, -- Charcoaled Elderhorn
            138975, -- Highmountain Runoff
            138976, -- Stale Thundertotem Rice Cake
            138977, -- Thundertotem Rice Cake
            138979, -- Spicy Sharp Cheddar
            138980, -- Butterhoof Singles
            138981, -- Skinny Milk
            138987, -- Butterhoof Can't Believe It's Butter
            139344, -- Mana Banana
            139346, -- Thuni's Patented Drinking Fluid
            140201, -- 'Organic' Azsunian Grapes
            140202, -- Smoked 'Grass Fed' Elderhorn
            140203, -- 'Natural' Highmountain Spring Water
            140276, -- Dalaran Rice Pudding
            140626, -- Frosted Mini-Brans
            140628, -- Lavacolada
            140668, -- Meaty Racks of Musken Ribs
            141527, -- Slightly Rusted Canteen
            151774, -- Distilled Voidblend
            113099, -- Shadowcap Mushrooms
            124036, -- Juicy Apple
            129179, -- Fighter Chow
            130259, -- Ancient Bandana
            132752, -- Illidari Rations
            132753, -- Legion Rations
            133586, -- Illidari Waterskin
            133893, -- Darkpit Mushroom Burger
            133979, -- Grilled Snail
            133980, -- Murky Cavewater
            133981, -- Raw Cavescale
            135557, -- Inferno Curry Crab Legs
            138874, -- Crackling Shards
            140355, -- Laden Apple
            133988, -- "Sessionable" Drog
            133989, -- Homebrew Drog
            140340, -- Bottled - Carbonated Water
            140343, -- Exotic Squirmy Delight
            140341, -- Wild Poached Emperor Salmon
            152558, -- Cherry Pie Slice
            140342, -- Lean - Mok'Nathal Shortribs
            140344, -- Preserved Pickled Egg
            140339, -- Hormone Free - Alterac Swiss
            142322, -- Knockout
            142323, -- Puncher's Punch
            142324, -- Brawler's Lager
            140753, -- Half Eaten Candy Bar
            140754, -- Soggy Waffle Cone
            145272, -- Sparkling Snowplum Brandy
            147669, -- Half an Animal Biscuit
            140338, -- Mana-Wrapped Goretusk Liver Pie
            133983, -- Mammoth Milk
            137613, -- Hearty Steak
            137618, -- Mountain Berries
            138731, -- Bushel of Apples
            138966, -- Mountain Berries
            140337, -- Imported Tough Jerky
            144409, -- Entangling Rootbeer
            144410, -- Barrel-Aged Stormstout
            144411, -- Sargeras Sangria
            144412, -- Mechs on the Beach
            144413, -- Wiley's Wicked Ale
            144414, -- Liquid Qiraj
            144415, -- Coilfang Reserve
            144416, -- Rocket Fuel
            144417, -- Blackout Kick
            144418, -- Stormwind Stout
            144419, -- Grunt's Grog
            144420, -- Earl Greymane, Hot
            144421, -- Silvermoon Squeeze
            144422, -- Nomi's Fire Whiskey
            144423, -- Crash Landing
            144424, -- Blood and Thunder
            144425, -- Magni's Beard
            144426, -- Wild Murky
            144427, -- Blood Red Ale
            144428, -- Mulgore Mule
            144429, -- Kungaloosh
            147774, -- Hunk of Mystery Meat
            147776, -- "Killer" Brew
            147777, -- P.B.R.
            151121, -- Booty Bay Bilgewater
            151122, -- Stolen Coconut Rum
            151123, -- Smuggled Dalaran Red
            151124, -- South Seas Moonshine
            152564, -- Feast of the Fishes
            139398, -- Pant Loaf
            140352, -- Dreamberries
            128875, -- Rotten Flank
            146845, -- Arne's Green Winter Clothes BoA
            146846, -- Arne's Green Winter Clothes BoA -> BoE
            151133, -- Nomi Snacks
            128851, -- Roasted Juicycrunch Carrots
            138478, -- Feast of Ribs
            138479  -- Potato Stew Feast
        },
        [addon.CONS.C_ITEM_ENHANCEMENTS_ID] = {
            128537, -- Enchant Ring - Word of Critical Strike
            128538, -- Enchant Ring - Word of Haste
            128539, -- Enchant Ring - Word of Mastery
            128540, -- Enchant Ring - Word of Versatility
            128545, -- Enchant Cloak - Word of Strength
            128546, -- Enchant Cloak - Word of Agility
            128547, -- Enchant Cloak - Word of Intellect
            144304, -- Enchant Neck - Mark of the Master
            144305, -- Enchant Neck - Mark of the Versatile
            144306, -- Enchant Neck - Mark of the Quick
            144307, -- Enchant Neck - Mark of the Deadly
            128541, -- Enchant Ring - Binding of Critical Strike
            128542, -- Enchant Ring - Binding of Haste
            128543, -- Enchant Ring - Binding of Mastery
            128544, -- Enchant Ring - Binding of Versatility
            128548, -- Enchant Cloak - Binding of Strength
            128549, -- Enchant Cloak - Binding of Agility
            128550, -- Enchant Cloak - Binding of Intellect
            128551, -- Enchant Neck - Mark of the Claw
            128552, -- Enchant Neck - Mark of the Distant Army
            128553, -- Enchant Neck - Mark of the Hidden Satyr
            128554, -- Enchant Shoulder - Boon of the Scavenger
            128558, -- Enchant Gloves - Legion Herbalism
            128559, -- Enchant Gloves - Legion Mining
            128560, -- Enchant Gloves - Legion Skinning
            128561, -- Enchant Gloves - Legion Surveying
            140213, -- Boon of the Gemfinder
            140214, -- Boon of the Harvester
            140215, -- Boon of the Butcher
            140217, -- Boon of the Salvager
            140218, -- Boon of the Manaseeker
            140219, -- Boon of the Bloodhunter
            141908, -- Enchant Neck - Mark of the Heavy Hide
            141909, -- Enchant Neck - Mark of the Trained Soldier
            141910, -- Enchant Neck - Mark of the Ancient Priestess
            144328, -- Boon of the Builder
            144346, -- Boon of the Zookeeper
            153197, -- Boon of the Steadfast
            153203, -- Ancient Fishing Line
            153247  -- Boon of the Lightbearer
        },
        [addon.CONS.C_POTIONS_ID] = {
            127834, -- Ancient Healing Potion
            127835, -- Ancient Mana Potion
            127836, -- Ancient Rejuvenation Potion
            127843, -- Potion of Deadly Grace
            127844, -- Potion of the Old War
            127845, -- Unbending Potion
            127846, -- Leytorrent Potion
            136569, -- Aged Health Potion
            142117, -- Potion of Prolonged Power
            142325, -- Brawler's Ancient Healing Potion
            142326, -- Brawler's Potion of Prolonged Power
            144396, -- Valorous Healing Potion
            144397, -- Valorous Potion of Armor
            144398, -- Valorous Rage Potion
            152615, -- Astral Healing Potion
            152619, -- Astral Mana Potion
            129196, -- Legion Healthstone
            130258, -- Pocket Friend
            128814, -- Potion of Cowardly Flight
            131729, -- Zanzil's Slow Poison
            144228, -- Dino Mojo
            140347, -- Spirit Berries
            140351, -- Sunfruit
            143542, -- Crown Co. "Kure-Everything" Tonic
            147445, -- Ancient Draught of Regeneration
            147707, -- Repurposed Fel Focuser
            129192, -- Inquisitor's Menacing Eye
            138486, -- "Third Wind" Potion
            138488, -- Saltwater Potion
            138727, -- Potion of Defiance
            138728, -- Potion of Trivial Invisibility
            138729, -- Potion of Heightened Senses
            143660  -- Mrgrglhjorn
        },
        [addon.CONS.C_OTHER_ID] = {
            127837, -- Draught of Raw Magic
            127838, -- Sylvan Elixir
            127839, -- Avalanche Elixir
            127840, -- Skaggldrynk
            127841, -- Skystep Potion
            127851, -- Spirit Cauldron
            128979, -- Unwritten Legend
            128980, -- Scroll of Forgotten Knowledge
            129045, -- Whitewater Tsunami
            137178, -- Enchanted Firecrackers
            137198, -- Brewers Kit
            137248, -- Hiro Brew
            139884, -- Ancient Mana Fragments
            140234, -- Selentia's Mana-Infused Brooch
            143617, -- Testament of the Valorous
            143753, -- Damp Pet Supplies
            146317, -- Mr. Smite's Supplies
            133701, -- Skrog Toenail
            133702, -- Aromatic Murloc Slime
            133703, -- Pearlescent Conch
            133704, -- Rusty Queenfish Brooch
            133705, -- Rotten Fishbone
            133706, -- Mossgill Bait
            133707, -- Nightmare Nightcrawler
            133708, -- Drowned Thistleleaf
            133709, -- Funky Sea Snail
            133710, -- Salmon Lure
            133711, -- Swollen Murloc Egg
            133712, -- Frost Worm
            133713, -- Moosehorn Hook
            133714, -- Silverscale Minnow
            133715, -- Ancient Vrykul Ring
            133716, -- Soggy Drakescale
            133717, -- Enchanted Lure
            133719, -- Sleeping Murloc
            133720, -- Demonic Detritus
            133721, -- Message in a Beer Bottle
            133722, -- Axefish Lure
            133723, -- Stunned, Angry Shark
            133724, -- Decayed Whale Blubber
            133795, -- Ravenous Fly
            138956, -- Hypermagnetic Lure
            138957, -- Auriphagic Sardine
            138958, -- Glob of Really Sticky Glue
            138959, -- Micro-Vortex Generator
            138960, -- Wish Crystal
            138961, -- Alchemical Bonding Agent
            138962, -- Starfish on a String
            138963, -- Tiny Little Grabbing Apparatus
            139175, -- Arcane Lure
            139500, -- Hippogryph Feather
            141333, -- Codex of the Tranquil Mind
            141641, -- Codex of the Clear Mind
            133888, -- Shortstalk Mushroom
            133889, -- Giantcap Mushroom
            133890, -- Stoneshroom
            133891, -- Wormstalk Mushroom
            133892, -- Floaty Fungus
            133985, -- Heavy Drog
            133987, -- Blue Drog
            133990, -- Drinking Buddy
            133992, -- DrogLite
            133993, -- Jug of Drog
            128762, -- Memorial Bouquet
            146459, -- Ensemble: Elite Cataclysmic Gladiator's Chain Armor
            146461, -- Ensemble: Elite Cataclysmic Gladiator's Dragonhide Armor
            146463, -- Ensemble: Elite Cataclysmic Gladiator's Dreadplate Armor
            146465, -- Ensemble: Elite Cataclysmic Gladiator's Felweave Armor
            146467, -- Ensemble: Elite Cataclysmic Gladiator's Leather Armor
            146469, -- Ensemble: Elite Cataclysmic Gladiator's Plate Armor
            146471, -- Ensemble: Elite Cataclysmic Gladiator's Ringmail Armor
            146473, -- Ensemble: Elite Cataclysmic Gladiator's Satin Armor
            146475, -- Ensemble: Elite Cataclysmic Gladiator's Scaled Armor
            146477, -- Ensemble: Elite Cataclysmic Gladiator's Silk Armor
            146479, -- Ensemble: Elite Ruthless Gladiator's Chain Armor
            146481, -- Ensemble: Elite Ruthless Gladiator's Dragonhide Armor
            146483, -- Ensemble: Elite Ruthless Gladiator's Dreadplate Armor
            146485, -- Ensemble: Elite Ruthless Gladiator's Felweave Armor
            146487, -- Ensemble: Elite Ruthless Gladiator's Leather Armor
            146489, -- Ensemble: Elite Ruthless Gladiator's Plate Armor
            146491, -- Ensemble: Elite Ruthless Gladiator's Ringmail Armor
            146493, -- Ensemble: Elite Ruthless Gladiator's Satin Armor
            146495, -- Ensemble: Elite Ruthless Gladiator's Scaled Armor
            146497, -- Ensemble: Elite Ruthless Gladiator's Silk Armor
            146610, -- Ensemble: Gladiator's Dreadplate Armor
            146620, -- Ensemble: Merciless Gladiator's Dreadplate Armor
            146630, -- Ensemble: Vengeful Gladiator's Dreadplate Armor
            147544, -- Candy Bar
            137222, -- Crimson Vial
            137648, -- Nimble Brew
            122610, -- Storm Drake Scale
            123865, -- Relic of Ursol
            123868, -- Relic of Shakama
            123869, -- Relic of Elune
            123956, -- Leystone Hoofplates
            124037, -- Storm Drake Scale
            124640, -- Inky Black Potion
            126939, -- Improvised Flight System
            127003, -- Liadrin's Scouting Report
            127029, -- Scoutmaster's Orders
            127040, -- New Recruit
            127122, -- Legion Presence
            127690, -- Demon Hunt: Felwrought Destructor
            127691, -- Shrine: Glory of the Lightbringer
            127692, -- Invasion: Kirin Tor Shore
            127916, -- Altruis' Findings
            127992, -- New Recruit
            128207, -- Reconnaissance: Legion Gateway
            128324, -- Ashilvara, Verse 1
            128707, -- Azsuna Scouting Report
            128747, -- Val'Sharah Scouting Report
            128748, -- Highmountain Scouting Report
            128749, -- Stormheim Scouting Report
            128966, -- Demon Hunt: Wingterror Ikzil
            128967, -- Recon: Titan Relic
            128968, -- Invasion: The Priestess' Moonwell
            128969, -- Demon Hunt: Lord Malus
            128970, -- Shrine: Glory of the Lightbringer
            128971, -- Invasion: The Stormtide
            128972, -- Demon Hunt: Emissary Azathar
            128973, -- Shrine: Glory of the Lightbringer
            128975, -- Invasion: Wheathoof Village
            129036, -- Ancient Mana Fragment
            129039, -- Ancient Mana
            129097, -- Ancient Mana Gem
            129099, -- Gem Chip
            130184, -- Leylight Binder
            130200, -- Gem Chip
            130201, -- Gem Chip
            130202, -- Gem Chip
            130203, -- Gem Chip
            130204, -- Gem Chip
            130260, -- Thaedris's Elixir
            131746, -- Stonehide Leather Barding
            132262, -- Design: Deadly Deep Amber
            132268, -- Design: Quick Azsunite
            132269, -- Design: Masterful Queen's Opal
            132274, -- New Recruit
            132331, -- Design: Skystone Loop
            132332, -- Design: Deep Amber Loop
            132333, -- Design: Azsunite Loop
            133329, -- New Recruit
            133552, -- New Recruit
            133937, -- New Recruit
            133984, -- Nightborne Disguise
            134033, -- Lady S'theno's Reconnaissance
            134063, -- New Recruit
            136373, -- Can O' Worms Bobber
            136374, -- Toy Cat Head Bobber
            136375, -- Squeaky Duck Bobber
            136376, -- Murloc Bobber
            136377, -- Oversized Bobber
            136394, -- Squad of Ox Initiates
            136404, -- Squad of Tiger Initiates
            136405, -- Student of Chi-Ji - Crane Initiate
            136606, -- Leystone Buoy
            136708, -- Demonsteel Stirrups
            136810, -- New Recruit
            136843, -- Black Harvest Acolytes
            136844, -- Pack of Imps
            136845, -- Felguard Demons
            136936, -- Water Elementals
            136939, -- Apprentice
            136940, -- Troop C
            136941, -- Ghouls
            136942, -- Ebon Knights
            136943, -- Troop C
            136944, -- Ashtongue Warriors
            136945, -- Illidari Adept
            136946, -- Troop C
            136947, -- Dryads
            136948, -- Druid of the Claw
            136949, -- Keepers of the Grove
            136950, -- Archers
            136951, -- Trackers
            136952, -- Troop C
            136953, -- Silver Hand Squires
            136954, -- Silver Hand Knights
            136955, -- Troop C
            136956, -- Paragons
            136957, -- Zealots
            136958, -- Troop C
            136959, -- Thieves
            136960, -- Duelists
            136961, -- Troop C
            136962, -- Earthcallers
            136963, -- Lesser Elementals
            136964, -- Ascendants
            136965, -- Valarjar Aspirant
            136966, -- Shieldmaidens
            136967, -- Troop C
            136984, -- Celestial
            137173, -- Recruiter Tianji
            137210, -- A Missive from Archmage Vargoth
            137226, -- Squad of Ox Adepts
            137247, -- Squad of Tiger Adepts
            137251, -- Number Nine Jia
            137296, -- Banner of Power
            137297, -- Banner of Power
            137374, -- Journeyman Goldmine
            137376, -- Summoner Morn
            137500, -- Banner of Protection
            137501, -- Banner of Rejuvenation
            137556, -- Clothes Chest: Dalaran Citizens
            137557, -- Clothes Chest: Karazhan Opera House
            137558, -- Clothes Chest: Molten Core
            137571, -- Archmage Vargoth's Journal
            138026, -- Empowered Charging Device
            138027, -- Portal Key: Sewer Guard Station
            138028, -- Portal Key: Black Market
            138029, -- Portal Key: Inn Entrance
            138030, -- Portal Key: Alchemists' Lair
            138031, -- Portal Key: Abandoned Shack
            138032, -- Portal Key: Rear Entrance
            138120, -- Sister Lilith
            138121, -- Inscribed Leaf of Wisdom
            138137, -- Captain Hjalmar Stahlstrom
            138138, -- Einar the Runecaster
            138231, -- Battlelord Gaardoun
            138232, -- Loramus Thalipedes
            138252, -- Chronicler Elrianne
            138271, -- Vial of Arcane Water
            138272, -- Khadgar's Vial
            138388, -- Pack of Wild Imps
            138390, -- Fel Wrathguard Demons
            138400, -- Petey
            138414, -- Emergency Pirate Outfit
            138760, -- Commander Ansela
            138761, -- Sir Alamande Graythorn
            138817, -- Demonic Runestone
            138866, -- Well-Wrapped Package
            138867, -- Shimmer Stout
            138868, -- Mannoroth's Blood Red Ale
            138869, -- Gordok Bock
            138870, -- Spirit Spirits
            138871, -- Storming Saison
            138872, -- Black Harvest Invokers
            139001, -- Recruiter [RECRUITER A]
            139004, -- Hallowed Wand - Banshee
            139038, -- Greater Elementals
            139039, -- Geomancers
            139137, -- Hag's Belongings
            139138, -- Marksmen
            139139, -- Rangers
            139140, -- Recruiter Lenara
            139141, -- Dark Summoner Marogh
            139142, -- Grand Anchorite Gesslar
            139143, -- Archivist Zubashi
            139144, -- Archon Torias
            139145, -- Arcane Golem
            139146, -- Conjurer
            139147, -- Treant
            139148, -- Silver Hand Phalanx
            139149, -- Silver Hand Champions
            139150, -- Stormforged Valarjar
            139151, -- Valkyra Shieldmaiden
            139178, -- Coded Message
            139179, -- Naga Myrmidon
            139180, -- Illidari Fury
            139285, -- Celebration Package
            139286, -- Geists
            139287, -- Ebon Champions
            139306, -- Defias Bandits
            139307, -- Pirates
            139308, -- Champion Armaments
            139350, -- Hasty Pack of Imps
            139361, -- Hasty Lesser Elementals
            139363, -- Hasty Squad of Ox Initiates
            139365, -- Hasty Valarjar Aspirant
            139366, -- Hasty Thieves
            139367, -- Hasty Silver Hand Squires
            139369, -- Hasty Water Elementals
            139370, -- Hasty Archers
            139371, -- Hasty Treant
            139372, -- Hasty Ashtongue Warriors
            139373, -- Hasty Ghouls
            139377, -- Acolytes
            139379, -- Dark Zealots
            139411, -- Underbelly Banquet
            139412, -- Young Mutant Warturtles
            139416, -- Bloated Sewersucker
            139421, -- Ratstallion Harness
            139422, -- Fel Chum
            139423, -- Croc Mojo
            139424, -- Screecher Whistle
            139425, -- Imp-Binding Contract
            139426, -- Widowsister Contract
            139427, -- Wild Mana Wand
            139431, -- Crate of Ratstallion Harnesses
            139432, -- The Edict of Fire
            139433, -- The Edict of Stone
            139434, -- The Edict of the Storm
            139453, -- Greater Demonic Runestone
            139489, -- Forgotten Techniques of the Broken Isles
            139490, -- Forgotten Techniques of the Broken Isles
            139491, -- Forgotten Techniques of the Broken Isles
            139492, -- Forgotten Formulas of the Broken Isles
            139493, -- Forgotten Plans of the Broken Isles
            139494, -- Forgotten Formulas of the Broken Isles
            139495, -- Forgotten Schematics of the Broken Isles
            139496, -- Forgotten Techniques of the Broken Isles
            139497, -- Forgotten Designs of the Broken Isles
            139498, -- Forgotten Patterns of the Broken Isles
            139499, -- Forgotten Patterns of the Broken Isles
            139503, -- Bloodtotem Saddle Blanket
            140134, -- Lonika Stillblade
            140136, -- Archmage Omniara
            140308, -- Rescued Valarjar
            140584, -- Player Experience
            140588, -- Survivalist Bahn
            140589, -- Archivist Melinda
            140764, -- Grimoire of the First Necrolyte
            141154, -- Improved Quality
            141318, -- Angry Post
            141319, -- Happy Post
            141320, -- Thoughtful Post
            141334, -- Mighty Mess Remover
            141920, -- Access to the Ruins of Falanaar
            142026, -- Challenge Card: Blackmange
            142028, -- Challenge Card: Thwack U
            142029, -- Challenge Card: Ogrewatch
            142030, -- Challenge Card: Burnstachio
            142031, -- Challenge Card: Ray D. Tear
            142032, -- Challenge Card: Johnny Awesome
            142033, -- Challenge Card: Carl
            142034, -- Challenge Card: Beat Box
            142035, -- Challenge Card: Warhammer Council
            142036, -- Challenge Card: Master Paku
            142037, -- Challenge Card: Topps
            142038, -- Challenge Card: Serpent of Old
            142039, -- Challenge Card: Shadowmaster Aameen
            142040, -- Challenge Card: Ash'katzuum
            142041, -- Challenge Card: Bill the Janitor
            142042, -- Challenge Card: Ooliss
            142043, -- Challenge Card: Doomflipper
            142044, -- Challenge Card: Strange Thing
            142045, -- Challenge Card: Stitches
            142049, -- Legacy of the Mountain King
            142051, -- Torment of the Worgen
            142052, -- Redemption of the Fallen
            142073, -- Wrath of the Titans
            142074, -- Player Experience
            142260, -- Arcane Nullifier
            142315, -- Helya's Horn
            142350, -- Challenger's Purse
            143639, -- Time Ripple Orb
            143640, -- Fire Prevention Orb
            143641, -- Frost Prevention Orb
            143642, -- Arcane Prevention Orb
            143664, -- Rodent of Usual Size
            143772, -- Challenge Card: Oolis
            143786, -- Player Experience
            143794, -- Challenge Card: A Seagull
            143843, -- Horde War Effort Credit
            143899, -- Challenge Card: Oso the Betrayer
            144261, -- Sporeggium
            144262, -- Fungal Lifestalk
            144263, -- Pungent Truffle
            144264, -- Pungent Truffle
            144265, -- Rimecap
            144276, -- Sack of Healing Spores
            144296, -- Kroksy's Key
            144357, -- Corin's Key
            144373, -- Claw-Marked Brawler's Purse
            144374, -- Groovy Brawler's Purse
            144375, -- Feathered Brawler's Purse
            144376, -- Agile Brawler's Purse
            144377, -- Beginning Brawler's Purse
            144378, -- Gorestained Brawler's Purse
            144379, -- Murderous Brawler's Purse
            144439, -- Challenge Card: Klunk
            147434, -- Player Experience
            147455, -- Water Stone
            147877, -- Celebration Package
            147882, -- Celebration Wand - Trogg
            147883, -- Celebration Wand - Quilboar
            151129, -- Emergency Wardrobe Kit
            151221, -- Gooey Brawler's Purse
            151222, -- Leather Brawler's Purse
            151223, -- Booming Brawler's Purse
            151224, -- Bitten Brawler's Purse
            151225, -- Wet Brawler's Purse
            151229, -- Brawler's Music Box
            151230, -- Croc-Skin Brawler's Purse
            151231, -- Brawler's Egg
            151232, -- Brawler's Package
            151233, -- Blingin' Brawler's Bag
            151235, -- Filthy Brawler's Purse
            151238, -- Dark Brawler's Purse
            151239, -- Felslate Anchor
            151256, -- Purple Dance Stick
            151257, -- Green Dance Stick
            151264, -- Clunky Brawler's Purse
            151365, -- Bringing Life to Death
            151463, -- Death's Reign
            151472, -- Blood Infusion
            151482, -- Time-Lost Wallet
            151483, -- Timewarped Badge
            151484, -- Reputation
            151485, -- Reputation
            151486, -- Reputation
            151487, -- Fangs of the Bronze
            151488, -- Hide of the Bronze
            151489, -- Wings of the Bronze
            151490, -- Stolen Time
            151491, -- Favor of the Bronze
            151544, -- Stolen Time
            151545, -- Stolen Time
            151599, -- Blighthead Slack-Jaw Mask
            151600, -- Blighthead Mohawk Mask
            151601, -- Blighthead Romero Mask
            151602, -- Blighthead Electric Beehive Mask
            151603, -- Blighthead Grim Smile Mask
            151604, -- Blighthead Bitter Wounds Mask
            151605, -- Devlynn Styx Mask
            151651, -- Gravitational Reduction Slippers
            153015, -- Box of Void-Tainted Entities
            153069, -- All-Seer's Draught
            153190, -- Fel-Spotted Egg
            153191, -- Cracked Fel-Spotted Egg
            153251, -- Fresh Talbuk Meat
            133549, -- Muck-Covered Shoes
            127999, -- Shard of Potentiation
            128022, -- Treasured Coin
            138726, -- Shard of Potentiation
            138732, -- History of the Blade
            138783, -- Glittering Memento
            138814, -- Adventurer's Renown
            138839, -- Valiant's Glory
            138864, -- Skirmisher's Advantage
            138865, -- Gladiator's Triumph
            138885, -- Treasure of the Ages
            139507, -- Cracked Vrykul Insignia
            139508, -- Dried Worldtree Seeds
            139509, -- Worldtree Bloom
            139511, -- Hallowed Runestone
            139608, -- Brittle Spelltome
            139609, -- Depleted Cadet's Wand
            139611, -- Primitive Roggtotem
            139612, -- Highmountain Mystic's Totem
            139614, -- Azsharan Manapearl
            139615, -- Untapped Mana Gem
            140235, -- Small Jar of Arcwine
            140243, -- Azurefall Essence
            140310, -- Crude Statuette
            140322, -- Trainer's Insight
            140390, -- Red Or'ligai Egg
            140439, -- Sunblossom Pollen
            140446, -- Ancient Branch
            140788, -- Bug Zapper XT
            140949, -- Onyx Or'ligai Egg
            141011, -- Recipe: Surf
            141012, -- Recipe: Turf
            141702, -- Spoiled Manawine Dregs
            141703, -- Witch-Harpy Talon
            141704, -- Forgotten Offering
            141705, -- Disorganized Ravings
            141706, -- Carved Oaken Windchimes
            141857, -- Soldier's Exertion
            141858, -- Soldier's Worth
            141883, -- Azsharan Keepsake
            141886, -- Crackling Dragonscale
            141887, -- Lucky Brulstone
            141888, -- Discarded Aristocrat's Censer
            141890, -- Petrified Acorn
            141921, -- Dessicated Blue Dragonscale
            141922, -- Brulstone Fishing Sinker
            141923, -- Petrified Axe Haft
            141924, -- Broken Control Mechanism
            141925, -- Pruned Nightmare Shoot
            141926, -- Druidic Molting
            141927, -- Burrowing Worm Mandible
            141928, -- Reaver's Harpoon Head
            141929, -- Hippogryph Plumage
            141930, -- Smolderhide Spirit Beads
            141931, -- Tattered Farondis Heraldry
            141956, -- Rotten Spellbook
            142363, -- Mark of Prey
            143677, -- Gladiator's Exultation
            143735, -- Ancient Mana Shards
            143748, -- Leyscale Koi
            144266, -- Shattered Wrathguard Horn
            144267, -- Bloodied Horde Axe Head
            144268, -- Alliance Infantry Blade
            144269, -- Vacant Soul Shard
            144270, -- Fel-Scarred Tomb Stone
            144271, -- Searing Hellion Claw
            146124, -- Exhausted Portal Key
            146125, -- Legion Mark of Command
            146128, -- Nefarious Trophy Jar
            146129, -- Demonic Whetstone
            146903, -- Sentinax Beacon of Domination
            146905, -- Sentinax Beacon of the Firestorm
            146906, -- Sentinax Beacon of Carnage
            146907, -- Sentinax Beacon of Warbeasts
            146908, -- Sentinax Beacon of Engineering
            146909, -- Sentinax Beacon of Torment
            147198, -- Gladiator's Exultation
            147199, -- Gladiator's Triumph
            147482, -- Contorted Eredar Bangle
            147483, -- Defiled Moonborn Statuette
            147484, -- Sightless Tormentor's Tome
            147485, -- Wicked Wyrmtongue Fetish
            147486, -- Legionfall Vanguard Battle Standard
            147865, -- Crescent Moonstone
            151240, -- Corroded Hologram Crystal
            151241, -- Blistering Infernal Fragment
            151242, -- Pious Anchorite's Breviary
            151243, -- Soulgorged Felglobe
            151244, -- Legion Doom-Horn
            151914, -- Gladiator's Perseverance
            151915, -- Gladiator's Perseverance
            151916, -- Gladiator's Perseverance
            151917, -- Gladiator's Perseverance
            151918, -- Gladiator's Perseverance
            151919, -- Gladiator's Perseverance
            151920, -- Gladiator's Perseverance
            151921, -- Gladiator's Perseverance
            151922, -- Gladiator's Perseverance
            153114, -- Nathrezim Tome of Manipulation
            153127, -- Cube of Discovery
            153159, -- Void-Swallowed Memento
            153160, -- Crude Effigy of Despair
            153161, -- Incomprehensible Scribbling
            153162, -- Unattuned Portal Stones
            153163, -- Gruesome Wind Chime
            153164, -- Otherworldly Trophy
            153165, -- Krokul Remembrance-Crystal
            153188, -- Greater Blessed Bandage
            153189, -- Shattered Lightsword
            153246, -- Strangely-Flawed Gemstone
            155657, -- Alor'idal Crystal Shard
            130250, -- Jeweled Lockpick
            146309, -- Expended Spell Reagents
            146313, -- Highborne Pottery Shards
            146318, -- Imp's Femur
            146319, -- Doom Dust
            146323, -- Cracked Kobold Skull
            146324, -- Ossified Roc Feather
            146328, -- Petrified Wyrmtongue
            146329, -- Jar of Ashes
            129210, -- Fel Crystal Fragments
            132172, -- Crowbar
            136852, -- Songs of Battle
            136856, -- Songs of Peace
            136857, -- Songs of the Legion
            138480, -- Black Harvest Tome
            138487, -- Shinfel's Staff of Torment
            139594, -- Salvage Crate
            140357, -- Fel Lava Rock
            140358, -- Eredar Armor Clasp
            140359, -- Darkened Eyeball
            140361, -- Pulsating Runestone
            140364, -- Frostwyrm Bone Fragment
            140365, -- Dried Stratholme Lily
            140366, -- Scarlet Hymnal
            140367, -- Tattered Sheet Music
            140368, -- Tarnished Engagement Ring
            140369, -- Scrawled Recipe
            140370, -- Amber Shard
            140371, -- Letter from Exarch Maladaar
            140373, -- Ornamented Boot Strap
            140374, -- Jagged Worgen Fang
            140377, -- Broken Medallion of Karabor
            140379, -- Broken Warden Glaive Blade
            140380, -- Swiftflight's Tail Feather
            140382, -- Tiny War Drum
            140383, -- Glowing Cave Mushroom
            140385, -- Legion Pamphlet
            140387, -- Bracer Gemstone
            140389, -- Petrified Flame
            140391, -- Argussian Diamond
            140392, -- Safety Valve
            140393, -- Repentia's Whip
            140459, -- Moon Lily
            140460, -- Thisalee's Fighting Claws
            140461, -- Battered Trophy
            140462, -- Draketaming Spurs
            140463, -- Broken Eredar Blade
            140466, -- Corroded Eternium Rose
            140467, -- Fel-Infused Shell
            140468, -- Eagle Eggshell Fragment
            140469, -- Felslate Arrowhead
            140470, -- Ancient Gilnean Locket
            140471, -- Lord Shalzaru's Relic
            140473, -- Night-forged Halberd
            140474, -- Nar'thalas Pottery Fragment
            140475, -- Morning Glory Vine
            140476, -- Astranaar Globe
            140477, -- Inert Ashes
            140478, -- Painted Bark
            140479, -- Broken Legion Communicator
            140480, -- Drained Construct Core
            140481, -- Shimmering Hourglass
            140482, -- Storm Drake Fang
            140484, -- Well-Used Drinking Horn
            140485, -- Duskpelt Fang
            140486, -- Storm Drake Scale
            140487, -- War-Damaged Vrykul Helmet
            140488, -- Huge Blacksmith's Hammer
            140489, -- Ettin Toe Ring
            140490, -- Wooden Snow Shoes
            140491, -- Stolen Pearl Ring
            140492, -- Gleaming Glacial Pebble
            140494, -- Eredar Tail-Cuff
            140497, -- Bundle of Tiny Spears
            140498, -- Legion Admirer's Note
            140503, -- Blank To-Do List
            140504, -- Kvaldir Anchor Line
            140505, -- Sweaty Bandanna
            140507, -- Unlabeled Potion
            140508, -- Nightborne Artificer's Ring
            140509, -- Demon-Scrawled Drawing
            140510, -- Iron Black Rook Hold Key
            140511, -- Soul Shackle
            140512, -- Oversized Drinking Mug
            140513, -- Dreadlord's Commendation
            140516, -- Elemental Bracers
            140518, -- Bottled Lightning
            140519, -- Whispering Totem
            140520, -- Amethyst Geode
            140521, -- Fire Turtle Shell Fragment
            140522, -- Petrified Spiderweb
            140523, -- Crimson Cavern Mushroom
            140524, -- Sharp Twilight Tooth
            140525, -- Obsidian Mirror
            140528, -- Dalaran Wine Glass
            140529, -- Felstalker's Ring
            140530, -- Opalescent Shell
            140531, -- Ravencrest Family Seal
            140532, -- Inscribed Vrykul Runestone
            140567, -- Songs of the Horde
            140568, -- Songs of the Alliance
            141295, -- Extra Thick Mojo
            142023, -- Adventurer's Footlocker
            142406, -- Drums of the Mountain
            143855, -- Twilight Cultist Robe
            143857, -- Twilight Cultist Mantle
            143858, -- Twilight Cultist Cowl
            142259, -- Spectral Feather
            142447, -- Torn Sack of Pet Supplies
            151638, -- Leprous Sack of Pet Supplies
            124069, -- Felstone
            124070, -- Orb of the Eredar
            124071, -- Shadowstone
            124072, -- Ward of Sargeras
            124073, -- Flare Launcher
            129098, -- Ancient Mana Stone
            129211, -- Steamy Romance Novel Kit
            129758, -- Reinforced Kodo Hide
            134125, -- Mecha-Bond Imprint Matrix
            137010, -- Half-Full Bottle of Arcwine
            137209, -- Armor Enhancement Token
            137467, -- Shimmerfield Crystal
            139592, -- Smoky Boots
            140742, -- Delightfully Wrapped Armaments
            142263, -- Machine Fluid
            142264, -- Drudge Fluid
            142359, -- Lava Oil
            143931, -- Small Boat
            146715, -- Overcharged Portalstone
            147351, -- Fel Armor Enhancement Token
            147573, -- Trial of Style Reward: First Place
            147574, -- Trial of Style Reward: Second Place
            147575, -- Trial of Style Reward: Third Place
            147576, -- Trial of Style Consolation Prize
            151134, -- Trial of Style Token
            151653, -- Broken Isles Recipe Scrap
            152733, -- Unsullied Trinket
            152734, -- Unsullied Cloth Mantle
            152735, -- Unsullied Ring
            152736, -- Unsullied Necklace
            152737, -- Unsullied Leather Trousers
            152738, -- Unsullied Cloth Cap
            152739, -- Unsullied Leather Grips
            152740, -- Unsullied Cloak
            152741, -- Unsullied Mail Chestguard
            152742, -- Unsullied Cloth Cuffs
            152743, -- Unsullied Plate Sabatons
            152744, -- Unsullied Mail Girdle
            152799, -- Unsullied Relic
            153135, -- Unsullied Cloth Robes
            153136, -- Unsullied Leather Treads
            153137, -- Unsullied Mail Spaulders
            153138, -- Unsullied Mail Legguards
            153139, -- Unsullied Leather Headgear
            153140, -- Unsullied Plate Waistplate
            153141, -- Unsullied Cloth Mitts
            153142, -- Unsullied Leather Armbands
            153143, -- Unsullied Plate Breastplate
            153144, -- Unsullied Cloth Slippers
            153145, -- Unsullied Leather Spaulders
            153146, -- Unsullied Plate Greaves
            153147, -- Unsullied Mail Coif
            153148, -- Unsullied Leather Belt
            153149, -- Unsullied Mail Gloves
            153150, -- Unsullied Plate Vambraces
            153151, -- Unsullied Leather Tunic
            153152, -- Unsullied Mail Boots
            153153, -- Unsullied Plate Pauldrons
            153154, -- Unsullied Cloth Leggings
            153155, -- Unsullied Plate Helmet
            153156, -- Unsullied Cloth Sash
            153157, -- Unsullied Plate Gauntlets
            153158, -- Unsullied Mail Bracers
            147212, -- Dauntless Bracers
            147213, -- Dauntless Tunic
            147214, -- Dauntless Treads
            147215, -- Dauntless Gauntlets
            147216, -- Dauntless Hood
            147217, -- Dauntless Leggings
            147218, -- Dauntless Spaulders
            147219, -- Dauntless Girdle
            147220, -- Dauntless Ring
            147221, -- Dauntless Choker
            147222, -- Dauntless Cloak
            147223, -- Dauntless Trinket
            128000, -- Crystal of Ensoulment
            128021, -- Scroll of Enlightenment
            130193, -- Disarmed Treasure Chest Trap
            138781, -- Brief History of the Aeons
            138784, -- Questor's Glory
            138785, -- Adventurer's Resounding Glory
            138786, -- Talisman of Victory
            138812, -- Adventurer's Wisdom
            138813, -- Adventurer's Resounding Renown
            138816, -- Adventurer's Glory
            138880, -- Soldier's Grit
            138881, -- Soldier's Glory
            138886, -- Favor of Valarjar
            139010, -- Petrified Silkweave
            139017, -- Soothing Leystone Shard
            139018, -- Box of Calming Whispers
            139020, -- Valarjar Insignia
            139021, -- Dreamweaver Insignia
            139023, -- Court of Farondis Insignia
            139024, -- Highmountain Tribe Insignia
            139025, -- Wardens Insignia
            139026, -- Nightfallen Insignia
            139510, -- Black Rook Soldier's Insignia
            139512, -- Sigilstone of Tribute
            139610, -- Musty Azsharan Grimoire
            139613, -- Tale-Teller's Staff
            139616, -- Dropper of Nightwell Liquid
            139617, -- Ancient Warden Manacles
            139786, -- Ancient Mana Crystal
            139890, -- Ancient Mana Gem
            140176, -- Accolade of Victory
            140236, -- A Mrglrmrl Mlrglr
            140237, -- Iadreth's Enchanted Birthstone
            140238, -- Scavenged Felstone
            140240, -- Enchanted Moonwell Waters
            140241, -- Enchanted Moonfall Text
            140244, -- Jandvik Jarl's Pendant Stone
            140246, -- Arc of Snow
            140248, -- Master Jeweler's Gem
            140250, -- Ingested Legion Stabilizer
            140251, -- Purified Satyr Totem
            140252, -- Tel'anor Ancestral Tablet
            140254, -- The Seawarden's Beacon
            140255, -- Enchanted Nightborne Coin
            140304, -- Activated Essence
            140306, -- Mark of the Valorous
            140326, -- Enchanted Burial Urn
            140327, -- Kyrtos's Research Notes
            140328, -- Volatile Leyline Crystal
            140329, -- Infinite Stone
            140349, -- Spare Arcane Ward
            140372, -- Ancient Artificer's Manipulator
            140381, -- Jandvik Jarl's Ring, and Finger
            140384, -- Azsharan Court Scepter
            140386, -- Inquisitor's Shadow Orb
            140388, -- Falanaar Gemstone
            140396, -- Friendly Brawler's Wager
            140399, -- Yellow Or'ligai Egg
            140401, -- Blue Or'ligai Egg
            140402, -- Green Or'ligai Egg
            140403, -- Lylandre's Fel Crystal
            140404, -- Intact Guardian Core
            140405, -- Illusion Matrix Crystal
            140406, -- Primed Arcane Charge
            140447, -- Vintage Quietwine
            140448, -- Lens of Qin'dera
            140449, -- Elixir-Soaked Wrappings
            140587, -- Defiled Augment Rune
            140685, -- Enchanted Sunrunner Kidney
            140847, -- Ancient Workshop Focusing Crystal
            141018, -- Sargerei Blood Vessel
            141023, -- Seal of Victory
            141024, -- Seal of Leadership
            141310, -- Falanaar Crescent
            141338, -- Valarjar Insignia
            141339, -- Dreamweaver Insignia
            141340, -- Court of Farondis Insignia
            141341, -- Highmountain Tribe Insignia
            141342, -- Wardens Insignia
            141343, -- Nightfallen Insignia
            141344, -- Tribute of the Broken Isles
            141383, -- Crystallized Moon Drop
            141384, -- Emblem of the Dark Covenant
            141385, -- Tidestone Sliver
            141386, -- Giant Pearl Scepter
            141387, -- Emerald Bloom
            141388, -- Warden's Boon
            141389, -- Stareye Gem
            141390, -- The Corruptor's Totem
            141391, -- Ashildir's Unending Courage
            141392, -- Fragment of the Soulcage
            141393, -- Onyx Arrowhead
            141394, -- Plume of the Great Eagle
            141395, -- Stonedark's Pledge
            141396, -- The River's Blessing
            141397, -- The Spiritwalker's Wisdom
            141398, -- Blessing of the Watchers
            141399, -- Overcharged Stormscale
            141400, -- Underking's Fist
            141401, -- Renewed Lifeblood
            141402, -- Odyn's Watchful Gaze
            141403, -- Tablet of Tyr
            141404, -- Insignia of the Second Command
            141405, -- Senegos' Favor
            141638, -- Falanaar Scepter
            141639, -- Falanaar Orb
            141668, -- The Arcanist's Codex
            141670, -- Arcane Trap Power Core
            141671, -- Moon Guard Focusing Stone
            141672, -- Insignia of the Nightborne Commander
            141674, -- Brand of a Blood Brother
            141675, -- Deepwater Blossom
            141689, -- Jewel of Victory
            141690, -- Symbol of Victory
            141701, -- Selfless Glory
            141707, -- Smuggled Magical Supplies
            141708, -- Curio of Neltharion
            141709, -- Ancient Champion Effigy
            141710, -- Discontinued Suramar City Key
            141711, -- Ancient Druidic Carving
            141852, -- Accolade of Heroism
            141853, -- Accolade of Myth
            141854, -- Accolade of Achievement
            141855, -- History of the Aeons
            141859, -- Soldier's Splendor
            141863, -- Daglop's Precious
            141872, -- Artisan's Handiwork
            141876, -- Soul-Powered Containment Unit
            141877, -- Coura's Ancient Scepter
            141933, -- Citrine Telemancy Index
            141934, -- Partially Enchanted Nightborne Coin
            141935, -- Enchrgled Mlrgmlrg of Enderglment
            141936, -- Petrified Fel-Heart
            141937, -- Eredari Ignition Crystal
            141938, -- Shard of Vorgos
            141939, -- Shard of Kozak
            141940, -- Starsong's Bauble
            141941, -- Crystallized Sablehorn Antler
            141942, -- Managazer's Petrifying Eye
            141943, -- Moon Guard Power Gem
            141944, -- Empowered Half-Shell
            141945, -- Magically-Fortified Vial
            141946, -- Trident of Sashj'tar
            141947, -- Mark of Lunastre
            141948, -- Token of a Master Cultivator
            141949, -- Everburning Arcane Glowlamp
            141987, -- Greater Valarjar Insignia
            141988, -- Greater Dreamweaver Insignia
            141989, -- Greater Court of Farondis Insignia
            141990, -- Greater Highmountain Tribe Insignia
            141991, -- Greater Wardens Insignia
            141992, -- Greater Nightfallen Insignia
            142001, -- Antler of Cenarius
            142002, -- Dragonscale of the Earth Aspect
            142003, -- Talisman of the Ascended
            142004, -- Nar'thalas Research Tome
            142005, -- Vial of Diluted Nightwell Liquid
            142006, -- Ceremonial Warden Glaive
            142007, -- Omnibus: The Schools of Arcane Magic
            142054, -- Enchanted Nightborne Coin
            142449, -- Unearthed Staff Headpiece
            142450, -- Ley to Fel Power Converter
            142451, -- Overcharged Ward Focus
            143488, -- Enchanted Nightborne Coin
            143680, -- Soldier's Esteem
            143713, -- Skirmisher's Mastery
            143714, -- Gladiator's Glory
            143715, -- Gladiator's Revelry
            143733, -- Ancient Mana Shards
            143734, -- Ancient Mana Crystal Cluster
            143844, -- Glory of Combat
            143868, -- Accolade of Achievement
            143869, -- Accolade of Victory
            143870, -- Accolade of Heroism
            143871, -- Accolade of Myth
            144272, -- Seal of Bravery
            144297, -- Talisman of Victory
            146122, -- Warden's Lantern
            146123, -- Enchanted Vault Cell Key
            146126, -- "Revised" Chronicles of Argus
            146127, -- Shard of a Dead World
            146152, -- Ensemble: Vindictive Combatant's Ringmail Armor
            146153, -- Ensemble: Vindictive Combatant's Ringmail Armor
            146154, -- Ensemble: Vindictive Combatant's Chain Armor
            146155, -- Ensemble: Vindictive Combatant's Chain Armor
            146156, -- Ensemble: Vindictive Combatant's Silk Armor
            146157, -- Ensemble: Vindictive Combatant's Silk Armor
            146158, -- Ensemble: Vindictive Combatant's Satin Armor
            146159, -- Ensemble: Vindictive Combatant's Satin Armor
            146160, -- Ensemble: Vindictive Combatant's Felweave Armor
            146161, -- Ensemble: Vindictive Combatant's Felweave Armor
            146162, -- Ensemble: Vindictive Combatant's Plate Armor
            146163, -- Ensemble: Vindictive Combatant's Plate Armor
            146164, -- Ensemble: Vindictive Combatant's Dreadplate Armor
            146165, -- Ensemble: Vindictive Combatant's Dreadplate Armor
            146166, -- Ensemble: Vindictive Combatant's Scaled Armor
            146167, -- Ensemble: Vindictive Combatant's Scaled Armor
            146168, -- Ensemble: Vindictive Combatant's Dragonhide Armor
            146169, -- Ensemble: Vindictive Combatant's Dragonhide Armor
            146170, -- Ensemble: Vindictive Combatant's Ironskin Armor
            146171, -- Ensemble: Vindictive Combatant's Ironskin Armor
            146172, -- Ensemble: Vindictive Combatant's Leather Armor
            146173, -- Ensemble: Vindictive Combatant's Leather Armor
            146264, -- Ensemble: Vindictive Combatant's Felskin Armor
            146265, -- Ensemble: Vindictive Combatant's Felskin Armor
            146330, -- Test Insignia
            146910, -- Sentinax Beacon of Greater Domination
            146911, -- Sentinax Beacon of the Greater Firestorm
            146912, -- Sentinax Beacon of Greater Carnage
            146913, -- Sentinax Beacon of Greater Warbeasts
            146914, -- Sentinax Beacon of Greater Engineering
            146915, -- Sentinax Beacon of Greater Torment
            146922, -- Sentinax Beacon of Fel Growth
            146923, -- Sentinax Beacon of Petrification
            146935, -- Valarjar Insignia
            146936, -- Dreamweaver Insignia
            146937, -- Court of Farondis Insignia
            146938, -- Highmountain Tribe Insignia
            146939, -- Wardens Insignia
            146940, -- Nightfallen Insignia
            146941, -- Valarjar Insignia
            146942, -- Dreamweaver Insignia
            146943, -- Court of Farondis Insignia
            146944, -- Highmountain Tribe Insignia
            146945, -- Wardens Insignia
            146946, -- Nightfallen Insignia
            146948, -- Tribute of the Broken Isles
            146949, -- Legionfall Insignia
            146950, -- Legionfall Insignia
            147200, -- Soldier's Glory
            147201, -- Gladiator's Glory
            147202, -- Gladiator's Revelry
            147355, -- Sentinax Beacon of the Bloodstrike
            147398, -- Lesser Pathfinder's Symbol
            147399, -- Lesser Adventurer's Symbol
            147400, -- Lesser Hero's Symbol
            147401, -- Lesser Champion's Symbol
            147402, -- Pathfinder's Symbol
            147403, -- Adventurer's Symbol
            147404, -- Hero's Symbol
            147405, -- Champion's Symbol
            147406, -- Greater Pathfinder's Symbol
            147407, -- Greater Adventurer's Symbol
            147408, -- Greater Hero's Symbol
            147409, -- Greater Champion's Symbol
            147410, -- Greater Court of Farondis Insignia
            147411, -- Greater Dreamweaver Insignia
            147412, -- Greater Highmountain Tribe Insignia
            147413, -- Greater Nightfallen Insignia
            147414, -- Greater Valarjar Insignia
            147415, -- Greater Wardens Insignia
            147513, -- Contorted Eredar Bangle
            147545, -- Deviate Spores
            147548, -- Champion's Trophy
            147549, -- Hero's Trophy
            147550, -- Adventurer's Trophy
            147551, -- Pathfinder's Trophy
            147579, -- Tome of the Legionfall Magi
            147581, -- Depleted Azsharan Seal
            147673, -- Ensemble: Cruel Combatant's Ringmail Armor
            147674, -- Ensemble: Cruel Combatant's Ringmail Armor
            147675, -- Ensemble: Cruel Combatant's Dragonhide Armor
            147676, -- Ensemble: Cruel Combatant's Dragonhide Armor
            147677, -- Ensemble: Cruel Combatant's Dreadplate Armor
            147678, -- Ensemble: Cruel Combatant's Dreadplate Armor
            147679, -- Ensemble: Cruel Combatant's Felskin Armor
            147680, -- Ensemble: Cruel Combatant's Felskin Armor
            147681, -- Ensemble: Cruel Combatant's Felweave Armor
            147682, -- Ensemble: Cruel Combatant's Felweave Armor
            147683, -- Ensemble: Cruel Combatant's Ironskin Armor
            147684, -- Ensemble: Cruel Combatant's Ironskin Armor
            147685, -- Ensemble: Cruel Combatant's Leather Armor
            147686, -- Ensemble: Cruel Combatant's Leather Armor
            147687, -- Ensemble: Cruel Combatant's Satin Armor
            147688, -- Ensemble: Cruel Combatant's Satin Armor
            147689, -- Ensemble: Cruel Combatant's Chain Armor
            147690, -- Ensemble: Cruel Combatant's Chain Armor
            147691, -- Ensemble: Cruel Combatant's Plate Armor
            147692, -- Ensemble: Cruel Combatant's Plate Armor
            147693, -- Ensemble: Cruel Combatant's Scaled Armor
            147694, -- Ensemble: Cruel Combatant's Scaled Armor
            147695, -- Ensemble: Cruel Combatant's Silk Armor
            147696, -- Ensemble: Cruel Combatant's Silk Armor
            147718, -- Master's Symbol
            147719, -- Lesser Master's Symbol
            147720, -- Greater Master's Symbol
            147721, -- Master's Trophy
            147726, -- Nethercluster
            147727, -- Greater Legionfall Insignia
            147729, -- Netherchunk
            147808, -- Lesser Adept's Spoils
            147809, -- Adept's Spoils
            147810, -- Greater Adept's Spoils
            147811, -- Expert's Bounty
            147812, -- Greater Expert's Bounty
            147814, -- Lesser Expert's Bounty
            147818, -- Expert's Crest
            147819, -- Adept's Medal
            147842, -- Unity of the Orders
            150248, -- Ensemble: Fierce Combatant's Ringmail Armor
            150249, -- Ensemble: Fierce Combatant's Ringmail Armor
            150250, -- Ensemble: Fierce Combatant's Dragonhide Armor
            150251, -- Ensemble: Fierce Combatant's Dragonhide Armor
            150252, -- Ensemble: Fierce Combatant's Dreadplate Armor
            150253, -- Ensemble: Fierce Combatant's Dreadplate Armor
            150254, -- Ensemble: Fierce Combatant's Felskin Armor
            150255, -- Ensemble: Fierce Combatant's Felskin Armor
            150256, -- Ensemble: Fierce Combatant's Felweave Armor
            150257, -- Ensemble: Fierce Combatant's Felweave Armor
            150258, -- Ensemble: Fierce Combatant's Ironskin Armor
            150259, -- Ensemble: Fierce Combatant's Ironskin Armor
            150260, -- Ensemble: Fierce Combatant's Leather Armor
            150261, -- Ensemble: Fierce Combatant's Leather Armor
            150262, -- Ensemble: Fierce Combatant's Satin Armor
            150263, -- Ensemble: Fierce Combatant's Satin Armor
            150264, -- Ensemble: Fierce Combatant's Chain Armor
            150265, -- Ensemble: Fierce Combatant's Chain Armor
            150266, -- Ensemble: Fierce Combatant's Plate Armor
            150267, -- Ensemble: Fierce Combatant's Plate Armor
            150268, -- Ensemble: Fierce Combatant's Scaled Armor
            150269, -- Ensemble: Fierce Combatant's Scaled Armor
            150270, -- Ensemble: Fierce Combatant's Silk Armor
            150271, -- Ensemble: Fierce Combatant's Silk Armor
            150579, -- Cookie's Old Stew
            150924, -- Greater Tribute of the Broken Isles
            150925, -- Greater Valarjar Insignia
            150926, -- Greater Dreamweaver Insignia
            150927, -- Greater Court of Farondis Insignia
            150928, -- Greater Highmountain Tribe Insignia
            150929, -- Greater Wardens Insignia
            150930, -- Greater Nightfallen Insignia
            150931, -- Seal of Authority
            151245, -- Novitiate's Tarnished Arcanoscope
            151246, -- Fel-Imbued Artillery Catalyst
            151247, -- Lightguard Battle Medal
            151493, -- Sands of Time
            151556, -- Spoils of the Triumphant
            151561, -- Bauble of the Triumphant
            151619, -- Prize of the Triumphant
            151620, -- Humming Shard
            151696, -- Crumbling Bust
            151697, -- Chromie's Eighth Spare Pocketwatch
            151698, -- Sinister Signet Ring
            151699, -- Scrimshawed Dragonbone Paperweight
            151700, -- Wayfinder's Sundial
            152396, -- Arsenal: Weapons of the Lightforged
            152464, -- Greater Legionfall Insignia
            152651, -- Crumbling Chronicles of Argus
            152653, -- Virtue of the Light
            152654, -- Lesser Unity of the Orders
            152700, -- Krokul Battle Horn
            152706, -- Sanctified Warrior Memento
            152707, -- Lightforged Cannon Projectile
            152708, -- [addon.CONS.PH] Lightly Roasted Artifact Power
            152709, -- Partially Charged Conduit
            152710, -- Ruined Conservatory Emblem
            152711, -- Light-Smote Skullplate
            152937, -- Dendrite Flux
            152938, -- Emblazoned Shadowguard Insignia
            152939, -- Mark of the Triumvirate Warriors
            152954, -- Greater Argussian Reach Insignia
            152955, -- Greater Army of the Light Insignia
            152956, -- Greater Army of the Light Insignia
            152957, -- Army of the Light Insignia
            152958, -- Army of the Light Insignia
            152959, -- Argussian Reach Insignia
            152960, -- Argussian Reach Insignia
            152961, -- Greater Argussian Reach Insignia
            152964, -- Krokul Flute
            153007, -- Rite-Blessed Sheath
            153008, -- Timeworn Amethyst Figurine
            153009, -- Brazier of the Faithful
            153023, -- Lightforged Augment Rune
            153046, -- Duskcloak Dorsal Plate
            153047, -- Talbuk Bridle
            153052, -- Depleted Pylon Core
            153113, -- Demon's Soulstone
            153198, -- Rimy Hailstone
            153199, -- Chillward Brazier
            153200, -- Shimmering Floatroot
            153201, -- Glinting Aurinor Dewdrops
            153217, -- Emblazoned Fire Core
            153218, -- Primordial Giant's Heart
            153220, -- Blood Drainer Globe
            153221, -- Congealed Animus Plasma
            153222, -- Hallowed Glade Pinecone
            153223, -- Shed Antlers of Elkeirnir
            153224, -- Luminescent Moonshroom
            153225, -- Naigtal Shambler Eye
            130144, -- Crystallized Fey Darter Egg
            130149, -- Carved Smolderhide Figurines
            130153, -- Godafoss Essence
            130159, -- Ravencrest Shield
            130160, -- Vial of Pure Moonrest Water
            130165, -- Heathrow Keepsake
            131728, -- Urn of Malgalor's Blood
            131753, -- Prayers to the Earthmother
            131758, -- Oversized Acorn
            131778, -- Woodcarved Rabbit
            131784, -- Left Half of a Locket
            131785, -- Right Half of a Locket
            131789, -- Handmade Mobile
            131808, -- Engraved Bloodtotem Armlet
            132361, -- Petrified Arkhana
            132923, -- Hrydshal Etching
            130152, -- Condensed Light of Elune
            131751, -- Fractured Portal Shard
            131763, -- Bundle of Trueshot Arrows
            131795, -- Nar'thalasian Corsage
            131802, -- Offering to Ram'Pag
            132897, -- Mandate of the Watchers
            132950, -- Petrified Snake
            141891, -- Branch of Shaladrassil
            141892, -- Gilbert's Finest
            141896, -- Nashal's Spyglass
            146314, -- Marble Arrowhead
            146315, -- Moon-Rune of Elune
            146320, -- Faded Green Gem
            146321, -- Fossilized Succubus Horn
            146325, -- Worm-Eaten Grain Pouch
            146326, -- Scorched Pyrestone
            137414, -- Pet Tournament Purse
            138535, -- Ensemble: Warmongering Combatant's Ringmail Armor
            138536, -- Ensemble: Warmongering Combatant's Ringmail Armor
            138537, -- Ensemble: Warmongering Combatant's Chain Armor
            138538, -- Ensemble: Warmongering Combatant's Chain Armor
            138539, -- Ensemble: Warmongering Combatant's Silk Armor
            138540, -- Ensemble: Warmongering Combatant's Silk Armor
            138541, -- Ensemble: Warmongering Combatant's Satin Armor
            138542, -- Ensemble: Warmongering Combatant's Satin Armor
            138543, -- Ensemble: Warmongering Combatant's Felweave Armor
            138544, -- Ensemble: Warmongering Combatant's Felweave Armor
            138545, -- Ensemble: Wild Combatant's Ringmail Armor
            138546, -- Ensemble: Wild Combatant's Ringmail Armor
            138547, -- Ensemble: Wild Combatant's Chain Armor
            138548, -- Ensemble: Wild Combatant's Chain Armor
            138549, -- Ensemble: Wild Combatant's Silk Armor
            138550, -- Ensemble: Wild Combatant's Silk Armor
            138551, -- Ensemble: Wild Combatant's Satin Armor
            138552, -- Ensemble: Wild Combatant's Satin Armor
            138553, -- Ensemble: Wild Combatant's Felweave Armor
            138554, -- Ensemble: Wild Combatant's Felweave Armor
            138577, -- Ensemble: Primal Combatant's Ringmail Armor
            138578, -- Ensemble: Primal Combatant's Ringmail Armor
            138579, -- Ensemble: Primal Combatant's Chain Armor
            138580, -- Ensemble: Primal Combatant's Chain Armor
            138581, -- Ensemble: Primal Combatant's Silk Armor
            138582, -- Ensemble: Primal Combatant's Silk Armor
            138583, -- Ensemble: Primal Combatant's Satin Armor
            138584, -- Ensemble: Primal Combatant's Satin Armor
            138585, -- Ensemble: Primal Combatant's Felweave Armor
            138586, -- Ensemble: Primal Combatant's Felweave Armor
            138587, -- Ensemble: Warmongering Combatant's Plate Armor
            138588, -- Ensemble: Warmongering Combatant's Plate Armor
            138589, -- Ensemble: Warmongering Combatant's Dreadplate Armor
            138590, -- Ensemble: Warmongering Combatant's Dreadplate Armor
            138591, -- Ensemble: Warmongering Combatant's Scaled Armor
            138592, -- Ensemble: Warmongering Combatant's Scaled Armor
            138593, -- Ensemble: Warmongering Combatant's Dragonhide Armor
            138594, -- Ensemble: Warmongering Combatant's Dragonhide Armor
            138595, -- Ensemble: Warmongering Combatant's Ironskin Armor
            138596, -- Ensemble: Warmongering Combatant's Ironskin Armor
            138597, -- Ensemble: Warmongering Combatant's Leather Armor
            138598, -- Ensemble: Warmongering Combatant's Leather Armor
            138599, -- Ensemble: Wild Combatant's Plate Armor
            138600, -- Ensemble: Wild Combatant's Plate Armor
            138601, -- Ensemble: Wild Combatant's Dreadplate Armor
            138602, -- Ensemble: Wild Combatant's Dreadplate Armor
            138603, -- Ensemble: Wild Combatant's Scaled Armor
            138604, -- Ensemble: Wild Combatant's Scaled Armor
            138605, -- Ensemble: Wild Combatant's Dragonhide Armor
            138606, -- Ensemble: Wild Combatant's Dragonhide Armor
            138607, -- Ensemble: Wild Combatant's Ironskin Armor
            138608, -- Ensemble: Wild Combatant's Ironskin Armor
            138609, -- Ensemble: Wild Combatant's Leather Armor
            138610, -- Ensemble: Wild Combatant's Leather Armor
            138611, -- Ensemble: Primal Combatant's Plate Armor
            138612, -- Ensemble: Primal Combatant's Plate Armor
            138613, -- Ensemble: Primal Combatant's Dreadplate Armor
            138614, -- Ensemble: Primal Combatant's Dreadplate Armor
            138615, -- Ensemble: Primal Combatant's Scaled Armor
            138616, -- Ensemble: Primal Combatant's Scaled Armor
            138617, -- Ensemble: Primal Combatant's Dragonhide Armor
            138618, -- Ensemble: Primal Combatant's Dragonhide Armor
            138619, -- Ensemble: Primal Combatant's Ironskin Armor
            138620, -- Ensemble: Primal Combatant's Ironskin Armor
            138621, -- Ensemble: Primal Combatant's Leather Armor
            138622, -- Ensemble: Primal Combatant's Leather Armor
            139777, -- Strange Crate
            142381, -- Oath of Fealty
            138714, -- Ensemble: Dreadful Gladiator's Plate Armor
            138715, -- Ensemble: Dreadful Gladiator's Dreadplate Armor
            138716, -- Ensemble: Dreadful Gladiator's Scaled Armor
            138717, -- Ensemble: Dreadful Gladiator's Ringmail Armor
            138718, -- Ensemble: Dreadful Gladiator's Chain Armor
            138719, -- Ensemble: Dreadful Gladiator's Dragonhide Armor
            138720, -- Ensemble: Dreadful Gladiator's Ironskin Armor
            138721, -- Ensemble: Dreadful Gladiator's Leather Armor
            138722, -- Ensemble: Dreadful Gladiator's Silk Armor
            138723, -- Ensemble: Dreadful Gladiator's Satin Armor
            138724, -- Ensemble: Dreadful Gladiator's Felweave Armor
            146419, -- Ensemble: Bloodthirsty Gladiator's Chain Armor
            146421, -- Ensemble: Bloodthirsty Gladiator's Dragonhide Armor
            146423, -- Ensemble: Bloodthirsty Gladiator's Dreadplate Armor
            146425, -- Ensemble: Bloodthirsty Gladiator's Felweave Armor
            146427, -- Ensemble: Bloodthirsty Gladiator's Leather Armor
            146429, -- Ensemble: Bloodthirsty Gladiator's Plate Armor
            146431, -- Ensemble: Bloodthirsty Gladiator's Ringmail Armor
            146433, -- Ensemble: Bloodthirsty Gladiator's Satin Armor
            146435, -- Ensemble: Bloodthirsty Gladiator's Scaled Armor
            146437, -- Ensemble: Bloodthirsty Gladiator's Silk Armor
            146578, -- Ensemble: Savage Gladiator's Chain Armor
            146579, -- Ensemble: Savage Gladiator's Dragonhide Armor
            146580, -- Ensemble: Savage Gladiator's Dreadplate Armor
            146581, -- Ensemble: Savage Gladiator's Felweave Armor
            146582, -- Ensemble: Savage Gladiator's Leather Armor
            146583, -- Ensemble: Savage Gladiator's Plate Armor
            146584, -- Ensemble: Savage Gladiator's Ringmail Armor
            146585, -- Ensemble: Savage Gladiator's Satin Armor
            146586, -- Ensemble: Savage Gladiator's Scaled Armor
            146587, -- Ensemble: Savage Gladiator's Silk Armor
            138430, -- Ensemble: Chain of the Scarlet Crusade
            138431, -- Ensemble: Scale of the Scarlet Crusade
            142273, -- Ensemble: Blackened Defias Armor
            129094, -- Nightwell Ambrosia
            130897, -- Miracles and You
            130898, -- Light in the Darkness
            130899, -- Striding with the Sunwalkers
            130900, -- Sacrificing for Your Friends
            130936, -- Silver Squire Horn
            133555, -- Ritssyn Flamescowl
            133556, -- Lulubelle Fizzlebang
            134026, -- Honorable Pennant
            134031, -- Prestigious Pennant
            134032, -- Elite Pennant
            134034, -- Esteemed Pennant
            134062, -- Mog'dorg the Wizened
            134064, -- The Great Akazamzarak
            136269, -- Kel'danath's Manaflask
            137675, -- Transmog Set Test
            138413, -- Boots of Efficiency
            138787, -- Tome of Illusions: Azeroth
            138789, -- Tome of Illusions: Outland
            138790, -- Tome of Illusions: Northrend
            138791, -- Tome of Illusions: Cataclysm
            138792, -- Tome of Illusions: Elemental Lords
            138793, -- Tome of Illusions: Pandaria
            138794, -- Tome of Illusions: Secrets of the Shado-Pan
            138795, -- Tome of Illusions: Draenor
            138796, -- Illusion: Executioner
            138797, -- Illusion: Mongoose
            138798, -- Illusion: Sunfire
            138799, -- Illusion: Soulfrost
            138800, -- Illusion: Blade Ward
            138801, -- Illusion: Blood Draining
            138802, -- Illusion: Power Torrent
            138803, -- Illusion: Mending
            138804, -- Illusion: Colossus
            138805, -- Illusion: Jade Spirit
            138806, -- Illusion: Mark of Shadowmoon
            138807, -- Illusion: Mark of the Shattered Hand
            138808, -- Illusion: Mark of the Bleeding Hollow
            138809, -- Illusion: Mark of Blackrock
            138827, -- Illusion: Nightmare
            138828, -- Illusion: Chronos
            138832, -- Illusion: Earthliving
            138833, -- Illusion: Flametongue
            138834, -- Illusion: Frostbrand
            138835, -- Illusion: Rockbiter
            138836, -- Illusion: Windfury
            138838, -- Illusion: Deathfrost
            138954, -- Illusion: Poisoned
            138955, -- Illusion: Rune of Razorice
            139888, -- Frost Crux
            139892, -- Demonic Phylactery
            140038, -- Focusing Crystal
            140155, -- Silver Hand Orders
            140157, -- Horn of War
            140158, -- Empowered Rift Core
            140260, -- Arcane Remnant of Falanaar
            140336, -- Brulfist Idol
            141870, -- Arcane Tablet of Falanaar
            142288, -- Rumble Card: Grief Warden
            142289, -- Rumble Card: Penguin Stampede
            142290, -- Rumble Card: Battle of the Brew
            142291, -- Rumble Card: Senya
            142292, -- Rumble Card: Stranglethorn Streak
            142293, -- Rumble Card: Mindbreaker Gzzaj
            142294, -- Rumble Card: Mazhareen
            142295, -- Rumble Card: Rumble 08
            142311, -- Free Drinks Voucher
            142313, -- Zeppelin Rental Form
            142314, -- Brawler's Potion Dispenser
            142317, -- Blood-Soaked Angel Figurine
            142318, -- High Roller's Contract
            142319, -- Bag of Chipped Dice
            142528, -- Crate of Bobbers: Can of Worms
            142529, -- Crate of Bobbers: Cat Head
            142530, -- Crate of Bobbers: Tugboat
            142531, -- Crate of Bobbers: Squeaky Duck
            142532, -- Crate of Bobbers: Murloc Head
            143662, -- Crate of Bobbers: Wooden Pepe
            143727, -- Champion's Salute
            143758, -- Free Drinks Voucher
            143759, -- VIP Room Rental Form
            143760, -- Brawler's Potion Dispenser
            143761, -- Blood-Soaked Angel Figurine
            143762, -- High Roller's Contract
            143763, -- Bag of Chipped Dice
            143948, -- Chilled Satchel of Vegetables
            147307, -- Carved Wooden Helm
            147308, -- Enchanted Bobber
            147309, -- Face of the Forest
            147310, -- Floating Totem
            147311, -- Replica Gondola
            147312, -- Demon Noggin
            147416, -- Arcane Tablet of Falanaar
            147418, -- Arcane Remnant of Falanaar
            151117, -- Ensemble: Mana-Etched Regalia
            151118, -- Ensemble: Obsidian Prowler's Garb
            151119, -- Ensemble: Der'izu Armor
            151120, -- Ensemble: Righteous Battleplate
            151492, -- Bronze Drake
            152556, -- Trawler Totem
            152574, -- Corbyn's Beacon
            153059, -- Relinquished Arcane Relic
            153060, -- Relinquished Blood Relic
            153061, -- Relinquished Fel Relic
            153062, -- Relinquished Fire Relic
            153063, -- Relinquished Frost Relic
            153064, -- Relinquished Holy Relic
            153065, -- Relinquished Iron Relic
            153066, -- Relinquished Life Relic
            153067, -- Relinquished Shadow Relic
            153068, -- Relinquished Storm Relic
            153205, -- Relinquished Girdle
            153206, -- Relinquished Bracers
            153207, -- Relinquished Treads
            153208, -- Relinquished Chestguard
            153209, -- Relinquished Cloak
            153210, -- Relinquished Gauntlets
            153211, -- Relinquished Hood
            153212, -- Relinquished Leggings
            153213, -- Relinquished Necklace
            153214, -- Relinquished Ring
            153215, -- Relinquished Spaulders
            153216, -- Relinquished Trinket
            128026, -- Trembling Phylactery
            134118, -- Cluster of Potentiation
            134133, -- Jewel of Brilliance
            138782, -- Brief History of the Ages
            139011, -- Berserking Helm of Ondry'el
            139019, -- Spellmask of Alla'onus
            139027, -- Lenses of Spellseer Dellian
            139028, -- Disc of the Starcaller
            139413, -- Greater Questor's Glory
            139506, -- Greater Glory of the Order
            140239, -- Excavated Highborne Artifact
            140242, -- Astromancer's Compass
            140245, -- The Tidemistress' Enchanted Pearl
            140247, -- Mornath's Enchanted Statue
            140305, -- Brimming Essence
            140307, -- Heart of Zin-Azshari
            140409, -- Tome of Dimensional Awareness
            140410, -- Mark of the Rogues
            140421, -- Ancient Qiraji Idol
            140422, -- Moonglow Idol
            140444, -- Dream Tear
            140445, -- Arcfruit
            140450, -- Berserking Helm of Taenna
            140451, -- Spellmask of Azsylla
            140517, -- Glory of the Order
            141070, -- Portal Stone
            141072, -- Grimoire of Doom
            141313, -- Manafused Fal'dorei Egg Sac
            141314, -- Treemender's Beacon
            141655, -- Shimmering Ancient Mana Cluster
            141667, -- Ancient Keeper's Brooch
            141669, -- Fel-Touched Tome
            141673, -- Love-Laced Arrow
            141676, -- The Valewatcher's Boon
            141677, -- Key to the Bazaar
            141678, -- Night Devint: The Perfection of Arcwine
            141679, -- Cobalt Amber Crystal
            141680, -- Titan-Forged Locket
            141681, -- Valewalker Talisman
            141682, -- Free Floating Ley Spark
            141683, -- Mana-Injected Chronarch Power Core
            141684, -- Residual Manastorm Energy
            141685, -- The Valewalker's Blessing
            141712, -- Gjallar's "Horn"
            141856, -- History of the Ages
            141884, -- Krota's Shield
            141889, -- Glory of the Melee
            141932, -- Shard of Compacted Energy
            141950, -- Arcane Seed Case
            141951, -- Spellbound Jewelry Box
            141952, -- Delving Deeper by Arcanist Perclanea
            141953, -- Nightglow Energy Vessel
            141954, -- 'Borrowed' Highborne Magi's Chalice
            141955, -- Corrupted Duskmere Crest
            142454, -- Viz'aduum's Eye
            142455, -- Demonic Command Shards
            142533, -- Titan's Boon
            142534, -- Plume of the Fallen Val'kyr
            142535, -- Soulcatcher of the Encroaching Mist
            142555, -- Aoire's Crook
            143333, -- Badge of Vengeance
            143486, -- Arcshaper's Barrier
            143487, -- Enchanted Dusk Lily
            143498, -- Charged Construct's Finger
            143499, -- Overcharged Infiltrator's Mask
            143506, -- Ensemble: Vestment of the Chosen Dead
            143507, -- Ensemble: Vestment of the Chosen Dead
            143508, -- Ensemble: Vestment of the Chosen Dead
            143509, -- Ensemble: Vestment of the Chosen Dead
            143510, -- Ensemble: Garb of the Chosen Dead
            143511, -- Ensemble: Garb of the Chosen Dead
            143512, -- Ensemble: Garb of the Chosen Dead
            143513, -- Ensemble: Garb of the Chosen Dead
            143514, -- Ensemble: Chains of the Chosen Dead
            143515, -- Ensemble: Chains of the Chosen Dead
            143516, -- Ensemble: Chains of the Chosen Dead
            143517, -- Ensemble: Chains of the Chosen Dead
            143518, -- Ensemble: Funerary Plate of the Chosen Dead
            143519, -- Ensemble: Funerary Plate of the Chosen Dead
            143520, -- Ensemble: Funerary Plate of the Chosen Dead
            143521, -- Ensemble: Funerary Plate of the Chosen Dead
            143533, -- Infused Pit Lord Tusk
            143536, -- Overloaded Scrying Orb
            143538, -- Entangled Telemancy Orb
            143540, -- Undertuned Attunement Crystal
            143716, -- Soldier's Legacy
            143738, -- Flying Hourglass
            143739, -- Mark of the Soulcleaver
            143740, -- Enchanted Sin'dorei Banner
            143741, -- Blessed Kaldorei Banner
            143742, -- Scepter of Kerxan
            143743, -- Highly Charged Mana Clog
            143744, -- Blessed Ravencrest Blades
            143745, -- Ley-Syphoner's Focus
            143746, -- Fel-Dipped Blade
            143747, -- Key to the Nighthold
            143749, -- Corrupted Nightborne Matrix
            143757, -- Headpiece of the Shadow Council
            146130, -- Ensemble: Vindictive Gladiator's Plate Armor
            146131, -- Ensemble: Vindictive Gladiator's Plate Armor
            146132, -- Ensemble: Vindictive Gladiator's Dreadplate Armor
            146133, -- Ensemble: Vindictive Gladiator's Dreadplate Armor
            146134, -- Ensemble: Vindictive Gladiator's Scaled Armor
            146135, -- Ensemble: Vindictive Gladiator's Scaled Armor
            146136, -- Ensemble: Vindictive Gladiator's Ringmail Armor
            146137, -- Ensemble: Vindictive Gladiator's Ringmail Armor
            146138, -- Ensemble: Vindictive Gladiator's Chain Armor
            146139, -- Ensemble: Vindictive Gladiator's Chain Armor
            146140, -- Ensemble: Vindictive Gladiator's Dragonhide Armor
            146141, -- Ensemble: Vindictive Gladiator's Dragonhide Armor
            146142, -- Ensemble: Vindictive Gladiator's Ironskin Armor
            146143, -- Ensemble: Vindictive Gladiator's Ironskin Armor
            146144, -- Ensemble: Vindictive Gladiator's Leather Armor
            146145, -- Ensemble: Vindictive Gladiator's Leather Armor
            146146, -- Ensemble: Vindictive Gladiator's Silk Armor
            146147, -- Ensemble: Vindictive Gladiator's Silk Armor
            146148, -- Ensemble: Vindictive Gladiator's Satin Armor
            146149, -- Ensemble: Vindictive Gladiator's Satin Armor
            146150, -- Ensemble: Vindictive Gladiator's Felweave Armor
            146151, -- Ensemble: Vindictive Gladiator's Felweave Armor
            146174, -- Ensemble: Fearless Gladiator's Plate Armor
            146175, -- Ensemble: Fearless Gladiator's Plate Armor
            146176, -- Ensemble: Fearless Gladiator's Dreadplate Armor
            146177, -- Ensemble: Fearless Gladiator's Dreadplate Armor
            146178, -- Ensemble: Fearless Gladiator's Scaled Armor
            146179, -- Ensemble: Fearless Gladiator's Scaled Armor
            146180, -- Ensemble: Fearless Gladiator's Ringmail Armor
            146181, -- Ensemble: Fearless Gladiator's Ringmail Armor
            146182, -- Ensemble: Fearless Gladiator's Chain Armor
            146183, -- Ensemble: Fearless Gladiator's Chain Armor
            146184, -- Ensemble: Fearless Gladiator's Dragonhide Armor
            146185, -- Ensemble: Fearless Gladiator's Dragonhide Armor
            146186, -- Ensemble: Fearless Gladiator's Ironskin Armor
            146187, -- Ensemble: Fearless Gladiator's Ironskin Armor
            146188, -- Ensemble: Fearless Gladiator's Leather Armor
            146189, -- Ensemble: Fearless Gladiator's Leather Armor
            146190, -- Ensemble: Fearless Gladiator's Silk Armor
            146191, -- Ensemble: Fearless Gladiator's Silk Armor
            146192, -- Ensemble: Fearless Gladiator's Satin Armor
            146193, -- Ensemble: Fearless Gladiator's Satin Armor
            146194, -- Ensemble: Fearless Gladiator's Felweave Armor
            146195, -- Ensemble: Fearless Gladiator's Felweave Armor
            146196, -- Ensemble: Fearless Combatant's Ringmail Armor
            146197, -- Ensemble: Fearless Combatant's Ringmail Armor
            146198, -- Ensemble: Fearless Combatant's Chain Armor
            146199, -- Ensemble: Fearless Combatant's Chain Armor
            146200, -- Ensemble: Fearless Combatant's Silk Armor
            146201, -- Ensemble: Fearless Combatant's Silk Armor
            146202, -- Ensemble: Fearless Combatant's Satin Armor
            146203, -- Ensemble: Fearless Combatant's Satin Armor
            146204, -- Ensemble: Fearless Combatant's Felweave Armor
            146205, -- Ensemble: Fearless Combatant's Felweave Armor
            146206, -- Ensemble: Fearless Combatant's Plate Armor
            146207, -- Ensemble: Fearless Combatant's Plate Armor
            146208, -- Ensemble: Fearless Combatant's Dreadplate Armor
            146209, -- Ensemble: Fearless Combatant's Dreadplate Armor
            146210, -- Ensemble: Fearless Combatant's Scaled Armor
            146211, -- Ensemble: Fearless Combatant's Scaled Armor
            146212, -- Ensemble: Fearless Combatant's Dragonhide Armor
            146213, -- Ensemble: Fearless Combatant's Dragonhide Armor
            146214, -- Ensemble: Fearless Combatant's Ironskin Armor
            146215, -- Ensemble: Fearless Combatant's Ironskin Armor
            146216, -- Ensemble: Fearless Combatant's Leather Armor
            146217, -- Ensemble: Fearless Combatant's Leather Armor
            146218, -- Ensemble: Elite Vindictive Gladiator's Plate Armor
            146219, -- Ensemble: Elite Vindictive Gladiator's Plate Armor
            146220, -- Ensemble: Elite Vindictive Gladiator's Dreadplate Armor
            146221, -- Ensemble: Elite Vindictive Gladiator's Dreadplate Armor
            146222, -- Ensemble: Elite Vindictive Gladiator's Scaled Armor
            146223, -- Ensemble: Elite Vindictive Gladiator's Scaled Armor
            146224, -- Ensemble: Elite Vindictive Gladiator's Ringmail Armor
            146225, -- Ensemble: Elite Vindictive Gladiator's Ringmail Armor
            146226, -- Ensemble: Elite Vindictive Gladiator's Chain Armor
            146227, -- Ensemble: Elite Vindictive Gladiator's Chain Armor
            146228, -- Ensemble: Elite Vindictive Gladiator's Dragonhide Armor
            146229, -- Ensemble: Elite Vindictive Gladiator's Dragonhide Armor
            146230, -- Ensemble: Elite Vindictive Gladiator's Ironskin Armor
            146231, -- Ensemble: Elite Vindictive Gladiator's Ironskin Armor
            146232, -- Ensemble: Elite Vindictive Gladiator's Leather Armor
            146233, -- Ensemble: Elite Vindictive Gladiator's Leather Armor
            146234, -- Ensemble: Elite Vindictive Gladiator's Silk Armor
            146235, -- Ensemble: Elite Vindictive Gladiator's Silk Armor
            146236, -- Ensemble: Elite Vindictive Gladiator's Satin Armor
            146237, -- Ensemble: Elite Vindictive Gladiator's Satin Armor
            146238, -- Ensemble: Elite Vindictive Gladiator's Felweave Armor
            146239, -- Ensemble: Elite Vindictive Gladiator's Felweave Armor
            146240, -- Ensemble: Elite Fearless Gladiator's Plate Armor
            146241, -- Ensemble: Elite Fearless Gladiator's Plate Armor
            146242, -- Ensemble: Elite Fearless Gladiator's Dreadplate Armor
            146243, -- Ensemble: Elite Fearless Gladiator's Dreadplate Armor
            146244, -- Ensemble: Elite Fearless Gladiator's Scaled Armor
            146245, -- Ensemble: Elite Fearless Gladiator's Scaled Armor
            146246, -- Ensemble: Elite Fearless Gladiator's Ringmail Armor
            146247, -- Ensemble: Elite Fearless Gladiator's Ringmail Armor
            146248, -- Ensemble: Elite Fearless Gladiator's Chain Armor
            146249, -- Ensemble: Elite Fearless Gladiator's Chain Armor
            146250, -- Ensemble: Elite Fearless Gladiator's Dragonhide Armor
            146251, -- Ensemble: Elite Fearless Gladiator's Dragonhide Armor
            146252, -- Ensemble: Elite Fearless Gladiator's Ironskin Armor
            146253, -- Ensemble: Elite Fearless Gladiator's Ironskin Armor
            146254, -- Ensemble: Elite Fearless Gladiator's Leather Armor
            146255, -- Ensemble: Elite Fearless Gladiator's Leather Armor
            146256, -- Ensemble: Elite Fearless Gladiator's Silk Armor
            146257, -- Ensemble: Elite Fearless Gladiator's Silk Armor
            146258, -- Ensemble: Elite Fearless Gladiator's Satin Armor
            146259, -- Ensemble: Elite Fearless Gladiator's Satin Armor
            146260, -- Ensemble: Elite Fearless Gladiator's Felweave Armor
            146261, -- Ensemble: Elite Fearless Gladiator's Felweave Armor
            146262, -- Ensemble: Vindictive Gladiator's Felskin Armor
            146263, -- Ensemble: Vindictive Gladiator's Felskin Armor
            146266, -- Ensemble: Fearless Gladiator's Felskin Armor
            146267, -- Ensemble: Fearless Gladiator's Felskin Armor
            146268, -- Ensemble: Fearless Combatant's Felskin Armor
            146269, -- Ensemble: Fearless Combatant's Felskin Armor
            146270, -- Ensemble: Elite Vindictive Gladiator's Felskin Armor
            146271, -- Ensemble: Elite Vindictive Gladiator's Felskin Armor
            146272, -- Ensemble: Elite Fearless Gladiator's Felskin Armor
            146273, -- Ensemble: Elite Fearless Gladiator's Felskin Armor
            146916, -- Portal-Stone: Than'otalion
            146917, -- Portal-Stone: Skulguloth
            146918, -- Portal-Stone: Force-Commander Xillious
            146919, -- Portal-Stone: An'thyna
            146920, -- Portal-Stone: Fel Obliterator
            146921, -- Portal-Stone: Illisthyndria
            147203, -- Soldier's Legacy
            147441, -- Sheaf of Royal Stationery
            147442, -- Gilded Prayer Beads
            147444, -- Ravaged Wristclamp Strap
            147456, -- Frostbound Sinew
            147457, -- Spurs of the Risen Horsemen
            147458, -- Mardum Soulcrystal
            147459, -- Reclaimed Ashtongue Feltome
            147460, -- Fang of Goldrinn
            147461, -- Unblemished Leaf of Shaladrassil
            147462, -- Feather of Ohn'ahra
            147463, -- Skyhorn Ritual Drum
            147464, -- Scroll of Tirisgarde Arcana
            147465, -- Mage-Guard Dueling Wand
            147466, -- Chi-Ji Tailfeather
            147467, -- Marble Niuzao Effigy
            147468, -- Memoir of the Broken Temple
            147469, -- Blessed Sculpture of Tyr
            147470, -- Silver Hand Aspirant's Codex
            147471, -- Netherlight Reliquary
            147472, -- Blessed Brooch of the Conclave
            147473, -- Antique Ravenholdt Dagger
            147474, -- Uncrowned Poison Set
            147475, -- Mark of the Aligned Elemental Lords
            147476, -- Everburning Smolderon Ember
            147477, -- Council-Empowered Riftcrystal
            147478, -- Dreadscar Instigator's Broken Soul
            147479, -- Helgar Forged Trophy Blade
            147480, -- Valajar-Sanctified Weapon Polish
            147481, -- Gilded Skyhold Greatshield
            147621, -- Ensemble: Elite Cruel Gladiator's Chain Armor
            147622, -- Ensemble: Elite Cruel Gladiator's Chain Armor
            147623, -- Ensemble: Elite Cruel Gladiator's Dragonhide Armor
            147624, -- Ensemble: Elite Cruel Gladiator's Dragonhide Armor
            147625, -- Ensemble: Elite Cruel Gladiator's Dreadplate Armor
            147626, -- Ensemble: Elite Cruel Gladiator's Dreadplate Armor
            147627, -- Ensemble: Elite Cruel Gladiator's Felskin Armor
            147628, -- Ensemble: Elite Cruel Gladiator's Felskin Armor
            147629, -- Ensemble: Elite Cruel Gladiator's Felweave Armor
            147630, -- Ensemble: Elite Cruel Gladiator's Felweave Armor
            147631, -- Ensemble: Elite Cruel Gladiator's Ironskin Armor
            147632, -- Ensemble: Elite Cruel Gladiator's Ironskin Armor
            147633, -- Ensemble: Elite Cruel Gladiator's Leather Armor
            147634, -- Ensemble: Elite Cruel Gladiator's Leather Armor
            147635, -- Ensemble: Elite Cruel Gladiator's Plate Armor
            147636, -- Ensemble: Elite Cruel Gladiator's Plate Armor
            147637, -- Ensemble: Elite Cruel Gladiator's Ringmail Armor
            147638, -- Ensemble: Elite Cruel Gladiator's Ringmail Armor
            147639, -- Ensemble: Elite Cruel Gladiator's Satin Armor
            147640, -- Ensemble: Elite Cruel Gladiator's Satin Armor
            147641, -- Ensemble: Elite Cruel Gladiator's Scaled Armor
            147642, -- Ensemble: Elite Cruel Gladiator's Scaled Armor
            147643, -- Ensemble: Elite Cruel Gladiator's Silk Armor
            147644, -- Ensemble: Elite Cruel Gladiator's Silk Armor
            147645, -- Ensemble: Cruel Gladiator's Chain Armor
            147646, -- Ensemble: Cruel Gladiator's Chain Armor
            147647, -- Ensemble: Cruel Gladiator's Dragonhide Armor
            147648, -- Ensemble: Cruel Gladiator's Dragonhide Armor
            147649, -- Ensemble: Cruel Gladiator's Dreadplate Armor
            147650, -- Ensemble: Cruel Gladiator's Dreadplate Armor
            147651, -- Ensemble: Cruel Gladiator's Felskin Armor
            147652, -- Ensemble: Cruel Gladiator's Felskin Armor
            147653, -- Ensemble: Cruel Gladiator's Felweave Armor
            147654, -- Ensemble: Cruel Gladiator's Felweave Armor
            147655, -- Ensemble: Cruel Gladiator's Ironskin Armor
            147656, -- Ensemble: Cruel Gladiator's Ironskin Armor
            147657, -- Ensemble: Cruel Gladiator's Leather Armor
            147658, -- Ensemble: Cruel Gladiator's Leather Armor
            147659, -- Ensemble: Cruel Gladiator's Plate Armor
            147660, -- Ensemble: Cruel Gladiator's Plate Armor
            147661, -- Ensemble: Cruel Gladiator's Ringmail Armor
            147662, -- Ensemble: Cruel Gladiator's Ringmail Armor
            147663, -- Ensemble: Cruel Gladiator's Satin Armor
            147664, -- Ensemble: Cruel Gladiator's Satin Armor
            147665, -- Ensemble: Cruel Gladiator's Scaled Armor
            147666, -- Ensemble: Cruel Gladiator's Scaled Armor
            147667, -- Ensemble: Cruel Gladiator's Silk Armor
            147668, -- Ensemble: Cruel Gladiator's Silk Armor
            147778, -- Enchanter's Illusion - Demonic Tyranny
            147889, -- Attack Beacon: Illidari Stand
            147891, -- Attack Beacon: Starsong Refuge
            147892, -- Attack Beacon: Skyhorn
            147893, -- Attack Beacon: Stormtorn Foothills
            147894, -- Attack Beacon: Dalaran Underbelly
            149454, -- Ensemble: Elite Fierce Gladiator's Chain Armor
            149455, -- Ensemble: Elite Fierce Gladiator's Chain Armor
            149456, -- Ensemble: Elite Fierce Gladiator's Dragonhide Armor
            149457, -- Ensemble: Elite Fierce Gladiator's Dragonhide Armor
            149458, -- Ensemble: Elite Fierce Gladiator's Dreadplate Armor
            149459, -- Ensemble: Elite Fierce Gladiator's Dreadplate Armor
            149460, -- Ensemble: Elite Fierce Gladiator's Felskin Armor
            149461, -- Ensemble: Elite Fierce Gladiator's Felskin Armor
            149462, -- Ensemble: Elite Fierce Gladiator's Felweave Armor
            149463, -- Ensemble: Elite Fierce Gladiator's Felweave Armor
            149464, -- Ensemble: Elite Fierce Gladiator's Ironskin Armor
            149465, -- Ensemble: Elite Fierce Gladiator's Ironskin Armor
            149466, -- Ensemble: Elite Fierce Gladiator's Leather Armor
            149467, -- Ensemble: Elite Fierce Gladiator's Leather Armor
            149468, -- Ensemble: Elite Fierce Gladiator's Plate Armor
            149469, -- Ensemble: Elite Fierce Gladiator's Plate Armor
            149470, -- Ensemble: Elite Fierce Gladiator's Ringmail Armor
            149471, -- Ensemble: Elite Fierce Gladiator's Ringmail Armor
            149472, -- Ensemble: Elite Fierce Gladiator's Satin Armor
            149473, -- Ensemble: Elite Fierce Gladiator's Satin Armor
            149474, -- Ensemble: Elite Fierce Gladiator's Scaled Armor
            149475, -- Ensemble: Elite Fierce Gladiator's Scaled Armor
            149476, -- Ensemble: Elite Fierce Gladiator's Silk Armor
            149477, -- Ensemble: Elite Fierce Gladiator's Silk Armor
            149478, -- Ensemble: Fierce Gladiator's Chain Armor
            149479, -- Ensemble: Fierce Gladiator's Chain Armor
            149480, -- Ensemble: Fierce Gladiator's Dragonhide Armor
            149481, -- Ensemble: Fierce Gladiator's Dragonhide Armor
            149482, -- Ensemble: Fierce Gladiator's Dreadplate Armor
            149483, -- Ensemble: Fierce Gladiator's Dreadplate Armor
            149484, -- Ensemble: Fierce Gladiator's Felskin Armor
            149485, -- Ensemble: Fierce Gladiator's Felskin Armor
            149486, -- Ensemble: Fierce Gladiator's Felweave Armor
            149487, -- Ensemble: Fierce Gladiator's Felweave Armor
            149488, -- Ensemble: Fierce Gladiator's Ironskin Armor
            149489, -- Ensemble: Fierce Gladiator's Ironskin Armor
            149490, -- Ensemble: Fierce Gladiator's Leather Armor
            149491, -- Ensemble: Fierce Gladiator's Leather Armor
            149492, -- Ensemble: Fierce Gladiator's Plate Armor
            149493, -- Ensemble: Fierce Gladiator's Plate Armor
            149494, -- Ensemble: Fierce Gladiator's Ringmail Armor
            149495, -- Ensemble: Fierce Gladiator's Ringmail Armor
            149496, -- Ensemble: Fierce Gladiator's Satin Armor
            149497, -- Ensemble: Fierce Gladiator's Satin Armor
            149498, -- Ensemble: Fierce Gladiator's Scaled Armor
            149499, -- Ensemble: Fierce Gladiator's Scaled Armor
            149500, -- Ensemble: Fierce Gladiator's Silk Armor
            149501, -- Ensemble: Fierce Gladiator's Silk Armor
            151692, -- Ensemble: Chronoscryer's Finery
            151693, -- Ensemble: Riftscarred Vestments
            151694, -- Ensemble: Epoch Sentinel's Mail
            151695, -- Ensemble: Timewarden's Plate
            151789, -- Defense Construct Identifier
            152241, -- Ensemble: Light-Woven Triumvirate Regalia
            152242, -- Ensemble: Burnished Triumvirate Armor
            152243, -- Ensemble: Sterling Triumvirate Chainmail
            152244, -- Ensemble: Venerated Triumvirate Battleplate
            152430, -- Krokul Hovel Chronicle
            152431, -- Shadowguard Harvester
            152432, -- Stasis Core Crystal
            152433, -- Smoldering Infernal Stone
            152434, -- Praetorium Edict
            152435, -- Aranasi Mandible
            152504, -- Shattered Seal of the Unrepentant Guardian
            152712, -- Sanctified Prayer Effigy
            152713, -- Discarded Remnant of the Void
            152962, -- Voidcore Control Rod
            152984, -- Boon of the Pantheon
            153048, -- Phial of 'Sacred' Fel Liquid
            153259, -- Legion General's Medallion of Command
            153266, -- Undulating Orb of Fel Inferno
            153278, -- Vile Temptresses' Aphrodisiac
            155831, -- Pantheon's Blessing
            130251, -- JewelCraft
            146316, -- Ancient Suramar Silver
            146322, -- Fel Toast
            146327, -- Petrified Air Totem
            121818, -- Libram of Divinity
            129372, -- Spymaster Jenri's Scope
            129373, -- Spymaster Jenri's Thieves' Tools
            136356, -- Compendium of Ancient Weapons Volume I
            136655, -- Compendium of Ancient Weapons Volume II
            136656, -- Compendium of Ancient Weapons Volume III
            136657, -- Compendium of Ancient Weapons Volume IV
            136658, -- Compendium of Ancient Weapons Volume V
            136659, -- Compendium of Ancient Weapons Volume VI
            136660, -- Compendium of Ancient Weapons Volume VII
            136661, -- Compendium of Ancient Weapons Volume VIII
            136662, -- Compendium of Ancient Weapons Volume IX
            136663, -- Compendium of Ancient Weapons Volume X
            136664, -- Compendium of Ancient Weapons Volume XI
            136854, -- Arsenal: Draenor Challenger's Armaments
            138491, -- Ensemble: Warmongering Gladiator's Plate Armor
            138492, -- Ensemble: Warmongering Gladiator's Plate Armor
            138493, -- Ensemble: Warmongering Gladiator's Dreadplate Armor
            138494, -- Ensemble: Warmongering Gladiator's Dreadplate Armor
            138495, -- Ensemble: Warmongering Gladiator's Scaled Armor
            138496, -- Ensemble: Warmongering Gladiator's Scaled Armor
            138497, -- Ensemble: Warmongering Gladiator's Ringmail Armor
            138498, -- Ensemble: Warmongering Gladiator's Ringmail Armor
            138499, -- Ensemble: Warmongering Gladiator's Chain Armor
            138500, -- Ensemble: Warmongering Gladiator's Chain Armor
            138501, -- Ensemble: Warmongering Gladiator's Dragonhide Armor
            138502, -- Ensemble: Warmongering Gladiator's Dragonhide Armor
            138503, -- Ensemble: Warmongering Gladiator's Ironskin Armor
            138504, -- Ensemble: Warmongering Gladiator's Ironskin Armor
            138505, -- Ensemble: Warmongering Gladiator's Leather Armor
            138506, -- Ensemble: Warmongering Gladiator's Leather Armor
            138507, -- Ensemble: Warmongering Gladiator's Silk Armor
            138508, -- Ensemble: Warmongering Gladiator's Silk Armor
            138509, -- Ensemble: Warmongering Gladiator's Satin Armor
            138510, -- Ensemble: Warmongering Gladiator's Satin Armor
            138511, -- Ensemble: Warmongering Gladiator's Felweave Armor
            138512, -- Ensemble: Warmongering Gladiator's Felweave Armor
            138513, -- Ensemble: Wild Gladiator's Plate Armor
            138514, -- Ensemble: Wild Gladiator's Plate Armor
            138515, -- Ensemble: Wild Gladiator's Dreadplate Armor
            138516, -- Ensemble: Wild Gladiator's Dreadplate Armor
            138517, -- Ensemble: Wild Gladiator's Scaled Armor
            138518, -- Ensemble: Wild Gladiator's Scaled Armor
            138519, -- Ensemble: Wild Gladiator's Ringmail Armor
            138520, -- Ensemble: Wild Gladiator's Ringmail Armor
            138521, -- Ensemble: Wild Gladiator's Chain Armor
            138522, -- Ensemble: Wild Gladiator's Chain Armor
            138523, -- Ensemble: Wild Gladiator's Dragonhide Armor
            138524, -- Ensemble: Wild Gladiator's Dragonhide Armor
            138525, -- Ensemble: Wild Gladiator's Ironskin Armor
            138526, -- Ensemble: Wild Gladiator's Ironskin Armor
            138527, -- Ensemble: Wild Gladiator's Leather Armor
            138528, -- Ensemble: Wild Gladiator's Leather Armor
            138529, -- Ensemble: Wild Gladiator's Silk Armor
            138530, -- Ensemble: Wild Gladiator's Silk Armor
            138531, -- Ensemble: Wild Gladiator's Satin Armor
            138532, -- Ensemble: Wild Gladiator's Satin Armor
            138533, -- Ensemble: Wild Gladiator's Felweave Armor
            138534, -- Ensemble: Wild Gladiator's Felweave Armor
            138555, -- Ensemble: Primal Gladiator's Plate Armor
            138556, -- Ensemble: Primal Gladiator's Plate Armor
            138557, -- Ensemble: Primal Gladiator's Dreadplate Armor
            138558, -- Ensemble: Primal Gladiator's Dreadplate Armor
            138559, -- Ensemble: Primal Gladiator's Scaled Armor
            138560, -- Ensemble: Primal Gladiator's Scaled Armor
            138561, -- Ensemble: Primal Gladiator's Ringmail Armor
            138562, -- Ensemble: Primal Gladiator's Ringmail Armor
            138563, -- Ensemble: Primal Gladiator's Chain Armor
            138564, -- Ensemble: Primal Gladiator's Chain Armor
            138565, -- Ensemble: Primal Gladiator's Dragonhide Armor
            138566, -- Ensemble: Primal Gladiator's Dragonhide Armor
            138567, -- Ensemble: Primal Gladiator's Ironskin Armor
            138568, -- Ensemble: Primal Gladiator's Ironskin Armor
            138569, -- Ensemble: Primal Gladiator's Leather Armor
            138570, -- Ensemble: Primal Gladiator's Leather Armor
            138571, -- Ensemble: Primal Gladiator's Silk Armor
            138572, -- Ensemble: Primal Gladiator's Silk Armor
            138573, -- Ensemble: Primal Gladiator's Satin Armor
            138574, -- Ensemble: Primal Gladiator's Satin Armor
            138575, -- Ensemble: Primal Gladiator's Felweave Armor
            138576, -- Ensemble: Primal Gladiator's Felweave Armor
            138625, -- Arsenal: Primal Combatant's Weapons
            138626, -- Arsenal: Primal Combatant's Weapons
            138627, -- Arsenal: Warmongering Combatant's Weapons
            138628, -- Arsenal: Warmongering Combatant's Weapons
            138629, -- Arsenal: Wild Combatant's Weapons
            138630, -- Arsenal: Wild Combatant's Weapons
            138631, -- Arsenal: Primal Gladiator's Weapons
            138632, -- Arsenal: Primal Gladiator's Weapons
            138633, -- Arsenal: Wild Gladiator's Weapons
            138634, -- Arsenal: Wild Gladiator's Weapons
            138635, -- Arsenal: Warmongering Gladiator's Weapons
            138636, -- Arsenal: Warmongering Gladiator's Weapons
            139167, -- Ensemble: Felforged Plate Armor
            139168, -- Ensemble: Fel-Chain Mail Armor
            139169, -- Ensemble: Felshroud Leather Armor
            139170, -- Ensemble: Fel-Infused Cloth Armor
            141371, -- Arsenal: Armaments of the Silver Hand
            141372, -- Arsenal: Armaments of the Ebon Blade
            138637, -- Ensemble: Prideful Gladiator's Plate Armor
            138638, -- Ensemble: Prideful Gladiator's Plate Armor
            138639, -- Ensemble: Prideful Gladiator's Dreadplate Armor
            138640, -- Ensemble: Prideful Gladiator's Dreadplate Armor
            138641, -- Ensemble: Prideful Gladiator's Scaled Armor
            138642, -- Ensemble: Prideful Gladiator's Scaled Armor
            138643, -- Ensemble: Prideful Gladiator's Ringmail Armor
            138644, -- Ensemble: Prideful Gladiator's Ringmail Armor
            138645, -- Ensemble: Prideful Gladiator's Chain Armor
            138646, -- Ensemble: Prideful Gladiator's Chain Armor
            138647, -- Ensemble: Prideful Gladiator's Dragonhide Armor
            138648, -- Ensemble: Prideful Gladiator's Dragonhide Armor
            138649, -- Ensemble: Prideful Gladiator's Ironskin Armor
            138650, -- Ensemble: Prideful Gladiator's Ironskin Armor
            138651, -- Ensemble: Prideful Gladiator's Leather Armor
            138652, -- Ensemble: Prideful Gladiator's Leather Armor
            138653, -- Ensemble: Prideful Gladiator's Silk Armor
            138654, -- Ensemble: Prideful Gladiator's Silk Armor
            138655, -- Ensemble: Prideful Gladiator's Satin Armor
            138656, -- Ensemble: Prideful Gladiator's Satin Armor
            138657, -- Ensemble: Prideful Gladiator's Felweave Armor
            138658, -- Ensemble: Prideful Gladiator's Felweave Armor
            138659, -- Ensemble: Grievous Gladiator's Plate Armor
            138660, -- Ensemble: Grievous Gladiator's Plate Armor
            138661, -- Ensemble: Grievous Gladiator's Dreadplate Armor
            138662, -- Ensemble: Grievous Gladiator's Dreadplate Armor
            138663, -- Ensemble: Grievous Gladiator's Scaled Armor
            138664, -- Ensemble: Grievous Gladiator's Scaled Armor
            138665, -- Ensemble: Grievous Gladiator's Ringmail Armor
            138666, -- Ensemble: Grievous Gladiator's Ringmail Armor
            138667, -- Ensemble: Grievous Gladiator's Chain Armor
            138668, -- Ensemble: Grievous Gladiator's Chain Armor
            138669, -- Ensemble: Grievous Gladiator's Dragonhide Armor
            138670, -- Ensemble: Grievous Gladiator's Dragonhide Armor
            138671, -- Ensemble: Grievous Gladiator's Ironskin Armor
            138672, -- Ensemble: Grievous Gladiator's Ironskin Armor
            138673, -- Ensemble: Grievous Gladiator's Leather Armor
            138674, -- Ensemble: Grievous Gladiator's Leather Armor
            138675, -- Ensemble: Grievous Gladiator's Silk Armor
            138676, -- Ensemble: Grievous Gladiator's Silk Armor
            138677, -- Ensemble: Grievous Gladiator's Satin Armor
            138678, -- Ensemble: Grievous Gladiator's Satin Armor
            138679, -- Ensemble: Grievous Gladiator's Felweave Armor
            138681, -- Ensemble: Tyrannical Gladiator's Plate Armor
            138682, -- Ensemble: Tyrannical Gladiator's Plate Armor
            138683, -- Ensemble: Tyrannical Gladiator's Dreadplate Armor
            138684, -- Ensemble: Tyrannical Gladiator's Dreadplate Armor
            138685, -- Ensemble: Tyrannical Gladiator's Scaled Armor
            138686, -- Ensemble: Tyrannical Gladiator's Scaled Armor
            138687, -- Ensemble: Tyrannical Gladiator's Ringmail Armor
            138688, -- Ensemble: Tyrannical Gladiator's Ringmail Armor
            138689, -- Ensemble: Tyrannical Gladiator's Chain Armor
            138690, -- Ensemble: Tyrannical Gladiator's Chain Armor
            138691, -- Ensemble: Tyrannical Gladiator's Dragonhide Armor
            138692, -- Ensemble: Tyrannical Gladiator's Dragonhide Armor
            138693, -- Ensemble: Tyrannical Gladiator's Ironskin Armor
            138694, -- Ensemble: Tyrannical Gladiator's Ironskin Armor
            138695, -- Ensemble: Tyrannical Gladiator's Leather Armor
            138696, -- Ensemble: Tyrannical Gladiator's Leather Armor
            138697, -- Ensemble: Tyrannical Gladiator's Silk Armor
            138698, -- Ensemble: Tyrannical Gladiator's Silk Armor
            138699, -- Ensemble: Tyrannical Gladiator's Satin Armor
            138700, -- Ensemble: Tyrannical Gladiator's Satin Armor
            138701, -- Ensemble: Tyrannical Gladiator's Felweave Armor
            138702, -- Ensemble: Tyrannical Gladiator's Felweave Armor
            138703, -- Ensemble: Malevolent Gladiator's Plate Armor
            138704, -- Ensemble: Malevolent Gladiator's Dreadplate Armor
            138705, -- Ensemble: Malevolent Gladiator's Scaled Armor
            138706, -- Ensemble: Malevolent Gladiator's Ringmail Armor
            138707, -- Ensemble: Malevolent Gladiator's Chain Armor
            138708, -- Ensemble: Malevolent Gladiator's Dragonhide Armor
            138709, -- Ensemble: Malevolent Gladiator's Ironskin Armor
            138710, -- Ensemble: Malevolent Gladiator's Leather Armor
            138711, -- Ensemble: Malevolent Gladiator's Silk Armor
            138712, -- Ensemble: Malevolent Gladiator's Satin Armor
            138713, -- Ensemble: Malevolent Gladiator's Felweave Armor
            143826, -- Ensemble: Grievous Gladiator's Felweave Armor
            144243, -- Arsenal: Malevolent Gladiator's Weapons
            144245, -- Arsenal: Tyrannical Gladiator's Weapons
            144246, -- Arsenal: Tyrannical Gladiator's Weapons
            144248, -- Arsenal: Prideful Gladiator's Weapons
            144250, -- Arsenal: Prideful Gladiator's Weapons
            144251, -- Arsenal: Grievous Gladiator's Weapons
            144252, -- Arsenal: Grievous Gladiator's Weapons
            146439, -- Ensemble: Cataclysmic Gladiator's Chain Armor
            146441, -- Ensemble: Cataclysmic Gladiator's Dragonhide Armor
            146443, -- Ensemble: Cataclysmic Gladiator's Dreadplate Armor
            146445, -- Ensemble: Cataclysmic Gladiator's Felweave Armor
            146447, -- Ensemble: Cataclysmic Gladiator's Leather Armor
            146449, -- Ensemble: Cataclysmic Gladiator's Plate Armor
            146451, -- Ensemble: Cataclysmic Gladiator's Ringmail Armor
            146453, -- Ensemble: Cataclysmic Gladiator's Satin Armor
            146455, -- Ensemble: Cataclysmic Gladiator's Scaled Armor
            146457, -- Ensemble: Cataclysmic Gladiator's Silk Armor
            146499, -- Ensemble: Ruthless Gladiator's Chain Armor
            146501, -- Ensemble: Ruthless Gladiator's Dragonhide Armor
            146503, -- Ensemble: Ruthless Gladiator's Dreadplate Armor
            146505, -- Ensemble: Ruthless Gladiator's Felweave Armor
            146507, -- Ensemble: Ruthless Gladiator's Leather Armor
            146509, -- Ensemble: Ruthless Gladiator's Plate Armor
            146511, -- Ensemble: Ruthless Gladiator's Ringmail Armor
            146513, -- Ensemble: Ruthless Gladiator's Satin Armor
            146515, -- Ensemble: Ruthless Gladiator's Scaled Armor
            146517, -- Ensemble: Ruthless Gladiator's Silk Armor
            146519, -- Ensemble: Vicious Gladiator's Chain Armor
            146521, -- Ensemble: Vicious Gladiator's Dragonhide Armor
            146523, -- Ensemble: Vicious Gladiator's Dreadplate Armor
            146525, -- Ensemble: Vicious Gladiator's Felweave Armor
            146527, -- Ensemble: Vicious Gladiator's Leather Armor
            146529, -- Ensemble: Vicious Gladiator's Plate Armor
            146531, -- Ensemble: Vicious Gladiator's Ringmail Armor
            146533, -- Ensemble: Vicious Gladiator's Satin Armor
            146535, -- Ensemble: Vicious Gladiator's Scaled Armor
            146537, -- Ensemble: Vicious Gladiator's Silk Armor
            146639, -- Arsenal: Cataclysmic Gladiator's Weapons
            146640, -- Arsenal: Ruthless Gladiator's Weapons
            146641, -- Arsenal: Vicious Gladiator's Weapons
            146538, -- Ensemble: Deadly Gladiator's Chain Armor
            146539, -- Ensemble: Deadly Gladiator's Dragonhide Armor
            146540, -- Ensemble: Deadly Gladiator's Dreadplate Armor
            146541, -- Ensemble: Deadly Gladiator's Felweave Armor
            146542, -- Ensemble: Deadly Gladiator's Leather Armor
            146543, -- Ensemble: Deadly Gladiator's Plate Armor
            146544, -- Ensemble: Deadly Gladiator's Ringmail Armor
            146545, -- Ensemble: Deadly Gladiator's Satin Armor
            146546, -- Ensemble: Deadly Gladiator's Scaled Armor
            146547, -- Ensemble: Deadly Gladiator's Silk Armor
            146548, -- Ensemble: Furious Gladiator's Chain Armor
            146549, -- Ensemble: Furious Gladiator's Dragonhide Armor
            146550, -- Ensemble: Furious Gladiator's Dreadplate Armor
            146551, -- Ensemble: Furious Gladiator's Felweave Armor
            146552, -- Ensemble: Furious Gladiator's Leather Armor
            146553, -- Ensemble: Furious Gladiator's Plate Armor
            146554, -- Ensemble: Furious Gladiator's Ringmail Armor
            146555, -- Ensemble: Furious Gladiator's Satin Armor
            146556, -- Ensemble: Furious Gladiator's Scaled Armor
            146557, -- Ensemble: Furious Gladiator's Silk Armor
            146558, -- Ensemble: Hateful Gladiator's Chain Armor
            146559, -- Ensemble: Hateful Gladiator's Dragonhide Armor
            146560, -- Ensemble: Hateful Gladiator's Dreadplate Armor
            146561, -- Ensemble: Hateful Gladiator's Felweave Armor
            146562, -- Ensemble: Hateful Gladiator's Leather Armor
            146563, -- Ensemble: Hateful Gladiator's Plate Armor
            146564, -- Ensemble: Hateful Gladiator's Ringmail Armor
            146565, -- Ensemble: Hateful Gladiator's Satin Armor
            146566, -- Ensemble: Hateful Gladiator's Scaled Armor
            146567, -- Ensemble: Hateful Gladiator's Silk Armor
            146568, -- Ensemble: Relentless Gladiator's Chain Armor
            146569, -- Ensemble: Relentless Gladiator's Dragonhide Armor
            146570, -- Ensemble: Relentless Gladiator's Dreadplate Armor
            146571, -- Ensemble: Relentless Gladiator's Felweave Armor
            146572, -- Ensemble: Relentless Gladiator's Leather Armor
            146573, -- Ensemble: Relentless Gladiator's Plate Armor
            146574, -- Ensemble: Relentless Gladiator's Ringmail Armor
            146575, -- Ensemble: Relentless Gladiator's Satin Armor
            146576, -- Ensemble: Relentless Gladiator's Scaled Armor
            146577, -- Ensemble: Relentless Gladiator's Silk Armor
            146588, -- Ensemble: Wrathful Gladiator's Chain Armor
            146589, -- Ensemble: Wrathful Gladiator's Dragonhide Armor
            146590, -- Ensemble: Wrathful Gladiator's Dreadplate Armor
            146591, -- Ensemble: Wrathful Gladiator's Felweave Armor
            146592, -- Ensemble: Wrathful Gladiator's Leather Armor
            146593, -- Ensemble: Wrathful Gladiator's Plate Armor
            146594, -- Ensemble: Wrathful Gladiator's Ringmail Armor
            146595, -- Ensemble: Wrathful Gladiator's Satin Armor
            146596, -- Ensemble: Wrathful Gladiator's Scaled Armor
            146597, -- Ensemble: Wrathful Gladiator's Silk Armor
            146642, -- Arsenal: Wrathful Gladiator's Weapons
            146643, -- Arsenal: Relentless Gladiator's Weapons
            146644, -- Arsenal: Furious Gladiator's Weapons
            146645, -- Arsenal: Deadly Gladiator's Weapons
            146646, -- Arsenal: Hateful Gladiator's Weapons
            146647, -- Arsenal: Savage Gladiator's Weapons
            138381, -- Ensemble: Brutal Gladiator's Plate
            146598, -- Ensemble: Brutal Gladiator's Chain Armor
            146599, -- Ensemble: Brutal Gladiator's Dragonhide Armor
            146600, -- Ensemble: Brutal Gladiator's Dreadplate Armor
            146601, -- Ensemble: Brutal Gladiator's Felweave Armor
            146602, -- Ensemble: Brutal Gladiator's Leather Armor
            146603, -- Ensemble: Brutal Gladiator's Plate Armor
            146604, -- Ensemble: Brutal Gladiator's Ringmail Armor
            146605, -- Ensemble: Brutal Gladiator's Satin Armor
            146606, -- Ensemble: Brutal Gladiator's Scaled Armor
            146607, -- Ensemble: Brutal Gladiator's Silk Armor
            146608, -- Ensemble: Gladiator's Chain Armor
            146609, -- Ensemble: Gladiator's Dragonhide Armor
            146611, -- Ensemble: Gladiator's Felweave Armor
            146612, -- Ensemble: Gladiator's Leather Armor
            146613, -- Ensemble: Gladiator's Plate Armor
            146614, -- Ensemble: Gladiator's Ringmail Armor
            146615, -- Ensemble: Gladiator's Satin Armor
            146616, -- Ensemble: Gladiator's Scaled Armor
            146617, -- Ensemble: Gladiator's Silk Armor
            146618, -- Ensemble: Merciless Gladiator's Chain Armor
            146619, -- Ensemble: Merciless Gladiator's Dragonhide Armor
            146621, -- Ensemble: Merciless Gladiator's Felweave Armor
            146622, -- Ensemble: Merciless Gladiator's Leather Armor
            146623, -- Ensemble: Merciless Gladiator's Plate Armor
            146624, -- Ensemble: Merciless Gladiator's Ringmail Armor
            146625, -- Ensemble: Merciless Gladiator's Satin Armor
            146626, -- Ensemble: Merciless Gladiator's Scaled Armor
            146627, -- Ensemble: Merciless Gladiator's Silk Armor
            146628, -- Ensemble: Vengeful Gladiator's Chain Armor
            146629, -- Ensemble: Vengeful Gladiator's Dragonhide Armor
            146631, -- Ensemble: Vengeful Gladiator's Felweave Armor
            146632, -- Ensemble: Vengeful Gladiator's Leather Armor
            146633, -- Ensemble: Vengeful Gladiator's Plate Armor
            146634, -- Ensemble: Vengeful Gladiator's Ringmail Armor
            146635, -- Ensemble: Vengeful Gladiator's Satin Armor
            146636, -- Ensemble: Vengeful Gladiator's Scaled Armor
            146637, -- Ensemble: Vengeful Gladiator's Silk Armor
            146648, -- Arsenal: Brutal Gladiator's Weapons
            146649, -- Arsenal: Vengeful Gladiator's Weapons
            146650, -- Arsenal: Merciless Gladiator's Weapons
            146651, -- Arsenal: Gladiator's Weapons
            131732, -- Purple Hills of Mac'Aree
            141860, -- Ingram's Puzzle
            151550, -- Time-Lost Keepsake Box
            151551, -- Time-Lost Keepsake Box
            151552, -- Time-Lost Keepsake Box
            151553, -- Time-Lost Keepsake Box
            151554, -- Time-Lost Keepsake Box
            147294, -- Bone-Wrought Coffer of the Damned
            147295, -- Demonslayer's Soul-Sealed Satchel
            147296, -- Living Root-Bound Cache
            147297, -- Deepwood Ranger's Quiver
            147298, -- Spell-Secured Pocket of Infinite Depths
            147299, -- Hand-Carved Jade Puzzle Box
            147300, -- Light-Bound Reliquary
            147301, -- Coffer of Twin Faiths
            147302, -- Hollow Skeleton Key
            147303, -- Giant Elemental's Closed Stone Fist
            147304, -- Pocket Keystone to Abandoned World
            147305, -- Stalwart Champion's War Chest
            150372, -- Arsenal: The Warglaives of Azzinoth
            138416, -- Xavier's Curiosity
            139536, -- Emanation of the Winds
            139546, -- Twisting Anima of Souls
            139547, -- Runes of the Darkening
            139548, -- The Bonereaper's Hook
            139549, -- Guise of the Deathwalker
            139550, -- Bulwark of the Iron Warden
            139551, -- The Sunbloom
            139552, -- Feather of the Moonspirit
            139553, -- Mark of the Glade Guardian
            139554, -- Acorn of the Endless
            139555, -- Designs of the Grand Architect
            139556, -- Syriel Crescentfall's Notes: Ravenguard
            139557, -- Last Breath of the Forest
            139558, -- The Woolomancer's Charge
            139559, -- The Stars' Design
            139560, -- Everburning Crystal
            139561, -- Legend of the Monkey King
            139562, -- Breath of the Undying Serpent
            139563, -- The Stormfist
            139564, -- Lost Edicts of the Watcher
            139565, -- Spark of the Fallen Exarch
            139566, -- Heart of Corruption
            139567, -- Writings of the End
            139568, -- Staff of the Lightborn
            139569, -- Claw of N'Zoth
            139570, -- The Cypher of Broken Bone
            139571, -- Tome of Otherworldly Venoms
            139572, -- Lost Codex of the Amani
            139573, -- The Warmace of Shirvallah
            139574, -- Coil of the Drowned Queen
            139575, -- Essence of the Executioner
            139576, -- Visage of the First Wakener
            139577, -- The Burning Jewel of Sargeras
            139578, -- The Arcanite Bladebreaker
            139579, -- The Dragonslayers
            139580, -- Burning Plate of the Worldbreaker
            140452, -- Crest of Heroism
            140453, -- Crest of Carnage
            140454, -- Crest of Devastation
            140455, -- Crest of Heroism
            140456, -- Crest of Carnage
            140457, -- Crest of Devastation
            140652, -- Seed of Solar Fire
            140653, -- Pure Drop of Shaladrassil's Sap
            140656, -- Rod of the Ascended
            140657, -- Crest of the Lightborn
            152583, -- Underlight Emerald
            141335, -- Lost Research Notes
            144395, -- Artifact Research Synopsis
            144436, -- Lost Legend of Odyn
            144437, -- Lost Legend of the Valarjar
            142156, -- Order Resources Cache
            150737, -- Abundant Order Resources Cache
            151385, -- Arne Test Heirloom - 110 Timeworn Heirloom Scabbard
            151614, -- Weathered Heirloom Armor Casing
            151615, -- Weathered Heirloom Scabbard
            151379, -- Arne Test Heirloom - Timeworn Heirloom Scabbard
            151378  -- Arne Test Heirloom - Ancient Heirloom Scabbard
        }
    },
    [addon.CONS.CURRENCY_ID] = {
        137642  -- Mark of Honor
    },
    [addon.CONS.GEMS_ID] = {
        130215, -- Deadly Deep Amber
        130216, -- Quick Azsunite
        130217, -- Versatile Skystone
        130218, -- Masterful Queen's Opal
        130219, -- Deadly Eye of Prophecy
        130220, -- Quick Dawnlight
        130221, -- Versatile Maelstrom Sapphire
        130222, -- Masterful Shadowruby
        130246, -- Saber's Eye of Strength
        130247, -- Saber's Eye of Agility
        130248, -- Saber's Eye of Intellect
        151580, -- Deadly Deep Chemirine
        151583, -- Quick Lightsphene
        151584, -- Masterful Argulite
        151585,  -- Versatile Labradorite
        [addon.CONS.G_RELIC_ID] = {
            146734, -- 7.2 QA Combat Test Relic Holy 1
            146735, -- 7.2 QA Combat Test Relic Arcane 1
            146736, -- 7.2 QA Combat Test Relic Blood 1
            146737, -- 7.2 QA Combat Test Relic Fel 1
            146738, -- 7.2 QA Combat Test Relic Fire 1
            146739, -- 7.2 QA Combat Test Relic Frost 1
            146740, -- 7.2 QA Combat Test Relic Iron 1
            146741, -- 7.2 QA Combat Test Relic Life 1
            146742, -- 7.2 QA Combat Test Relic Shadow 1
            146743, -- 7.2 QA Combat Test Relic Wind 1
            140041, -- Shadow Dew
            140042, -- Ancient Sacred Candle
            140043, -- Soul Forge Ember
            140044, -- Felbat Heart
            140045, -- Fel-Cleansed Leystone Key
            140047, -- Tincture of Arcwine
            140079, -- Stormforged Fist
            140080, -- Everchill Pearl
            140086, -- Intact Infernal Palm
            140088, -- Everblooming Sunflower
            141254, -- Mote of Fear
            141255, -- Mockery of Life
            141256, -- Promise of Rebirth
            141257, -- Roar of the Ocean
            141258, -- Whirlpool Seed
            141259, -- Seawitch's Foci
            141260, -- Shieldmaiden's Prayer
            141261, -- Fires of Heaven
            141262, -- Rune-Etched Quill
            141263, -- Stonedark Idol
            141264, -- Bitestone Fury
            141265, -- Gift of Flame
            141266, -- Manawracked Charm
            141267, -- Aspect of Disregard
            141268, -- Swordsinger's Counterweight
            141269, -- Tranquil Clipping
            141270, -- Restless Dreams
            141271, -- Hope of the Forest
            141272, -- Mana-Saber Eye
            141273, -- Echo of Eons
            141274, -- Frozen Ley Scar
            141275, -- Fertile Soil
            141276, -- Vision of An'she
            141277, -- Bloodtotem Brand
            141278, -- Glaivemaster's Whetstone
            141279, -- Prison Guard's Torchflame
            141280, -- Demonic Shackles
            141281, -- Legion Portalstone
            141282, -- Netherwhisper Arcanum
            141283, -- Felbat Razorfang
            141284, -- Nor'danil Ampoule
            141285, -- Nar'thalas Writ
            141286, -- Rite of the Val'kyr
            141287, -- Law of Strength
            141288, -- Ettin Bone Fragment
            141289, -- Corruption of the Bloodtotem
            141290, -- Dreamgrove Sproutling
            141291, -- Shala'nir Sproutling
            141292, -- Crystallizing Mana
            141293, -- Spellfire Oil
            143682, -- Viscous Terror
            143683, -- Rampant Wildfire
            143684, -- Slavemaster's Malevolence
            143685, -- Strength of the Nazjatar
            143686, -- Hatecoil Badge of Glory
            143687, -- Felskorn Mania
            143688, -- Stormwing's Roar
            143689, -- Weight of Tradition
            143690, -- Specter of Helheim
            143691, -- Bloody Kvaldir Pennant
            143692, -- Earthshaper's Mantra
            143693, -- Darkened Brulstone
            143694, -- Aristocratic Menace
            143695, -- Ambition of the Forlorn
            143696, -- Perilous Bargain
            143697, -- Warpblade Flux
            143698, -- Effervescent Leyblossom
            143699, -- Temporal Blossoming
            143700, -- Stoic Hibernation
            143701, -- Ancient Flamewaking
            143702, -- Eternity of Wisdom
            143703, -- Snowmane Totem
            143704, -- Harpybane Fetish
            143705, -- Conquered Summit
            135565, -- Battle-Touched Elemental Spark
            135568, -- Battle-Touched Blood of the Fallen
            135569, -- Battle-Touched Infernal Shard
            135570, -- Battle-Touched Helfrost
            135571, -- Battle-Touched Ember
            135572, -- Battle-Touched Martyr Stone
            135573, -- Battle-Touched Chain Link
            135574, -- Battle-Touched Blossom
            135576, -- Battle-Touched Fetish
            135577, -- Battle-Touched Dewdrop
            135578, -- Battle-Touched Banner
            132279, -- Cleansed Shrine Relic
            132280, -- Lost Priestess' Loop
            132281, -- Lunarwing Crystal
            132282, -- Enchanted Pool Garnet
            132283, -- Uncorrupted Val Blood
            132284, -- Preserved Blood-Stained Claw
            132285, -- Small Nightmare Totem
            132286, -- Felshroom
            132287, -- Firewater Essence
            132288, -- Trickster's Everburning Flames
            132289, -- Vale Shadow Frost
            132290, -- Frozen Moss of the Den
            132294, -- Stone of the Dream Den
            132295, -- Petrified Ancient Bark
            132296, -- Val'sharah Seed Pods
            132297, -- Everblooming Flower
            132298, -- Nightmare Cave Moss
            132299, -- Satyr's Nightmare Fetish
            132300, -- Firewater Infusion
            132301, -- Vial of Pure Vale Water
            132302, -- Rustling of the Forest
            132303, -- Enchanted Stoneblood Feather
            132305, -- Elothir's Sympathy
            132306, -- Varethos' Fortitude
            132307, -- Twisted Branch of Shaladrassil
            132308, -- Shaladrassil's Anger
            132309, -- Rimed Worldtree Blossom
            132310, -- Uncorrupted Soil
            132311, -- Blossom of Promise
            132312, -- Twisted Nightmare Totem
            132313, -- Moonrest Dewdrop
            132314, -- Desiccated Breeze
            132316, -- Mana-Fused Seedling
            132317, -- Sap of the Worldtree
            132318, -- Corrupted Grovewalker Core
            132319, -- Charred Imp Claw
            132320, -- Varethos' Frozen Heart
            132321, -- Petrified Ancient Branch
            132322, -- Lifelink to Elothir
            132323, -- Varethos' Last Breath
            132324, -- Purged Decanter
            132325, -- Nightmare Zephyr
            132775, -- Blessing of the Watchers
            132776, -- Yotnar's Pride
            132777, -- Offering of Spilled Blood
            132778, -- Skovald's Betrayal
            132779, -- Pillaged Honor
            132780, -- Grasp of the God-King
            132781, -- Archived Record of Might
            132782, -- Yotnar's Gratitude
            132783, -- Echo of Aggramar
            132784, -- Sorrow of Thwarted Champions
            132785, -- Whispers of the Thorignir
            132786, -- Archived Record of Valor
            132787, -- Vault Guardian Core
            132788, -- Yotnar's Turmoil
            132789, -- Twisted Tideskorn Rune
            132790, -- Rumblehoof's Flameheart
            132791, -- Archived Record of Will
            132792, -- Tideskorn War Brand
            132793, -- Spark of Will
            132794, -- Runetwister Talisman
            132795, -- Havi's Special Chowder
            132796, -- Breath of the Vault
            132799, -- Thrymjaris' Grace
            132800, -- Drekirjar Lifeblood
            132801, -- Soul of Azariah
            132802, -- Hrydshal Forgeflame
            132803, -- Thorim's Peak Snowcap
            132804, -- Sigil of Hrydshal
            132805, -- Stormborn Courage
            132806, -- Azariah's Last Moments
            132807, -- Mountainside Downpour
            132808, -- Breath of Vethir
            132810, -- Erratic Stormforce
            132811, -- Crystallized Tideskorn Cruelty
            132812, -- Ritual Binding Stone
            132813, -- Thrymjaris' Fury
            132814, -- Drekirjar Jarl's Disdain
            132815, -- Storm-Charged Lodestone
            132816, -- Hrydshal Weald
            132817, -- Shade of Thorim's Peak
            132818, -- Raging Tempest Crux
            132819, -- Thorignir Slipstream
            132983, -- Faronaar Prayer Beads
            132984, -- Abandoned Highborne Mana Crystal
            132985, -- Blood of the Vanquished Highborne
            132986, -- Pulsing Infernal Shard
            132987, -- Everburning Ruin Ember
            132988, -- The Dreadlord's Chill Eye
            132989, -- Legion Iron Nugget
            132990, -- Fel-Resistant Clipping
            132991, -- Mortiferous' Corruption
            132992, -- Fel-Boiled Water Sample
            132993, -- Nethrandamus' Zephyr
            132994, -- Lost Grace of the Nightborne
            132995, -- Faronaar Arcane Power-Core
            132996, -- Cursed Felstalker Flesh
            132997, -- Legion Camp Reactor Core
            132998, -- The Sufferer's Fury
            132999, -- Preserved Highborne Warrior's Fist
            133000, -- Stalwart Faronaar Keystone
            133001, -- Soul Fragment of a Faronaar Innocent
            133002, -- Coalesced Shadows
            133003, -- Blessed Faronaar Holy Water
            133004, -- Swirling Demonic Whispers
            133006, -- Blessed Llothien Stone
            133007, -- Everlasting Construct Core
            133008, -- Azurewing Blood
            133009, -- Corrupted Ley-Crystal
            133010, -- Azure Flame
            133011, -- Leyhollow Frost
            133012, -- Senegos' Resolve
            133013, -- Reinvigorating Crystal
            133014, -- Senegos' Despair
            133015, -- Ley-Pool Essence
            133016, -- Azurewing Guile
            133029, -- Dathrohan's Signet
            133030, -- 'Procured' Kirin Tor Wand Tip
            133031, -- Rare White Tiger Heart
            133032, -- Fel-Fire Demon Claw
            133033, -- Sorceror's Ember
            133034, -- Were-Yeti Paw
            133035, -- Myzrael Shard
            133036, -- Ravenous Seed
            133037, -- Zandalari Voodoo Totem
            133038, -- Vale Waters
            133039, -- Breath of Al'Akir
            133040, -- Teardrop of Elune
            133041, -- Radiating Ley Crystal
            133042, -- Ley-Infused Blood
            133043, -- Fel-Touched Mana Gems
            133044, -- Stellagosa's Rage
            133045, -- Senegos' Breath
            133046, -- Fortified Ancient Dragonscale
            133047, -- Crystallized Whelp Egg
            133048, -- Wretched Draining Essence
            133049, -- Crystalline Flow
            133050, -- Gale of the Blues
            133070, -- Empowered Lifespring Crystal
            133071, -- Rivermanes' Sacrifice
            133072, -- Dargrul's Arrogance
            133073, -- Jale's Fury
            133074, -- Snow of the Earthmother
            133075, -- Crageater Heart
            133076, -- Lifespring Mushroom
            133077, -- Vestiges of Gelmogg
            133078, -- Jale's Pride
            133079, -- Jale's Relief
            133081, -- Manathirster Focus
            133082, -- Gelmogg's Petrified Heart
            133083, -- Fel-Touched Riverstone
            133084, -- Drogbar Kindling
            133085, -- Whitewater Lake Ice
            133086, -- Gelmogg's Fractured Skull
            133087, -- Whitewater Carp Eggs
            133088, -- Creel's Sorrow
            133089, -- Whitewater Froth
            133090, -- Whitewater Breeze
            133092, -- Honor of the Skyhorn
            133093, -- Crawliac Charming Draught
            133094, -- Heart of the Witchqueen
            133095, -- Crawliac Death Scream
            133096, -- Scorched Skyhorn Shawl
            133097, -- Haglands Ice Shard
            133098, -- Rockcrawler Jaw
            133099, -- Darkfeather Seedling
            133100, -- Crawliac Hexrune
            133101, -- Haglands Snowmelt
            133102, -- Julan's Suppressing Wind
            133103, -- Lasan's Hope
            133104, -- Errant Mana
            133105, -- Skyhorn Survivalist's Blood
            133106, -- Fleshrender Roc Essence
            133107, -- Cinderwitch Flame-Song
            133108, -- Frosted Great Eagle Egg
            133109, -- Lasan's Determination
            133110, -- Hex-Cleansed Charm
            133111, -- Vengeful Skyhorn Spirit
            133112, -- Tears of the Skyhorn
            133113, -- Skyhorn Eagle Feather
            151005, -- Dram of Light
            151006, -- Augari Manuscript
            151007, -- Ur'zul Ichor
            151008, -- Vilefiend Fang
            151009, -- Annihilan Heart
            151010, -- Narouan Fin
            151011, -- Stygian Munition Casing
            151012, -- Marsuul Egg
            151013, -- Ethereal Anchor
            151014, -- Fulminating Arcstone
            136997, -- unused
            132404, -- Package 5 - Water 1
            132423, -- unused
            132425, -- Package 2 - Holy 1
            144446, -- 7.2 QA Functionality Test Relic - Not For Playtesting - Bronze 1 +3
            144447, -- 7.2 QA Functionality Test Relic - Not For Playtesting - Bronze 2 +3
            144448, -- 7.2 QA Functionality Test Relic - Not For Playtesting - Bronze 3 +3
            144449, -- 7.2 QA Functionality Test Relic - Not For Playtesting - Bronze 4 +3
            144450, -- 7.2 QA Functionality Test Relic - Not For Playtesting - Bronze 5 +3
            144451, -- 7.2 QA Functionality Test Relic - Not For Playtesting - Bronze 6 +3
            144452, -- 7.2 QA Functionality Test Relic - Not For Playtesting - Bronze 7 +3
            144453, -- 7.2 QA Functionality Test Relic - Not For Playtesting - Bronze 8 +3
            144454, -- 7.2 QA Functionality Test Relic - Not For Playtesting - Bronze 9 +3
            144227, -- Soul of Frost
            150996, -- Archaic Meditation Crystal
            150997, -- Crystallized Aether
            150998, -- Cragscaler Heart
            150999, -- Wakener's Demonic Focus
            151000, -- Construct Forge Cinder
            151001, -- Phial of Kaarinos Frost
            151002, -- Argussian Wrought Blade
            151003, -- Petrified Seedling
            151004, -- Voidbent Isolon Effigy
            151015, -- Skyfin Tail Barb
            152690, -- Darkfall Arcanum
            152691, -- Doomcaller Heart
            152692, -- Devastator Anchor Crystal
            152693, -- Volatile Devastator Round
            152694, -- Felbound Drudge's Sorrow
            152695, -- Crest of the Grand Army
            152696, -- Manacle of Dominance
            152697, -- Mark of Defiant Survival
            152715, -- Scarwing Talon
            153421, -- Grinning Vilefiend Skull
            140070, -- Deep Cave Ice Crystal
            140039, -- Bottled Stormwing Flame
            140046, -- Residual Fel Essence
            140048, -- Moonsage Fern
            140081, -- Ancient Zandalari Tablet
            140082, -- Staff Head of the Second Elunian Cycle
            140083, -- Sunstrider Sigil
            140084, -- Petrified Devilsaur Fang
            140085, -- Qiraji Idol of Command
            142055, -- Light of the Order
            142056, -- Arcanum of the Order
            142057, -- Heartbeat of the Order
            142058, -- Fel Ward of the Order
            142059, -- Flame of the Order
            142060, -- Icy Core of the Order
            142061, -- Iron Will of the Order
            142062, -- Prosperity of the Order
            142063, -- Dusk of the Order
            142064, -- Gale Wind of the Order
            133682, -- Northern Gale
            133683, -- Seacursed Mist
            133684, -- Screams of the Unworthy
            133685, -- Odyn's Boon
            133686, -- Stormforged Inferno
            133687, -- Fenryr's Bloodstained Fang
            133763, -- Key to the Halls
            133764, -- Ragnarok Ember
            133768, -- Harbaron's Tether
            136717, -- Absolved Ravencrest Brooch
            136718, -- Mark of Varo'then
            136719, -- Curdled Soul Essence
            136720, -- Snapped Emerald Pendant
            136721, -- Mo'arg Eyepatch
            136769, -- Ravencrest's Wrath
            136771, -- Eyir's Blessing
            136778, -- Skovald's Resolve
            136973, -- Burden of Vigilance
            136974, -- Empowerment of Thunder
            137270, -- Howling Echoes
            137272, -- Cruelty of Dantalionax
            137302, -- Misshapen Abomination Heart
            137303, -- Touch of Nightfall
            137307, -- Corrupted Knot
            137308, -- Clotted Sap of the Grove
            137313, -- Roiling Fog
            137316, -- Torch of Shaladrassil
            137317, -- Xavius' Mad Whispers
            137318, -- [addon.CONS.PH] Darkheart Thicket - Boss 4 - Relic FEL
            137323, -- Titanic Rune Ward
            137326, -- Fragmented Meteorite Whetstone
            137327, -- Relinquishing Grip of Helheim
            137328, -- [addon.CONS.PH] Maw of Souls - Boss 3 - Relic BLOOD
            137339, -- Quivering Blightshard Husk
            137340, -- Crystalline Energies
            137346, -- Murmuring Idol
            137347, -- Fragment of Loathing
            137350, -- Monstrous Gluttony
            137351, -- Noxious Entrails
            137358, -- Hate-Sculpted Magma
            137359, -- Pebble of Ages
            137363, -- Bloodied Spearhead
            137365, -- Condensed Saltsea Globule
            137366, -- Gift of the Ocean Empress
            137370, -- Heart of the Sea
            137371, -- Tumultuous Aftershock
            137375, -- Blazing Hydra Flame Sac
            137377, -- Serpentrix's Guile
            137379, -- Tempestbinder's Crystal
            137380, -- Rage of the Tides
            137381, -- Pact of Vengeful Service
            137399, -- Ivanyr's Hunger
            137402, -- Cleansing Isotope
            137403, -- Quarantine Catalyst
            137407, -- Sealed Fel Fissure
            137408, -- Xakal's Determination
            137411, -- Nal'tira's Venom Gland
            137412, -- Fistful of Eyes
            137420, -- Split Second
            137421, -- Accelerating Torrent
            137463, -- Fealty of Nerub
            137464, -- Tendril of Darkness
            137465, -- Festerface's Rotted Gut
            137466, -- Frostwyrm Heart
            137468, -- Bonecrushing Hail
            137469, -- Thorium-Plated Egg
            137470, -- Rocket Chicken Rocket Fuel
            137471, -- Drop of True Blood
            137472, -- Betrug's Vigor
            137473, -- Phase Spider Mandible
            137474, -- Loyalty to the Matriarch
            137475, -- [addon.CONS.PH] Violet Hold - Boss 6 - Relic LIFE
            137476, -- Brand of Tyranny
            137477, -- [addon.CONS.PH] Violet Hold - Boss 7 - Relic STORM
            137478, -- Reflection of Sorrow
            137490, -- Self-Forging Credentials
            137491, -- Felsworn Covenant
            137492, -- Flamewreath Spark
            137493, -- Edge of the First Blade
            137494, -- [addon.CONS.PH] Court of Stars - Boss 3 - Relic IRON
            137495, -- Crux of Blind Faith
            137542, -- Metamorphosis Spark
            137543, -- Soulsap Shackles
            137544, -- Prisoner's Wail
            137545, -- Flashfrozen Ember
            137546, -- Molten Giant's Eye
            137547, -- Pulsing Prism
            137548, -- Elune's Light
            137549, -- Shade of the Vault
            137550, -- Moonglaive Dervish
            144458, -- Agronox's Unsullied Heartwood
            144459, -- Knot of Fel
            144460, -- Fury of the Scorned
            144461, -- Thrashbite's Spite
            144462, -- Fel-Tempered Link
            144463, -- Shard of Kaldorei Stained Glass
            144464, -- Mephistroth's Nail
            144465, -- Essence of the Legion Tempest
            144466, -- Gore-Flecked Feaster Fang
            144467, -- Vial of Eternal Moon
            151288, -- Void-Resistant Seedpod
            151289, -- Badge of the Fallen Vindicator
            151290, -- Darktide Fervor
            151291, -- Frozen Void Shard
            151292, -- Sanctified Eredar Lock
            151293, -- Orb of the Abandoned Magi
            151294, -- Coalesced Void
            151295, -- Darkstorm Arrowhead
            151296, -- Blood of the Vanquished
            151297, -- Carved Argunite Idol
            140049, -- PH - Storm Relic 2 - Unused
            140050, -- PH - Frost Relic 2 - Unused
            140051, -- PH - Shadow Relic 2 - Unused
            140052, -- PH - Holy Relic 2 - Unused
            140053, -- PH - Fire Relic 2 - Unused
            140054, -- PH - Blood Relic 2 - Unused
            140055, -- PH - Iron Relic 2 - Unused
            140056, -- PH - Fel Relic 2 - Unused
            140057, -- PH - Arcane Relic 2 - Unused
            140058, -- PH - Life Relic 2 - Unused
            140059, -- PH - Storm Relic 3 - Unused
            140060, -- PH - Frost Relic 3 - Unused
            140061, -- PH - Shadow Relic 3 - Unused
            140062, -- PH - Holy Relic 3 - Unused
            140063, -- PH - Fire Relic 3 - Unused
            140064, -- PH - Blood Relic 3 - Unused
            140065, -- PH - Iron Relic 3 - Unused
            140066, -- PH - Fel Relic 3 - Unused
            140067, -- PH - Arcane Relic 3 - Unused
            140068, -- PH - Life Relic 3 - Unused
            136683, -- Terrorspike
            136684, -- Gleaming Iron Spike
            136685, -- Consecrated Spike
            136686, -- Flamespike
            136687, -- "The Felic"
            136688, -- Shockinator
            136689, -- Soul Fibril
            136691, -- Immaculate Fibril
            136692, -- Aqual Mark
            136693, -- Straszan Mark
            140411, -- Clarity of Conviction
            140412, -- Reactive Intuition
            140413, -- Grisly Souvenir
            140414, -- Fel-Loaded Dice
            140415, -- Blaze of Glory
            140416, -- Conscience of the Victorious
            140417, -- Battle-Tempered Hilt
            140418, -- "Borrowed" Soul Essence
            140419, -- Blindside Approach
            140420, -- Battering Tempest
            140423, -- Exhaustive Research
            140424, -- Taboo Knowledge
            140425, -- Thirsty Bloodstone
            140426, -- Thrill of Battle
            140427, -- Performance Enhancing Curio
            140428, -- Alliance of Convenience
            140429, -- Flame of the Fallen
            140430, -- Torch of Competition
            140431, -- Superiority's Contempt
            140432, -- Cold Sweat
            140433, -- Brilliant Sunstone
            140434, -- Radiance of Dawn
            140435, -- Unflinching Grit
            140436, -- Steadfast Conviction
            140437, -- Tombweed Bloom
            140438, -- Petrified Ancient's Thumb
            140440, -- Polished Shadowstone
            140441, -- Dead Man's Tale
            140442, -- Thundering Impact
            140443, -- Roar of the Crowd
            143796, -- Clarity of Conviction
            143797, -- Reactive Intuition
            143798, -- Grisly Souvenir
            143799, -- Fel-Loaded Dice
            143800, -- Blaze of Glory
            143801, -- Conscience of the Victorious
            143802, -- Battle-Tempered Hilt
            143803, -- "Borrowed" Soul Essence
            143804, -- Blindside Approach
            143805, -- Battering Tempest
            143806, -- Exhaustive Research
            143807, -- Taboo Knowledge
            143808, -- Thirsty Bloodstone
            143809, -- Thrill of Battle
            143810, -- Performance Enhancing Curio
            143811, -- Alliance of Convenience
            143812, -- Flame of the Fallen
            143813, -- Torch of Competition
            143814, -- Superiority's Contempt
            143815, -- Cold Sweat
            143816, -- Brilliant Sunstone
            143817, -- Radiance of Dawn
            143818, -- Unflinching Grit
            143819, -- Steadfast Conviction
            143820, -- Tombweed Bloom
            143821, -- Petrified Ancient's Thumb
            143822, -- Polished Shadowstone
            143823, -- Dead Man's Tale
            143824, -- Thundering Impact
            143825, -- Roar of the Crowd
            144504, -- Reactive Intuition
            144505, -- Exhaustive Research
            144506, -- Taboo Knowledge
            144507, -- Grisly Souvenir
            144508, -- Thirsty Bloodstone
            144509, -- Thrill of Battle
            144510, -- Fel-Loaded Dice
            144511, -- Performance Enhancing Curio
            144512, -- Alliance of Convenience
            144513, -- Blaze of Glory
            144514, -- Flame of the Fallen
            144515, -- Torch of Competition
            144516, -- Conscience of the Victorious
            144517, -- Superiority's Contempt
            144518, -- Cold Sweat
            144519, -- Clarity of Conviction
            144520, -- Brilliant Sunstone
            144521, -- Radiance of Dawn
            144522, -- Battle-Tempered Hilt
            144523, -- Unflinching Grit
            144524, -- Steadfast Conviction
            144525, -- "Borrowed" Soul Essence
            144526, -- Tombweed Bloom
            144527, -- Petrified Ancient's Thumb
            144528, -- Blindside Approach
            144529, -- Polished Shadowstone
            144530, -- Dead Man's Tale
            144531, -- Battering Tempest
            144532, -- Thundering Impact
            144533, -- Roar of the Crowd
            145346, -- Reactive Intuition
            145347, -- Exhaustive Research
            145348, -- Taboo Knowledge
            145349, -- Grisly Souvenir
            145350, -- Thirsty Bloodstone
            145351, -- Thrill of Battle
            145352, -- Fel-Loaded Dice
            145353, -- Performance Enhancing Curio
            145354, -- Alliance of Convenience
            145355, -- Blaze of Glory
            145356, -- Flame of the Fallen
            145357, -- Torch of Competition
            145358, -- Conscience of the Victorious
            145359, -- Superiority's Contempt
            145360, -- Cold Sweat
            145361, -- Clarity of Conviction
            145362, -- Brilliant Sunstone
            145363, -- Radiance of Dawn
            145364, -- Battle-Tempered Hilt
            145365, -- Unflinching Grit
            145366, -- Steadfast Conviction
            145367, -- "Borrowed" Soul Essence
            145368, -- Tombweed Bloom
            145369, -- Petrified Ancient's Thumb
            145370, -- Blindside Approach
            145371, -- Polished Shadowstone
            145372, -- Dead Man's Tale
            145373, -- Battering Tempest
            145374, -- Thundering Impact
            145375, -- Roar of the Crowd
            150274, -- Reactive Intuition
            150275, -- Exhaustive Research
            150276, -- Taboo Knowledge
            150277, -- Grisly Souvenir
            150278, -- Thirsty Bloodstone
            150279, -- Thrill of Battle
            150280, -- Fel-Loaded Dice
            150281, -- Performance Enhancing Curio
            150282, -- Alliance of Convenience
            150283, -- Blaze of Glory
            150284, -- Flame of the Fallen
            150285, -- Torch of Competition
            150286, -- Conscience of the Victorious
            150287, -- Superiority's Contempt
            150288, -- Cold Sweat
            150289, -- Clarity of Conviction
            150290, -- Brilliant Sunstone
            150291, -- Radiance of Dawn
            150292, -- Battle-Tempered Hilt
            150293, -- Unflinching Grit
            150294, -- Steadfast Conviction
            150295, -- "Borrowed" Soul Essence
            150296, -- Tombweed Bloom
            150297, -- Petrified Ancient's Thumb
            150298, -- Reactive Intuition
            150299, -- Exhaustive Research
            150300, -- Taboo Knowledge
            150301, -- Grisly Souvenir
            150302, -- Thirsty Bloodstone
            150303, -- Thrill of Battle
            150304, -- Fel-Loaded Dice
            150305, -- Performance Enhancing Curio
            150306, -- Alliance of Convenience
            150307, -- Blaze of Glory
            150308, -- Flame of the Fallen
            150309, -- Torch of Competition
            150310, -- Conscience of the Victorious
            150311, -- Superiority's Contempt
            150312, -- Cold Sweat
            150313, -- Clarity of Conviction
            150314, -- Brilliant Sunstone
            150315, -- Radiance of Dawn
            150316, -- Battle-Tempered Hilt
            150317, -- Unflinching Grit
            150318, -- Steadfast Conviction
            150319, -- "Borrowed" Soul Essence
            150320, -- Tombweed Bloom
            150321, -- Petrified Ancient's Thumb
            150323, -- Reactive Intuition
            150324, -- Exhaustive Research
            150325, -- Taboo Knowledge
            150326, -- Grisly Souvenir
            150327, -- Thirsty Bloodstone
            150328, -- Thrill of Battle
            150329, -- Fel-Loaded Dice
            150330, -- Performance Enhancing Curio
            150331, -- Alliance of Convenience
            150332, -- Blaze of Glory
            150333, -- Flame of the Fallen
            150334, -- Torch of Competition
            150335, -- Conscience of the Victorious
            150336, -- Superiority's Contempt
            150337, -- Cold Sweat
            150338, -- Clarity of Conviction
            150339, -- Brilliant Sunstone
            150340, -- Radiance of Dawn
            150341, -- Battle-Tempered Hilt
            150342, -- Unflinching Grit
            150343, -- Steadfast Conviction
            150344, -- "Borrowed" Soul Essence
            150345, -- Tombweed Bloom
            150346, -- Petrified Ancient's Thumb
            150348, -- Blindside Approach
            150349, -- Polished Shadowstone
            150350, -- Dead Man's Tale
            150351, -- Battering Tempest
            150352, -- Thundering Impact
            150353, -- Roar of the Crowd
            150354, -- Blindside Approach
            150355, -- Polished Shadowstone
            150356, -- Dead Man's Tale
            150357, -- Battering Tempest
            150358, -- Thundering Impact
            150359, -- Roar of the Crowd
            150360, -- Blindside Approach
            150361, -- Polished Shadowstone
            150362, -- Dead Man's Tale
            150363, -- Battering Tempest
            150364, -- Thundering Impact
            150365, -- Roar of the Crowd
            153446, -- Reactive Intuition
            153447, -- Exhaustive Research
            153448, -- Taboo Knowledge
            153449, -- Grisly Souvenir
            153450, -- Thirsty Bloodstone
            153451, -- Thrill of Battle
            153452, -- Fel-Loaded Dice
            153453, -- Performance Enhancing Curio
            153454, -- Alliance of Convenience
            153455, -- Blaze of Glory
            153456, -- Flame of the Fallen
            153457, -- Torch of Competition
            153458, -- Conscience of the Victorious
            153459, -- Superiority's Contempt
            153460, -- Cold Sweat
            153461, -- Clarity of Conviction
            153462, -- Brilliant Sunstone
            153463, -- Radiance of Dawn
            153464, -- Battle-Tempered Hilt
            153465, -- Unflinching Grit
            153466, -- Steadfast Conviction
            153467, -- "Borrowed" Soul Essence
            153468, -- Tombweed Bloom
            153469, -- Petrified Ancient's Thumb
            153470, -- Blindside Approach
            153471, -- Polished Shadowstone
            153472, -- Dead Man's Tale
            153473, -- Battering Tempest
            153474, -- Thundering Impact
            153475, -- Roar of the Crowd
            132334, -- Blessed Cup of the Moon
            132335, -- Tower Magi's Eye
            132336, -- The Jailer's Cat Tail
            132337, -- Araxxas's Badge
            132338, -- Everflame Arrowhead
            132339, -- Death's Chill Mirror Shard
            132340, -- Rook Fired Ore
            132341, -- Nourishmoss
            132342, -- Vial of Dormant Shadowswarm
            132343, -- Last Drops of Uncorrupted Water
            132344, -- Guile of the Hold's Sky Terrors
            132345, -- Elune Graced Signet
            132346, -- Small Highborne Figurine
            132347, -- The Interrogator's Vial
            132348, -- Gul'dan's Commission
            132349, -- Inquisitor's Fire-Brand Tip
            132350, -- Defiant Frozen Fist
            132351, -- The Forgemaster's Hammer Head
            132352, -- Revitalizing Incense
            132353, -- Patch of Risen Saber Pelt
            132354, -- Hold Surgeon's Tonic
            132355, -- Wind-Whipped Hold Banner Strip
            132824, -- Helheim Waylight
            132825, -- Val'kyra Boon
            132826, -- Cursed Kvaldir Blood
            132827, -- Gaze of Helya
            132828, -- Helhound Core
            132829, -- Sliver of Helfrost
            132830, -- Cursebinder Chains
            132831, -- Worthy Soul
            132832, -- Bones of Geir
            132833, -- Draught of the Underworld
            132834, -- Wailing Winds
            132844, -- Embrace of the Valkyra
            132845, -- Odyn's Veil
            132846, -- Felbound Plasma
            132847, -- Valgrinn's Heart
            132848, -- Flame of Valhallas
            132849, -- Dravak's Jailing Shard
            132850, -- Stormforged Horn
            132851, -- Sprig of Valhallas
            132852, -- Horn of Helheim
            132853, -- Echo of the Storm
            132854, -- Fel-Tainted Haze
            133018, -- Azsuna Package 3 - Holy 1 - Unused
            133019, -- Jewel of Nar'thalas
            133020, -- Blood of the Snake
            133021, -- Intro to Fel Magic - Pocket Edition
            133022, -- Eternal Mage Flame
            133023, -- Depths Shard Ice Crystal
            133024, -- Oracle's Sharpening Stone
            133025, -- Enchanted El'dranil Frond
            133026, -- Cursed Dissection Blade
            133027, -- Petrified Starfish
            133028, -- Gale of Azshara
            133051, -- Azsuna Package 3 - Holy 2 - Unused
            133052, -- Instructor's Crystal Head
            133053, -- Resilient Skrog Blood
            133054, -- Fel Flame Burner
            133055, -- Azshara's Ire
            133056, -- Azshara's Tempest
            133057, -- Etched Talisman of Nar'thalas
            133058, -- Life-Giving Pearl
            133059, -- Corrupted Farondis House Insignia
            133060, -- Neptulon Waters
            133061, -- Heron's Grace
            133114, -- Valor of the Stonedark
            133115, -- Stonedark Focus
            133116, -- Bloodsinger Essence
            133117, -- Torok's Heart
            133118, -- Demonkindre Fangs
            133119, -- Bloodtotems' Fear
            133120, -- Frag's Core
            133121, -- Instincts of the Elderhorn
            133122, -- Betrayal of the Bloodtotem
            133123, -- Highmountain's Tear
            133124, -- Ironbull's Last Words
            133125, -- Confluence of the Tribes
            133126, -- Captured Time Scar
            133127, -- Blood of Neltharion
            133128, -- Corrupted Earthrender Fragment
            133129, -- Smoldering Crux
            133130, -- Coldscale Basilisk Eyes
            133131, -- Obsidian Reclaimer Scale
            133132, -- Echo of Neltharion
            133133, -- Sha Splinters
            133134, -- Time-Lost Eddy
            133135, -- Pride of Highmountain
            133136, -- Glory of Highmountain
            133137, -- Wisps of Illusion
            133138, -- Time-Lost Dragon Heart
            133139, -- Feltotem Sigil
            133140, -- Time-Lost Dragonfire
            133141, -- Flawless Kun-Lai Blossom
            133142, -- Stonedark Brul Brand
            133143, -- Spark of Khaz'goroth
            133144, -- Memory of Neltharion
            133145, -- Anguish of Princes
            133146, -- The Four Winds
            132254, -- Heart of the Inferno
            152024, -- Fallen Magi's Seerstone
            152025, -- Thu'rayan Lash
            152026, -- Prototype Titan-Disc
            152027, -- Gravitational Condensate
            152028, -- Spurting Reaver Heart
            152029, -- Shivarran Cachabon
            152030, -- Scourge of Perverse Desire
            152031, -- Doomfire Dynamo
            152032, -- Twisted Engineer's Fel-Infuser
            152033, -- Sliver of Corruption
            152034, -- Obliterator Propellant
            152035, -- Blazing Dreadsteed Horseshoe
            152036, -- Hellfire Ignition Switch
            152037, -- Tormentor's Brand
            152038, -- Pyretic Bronze Clasp
            152039, -- Viscous Reaver-Coolant
            152040, -- Frigid Gloomstone
            152041, -- Sublimating Portal Frost
            152042, -- Hoarfrost-Beast Talon
            152043, -- Lightshield Amplifier
            152044, -- Spark of Everburning Light
            152045, -- Venerated Puresoul Idol
            152046, -- Coven Prayer Bead
            152047, -- Ironvine Thorn
            152048, -- Decimator Crankshaft
            152049, -- Fel-Engraved Handbell
            152050, -- Mysterious Petrified Egg
            152051, -- Eidolon of Life
            152052, -- Sporemound Seedling
            152053, -- Essence of the Burgeoning Brood
            152054, -- Unwavering Soul Essence
            152055, -- Kin'garoth's Oil-Sump
            152056, -- Corrupting Dewclaw
            152057, -- Crepuscular Skitterer Egg
            152058, -- Stormcaller's Fury
            152059, -- Whistling Ulna
            152060, -- Neuroshock Electrode
            152061, -- Droplets of the Cleansing Storm
            152092, -- Nathrezim Incisor
            152290, -- Censer of Dark Intent
            152291, -- Fraternal Fervor
            152292, -- Spike of Immortal Command
            152293, -- Fasces of the Endless Legions
            152294, -- Fel Mistress' Brand
            152295, -- Svirax's Grim Trophy
            152344, -- Meto's Orb of Entropy
            152345, -- Vilemus' Bile
            152346, -- Frigid Earring
            152347, -- Occularus' Unblemished Lens
            152348, -- Sotanathor's Thundering Hoof
            155846, -- Miniaturized Cosmic Beacon
            155847, -- Cruor of the Avenger
            155848, -- Condensed Blight Orb
            155849, -- Flickering Ember of Rage
            155850, -- Rime of the Spirit Realm
            155851, -- Reorigination Spark
            155852, -- Volatile Soul Fragment
            155853, -- Conch of the Thunderer
            155854, -- Root of the Lifebinder
            155855, -- Mote of the Forgemaster
            147076, -- Charred Hymnal of the Moon
            147077, -- Inexorable Truth Serum
            147078, -- Mote of Astral Suffusion
            147079, -- Torn Fabric of Reality
            147080, -- Blood of the Unworthy
            147081, -- Pungent Chum
            147082, -- Man'ari Blood Pact
            147084, -- Imploding Infernal Star
            147085, -- Mutated Nautilus
            147086, -- Befouled Effigy of Elune
            147087, -- Ruinous Ashes
            147088, -- Smoldering Thumbscrews
            147089, -- Ferocity of the Devout
            147090, -- Stabilized Extinction Protocol
            147091, -- Cleansing Ignition Catalyst
            147092, -- Ice-Threaded Conch
            147093, -- Globe of Frothing Eddies
            147094, -- Virus of Lethargy
            147095, -- Sphere of Entropy
            147096, -- Inquisition's Master Key
            147097, -- Blessing of the White Lady
            147098, -- Fragment of Grace
            147099, -- Boon of the Prophet
            147100, -- Calcified Barnacle
            147101, -- Chiseled Starlight
            147102, -- Reactive Pylon Casing
            147104, -- Icon of Perverse Animation
            147105, -- Moontalon's Feather
            147106, -- Glowing Prayer Candle
            147107, -- Valorous Spark of Hope
            147108, -- Brand of Relentless Agony
            147109, -- Harjatan's Leering Eye
            147110, -- Grimacing Highborne Skull
            147111, -- Scornful Reflection
            147112, -- Felsoul Vortex
            147113, -- Flawless Hurricane Pearl
            147114, -- Preserved Starlight Incense
            147115, -- Unfurling Origination
            147754, -- Gory Dreadlord Horn
            147755, -- Brutallus's Wretched Heart
            147756, -- Crashing Ember
            147757, -- Globule of Submersion
            147758, -- Beguiling Revelation
            147759, -- Charged Felfire Casing
            147760, -- Apocron's Energy Core
            147761, -- Sandblasted Conch
            151189, -- Tears of the Maiden
            140810, -- Farsight Spiritjewel
            140812, -- Soggy Manascrubber Brush
            140813, -- Arcana Crux
            140815, -- Infused Chitin Fragment
            140816, -- Fingernail of the Fel Brute
            140817, -- Lionshead Lapel Clasp
            140818, -- Foreign Contaminant
            140819, -- Vampiric Fetish
            140820, -- Phial of Fel Blood
            140821, -- Precipice of Eternity
            140822, -- Breath of Dusk
            140823, -- Warchief's Shattered Tusk
            140824, -- Writ of Subjugation
            140825, -- Felfire Pitch
            140826, -- Felstained Jawbone Fragments
            140827, -- Manatoxin Gland
            140831, -- Suspended Nightwell Droplet
            140832, -- Heart of Frost
            140833, -- Sundered Comet
            140834, -- Soul of Flame
            140835, -- Unkindled Ember
            140836, -- Sunflare Coal
            140837, -- Exothermic Core
            140838, -- Construct Personality Sphere
            140839, -- Parasitic Spore
            140840, -- Chittering Mandible
            140841, -- Tempest of the Heavens
            140842, -- Collapsing Epoch
            140843, -- Flickering Timespark
            140844, -- Archaic Nathrezim Keepsake
            140845, -- Glistening Meteorite Shard
            143523, -- Talisman of the Violet Eye
            143524, -- Bones of the Restless
            143525, -- Necrotic Dominion
            143526, -- Searing Cinder
            143527, -- Scale of Arcanagos
            143528, -- Glimpse of the Afterlife
            143529, -- Star of Hollow Spite
            143530, -- Ritual of Animation
            143531, -- Nightmares of the Dead
            143532, -- Echoing Madness
            141514, -- Barnacled Mistcaller Orb
            141515, -- Leystone Nugget
            141516, -- "Liberated" Un'goro Relic
            141517, -- Drugon's Snowglobe
            141518, -- Decaying Dragonfang
            141519, -- Pillaged Titan Disc
            141520, -- Imp-Eye Diamond
            141521, -- Sea Giant Toothpick Fragment
            141522, -- Calamir's Jaw
            141523, -- Fel-Scented Bait
            142176, -- Arcing Static Charge
            142180, -- Grisly Schism
            142181, -- Seeping Corruption
            142182, -- Viz'aduum's Mindstone
            142305, -- Suffused Manapearl
            142306, -- Rift Stabilization Shard
            142307, -- Miniature Bonfire
            142308, -- Ageless Winter
            142309, -- Fauna Analysis Widget
            142310, -- Anthology of Horrors
            146925, -- Mature Morrowsprout
            146926, -- Fel Command Beacon
            146927, -- Inferno Oil
            146928, -- Heat Absorbing Prism
            146929, -- Moonstone Figurine
            146930, -- Pure Arcane Powder
            146931, -- Mephistroth's Rib
            146932, -- Condensed Storm's Fury
            146933, -- Highborne Martyr's Blood
            146934, -- Untouched Holy Candle
            140040, -- Comet Dust
            140069, -- Trueflight Arrow
            140071, -- First Page of the Book of Shadows
            140072, -- Headpiece of the Elunian Cycle
            140073, -- Khadgar's Pocket Warmer
            140074, -- Sin'dorei Blood Gems
            140075, -- Petrified Ancient Bark
            140076, -- Felborne Energist's Gem
            140077, -- Goldleaf Arcwine Phial
            140078, -- Crystalized Leypetal
            142175, -- Arcanum of Weightlessness
            142177, -- Jagged Emerald
            142178, -- Ruffian's Poisoned Blade
            142179, -- Memory of Betrayal
            142183, -- Lava-Quenched Hoofplate
            142184, -- Candle of Flickering Lumens
            142185, -- Fear of Predation
            142186, -- Mrrmgmrl Grmmlmglrg
            142187, -- Virtuous Directive
            142188, -- Spellbound Rose Petal
            142189, -- Perfectly Preserved Apple
            142190, -- Love's Intermission
            142191, -- Dirge of the Hunted
            142192, -- Ghastly Curse
            142193, -- Begrudging Confessions
            142194, -- Gloomy Vortex
            142510, -- Phylactery of Unwilling Servitude
            142511, -- Unforged Titansteel
            142512, -- Accursed Cuspid
            142513, -- Token of the Lightning Keeper
            142514, -- Ravens' Sight
            142515, -- Chilled Incisor
            142516, -- Sizzling Fang
            142517, -- Swell of the Tides
            142518, -- Fury of the Sea
            142519, -- Favor of the Prime Designate
            137008, -- Stormfury Diamond
            138226, -- Nightmare Engulfed Jewel
            138227, -- Entrancing Stone
            138228, -- Bioluminescent Mushroom
            138229, -- Nightmare - Boss 3 - Relic STORM
            139249, -- Shaladrassil's Blossom
            139250, -- Unwaking Slumber
            139251, -- Despoiled Dragonscale
            139252, -- Preserved Worldseed
            139253, -- Fel-Bloated Venom Sac
            139254, -- Shrieking Bloodstone
            139255, -- Scything Quill
            139256, -- Sloshing Core of Hatred
            139257, -- Gore-Drenched Fetish
            139258, -- Radiating Metallic Shard
            139259, -- Cube of Malice
            139260, -- Bloodied Bear Fang
            139261, -- Tuft of Ironfur
            139262, -- Reverberating Femur
            139263, -- Blessing of Cenarius
            139264, -- Uplifting Emerald
            139265, -- Radiant Dragon Egg
            139266, -- Fragment of Eternal Spite
            139267, -- Azsharan Councillor's Clasp
            139268, -- Nightmarish Elm Branch
            139269, -- Crystallized Drop of Eternity
            140828, -- [addon.CONS.PH] 7.0 Raid - Suramar City - Boss 3 - Relic ARCANE Bronze5
            140829, -- [addon.CONS.PH] 7.0 Raid - Suramar City - Boss 4 - Relic ARCANE Bronze1
            140830, -- [addon.CONS.PH] 7.0 Raid - Suramar City - Boss 9 - Relic ARCANE Bronze7
            143584, -- Super Cleansed Grovewalker Core
            143585, -- Ultra Cleansed Grovewalker Core
            131731, -- Glinting Shard of Sciallax
            134076, -- Crystallized Shard of Sciallax
            134077, -- Glowing Shard of Sciallax
            134078, -- Dark Shard of Sciallax
            134079, -- Ardent Shard of Sciallax
            134081, -- Adamant Shard of Sciallax
            151494, -- Reactive Intuition
            151495, -- Exhaustive Research
            151496, -- Taboo Knowledge
            151497, -- Grisly Souvenir
            151498, -- Thirsty Bloodstone
            151499, -- Thrill of Battle
            151500, -- Fel-Loaded Dice
            151501, -- Performance Enhancing Curio
            151502, -- Alliance of Convenience
            151503, -- Blaze of Glory
            151504, -- Flame of the Fallen
            151505, -- Torch of Competition
            151506, -- Conscience of the Victorious
            151507, -- Superiority's Contempt
            151508, -- Cold Sweat
            151509, -- Clarity of Conviction
            151510, -- Brilliant Sunstone
            151511, -- Radiance of Dawn
            151512, -- Battle-Tempered Hilt
            151513, -- Unflinching Grit
            151514, -- Steadfast Conviction
            151515, -- "Borrowed" Soul Essence
            151516, -- Tombweed Bloom
            151517, -- Petrified Ancient's Thumb
            151518, -- Blindside Approach
            151519, -- Polished Shadowstone
            151520, -- Dead Man's Tale
            151521, -- Battering Tempest
            151522, -- Thundering Impact
            151523, -- Roar of the Crowd
            154052, -- Reactive Intuition
            154053, -- Exhaustive Research
            154054, -- Taboo Knowledge
            154055, -- Grisly Souvenir
            154056, -- Thirsty Bloodstone
            154057, -- Thrill of Battle
            154058, -- Fel-Loaded Dice
            154059, -- Performance Enhancing Curio
            154060, -- Alliance of Convenience
            154061, -- Blaze of Glory
            154062, -- Flame of the Fallen
            154063, -- Torch of Competition
            154064, -- Conscience of the Victorious
            154065, -- Superiority's Contempt
            154066, -- Cold Sweat
            154067, -- Clarity of Conviction
            154068, -- Brilliant Sunstone
            154069, -- Radiance of Dawn
            154070, -- Battle-Tempered Hilt
            154071, -- Unflinching Grit
            154072, -- Steadfast Conviction
            154073, -- "Borrowed" Soul Essence
            154074, -- Tombweed Bloom
            154075, -- Petrified Ancient's Thumb
            154076, -- Blindside Approach
            154077, -- Polished Shadowstone
            154078, -- Dead Man's Tale
            154079, -- Battering Tempest
            154080, -- Thundering Impact
            154081, -- Roar of the Crowd
            154082, -- Reactive Intuition
            154083, -- Exhaustive Research
            154084, -- Taboo Knowledge
            154085, -- Grisly Souvenir
            154086, -- Thirsty Bloodstone
            154087, -- Thrill of Battle
            154088, -- Fel-Loaded Dice
            154089, -- Performance Enhancing Curio
            154090, -- Alliance of Convenience
            154091, -- Blaze of Glory
            154092, -- Flame of the Fallen
            154093, -- Torch of Competition
            154094, -- Conscience of the Victorious
            154095, -- Superiority's Contempt
            154096, -- Cold Sweat
            154097, -- Clarity of Conviction
            154098, -- Brilliant Sunstone
            154099, -- Radiance of Dawn
            154100, -- Battle-Tempered Hilt
            154101, -- Unflinching Grit
            154102, -- Steadfast Conviction
            154103, -- "Borrowed" Soul Essence
            154104, -- Tombweed Bloom
            154105, -- Petrified Ancient's Thumb
            154106, -- Blindside Approach
            154107, -- Polished Shadowstone
            154108, -- Dead Man's Tale
            154109, -- Battering Tempest
            154110, -- Thundering Impact
            154111, -- Roar of the Crowd
            155695, -- Reactive Intuition
            155696, -- Exhaustive Research
            155697, -- Taboo Knowledge
            155698, -- Grisly Souvenir
            155699, -- Thirsty Bloodstone
            155700, -- Thrill of Battle
            155701, -- Fel-Loaded Dice
            155702, -- Performance Enhancing Curio
            155703, -- Alliance of Convenience
            155704, -- Blaze of Glory
            155705, -- Flame of the Fallen
            155706, -- Torch of Competition
            155707, -- Conscience of the Victorious
            155708, -- Superiority's Contempt
            155709, -- Cold Sweat
            155710, -- Clarity of Conviction
            155711, -- Brilliant Sunstone
            155712, -- Radiance of Dawn
            155713, -- Battle-Tempered Hilt
            155714, -- Unflinching Grit
            155715, -- Steadfast Conviction
            155716, -- "Borrowed" Soul Essence
            155717, -- Tombweed Bloom
            155718, -- Petrified Ancient's Thumb
            155719, -- Blindside Approach
            155720, -- Polished Shadowstone
            155721, -- Dead Man's Tale
            155722, -- Battering Tempest
            155723, -- Thundering Impact
            155724, -- Roar of the Crowd
            155725, -- Reactive Intuition
            155726, -- Exhaustive Research
            155727, -- Taboo Knowledge
            155728, -- Grisly Souvenir
            155729, -- Thirsty Bloodstone
            155730, -- Thrill of Battle
            155731, -- Fel-Loaded Dice
            155732, -- Performance Enhancing Curio
            155733, -- Alliance of Convenience
            155734, -- Blaze of Glory
            155735, -- Flame of the Fallen
            155736, -- Torch of Competition
            155737, -- Conscience of the Victorious
            155738, -- Superiority's Contempt
            155739, -- Cold Sweat
            155740, -- Clarity of Conviction
            155741, -- Brilliant Sunstone
            155742, -- Radiance of Dawn
            155743, -- Battle-Tempered Hilt
            155744, -- Unflinching Grit
            155745, -- Steadfast Conviction
            155746, -- "Borrowed" Soul Essence
            155747, -- Tombweed Bloom
            155748, -- Petrified Ancient's Thumb
            155749, -- Blindside Approach
            155750, -- Polished Shadowstone
            155751, -- Dead Man's Tale
            155752, -- Battering Tempest
            155753, -- Thundering Impact
            155754, -- Roar of the Crowd
            143586  -- Mega Cleansed Grovewalker Core
        }
    },
    [addon.CONS.GLYPHS_ID] = {
        133796, -- Glyph of Fearsome Metamorphosis
        129017, -- Glyph of Ghostly Fade
        129018, -- Glyph of Fel Imp
        129019, -- Glyph of Sparkles
        129020, -- Glyph of Flash Bang
        129021, -- Glyph of the Sentinel
        129022, -- Glyph of Crackling Ox Lightning
        129028, -- Glyph of Fel Touched Souls
        129029, -- Glyph of Crackling Flames
        136825, -- Glyph of the Feral Chameleon
        136826, -- Glyph of Autumnal Bloom
        137188, -- Glyph of the Blazing Savior
        137191, -- Glyph of the Inquisitor's Eye
        137194, -- Glyph of the Bullseye
        137238, -- Glyph of the Trident
        137239, -- Glyph of the Hook
        137240, -- Glyph of the Headhunter
        137249, -- Glyph of Arachnophobia
        137250, -- Glyph of Nesingwary's Nemeses
        137261, -- Glyph of the Skullseye
        137267, -- Glyph of the Goblin Anti-Grav Flare
        137269, -- Glyph of Stellar Flare
        137274, -- Glyph of Cracked Ice
        137287, -- Glyph of the Spectral Raptor
        137288, -- Glyph of Pebbles
        137289, -- Glyph of Flickering
        137293, -- Glyph of the Queen
        139270, -- Glyph of the Crimson Shell
        139271, -- Glyph of the Chilled Shell
        139272, -- Glyph of the Blood Wraith
        139273, -- Glyph of the Unholy Wraith
        139274, -- Glyph of the Wraith Walker
        139278, -- Glyph of the Forest Path
        139288, -- Glyph of the Dire Stable
        139289, -- Glyph of Critterhex
        139310, -- Glyph of the Shivarra
        139311, -- Glyph of the Voidlord
        139312, -- Glyph of the Observer
        139313, -- Glyph of the Terrorguard
        139314, -- Glyph of the Abyssal
        139315, -- Glyph of Wrathguard
        139338, -- Glyph of Crackling Crane Lightning
        139339, -- Glyph of Yu'lon's Grace
        139348, -- Glyph of Smolder
        139352, -- Glyph of Polymorphic Proportions
        139358, -- Glyph of Blackout
        139362, -- Glyph of Mana Touched Souls
        139417, -- Glyph of Fallow Wings
        139435, -- Glyph of Fel Wings
        139436, -- Glyph of Tattered Wings
        139437, -- Glyph of Fel-Enemies
        139438, -- Glyph of Shadow-Enemies
        139442, -- Glyph of Burnout
        140630, -- Glyph of the Doe
        141898, -- Glyph of Falling Thunder
        143588, -- Glyph of the Trusted Steed
        143750, -- Glyph of Twilight Bloom
        146979, -- Glyph of Blood Grip
        146981, -- Glyph of Frost Grip
        147119, -- Glyph of the Shadow Succubus
        149755, -- Glyph of Angels
        151538, -- Glyph of Ember Shards
        151540, -- Glyph of Floating Shards
        151542, -- Glyph of Fel-Touched Shards
        153031, -- Glyph of the Lightspawn
        153033, -- Glyph of the Voidling
        153036, -- Glyph of Dark Absolution
        147117, -- Orb of the Fel Temptress
        153174, -- Valorous Charger's Bridle
        153175, -- Vengeful Charger's Bridle
        153176, -- Vigilant Charger's Bridle
        153177  -- Golden Charger's Bridle
    },
    [addon.CONS.MISCELLANEOUS_ID] = {
        [addon.CONS.M_HOLIDAY_ID] = {
            139036, -- Ominous Pet Treat
            150366, -- Moonkin Stone
            150735, -- Moonberry
            150747, -- Brilliant Vial
            150748, -- Constellas Corruption
            150749, -- Moonglow Water
            150750, -- Moonkissed Antidote
            147884, -- Tranquil Mechanical Yeti Costume
            150740, -- Beanie Boomie
            138990, -- Exquisite Costume Set: "Grommash"
            151268  -- Exquisite Costume Set: "Xavius"
        },
        [addon.CONS.M_REAGENTS_ID] = {
            136342,
            137622, -- Imp in a Jar
            137623, -- Amulet of Command
            137626, -- Felsurge Web Gland
            138100, -- Pest-Be-Gone Bomb
            128775, -- Mark of Cenarius
            142356, -- Wispbreath Poultice
            147775, -- Nether Portal Disruptor
            138875, -- Small Ley Crystal
            138019  -- Mythic Keystone
        },
        [addon.CONS.M_MOUNTS_ID] = {
            153193, -- Baarut the Brisk
            140228, -- Prestigious Bronze Courser
            140230, -- Prestigious Royal Courser
            140232, -- Prestigious Forest Courser
            140233, -- Prestigious Ivory Courser
            140407, -- Prestigious Midnight Courser
            140408, -- Prestigious Azure Courser
            131734, -- Spirit of Eche'ro
            137570, -- Bloodfang Cocoon
            137574, -- Living Infernal Core
            137575, -- Fiendish Hellfire Core
            137576, -- Dim Coldflame Core
            137577, -- Predatory Bloodgazer
            137578, -- Snowfeather Hunter
            137579, -- Brilliant Direbeak
            137580, -- Viridian Sharptalon
            137614, -- Biting Frostshard Core
            137615, -- Molten Flarecore
            137686, -- Steelbound Harness
            138201, -- Fathom Dweller
            138258, -- Reins of the Long-Forgotten Hippogryph
            138387, -- Ratstallion
            141216, -- Defiled Reins
            141217, -- Reins of the Leyfeather Hippogryph
            141843, -- Vindictive Gladiator's Storm Dragon
            141844, -- Fearless Gladiator's Storm Dragon
            141845, -- Cruel Gladiator's Storm Dragon
            141846, -- Ferocious Gladiator's Storm Dragon
            141847, -- Fierce Gladiator's Storm Dragon
            141848, -- Dominant Gladiator's Storm Dragon
            142224, -- Gift of the Holy Keepers
            142225, -- Ban-lu, Grandmaster's Companion
            142226, -- Trust of a Fierce Wolfhawk
            142227, -- Trust of a Loyal Wolfhawk
            142228, -- Trust of a Dire Wolfhawk
            142231, -- Decaying Reins of the Vilebrood Vanquisher
            142232, -- Iron Reins of the Bloodthirsty War Wyrm
            142233, -- Shadowy Reins of the Accursed Wrathsteed
            142436, -- Arcanist's Manasaber
            142552, -- Smoldering Ember Wyrm
            143489, -- Raging Tempest Totem
            143490, -- Bloody Reins of Dark Portent
            143491, -- Mephitic Reins of Dark Portent
            143492, -- Midnight Black Reins of Dark Portent
            143493, -- Razor-Lined Reins of Dark Portent
            143502, -- Glowing Reins of the Golden Charger
            143503, -- Harsh Reins of the Vengeful Charger
            143504, -- Stoic Reins of the Vigilant Charger
            143505, -- Heraldic Reins of the Valorous Charger
            143637, -- Hellblazing Reins of the Brimstone Wrathsteed
            143638, -- Moon-Kissed Feather
            143643, -- Abyss Worm
            143764, -- Leywoven Flying Carpet
            147804, -- Wild Dreamrunner
            147805, -- Valarjar Stormwing
            147806, -- Cloudwing Hippogryph
            147807, -- Highmountain Elderhorn
            147835, -- Riddler's Mind-Worm
            151623, -- Lucid Nightmare
            152788, -- Lightforged Warframe
            152789, -- Shackled Ur'zul
            152790, -- Vile Fiend
            152791, -- Reins of the Sable Ruinstrider
            152793, -- Reins of the Russet Ruinstrider
            152794, -- Reins of the Amethyst Ruinstrider
            152795, -- Reins of the Beryl Ruinstrider
            152796, -- Reins of the Umber Ruinstrider
            152797, -- Reins of the Cerulean Ruinstrider
            152814, -- Maddened Chaosrunner
            152815, -- Antoran Gloomhound
            152816, -- Antoran Charhound
            152840, -- Scintillating Mana Ray
            152841, -- Felglow Mana Ray
            152842, -- Vibrant Mana Ray
            152843, -- Darkspore Mana Ray
            152844, -- Lambent Mana Ray
            152901, -- Kirin Tor Summoning Crystal
            152903, -- Biletooth Gnasher
            152904, -- Acid Belcher
            152905, -- Crimson Slavermaw
            152912, -- Pond Nettle
            153041, -- Bleakhoof Ruinstrider
            153042, -- Glorious Felcrusher
            153043, -- Blessed Felcrusher
            153044, -- Avenging Felcrusher
            153493, -- Demonic Gladiator's Storm Dragon
            129962, -- Elderhorn Riding Harness
            137573, -- Reins of the Llothien Prowler
            138811, -- Brinedeep Bottom-Feeder
            142403, -- Brawler's Burly Basilisk
            129280, -- Reins of the Prestigious War Steed
            140348, -- Vicious Warstrider
            140350, -- Vicious War Elekk
            140353, -- Vicious Gilnean Warhorse
            140354, -- Vicious War Trike
            141713, -- Arcadian War Turtle
            142234, -- Vicious War Bear
            142235, -- Vicious War Bear
            142236, -- Midnight's Eternal Reins
            142237, -- Vicious War Lion
            142398, -- Darkwater Skate
            142437, -- Vicious War Scorpion
            143648, -- Vicious War Turtle
            143649, -- Vicious War Turtle
            143864, -- Reins of the Prestigious War Wolf
            152869, -- Vicious War Fox
            152870, -- Vicious War Fox
            129744, -- Iron Warhorse
            142369, -- Ivory Hawkstrider
            143631, -- Primal Flamesaber
            143752, -- Sound Test Mount
            147901, -- Luminous Starseeker
            151617, -- Orgrimmar Interceptor
            151618, -- Stormwind Skychaser
            153485, -- Darkmoon Dirigible
            140500  -- Mechanized Lumber Extractor
        },
        [addon.CONS.M_COMPANIONS_ID] = {
            143755, -- Young Venomfang
            129826, -- Nursery Spider
            129960, -- Leather Pet Bed
            129961, -- Flaming Hoop
            144394, -- Tylarr Gronnden
            150742, -- Pet Reaper 0.9
            140670, -- Souvenir Elekk
            140671, -- Souvenir Raptor
            141205, -- Souvenir Murloc
            143754, -- Cavern Moccasin
            129958, -- Leather Pet Leash
            136905, -- Ridgeback Piglet
            136908, -- Thaumaturgical Piglet
            150741, -- Tricorne
            151569, -- Sneaky Marmot
            151633, -- Dig Rat
            131737, -- Wyrmy Tunkins
            101426, -- Micronax Controller
            143756, -- Everliving Spore
            153195, -- Uuna's Doll
            153252, -- Rebellious Imp
            132519, -- Trigger
            142223, -- Sun Darter Hatchling
            142448, -- Albino Buzzard
            151645, -- Model D1-BB-L3R
            146417, -- Sound Test Pet
            128354, -- Grumpy's Leash
            128533, -- Enchanted Cauldron
            128534, -- Enchanted Torch
            128535, -- Enchanted Pen
            128690, -- Ashmaw Cub
            129108, -- Son of Goredome
            129175, -- Crispin
            129178, -- Emmigosa
            129188, -- Bleakwater Jelly
            129208, -- Stormborne Whelpling
            129277, -- Skyhorn Nestling
            129362, -- Broot
            129760, -- Fel Piglet
            129798, -- Plump Jelly
            129878, -- Nightwatch Swooper
            130154, -- Pygmy Owl
            130166, -- Risen Saber Kitten
            130167, -- Thistleleaf Adventurer
            130168, -- Fetid Waveling
            136897, -- Northern Hawk Owl
            136898, -- Fledgling Warden Owl
            136899, -- Extinguished Eye
            136900, -- Hateful Eye
            136901, -- Eye of Inquisition
            136902, -- Toxic Whelpling
            136903, -- Nightmare Whelpling
            136904, -- Sewer-Pipe Jelly
            136906, -- Brown Piglet
            136907, -- Black Piglet
            136910, -- Alarm-o-Bot
            136911, -- Knockoff Blingtron
            136913, -- Red Broodling
            136914, -- Leyline Broodling
            136919, -- Baby Elderhorn
            136920, -- Sunborne Val'kyr
            136921, -- Trigger
            136922, -- Wyrmy Tunkins
            136924, -- Felbat Pup
            136925, -- Corgi Pup
            137298, -- Zoom
            138810, -- Sting Ray Pup
            139775, -- Alliance Enthusiast
            139776, -- Horde Fanatic
            139789, -- Transmutant
            139790, -- Untethered Wyrmling
            139791, -- Lurking Owl Kitten
            140274, -- River Calf
            140316, -- Firebat Pup
            140320, -- Corgnelius
            140323, -- Lagan
            140672, -- Court Scribe
            140741, -- Nightmare Lasher
            140761, -- Nightmare Treant
            140934, -- Benax
            141316, -- Odd Murloc Egg
            141348, -- Wondrous Wisdomball
            141352, -- Rescued Fawn
            141530, -- Snowfang's Trust
            141532, -- Noblegarden Bunny
            141714, -- Igneous Flameling
            141893, -- Mischief
            141894, -- Knight-Captain Murky
            141895, -- Legionnaire Murky
            142083, -- Giant Worm Egg
            142084, -- Magnataur Hunting Horn
            142085, -- Nerubian Relic
            142086, -- Red-Hot Coal
            142087, -- Ironbound Collar
            142088, -- Stormforged Rune
            142089, -- Glittering Ball of Yarn
            142090, -- Ominous Pile of Snow
            142091, -- Blessed Seed
            142092, -- Overcomplicated Controller
            142093, -- Wriggling Darkness
            142094, -- Fragment of Frozen Bone
            142095, -- Remains of a Blood Beast
            142096, -- Putricide's Alchemy Supplies
            142097, -- Skull of a Frozen Whelp
            142098, -- Drudge Remains
            142099, -- Call of the Frozen Blade
            142100, -- Stardust
            142379, -- Dutiful Squire
            142380, -- Dutiful Gruntling
            143679, -- Crackers
            143842, -- Trashy
            143953, -- Infinite Hatchling
            143954, -- Paradox Spirit
            146953, -- Scraps
            147543, -- Son of Skum
            147841, -- Orphaned Felbat
            147900, -- Twilight
            150739, -- Pocket Cannon
            151234, -- Shadow
            151269, -- Naxxy
            151632, -- Mining Monkey
            151828, -- Ageless Bronze Drake
            151829, -- Bronze Proto-Whelp
            152555, -- Ghost Shark
            152963, -- Amalgam of Destruction
            152966, -- Rough-Hewn Remote
            152967, -- Experiment-In-A-Jar
            152968, -- Shadowy Pile of Bones
            152969, -- Odd Twilight Egg
            152970, -- Lesser Circle of Binding
            152972, -- Twilight Summoning Portal
            152973, -- Zephyr's Call
            152974, -- Breezy Essence
            152975, -- Smoldering Treat
            152976, -- Cinderweb Egg
            152977, -- Vibrating Stone
            152978, -- Fandral's Pet Carrier
            152979, -- Puddle of Black Liquid
            152980, -- Elementium Back Plate
            152981, -- Severed Tentacle
            153026, -- Cross Gazer
            153027, -- Orphaned Marsuul
            153040, -- Felclaw Marsuul
            153045, -- Fel Lasher
            153054, -- Docile Skyfin
            153055, -- Fel-Afflicted Skyfin
            153056, -- Grasping Manifestation
            153057, -- Fossorial Bile Larva
            136923, -- Celestial Calf
            140261, -- Hungering Claw
            147539, -- Bloodbrood Whelpling
            147540, -- Frostbrood Whelpling
            147541, -- Vilebrood Whelpling
            147542, -- Ban-Fu, Cub of Ban-Lu
            [addon.CONS.MC_BATTLE_STONE_ID] = {
                137387, -- Immaculate Dragonkin Battle-Stone
                137388, -- Immaculate Humanoid Battle-Stone
                137389, -- Immaculate Undead Battle-Stone
                137390, -- Immaculate Mechanical Battle-Stone
                137391, -- Immaculate Aquatic Battle-Stone
                137392, -- Immaculate Magic Battle-Stone
                137393, -- Immaculate Critter Battle-Stone
                137394, -- Immaculate Beast Battle-Stone
                137395, -- Immaculate Elemental Battle-Stone
                137396, -- Immaculate Flying Battle-Stone
                137627  -- Immaculate Battle-Stone
            },
            [addon.CONS.MC_SUPPLIES_ID] = {
                144345  -- Pile of Pet Goodies
            }
        },
        [addon.CONS.M_TELEPORT_ID] = {
            [addon.CONS.MT_HEARTSTONE_ID] = {
                140192, -- Dalaran Hearthstone
                132517, -- Intra-Dalaran Wormhole Generator
                140493, -- Adept's Guide to Dimensional Rifting
                132523, -- Reaves Battery
                144341, -- Rechargeable Reaves Battery
                138448  -- Emblem of Margoss
            },
            [addon.CONS.MT_JEWELRY_ID] = {
                139599, -- Empowered Ring of the Kirin Tor
                141324, -- Talisman of the Shal'dorei
                142469, -- Violet Seal of the Grand Magus
                144391  -- Pugilist's Powerful Punching Ring
            },
            [addon.CONS.MT_SCROLLS_ID] = {
                139590, -- Scroll of Teleport: Ravenholdt
                141015, -- Scroll of Town Portal: Kal'delar
                141014, -- Scroll of Town Portal: Sashj'tar
                141017, -- Scroll of Town Portal: Lian'tril
                141016, -- Scroll of Town Portal: Faronaar
                141013, -- Scroll of Town Portal: Shala'nir
                142543  -- Scroll of Town Portal (Diablo 3 event)
            },
            [addon.CONS.MT_TOYS_ID] = {
                151652, -- Wormhole Generator: Argus
                142542  -- Tome of Town Portal (Diablo 3 event)
            },
            [addon.CONS.MT_WHISTLE_ID] = {
                141605  -- Flight Master's Whistle
            }
        },
        [addon.CONS.M_ARCHAEOLOGY_ID] = {
            [addon.CONS.MA_CRATES_ID] = {
                142113, -- Crate of Arakkoa Archaeology Fragments
                142114, -- Crate of Draenor Clans Archaeology Fragments
                142115  -- Crate of Ogre Archaeology Fragments
            },
            [addon.CONS.MA_KEY_STONES_ID] = {
                130905, -- Mark of the Deceiver
                130903, -- Ancient Suramar Scroll
                130904  -- Highmountain Ritual-Stone
            },
            [addon.CONS.MA_QUEST_ID] = {
                136362, -- Ancient War Remnants
                130921, -- Pristine Violetglass Vessel
                130922, -- Pristine Inert Leystone Charm
                130923, -- Pristine Quietwine Vial
                130924, -- Pristine Pre-War Highborne Tapestry
                130925, -- Pristine Nobleman's Letter Opener
                130926, -- Pristine Trailhead Drum
                130927, -- Pristine Moosebone Fish-Hook
                130928, -- Pristine Hand-Smoothed Pyrestone
                130929, -- Pristine Drogbar Gem-Roller
                130930, -- Pristine Stonewood Bow
                130931, -- Pristine Imp's Cup
                130932, -- Pristine Flayed-Skin Chronicle
                130933, -- Pristine Malformed Abyssal
                130934, -- Pristine Orb of Inner Chaos
                130935  -- Pristine Houndstooth Hauberk
            }
        },
        [addon.CONS.M_OTHER_ID] = {
            137617, -- Researcher's Notes
            137604, -- Unstable Riftstone
            129746, -- Oddly-Shaped Stomach
            138033, -- Scroll of Command
            138099, -- Skyfire Stone
            138116, -- Throwing Torch
            144298, -- S.E.L.F.I.E. Camera
            134007, -- Eternal Black Diamond Ring
            134004, -- Noble's Eternal Elementium Signet
            123961, -- Recipe List: Leysmithing
            123962, -- Recipe List: Hardened Leystone
            123963, -- Recipe List: Masterwork Demonsteel
            126947, -- Nal'ryssa's Spare Mining Supplies
            127693, -- Wraith Zapper Rifle
            128379, -- Piece of Meat
            128443, -- Cleansed Moonthorn Bundle
            128779, -- Northshire Rations
            128780, -- Shadowglen Rations
            128781, -- Anvilmar Rations
            128782, -- New Tinkertown Rations
            128783, -- Argus Rations
            128784, -- Gilneas Rations
            128785, -- Shang Xi Rations
            128786, -- Sunspire Rations
            128787, -- Kezan Rations
            128788, -- Darkspear Rations
            128789, -- Den Rations
            128790, -- Camp Narache Rations
            128791, -- Deathknell Rations
            129734, -- Potion of Cowardly Flight
            130906, -- Violetglass Vessel
            130907, -- Inert Leystone Charm
            130908, -- Quietwine Vial
            130909, -- Pre-War Highborne Tapestry
            130910, -- Nobleman's Letter Opener
            130911, -- Trailhead Drum
            130912, -- Moosebone Fish-Hook
            130913, -- Hand-Smoothed Pyrestone
            130914, -- Drogbar Gem-Roller
            130915, -- Stonewood Bow
            130916, -- Imp's Cup
            130917, -- Flayed-Skin Chronicle
            130918, -- Malformed Abyssal
            130919, -- Orb of Inner Chaos
            130920, -- Houndstooth Hauberk
            132132, -- Glowing Runestone
            134824, -- "Sir Pugsington" Costume
            134860, -- Peddlefeet's Buffing Creme
            138111, -- Stormforged Grapple Launcher
            138114, -- Gloaming Frenzy
            138777, -- Drowned Mana
            139284, -- Anniversary Gift
            139389, -- Charred Locket
            139783, -- Weathered Relic
            140212, -- Ketchum Tablet
            141005, -- Vial of Hippogryph Pheromones
            141022, -- Legion Ammunition
            141410, -- Invasion Survival Kit
            141624, -- Love Potion No. 8
            142262, -- Electrified Key
            146848, -- Fragmented Enchantment
            146959, -- Corrupted Globule
            146960, -- Ancient Totem Fragment
            146961, -- Shiny Bauble
            146962, -- Golden Minnow
            146963, -- Desecrated Seaweed
            147876, -- Anniversary Gift
            151146, -- Charmed Band
            151147, -- Charmed Pendant
            151148, -- Charmed Choker
            151149, -- Charmed Ring
            151150, -- Charmed Bracelet
            151151, -- Tacky Chronometer
            152786, -- Call of the Devourer
            152890, -- Smashed Portal Generator
            152891, -- Power Cell
            152940, -- Arc Circuit
            152941, -- Conductive Sheath
            152965, -- Vishax's Portal Generator
            152991, -- Fiend Bone
            152992, -- Imp Bone
            152993, -- Ur'zul Bone
            152999, -- Imp Meat
            153013, -- Disgusting Feast
            153071, -- Gift of the All-Seer
            153226, -- Observer's Locus Resonator
            139785, -- Tales of the Broken Isles
            140394, -- Thornstalk Barbs
            140397, -- G'Hanir's Blossom
            142364, -- Bag of Twigs
            142366, -- Regurgitated Leaf
            153123, -- Cracked Radinax Control Gem
            141446, -- Tome of the Tranquil Mind
            141640, -- Tome of the Clear Mind
            143780, -- Tome of the Tranquil Mind
            143785, -- Tome of the Tranquil Mind
            134021, -- X-52 Rocket Helmet
            133998, -- Rainbow Generator
            151474, -- Blighthead Collector's Frame
            131809, -- Gleaming Roc Feather
            131810, -- Derelict Skyhorn Kite
            131926, -- Delicate Roc Feather
            131927, -- Shimmering Roc Feather
            133803, -- Common Bag of Loot
            133804, -- Rare Bag of Loot
            136410, -- Kalec's Image Crystal
            137278, -- Wardens Vendor List
            137279, -- Nightfallen Vendor List
            137280, -- Highmountain Tribes Vendor List
            137281, -- Court of Farondis Vendor List
            137282, -- Valarjar Vendor List
            137283, -- Dreamweavers Vendor List
            138115, -- Kalec's Image Crystal
            138123, -- Shiny Gold Nugget
            138125, -- Crystal Clear Gemstone
            138127, -- Mysterious Coin
            138129, -- Swatch of Priceless Silk
            138131, -- Magical Sprouting Beans
            138133, -- Elixir of Endless Wonder
            138135, -- Rax's Magnifying Glass
            138410, -- Summoning Portal
            139584, -- Sticky Bombs
            139585, -- Smoke Powder
            139586, -- Thistle Tea
            139588, -- Pack of Battle Potions
            139589, -- Poisoned Throwing Knives
            139632, -- A Tiny Pair of Goggles
            140220, -- Scavenged Cloth
            140221, -- Found Sack of Gems
            140222, -- Harvested Goods
            140224, -- Butchered Meat
            140225, -- Salvaged Armor
            140226, -- Mana-Tinged Pack
            140227, -- Bloodhunter's Quarry
            140767, -- Pile of Bits and Bones
            140923, -- Ghoul Tombstone
            140924, -- Ashtongue Beacon
            140925, -- Enchanted Bark
            140926, -- Bowmen's Orders
            140927, -- Water Globe
            140928, -- Ox Initiate's Pledge
            140929, -- Squire's Oath
            140930, -- Acolyte's Vows
            140931, -- Bandit Wanted Poster
            140932, -- Earthen Mark
            140933, -- Runed Aspirant's Band
            141071, -- Badge of Honor
            144330, -- Sprocket Container
            144457, -- Scrolls of the Faldrottin
            151152, -- Star-Etched Ring
            151153, -- Glinting Manaseal
            151154, -- Managleam Pendant
            151155, -- Mana-Etched Signet
            151156, -- Manaweft Bracelet
            151157, -- Flashy Chronometer
            153202, -- Argunite Cluster
            153248, -- Light's Fortune
            122681, -- Sternfathom's Pet Journal
            129113, -- Faintly Glowing Flagon of Mead
            129149, -- Death's Door Charm
            129165, -- Barnacle-Encrusted Gem
            129190, -- Rope of Friendship
            129276, -- Beginner's Guide to Dimensional Rifting
            129279, -- Enchanted Stone Whistle
            129367, -- Vrykul Toy Boat Kit
            129956, -- Leather Love Seat
            130157, -- Syxsehnz Rod
            130158, -- Path of Elothir
            130169, -- Tournament Favor
            130170, -- Tear of the Green Aspect
            130171, -- Cursed Orb
            130191, -- Trapped Treasure Chest Kit
            130209, -- Never Ending Toy Chest
            130214, -- Worn Doll
            130232, -- Moonfeather Statue
            130249, -- Waywatcher's Boon
            131812, -- Darkshard Fragment
            131814, -- Whitewater Carp
            131900, -- Majestic Elderhorn Hoof
            131933, -- Critter Hand Cannon
            136846, -- Familiar Stone
            136848, -- Worn Doll - Test
            136849, -- Nature's Beacon
            136855, -- Hunter's Call
            136927, -- Scarlet Confessional Book
            136928, -- Thaumaturgist's Orb
            136934, -- Raging Elemental Stone
            136935, -- Tadpole Cloudseeder
            136937, -- Vol'jin's Serpent Totem
            137294, -- Dalaran Initiates' Pin
            137560, -- Dreamweaver Provisions
            137561, -- Highmountain Tribute
            137562, -- Valarjar Cache
            137563, -- Farondis Lockbox
            137564, -- Nightfallen Hoard
            137565, -- Warden's Field Kit
            138202, -- Sparklepony XL
            138490, -- Waterspeaker's Totem
            138873, -- Mystical Frosh Hat
            138878, -- Copy of Daglop's Contract
            138900, -- Gravil Goldbraid's Famous Sausage Hat
            140324, -- Mobile Telemancy Beacon
            140325, -- Home Made Party Mask
            140591, -- Shattered Satchel of Cooperation
            140632, -- Lava Fountain
            140779, -- Falanaar Echo
            140780, -- Fal'dorei Egg
            140786, -- Ley Spider Eggs
            141155, -- Challenger's Spoils
            141156, -- Haunted Ravencrest Keepsake
            141157, -- Nightborne Rucksack
            141158, -- Despoiled Keeper's Cache
            141159, -- Watertight Salvage Bag
            141160, -- Seaweed-Encrusted Satchel
            141161, -- Cache of the Black Dragon
            141162, -- Unmarked Suramar Vault Crate
            141163, -- Bag of Confiscated Materials
            141164, -- Violet Hold Contraband Locker
            141165, -- Challenger's Spoils
            141166, -- Haunted Ravencrest Keepsake
            141167, -- Nightborne Rucksack
            141168, -- Despoiled Keeper's Cache
            141169, -- Watertight Salvage Bag
            141170, -- Seaweed-Encrusted Satchel
            141171, -- Cache of the Black Dragon
            141172, -- Unmarked Suramar Vault Crate
            141173, -- Bag of Confiscated Materials
            141174, -- Violet Hold Contraband Locker
            141175, -- Challenger's Spoils
            141176, -- Haunted Ravencrest Keepsake
            141177, -- Nightborne Rucksack
            141178, -- Despoiled Keeper's Cache
            141179, -- Watertight Salvage Bag
            141180, -- Seaweed-Encrusted Satchel
            141181, -- Cache of the Black Dragon
            141182, -- Unmarked Suramar Vault Crate
            141183, -- Bag of Confiscated Materials
            141184, -- Violet Hold Contraband Locker
            141296, -- Ancient Mana Basin
            141297, -- Arcano-Shower
            141298, -- Displacer Meditation Stone
            141299, -- Kaldorei Light Globe
            141300, -- Magi Focusing Crystal
            141301, -- Unstable Powder Box
            141306, -- Wisp in a Bottle
            141331, -- Vial of Green Goo
            141350, -- Kirin Tor Chest
            141652, -- Mana Divining Stone
            141879, -- Berglrgl Perrgl Girggrlf
            142265, -- Big Red Raygun
            142342, -- Glittering Pack
            142360, -- Blazing Ember Signet
            142361, -- Ivory Talon
            142367, -- Ivory Feather
            142452, -- Lingering Wyrmtongue Essence
            143544, -- Skull of Corruption
            143545, -- Fel Focusing Crystal
            143606, -- Satchel of Battlefield Spoils
            143607, -- Soldier's Footlocker
            146747, -- Dreamweaver Provisions
            146748, -- Highmountain Tribute
            146749, -- Valarjar Cache
            146750, -- Farondis Lockbox
            146751, -- Nightfallen Hoard
            146752, -- Warden's Field Kit
            146753, -- Kirin Tor Chest
            146897, -- Farondis Chest
            146898, -- Dreamweaver Cache
            146899, -- Highmountain Supplies
            146900, -- Nightfallen Cache
            146901, -- Valarjar Strongbox
            146902, -- Warden's Supply Kit
            147361, -- Legionfall Chest
            147384, -- Legionfall Recompense
            147446, -- Brawler's Footlocker
            147697, -- Pocket Portal of Felstalker Pups
            147698, -- Cauterizing Void Shard
            147708, -- Legion Invasion Simulator
            147832, -- Magical Saucer
            147864, -- Legionfall Banner
            151265, -- Blight Boar Microphone
            151464, -- Dreamweaver Provisions
            151465, -- Highmountain Tribute
            151466, -- Valarjar Cache
            151467, -- Farondis Lockbox
            151468, -- Nightfallen Hoard
            151469, -- Warden's Field Kit
            151470, -- Kirin Tor Chest
            151471, -- Legionfall Recompense
            151875, -- Call to the Light
            151929, -- Vindicaar Teleport Beacon
            152102, -- Farondis Chest
            152103, -- Dreamweaver Cache
            152104, -- Highmountain Supplies
            152105, -- Nightfallen Cache
            152106, -- Valarjar Strongbox
            152107, -- Warden's Supply Kit
            152108, -- Legionfall Chest
            152649, -- Legionfall Spoils
            152650, -- Scuffed Krokul Cache
            152652, -- Gilded Trunk
            152922, -- Brittle Krokul Chest
            152923, -- Gleaming Footlocker
            152982, -- Vixx's Chest of Tricks
            153004, -- Unstable Portal Emitter
            153124, -- Spire of Spite
            153126, -- Micro-Artillery Controller
            153179, -- Blue Conservatory Scroll
            153180, -- Yellow Conservatory Scroll
            153181, -- Red Conservatory Scroll
            153182, -- Holy Lightsphere
            153183, -- Barrier Generator
            153194, -- Legion Communication Orb
            153204, -- All-Seer's Eye
            153253, -- S.F.E. Interceptor
            153293, -- Sightless Eye
            154903, -- Dreamweaver Provisions
            154904, -- Highmountain Tribute
            154905, -- Valarjar Cache
            154906, -- Farondis Lockbox
            154907, -- Nightfallen Hoard
            154908, -- Warden's Field Kit
            154909, -- Kirin Tor Chest
            154910, -- Legionfall Spoils
            154911, -- Scuffed Krokul Cache
            154912, -- Gilded Trunk
            154991, -- Brawler's Footlocker
            154992, -- Brawler's Footlocker
            130151, -- The "Devilsaur" Lunchbox
            130254, -- Chatterstone
            132982, -- Sonic Environment Enhancer
            128536, -- Leylight Brazier
            138269, -- Strange Gem
            138298, -- Inconspicuous Crate
            138393, -- Essence Swapper
            138876, -- Runas' Crystal Grinder
            139502, -- Reins of the Deathcharger
            139505, -- Reins of the Charger
            139773, -- Emerald Winds
            140601, -- Sixtrigger Resource Crate
            142536, -- Memory Cube
            151877, -- Barrel of Eyepatches
            134023, -- Bottled Tornado
            134024, -- Cursed Swabby Helmet
            144072, -- Adopted Puppy Crate
            144393, -- Portable Yak Wash
            134019, -- Don Carlos' Famous Hat
            134022, -- Burgy Blackheart's Handsome Hat
            134020, -- Chef's Hat
            151016, -- Fractured Necrolyte Skull
            143326, -- Stone of Jordan
            133997, -- Black Ice
            151475, -- Complete Autographed Blight Boar Portait Collection
            140363, -- Pocket Fel Spreader
            129055, -- Shoe Shine Kit
            129057, -- Dalaran Disc
            129093, -- Ravenbear Disguise
            129111, -- Kvaldir Raiding Horn
            129733, -- Ancient Kodo Horn
            130867, -- Tag Toy
            131717, -- Starlight Beacon
            131724, -- Crystalline Eye of Undravius
            131744, -- Key to Nar'thalas Academy
            131745, -- Key of Kalyndras
            131811, -- Rocfeather Skyhorn Kite
            132118, -- Aggramar's Blessing
            134831, -- "Guy Incognito" Costume
            136360, -- Crystalline Demonic Eye
            136383, -- Ravencrest Cache
            137663, -- Soft Foam Sword
            138415, -- Slightly-Chewed Insult Book
            139337, -- Disposable Winter Veil Suits
            139467, -- Satchel of Spoils
            139587, -- Suspicious Crate
            139591, -- Stolen Tome of Artifact Lore
            139781, -- Marin Noggenfogger's Lucky Coin
            140160, -- Stormforged Vrykul Horn
            140314, -- Crab Shank
            141649, -- Set of Matches
            142250, -- Aegwynn and the Dragon Hunt
            142341, -- Love Boat
            142494, -- Purple Blossom
            142495, -- Fake Teeth
            142496, -- Dirty Spoon
            142497, -- Tiny Pack
            142545, -- Small Charm of Life
            142546, -- Small Charm of Inertia
            142547, -- Large Charm of Strength
            142548, -- Large Charm of Dexterity
            142549, -- Serpent's Grand Charm
            142551, -- Stalwart's Grand Charm
            143534, -- Wand of Simulated Life
            143628, -- Blank Diabolic Tome
            143827, -- Dragon Head Costume
            143828, -- Dragon Body Costume
            143829, -- Dragon Tail Costume
            144339, -- Sturdy Love Fool
            145273, -- The Birth of the Lich King
            145275, -- Icecrown and the Frozen Throne
            145276, -- War of the Spider
            145277, -- Aftermath of the Second War
            145278, -- Beyond the Dark Portal
            145279, -- The Kaldorei and the Well of Eternity
            145280, -- Sargeras and the Betrayal
            145281, -- The Old Gods and the Ordering of Azeroth
            145282, -- Charge of the Dragonflights
            145283, -- The War of the Ancients
            145284, -- Mount Hyjal and Illidan's Gift
            145285, -- The World Tree and the Emerald Dream
            145286, -- Exile of the High Elves
            145287, -- The Sentinels and the Long Vigil
            145288, -- The Founding of Quel'Thalas
            145289, -- Arathor and the Troll Wars
            145290, -- The Guardians of Tirisfal
            145291, -- Ironforge - the Awakening of the Dwarves
            145292, -- The Seven Kingdoms
            145293, -- War of the Three Hammers
            145294, -- The Last Guardian
            145295, -- Kil'jaeden and the Shadow Pact
            145296, -- Rise of the Horde
            145297, -- The Dark Portal and the Fall of Stormwind
            145298, -- The Alliance of Lordaeron
            145299, -- The Invasion of Draenor
            145300, -- The Battle of Grim Batol
            145301, -- Lethargy of the Orcs
            145302, -- The New Horde
            145303, -- Kel'Thuzad and the Forming of the Scourge
            145304, -- The Scourge of Lordaeron
            145305, -- Sunwell - The Fall of Quel'Thalas
            145306, -- Archimonde's Return and the Flight to Kalimdor
            145307, -- The Betrayer Ascendant
            145308, -- Rise of the Blood Elves
            145309, -- Civil War in the Plaguelands
            145310, -- The Lich King Triumphant
            145311, -- Old Hatreds - The Colonization of Kalimdor
            145312, -- The Twin Empires
            145313, -- Empires' Fall
            145314, -- Wrath of Soulflayer
            147496, -- Fel Heart of Argus
            147717, -- Chipped Demonic Key Stone
            147836, -- Salt-Hardened Shell
            147867, -- Pilfered Sweeper
            150547, -- Jolly Roger
            151115, -- Mana-Cloaked Choker
            151158, -- Manaforged Worry-Chain
            151159, -- Managraphic Card
            151160, -- Elegant Manabraid
            151161, -- Subtle Chronometer
            151184, -- Verdant Throwing Sphere
            151191, -- Old Bottle Cap
            151270, -- Horse Tail Costume
            151271, -- Horse Head Costume
            151343, -- Hearthstation
            151344, -- Hearthstation
            151348, -- Toy Weapon Set
            151349, -- Toy Weapon Set
            151830, -- Light's Judgment
            151912, -- Shroud of Arcane Echoes
            152098, -- Lightforged Warframe
            152469, -- Matrix Uplink
            149433, -- Helm of the Demonic Gladiator
            149434, -- Chest of the Demonic Gladiator
            149435, -- Leggings of the Demonic Gladiator
            149436, -- Pauldrons of the Demonic Gladiator
            149437, -- Gloves of the Demonic Gladiator
            149438, -- Cinch of the Demonic Gladiator
            149439, -- Treads of the Demonic Gladiator
            149440, -- Bracers of the Demonic Gladiator
            149441, -- Cloak of the Demonic Gladiator
            149415, -- Helm of the Dominant Gladiator
            149416, -- Chest of the Dominant Gladiator
            149417, -- Leggings of the Dominant Gladiator
            149418, -- Pauldrons of the Dominant Gladiator
            149419, -- Gloves of the Dominant Gladiator
            149420, -- Cinch of the Dominant Gladiator
            149421, -- Treads of the Dominant Gladiator
            149422, -- Bracers of the Dominant Gladiator
            149423, -- Cloak of the Dominant Gladiator
            149397, -- Helm of the Fierce Gladiator
            149398, -- Chest of the Fierce Gladiator
            149399, -- Leggings of the Fierce Gladiator
            149400, -- Pauldrons of the Fierce Gladiator
            149401, -- Gloves of the Fierce Gladiator
            149402, -- Cinch of the Fierce Gladiator
            149403, -- Treads of the Fierce Gladiator
            149404, -- Bracers of the Fierce Gladiator
            149405, -- Cloak of the Fierce Gladiator
            149424, -- Helm of the Demonic Gladiator
            149425, -- Chest of the Demonic Gladiator
            149426, -- Leggings of the Demonic Gladiator
            149427, -- Pauldrons of the Demonic Gladiator
            149428, -- Gloves of the Demonic Gladiator
            149429, -- Cinch of the Demonic Gladiator
            149430, -- Treads of the Demonic Gladiator
            149431, -- Bracers of the Demonic Gladiator
            149432, -- Cloak of the Demonic Gladiator
            146835, -- Helm of the Ferocious Gladiator
            146836, -- Chest of the Ferocious Gladiator
            146837, -- Leggings of the Ferocious Gladiator
            146838, -- Pauldrons of the Ferocious Gladiator
            146839, -- Gloves of the Ferocious Gladiator
            146840, -- Cinch of the Ferocious Gladiator
            146841, -- Treads of the Ferocious Gladiator
            146842, -- Bracers of the Ferocious Gladiator
            146843, -- Cloak of the Ferocious Gladiator
            149406, -- Helm of the Dominant Gladiator
            149407, -- Chest of the Dominant Gladiator
            149408, -- Leggings of the Dominant Gladiator
            149409, -- Pauldrons of the Dominant Gladiator
            149410, -- Gloves of the Dominant Gladiator
            149411, -- Cinch of the Dominant Gladiator
            149412, -- Treads of the Dominant Gladiator
            149413, -- Bracers of the Dominant Gladiator
            149414, -- Cloak of the Dominant Gladiator
            149388, -- Helm of the Fierce Gladiator
            149389, -- Chest of the Fierce Gladiator
            149390, -- Leggings of the Fierce Gladiator
            149391, -- Pauldrons of the Fierce Gladiator
            149392, -- Gloves of the Fierce Gladiator
            149393, -- Cinch of the Fierce Gladiator
            149394, -- Treads of the Fierce Gladiator
            149395, -- Bracers of the Fierce Gladiator
            149396, -- Cloak of the Fierce Gladiator
            146817, -- Helm of the Cruel Gladiator
            146818, -- Chest of the Cruel Gladiator
            146819, -- Leggings of the Cruel Gladiator
            146820, -- Pauldrons of the Cruel Gladiator
            146821, -- Gloves of the Cruel Gladiator
            146822, -- Cinch of the Cruel Gladiator
            146823, -- Treads of the Cruel Gladiator
            146824, -- Bracers of the Cruel Gladiator
            146825, -- Cloak of the Cruel Gladiator
            146826, -- Helm of the Ferocious Gladiator
            146827, -- Chest of the Ferocious Gladiator
            146828, -- Leggings of the Ferocious Gladiator
            146829, -- Pauldrons of the Ferocious Gladiator
            146830, -- Gloves of the Ferocious Gladiator
            146831, -- Cinch of the Ferocious Gladiator
            146832, -- Treads of the Ferocious Gladiator
            146833, -- Bracers of the Ferocious Gladiator
            146834, -- Cloak of the Ferocious Gladiator
            146804, -- Helm of the Cruel Gladiator
            146809, -- Chest of the Cruel Gladiator
            146810, -- Leggings of the Cruel Gladiator
            146811, -- Pauldrons of the Cruel Gladiator
            146812, -- Gloves of the Cruel Gladiator
            146813, -- Cinch of the Cruel Gladiator
            146814, -- Treads of the Cruel Gladiator
            146815, -- Bracers of the Cruel Gladiator
            146816, -- Cloak of the Cruel Gladiator
            139003, -- Pocket Pet Portal
            139484, -- Cache of Nightmarish Treasures
            139486, -- Cache of Nightmarish Treasures
            139487, -- Cache of Nightmarish Treasures
            139488, -- Cache of Nightmarish Treasures
            140148, -- Cache of Nightborne Treasures
            140150, -- Cache of Nightborne Treasures
            140152, -- Cache of Nightborne Treasures
            140154, -- Cache of Nightborne Treasures
            140231, -- Narcissa's Mirror
            140309, -- Prismatic Bauble
            147518, -- Cache of Fel Treasures
            147519, -- Cache of Fel Treasures
            147520, -- Cache of Fel Treasures
            147521, -- Cache of Fel Treasures
            147843, -- Sira's Extra Cloak
            153501, -- Cache of Antoran Treasures
            153502, -- Cache of Antoran Treasures
            153503, -- Cache of Antoran Treasures
            153504, -- Cache of Antoran Treasures
            129742, -- Badge of Timewalking Justice
            134006, -- Dwyer's Spare Caber
            134008, -- Simple Rosary of Light
            143543, -- Twelve-String Guitar
            139383, -- Keystone Container
            139382, -- Keystone Container
            128368, -- Dripping Fangs of Goremaw
            136786, -- Uncrowned Insignia
            139381, -- Keystone Container
            140997, -- Alliance Strongbox
            140998, -- Horde Strongbox
            146799, -- BUILDING CONTRIBUTION REWARD ITEM [addon.CONS.NYI]
            146800, -- BUILDING CONTRIBUTION REWARD ITEM [addon.CONS.NYI]
            146801, -- BUILDING CONTRIBUTION REWARD ITEM [addon.CONS.NYI]
            147537, -- A Tiny Set of Warglaives
            151131, -- Lamp of Al'Abas
            151143, -- Shining Lamp of Al'Abas
            151144, -- Gleaming Lamp of Al'Abas
            151162, -- Glitzy Mana-Chain
            151163, -- Locket of Magical Memories
            151164, -- Sparkling Sin'dorei Signet
            151165, -- Verbellin Tourbillon Chronometer
            127009, -- Fragment of Frostmourne
            139771, -- Seething Essence
            140199, -- Nightshard
            140200, -- Immaculate Nightshard Curio
            -- JUNK
            135546, --Fel-Touched Crate of Battlefield Goods
            138098, -- Iron-Bound Crate of Battlefield Goods
            139049, -- Large Legion Chest
            141069, -- Skyhold Chest of Riches
            135545, -- Savage Crate of Battlefield Goods
            139048, -- Small Legion Chest
            135543, -- Rival's Crate of Battlefield Goods
            135544, -- Tranquil Crate of Helpful Goods - Reuse Me
            135542, -- Icy Crate of Battlefield Goods
            139403, -- Powerful Magical Foci and Those Who Wielded Them
            140093, -- The Untold Tales of the War of the Ancients
            140097, -- The Fall of Lordaeron and the Scouring of the Eastweald
            140101, -- A Hypothetical Examination of the Legion's Weaknesses
            140105, -- The Untold Tales of the War of the Ancients
            140109, -- How to Meditate in a Hurricane
            140113, -- Legends of the Silver Hand
            140117, -- The Hunt for Light's Wrath
            140121, -- The Seven Curses of the Southern Seas
            140125, -- The Fall of the Warchief
            140129, -- A Hypothetical Examination of the Legion's Weaknesses
            140133, -- Axe, Blade, and Fist
            141332, -- The Annals of Light and Shadow
            154879, -- Awoken Titan Essence
            141862, -- Mote of Light
            141904, -- Battleground Victory Bonus
            141905, -- Battleground Victory Bonus
            141906, -- Arena Victory Bonus
            143562, -- Chest of the Foreseen Conqueror
            143563, -- Gauntlets of the Foreseen Conqueror
            143564, -- Leggings of the Foreseen Conqueror
            143565, -- Helm of the Foreseen Conqueror
            143566, -- Shoulders of the Foreseen Conqueror
            143567, -- Gauntlets of the Foreseen Vanquisher
            143568, -- Helm of the Foreseen Vanquisher
            143569, -- Leggings of the Foreseen Vanquisher
            143570, -- Shoulders of the Foreseen Vanquisher
            143571, -- Chest of the Foreseen Vanquisher
            143572, -- Chest of the Foreseen Protector
            143573, -- Gauntlets of the Foreseen Protector
            143574, -- Leggings of the Foreseen Protector
            143575, -- Helm of the Foreseen Protector
            143576, -- Shoulders of the Foreseen Protector
            143577, -- Cloak of the Foreseen Conqueror
            143578, -- Cloak of the Foreseen Vanquisher
            143579, -- Cloak of the Foreseen Protector
            147316, -- Chest of the Foregone Vanquisher
            147317, -- Chest of the Foregone Conqueror
            147318, -- Chest of the Foregone Protector
            147319, -- Gauntlets of the Foregone Vanquisher
            147320, -- Gauntlets of the Foregone Conqueror
            147321, -- Gauntlets of the Foregone Protector
            147322, -- Helm of the Foregone Vanquisher
            147323, -- Helm of the Foregone Conqueror
            147324, -- Helm of the Foregone Protector
            147325, -- Leggings of the Foregone Vanquisher
            147326, -- Leggings of the Foregone Conqueror
            147327, -- Leggings of the Foregone Protector
            147328, -- Shoulders of the Foregone Vanquisher
            147329, -- Shoulders of the Foregone Conqueror
            147330, -- Shoulders of the Foregone Protector
            147331, -- Cloak of the Foregone Vanquisher
            147332, -- Cloak of the Foregone Conqueror
            147333, -- Cloak of the Foregone Protector
            152065, -- Copy Chest of the Foregone Vanquisher
            152066, -- Copy Chest of the Foregone Conqueror
            152067, -- Copy Chest of the Foregone Protector
            152068, -- Copy Gauntlets of the Foregone Vanquisher
            152069, -- Copy Gauntlets of the Foregone Conqueror
            152070, -- Copy Gauntlets of the Foregone Protector
            152071, -- Copy Helm of the Foregone Vanquisher
            152072, -- Copy Helm of the Foregone Conqueror
            152073, -- Copy Helm of the Foregone Protector
            152074, -- Copy Leggings of the Foregone Vanquisher
            152075, -- Copy Leggings of the Foregone Conqueror
            152076, -- Copy Leggings of the Foregone Protector
            152077, -- Copy Shoulders of the Foregone Vanquisher
            152078, -- Copy Shoulders of the Foregone Conqueror
            152079, -- Copy Shoulders of the Foregone Protector
            152080, -- Copy Cloak of the Foregone Vanquisher
            152081, -- Copy Cloak of the Foregone Conqueror
            152082, -- Copy Cloak of the Foregone Protector
            152515, -- Cloak of the Antoran Protector
            152516, -- Cloak of the Antoran Conqueror
            152517, -- Cloak of the Antoran Vanquisher
            152518, -- Chest of the Antoran Vanquisher
            152519, -- Chest of the Antoran Conqueror
            152520, -- Chest of the Antoran Protector
            152521, -- Gauntlets of the Antoran Vanquisher
            152522, -- Gauntlets of the Antoran Conqueror
            152523, -- Gauntlets of the Antoran Protector
            152524, -- Helm of the Antoran Vanquisher
            152525, -- Helm of the Antoran Conqueror
            152526, -- Helm of the Antoran Protector
            152527, -- Leggings of the Antoran Vanquisher
            152528, -- Leggings of the Antoran Conqueror
            152529, -- Leggings of the Antoran Protector
            152530, -- Shoulders of the Antoran Vanquisher
            152531, -- Shoulders of the Antoran Conqueror
            152532, -- Shoulders of the Antoran Protector
            130147, -- Thistleleaf Branch
            130194, -- Silver Gilnean Brooch
            130199, -- Legion Pocket Portal
            141899, -- Battleground Victory Award
            141901, -- Battleground Victory Award
            141902, -- Arena Victory Award
            141903, -- Arena Victory Award
            147838, -- Akazamzarak's Spare Hat
            147871, -- Doom Stone
            153001, -- Faintly Glowing Phoenix Down
            153039, -- Crystalline Campfire
            153116, -- Wyrmtongue Cache of Herbs
            153117, -- Wyrmtongue Cache of Supplies
            153118, -- Wyrmtongue Cache of Shiny Things
            153119, -- Wyrmtongue Cache of Finery
            153120, -- Wyrmtongue Cache of Minerals
            153121, -- Wyrmtongue Cache of Skins
            153122, -- Wyrmtongue Cache of Magic
            121331, -- Leystone Lockbox
            147870, -- Strange Dimensional Shard
            151526, -- Depleted Riftstone
            151702, -- Charged Riftstone
            135541, -- Crusader's Crate of Battlefield Goods
            129158, -- Starlight Rosedust
            140658, -- Skull of Nithogg
            140659, -- Skull of Shar'thos
            140660, -- Haft of the God-King
            138470, -- Silver Strongbox
            138475, -- Silver Strongbox
            141409, -- Candrael's Charm
            132892, -- Blingtron 6000 Gift Package
            138469, -- Champion's Strongbox
            138471, -- Bronze Strongbox
            138472, -- Steel Strongbox
            138473, -- Steel Strongbox
            138474, -- Champion's Strongbox
            138476, -- Bronze Strongbox
            140179, -- Faded Star Chart
            140356, -- Demonic Scribblings
            140360, -- Rockwurm Barb
            140362, -- Dust from the Shadowlands
            140375, -- Used Felblades
            140376, -- Mardum-Calibrated Balancer
            140458, -- Lasher Seed
            140464, -- Miniature Totem
            140465, -- Duskpelt Hide
            140483, -- Jade Fragment
            140499, -- Off-Colored Fel Stone
            140501, -- Outdated Intelligence
            140502, -- Tattered Eye Patch
            140514, -- Vial of Air
            140515, -- Gift of Al'Akir
            140526, -- Eredar Signet
            140527, -- Wrathguard's Medallion
            149574, -- Loot-Stuffed Pumpkin
            149752, -- Keg-Shaped Treasure Box
            149753, -- Knapsack of Chilled Goods
            151557, -- Champion's Strongbox
            151558, -- Champion's Strongbox
            149503, -- Stolen Gift
            147907, -- Heart-Shaped Carton
            135540, -- Crate of Battlefield Goods
            135539, -- Crate of Battlefield Goods
            150743, -- Surviving Kalimdor
            150744, -- Walking Kalimdor with the Earthmother
            150745, -- The Azeroth Campaign
            150746, -- To Modernize the Provisioning of Azeroth
            131743, -- Blood of Young Mannoroth
            139624, -- Shard of Darkness
            141995, -- Unclaimed Black Market Container
            132107, -- Hidden Horde Cache Map
            132122, -- Legion Commander's Key
            133901, -- Faded Treasure Map
            133902, -- Worn Treasure Map
            133903, -- Old Treasure Map
            133904, -- Stained Treasure Map
            133905, -- Bloody Treasure Map
            133906, -- Ripped Treasure Map
            133907, -- Torn Treasure Map
            133908, -- Smudged Treasure Map
            133909, -- Folded Treasure Map
            143559, -- Wyrmtongue's Cache Key
            122291, -- Sanctuary Coins
            130186, -- Intern Items - BJI
            141593, -- Dro's Key
            146662, -- Potion of Potion Drinking
            146663, -- Soggy Tapestry
            146664, -- Finely-Jeweled Key
            146671, -- Cubic Zirconia
            147536, -- Giant Pile of Wooden Coins
            147869, -- Fel Meteorite
            133877, -- Lean Shank Recipes
            133886, -- Wildfowl Egg Recipes
            133914, -- Fatty Bearsteak Recipes
            133915, -- Big Gamy Ribs Recipes
            133916, -- Leyblood Recipes
            133917, -- Cursed Queenfish Recipes
            133918, -- Mossgill Perch Recipes
            133919, -- Highmountain Salmon Recipes
            133920, -- Stormray Recipes
            133921, -- Runescale Koi Recipes
            133922, -- Black Barracuda Recipes
            133923, -- Bacon Recipes
            136359, -- Shaman's Pouch
            136577, -- Mornath's Key
            136602, -- Valeera's Note
            137650, -- Bucket of Blue Paint
            137651, -- Bucket of Teal Paint
            137652, -- Bucket of Green Paint
            137653, -- Bucket of Grey Paint
            137654, -- Pile of Juggernaut Parts
            137655, -- Deactivated Grey Juggernaut
            137656, -- Deactivated Blue Juggernaut
            137657, -- Deactivated Teal Juggernaut
            137658, -- Deactivated Green Juggernaut
            138097, -- Blackfuse's Power Core
            138382, -- Lucky Rat's Tooth
            138383, -- Old Lucky Coin
            138384, -- Lucky Charm
            138884, -- Throwing Sausage
            139302, -- Etching from the Raven's Eye Tablet
            139341, -- Winter Veil Gift
            139343, -- Gently Shaken Gift
            139375, -- Glorious Earwax Candle
            139460, -- Seal of Broken Fate
            139620, -- A Complete Copy of "Nat Pagle's Guide to Extreme Anglin'."
            139623, -- Timolain's Phylactery
            140330, -- Windfall Totem
            140331, -- Skyhorn War Harness
            140332, -- Rivermane War Harness
            140333, -- Bloodtotem War Harness
            140334, -- Highmountain War Harness
            140655, -- Log
            140731, -- Treasure Map: Highmountain
            140743, -- Treasure Map: Stormheim
            140744, -- Treasure Map: Azsuna
            140745, -- Treasure Map: Val'sharah
            140746, -- Treasure Map: Suramar
            141700, -- Silver Mackerel Recipes
            142266, -- Handful of Gizmos
            147315, -- Smelly's Luckydo
            147420, -- Pebble
            149504, -- Smokywood Pastures Special Present
            151345, -- Gently Shaken Gift
            151350, -- Winter Veil Gift
            152996, -- Vrykul Toy Boat
            153219  -- Squished Demon Eye
        }
    },
    [addon.CONS.QUEST_ID] = {
        143661, -- Soul Prism of the Illidari
        139043, -- Tear of Elune
        141351, -- Tear of Elune
        146975, -- Gladiator's Tattered Cloak
        147417, -- Gladiator's Tattered Cloak
        136834, -- Empowered Soul Shard
        137119, -- Shard of Nightmare
        137268, -- Fragment of Xavius
        139368, -- Pure Holy Light
        134001, -- Prototype X
        134002, -- Prototype N
        134003, -- Prototype B
        134055, -- Reinforced Cage Key
        136400, -- Shadowice Shard
        139619, -- Ymiron's Broken Blade
        142487, -- Valarjar Soul Fragment
        147116, -- Captured Spirit of the Father of Owls
        147356, -- Broken Sentinax Beacon
        153014, -- Pristine Argunite
        153555, -- Alor'idal Crystal
        127767, -- Morashu's Antlers
        127769, -- Quiver of Arrows
        128343, -- Stonedark Focus
        128380, -- Hag Feather
        128393, -- Empowered Crystal
        128651, -- Critter Hand Cannon
        128655, -- Rabbit
        128752, -- Soul Chamber
        129183, -- Pufferfish Egg
        129191, -- Shipwrecked Supplies
        130074, -- Flare Gun
        133941, -- Hobart's Prototype Gunshoes
        137625, -- Diamond Lockpicks
        143676, -- Rimefang's Harness
        139463, -- Felbat Toxin Salve
        124632, -- Incandescent Blood
        129117, -- Aethril Sample
        129118, -- Dreamleaf Sample
        129119, -- Foxflower Sample
        129120, -- Fjarnskaggl Sample
        129121, -- Starlight Rosedust
        129122, -- Felwort Sample
        129128, -- Pristine Pistil
        129131, -- Stainless Stamen
        129135, -- Ragged Strips of Silk
        129136, -- Blight-Twisted Herb
        129137, -- Nibbled Foxflower Stem
        129138, -- Ram's-Horn Trowel
        129140, -- Jeweled Spade Handle
        129141, -- Blight-Choked Herb
        129142, -- Runed Journal Page
        129143, -- Scribbled Ramblings
        129150, -- Healthy Dreamleaf
        129151, -- Blight-Infested Dreamleaf
        129153, -- Chewed Aethril Stem
        129155, -- Broken Herbalist's Blade
        129200, -- The Fjarnskaggl Fjormula
        129201, -- The Tangled Beard
        129202, -- Herblore of the Ancients
        129209, -- Tharillon's Notebook
        129212, -- Ryno Bloomfield's Analysis
        129213, -- Dani Earthtouch's Analysis
        129214, -- Lohor's Analysis
        129220, -- Chewed Foxflower Bit
        129278, -- Foxflower Scent Gland
        129860, -- Stonehide Leather Sample
        129862, -- Stormscale Sample
        129863, -- Felhide Sample
        129864, -- Scrap of Pants
        129865, -- Unfinished Treatise on the Properties of Stormscale
        129866, -- Immaculate Stonehide Leather
        129867, -- Immaculate Stormscale
        129888, -- Undivided Hide
        129894, -- Stormscale Spark
        129900, -- Bear Hide
        129901, -- Elderhorn Hide
        129903, -- Saber Hide
        129904, -- Wolf Hide
        129905, -- Deer Hide
        129906, -- Lion Seal Hide
        129907, -- Shoveltusk Hide
        129908, -- Hippogryph Scale
        129909, -- Enormous Hippogryph Scale
        129910, -- Agnes' Skinning Knife
        129911, -- Scale of Drakol'nir
        129912, -- Hide of Icehowl
        129913, -- Hide of Occu'thar
        129914, -- Hide of Horridon
        129915, -- Scale of Netherspite
        129916, -- Scale of Sartharion
        129917, -- Scales of Garalon
        129920, -- Hide of Fenryr
        129921, -- Scales of Serpentrix
        133912, -- Nomi's Silver Mackerel
        134129, -- Thick Ironhorn Hide
        134130, -- Shaggy Saber Hide
        134131, -- Bristled Bear Skin
        134132, -- Calcified Wormscale
        134806, -- Unscratched Hippogryph Scale
        134807, -- Fatty Lion Seal Skin
        134808, -- Silky Prowler Fur
        134809, -- Slick Seal Hide
        134810, -- Rugged Wolf Hide
        134811, -- Musky Bear Hide
        134812, -- Bristly Musken Hide
        134813, -- Pristine Stag Hide
        134814, -- Perfect Storm Drake Scale
        134815, -- Furry Shoveltusk Hide
        134816, -- Thick Bear Hide
        134817, -- Solid Crabshell Fragment
        134818, -- Velvety Stalker Hide
        134819, -- Extra-Rancid Felhound Hide
        134820, -- Massive Boar Hide
        134822, -- Rock-Hard Crab Chitin
        135480, -- Aqueous Aethril
        135500, -- Singed Fjarnskaggl
        136538, -- Namha's Stonehide Leather
        136578, -- Hide of Dresaron
        136909, -- Aethrem Crystal
        136912, -- Bulging Nightmare Pod
        136915, -- Woody Seed Cluster
        136916, -- Fjarnsk
        136917, -- Roseate Essence
        136918, -- Sallow Essence
        136926, -- Nightmare Pod
        137628, -- Smooth Sunrunner Hide
        138296, -- Hatecoil Glyptic
        140654, -- Pestilential Hide of Nythendra
        140856, -- Musken Hide
        128224, -- Deucus' Experiemental Treasure Potion
        129930, -- Guron's Drum
        133887, -- Luminous Pearl
        133939, -- Luminous Pearl
        134399, -- Huge Highmountain Salmon
        134400, -- Lively Highmountain Salmon
        134547, -- Wild Northern Barracuda
        134550, -- The Seed of Ages
        134564, -- Lively Cursed Queenfish
        134565, -- Huge Cursed Queenfish
        134566, -- Blue Barracuda
        134567, -- Lively Mossgill Perch
        134568, -- Huge Mossgill Perch
        134569, -- Sharptooth Barracuda
        134570, -- Lively Stormray
        134571, -- Huge Stormray
        134572, -- East Ocean Barracuda
        134573, -- Lively Runescale Koi
        134574, -- Huge Runescale Koi
        134575, -- Mana-Streaked Barracuda
        135489, -- Stunned Stormray
        136710, -- Aura Stone
        138830, -- Demonic Logbook on Uthalesh
        138849, -- Demonic Logbook on Thal'kiel
        139046, -- Demonic Logbook on the Scepter
        139182, -- Scrying Report on Aluneth
        139183, -- Scrying Report on Felo'melorn
        139184, -- Scrying Report on Ebonchill
        139279, -- Albino Barracuda
        139537, -- Braid of the Underking
        139538, -- Nightmare Lash
        140162, -- Maul of the Dead
        138624, -- Odd Smelling Brew
        133928, -- Prototype Pump-Action Bandage Gun
        151927, -- Prototype Gravitational Reduction Slippers
        133875, -- Fargo's Deployable Bullet Dispenser
        143876, -- Coreforged Sigil of Skaldrenox
        143877, -- Glimmering Sigil of Whirlaxis
        143878, -- Diamondine Sigil of Kazum
        143879, -- Tidal Sigil of Skwol
        143884, -- Coreforged Sigil of Skaldrenox
        143885, -- Glimmering Sigil of Whirlaxis
        143886, -- Diamondine Sigil of Kazum
        143887, -- Tidal Sigil of Skwol
        143872, -- Cinder of Cynders
        143873, -- Pearl of Fathoms
        143874, -- Salt of Shards
        143875, -- Song of Zephyrs
        143880, -- Cinder of Cynders
        143881, -- Pearl of Fathoms
        143882, -- Salt of Shards
        143883, -- Song of Zephyrs
        135583, -- Brinescuttle Crab Meat
        118330, -- Pile of Weapons
        120079, -- Head of Fathom-Commander Zarrin
        120080, -- Pilfered Night Elf Bone
        120169, -- Tidestone Vault Key
        120181, -- Giants' Stash of Weapons
        120187, -- Tidestone Shards
        120358, -- Malinoth's Blood
        120359, -- Lykill's Key
        120401, -- Tidestone Shard
        120418, -- Nar'thalas Spellscroll
        120939, -- Arcane-Infused Egg
        120940, -- The Six Eyes of Gangamesh
        120946, -- Nar'thalas Academy Spellbook
        120947, -- Nar'thalas Academy Hat
        120948, -- Nar'thalas Academy Robes
        120949, -- Nar'thalas Academy Wand
        120955, -- Nar'thalas Academy Robe Appearance
        120956, -- Nar'thalas Academy Hat Appearance
        120960, -- Tidestone Vault Key
        121816, -- Unbound Essence
        121817, -- Mana Bark
        121821, -- Azurian Focus
        122095, -- Crackling Leyworm Core
        122100, -- Soul Gem
        122153, -- Charged Mana Jewel
        122157, -- Staff of Eternity
        122158, -- The Violet Heart
        122159, -- Eye of Antonidas
        122188, -- Dim Ley Crystal
        122281, -- Parachute Pack
        122292, -- Grievously Wounded Whelpling
        122294, -- Yotnar's Right Eye
        122295, -- Transponder Battery
        122296, -- Yotnar's Left Eye
        122297, -- Combat Stim
        122306, -- Cracked Ley Crystal
        122393, -- Nightrose Recipe: Part 1, Ingredients
        122394, -- Nightrose Recipe: Part 2, Preparation
        122395, -- Nightrose Recipe: Part 3, Instructions
        122425, -- Stone of Remembrance
        122426, -- Imp Blood
        122427, -- Golden Nectar
        122445, -- Fistful of Feathers
        122446, -- Shadowhorn
        122447, -- Wild Olive
        122526, -- Malinoth's Blood
        122527, -- Seeds of Remedy
        122570, -- Captain's Log of the Queen's Reprisal
        122577, -- Stormforged Grapple Launcher
        122578, -- Heavy Worg Pelt
        122581, -- SI:7 Intel
        122597, -- Enchanted Lodestone
        122609, -- Storm Drake Scale
        122611, -- Cursed Bones
        122612, -- Vrykul Armament
        122619, -- Kvaldir Seaweed
        122638, -- Helira's Lantern
        122679, -- Vrykul Weapon
        122685, -- Climbing Treads
        122686, -- Oiled Cloak
        122699, -- Okuna's Message
        122701, -- High Explosive Grenades
        122702, -- Containment Unit
        122704, -- Elothir's Branch
        122719, -- Mysterious Runestone
        122720, -- Vrykul Helmet
        122721, -- Vrykul Shield
        122746, -- Head of Thane Wildsky
        123860, -- Odyn's Horn
        123861, -- Piece of your Soul
        123876, -- Skyfire First Aid Kit
        123878, -- Intact Thorignir Egg
        123880, -- Lord Hal'shara's Tomb
        123887, -- Crystal Oscillator
        123971, -- Masterwork Hatecoil Breastplate
        123972, -- Masterwork Hatecoil Pauldrons
        123976, -- Ancient Sentry Construct Memory Core
        123977, -- Ancient Sentry Construct Memory Core
        123978, -- Ancient Highborne Data Printout
        123979, -- Leysmithing Guide
        123980, -- Lunarwing Egg
        124003, -- Barm's Recipes
        124004, -- Grayheft, Ancient Hammer of the Highmountain Tauren
        124005, -- Shopkeeper's Leystone Ore
        124006, -- Leystone Slag
        124007, -- Leystone Bar
        124008, -- Red-Hot Leystone Bar
        124009, -- Leystone Cuffplate
        124010, -- Leystone Fingerguard
        124024, -- Leystone Armor Stand
        124025, -- Ancient Vrykul Hammer
        124038, -- Enchanted Lodestone
        124049, -- Handcrafted Leystone Gauntlets
        124051, -- Burnt, Exploded Mess
        124055, -- Runed Scroll
        124075, -- Illidari Warglaives [update icon]
        124096, -- Fel Cuirass
        124097, -- Havoc's Gorget
        124098, -- Hauberk of Vengeance
        124100, -- Moonwater Vial
        124131, -- Demonic Emblem
        124392, -- Shopkeeper's Leystone Ore
        124393, -- Leystone Slag
        124394, -- Hard Leystone Bar
        124395, -- Heated Hard Leystone Bar
        124396, -- Dull Hard Leystone Armguards
        124397, -- Hard Leystone Armguards
        124398, -- Nightberry Truffle
        124399, -- Moonthorn Branch
        124401, -- Elder Branch
        124402, -- Small Metal Scrap
        124403, -- Medium Metal Scrap
        124404, -- Large Metal Scrap
        124405, -- Small Heated Metal Scrap
        124406, -- Medium Heated Metal Scrap
        124407, -- Large Heated Metal Scrap
        124408, -- Scrapmetal Fingerplates
        124409, -- Scrapmetal Palmplate
        124410, -- Scrapmetal Handguard
        124411, -- Scrapmetal Cuffplate
        124412, -- Warden's Glaive
        124413, -- Warden's Helmet
        124416, -- Ancient Seed
        124417, -- Shopkeeper's Leystone Ore
        124418, -- Leystone Slag
        124419, -- Hard Leystone Bar
        124420, -- Leystone Shard
        124421, -- Lump of Leystone Slag
        124422, -- Hard Leystone Ingot
        124423, -- Heated Hard Leystone Ingot
        124424, -- Hard Leystone Nail
        124425, -- Felsmith's Leystone Bar
        124426, -- Red-Hot Leystone Bar
        124427, -- Leystone Shinplate
        124428, -- Leystone Heelguard
        124429, -- Leystone Footguard
        124430, -- Leystone Soleplate
        124431, -- Leystone Faceguard
        124432, -- Leystone Dome
        124433, -- Handmade Leystone Boots
        124434, -- Handmade Leystone Helm
        124435, -- Leystone Neckplate
        124449, -- Felsmith's Leystone Armguards
        124450, -- Engraved Leystone Armguards
        124451, -- Felsmith's Infernal Brimstone
        124452, -- Brimstone Slag
        124453, -- Brimstone-Covered Armguards
        124454, -- Brimstone-Crusted Armguards
        124455, -- Masterwork Leystone Armguards
        124489, -- Leystone Deposit Sample
        124490, -- Leystone Seam Sample
        124491, -- Living Leystone Sample
        124492, -- Torn Journal Page
        124493, -- Battered Mining Pick
        124494, -- Chunk of Horn
        124496, -- Felslate Deposit Sample
        124497, -- Felslate Seam Sample
        124498, -- Living Felslate Sample
        124499, -- Ore-Bound Eye
        124500, -- Severed Arm
        124501, -- Ore-Choked Heart
        124502, -- Infernal Brimstone Sample
        124503, -- Engorged Bear Heart
        124504, -- Soulcap
        124505, -- Ashildir's Skull
        124508, -- Piece of your Soul
        124509, -- Piece of your Soul
        124511, -- Ashildir's Hand
        124512, -- Ashildir's Bones
        124528, -- Intact Murktide Tail
        124530, -- Head of Akaliss
        124535, -- Lit Torch
        124633, -- Theodric's Cipher
        124672, -- Sargerite Keystone
        126940, -- Lyrelle's Signet Ring
        126946, -- Verse of Ashilvara
        127005, -- Barrel of Corn
        127007, -- Gathered Stones
        127030, -- Granny's Flare Grenades
        127038, -- Stained Silken Robe
        127039, -- Hatecoil Wristwraps
        127042, -- Spritethorn
        127043, -- Sharpened Spritethorn
        127044, -- Runed Breeches
        127046, -- Helsquid Ink
        127047, -- Bear Fur
        127048, -- Heart of the Storm
        127129, -- Targeting Flare
        127138, -- Heart of a Giant
        127139, -- Hand of a Giant
        127261, -- Pinkish Leyfeather
        127262, -- Arcane Whisker
        127263, -- Enchanted Wyrmdust
        127264, -- Stolen Fish
        127266, -- Shal'dorei Mannequin
        127271, -- Stonedark Crystal
        127273, -- Seal of Ysera
        127281, -- Fel Inscribed Shroud
        127284, -- Ragnar's Rune
        127285, -- Forsaken Scouting Orders
        127286, -- Tanithria's Silkweave
        127287, -- Tanithria's Thread
        127288, -- Tanithria's Runic Catgut
        127289, -- Tanithria's Purple Dye
        127290, -- Tanithria's Blue Dye
        127291, -- Tanithria's Red Dye
        127292, -- Tanithria's Green Dye
        127293, -- Torn, Ragged Mess
        127294, -- Handcrafted Silkweave Robe
        127295, -- Blazing Torch
        127342, -- Lyndras' Silkweave
        127343, -- Lyndras' Runic Catgut
        127344, -- Rune-Threaded Silkweave Robe
        127345, -- Rune-Threaded Silkweave Bracers
        127358, -- Engraved Vrykul Shield
        127359, -- Basic Silkweave Robe
        127360, -- Embroidered Silkweave Robe
        127361, -- Handcrafted Silkweave Bag
        127362, -- Bolt of Richly-Dyed Silkweave
        127363, -- Silkweave Hood: Outer Layer
        127364, -- Silkweave Hood Lining
        127367, -- Handcrafted Silkweave Hood
        127368, -- Bolt of Brimstone-Soaked Silkweave
        127370, -- Silkweave Bracer: Outer Layer
        127372, -- Silkweave Bracer Lining
        127373, -- Masterwork Silkweave Bracers
        127382, -- Tanithria's Sharpened Spritethorn
        127411, -- Rotbeak's Head
        127675, -- Leyweavers' Amber Dye
        127676, -- Leyweavers' Silkweave
        127677, -- Leyweavers' Sharpened Spritethorn
        127679, -- Leyweavers' Runic Catgut
        127680, -- Alard's Brimstone Shavings
        127702, -- Odyn's Horn
        127710, -- Bloodstone
        127750, -- Gilnean Heavy Explosives
        127752, -- Engraved Vrykul Shield
        127860, -- Warden's Signet
        127863, -- Prison Keys
        127871, -- Depleted Leyflame Burner
        127872, -- Cracked Dual-Chambered Mixing Flask
        127873, -- Advanced Corks
        127874, -- Precipitating Powder
        127875, -- Shal'dorei Silk Filter
        127876, -- Fragile Demonsbreath Crucible
        127877, -- Bendy Glass Tubes
        127878, -- Uncalibrated Malloric Burette
        127879, -- High-Capacity Decoction Conduit
        127897, -- The Sword of Truth
        127988, -- Bug Sprayer
        128164, -- Severed Head
        128227, -- Soulwrought Key
        128280, -- Moonthorn Bundle
        128286, -- Bristlefur Pelt
        128287, -- Detonator
        128290, -- Runewritten Tome
        128291, -- Runewritten Tome Translation
        128305, -- Havi's Horn
        128329, -- Depleted Leyflame Burner
        128330, -- Rotwood Root
        128331, -- Magic Acorn
        128335, -- Smolderhide Firewater
        128340, -- Chieftain's Beads
        128342, -- Furbolg Cork
        128344, -- Kobold Candle
        128345, -- Katterin's Alchemy Kit
        128355, -- Crate of Khadgar's Whiskers
        128356, -- Barrel of Fish Oil
        128357, -- Basket of Dried Herbs
        128367, -- Scavenged Supplies
        128392, -- Deepholm Mineral Analysis
        128396, -- Obsidian Forest Analysis
        128397, -- Bug Sprayer
        128398, -- Fishing Gear
        128405, -- Saronite Composition Analysis
        128493, -- Yotnar's Left Arm
        128494, -- Yotnar's Left Arm
        128495, -- Yotnar's Right Arm
        128496, -- Yotnar's Right Foot
        128497, -- Yotnar's Left Foot
        128508, -- Gutspill's Head
        128509, -- Rumblehoof's Head
        128511, -- Challenger's Tribute
        128512, -- Challenger's Tribute
        128515, -- Cracked Dual-Chambered Mixing Flask
        128516, -- Dual-Chambered Mixing Flask
        128517, -- Torn Shal'dorei Silk
        128518, -- Mended Shal'dorei Silk
        128654, -- Alard's Flux
        128673, -- Demonsbreath Crucible
        128674, -- Malloric Burette
        128676, -- Uncalibrated Malloric Burettes
        128679, -- Lined Demonsbreath Crucible
        128680, -- Corrupted Root Sample
        128681, -- Fragile Demonsbreath Crucible
        128687, -- Royal Summons
        128692, -- Royal Summons
        128697, -- Chains of Resistance
        128703, -- Wheel of Fortitude
        128704, -- Rod of Bearing
        128745, -- Saris' Hammer
        128746, -- Saris' Research
        128751, -- Black Rook Breastplate
        128753, -- Skull of Bossing Around
        128754, -- Yellow Candle
        128755, -- Green Candle
        128756, -- Oenia's Skull
        128757, -- Eagle Eggs
        128758, -- Spotted Gloomcap
        128759, -- Shimmering Pollen
        128760, -- Eagle Egg
        128765, -- Hammer of Khaz'goroth
        128767, -- Ronos' Pick
        128769, -- Glass-Extracted Leystone
        128771, -- Amberblight Antler
        128772, -- Branch of the Runewood
        128773, -- Bear Sirloin
        128774, -- Blackcraw's Tail
        128778, -- Mightstone Crystal
        128801, -- Modified Mightstone Crystal
        128809, -- New Recruit
        128813, -- Fel Energy Core
        128852, -- Infernal Brimstone Sample
        128854, -- Helheim Waylight
        128855, -- Sturdy Shield
        128856, -- Servicable Spear
        128864, -- The Rocky Hills of Highmountain
        128921, -- Spellsludge
        128922, -- Alchemical Flame
        128923, -- Spellbound Insulation
        128924, -- Decommissioned Calefactor
        128933, -- Rashar's Right Eye
        128960, -- Cleansing Orb
        129033, -- Rotting Deck
        129043, -- Signed Contract
        129047, -- Unsigned Contract
        129054, -- Okuna's Fishing Pole
        129058, -- Hatecoil Scale Patch
        129059, -- Salteye Oil
        129060, -- Oily Mak'rana Chunk
        129092, -- Hearthstone Card
        129095, -- Fel Crystal
        129105, -- Ley Dust
        129106, -- Jailer's Shard
        129161, -- Stormforged Horn
        129164, -- Ancient Vrykul Rune Tablet
        129169, -- Runic Bone Knife
        129173, -- Tattered History Tome
        129174, -- Corroded Maritime Plaque
        129176, -- Guron's Journal
        129177, -- Rotting Tarot Card
        129193, -- Leather Book Cloth
        129194, -- Metal Ring
        129197, -- Shala'nir Painting
        129203, -- Ring of Resonant Fury
        129204, -- Vial of Felsoul Blood
        129207, -- Aegis of Aggramar
        129215, -- Pulsing Ring of Resonant Fury
        129283, -- Illidari Request
        129290, -- The Scrivener's Tome
        129292, -- Stolen Shadowruby
        129293, -- Tigrid's Enchanting Rod
        129356, -- Cask of Challiane Wine
        129371, -- Symbol of Loyalty to Azshara
        129725, -- Smoldering Torch
        129740, -- Desiccated Journal
        129749, -- Argunite Fragments
        129750, -- Arcane Housing Fragment
        129756, -- Infused Clam
        129757, -- Infused Clam
        129957, -- Tome of Fel Secrets
        129963, -- Stonehide Boot Base
        129964, -- Sturdy Stonehide Boots
        129966, -- Lunarwing Egg
        129971, -- Hideshaper's Vestment
        129972, -- Vrykul Leather Binding
        129973, -- Black Rook Armor
        129974, -- Black Rook Hauberk
        129975, -- Rough Warhide Mask
        129976, -- Lohrumn's Shoulderguard
        129978, -- Drogbar Armor
        129979, -- Crystallized Ancient Mana
        129980, -- Naga Shoulderguard
        129981, -- Dusty Boot
        130069, -- Harpy Fang
        130070, -- Stalker Claw
        130072, -- Mail Armor Shipment
        130075, -- Damaged Power Source
        130076, -- Inert Power Source
        130077, -- Highmountain Leatherworking Pattern
        130078, -- Leatherworking Pattern Scrap
        130079, -- Inert Power Source
        130081, -- Crowbar
        130082, -- Rollo's Rune
        130083, -- Floki's Rune
        130088, -- Repaired Power Core
        130090, -- Stonehide Leather Bed
        130094, -- Saylanna's Rod
        130098, -- Scale of Deathwing
        130100, -- Basilisk Hide
        130104, -- Blue Dragon Scale
        130106, -- Warlord Parjesh's Hauberk
        130108, -- Cliffthorn
        130110, -- Axetail Basilisk Matriarch Scales
        130111, -- Captured Musken
        130129, -- Fel Hound Corpse
        130130, -- Felhide Bracers
        130131, -- Glowing Resonate Crystal
        130146, -- Legion Portal Key
        130197, -- Soul Fragment
        130208, -- Encyclopedia Botanica
        130210, -- Lyana's Wrathful Warglaive
        130211, -- Lyana's Vengeful Warglaive
        130212, -- Tel'anor Memento
        130252, -- Hack
        130253, -- Slash
        130255, -- Latara's Bow
        130263, -- Celea's Hauberk
        130326, -- Sashj'tar Accord
        130328, -- Sunken Rockweed
        130410, -- Intact Spikeback Gills
        130866, -- Mucktrail Snail
        130868, -- Fresh Stonehide Pelt
        130869, -- Shaved Stonehide Pelt
        130870, -- Tanned Stonehide Leather
        130872, -- Stonehide Leather Lining
        130873, -- Stonehide Boot Exterior
        130874, -- Stonehide Leather Toe Cap
        130875, -- Stonehide Leather Strip
        130877, -- Fresh Felhide
        130878, -- Shaved Felhide
        130879, -- Tanned Fel Leather
        130880, -- Fel Leather Strap
        130887, -- Stonehide Leather Barding
        130889, -- Pilfered Provisions
        130891, -- Namha's Tanning Mixture
        130892, -- Stalriss' Tanning Mixture
        130893, -- Pilfered Provisions
        130894, -- Stonehide Leather Champron
        130895, -- Stonehide Leather Crinet
        130896, -- Stonehide Leather Caparison
        130901, -- Candleking's Special Candle
        130937, -- Fel Leather Cuff
        130942, -- Shimmering Snapper Scale
        130944, -- Needle Coral
        131720, -- [addon.CONS.UNUSED]Sigil of Brutality
        131721, -- [addon.CONS.UNUSED]Sigil of Destruction
        131722, -- [addon.CONS.UNUSED]Heart of Chaos
        131723, -- [addon.CONS.UNUSED]Shard of Incandescence
        131760, -- Cleansing Ritual Focus
        131813, -- Head of Mal'kess
        131928, -- Valarjar Rune-Sigil
        131930, -- Demonstone
        131931, -- Khadgar's Wand
        131932, -- Slatewhisker Candle
        132098, -- Axetail Spine
        132102, -- Colossal Fillet
        132119, -- Orgrimmar Portal Stone
        132120, -- Stormwind Portal Stone
        132177, -- Stashed Supplies
        132244, -- Sashj'tar Fang
        132246, -- Darkshard Crystal
        132247, -- Bottle of Airspark
        132248, -- Mu'sha's Tears
        132251, -- Letter to Anduin
        132253, -- Flask of Sacred Oil
        132255, -- Bundle of Preserving Incense
        132259, -- Bag of Real Jewels
        132265, -- Crude Deadly Deep Amber
        132266, -- Crude Quick Azsunite
        132267, -- Crude Masterful Queen's Opal
        132368, -- First Piece of the Band of Farondis
        132370, -- Salteye Bangle
        132371, -- Driftwood
        132372, -- Deep Amber Nose Ring
        132373, -- Azsunite Belly Ring
        132377, -- Ariden's Compass
        132438, -- Talisman of Ascension
        132440, -- The Svalnguard
        132446, -- Jabrul's Jewels
        132462, -- Thunderhorn Flank
        132463, -- Spinesever's Spine
        132464, -- Leytusk Steak
        132471, -- Grand Feast of Valhallas
        132472, -- Fel Deconstructor
        132484, -- Well-Cut Maelstrom Sapphire
        132486, -- Gift of Appreciation
        132512, -- Tome of Blighted Implements
        132738, -- Heart of Zin-Azshari
        132744, -- Adamantium Casing Scrap
        132746, -- Lord Ravencrest's Research
        132749, -- Legion Portal Fragment
        132750, -- Greater Legion Portal Stone
        132751, -- Twisted Runebindings
        132760, -- Conducting Jewel
        132761, -- Fel-Infused Plate
        132767, -- Eye of Nashal
        132768, -- Eye of Nashal
        132769, -- Eye of Nashal
        132820, -- Dragon Glass
        132823, -- Wooden Plank
        132838, -- Crystal Shell
        132839, -- Crystal Core
        132840, -- Crystal Scale
        132841, -- Crystal Heart
        132842, -- Crystal Shard
        132843, -- Crystal Plume
        132856, -- Vrykul Crucible
        132859, -- Delayeth's Journal: The Jewels of the Isles
        132860, -- The Black Tome
        132862, -- Ring of the Fallen
        132866, -- Thrymja, Ring of the Gods
        132867, -- Nightborne Loupe
        132875, -- Second Piece of the Band of Farondis
        132876, -- Third Piece of the Band of Farondis
        132877, -- Eye of Azzorok
        132880, -- Heart of the Overlord
        132881, -- Harpy Talon
        132882, -- Lockpick Design
        132883, -- First Arcanist's Token
        132885, -- Moon Lily
        132886, -- Moon Lily
        132887, -- Bouquet of Moon Lilies
        132893, -- Weathered Telemancy Beacon
        132894, -- Cracked Warpsleeve
        132918, -- Narla's Branch
        132919, -- Drawzle's Bough
        132925, -- Tiffany's Notebook
        132976, -- Golden Hare
        133017, -- Redtooth's Head
        133063, -- Animal Bits
        133064, -- Redtooth's Head
        133065, -- Tony Mourdain's Cleaver
        133068, -- Fragrant Poppy
        133328, -- [addon.CONS.UNUSED]Jailer's Soul Cage
        133336, -- Dorion's Sword
        133337, -- Dorion's Shield
        133338, -- Dorion's Amulet
        133379, -- Arcano-Plasm
        133380, -- Arcano-Drano
        133512, -- Mistbreaker Scale
        133513, -- Spikehide Dew
        133514, -- Rigid Stick
        133515, -- Mistbreaker Pheromones
        133516, -- Sakarthiss' Trident
        133558, -- Sealed Letter
        133559, -- Mistbreaker Missile
        133581, -- Seed Pouch
        133599, -- Ancient Mana Geode
        133600, -- Ancient Mana Geode
        133669, -- Leafwind's Salve
        133675, -- Moon Guard Sigil
        133677, -- Pristine Shalebark Core
        133743, -- Fresh Fel-Flesh
        133750, -- Crate of Guns
        133752, -- Fel Reaver Husk
        133753, -- Fel Reaver Arm
        133754, -- Fel Reaver Leg
        133756, -- Fresh Mound of Flesh
        133761, -- Flintlocke's Headgun Prototype
        133772, -- Crate of Ammunition
        133775, -- Gunpowder Charges
        133780, -- Felbat Hide Scraps
        133782, -- Stormheim Cerith Shell
        133791, -- Murloc Trinket
        133793, -- Felfire Spine
        133794, -- Didi's Green Fireworks
        133797, -- Gunpack Parts
        133798, -- Barrel of Gunpowder
        133799, -- Engineering Schematic
        133801, -- Trigger Prototype
        133802, -- Runestone of Vitality
        133806, -- Arcanic Compressor
        133807, -- Legion Emblem
        133808, -- Prized Racing Snail
        133874, -- Private Stalk
        133879, -- Prismatic Felslate Diamond
        133880, -- Volatile Leytorrent Potion
        133881, -- Fel-Infused Core
        133882, -- Trap Rune
        133894, -- "Odyn's Blessing"
        133895, -- Catriona's Jewel
        133897, -- Telemancy Beacon
        133898, -- Anthology of the Guard
        133899, -- Magical Manifest of the Moon
        133900, -- Aftermath of the Well
        133910, -- Forgalash's Prow
        133924, -- Astromancer's Keystone
        133925, -- Fel Lash
        133926, -- Harpoon Parts
        133927, -- Sturdy Plank
        133934, -- Didi's Red Fireworks
        133935, -- Didi's Blue Fireworks
        133936, -- Didi's Purple Fireworks
        133943, -- Sashj'tar Harpoon
        133944, -- Rageshard
        133946, -- Plasmatic Laser Bolt
        133947, -- Unrefined Gem
        133955, -- BC Crystal
        133956, -- Volatile Spell Focus
        133957, -- Spell Focus Fragment
        133960, -- Axetail Eyeball
        133961, -- Sashj'tar Air Bladder
        133962, -- Siren Blood
        133968, -- Moon Guard Sigil
        133972, -- Basilisk Meat
        133975, -- Gnopetto's Journal
        133978, -- Deployable Bullet Dispenser Parts
        133991, -- Legion Shackle Key
        133994, -- Explosive Ice
        133995, -- Storm Brew Recipe
        133999, -- Inert Crystal
        134005, -- Odyn's Cauldron
        134009, -- Living Coral
        134010, -- Lunarwing Lily
        134011, -- Ambershard Crystal
        134014, -- Shadowbloom
        134015, -- Stinky Fish Head
        134016, -- Jar of Floog
        134027, -- Shard of Vorgos
        134028, -- Shard of Kozak
        134041, -- Bloodspattered Signet Ring
        134056, -- Wax Ingot
        134060, -- A Sizeable Pouch of Gold
        134065, -- Grimwing's Head
        134080, -- Felsoul Cage Key
        134082, -- War of the Ancients Fragment
        134083, -- Chipped Titan Disc Fragment
        134084, -- Part of the Infernal Device
        134085, -- Chamber Key Fragment
        134087, -- Page from The Purple Hills of Mac'Aree
        134088, -- Chapter from The Purple Hills of Mac'Aree
        134089, -- The Purple Hills of Mac'Aree
        134090, -- Final Chapter of The Purple Hills of Mac'Aree
        134091, -- Crumbling Titan Disc Piece
        134092, -- Chest of Shrouds
        134093, -- Ancient Highmountain Artifact
        134094, -- Ancient Highmountain Necklace
        134095, -- Bone Fragment of Eche'ro
        134104, -- Leystone-Encrusted Spike
        134105, -- Felslate-Encrusted Spike
        134106, -- Leystone-Rich Core
        134107, -- Felslate-Rich Core
        134108, -- Ancient Highborne Artifact
        134109, -- Sashj'tar Cache Key
        134114, -- Jewel of Aellis
        134115, -- Freed Artifact Fragment
        134116, -- Tattered Highborne Scroll
        134117, -- Azsunian Key Mold Piece
        134119, -- Overloaded Collar
        134120, -- Collar of Domination
        134128, -- Arcane Trap
        134549, -- Toad Gizzard
        134821, -- Harpy Necklace
        134832, -- Molten Slag
        134833, -- Newly Forged Key
        134834, -- Filled Mold
        134835, -- Metal Bar
        134836, -- Trident
        134837, -- Impure Elementium Ore
        134859, -- Lost Mail
        135464, -- Bulging Sack of Gold
        135478, -- Sealed Powder Keg
        135479, -- Lost Mail
        135488, -- Goblin Explosives
        135496, -- Boom Bait
        135501, -- Cup of Moonwater
        135502, -- Gloryndel's Satchel
        135506, -- Cup of Moonwater
        135507, -- Gemcutter's Tome
        135508, -- Lespin's Brooch
        135509, -- "The Claw Clacks for Thee"
        135513, -- Vintage Wine Bottle
        135514, -- Dalaran Cabernet
        135515, -- Manasaber Pelt
        135525, -- Lyndras' Pinking Shears
        135526, -- Box of Measuring Tools
        135527, -- Lyndras' Threading Needles
        135534, -- Heavy Torch
        135538, -- Bear Fur
        135556, -- Violet Hold Prison Key
        135558, -- Lush Grass Seed
        135563, -- Flask of Moonwell Water
        135575, -- Hymdall's Loincloth
        135587, -- Vortex Orb
        135588, -- Solstice Crystal
        135589, -- Heart of Skywall
        135590, -- Crown of the West Wind
        136271, -- Spellstone of Kel'danath
        136272, -- Shadowfen Heirlooms
        136339, -- Spellstone of Kel'danath
        136343, -- Prized Hooves
        136357, -- Shimmering Elixir of Suspension
        136358, -- Offerings to Ursol
        136363, -- Galius' Confession
        136369, -- Stormwing Scale
        136370, -- Crumbled Stormwing Skull
        136371, -- Massive Stormwing Skull
        136372, -- Archaeologist's Whistle
        136385, -- Crystallized Soul
        136386, -- Bloodstone
        136389, -- The Infernal Codex
        136391, -- Corrupted Petals
        136397, -- Dusty Disc Fragment
        136398, -- Ancient Vial
        136403, -- Staff of Four Winds
        136406, -- Dravax's Key
        136413, -- Seed of Corruption
        136414, -- Flask of Moonwell Water
        136535, -- Pyroth's Molten Core
        136536, -- Garixxia's Smoldering Heart
        136537, -- Arcane Glowlamp
        136539, -- Tanned Stonehide Leather
        136579, -- Ularogg's Crystal Charm
        136580, -- Dargrul's Crystal Charm
        136592, -- Fel Essence
        136600, -- Enchanted Party Mask
        136603, -- Rune of Portals
        136605, -- Solendra's Compassion
        136615, -- Skystone Rod
        136616, -- Radiant Zephyrite
        136617, -- Ancient Arkhana
        136622, -- Oldus' Power Core
        136623, -- Or'ell's Power Core
        136624, -- Olmoore's Power Core
        136625, -- Oll'ison's Power Core
        136673, -- Rivermane Remedy
        136674, -- Ripe Pumpkin Juice
        136675, -- Barm's Mortar and Pestle
        136680, -- Ritual Bundle
        136694, -- Ancient Scrolls of Meitre
        136723, -- Silver Hair of Khadgar
        136779, -- Empowered Bloodstone
        136784, -- Eternity Sand
        136785, -- Shadowfen Valuables
        136791, -- The Master's Journal pt. 1
        136792, -- The Master's Journal pt. 2
        136793, -- The Master's Journal pt. 3
        136802, -- Holding Cell Key
        136804, -- Idol of the Wild
        136811, -- Piscine Pigment
        136812, -- Sabelite Sulfate
        136819, -- Dragur Dust
        136820, -- Nythendra's Heart
        136821, -- Ley Pigment
        136822, -- Stolen Nar'thalas Relic
        136823, -- Deucus' Conduit List
        136827, -- Ysera's Dream Draught
        136832, -- Illegible Writings
        136833, -- Oracle's Scrying Orb
        136835, -- Dreamer's Tear Leaves
        136836, -- Nightborne Flask
        136838, -- Intact Murloc Eye
        136839, -- Egg of Gangamesh
        136840, -- Mylune's Flute
        136850, -- Stolen Ley Crystal
        136859, -- Fat Bear Liver
        136860, -- Cliffwing Hippogryph Egg
        136931, -- The Masterful Miller's Manual
        136968, -- Storm Dragon Egg
        136970, -- Mask of Mirror Image
        136981, -- Empyrean Fel Tome
        136983, -- Grave Dust
        136985, -- Can of Overheated Oil
        136986, -- Stag Blood Sample
        136987, -- Aged Snowplum Brandy
        137011, -- Corked Bottle
        137110, -- Book of Exalted Deeds
        137111, -- Tinhead Chest
        137112, -- Tinhead Belt
        137113, -- Tinhead Pants
        137114, -- Tinhead Boots
        137115, -- Red Riding Hood Boots
        137116, -- Red Riding Hood Skirt
        137117, -- Red Riding Hood Robe
        137120, -- Stack Of Vellums
        137122, -- Dalaran Mage Boots
        137123, -- Dalaran Mage Belt
        137179, -- Grotesque Ettin Leather
        137182, -- Crown of the Forgotten King
        137183, -- Stormcloak Signet
        137184, -- Drakerider's Medallion
        137185, -- Rune of Dominance
        137186, -- Crate of Enchanting Vellums
        137187, -- Felsoul Ink
        137189, -- Satyr Horn
        137190, -- Rune of Opening
        137193, -- Empowered Ambershard Crystal
        137196, -- Thunder Dust
        137206, -- Tidestone of Golganneth
        137211, -- Nalamya's Book of Enchantments
        137213, -- Raven Dust
        137235, -- Tigrid's Arkhana
        137271, -- Archaeology Fragment Sample
        137273, -- Empyrean Rune Fragment
        137277, -- Enchanted Pen and Paper
        137290, -- Idol of the Paw
        137291, -- Idol of the Claw
        137292, -- Idol of the Moon
        137295, -- Bottle of Arcwine
        137299, -- Nightborne Spellblade
        137330, -- Manastalker Tendril
        137335, -- Felsurge Spider Egg
        137383, -- Speckled Pearl
        137422, -- Courtship Rituals of the Skrog
        147354, -- Lightbound Runestone
        147491, -- Sealed Missive
        147763, -- Fragmented Prayers
        147878, -- Prophet's Guidestone
        147879, -- Prophet's Guidestone
        147899, -- Sequenced Quest Chest Item
        150578, -- Void Cleansing Crystal
        150942, -- Argunite Crystal
        150993, -- Army of the Light Code Book
        150995, -- Enigmatic Legion Orders
        151028, -- Demon Soul
        151061, -- Chewed Eredar Bones
        151088, -- Heart of Nhal'athoth
        151090, -- The Crest of Knowledge
        151126, -- Fragment of Acuity
        151127, -- Fragment of Wit
        151128, -- Fragment of Guile
        151188, -- Devastator Energy Cell
        151366, -- Partially Digested Arcana
        151367, -- Mark of Cunning
        151476, -- Sigil of Awakening
        151555, -- Crystallized Memory
        151563, -- Hallowed Prayer Effigy
        151570, -- Lightbound Crystal
        151624, -- Y'mera's Arcanocrystal
        151837, -- Invocation Array
        151838, -- Judgment Core
        151839, -- Vindicator Plating
        151845, -- Used Vial
        151851, -- Immaculate Felcore
        151856, -- Withered Astral Glory
        151857, -- Adolescent Astral Glory
        151858, -- Astral Glory Root Cluster
        151860, -- Empyrium Deposit Chunk
        151861, -- Empyrium Dust
        151862, -- Unusable Empyrium Core
        151863, -- Empyrium Seam Chunk
        151864, -- Embedded Empyrium Ore
        151865, -- Empyrium Bits
        151866, -- Tainted Scraps
        151867, -- Spoiled Fiendish Leather
        151868, -- Corrupted Tooth
        151871, -- Crate of Felslate
        151872, -- Empyrial Breastplate
        151874, -- Lightweave Loom
        151876, -- Vorel's Design Book
        151878, -- Empyrial Crown
        151879, -- Purified Fiendish Leather
        151880, -- Enstraa's Tanning Oil
        151923, -- Empyrial Rivet
        151924, -- Empyrial Chest Plate
        151925, -- Empyrial Back Plate
        151926, -- Ionized Geographical Recorder
        151930, -- Tears of the Naaru
        151931, -- Empyrial Circlet
        151932, -- Empyrial Hesselian Setting
        151933, -- Empyrial Florid Malachite Setting
        152110, -- Talisman of the Prophet
        152200, -- Dendrite Cluster
        152201, -- Armory Key Fragment
        152202, -- Armor of the Triumvirate
        152203, -- Reforged Armory Key
        152204, -- Glowing Key Fragment
        152205, -- Glowing Key Fragment
        152357, -- Vigilant Power Crystal
        152407, -- Argunite Keystone
        152408, -- Stolen Pylon Core
        152411, -- Delicate Panthara Scales
        152415, -- Clotted Void Crystal
        152465, -- Banishment Stone Shard
        152472, -- Chieftain's Salve
        152575, -- Birth of the Ur'zul
        152593, -- Essence of Light
        152594, -- Essence of Shadow
        152602, -- Remnant of Nhal'athoth
        152648, -- Brightsoul Scepter
        152657, -- Target Designator
        152689, -- Crystalline Construct Core
        152714, -- Ancient Augari Essence
        152971, -- Talisman of the Prophet
        152995, -- Sacred Stone
        153021, -- Intact Demon Eye
        153125, -- Vestige of Light
        153249, -- Y'mera's Attuning Crystal
        129370, -- Dust of Betrayal
        129751, -- Dust of Foul Lies
        144363, -- Blood-Soaked Invitation
        150938, -- Demonic Corestone
        150939, -- Soul Crystal Fragment
        150940, -- Soul Crystal
        150941, -- Voidcallers' Scroll
        150943, -- Smoldering Chronoshard
        150944, -- Fel-Touched Chronoshard
        150945, -- Gleaming Chronoshard
        150946, -- Frozen Chronoshard
        151141, -- Darkened Scrap of Vellum
        151166, -- Nightmare-Catcher
        151167, -- Twisted Fiber
        151168, -- Emeraldine Plume
        151280, -- Fras' Special Pipe Blend
        151281, -- Salted Venison Jerky
        151282, -- Refurbished Military Rifle
        151283, -- Basic Cloth Bandages
        151284, -- Flinty Firestarter
        151285, -- Keg of Booty Bay Rum
        151286, -- Heavy Straw Rope
        151287, -- Marigold Bouquet
        151347, -- Town Hall Door Key
        151368, -- Experimental Alchemy Reagent
        151369, -- Lightning Absorption Capsule
        151370, -- Military Explosives
        135511, -- Thick Slab of Bacon
        137212, -- Cut Skystone
        137214, -- Cut Deep Amber
        137215, -- Cut Azsunite
        137195, -- Highmountain Armor
        137221, -- Enchanted Raven Sigil
        137286, -- Fel-Crusted Rune
        139277, -- Historian's Badge
        140378, -- Ariana Fireheart
        141878, -- Arcane-Infused Vial
        142208, -- Essence of Wyrmtongue
        142213, -- Empowered Arcane Ward
        142344, -- Broken Axe Blade
        142375, -- Dispelling Crystal
        142377, -- Badly Broken Dark Spear
        142399, -- Experimental Targeting Orb
        142400, -- Advanced Targeting Orb
        142491, -- Experimental Telemancy Orb
        142509, -- Withered Targeting Orb
        143845, -- "Jewelry"
        143865, -- Abyssal Crest
        143900, -- Letter from Maximillian
        144073, -- Ship Mast
        144074, -- Mainsail
        144075, -- Waxy Reeds
        144076, -- Rigging Rope
        144077, -- Submarine Tar
        144235, -- Wand of Dispelling
        146308, -- Barrel of Storm Brew
        146896, -- Hozen Vaulting Staff
        147582, -- Mark of the Sentinax
        151825, -- Horn of Hatuun
        152419, -- Fossilized Resin
        152466, -- Jewel of the Vindicaar
        152467, -- Lightforged Satchel Charge
        128713, -- Eight of Immortality
        128714, -- Seven of Immortality
        128715, -- Six of Immortality
        128716, -- Four of Immortality
        128717, -- Five of Immortality
        128718, -- Three of Immortality
        128719, -- Two of Immortality
        128720, -- Ace of Immortality
        128721, -- Eight of Promises
        128722, -- Seven of Promises
        128723, -- Six of Promises
        128724, -- Five of Promises
        128725, -- Four of Promises
        128726, -- Three of Promises
        128727, -- Two of Promises
        128728, -- Ace of Promises
        128729, -- Eight of Dominion
        128730, -- Seven of Dominion
        128731, -- Six of Dominion
        128732, -- Five of Dominion
        128733, -- Four of Dominion
        128734, -- Three of Dominion
        128735, -- Two of Dominion
        128736, -- Ace of Dominion
        128737, -- Eight of Hellfire
        128738, -- Seven of Hellfire
        128739, -- Six of Hellfire
        128740, -- Five of Hellfire
        128741, -- Four of Hellfire
        128742, -- Three of Hellfire
        128743, -- Two of Hellfire
        128744, -- Ace of Hellfire
        140778, -- Traveler's Banking Chest
        146678, -- Vandros' Shadoweave Robes
        146952, -- Legendary Tailor's Materials
        146977, -- Legendary Tanner's Kit
        147075, -- Spirit Net
        147197, -- Legends of the Four Hammers
        147207, -- Legendary Blacksmithing Supplies
        147209, -- Legendary Wardenscale Supplies
        147281, -- Nights with the Nightborne
        147906, -- Fel Heart of Argus
        151546, -- Stratholme Gate Key
        151547, -- Brimstone Beacon
        151548, -- Tyrande's Moonstone
        151549, -- Cenarion Circle Documents
        143776, -- Shrouded Timewarped Coin
        124023, -- Leystone Armor Set
        127265, -- Silkweave Set
        128777, -- Heated Leystone Bar
        132748, -- Moonstone
        134058, -- Teleport: Hall of the Guardian
        139349, -- Stormy Gland
        139384, -- Pristine Harpy Feather
        139385, -- Reflective Scale
        139386, -- Essence of Pure Spirit
        139457, -- Odyn's Challenge
        139471, -- Black Rook Missive
        139472, -- Glamorous Party Invitation
        139473, -- Vial of Poisoned Blood
        139474, -- Carved Shell
        139475, -- Waterlogged Letter
        139476, -- Broken Dragon Scale
        139477, -- Drained Mana Orb
        139478, -- Broken Warden Helm
        139479, -- Broken Portal Shard
        139480, -- Corrupted Egg Fragment
        139481, -- Corrupted Egg Fragment
        139482, -- Corrupted Egg Fragment
        139483, -- Corrupted Egg Fragment
        140147, -- Pulsauron Bindings
        140149, -- Pulsauron Bindings
        140151, -- Pulsauron Bindings
        140153, -- Pulsauron Bindings
        140163, -- Tainted Moonglade Leaf
        140164, -- Vial of Swirling Shadow
        140165, -- Tainted Moonglade Leaf
        140166, -- Vial of Swirling Shadow
        140167, -- Tainted Moonglade Leaf
        140168, -- Vial of Swirling Shadow
        140169, -- Tainted Moonglade Leaf
        140170, -- Vial of Swirling Shadow
        140171, -- Glowing Lily
        140172, -- Ancient Nightborne Tome
        140173, -- Shred of Tattered Robe
        140174, -- Glowing Lily
        140175, -- Ancient Nightborne Tome
        140177, -- Shred of Tattered Robe
        140178, -- Glowing Lily
        140180, -- Shred of Tattered Robe
        140181, -- Glowing Lily
        140182, -- Ancient Nightborne Tome
        140183, -- Shred of Tattered Robe
        140472, -- Ancient Nightborne Tome
        140495, -- Torn Invitation
        140774, -- Vault Ticket
        141185, -- Broken Portal Shard
        141186, -- Broken Portal Shard
        141187, -- Broken Warden Helm
        141188, -- Broken Warden Helm
        141189, -- Drained Mana Orb
        141190, -- Drained Mana Orb
        141191, -- Broken Dragon Scale
        141192, -- Broken Dragon Scale
        141193, -- Waterlogged Letter
        141194, -- Waterlogged Letter
        141195, -- Odyn's Challenge
        141196, -- Odyn's Challenge
        141197, -- Carved Shell
        141198, -- Carved Shell
        141199, -- Vial of Poisoned Blood
        141200, -- Vial of Poisoned Blood
        141201, -- Glamorous Party Invitation
        141202, -- Glamorous Party Invitation
        141203, -- Black Rook Missive
        141204, -- Black Rook Missive
        142210, -- Celestial Invitation
        142246, -- Broken Pocket Watch
        142338, -- Karazhan Item - Normal
        142339, -- Karazhan Item - Heroic
        142340, -- Head of Thar'zul
        142522, -- Rude Letter
        142553, -- Tuft of Dwarvish Beard
        142554, -- Broken Broom Handle
        143328, -- Battle Report
        143329, -- Dire Summons
        143478, -- Arena Invitation
        143479, -- Frayed Banner
        143561, -- Exhausted Shadow Candle
        143590, -- Hunk of Meatball
        143867, -- Twilight Cultist Medallion of Station
        146680, -- Melandrus' Star-Touched Bracers
        146682, -- Suramari Soul-Wraps
        146684, -- Ancient Imbued Silkweave Armor
        146686, -- Colorless Shroud of Xavius
        146688, -- Waterlogged Handmaiden's Gloves
        146690, -- Xavius' Torn Spellsash
        146692, -- Ancient Dreadleather Armor
        146694, -- Chipped Felsong Mantle
        146696, -- Rusted Legplates of Mephistroth
        146698, -- Torn Bracers of the First War
        146700, -- Ancient Gravenscale Armor
        146702, -- White Lightningsteel
        146704, -- Red-Hot Draconic Slag
        146706, -- Frostveined Shale
        146708, -- Ancient Demonsteel Armor
        147497, -- Encrusted Naga Scale
        147498, -- Encrusted Naga Scale
        147499, -- Encrusted Naga Scale
        147500, -- Encrusted Naga Scale
        147501, -- Worshipper's Scrawlings
        147502, -- Worshipper's Scrawlings
        147503, -- Worshipper's Scrawlings
        147504, -- Worshipper's Scrawlings
        147505, -- Wailing Soul
        147506, -- Wailing Soul
        147507, -- Wailing Soul
        147508, -- Wailing Soul
        147509, -- Seal of the Deceiver
        147510, -- Seal of the Deceiver
        147511, -- Seal of the Deceiver
        147512, -- Seal of the Deceiver
        152313, -- Azeroth Invasion Plans
        152314, -- Azeroth Invasion Plans
        152315, -- Azeroth Invasion Plans
        152316, -- Azeroth Invasion Plans
        152317, -- Discharged Shock Lance
        152318, -- Discharged Shock Lance
        152319, -- Discharged Shock Lance
        152320, -- Discharged Shock Lance
        152321, -- Sargerei Manifesto
        152322, -- Sargerei Manifesto
        152323, -- Sargerei Manifesto
        152324, -- Sargerei Manifesto
        152325, -- Sanguine Argunite
        152326, -- Sanguine Argunite
        152327, -- Sanguine Argunite
        152328, -- Sanguine Argunite
        152470, -- Matrix Uplink
        141305, -- Essence of Clarity
        141304, -- Essence of Clarity
        141303, -- Essence of Clarity
        143656, -- Echo of Time
        143657, -- Echo of Time
        143658, -- Echo of Time
        151248, -- Fragment of the Guardian's Seal
        151249, -- Fragment of the Guardian's Seal
        151250, -- Fragment of the Guardian's Seal
        152902, -- Rune of Passage
        152906, -- Rune of Passage
        152907, -- Rune of Passage
        152908, -- Sigil of the Dark Titan
        152909, -- Sigil of the Dark Titan
        152910, -- Sigil of the Dark Titan
        136352, -- Archmage Karlain's Imbued Silkweave Robe
        152900, -- Blood of the Unmaker
        136351, -- Handful of Obliterum Ash
        146978, -- Handful of Obliterum Ash
        134857, -- Invincible's Reins
        135463, -- Invincible's Reins
        143866, -- Twilight Cultist Ring of Lordship
        146653, -- Ancient Shard of Binding
        146955, -- Potentially Precious Gem
        146956, -- Promising Treasure Key
        146957, -- Mysterious Potion
        146958, -- Wonderfully-Adorned Cloth
        147437, -- Satchel of Starweave and Shadowcloth
        147438, -- Bag of Wisp-Touched Elderhide
        147439, -- Box of Completed Prime Wardenscale
        147443, -- Carrying Case For Hammer of Forgotten Heroes
        154880, -- Cursed Shard
        132179, -- Hammer of Khaz'goroth
        136604, -- Emberscar, Brand of Damnation
        139000, -- Damaged Thunderfury
        139037, -- Thunderfury, Blessed Blade of the Windseeker
        147451, -- Armorcrafter's Commendation
        138992, -- Grand Marshal's Medal of Valor
        138996, -- High Warlord's Medal of Valor
        139400, -- Ancient Magic and How to Wield It Without Destroying the World
        139401, -- The Fate of Aegwynn
        139402, -- The Fel Tome of Vorgalus Dor
        140090, -- Which Came First, the Tree or the Seed?
        140091, -- The Mysteries of the Wild Gods
        140092, -- Druids of the Pack
        140094, -- Ancient Magic and How to Wield It Without Destroying the World
        140095, -- On Plagues, Curses, and Blights
        140096, -- The Fel Tome of Vorgalus Dor
        140098, -- The Last Vestiges of the Illidari
        140099, -- The Codex of the Dead
        140100, -- The Fel Tome of Vorgalus Dor
        140102, -- The Alliance Expedition to Draenor
        140103, -- The Mysteries of the Wild Gods
        140104, -- The False Keeper
        140106, -- The Sacrifice of Emperor Shaohao
        140107, -- The Codex of the Dead
        140108, -- Beyond the Throne of Air
        140110, -- Liber Monasterium
        140111, -- The Winterskorn War
        140112, -- The Age of Galakrond
        140114, -- Touch of the Abyss
        140115, -- The Corruption of the Eredar and the Flight of the Draenei
        140116, -- Modgud's Doom
        140118, -- The Day the Horde Came
        140119, -- The Codex of the Dead
        140120, -- The Fel Tome of Vorgalus Dor
        140122, -- The Banishing of the Elemental Lords
        140123, -- The Song of Scales
        140124, -- Beyond the Throne of Air
        140126, -- Ancient Magic and How to Wield It Without Destroying the World
        140127, -- The Fate of Aegwynn
        140128, -- The Fel Tome of Vorgalus Dor
        140130, -- The Warring Tribes and the Rise of Arathor
        140131, -- The Winterskorn War
        140132, -- The Age of Galakrond
        139466, -- Bindings of the Windlord
        139468, -- Bindings of the Windlord
        132560, -- Essence of the Whirlwind
        132745, -- Essence of the Whirlwind
        139671, -- Deathglare Iris
        139672, -- Horn of the Nightmare Lord
        139706, -- Corrupted Essence
        142362, -- ZZZ OLD Fel-Etched Bone
        142371, -- ZZZ OLD Inferno Stone
        142373, -- ZZZ OLD Locket of Eldre'Thalas
        142376, -- ZZZ OLD Glowing Bloodthistle Petal
        142378, -- ZZZ OLD Vial of Ancient Mana
        142382, -- Infiltration Plans
        142384, -- Depleted Nether Crystal
        142392, -- Thief's Instructions
        142394, -- "Ancient" Elven War Orders
        142395, -- Fel Infused Totem Fragment
        142396, -- Damaged Kirin Tor Insignia
        142397, -- Gilded Scroll Case
        143840  -- Excaliberto
    },
    [addon.CONS.RECIPES_ID] = {
        [addon.CONS.R_BOOKS_ID] = {
            136780, -- Pet Training Manual: Play Dead
            136781, -- Pet Training Manual: Fetch
            136782, -- Fireworks Instruction Manual
            136783, -- The Art of Concealment
            136787, -- Tome of the Wilds: Treant Form
            136789, -- Tome of the Wilds: Stag Form
            136790, -- Tome of the Wilds: Track Beasts
            136794, -- Tome of the Wilds: Flap
            136795, -- Tome of the Wilds: Charm Woodland Creature
            136796, -- Necrophile Tome: Corpse Explosion
            136797, -- Mystical Tome: Arcane Linguist
            136799, -- Mystical Tome: Illusion
            136800, -- Meditation Manual: Zen Flight
            136801, -- Divine Tome: Contemplation
            136803, -- Dirty Tricks, Vol 1: Detection
            136938, -- Tome of Hex: Compy
            136969, -- Tome of Hex: Spider
            136971, -- Tome of Hex: Snake
            136972, -- Tome of Hex: Cockroach
            136419, -- Excavator's Notebook
            147580, -- Tome of the Hybrid Beast
            147770  -- Tome of the PTR Beast
        },
        [addon.CONS.R_ALCHEMY_ID] = {
            127898, -- Recipe: Ancient Healing Potion
            127899, -- Recipe: Ancient Mana Potion
            127900, -- Recipe: Ancient Rejuvenation Potion
            127901, -- Recipe: Draught of Raw Magic
            127902, -- Recipe: Sylvan Elixir
            127903, -- Recipe: Avalanche Elixir
            127904, -- Recipe: Skaggldrynk
            127905, -- Recipe: Skystep Potion
            127906, -- Recipe: Infernal Alchemist Stone
            127907, -- Recipe: Potion of Deadly Grace
            127908, -- Recipe: Potion of the Old War
            127909, -- Recipe: Unbending Potion
            127910, -- Recipe: Leytorrent Potion
            127911, -- Recipe: Flask of the Whispered Pact
            127912, -- Recipe: Flask of the Seventh Demon
            127913, -- Recipe: Flask of the Countless Armies
            127914, -- Recipe: Flask of Ten Thousand Scars
            127915, -- Recipe: Spirit Cauldron
            127917, -- Recipe: Ancient Healing Potion
            127918, -- Recipe: Ancient Mana Potion
            127919, -- Recipe: Ancient Rejuvenation Potion
            127920, -- Recipe: Draught of Raw Magic
            127921, -- Recipe: Sylvan Elixir
            127922, -- Recipe: Avalanche Elixir
            127923, -- Recipe: Skaggldrynk
            127924, -- Recipe: Skystep Potion
            127925, -- Recipe: Infernal Alchemist Stone
            127926, -- Recipe: Potion of Deadly Grace
            127927, -- Recipe: Potion of the Old War
            127928, -- Recipe: Unbending Potion
            127929, -- Recipe: Leytorrent Potion
            127930, -- Recipe: Flask of the Whispered Pact
            127931, -- Recipe: Flask of the Seventh Demon
            127932, -- Recipe: Flask of the Countless Armies
            127933, -- Recipe: Flask of Ten Thousand Scars
            127934, -- Recipe: Spirit Cauldron
            127935, -- Recipe: Ancient Healing Potion
            127936, -- Recipe: Ancient Mana Potion
            127937, -- Recipe: Ancient Rejuvenation Potion
            127938, -- Recipe: Draught of Raw Magic
            127939, -- Recipe: Sylvan Elixir
            127940, -- Recipe: Avalanche Elixir
            127941, -- Recipe: Skaggldrynk
            127942, -- Recipe: Skystep Potion
            127943, -- Recipe: Infernal Alchemist Stone
            127944, -- Recipe: Potion of Deadly Grace
            127945, -- Recipe: Potion of the Old War
            127946, -- Recipe: Unbending Potion
            127947, -- Recipe: Leytorrent Potion
            127948, -- Recipe: Flask of the Whispered Pact
            127949, -- Recipe: Flask of the Seventh Demon
            127950, -- Recipe: Flask of the Countless Armies
            127951, -- Recipe: Flask of Ten Thousand Scars
            127952, -- Recipe: Spirit Cauldron
            128209, -- Recipe: Wild Transmutation
            128210, -- Recipe: Wild Transmutation
            128211, -- Recipe: Wild Transmutation
            142119, -- Recipe: Potion of Prolonged Power
            142120, -- Recipe: Potion of Prolonged Power
            142121, -- Recipe: Potion of Prolonged Power
            151657, -- Recipe: Lightblood Elixir
            151658, -- Recipe: Lightblood Elixir
            151659, -- Recipe: Lightblood Elixir
            151703, -- Recipe: Tears of the Naaru
            151704, -- Recipe: Tears of the Naaru
            151705, -- Recipe: Tears of the Naaru
            151706, -- Recipe: Astral Alchemist Stone
            151707, -- Recipe: Astral Alchemist Stone
            151708, -- Recipe: Astral Alchemist Stone
            151710, -- Recipe: Transmute Primal Sargerite
            152616, -- Recipe: Astral Healing Potion
            152617, -- Recipe: Astral Healing Potion
            152618, -- Recipe: Astral Healing Potion
            152620, -- Recipe: Astral Mana Potion
            152621, -- Recipe: Astral Mana Potion
            152622  -- Recipe: Astral Mana Potion
        },
        [addon.CONS.R_BLACKSMITHING_ID] = {
            123899, -- Recipe: Leystone Armguards
            123900, -- Recipe: Leystone Waistguard
            123901, -- Recipe: Leystone Pauldrons
            123902, -- Recipe: Leystone Greaves
            123903, -- Recipe: Leystone Helm
            123904, -- Recipe: Leystone Gauntlets
            123905, -- Recipe: Leystone Boots
            123906, -- Recipe: Leystone Breastplate
            123920, -- Recipe: Demonsteel Armguards
            123921, -- Recipe: Demonsteel Waistguard
            123922, -- Recipe: Demonsteel Pauldrons
            123923, -- Recipe: Demonsteel Greaves
            123924, -- Recipe: Demonsteel Helm
            123925, -- Recipe: Demonsteel Gauntlets
            123926, -- Recipe: Demonsteel Boots
            123927, -- Recipe: Demonsteel Breastplate
            123928, -- Recipe: Leystone Armguards
            123929, -- Recipe: Leystone Waistguard
            123930, -- Recipe: Leystone Pauldrons
            123931, -- Recipe: Leystone Greaves
            123932, -- Recipe: Leystone Helm
            123933, -- Recipe: Leystone Gauntlets
            123934, -- Recipe: Leystone Boots
            123935, -- Recipe: Leystone Breastplate
            123936, -- Recipe: Leystone Armguards
            123937, -- Recipe: Leystone Waistguard
            123938, -- Recipe: Leystone Pauldrons
            123939, -- Recipe: Leystone Breastplate
            123940, -- Recipe: Demonsteel Armguards
            123941, -- Recipe: Demonsteel Waistguard
            123942, -- Recipe: Demonsteel Pauldrons
            123943, -- Recipe: Demonsteel Greaves
            123944, -- Recipe: Demonsteel Helm
            123945, -- Recipe: Demonsteel Gauntlets
            123946, -- Recipe: Demonsteel Boots
            123947, -- Recipe: Demonsteel Breastplate
            123948, -- Recipe: Demonsteel Armguards
            123949, -- Recipe: Demonsteel Waistguard
            123950, -- Recipe: Demonsteel Pauldrons
            123951, -- Recipe: Demonsteel Greaves
            123952, -- Recipe: Demonsteel Helm
            123953, -- Recipe: Demonsteel Gauntlets
            123954, -- Recipe: Demonsteel Boots
            123955, -- Recipe: Demonsteel Breastplate
            123957, -- Recipe: Leystone Hoofplates
            124462, -- Recipe: Demonsteel Bar
            124463, -- Mining Technique: Leystone Deposit
            124464, -- Mining Technique: Leystone Deposit
            124465, -- Mining Technique: Leystone Deposit
            124467, -- Mining Technique: Leystone Seam
            124468, -- Mining Technique: Leystone Seam
            124469, -- Mining Technique: Leystone Seam
            124471, -- Mining Technique: Living Leystone
            124472, -- Mining Technique: Living Leystone
            124473, -- Mining Technique: Living Leystone
            124475, -- Mining Technique: Felslate Deposit
            124476, -- Mining Technique: Felslate Deposit
            124477, -- Mining Technique: Felslate Deposit
            124479, -- Mining Technique: Felslate Seam
            124480, -- Mining Technique: Felslate Seam
            124481, -- Mining Technique: Felslate Seam
            124483, -- Mining Technique: Living Felslate
            124484, -- Mining Technique: Living Felslate
            124485, -- Mining Technique: Living Felslate
            124487, -- Mining Technique: Infernal Brimstone
            124488, -- Mining Technique: Infernal Brimstone
            136696, -- Recipe: Terrorspike
            136697, -- Recipe: Gleaming Iron Spike
            136698, -- Recipe: Consecrated Spike
            136699, -- Recipe: Flamespike
            136709, -- Recipe: Demonsteel Stirrups
            137605, -- Recipe: Leystone Boots
            137606, -- Recipe: Leystone Gauntlets
            137607, -- Recipe: Leystone Helm
            137680, -- Recipe: Leystone Greaves
            137687, -- Recipe: Fel Core Hound Harness
            151709, -- Recipe: Felslate Anchor
            151711, -- Recipe: Empyrial Breastplate
            151712, -- Recipe: Empyrial Breastplate
            151713, -- Recipe: Empyrial Breastplate
            142279, -- Plans: Windforged Rapier
            142282, -- Plans: Stormforged Axe
            142283, -- Plans: Skyforged Great Axe
            142284, -- Plans: Stoneforged Claymore
            142286, -- Plans: Lavaforged Warhammer
            142287, -- Plans: Great Earthforged Hammer
            142346, -- Plans: Bleakwood Hew
            142357, -- Plans: Dawn's Edge
            142358, -- Plans: Blazing Rapier
            142383, -- Plans: Darkspear
            142337, -- Plans: Blight
            142370, -- Plans: Arcanite Champion
            142402  -- Plans: Light Earthforged Blade
        },
        [addon.CONS.R_COOKING_ID] = {
            133810, -- Recipe: Salt and Pepper Shank
            133812, -- Recipe: Deep-Fried Mossgill
            133813, -- Recipe: Pickled Stormray
            133814, -- Recipe: Faronaar Fizz
            133815, -- Recipe: Spiced Rib Roast
            133816, -- Recipe: Leybeque Ribs
            133817, -- Recipe: Suramar Surf and Turf
            133818, -- Recipe: Barracuda Mrglgagh
            133819, -- Recipe: Koi-Scented Stormray
            133820, -- Recipe: Drogbar-Style Salmon
            133821, -- Recipe: The Hungry Magister
            133822, -- Recipe: Azshari Salad
            133823, -- Recipe: Nightborne Delicacy Platter
            133824, -- Recipe: Seed-Battered Fish Plate
            133825, -- Recipe: Fishbrul Special
            133826, -- Recipe: Dried Mackerel Strips
            133827, -- Recipe: Bear Tartare
            133828, -- Recipe: Fighter Chow
            133829, -- Recipe: Hearty Feast
            133830, -- Recipe: Lavish Suramar Feast
            133831, -- Recipe: Salt and Pepper Shank
            133832, -- Recipe: Deep-Fried Mossgill
            133833, -- Recipe: Pickled Stormray
            133834, -- Recipe: Faronaar Fizz
            133835, -- Recipe: Spiced Rib Roast
            133836, -- Recipe: Leybeque Ribs
            133837, -- Recipe: Suramar Surf and Turf
            133838, -- Recipe: Barracuda Mrglgagh
            133839, -- Recipe: Koi-Scented Stormray
            133840, -- Recipe: Drogbar-Style Salmon
            133841, -- Recipe: The Hungry Magister
            133842, -- Recipe: Azshari Salad
            133843, -- Recipe: Nightborne Delicacy Platter
            133844, -- Recipe: Seed-Battered Fish Plate
            133845, -- Recipe: Fishbrul Special
            133846, -- Recipe: Dried Mackerel Strips
            133847, -- Recipe: Bear Tartare
            133848, -- Recipe: Fighter Chow
            133849, -- Recipe: Hearty Feast
            133850, -- Recipe: Lavish Suramar Feast
            133851, -- Recipe: Salt and Pepper Shank
            133852, -- Recipe: Deep-Fried Mossgill
            133853, -- Recipe: Pickled Stormray
            133854, -- Recipe: Faronaar Fizz
            133855, -- Recipe: Spiced Rib Roast
            133856, -- Recipe: Leybeque Ribs
            133857, -- Recipe: Suramar Surf and Turf
            133858, -- Recipe: Barracuda Mrglgagh
            133859, -- Recipe: Koi-Scented Stormray
            133860, -- Recipe: Drogbar-Style Salmon
            133861, -- Recipe: The Hungry Magister
            133862, -- Recipe: Azshari Salad
            133863, -- Recipe: Nightborne Delicacy Platter
            133864, -- Recipe: Seed-Battered Fish Plate
            133865, -- Recipe: Fishbrul Special
            133866, -- Recipe: Dried Mackerel Strips
            133867, -- Recipe: Bear Tartare
            133868, -- Recipe: Fighter Chow
            133869, -- Recipe: Hearty Feast
            133870, -- Recipe: Lavish Suramar Feast
            133871, -- Recipe: Crispy Bacon
            133872, -- Recipe: Crispy Bacon
            133873, -- Recipe: Crispy Bacon
            142331, -- Recipe: Spiced Falcosaur Omelet
            152565, -- Recipe: Feast of the Fishes
            129180  -- Recipe: Fighter Chow
        },
        [addon.CONS.R_ENCHANTING_ID] = {
            128562, -- Formula: Enchant Ring - Word of Critical Strike
            128563, -- Formula: Enchant Ring - Word of Haste
            128564, -- Formula: Enchant Ring - Word of Mastery
            128565, -- Formula: Enchant Ring - Word of Versatility
            128566, -- Formula: Enchant Ring - Binding of Critical Strike
            128567, -- Formula: Enchant Ring - Binding of Haste
            128568, -- Formula: Enchant Ring - Binding of Mastery
            128569, -- Formula: Enchant Ring - Binding of Versatility
            128570, -- Formula: Enchant Cloak - Word of Strength
            128571, -- Formula: Enchant Cloak - Word of Agility
            128572, -- Formula: Enchant Cloak - Word of Intellect
            128573, -- Formula: Enchant Cloak - Binding of Strength
            128574, -- Formula: Enchant Cloak - Binding of Agility
            128575, -- Formula: Enchant Cloak - Binding of Intellect
            128576, -- Formula: Enchant Neck - Mark of the Claw
            128577, -- Formula: Enchant Neck - Mark of the Distant Army
            128578, -- Formula: Enchant Neck - Mark of the Hidden Satyr
            128579, -- Formula: Enchant Ring - Word of Critical Strike
            128580, -- Formula: Enchant Ring - Word of Haste
            128581, -- Formula: Enchant Ring - Word of Mastery
            128582, -- Formula: Enchant Ring - Word of Versatility
            128583, -- Formula: Enchant Ring - Binding of Critical Strike
            128584, -- Formula: Enchant Ring - Binding of Haste
            128585, -- Formula: Enchant Ring - Binding of Mastery
            128586, -- Formula: Enchant Ring - Binding of Versatility
            128587, -- Formula: Enchant Cloak - Word of Strength
            128588, -- Formula: Enchant Cloak - Word of Agility
            128589, -- Formula: Enchant Cloak - Word of Intellect
            128590, -- Formula: Enchant Cloak - Binding of Strength
            128591, -- Formula: Enchant Cloak - Binding of Agility
            128592, -- Formula: Enchant Cloak - Binding of Intellect
            128593, -- Formula: Enchant Neck - Mark of the Claw
            128594, -- Formula: Enchant Neck - Mark of the Distant Army
            128595, -- Formula: Enchant Neck - Mark of the Hidden Satyr
            128596, -- Formula: Enchant Ring - Word of Critical Strike
            128597, -- Formula: Enchant Ring - Word of Haste
            128598, -- Formula: Enchant Ring - Word of Mastery
            128599, -- Formula: Enchant Ring - Word of Versatility
            128600, -- Formula: Enchant Ring - Binding of Critical Strike
            128601, -- Formula: Enchant Ring - Binding of Haste
            128602, -- Formula: Enchant Ring - Binding of Mastery
            128603, -- Formula: Enchant Ring - Binding of Versatility
            128604, -- Formula: Enchant Cloak - Word of Strength
            128605, -- Formula: Enchant Cloak - Word of Agility
            128606, -- Formula: Enchant Cloak - Word of Intellect
            128607, -- Formula: Enchant Cloak - Binding of Strength
            128608, -- Formula: Enchant Cloak - Binding of Agility
            128609, -- Formula: Enchant Cloak - Binding of Intellect
            128611, -- Formula: Enchant Neck - Mark of the Distant Army
            128612, -- Formula: Enchant Neck - Mark of the Hidden Satyr
            128617, -- Formula: Enchant Gloves - Legion Herbalism
            128618, -- Formula: Enchant Gloves - Legion Mining
            128619, -- Formula: Enchant Gloves - Legion Skinning
            128620, -- Formula: Enchant Gloves - Legion Surveying
            128622, -- Formula: Enchanted Torch
            136702, -- Formula: Soul Fibril
            136704, -- Formula: Immaculate Fibril
            140634, -- Formula: Ley Shatter
            141911, -- Formula: Enchant Neck - Mark of the Heavy Hide
            141912, -- Formula: Enchant Neck - Mark of the Trained Soldier
            141913, -- Formula: Enchant Neck - Mark of the Ancient Priestess
            141914, -- Formula: Enchant Neck - Mark of the Heavy Hide
            141915, -- Formula: Enchant Neck - Mark of the Trained Soldier
            141916, -- Formula: Enchant Neck - Mark of the Ancient Priestess
            141917, -- Formula: Enchant Neck - Mark of the Heavy Hide
            152658, -- Formula: Chaos Shatter
            144308, -- Formula: Enchant Neck - Mark of the Master
            144309, -- Formula: Enchant Neck - Mark of the Master
            144310, -- Formula: Enchant Neck - Mark of the Master
            144311, -- Formula: Enchant Neck - Mark of the Versatile
            144312, -- Formula: Enchant Neck - Mark of the Versatile
            144313, -- Formula: Enchant Neck - Mark of the Versatile
            144314, -- Formula: Enchant Neck - Mark of the Quick
            144315, -- Formula: Enchant Neck - Mark of the Quick
            144316, -- Formula: Enchant Neck - Mark of the Quick
            144317, -- Formula: Enchant Neck - Mark of the Deadly
            144318, -- Formula: Enchant Neck - Mark of the Deadly
            144319, -- Formula: Enchant Neck - Mark of the Deadly
            128621, -- Formula: Enchanted Cauldron
            128623, -- Formula: Enchanted Pen
            128625, -- Formula: Leylight Brazier
            138882, -- Formula: Tome of Illusions: Draenor
            128610, -- Formula: Enchant Neck - Mark of the Claw
            141918, -- Formula: Enchant Neck - Mark of the Trained Soldier
            141919  -- Formula: Enchant Neck - Mark of the Ancient Priestess
        },
        [addon.CONS.R_ENGINEERING_ID] = {
            133671, -- Schematic: Semi-Automagic Cranial Cannon
            136700, -- Schematic: "The Felic"
            136701, -- Schematic: Shockinator
            137691, -- Schematic: Intra-Dalaran Wormhole Generator
            137692, -- Schematic: Blingtron's Circuit Design Tutorial
            137693, -- Schematic: Reaves Module: Failure Detection Mode
            137694, -- Schematic: Reaves Module: Repair Mode
            137695, -- Schematic: Reaves Module: Wormhole Generator Mode
            137697, -- Schematic: Blink-Trigger Headgun
            137698, -- Schematic: Tactical Headgun
            137699, -- Schematic: Bolt-Action Headgun
            137700, -- Schematic: Reinforced Headgun
            137701, -- Schematic: Semi-Automagic Cranial Cannon
            137702, -- Schematic: Sawed-Off Cranial Cannon
            137703, -- Schematic: Double-Barreled Cranial Cannon
            137704, -- Schematic: Ironsight Cranial Cannon
            137705, -- Schematic: Deployable Bullet Dispenser
            137706, -- Schematic: Gunpowder Charge
            137707, -- Schematic: Pump-Action Bandage Gun
            137708, -- Schematic: Gunpack
            137709, -- Schematic: Auto-Hammer
            137710, -- Schematic: Failure Detection Pylon
            137711, -- Schematic: Blink-Trigger Headgun
            137712, -- Schematic: Tactical Headgun
            137713, -- Schematic: Bolt-Action Headgun
            137714, -- Schematic: Reinforced Headgun
            137715, -- Schematic: Semi-Automagic Cranial Cannon
            137716, -- Schematic: Sawed-Off Cranial Cannon
            137717, -- Schematic: Double-Barreled Cranial Cannon
            137718, -- Schematic: Ironsight Cranial Cannon
            137719, -- Schematic: Deployable Bullet Dispenser
            137720, -- Schematic: Gunpowder Charge
            137721, -- Schematic: Pump-Action Bandage Gun
            137722, -- Schematic: Gunpack
            137723, -- Schematic: Auto-Hammer
            137724, -- Schematic: Failure Detection Pylon
            137725, -- Schematic: Sonic Environment Enhancer
            137726, -- Schematic: Leystone Buoy
            137727, -- Schematic: Mecha-Bond Imprint Matrix
            141849, -- Schematic: Reaves Module: Bling Mode
            144335, -- Schematic: Tailored Skullblasters
            144336, -- Schematic: Rugged Skullblasters
            144337, -- Schematic: Chain Skullblasters
            144338, -- Schematic: Heavy Skullblasters
            144343, -- Schematic: Rechargeable Reaves Battery
            151714, -- Schematic: Gravitational Reduction Slippers
            151717  -- Schematic: Wormhole Generator: Argus
        },
        [addon.CONS.R_FIRST_AID_ID] = {
            142333  -- Recipe: Feathered Luffa
        },
        [addon.CONS.R_INSCRIPTIONS_ID] = {
            136705, -- Technique: Aqual Mark
            136706, -- Technique: Straszan Mark
            137728, -- Technique: Scroll of Forgotten Knowledge
            137730, -- Technique: Glyph of Ghostly Fade
            137731, -- Technique: Glyph of Fel Imp
            137732, -- Technique: Glyph of Sparkles
            137733, -- Technique: Glyph of Blackout
            137734, -- Technique: Glyph of the Sentinel
            137735, -- Technique: Glyph of Crackling Crane Lightning
            137736, -- Technique: Glyph of the Spectral Raptor
            137737, -- Technique: Glyph of Stellar Flare
            137738, -- Technique: Glyph of the Queen
            137740, -- Technique: Glyph of the Wraith Walker
            137741, -- Technique: Glyph of Fel Touched Souls
            137742, -- Technique: Glyph of Crackling Flames
            137743, -- Technique: Glyph of Fallow Wings
            137744, -- Technique: Glyph of Tattered Wings
            137745, -- Technique: Prophecy Tarot
            137746, -- Technique: Prophecy Tarot
            137747, -- Vantus Rune Technique: Ursoc
            137748, -- Vantus Rune Technique: Nythendra
            137749, -- Vantus Rune Technique: Il'gynoth, The Heart of Corruption
            137750, -- Vantus Rune Technique: Dragons of Nightmare
            137751, -- Vantus Rune Technique: Xavius
            137752, -- Vantus Rune Technique: Elerethe Renferal
            137753, -- Vantus Rune Technique: Cenarius
            137754, -- Vantus Rune Technique: Skorpyron
            137755, -- Vantus Rune Technique: Chronomatic Anomaly
            137756, -- Vantus Rune Technique: Trilliax
            137757, -- Vantus Rune Technique: Spellblade Aluriel
            137758, -- Vantus Rune Technique: Tichondrius
            137759, -- Vantus Rune Technique: High Botanist Tel'arn
            137760, -- Vantus Rune Technique: Krosus
            137761, -- Vantus Rune Technique: Star Augur Etraeus
            137762, -- Vantus Rune Technique: Grand Magistrix Elisande
            137763, -- Vantus Rune Technique: Gul'dan
            137767, -- Vantus Rune Technique: Ursoc
            137768, -- Vantus Rune Technique: Nythendra
            137769, -- Vantus Rune Technique: Il'gynoth, The Heart of Corruption
            137770, -- Vantus Rune Technique: Dragons of Nightmare
            137771, -- Vantus Rune Technique: Xavius
            137772, -- Vantus Rune Technique: Elerethe Renferal
            137773, -- Vantus Rune Technique: Cenarius
            137774, -- Vantus Rune Technique: Skorpyron
            137775, -- Vantus Rune Technique: Chronomatic Anomaly
            137776, -- Vantus Rune Technique: Trilliax
            137777, -- Vantus Rune Technique: Spellblade Aluriel
            137778, -- Vantus Rune Technique: Tichondrius
            137779, -- Vantus Rune Technique: High Botanist Tel'arn
            137780, -- Vantus Rune Technique: Krosus
            137781, -- Vantus Rune Technique: Star Augur Etraeus
            137782, -- Vantus Rune Technique: Grand Magistrix Elisande
            137783, -- Vantus Rune Technique: Gul'dan
            137787, -- Technique: Songs of Battle
            137788, -- Technique: Songs of Peace
            137789, -- Technique: Songs of the Legion
            137790, -- Technique: Darkmoon Card of the Legion (Rank 2)
            137791, -- Technique: Darkmoon Card of the Legion (Rank 3)
            139635, -- Vantus Rune Technique: Ursoc
            139636, -- Vantus Rune Technique: Nythendra
            139637, -- Vantus Rune Technique: Il'gynoth, The Heart of Corruption
            139638, -- Vantus Rune Technique: Dragons of Nightmare
            139639, -- Vantus Rune Technique: Xavius
            139640, -- Vantus Rune Technique: Elerethe Renferal
            139641, -- Vantus Rune Technique: Cenarius
            139642, -- Vantus Rune Technique: Skorpyron
            139643, -- Vantus Rune Technique: Chronomatic Anomaly
            139644, -- Vantus Rune Technique: Trilliax
            139645, -- Vantus Rune Technique: Spellblade Aluriel
            139646, -- Vantus Rune Technique: Tichondrius
            139647, -- Vantus Rune Technique: High Botanist Tel'arn
            139648, -- Vantus Rune Technique: Krosus
            139649, -- Vantus Rune Technique: Star Augur Etraeus
            139650, -- Vantus Rune Technique: Grand Magistrix Elisande
            139651, -- Vantus Rune Technique: Gul'dan
            140565, -- Technique: Songs of the Horde
            140566, -- Technique: Songs of the Alliance
            141030, -- Technique: Glyph of Cracked Ice
            141031, -- Technique: Glyph of the Blood Wraith
            141032, -- Technique: Glyph of the Chilled Shell
            141033, -- Technique: Glyph of the Crimson Shell
            141034, -- Technique: Glyph of the Unholy Wraith
            141035, -- Technique: Glyph of Fel Wings
            141036, -- Technique: Glyph of Fel-Enemies
            141037, -- Technique: Glyph of Mana Touched Souls
            141038, -- Technique: Glyph of Shadow-Enemies
            141039, -- Technique: Glyph of the Doe
            141040, -- Technique: Glyph of the Feral Chameleon
            141041, -- Technique: Glyph of the Forest Path
            141042, -- Technique: Glyph of Autumnal Bloom
            141043, -- Technique: Glyph of Arachnophobia
            141044, -- Technique: Glyph of Nesingwary's Nemeses
            141045, -- Technique: Glyph of the Bullseye
            141046, -- Technique: Glyph of the Dire Stable
            141047, -- Technique: Glyph of the Goblin Anti-Grav Flare
            141048, -- Technique: Glyph of the Headhunter
            141049, -- Technique: Glyph of the Hook
            141050, -- Technique: Glyph of the Skullseye
            141051, -- Technique: Glyph of the Trident
            141053, -- Technique: Glyph of Polymorphic Proportions
            141054, -- Technique: Glyph of Smolder
            141055, -- Technique: Glyph of Yu'lon's Grace
            141056, -- Technique: Glyph of Burnout
            141057, -- Technique: Glyph of Flash Bang
            141058, -- Technique: Glyph of Critterhex
            141059, -- Technique: Glyph of Flickering
            141060, -- Technique: Glyph of Pebbles
            141061, -- Technique: Glyph of the Abyssal
            141062, -- Technique: Glyph of the Inquisitor's Eye
            141063, -- Technique: Glyph of the Observer
            141064, -- Technique: Glyph of the Shivarra
            141065, -- Technique: Glyph of the Terrorguard
            141066, -- Technique: Glyph of the Voidlord
            141067, -- Technique: Glyph of Wrathguard
            141068, -- Technique: Glyph of the Blazing Savior
            141447, -- Technique: Tome of the Tranquil Mind
            141642, -- Technique: Tome of the Clear Mind
            141900, -- Technique: Glyph of Falling Thunder
            142104, -- Vantus Rune Technique: Odyn
            142105, -- Vantus Rune Technique: Guarm
            142106, -- Vantus Rune Technique: Helya
            142107, -- Vantus Rune Technique: Odyn
            142108, -- Vantus Rune Technique: Guarm
            142109, -- Vantus Rune Technique: Helya
            142110, -- Vantus Rune Technique: Odyn
            142111, -- Vantus Rune Technique: Guarm
            142112, -- Vantus Rune Technique: Helya
            143615, -- Technique: Glyph of Crackling Ox Lightning
            143616, -- Technique: Glyph of the Trusted Steed
            143751, -- Technique: Glyph of Twilight Bloom
            146335, -- Vantus Rune Technique: Black Rook Hold
            146336, -- Vantus Rune Technique: Darkheart Thicket
            146337, -- Vantus Rune Technique: Halls of Valor
            146338, -- Vantus Rune Technique: Maw of Souls
            146339, -- Vantus Rune Technique: Neltharion's Lair
            146340, -- Vantus Rune Technique: Return to Karazhan (Lower)
            146341, -- Vantus Rune Technique: The Arcway
            146342, -- Vantus Rune Technique: Vault of the Wardens
            146343, -- Vantus Rune Technique: Violet Hold
            146344, -- Vantus Rune Technique: Cathedral of Eternal Night
            146345, -- Vantus Rune Technique: Return to Karazhan (Upper)
            146355, -- Vantus Rune Technique: Black Rook Hold
            146356, -- Vantus Rune Technique: Darkheart Thicket
            146357, -- Vantus Rune Technique: Halls of Valor
            146358, -- Vantus Rune Technique: Maw of Souls
            146359, -- Vantus Rune Technique: Neltharion's Lair
            146360, -- Vantus Rune Technique: Return to Karazhan (Lower)
            146361, -- Vantus Rune Technique: The Arcway
            146362, -- Vantus Rune Technique: Vault of the Wardens
            146363, -- Vantus Rune Technique: Violet Hold
            146364, -- Vantus Rune Technique: Cathedral of Eternal Night
            146365, -- Vantus Rune Technique: Return to Karazhan (Upper)
            146375, -- Vantus Rune Technique: Black Rook Hold
            146376, -- Vantus Rune Technique: Darkheart Thicket
            146377, -- Vantus Rune Technique: Halls of Valor
            146378, -- Vantus Rune Technique: Maw of Souls
            146379, -- Vantus Rune Technique: Neltharion's Lair
            146380, -- Vantus Rune Technique: Return to Karazhan (Lower)
            146381, -- Vantus Rune Technique: The Arcway
            146382, -- Vantus Rune Technique: Vault of the Wardens
            146383, -- Vantus Rune Technique: Violet Hold
            146384, -- Vantus Rune Technique: Cathedral of Eternal Night
            146385, -- Vantus Rune Technique: Return to Karazhan (Upper)
            146395, -- Vantus Rune Technique: Court of Stars
            146396, -- Vantus Rune Technique: Eye of Azshara
            146397, -- Vantus Rune Technique: Court of Stars
            146398, -- Vantus Rune Technique: Eye of Azshara
            146399, -- Vantus Rune Technique: Court of Stars
            146400, -- Vantus Rune Technique: Eye of Azshara
            146411, -- Vantus Rune Technique: Tomb of Sargeras
            146412, -- Vantus Rune Technique: Tomb of Sargeras
            146413, -- Vantus Rune Technique: Tomb of Sargeras
            147118, -- Technique: Glyph of the Fel Succubus
            147120, -- Technique: Glyph of the Shadow Succubus
            151539, -- Technique: Glyph of Ember Shards
            151541, -- Technique: Glyph of Floating Shards
            151654, -- Vantus Rune Technique: Antorus, the Burning Throne
            151655, -- Vantus Rune Technique: Antorus, the Burning Throne
            151656, -- Vantus Rune Technique: Antorus, the Burning Throne
            152725, -- Technique: Mass Mill Astral Glory
            152726, -- Design: Mass Prospect Empyrium
            140037, -- Technique: Unwritten Legend
            153032, -- Technique: Glyph of the Lightspawn
            153034, -- Technique: Glyph of the Voidling
            137729, -- Technique: Codex of the Tranquil Mind
            141591, -- Technique: Codex of the Tranquil Mind
            141592, -- Technique: Codex of the Tranquil Mind
            141643, -- Technique: Codex of the Clear Mind
            151543, -- Technique: Glyph of Fel-Touched Shards
            153037  -- Technique: Glyph of Dark Absolution
        },
        [addon.CONS.R_JEWELCRAFTING_ID] = {
            133672, -- Schematic: Sawed-Off Cranial Cannon
            133673, -- Schematic: Double-Barreled Cranial Cannon
            133674, -- Schematic: Ironsight Cranial Cannon
            137792, -- Design: Deep Amber Loop
            137793, -- Design: Skystone Loop
            137794, -- Design: Azsunite Loop
            137795, -- Design: Deep Amber Pendant
            137796, -- Design: Skystone Pendant
            137797, -- Design: Azsunite Pendant
            137798, -- Design: Prophetic Band
            137799, -- Design: Maelstrom Band
            137800, -- Design: Dawnlight Band
            137801, -- Design: Sorcerous Shadowruby Pendant
            137802, -- Design: Blessed Dawnlight Medallion
            137803, -- Design: Twisted Pandemonite Choker
            137804, -- Design: Subtle Shadowruby Pendant
            137805, -- Design: Tranquil Necklace of Prophecy
            137806, -- Design: Vindictive Pandemonite Choker
            137807, -- Design: Sylvan Maelstrom Amulet
            137808, -- Design: Intrepid Necklace of Prophecy
            137809, -- Design: Ancient Maelstrom Amulet
            137810, -- Design: Righteous Dawnlight Medallion
            137811, -- Design: Raging Furystone Gorget
            137812, -- Design: Grim Furystone Gorget
            137813, -- Design: Saber's Eye
            137814, -- Design: Saber's Eye of Strength
            137815, -- Design: Saber's Eye of Agility
            137816, -- Design: Saber's Eye of Intellect
            137817, -- Design: Deep Amber Loop
            137818, -- Design: Skystone Loop
            137819, -- Design: Azsunite Loop
            137820, -- Design: Deep Amber Pendant
            137821, -- Design: Skystone Pendant
            137822, -- Design: Azsunite Pendant
            137823, -- Design: Prophetic Band
            137824, -- Design: Maelstrom Band
            137825, -- Design: Dawnlight Band
            137826, -- Design: Sorcerous Shadowruby Pendant
            137827, -- Design: Blessed Dawnlight Medallion
            137828, -- Design: Twisted Pandemonite Choker
            137829, -- Design: Subtle Shadowruby Pendant
            137830, -- Design: Tranquil Necklace of Prophecy
            137831, -- Design: Vindictive Pandemonite Choker
            137832, -- Design: Sylvan Maelstrom Amulet
            137833, -- Design: Intrepid Necklace of Prophecy
            137834, -- Design: Ancient Maelstrom Amulet
            137835, -- Design: Righteous Dawnlight Medallion
            137836, -- Design: Raging Furystone Gorget
            137837, -- Design: Grim Furystone Gorget
            137838, -- Design: Deep Amber Loop
            137839, -- Design: Skystone Loop
            137840, -- Design: Azsunite Loop
            137841, -- Design: Deep Amber Pendant
            137842, -- Design: Skystone Pendant
            137843, -- Design: Azsunite Pendant
            137844, -- Design: Prophetic Band
            137845, -- Design: Maelstrom Band
            137846, -- Design: Dawnlight Band
            137847, -- Design: Sorcerous Shadowruby Pendant
            137848, -- Design: Blessed Dawnlight Medallion
            137849, -- Design: Twisted Pandemonite Choker
            137850, -- Design: Subtle Shadowruby Pendant
            137851, -- Design: Tranquil Necklace of Prophecy
            137852, -- Design: Vindictive Pandemonite Choker
            137853, -- Design: Sylvan Maelstrom Amulet
            137854, -- Design: Intrepid Necklace of Prophecy
            137855, -- Design: Ancient Maelstrom Amulet
            137856, -- Design: Righteous Dawnlight Medallion
            137857, -- Design: Raging Furystone Gorget
            137858, -- Design: Grim Furystone Gorget
            137859, -- Design: Queen's Opal Loop
            137860, -- Design: Queen's Opal Pendant
            137861, -- Design: Shadowruby Band
            137862, -- Design: Queen's Opal Loop
            137863, -- Design: Queen's Opal Pendant
            137864, -- Design: Shadowruby Band
            137865, -- Design: Queen's Opal Loop
            137866, -- Design: Queen's Opal Pendant
            137867, -- Design: Shadowruby Band
            138451, -- Design: Deadly Deep Amber
            138452, -- Design: Quick Azsunite
            138453, -- Design: Masterful Queen's Opal
            138454, -- Design: Deadly Eye of Prophecy
            138455, -- Design: Quick Dawnlight
            138456, -- Design: Masterful Shadowruby
            141311, -- Design: Mass Prospect Leystone
            141312, -- Design: Mass Prospect Felslate
            151724, -- Design: Empyrial Cosmic Crown
            151725, -- Design: Empyrial Cosmic Crown
            151726, -- Design: Empyrial Cosmic Crown
            151727, -- Design: Empyrial Deep Crown
            151728, -- Design: Empyrial Deep Crown
            151729, -- Design: Empyrial Deep Crown
            151730, -- Design: Empyrial Elemental Crown
            151731, -- Design: Empyrial Elemental Crown
            151732, -- Design: Empyrial Elemental Crown
            151733, -- Design: Empyrial Titan Crown
            151734, -- Design: Empyrial Titan Crown
            151735, -- Design: Empyrial Titan Crown
            151736, -- Design: Deadly Deep Chemirine
            151737, -- Design: Quick Lightsphene
            151738, -- Design: Masterful Argulite
            151739, -- Design: Versatile Labradorite
            132467, -- Design: Skystone Pendant
            132468, -- Design: Deep Amber Pendant
            132469  -- Design: Azsunite Pendant
        },
        [addon.CONS.R_LEATHERWORKING_ID] = {
            137868, -- Recipe: Warhide Bindings
            137869, -- Recipe: Warhide Belt
            137870, -- Recipe: Warhide Shoulderguard
            137871, -- Recipe: Warhide Pants
            137872, -- Recipe: Warhide Mask
            137873, -- Recipe: Warhide Gloves
            137874, -- Recipe: Warhide Footpads
            137875, -- Recipe: Warhide Jerkin
            137876, -- Recipe: Warhide Bindings
            137877, -- Recipe: Warhide Pants
            137878, -- Recipe: Warhide Mask
            137879, -- Recipe: Warhide Gloves
            137880, -- Recipe: Warhide Footpads
            137881, -- Recipe: Warhide Belt
            137882, -- Recipe: Warhide Shoulderguard
            137883, -- Recipe: Warhide Jerkin
            137884, -- Recipe: Dreadleather Bindings
            137885, -- Recipe: Dreadleather Belt
            137886, -- Recipe: Dreadleather Shoulderguard
            137887, -- Recipe: Dreadleather Pants
            137888, -- Recipe: Dreadleather Mask
            137889, -- Recipe: Dreadleather Gloves
            137890, -- Recipe: Dreadleather Footpads
            137891, -- Recipe: Dreadleather Jerkin
            137892, -- Recipe: Dreadleather Bindings
            137893, -- Recipe: Dreadleather Belt
            137894, -- Recipe: Dreadleather Shoulderguard
            137895, -- Recipe: Dreadleather Pants
            137896, -- Recipe: Dreadleather Mask
            137897, -- Recipe: Dreadleather Gloves
            137898, -- Recipe: Dreadleather Footpads
            137899, -- Recipe: Dreadleather Jerkin
            137900, -- Recipe: Battlebound Armbands
            137901, -- Recipe: Battlebound Girdle
            137902, -- Recipe: Battlebound Spaulders
            137903, -- Recipe: Battlebound Leggings
            137904, -- Recipe: Battlebound Warhelm
            137905, -- Recipe: Battlebound Grips
            137906, -- Recipe: Battlebound Treads
            137907, -- Recipe: Battlebound Hauberk
            137908, -- Recipe: Battlebound Armbands
            137909, -- Recipe: Battlebound Leggings
            137910, -- Recipe: Battlebound Warhelm
            137911, -- Recipe: Battlebound Grips
            137912, -- Recipe: Battlebound Treads
            137913, -- Recipe: Battlebound Girdle
            137914, -- Recipe: Battlebound Spaulders
            137915, -- Recipe: Battlebound Hauberk
            137916, -- Recipe: Gravenscale Armbands
            137917, -- Recipe: Gravenscale Girdle
            137918, -- Recipe: Gravenscale Spaulders
            137919, -- Recipe: Gravenscale Leggings
            137920, -- Recipe: Gravenscale Warhelm
            137921, -- Recipe: Gravenscale Grips
            137922, -- Recipe: Gravenscale Treads
            137923, -- Recipe: Gravenscale Hauberk
            137924, -- Recipe: Gravenscale Armbands
            137925, -- Recipe: Gravenscale Girdle
            137926, -- Recipe: Gravenscale Spaulders
            137927, -- Recipe: Gravenscale Leggings
            137928, -- Recipe: Gravenscale Warhelm
            137929, -- Recipe: Gravenscale Grips
            137930, -- Recipe: Gravenscale Treads
            137931, -- Recipe: Gravenscale Hauberk
            137932, -- Recipe: Flaming Hoop
            137933, -- Recipe: Leather Pet Bed
            137934, -- Recipe: Leather Pet Leash
            137935, -- Recipe: Leather Love Seat
            137952, -- Recipe: Stonehide Leather Barding
            140636, -- Recipe: Dreadleather Bindings
            140637, -- Recipe: Dreadleather Belt
            140638, -- Recipe: Dreadleather Shoulderguard
            140639, -- Recipe: Dreadleather Pants
            140640, -- Recipe: Dreadleather Mask
            140641, -- Recipe: Dreadleather Gloves
            140642, -- Recipe: Dreadleather Footpads
            140643, -- Recipe: Dreadleather Jerkin
            140644, -- Recipe: Gravenscale Armbands
            140645, -- Recipe: Gravenscale Girdle
            140646, -- Recipe: Gravenscale Spaulders
            140647, -- Recipe: Gravenscale Leggings
            140648, -- Recipe: Gravenscale Warhelm
            140649, -- Recipe: Gravenscale Grips
            140650, -- Recipe: Gravenscale Treads
            140651, -- Recipe: Gravenscale Hauberk
            141850, -- Recipe: Elderhorn Riding Harness
            151740, -- Recipe: Fiendish Shoulderguards
            151741, -- Recipe: Fiendish Shoulderguards
            151742, -- Recipe: Fiendish Shoulderguards
            151743, -- Recipe: Fiendish Spaulders
            151744, -- Recipe: Fiendish Spaulders
            151745, -- Recipe: Fiendish Spaulders
            139893, -- Skinning Technique: Unbroken Tooth
            139894, -- Skinning Technique: Unbroken Claw
            139895, -- Skinning Technique: Legion Butchery
            139896, -- Skinning Technique: Legion Gutting
            142407, -- Recipe: Drums of the Mountain
            142408, -- Recipe: Drums of the Mountain
            142409, -- Recipe: Drums of the Mountain
            132123, -- Pattern: Battlebound Warhelm
            132124  -- Pattern: Battlebound Treads
        },
        [addon.CONS.R_TAILORING_ID] = {
            127022, -- Pattern: Imbued Silkweave Cinch
            127023, -- Pattern: Imbued Silkweave Epaulets
            127024, -- Pattern: Imbued Silkweave Pantaloons
            127025, -- Pattern: Imbued Silkweave Hood
            127026, -- Pattern: Imbued Silkweave Gloves
            127027, -- Pattern: Imbued Silkweave Slippers
            127028, -- Pattern: Imbued Silkweave Robe
            127277, -- Pattern: Imbued Silkweave Cover
            127278, -- Pattern: Imbued Silkweave Drape
            127279, -- Pattern: Imbued Silkweave Shade
            127280, -- Pattern: Imbued Silkweave Flourish
            137681, -- Recipe: Bloodtotem Saddle Blanket
            137953, -- Pattern: Silkweave Bracers
            137954, -- Pattern: Silkweave Cinch
            137955, -- Pattern: Silkweave Epaulets
            137956, -- Pattern: Silkweave Pantaloons
            137957, -- Pattern: Silkweave Hood
            137958, -- Pattern: Silkweave Gloves
            137959, -- Pattern: Silkweave Slippers
            137960, -- Pattern: Silkweave Robe
            137961, -- Pattern: Silkweave Bracers
            137962, -- Pattern: Silkweave Cinch
            137963, -- Pattern: Silkweave Epaulets
            137964, -- Pattern: Silkweave Robe
            137965, -- Pattern: Imbued Silkweave Bracers
            137966, -- Pattern: Imbued Silkweave Cinch
            137967, -- Pattern: Imbued Silkweave Epaulets
            137968, -- Pattern: Imbued Silkweave Pantaloons
            137969, -- Pattern: Imbued Silkweave Hood
            137970, -- Pattern: Imbued Silkweave Gloves
            137971, -- Pattern: Imbued Silkweave Slippers
            137972, -- Pattern: Imbued Silkweave Robe
            137973, -- Pattern: Imbued Silkweave Bracers
            137974, -- Pattern: Imbued Silkweave Cinch
            137975, -- Pattern: Imbued Silkweave Epaulets
            137976, -- Pattern: Imbued Silkweave Pantaloons
            137977, -- Pattern: Imbued Silkweave Hood
            137978, -- Pattern: Imbued Silkweave Gloves
            137979, -- Pattern: Imbued Silkweave Slippers
            137980, -- Pattern: Imbued Silkweave Robe
            137981, -- Pattern: Silkweave Cloak
            137984, -- Pattern: Silkweave Shade
            137987, -- Pattern: Silkweave Flourish
            137990, -- Pattern: Silkweave Cover
            137993, -- Pattern: Silkweave Drape
            138000, -- Pattern: Imbued Silkweave Shade
            138003, -- Pattern: Imbued Silkweave Flourish
            138006, -- Pattern: Imbued Silkweave Cover
            138009, -- Pattern: Imbued Silkweave Drape
            138011, -- Pattern: Silkweave Satchel
            138012, -- Pattern: Silkweave Gloves
            138013, -- Pattern: Silkweave Hood
            138014, -- Pattern: Silkweave Slippers
            138015, -- Pattern: Silkweave Pantaloons
            138016, -- Clothes Chest Pattern: Dalaran Citizens
            138017, -- Clothes Chest Pattern: Karazhan Opera House
            138018, -- Clothes Chest Pattern: Molten Core
            151746, -- Recipe: Lightweave Breeches
            151747, -- Recipe: Lightweave Breeches
            151748, -- Recipe: Lightweave Breeches
            142076, -- Pattern: Imbued Silkweave Bag
            142077, -- Pattern: Imbued Silkweave Bag
            142078, -- Pattern: Imbued Silkweave Bag
            138001, -- Pattern: Imbued Silkweave Shade
            138004, -- Pattern: Imbued Silkweave Flourish
            138007, -- Pattern: Imbued Silkweave Cover
            138010  -- Pattern: Imbued Silkweave Drape
        }
    },
    [addon.CONS.TRADE_GOODS_ID] = {
        129032, -- Roseate Pigment
        129034, -- Sallow Pigment
        [addon.CONS.T_CLOTH_ID] = {
            127004, -- Imbued Silkweave
            124437, -- Shal'dorei Silk
            127037, -- Runic Catgut
            127681, -- Sharp Spritethorn
            151567, -- Lightweave Cloth
            146710, -- Bolt of Shadowcloth
            146711  -- Bolt of Starweave
        },
        [addon.CONS.T_ELEMENTAL_ID] = {
            124124, -- Blood of Sargeras
            151568, -- Primal Sargerite
            124122, -- Leyfire
            124123, -- Demonfire
            141323  -- Wild Transmutation
        },
        [addon.CONS.T_ENCHANTING_ID] = {
            124442, -- Chaos Crystal
            124441, -- Leylight Shard
            124440  -- Arkhana
        },
        [addon.CONS.T_PARTS_ID] = {
            140781, -- X-87, -- Battle Circuit
            144329, -- Hardened Felglass
            136636, -- Sniping Scope
            136637, -- Oversized Blasting Cap
            136638, -- True Iron Barrel
            136633, -- Loose Trigger
            140782, -- Neural Net Detangler
            140783, -- Predictive Combat Operations Databank
            140784, -- Fel Piston Stabilizer
            140785, -- Hardened Circuitboard Plating
            147619  -- [addon.CONS.QA] Big Stack Test
        },
        [addon.CONS.T_FISHING_ID] = {
            138967, -- Big Fountain Goldfish
            133725, -- Leyshimmer Blenny
            133727, -- Ghostly Queenfish
            133728, -- Terrorfin
            133729, -- Thorned Flounder
            133730, -- Ancient Mossgill
            133731, -- Mountain Puffer
            133732, -- Coldriver Carp
            133733, -- Ancient Highmountain Salmon
            133734, -- Oodelfjisk
            133735, -- Graybelly Lobster
            133736, -- Thundering Stormray
            133737, -- Magic-Eater Frog
            133739, -- Tainted Runescale Koi
            133740, -- Axefish
            133742, -- Ancient Black Barracuda
            133726, -- Nar'thalas Hermit
            139652, -- Leyshimmer Blenny
            139653, -- Nar'thalas Hermit
            139654, -- Ghostly Queenfish
            139655, -- Terrorfin
            139656, -- Thorned Flounder
            139657, -- Ancient Mossgill
            139658, -- Mountain Puffer
            139659, -- Coldriver Carp
            139660, -- Ancient Highmountain Salmon
            139661, -- Oodelfjisk
            139662, -- Graybelly Lobster
            139663, -- Thundering Stormray
            139664, -- Magic-Eater Frog
            139666, -- Tainted Runescale Koi
            139667, -- Axefish
            139669  -- Ancient Black Barracuda
        },
        [addon.CONS.T_HERBS_ID] = {
            128304, -- Yseralline Seed
            124106, -- Felwort
            129289, -- Felwort Seed
            124101, -- Aethril
            129284, -- Aethril Seed
            124102, -- Dreamleaf
            129285, -- Dreamleaf Seed
            124103, -- Foxflower
            129286, -- Foxflower Seed
            124104, -- Fjarnskaggl
            129287, -- Fjarnskaggl Seed
            124105, -- Starlight Rose
            129288, -- Starlight Rose Seed
            151565  -- Astral Glory
        },
        [addon.CONS.T_JEWELCRAFTING_ID] = {
            144344, -- Orange Gem
            151579, -- Labradorite
            151719, -- Lightsphene
            151720, -- Chemirine
            151721, -- Hesselian
            151722, -- Florid Malachite
            130178, -- Furystone
            130179, -- Eye of Prophecy
            130180, -- Dawnlight
            130181, -- Pandemonite
            130182, -- Maelstrom Sapphire
            130183, -- Shadowruby
            130245, -- Saber's Eye
            151718, -- Argulite
            144347, -- Red Gem
            144348, -- Yellow Gem
            144349, -- Green Gem
            144350, -- Blue Gem
            144351, -- Purple Gem
            130172, -- Sangrite
            130173, -- Deep Amber
            130174, -- Azsunite
            130175, -- Chaotic Spinel
            130176, -- Skystone
            130177, -- Queen's Opal
            129100  -- Gem Chip
        },
        [addon.CONS.T_LEATHER_ID] = {
            124116, -- Felhide
            136533, -- Dreadhide Leather
            136534, -- Gravenscale
            151566, -- Fiendish Leather
            124113, -- Stonehide Leather
            124115, -- Stormscale
            146712, -- Wisp-Touched Elderhide
            146713  -- Prime Wardenscale
        },
        [addon.CONS.T_MEAT_ID] = {
            133588, -- Flaked Sea Salt
            133589, -- Dalapeño Pepper
            133590, -- Muskenbutter
            133591, -- River Onion
            133592, -- Stonedark Snail
            133593,  -- Royal Olive
            [addon.CONS.TM_ANIMAL_ID] = {
                133680, -- Slice of Bacon
                124118, -- Fatty Bearsteak
                124119, -- Big Gamy Ribs
                124120, -- Leyblood
                124117  -- Lean Shank
            },
            [addon.CONS.TM_EGG_ID] = {
                142336, -- Falcosaur Egg
                124121  -- Wildfowl Egg
            },
            [addon.CONS.TM_FISH_ID] = {
                124107, -- Cursed Queenfish
                124108, -- Mossgill Perch
                124109, -- Highmountain Salmon
                124110, -- Stormray
                124111, -- Runescale Koi
                124112, -- Black Barracuda
                133607  -- Silver Mackerel
            }
        },
        [addon.CONS.T_METAL_STONE_ID] = {
            124444, -- Infernal Brimstone
            151564, -- Empyrium
            123919, -- Felslate
            124461, -- Demonsteel Bar
            123918  -- Leystone Ore
        },
        [addon.CONS.T_OTHER_ID] = {
            146655, -- Empty Felessence Vessel
            124436, -- Foxflower Flux
            136654, -- Field Pack
            124438, -- Unbroken Claw
            124439, -- Unbroken Tooth
            142335, -- Pristine Falcosaur Feather
            137590, -- Pile of Silk
            137591, -- Pile of Skins
            137592, -- Pile of Ore
            137593, -- Pile of Herbs
            137594, -- Pile of Gems
            137600, -- Pile of Pants
            146656, -- Felessence Vessel
            141975, -- Mark of Aquaos
            146964, -- Hatecoil Spearhead
            146965, -- Disgusting Ooze
            146966, -- Water Totem Figurine
            146967, -- White Sparkly Bauble
            146968, -- Glowing Fish Scale
            146969, -- Faintly Pulsing Felstone
            137595, -- Viscous Transmutagen
            137596, -- Black Transmutagen
            137597, -- Oily Transmutagen
            137599, -- Pulsating Sac
            137608, -- Growling Sac
            128712, -- Darkmoon Card of the Legion
            146657, -- Felessence Phial
            146658, -- Felessence Phylactery
            146659  -- Nethershard Essence
        },
        [addon.CONS.ORDER_HALL_ID] = {
            [addon.CONS.O_CHAMPION_ARMOR_ID] = {
                147348, -- Bulky Armor Set
                147349, -- Spiked Armor Set
                147350, -- Invincible Armor Set
                153005, -- Relinquished Armor Set
                151842, -- Krokul Armor Set
                151843, -- Mac'Aree Armor Set
                151844, -- Xenedar Armor Set
                136412, -- Heavy Armor Set
                137207, -- Fortified Armor Set
                137208  -- Indestructible Armor Set
            },
            [addon.CONS.O_CHAMPION_EQUIPMENT_ID] = {
                139816, -- Well-Worn Stone
                139801, -- Doodad
                139802, -- Auspicious Fetish
                152438, -- Krokul Sledgehammer
                152935, -- Wakener's Bauble
                152445, -- Memento of the Lightforged
                140572, -- Hasty Pocketwatch
                140571, -- Potion of Energy
                140573, -- Elixir of Overwhelming Focus
                152927, -- Serrated Stone Axe
                152440, -- Void-Touched Arinor Blossom
                152443, -- Sanctified Armaments of the Light
                140581, -- Sturdy Hiking Boots
                152437, -- Viscid Demon Blood
                140582, -- Bottomless Flask
                152936, -- Azurelight Sapphire
                140583, -- Vial of Timeless Breath
                152444, -- Exalted Xenedar Hammer
                152439, -- Pit Lord Tusk
                152933, -- Shadowguard Void Effusion
                152931, -- Xenic Tincture
                152930, -- Vilefiend-Spine Whip
                152934, -- Shadow-Soaked Stalker Heart
                152447, -- Lightburst Charge
                152929, -- Pronged Ridgestalker Spear
                152442, -- Impervious Shadoweave Hood
                152932, -- Runewarded Lightblade
                139813, -- Swift Boots
                139814, -- Carrot on a Stick
                139799, -- Pathfinder's Saddle
                152928, -- Archaic Seerstone
                152441, -- Satchel of Lucidity
                152446, -- Writ of Holy Orders
                139792, -- Fruitful Bauble
                139808, -- Curio of Abundant Happiness
                139809, -- Elixir of Plenty
                139795, -- Draught of Courage
                139811, -- Necklace of Endless Memories
                139812, -- Potion of Triton
                139875, -- Undead Token
                139876, -- Rune of Reckoning
                139877, -- Death's Touch
                139878, -- Relic of the Ebon Blade
                147572, -- Sigil of Ebon Frost
                139835, -- Marauder's Vestige
                139836, -- Shadow Relic
                139837, -- Demon's Sigil
                139838, -- Essence of Malice
                147571, -- Demonic Standard
                139863, -- Elune's Sight
                139864, -- Scroll of Growth
                139865, -- Glowing Token
                139866, -- Forest Ember
                147570, -- Dreamgrove Leaf
                139847, -- Bow of Ancient Kings
                139848, -- Seeker's Scrip
                139849, -- Windrunner's Gift
                139850, -- Cloak of Deception
                147569, -- Arcane Trap
                139845, -- Band of Primordial Strength
                139846, -- Highborne Bauble
                139843, -- Conjurer's Bauble
                139844, -- Arcanist's Trifle
                147568, -- Essence of Nether
                139859, -- Chi Empowered Jewel
                139860, -- Tea of Blessing
                139861, -- Mogu Madstone
                139862, -- Bell of Fury
                147567, -- Soothing Focus
                139867, -- Justice Hammer
                139868, -- Light's Shield
                139869, -- Libram of Enlightenment
                139870, -- Silver Hand Ornament
                147566, -- Horn of Valor
                139871, -- Holy Figurine
                139872, -- Light's Command
                139873, -- Bottled Sanity
                139874, -- Sanity Edge
                147565, -- Book of Lost Sermons
                139831, -- Smoke Grenades
                139832, -- Sleep Potion
                139833, -- Leech Brew
                139834, -- Vanishing Dust
                147564, -- Diamond Stone
                139839, -- Voodoo Post
                139840, -- Earthly Pincer
                139841, -- Totem of the Earth
                139842, -- Furious Charge
                147563, -- Stone Totem
                139855, -- Skull of Embrace
                139856, -- Demonic Brew
                139857, -- Helm of Command
                139858, -- Black Harvest Curio
                147561, -- Relic of Demonic Influence
                139851, -- Valarjar's Might
                139852, -- War Banner
                139853, -- Axe of the Valkyra
                139854, -- Skull of a Fallen Foe
                147560, -- Horn of Rage
                139830, -- Demon in a Box
                147557, -- Fel Imp Tooth
                152448, -- Praetorium Tome of Arcana
                139828, -- Queen's Feathers
                139829, -- Potion of Sacrifice
                147554, -- Harpy Feather
                152454, -- Darklost Claw
                147555, -- Vial of Sight
                147556, -- Cloak of Concealment
                152453, -- Fel-Infused Legion Effigy
                152449, -- Azurelight Candelabra
                152450, -- Augari Censorite Staff
                152451, -- Volatile Stygian Scroll
                152452, -- Pulsing Wrathguard Skull
                139827, -- Brooch of Endless Dreams
                139825, -- Tome of Secrets
                139826, -- Eltrig's Grace
                139821, -- Omen's Bidding
                139804, -- Glacial Fang
                139819, -- Embers of the Firelands
                147553, -- Shard of Twisting Nether
                139824, -- Light's Haven
                139823, -- Nightmare's End
                139822, -- Dust of Azeroth
                147559, -- Ward of Infinite Fury
                147558  -- Pouch of Wonder
            },
            [addon.CONS.O_CHESTS_ID] = {
                147432, -- Champion Equipment
                153132, -- Coffer of Argus Equipment
                139879, -- Crate of Champion Equipment
                147905, -- Chest of Champion Equipment
                150581, -- Chest of Useful Equipment
                134086  -- Surveying Equipment
            },
            [addon.CONS.O_CONSUMABLES_ID] = {
                140749, -- Horn of Winter
                143852, -- Lucky Rabbit's Foot
                139419, -- Golden Banana
                140760, -- Libram of Truth
                140156, -- Blessing of the Order
                139428, -- A Master Plan
                143605, -- Strange Ball of Energy
                139177, -- Shattered Soul
                139420, -- Wild Mushroom
                138883, -- Meryl's Conjured Refreshment
                139376, -- Healing Well
                139418, -- Healing Stream Totem
                138412, -- Iresoul's Healthstone
                140922, -- Imp Pact
                139670, -- Scream of the Dead
                143849, -- Summon Royal Guard
                143850, -- Summon Grimtotem Warrior
                142209, -- Dinner Invitation
                -- Order Hall Gives
                141961, -- Soul Flame of Rejuvenation
                141962, -- Soul Flame of Castigation
                141958, -- Soul Flame of Fortification
                141960, -- Soul Flame of Insight
                141959, -- Soul Flame of Alacrity
                140216, -- Eagle Feather
                140295, -- Badgercharm Brew
                140293, -- Exploding Cask
                140292, -- Tumblerun Brew
                140291, -- Featherfoot Brew
                140290, -- Seastrider Brew
                140289, -- Lungfiller Brew
                140288, -- Bubblebelly Brew
                140256, -- Skysinger Brew
                140253, -- Swiftpad Brew
                140287, -- Stoutheart Brew
                139447, -- Vial of Pure Light
                139449, -- Essence of the Naaru
                139452, -- Essence of the Light
                139459, -- Blessing of the Light
                139448, -- Vial of Dark Shadows
                139450, -- Void Sphere
                139451, -- Swirling Void Potion
                139461, -- Rune of Darkness
                141964, -- Flaming Demonheart
                141965, -- Shadowy Demonheart
                141966, -- Coercive Demonheart
                141967, -- Whispering Demonheart
                141968  -- Immense Demonheart
            },
            [addon.CONS.O_TROOPS_ID] = {
                152095, -- Krokul Ridgestalker
                152096, -- Void-Purged Krokul
                152097, -- Lightforged Bulwark
                153006, -- Grimoire of Lost Knowledge
                141028  -- Grimoire of Knowledge
            }
        }
    }
}