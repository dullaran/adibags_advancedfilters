local _, addon = ...


addon.ITEM_DATABASE[addon.CONS.CLASSIC_ID] = {
    [addon.CONS.CONTAINERS_ID] = {
        14046, -- Runecloth Bag
        1685, -- Troll-Hide Bag
        3914, -- Journeyman's Backpack
        19291, -- Darkmoon Storage Box
        1725, -- Large Knapsack
        4499, -- Huge Brown Sack
        10050, -- Mageweave Bag
        10051, -- Red Mageweave Bag
        804, -- Large Blue Sack
        857, -- Large Red Sack
        932, -- Fel Steed Saddlebags
        933, -- Large Rucksack
        1470, -- Murloc Skin Bag
        4245, -- Small Silk Pack
        4497, -- Heavy Brown Bag
        5575, -- Large Green Sack
        5576, -- Large Brown Sack
        5764, -- Green Silk Pack
        5765, -- Black Silk Pack
        856, -- Blue Leather Bag
        2657, -- Red Leather Bag
        3233, -- Gnoll Hide Sack
        3343, -- Captain Sanders' Booty Bag
        4240, -- Woolen Bag
        4241, -- Green Woolen Bag
        4498, -- Brown Leather Satchel
        5573, -- Green Leather Bag
        5574, -- White Leather Bag
        5763, -- Red Woolen Bag
        805, -- Small Red Pouch
        828, -- Small Blue Pouch
        4238, -- Linen Bag
        4496, -- Small Brown Pouch
        4957, -- Old Moneybag
        5081, -- Kodo Hide Bag
        5571, -- Small Black Pouch
        5572, -- Small Green Pouch
        5762, -- Red Linen Bag
        20474, -- Sunstrider Book Satchel
        14155, -- Mooncloth Bag
        4500, -- Traveler's Backpack
        21340, -- Soul Pouch
        20400, -- Pumpkin Bag
        14156, -- Bottomless Bag
        19319, -- Harpy Hide Quiver
        19320, -- Gnoll Skin Bandolier
        21341, -- Felcloth Bag
        11742, -- Wayfarer's Knapsack
        6446, -- Snakeskin Bag
        21342, -- Core Felcloth Bag
        22249, -- Big Bag of Enchantment
        22248, -- Enchanted Runecloth Bag
        22246, -- Enchanted Mageweave Pouch
        22252, -- Satchel of Cenarius
        22251, -- Cenarion Herb Bag
        22250, -- Herb Pouch
    },
    [addon.CONS.CONSUMABLES_ID] = {
        [addon.CONS.C_BANDAGES_ID] = {
            14530, -- Heavy Runecloth Bandage
            19307, -- Alterac Heavy Runecloth Bandage
            19066, -- Warsong Gulch Runecloth Bandage
            20066, -- Arathi Basin Runecloth Bandage
            20234, -- Defiler's Runecloth Bandage
            20243, -- Highlander's Runecloth Bandage
            14529, -- Runecloth Bandage
            19067, -- Warsong Gulch Mageweave Bandage
            20065, -- Arathi Basin Mageweave Bandage
            20232, -- Defiler's Mageweave Bandage
            20237, -- Highlander's Mageweave Bandage
            19068, -- Warsong Gulch Silk Bandage
            20067, -- Arathi Basin Silk Bandage
            20235, -- Defiler's Silk Bandage
            20244, -- Highlander's Silk Bandage
            1251, -- Linen Bandage
            2581, -- Heavy Linen Bandage
            3530, -- Wool Bandage
            3531, -- Heavy Wool Bandage
            6450, -- Silk Bandage
            6451, -- Heavy Silk Bandage
            8544, -- Mageweave Bandage
            8545  -- Heavy Mageweave Bandage
        },
        [addon.CONS.C_CONSUMABLES_ID] = {
            16040, -- Arcane Bomb
            18232, -- Field Repair Bot 74A
            16005, -- Dark Iron Bomb
            16023, -- Masterwork Target Dummy
            18594, -- Powerful Seaforium Charge
            18587, -- Goblin Jumper Cables XL
            15993, -- Thorium Grenade
            10562, -- Hi-Explosive Bomb
            10586, -- The Big One
            18641, -- Dense Dynamite
            4394, -- Big Iron Bomb
            10514, -- Mithril Frag Bomb
            9036, -- Magic Resistance Potion
            10646, -- Goblin Sapper Charge
            4398, -- Large Seaforium Charge
            11590, -- Mechanical Repair Kit
            18588, -- Ez-Thro Dynamite II
            4395, -- Goblin Land Mine
            4392, -- Advanced Target Dummy
            4852, -- Flash Bomb
            4390, -- Iron Grenade
            4391, -- Compact Harvest Reaper Kit
            10507, -- Solid Dynamite
            4380, -- Big Bronze Bomb
            4403, -- Portable Bronze Mortar
            7148, -- Goblin Jumper Cables
            4388, -- Discombobulator Ray
            4386, -- Ice Deflector
            4378, -- Heavy Dynamite
            4384, -- Explosive Sheep
            7809, -- Easter Dress
            4374, -- Small Bronze Bomb
            5507, -- Ornate Spyglass
            4370, -- Large Copper Bomb
            4376, -- Flame Deflector
            6714, -- Ez-Thro Dynamite
            3384, -- Minor Magic Resistance Potion
            3393, -- Recipe: Minor Magic Resistance Potion
            4365, -- Coarse Dynamite
            4367, -- Small Seaforium Charge
            4366, -- Target Dummy
            4360, -- Rough Copper Bomb
            4358, -- Rough Dynamite
            9299, -- Thermaplugg's Safe Combination
            10757, -- Ward of the Defiler
            11079, -- Gor'tesh's Lopped Off Head
            18645, -- Gnomish Alarm-o-Bot
            11224, -- Formula: Enchant Shield - Frost Resistance
            9293, -- Recipe: Magic Resistance Potion
            11098, -- Formula: Enchant Cloak - Lesser Shadow Resistance
            18984, -- Dimensional Ripper - Everlook
            18986, -- Ultrasafe Transporter: Gadgetzan
            17716, -- Snowmaster 9000
            18660  -- World Enlarger
        },
        [addon.CONS.C_ELIXIRS_ID] = {
            20004, -- Mighty Troll's Blood Elixir
            13454, -- Greater Arcane Elixir
            13452, -- Elixir of the Mongoose
            13453, -- Elixir of Brute Force
            13447, -- Elixir of the Sages
            13445, -- Elixir of Superior Defense
            8827, -- Elixir of Water Walking
            9224, -- Elixir of Demonslaying
            9233, -- Elixir of Detect Demon
            9264, -- Elixir of Shadow Power
            12820, -- Winterfall Firewater
            20007, -- Mageblood Elixir
            21546, -- Elixir of Greater Firepower
            9088, -- Gift of Arthas
            9187, -- Elixir of Greater Agility
            9197, -- Elixir of Dream Vision
            9206, -- Elixir of Giants
            9155, -- Arcane Elixir
            9179, -- Elixir of Greater Intellect
            9154, -- Elixir of Detect Undead
            8529, -- Noggenfogger Elixir
            18294, -- Elixir of Greater Water Breathing
            10592, -- Catseye Elixir
            3828, -- Elixir of Detect Lesser Invisibility
            8951, -- Elixir of Greater Defense
            17708, -- Elixir of Frost Power
            8949, -- Elixir of Agility
            3826, -- Major Troll's Blood Elixir
            3825, -- Elixir of Fortitude
            3391, -- Elixir of Ogre's Strength
            3390, -- Elixir of Lesser Agility
            6373, -- Elixir of Firepower
            3389, -- Elixir of Defense
            3388, -- Strong Troll's Blood Elixir
            3383, -- Elixir of Wisdom
            5996, -- Elixir of Water Breathing
            6662, -- Elixir of Giant Growth
            2457, -- Elixir of Minor Agility
            2458, -- Elixir of Minor Fortitude
            3382, -- Weak Troll's Blood Elixir
            2454, -- Elixir of Lion's Strength
            5997, -- Elixir of Minor Defense
            128404, -- Helbrim's Special
            128437, -- Potion of Bubbling Pustules
            128708, -- Molted Feather
            128805, -- Potion of Fel Protection
            128810, -- Jar of Spiders
            128912, -- Blight Sample
            132176, -- Thunder Special
            132178, -- Battleguard's Sharpening Stone
        },
        [addon.CONS.C_FLASKS_ID] = {
            13510, -- Flask of the Titans
            13511, -- Flask of Distilled Wisdom
            13512  -- Flask of Supreme Power
        },
        [addon.CONS.C_FOOD_DRINKS_ID] = {
            8079, -- Conjured Crystal Water
            18300, -- Hyjal Nectar
            19696, -- Harvest Bread
            19994, -- Harvest Fruit
            19995, -- Harvest Boar
            19996, -- Harvest Fish
            20516, -- Bobbing Apple
            21023, -- Dirge's Kickin' Chimaerok Chops
            21235, -- Winter Veil Roast
            21254, -- Winter Veil Cookie
            22895, -- Conjured Cinnamon Roll
            19301, -- Alterac Manna Biscuit
            8076, -- Conjured Sweet Roll
            8078, -- Conjured Sparkling Water
            8766, -- Morning Glory Dew
            8932, -- Alterac Swiss
            8948, -- Dried King Bolete
            8950, -- Homemade Cherry Pie
            8952, -- Roasted Quail
            8953, -- Deep Fried Plantains
            8957, -- Spinefin Halibut
            11415, -- Mixed Berries
            11444, -- Grim Guzzler Boar
            13724, -- Enriched Manna Biscuit
            13810, -- Blessed Sunfruit
            13893, -- Large Raw Mightfish
            13933, -- Lobster Stew
            13934, -- Mightfish Steak
            13935, -- Baked Salmon
            18254, -- Runn Tum Tuber Surprise
            18255, -- Runn Tum Tuber
            19225, -- Deep Fried Candybar
            20452, -- Smoked Desert Dumplings
            21031, -- Cabbage Kimchi
            21033, -- Radish Kimchi
            22324, -- Winter Kimchi
            23160, -- Friendship Bread
            12216, -- Spiced Chili Crab
            12218, -- Monster Omelet
            18045, -- Tender Wolf Steak
            21215, -- Graccu's Mince Meat Fruitcake
            1645, -- Moonberry Juice
            2595, -- Jug of Badlands Bourbon
            3927, -- Fine Aged Cheddar
            4599, -- Cured Ham Steak
            4601, -- Soft Banana Bread
            4602, -- Moon Harvest Pumpkin
            4608, -- Raw Black Truffle
            6887, -- Spotted Yellowtail
            8075, -- Conjured Sourdough
            8077, -- Conjured Mineral Water
            12215, -- Heavy Kodo Stew
            12217, -- Dragonbreath Chili
            13546, -- Bloodbelly Fish
            13755, -- Winter Squid
            13927, -- Cooked Glossy Mightfish
            13928, -- Grilled Squid
            13929, -- Hot Smoked Bass
            13930, -- Filet of Redgill
            13931, -- Nightfin Soup
            13932, -- Poached Sunscale Salmon
            16168, -- Heaven Peach
            16766, -- Undermine Clam Chowder
            17222, -- Spider Sausage
            17402, -- Greatfather's Winter Ale
            17408, -- Spicy Beefstick
            18635, -- Bellara's Nutterbar
            19300, -- Bottled Winterspring Water
            19306, -- Crunchy Frog
            21030, -- Darnassus Kimchi Pie
            21552, -- Striped Yellowtail
            21217, -- Sagefish Delight
            1487, -- Conjured Pumpernickel
            1707, -- Stormwind Brie
            1708, -- Sweet Nectar
            2594, -- Flagon of Dwarven Mead
            3728, -- Tasty Lion Steak
            3729, -- Soothing Turtle Bisque
            3771, -- Wild Hog Shank
            3772, -- Conjured Spring Water
            4457, -- Barbecued Buzzard Wing
            4539, -- Goldenbark Apple
            4544, -- Mulgore Spice Bread
            4594, -- Rockscale Cod
            4600, -- Cherry Grog
            4607, -- Delicious Cave Mold
            4791, -- Enchanted Water
            6038, -- Giant Clam Scorcho
            6807, -- Frog Leg Stew
            8364, -- Mithril Head Trout
            10841, -- Goldthorn Tea
            12210, -- Roast Raptor
            12212, -- Jungle Stew
            12213, -- Carrion Surprise
            12214, -- Mystery Stew
            13851, -- Hot Wolf Ribs
            16169, -- Wild Ricecake
            17407, -- Graccu's Homemade Meat Pie
            18632, -- Moonbrook Riot Taffy
            19224, -- Red Hot Wings
            20074, -- Heavy Crocolisk Stew
            5342, -- Raptor Punch
            6657, -- Savory Deviate Delight
            422, -- Dwarven Mild
            733, -- Westfall Stew
            1017, -- Seasoned Wolf Kabob
            1082, -- Redridge Goulash
            1114, -- Conjured Rye
            1205, -- Melon Juice
            2136, -- Conjured Purified Water
            2593, -- Flask of Stormwind Tawny
            2596, -- Skin of Dwarven Stout
            2685, -- Succulent Pork Ribs
            3663, -- Murloc Fin Soup
            3664, -- Crocolisk Gumbo
            3665, -- Curiously Tasty Omelet
            3666, -- Gooey Spider Cake
            3726, -- Big Bear Steak
            3727, -- Hot Lion Chops
            3770, -- Mutton Chop
            4538, -- Snapvine Watermelon
            4542, -- Moist Cornbread
            4593, -- Bristle Whisker Catfish
            4606, -- Spongy Morel
            5478, -- Dig Rat Stew
            5479, -- Crispy Lizard Tail
            5480, -- Lean Venison
            5526, -- Clam Chowder
            5527, -- Goblin Deviled Clams
            6522, -- Deviate Fish
            7228, -- Tigule and Foror's Strawberry Ice Cream
            9451, -- Bubbling Water
            12209, -- Lean Wolf Steak
            16170, -- Steamed Mandu
            17403, -- Steamwheedle Fizzy Spirits
            19299, -- Fizzy Faire Drink
            19305, -- Pickled Kodo Foot
            21072, -- Smoked Sagefish
            414, -- Dalaran Sharp
            724, -- Goretusk Liver Pie
            961, -- Healing Herb
            1113, -- Conjured Bread
            1179, -- Ice Cold Milk
            2287, -- Haunch of Meat
            2288, -- Conjured Fresh Water
            2682, -- Cooked Crab Claw
            2683, -- Crab Cake
            2684, -- Coyote Steak
            2687, -- Dry Pork Ribs
            3220, -- Blood Sausage
            3662, -- Crocolisk Steak
            4537, -- Tel'Abim Banana
            4541, -- Freshly Baked Bread
            4592, -- Longjaw Mud Snapper
            4605, -- Red-Speckled Mushroom
            5066, -- Fissure Plant
            5095, -- Rainbow Fin Albacore
            5476, -- Fillet of Frenzy
            5477, -- Strider Stew
            5525, -- Boiled Clams
            6316, -- Loch Frenzy Delight
            6890, -- Smoked Bear Meat
            7676, -- Thistle Tea
            11584, -- Cactus Apple Surprise
            12238, -- Darkshore Grouper
            16167, -- Versicolor Treat
            17119, -- Deeprun Rat Kabob
            17404, -- Blended Bean Brew
            17406, -- Holiday Cheesewheel
            18633, -- Styleen's Sour Suckerpop
            19304, -- Spiced Beef Jerky
            1401, -- Riverpaw Tea Leaf
            117, -- Tough Jerky
            159, -- Refreshing Spring Water
            787, -- Slitherskin Mackerel
            2070, -- Darnassian Bleu
            2679, -- Charred Wolf Meat
            2680, -- Spiced Wolf Meat
            2681, -- Roasted Boar Meat
            2686, -- Thunder Ale
            2723, -- Bottle of Dalaran Noir
            2888, -- Beer Basted Boar Ribs
            2894, -- Rhapsody Malt
            3703, -- Southshore Stout
            4536, -- Shiny Red Apple
            4540, -- Tough Hunk of Bread
            4595, -- Junglevine Wine
            4604, -- Forest Mushroom Cap
            4656, -- Small Pumpkin
            5057, -- Ripe Watermelon
            5349, -- Conjured Muffin
            5350, -- Conjured Water
            5472, -- Kaldorei Spider Kabob
            5473, -- Scorpid Surprise
            5474, -- Roasted Kodo Meat
            6290, -- Brilliant Smallfish
            6299, -- Sickly Looking Fish
            6888, -- Herb Baked Egg
            7097, -- Leg Meat
            7806, -- Lollipop
            7807, -- Candy Bar
            7808, -- Chocolate Square
            9260, -- Volatile Rum
            11109, -- Special Chicken Feed
            12224, -- Crispy Bat Wing
            16166, -- Bean Soup
            17196, -- Holiday Spirits
            17197, -- Gingerbread Cookie
            17198, -- Winter Veil Egg Nog
            17344, -- Candy Cane
            18287, -- Evermurky
            18288, -- Molasses Firewater
            19221, -- Darkmoon Special Reserve
            19222, -- Cheap Beer
            19223, -- Darkmoon Dog
            20709, -- Rumsey Rum Light
            21114, -- Rumsey Rum Dark
            21151, -- Rumsey Rum Black Label
            21721  -- Moonglow
        },
        [addon.CONS.C_ITEM_ENHANCEMENTS_ID] = {
            18169, -- Flame Mantle of the Dawn
            18170, -- Frost Mantle of the Dawn
            18171, -- Arcane Mantle of the Dawn
            18172, -- Nature Mantle of the Dawn
            18173, -- Shadow Mantle of the Dawn
            18182, -- Chromatic Mantle of the Dawn
            12645, -- Thorium Shield Spike
            19971, -- High Test Eternium Fishing Line
            7967, -- Mithril Shield Spike
            7969, -- Mithril Spurs
            4407, -- Accurate Scope
            18251, -- Core Armor Kit
            18283, -- Biznicks 247x128 Accurascope
            20748, -- Brilliant Mana Oil
            20749, -- Brilliant Wizard Oil
            20747, -- Lesser Mana Oil
            20750, -- Wizard Oil
            12404, -- Dense Sharpening Stone
            12643, -- Dense Weightstone
            3829, -- Frost Oil
            20746, -- Lesser Wizard Oil
            7964, -- Solid Sharpening Stone
            7965, -- Solid Weightstone
            3824, -- Shadow Oil
            6532, -- Bright Baubles
            6533, -- Aquadynamic Fish Attractor
            7307, -- Flesh Eating Worm
            20745, -- Minor Mana Oil
            2871, -- Heavy Sharpening Stone
            3241, -- Heavy Weightstone
            6530, -- Nightcrawlers
            6811, -- Aquadynamic Fish Lens
            2863, -- Coarse Sharpening Stone
            3240, -- Coarse Weightstone
            20744, -- Minor Wizard Oil
            6529, -- Shiny Bauble
            2862, -- Rough Sharpening Stone
            3239, -- Rough Weightstone
            18262 -- Elemental Sharpening Stone
        },
        [addon.CONS.C_POTIONS_ID] = {
            13506, -- Potion of Petrification
            18253, -- Major Rejuvenation Potion
            20002, -- Greater Dreamless Sleep Potion
            13444, -- Major Mana Potion
            13456, -- Greater Frost Protection Potion
            13457, -- Greater Fire Protection Potion
            13458, -- Greater Nature Protection Potion
            13459, -- Greater Shadow Protection Potion
            13460, -- Greater Holy Protection Potion
            13461, -- Greater Arcane Protection Potion
            13462, -- Purification Potion
            20008, -- Living Action Potion
            13455, -- Greater Stoneshield Potion
            3386, -- Potion of Curing
            13446, -- Major Healing Potion
            17348, -- Major Healing Draught
            17351, -- Major Mana Draught
            13442, -- Mighty Rage Potion
            13443, -- Superior Mana Potion
            18841, -- Combat Mana Potion
            3387, -- Limited Invulnerability Potion
            9172, -- Invisibility Potion
            3928, -- Superior Healing Potion
            9144, -- Wildvine Potion
            12190, -- Dreamless Sleep Potion
            17349, -- Superior Healing Draught
            17352, -- Superior Mana Draught
            18839, -- Combat Healing Potion
            4623, -- Lesser Stoneshield Potion
            9030, -- Restorative Potion
            6149, -- Greater Mana Potion
            6050, -- Frost Protection Potion
            6052, -- Nature Protection Potion
            5633, -- Great Rage Potion
            3823, -- Lesser Invisibility Potion
            6049, -- Fire Protection Potion
            3827, -- Mana Potion
            1710, -- Greater Healing Potion
            5634, -- Free Action Potion
            6048, -- Shadow Protection Potion
            3385, -- Lesser Mana Potion
            929, -- Healing Potion
            6051, -- Holy Protection Potion
            6372, -- Swim Speed Potion
            2455, -- Minor Mana Potion
            2456, -- Minor Rejuvenation Potion
            2459, -- Swiftness Potion
            4596, -- Discolored Healing Potion
            5631, -- Rage Potion
            858, -- Lesser Healing Potion
            118  -- Minor Healing Potion
        },
        [addon.CONS.C_OTHER_ID] = {
            18606, -- Alliance Battle Standard
            8008, -- Mana Ruby
            19440, -- Powerful Anti-Venom
            13813, -- Blessed Sunfruit Juice
            19060, -- Warsong Gulch Enriched Ration
            19318, -- Bottled Alterac Spring Water
            19997, -- Harvest Nectar
            20062, -- Arathi Basin Enriched Ration
            20222, -- Defiler's Enriched Ration
            20225, -- Highlander's Enriched Ration
            20388, -- Lollipop
            20389, -- Candy Corn
            20390, -- Candy Bar
            21241, -- Winter Veil Eggnog
            21537, -- Festival Dumplings
            21711, -- Lunar Festival Invitation
            23161, -- Freshly-Squeezed Lemonade
            14894, -- Lily Root
            12586, -- Immature Venom Sac
            19026, -- Snake Burst Firework
            8007, -- Mana Citrine
            19061, -- Warsong Gulch Iron Ration
            20064, -- Arathi Basin Iron Ration
            20224, -- Defiler's Iron Ration
            20227, -- Highlander's Iron Ration
            8956, -- Oil of Immolation
            10305, -- Scroll of Protection IV
            10306, -- Scroll of Versatility IV
            10307, -- Scroll of Stamina IV
            10308, -- Scroll of Intellect IV
            10309, -- Scroll of Agility IV
            10310, -- Scroll of Strength IV
            18297, -- Thornling Seed
            5513, -- Mana Jade
            17747, -- Razorlash Root
            4479, -- Burning Charm
            4480, -- Thundering Charm
            4481, -- Cresting Charm
            19062, -- Warsong Gulch Field Ration
            20063, -- Arathi Basin Field Ration
            20223, -- Defiler's Field Ration
            20226, -- Highlander's Field Ration
            4419, -- Scroll of Intellect III
            4421, -- Scroll of Protection III
            4422, -- Scroll of Stamina III
            4424, -- Scroll of Versatility III
            4425, -- Scroll of Agility III
            4426, -- Scroll of Strength III
            5951, -- Moist Towelette
            6453, -- Strong Anti-Venom
            10684, -- Colossal Parachute
            1127, -- Flash Bundle
            2091, -- Magic Dust
            5740, -- Red Fireworks Rocket
            9312, -- Blue Firework
            9313, -- Green Firework
            9314, -- Red Streaks Firework
            9315, -- Yellow Rose Firework
            9317, -- Red, White and Blue Firework
            9318, -- Red Firework
            21212, -- Fresh Holly
            5232, -- Soulstone
            6452, -- Anti-Venom
            1477, -- Scroll of Agility II
            1478, -- Scroll of Protection II
            1711, -- Scroll of Stamina II
            1712, -- Scroll of Versatility II
            2289, -- Scroll of Strength II
            2290, -- Scroll of Intellect II
            5205, -- Sprouted Frond
            6458, -- Oil Covered Fish
            10621, -- Runed Scroll
            5206, -- Bogling Root
            5512, -- Healthstone
            1177, -- Oil of Olaf
            1399, -- Magic Candle
            3434, -- Slumber Sand
            5042, -- Red Ribboned Wrapping Paper
            5043, -- Red Ribboned Gift
            5044, -- Blue Ribboned Gift
            5048, -- Blue Ribboned Wrapping Paper
            5457, -- Severed Voodoo Claw
            17302, -- Blue Ribboned Holiday Gift
            17303, -- Blue Ribboned Wrapping Paper
            17304, -- Green Ribboned Wrapping Paper
            17305, -- Green Ribboned Holiday Gift
            17307, -- Purple Ribboned Wrapping Paper
            17308, -- Purple Ribboned Holiday Gift
            954, -- Scroll of Strength
            955, -- Scroll of Intellect
            1180, -- Scroll of Stamina
            1181, -- Scroll of Versatility
            3012, -- Scroll of Agility
            3013, -- Scroll of Protection
            5880, -- Crate With Holes
            6074, -- War Horn Mouthpiece
            6464, -- Wailing Essence
            6781, -- Bartleby's Mug
            6782, -- Marshal Haggard's Badge
            6812, -- Case of Elunite
            7266, -- Ur's Treatise on Shadow Magic
            7627, -- Dolanaar Delivery
            8048, -- Emerald Dreamcatcher
            8444, -- Executioner's Key
            8548, -- Divino-Matic Rod
            9546, -- Simple Scroll
            9569, -- Hallowed Scroll
            10663, -- Essence of Hakkar
            10687, -- Empty Vial Labeled #1
            10688, -- Empty Vial Labeled #2
            10689, -- Empty Vial Labeled #3
            10690, -- Empty Vial Labeled #4
            11148, -- Samophlange Manual Page
            11325, -- Dark Iron Ale Mug
            11914, -- Empty Cursed Ooze Jar
            11948, -- Empty Tainted Ooze Jar
            11953, -- Empty Pure Sample Jar
            12533, -- Roughshod Pike
            12712, -- Warosh's Mojo
            12884, -- Arnak's Hoof
            12885, -- Pamela's Doll
            12886, -- Pamela's Doll's Head
            12894, -- Joseph's Wedding Ring
            12922, -- Empty Canteen
            13155, -- Resonating Skull
            13156, -- Mystic Crystal
            13703, -- Kodo Bone
            14542, -- Kravel's Crate
            16282, -- Bundle of Hides
            17048, -- Rumsey Rum
            18607, -- Horde Battle Standard
            18640, -- Happy Fun Rock
            18662, -- Heavy Leather Ball
            19150, -- Sentinel Basic Care Package
            19151, -- Sentinel Standard Care Package
            19152, -- Sentinel Advanced Care Package
            19153, -- Outrider Advanced Care Package
            19154, -- Outrider Basic Care Package
            19155, -- Outrider Standard Care Package
            19183, -- Hourglass Sand
            20228, -- Defiler's Advanced Care Package
            20229, -- Defiler's Basic Care Package
            20230, -- Defiler's Standard Care Package
            20231, -- Arathor Advanced Care Package
            20233, -- Arathor Basic Care Package
            20236, -- Arathor Standard Care Package
            20391, -- Gnome Male Mask
            20392, -- Gnome Female Mask
            20397, -- Hallowed Wand - Pirate
            20398, -- Hallowed Wand - Ninja
            20399, -- Hallowed Wand - Leper Gnome
            20409, -- Hallowed Wand - Ghost
            20410, -- Hallowed Wand - Bat
            20411, -- Hallowed Wand - Skeleton
            20413, -- Hallowed Wand - Random
            20414, -- Hallowed Wand - Wisp
            20470, -- Solanian's Scrying Orb
            20471, -- Scroll of Scourge Magic
            20472, -- Solanian's Journal
            20557, -- Hallow's End Pumpkin Treat
            20561, -- Dwarf Male Mask
            20562, -- Dwarf Female Mask
            20563, -- Night Elf Female Mask
            20564, -- Night Elf Male Mask
            20565, -- Human Female Mask
            20566, -- Human Male Mask
            20567, -- Troll Female Mask
            20568, -- Troll Male Mask
            20569, -- Orc Female Mask
            20570, -- Orc Male Mask
            20571, -- Tauren Female Mask
            20572, -- Tauren Male Mask
            20573, -- Undead Male Mask
            20574, -- Undead Female Mask
            21740, -- Small Rocket Recipes
            21741, -- Cluster Rocket Recipes
            21742, -- Large Rocket Recipes
            21743, -- Large Cluster Rocket Recipes
            21744, -- Lucky Rocket Cluster
            21745, -- Elder's Moonstone
            21746, -- Lucky Red Envelope
            22236, -- Buttermilk Delight
            22237, -- Dark Desire
            22238, -- Very Berry Cream
            22239, -- Sweet Surprise
            22743, -- Bloodsail Sash
            23211, -- Toasted Smorc
            23246, -- Fiery Festival Brew
            23326, -- Midsummer Sausage
            23327, -- Fire-Toasted Bun
            23435, -- Elderberry Pie
            13508, -- Eye of Arachnida
            13509, -- Clutch of Foresight
            13514, -- Wail of the Banshee
            15778, -- Mechanical Yeti
            21325, -- Mechanical Greench
            15872, -- Arcanite Skeleton Key
            15871, -- Truesilver Skeleton Key
            1187, -- Spiked Collar
            15870, -- Golden Skeleton Key
            15869, -- Silver Skeleton Key
            18269, -- Gordok Green Grog
            18284, -- Kreeg's Stout Beatdown
            19425, -- Mysterious Lockbox
            19045, -- Stormpike Battle Standard
            19046  -- Frostwolf Battle Standard
        }
    },
    [addon.CONS.KEYS_ID] = {
        3704, -- Rusted Iron Key
        4882, -- Benedict's Key
        13304, -- Festival Lane Postbox Key
        13305, -- Elders' Square Postbox Key
        13306, -- King's Square Postbox Key
        13307, -- Fras Siabi's Postbox Key
        18268  -- Gordok Inner Door Key
    },
    [addon.CONS.MISCELLANEOUS_ID] = {
        [addon.CONS.M_HOLIDAY_ID] = {
            21100, -- Coin of Ancestry
            17405, -- Green Garden Tea
            18597, -- Orcish Orphan Whistle
            18598, -- Human Orphan Whistle
            17194, -- Holiday Spices
            17202, -- Snowball
            17685, -- Smokywood Pastures Sampler
            17727, -- Smokywood Pastures Gift Pack
            21191, -- Carefully Wrapped Present
            21216, -- Smokywood Pastures Extra-Special Gift
            21267, -- Toasting Goblet
            21270, -- Gently Shaken Gift
            21271, -- Gently Shaken Gift
            21310, -- Gaily Wrapped Present
            21314, -- Metzen's Letters and Notes
            21327, -- Ticking Present
            21328, -- Wand of Holiday Cheer
            21363, -- Festive Gift
            21519, -- Mistletoe
            21536, -- Elune Stone
            21557, -- Small Red Rocket
            21558, -- Small Blue Rocket
            21559, -- Small Green Rocket
            21561, -- Small White Rocket
            21562, -- Small Yellow Rocket
            21569, -- Firework Launcher
            21570, -- Cluster Launcher
            21571, -- Blue Rocket Cluster
            21574, -- Green Rocket Cluster
            21576, -- Red Rocket Cluster
            21589, -- Large Blue Rocket
            21590, -- Large Green Rocket
            21592, -- Large Red Rocket
            21593, -- Large White Rocket
            21595, -- Large Yellow Rocket
            21640, -- Lunar Festival Fireworks Pack
            21713, -- Elune's Candle
            21714, -- Large Blue Rocket Cluster
            21716, -- Large Green Rocket Cluster
            21718, -- Large Red Rocket Cluster
            21747, -- Festival Firecracker
            21813, -- Bag of Heart Candies
            21816, -- Heart Candy
            21817, -- Heart Candy
            21818, -- Heart Candy
            21819, -- Heart Candy
            21820, -- Heart Candy
            21821, -- Heart Candy
            21822, -- Heart Candy
            21823, -- Heart Candy
            21830, -- Empty Wrapper
            21831, -- Wrappered Gift
            22218, -- Handful of Rose Petals
            22261, -- Love Fool
            23022, -- Curmudgeon's Payoff
            23247, -- Burning Blossom
            21213, -- Preserved Holly
            17726, -- Smokywood Pastures Special Gift
            17712  -- Winter Veil Disguise Kit
        },
        [addon.CONS.M_REAGENTS_ID] = {
            6265  -- Soul Shard
        },
        [addon.CONS.M_MOUNTS_ID] = {
            21218, -- Blue Qiraji Resonating Crystal
            21321, -- Red Qiraji Resonating Crystal
            21323, -- Green Qiraji Resonating Crystal
            21324, -- Yellow Qiraji Resonating Crystal
            1132, -- Horn of the Timber Wolf
            2411, -- Black Stallion Bridle
            2414, -- Pinto Bridle
            5655, -- Chestnut Mare Bridle
            5656, -- Brown Horse Bridle
            5665, -- Horn of the Dire Wolf
            5668, -- Horn of the Brown Wolf
            5864, -- Gray Ram
            5872, -- Brown Ram
            5873, -- White Ram
            8563, -- Red Mechanostrider
            8588, -- Whistle of the Emerald Raptor
            8591, -- Whistle of the Turquoise Raptor
            8592, -- Whistle of the Violet Raptor
            8595, -- Blue Mechanostrider
            8629, -- Reins of the Striped Nightsaber
            8631, -- Reins of the Striped Frostsaber
            8632, -- Reins of the Spotted Frostsaber
            13321, -- Green Mechanostrider
            13322, -- Unpainted Mechanostrider
            13331, -- Red Skeletal Horse
            13332, -- Blue Skeletal Horse
            13333, -- Brown Skeletal Horse
            15277, -- Gray Kodo
            15290, -- Brown Kodo
            13086, -- Reins of the Winterspring Frostsaber
            13334, -- Green Skeletal Warhorse
            13335, -- Deathcharger's Reins
            18766, -- Reins of the Swift Frostsaber
            18767, -- Reins of the Swift Mistsaber
            18772, -- Swift Green Mechanostrider
            18773, -- Swift White Mechanostrider
            18774, -- Swift Yellow Mechanostrider
            18776, -- Swift Palomino
            18777, -- Swift Brown Steed
            18778, -- Swift White Steed
            18785, -- Swift White Ram
            18786, -- Swift Brown Ram
            18787, -- Swift Gray Ram
            18788, -- Swift Blue Raptor
            18789, -- Swift Olive Raptor
            18790, -- Swift Orange Raptor
            18791, -- Purple Skeletal Warhorse
            18793, -- Great White Kodo
            18794, -- Great Brown Kodo
            18795, -- Great Gray Kodo
            18796, -- Horn of the Swift Brown Wolf
            18797, -- Horn of the Swift Timber Wolf
            18798, -- Horn of the Swift Gray Wolf
            18902, -- Reins of the Swift Stormsaber
            19029, -- Horn of the Frostwolf Howler
            19030, -- Stormpike Battle Charger
            19872, -- Swift Razzashi Raptor
            19902  -- Swift Zulian Tiger
        },
        [addon.CONS.M_COMPANIONS_ID] = {
            4401, -- Mechanical Squirrel Box
            8485, -- Cat Carrier (Bombay)
            8486, -- Cat Carrier (Cornish Rex)
            8487, -- Cat Carrier (Orange Tabby)
            8488, -- Cat Carrier (Silver Tabby)
            8489, -- Cat Carrier (White Kitten)
            8490, -- Cat Carrier (Siamese)
            8491, -- Cat Carrier (Black Tabby)
            8492, -- Parrot Cage (Green Wing Macaw)
            8495, -- Parrot Cage (Senegal)
            8496, -- Parrot Cage (Cockatiel)
            8497, -- Rabbit Crate (Snowshoe)
            8498, -- Emerald Whelpling
            8499, -- Tiny Crimson Whelpling
            8500, -- Great Horned Owl
            8501, -- Hawk Owl
            10360, -- Black Kingsnake
            10361, -- Brown Snake
            10392, -- Crimson Snake
            10393, -- Undercity Cockroach
            10394, -- Prairie Dog Whistle
            10398, -- Mechanical Chicken
            10822, -- Dark Whelpling
            11023, -- Ancona Chicken
            11026, -- Tree Frog Box
            11027, -- Wood Frog Box
            11110, -- Chicken Egg
            11474, -- Sprite Darter Egg
            11825, -- Pet Bombling
            11826, -- Lil' Smoky
            15996, -- Lifelike Mechanical Toad
            19450, -- A Jubling's Tiny Home
            19462, -- Unhatched Jubling Egg
            20769, -- Disgusting Oozeling
            21277, -- Tranquil Mechanical Yeti
            21301, -- Green Helper Box
            21305, -- Red Helper Box
            21308, -- Jingling Bell
            21309, -- Snowman Kit
            22200, -- Silver Shafted Arrow
            22235, -- Truesilver Shafted Arrow
            23002, -- Turtle Box
            23007, -- Piglet's Collar
            23015, -- Rat Cage
            12264, -- Worg Carrier
            13582, -- Zergling Leash
            13583, -- Panda Collar
            13584, -- Diablo Stone
            20371, -- Blue Murloc Egg
            23083, -- Captured Flame
            8494   -- Parrot Cage (Hyacinth Macaw)
        },
        [addon.CONS.M_TELEPORT_ID] = {
            [addon.CONS.MT_HEARTSTONE_ID] = {
                6948   -- Hearthstone
            },
            [addon.CONS.MT_ARMOR_ID] = {
                22632, -- Atiesh, Greatstaff of the Guardian (Druid)
                22589, -- Atiesh, Greatstaff of the Guardian (Mage)
                22631, -- Atiesh, Greatstaff of the Guardian (Priest)
                22630  -- Atiesh, Greatstaff of the Guardian (Warlock)
            },
            [addon.CONS.MT_JEWELRY_ID] = {
                17690, -- Frostwolf Insignia Rank 1
                17905, -- Frostwolf Insignia Rank 2
                17906, -- Frostwolf Insignia Rank 3
                17907, -- Frostwolf Insignia Rank 4
                17908, -- Frostwolf Insignia Rank 5
                17909, -- Frostwolf Insignia Rank 6
                17691, -- Stormpike Insignia Rank 1
                17900, -- Stormpike Insignia Rank 2
                17901, -- Stormpike Insignia Rank 3
                17902, -- Stormpike Insignia Rank 4
                17903, -- Stormpike Insignia Rank 5
                17904  -- Stormpike Insignia Rank 6
            }
        },
        [addon.CONS.M_OTHER_ID] = {
            12033, -- Thaurissan Family Jewels
            13891, -- Bloated Salmon
            13907, -- 7 Pound Lobster
            13908, -- 9 Pound Lobster
            13909, -- 12 Pound Lobster
            13910, -- 15 Pound Lobster
            13911, -- 19 Pound Lobster
            13912, -- 21 Pound Lobster
            13913, -- 22 Pound Lobster
            13918, -- Reinforced Locked Chest
            15902, -- A Crazy Grab Bag
            20768, -- Oozing Bag
            9276, -- Pirate's Footlocker
            13874, -- Heavy Crate
            13875, -- Ironbound Locked Chest
            13876, -- 40 Pound Grouper
            13877, -- 47 Pound Grouper
            13878, -- 53 Pound Grouper
            13879, -- 59 Pound Grouper
            13880, -- 68 Pound Grouper
            13881, -- Bloated Redgill
            21228, -- Mithril Bound Trunk
            16884, -- Sturdy Junkbox
            20767, -- Scum Covered Bag
            6357, -- Sealed Crate
            8366, -- Bloated Trout
            21150, -- Iron Bound Trunk
            21164, -- Bloated Rockscale Cod
            6355, -- Sturdy Locked Chest
            16883, -- Worn Junkbox
            20766, -- Slimy Bag
            6352, -- Waterlogged Crate
            6647, -- Bloated Catfish
            21113, -- Watertight Trunk
            5332, -- Glowing Cat Figurine
            6354, -- Small Locked Chest
            6712, -- Clockwork Box
            16882, -- Battered Junkbox
            5335, -- A Sack of Coins
            16790, -- Damp Note
            6351, -- Dented Crate
            6353, -- Small Chest
            6645, -- Bloated Mud Snapper
            20708, -- Tightly Sealed Trunk
            6356, -- Battered Chest
            6643, -- Bloated Smallfish
            5020, -- Kolkar Booty Key
            5373, -- Lucky Charm
            5863, -- Guild Charter
            6307, -- Message in a Bottle
            8383, -- Plain Letter
            9279, -- White Punch Card
            9280, -- Yellow Punch Card
            9281, -- Red Punch Card
            9282, -- Blue Punch Card
            9316, -- Prismatic Punch Card
            9363, -- Sparklematic-Wrapped Box
            10460, -- Hakkari Blood
            11078, -- Relic Coffer Key
            11140, -- Prison Cell Key
            11197, -- Dark Keeper Key
            11422, -- Goblin Engineer's Renewal Gift
            11423, -- Gnome Engineer's Renewal Gift
            11602, -- Grim Guzzler Key
            11937, -- Fat Sack of Coins
            11938, -- Sack of Gems
            11966, -- Small Sack of Coins
            12973, -- Scarlet Cannonball
            18664, -- A Treatise on Military Ranks
            18675, -- Military Ranks of the Horde & Alliance
            19229, -- Sayge's Fortune #1
            19237, -- Sayge's Fortune #19
            19238, -- Sayge's Fortune #3
            19239, -- Sayge's Fortune #4
            19240, -- Sayge's Fortune #5
            19241, -- Sayge's Fortune #6
            19242, -- Sayge's Fortune #7
            19243, -- Sayge's Fortune #8
            19244, -- Sayge's Fortune #9
            19245, -- Sayge's Fortune #10
            19246, -- Sayge's Fortune #11
            19247, -- Sayge's Fortune #12
            19248, -- Sayge's Fortune #13
            19249, -- Sayge's Fortune #14
            19250, -- Sayge's Fortune #15
            19251, -- Sayge's Fortune #16
            19252, -- Sayge's Fortune #18
            19253, -- Sayge's Fortune #17
            19254, -- Sayge's Fortune #21
            19255, -- Sayge's Fortune #22
            19256, -- Sayge's Fortune #2
            19266, -- Sayge's Fortune #20
            19424, -- Sayge's Fortune #24
            19451, -- Sayge's Fortune #26
            19453, -- Sayge's Fortune #28
            19454, -- Sayge's Fortune #29
            19483, -- Peeling the Onion
            19484, -- The Frostwolf Artichoke
            19978, -- Fishing Tournament!
            20464, -- Glyphs of Calling
            20469, -- Decoded True Believer Clippings
            21103, -- Draconic for Dummies
            21104, -- Draconic for Dummies
            21105, -- Draconic for Dummies
            21108, -- Draconic for Dummies
            21110, -- Draconic for Dummies
            21130, -- Diary of Weavil
            21156, -- Scarab Bag
            21171, -- Filled Festive Mug
            21174, -- Empty Festive Mug
            21315, -- Smokywood Satchel
            22746, -- Buccaneer's Uniform
            5397, -- Defias Gunpowder
            5760, -- Eternium Lockbox
            10752, -- Emerald Encrusted Chest
            5758, -- Mithril Lockbox
            12122, -- Kum'isha's Junk
            4638, -- Reinforced Steel Lockbox
            4637, -- Steel Lockbox
            21540, -- Elune's Lantern
            4636, -- Strong Iron Lockbox
            19035, -- Lard's Special Picnic Basket
            4634, -- Iron Lockbox
            4633, -- Heavy Bronze Lockbox
            4632, -- Ornate Bronze Lockbox
            1703, -- Crystal Basilisk Spine
            9249, -- Captain's Key
            12382, -- Key to the City
            17962, -- Blue Sack of Gems
            17963, -- Green Sack of Gems
            17964, -- Gray Sack of Gems
            17965, -- Yellow Sack of Gems
            17969, -- Red Sack of Gems
            1973, -- Orb of Deception
            13379, -- Piccolo of the Flaming Fire
            -- JUNK
            18229, -- Nat Pagle's Guide to Extreme Anglin'
            18365  -- A Thoroughly Read Copy of "Nat Pagle's Guide to Extreme Anglin'."
        }
    },
    [addon.CONS.QUEST_ID] = {
        11885, -- Shadowforge Torch
        21229, -- Qiraji Lord's Insignia
        21230, -- Ancient Qiraji Artifact
        20461, -- Brann Bronzebeard's Lost Letter
        11562, -- Crystal Restore
        11563, -- Crystal Force
        11564, -- Crystal Ward
        11565, -- Crystal Yield
        11566, -- Crystal Charge
        11567, -- Crystal Spire
        4472, -- Scroll of Myzrael
        10465, -- Egg of Hakkar
        10662, -- Filled Egg of Hakkar
        10818, -- Yeh'kinya's Scroll
        11040, -- Morrowgrain
        12842, -- Crudely-Written Log
        16885, -- Heavy Junkbox
        13140, -- Blood Red Key
        11568, -- Torwa's Pouch
        11912, -- Package of Empty Ooze Containers
        11955, -- Bag of Empty Ooze Containers
        19807, -- Speckled Tastyfish
        20741, -- Deadwood Ritual Totem
        13892, -- Kodo Kombobulator
        9326, -- Grime-Encrusted Ring
        3340, -- Incendicite Ore
        2794, -- An Old History Book
        10441, -- Glowing Shard
        1357, -- Captain Sanders' Treasure Map
        19423, -- Sayge's Fortune #23
        19443, -- Sayge's Fortune #25
        19452, -- Sayge's Fortune #27
        1972, -- Westfall Deed
        3082, -- Dargol's Skull
        1307, -- Gold Pickup Schedule
        5179, -- Moss-Twined Heart
        182, -- Garrick's Head
        735, -- Rolf and Malakai's Medallions
        737, -- Holy Spring Water
        745, -- Marshal McBride's Documents
        748, -- Stormwind Armor Marker
        752, -- Red Burlap Bandana
        772, -- Large Candle
        773, -- Gold Dust
        780, -- Torn Murloc Fin
        782, -- Painted Gnoll Armband
        884, -- Ghoul Rib
        889, -- A Dusty Unsent Letter
        916, -- A Torn Journal Page
        938, -- Muddy Journal Pages
        957, -- William's Shipment
        962, -- Pork Belly Pie
        981, -- Bernice's Necklace
        1006, -- Brass Collar
        1019, -- Linen Scrap
        1083, -- Glyph of Azora
        1208, -- Maybell's Love Letter
        1252, -- Gramma Stonefield's Note
        1256, -- Crystal Kelp Frond
        1257, -- Invisibility Liquor
        1283, -- Verner's Note
        1284, -- Crate of Horseshoes
        1293, -- The State of Lakeshire
        1294, -- The General's Response
        1325, -- Daffodil Bouquet
        1327, -- Wiley's Note
        1349, -- Abercrombie's Crate
        1353, -- Shaw's Report
        1358, -- A Clue to Sanders' Treasure
        1361, -- Another Clue to Sanders' Treasure
        1362, -- Final Clue to Sanders' Treasure
        1381, -- A Mysterious Message
        1407, -- Solomon's Plea to Westfall
        1408, -- Stoutmantle's Response to Solomon
        1409, -- Solomon's Plea to Darkshire
        1410, -- Ebonlocke's Response to Solomon
        1451, -- Bottle of Zombie Juice
        1453, -- Spectral Comb
        1518, -- Ghost Hair Comb
        1532, -- Shrunken Head
        1596, -- Ghost Hair Thread
        1637, -- Letter to Ello
        1656, -- Translated Letter
        1922, -- Supplies for Sven
        1939, -- Skin of Sweet Rum
        1941, -- Cask of Merlot
        1942, -- Bottle of Moonshine
        1946, -- Mary's Looking Glass
        1956, -- Faded Shadowhide Pendant
        1968, -- Ogre's Monocle
        1971, -- Furlbrow's Deed
        1987, -- Krazek's Fixed Pot
        2004, -- Grelin Whitebeard's Journal
        2005, -- The First Troll Legend
        2113, -- Calor's Note
        2161, -- Book from Sven's Farm
        2162, -- Sarah's Ring
        2187, -- A Stack of Letters
        2188, -- A Letter to Grelin Whitebeard
        2223, -- The Collector's Schedule
        2239, -- The Collector's Ring
        2250, -- Dusky Crab Cakes
        2252, -- Miscellaneous Goblin Supplies
        2536, -- Trogg Stone Tooth
        2548, -- Barrel of Barleybrew Scalder
        2560, -- Jitters' Completed Journal
        2561, -- Chok'sul's Head
        2563, -- Strange Smelling Powder
        2607, -- Mo'grosh Crystal
        2609, -- Disarming Colloid
        2610, -- Disarming Mixture
        2611, -- Crude Flint
        2619, -- Grelin's Report
        2625, -- Menethil Statuette
        2628, -- Senir's Report
        2629, -- Intrepid Strongbox Key
        2633, -- Jungle Remedy
        2636, -- Carved Stone Idol
        2637, -- Ironband's Progress Report
        2639, -- Merrin's Letter
        2640, -- Miners' Gear
        2658, -- Ados Fragment
        2659, -- Modr Fragment
        2660, -- Golm Fragment
        2661, -- Neru Fragment
        2666, -- Barrel of Thunder Ale
        2667, -- MacGrann's Dried Meats
        2671, -- Wendigo Mane
        2676, -- Shimmerweed
        2696, -- Cask of Evershine
        2702, -- Lightforge Ingot
        2712, -- Crate of Lightforge Ingots
        2713, -- Ol' Sooty's Head
        2720, -- Muddy Note
        2722, -- Wine Ticket
        2724, -- Cloth Request
        2738, -- Green Hills of Stranglethorn - Page 14
        2760, -- Thurman's Sewing Kit
        2779, -- Tear of Tilloa
        2784, -- Musquash Root
        2788, -- Black Claw Stout
        2795, -- Book: Stresses of Iron
        2797, -- Heart of Mokk
        2799, -- Gorilla Fang
        2806, -- Package for Stormpike
        2828, -- Nissa's Remains
        2829, -- Gregor's Remains
        2830, -- Thurman's Remains
        2831, -- Devlin's Remains
        2832, -- Verna's Westfall Stew Recipe
        2834, -- Embalming Ichor
        2837, -- Thurman's Letter
        2846, -- Tirisfal Pumpkin
        2855, -- Putrid Claw
        2858, -- Darkhound Blood
        2859, -- Vile Fin Scale
        2872, -- Vicious Night Web Spider Venom
        2876, -- Duskbat Pelt
        2885, -- Scarlet Crusade Documents
        2956, -- Report on the Defias Brotherhood
        2999, -- Steelgrill's Tools
        3016, -- Gunther's Spellbook
        3017, -- Sevren's Orders
        3035, -- Laced Pumpkin
        3081, -- Nether Gem
        3084, -- Gyromechanic Gear
        3085, -- Barrel of Shimmer Stout
        3086, -- Cask of Shimmer Stout
        3117, -- Hildelve's Journal
        3156, -- Glutton Shackle
        3157, -- Darksoul Shackle
        3162, -- Notched Rib
        3163, -- Blackened Skull
        3165, -- Quinn's Potion
        3183, -- Mangy Claw
        3218, -- Pyrewood Shackle
        3234, -- Deliah's Ring
        3237, -- Sample Ichor
        3238, -- Holland's Findings
        3250, -- Bethor's Scroll
        3252, -- Deathstalker Report
        3256, -- Lake Skulker Moss
        3257, -- Lake Creeper Moss
        3264, -- Duskbat Wing
        3265, -- Scavenger Paw
        3266, -- Scarlet Armband
        3297, -- Fel Moss
        3318, -- Alaric's Remains
        3337, -- Dragonmaw War Banner
        3339, -- Dwarven Tinder
        3347, -- Bundle of Crocolisk Skins
        3348, -- Giant Crocolisk Skin
        3349, -- Sida's Bag
        3353, -- Rune-Inscribed Pendant
        3397, -- Young Crocolisk Skin
        3405, -- Raven Claw Talisman
        3406, -- Black Feather Quill
        3407, -- Sapphire of Sky
        3408, -- Rune of Nesting
        3409, -- Nightsaber Fang
        3411, -- Strigid Owl Feather
        3412, -- Webwood Spider Silk
        3418, -- Fel Cone
        3425, -- Woven Wand
        3460, -- Holland's Special Drink
        3468, -- Renferrel's Findings
        3495, -- Elixir of Suffering
        3497, -- Elixir of Pain
        3498, -- Taretha's Necklace
        3502, -- Mudsnout Blossom
        3506, -- Mudsnout Composite
        3508, -- Mudsnout Mixture
        3514, -- Mor'Ladim's Skull
        3518, -- Decrypted Letter
        3520, -- Tainted Keg
        3521, -- Cleverly Encrypted Letter
        3601, -- Syndicate Missive
        3613, -- Valdred's Hands
        3615, -- Kurzen's Head
        3616, -- Mind's Eye
        3618, -- Gobbler's Head
        3619, -- Snellig's Snuffbox
        3621, -- Ivar's Head
        3622, -- Essence of Nightlash
        3623, -- Thule's Head
        3625, -- Nek'rosh's Head
        3626, -- Head of Baron Vardus
        3627, -- Fang of Vagash
        3629, -- Mistmantle Family Ring
        3631, -- Bellygrub's Tusk
        3634, -- Head of Grimson
        3635, -- Maggot Eye's Paw
        3636, -- Scale of Old Murk-Eye
        3637, -- Head of VanCleef
        3639, -- Ear of Balgaras
        3659, -- Worn Leather Book
        3660, -- Tomes of Alterac
        3672, -- Head of Nagaz
        3684, -- Perenolde Tiara
        3688, -- Bloodstone Oval
        3689, -- Bloodstone Marble
        3690, -- Bloodstone Shard
        3691, -- Bloodstone Wedge
        3692, -- Hillsbrad Human Skull
        3693, -- Humbert's Sword
        3701, -- Darthalia's Sealed Commendation
        3710, -- Rod of Helcular
        3715, -- Bracers of Earth Binding
        3717, -- Sack of Murloc Heads
        3718, -- Foreboding Plans
        3721, -- Farren's Report
        3863, -- Jungle Stalker Feather
        3876, -- Fang of Bhag'thera
        3877, -- Talon of Tethis
        3879, -- Paw of Sin'Dall
        3880, -- Head of Bangalash
        3897, -- Dizzy's Eye
        3898, -- Library Scrip
        3900, -- Pupellyverbos Port
        3901, -- Bloodscalp Tusk
        3905, -- Nezzliok's Head
        3906, -- Balia'mah Trophy
        3907, -- Ziata'jai Trophy
        3910, -- Snuff
        3911, -- Pulsing Blue Shard
        3912, -- Soul Gem
        3919, -- Mistvale Giblets
        3920, -- Bloodsail Charts
        3922, -- Shaky's Payment
        3923, -- Water Elemental Bracers
        3924, -- Maury's Clubbed Foot
        3925, -- Jon-Jon's Golden Spyglass
        3926, -- Chucky's Huge Ring
        3932, -- Smotts' Chest
        3960, -- Bag of Water Elemental Bracers
        4016, -- Zanzil's Mixture
        4027, -- Catelyn's Blade
        4028, -- Bundle of Akiris Reeds
        4029, -- Akiris Reed
        4034, -- Stone of the Tides
        4053, -- Large River Crocolisk Skin
        4085, -- Krazek's Crock Pot
        4103, -- Shackle Key
        4104, -- Snapjaw Crocolisk Skin
        4105, -- Elder Crocolisk Skin
        4106, -- Tumbled Crystal
        4278, -- Lesser Bloodstone Ore
        4429, -- Deepfury's Orders
        4432, -- Sully Balloo's Letter
        4435, -- Mote of Myzrael
        4440, -- Sigil of Strom
        4441, -- MacKreel's Moonshine
        4450, -- Sigil Fragment
        4453, -- Sigil of Thoradin
        4458, -- Sigil of Arathor
        4466, -- Sigil of Trollbane
        4467, -- Sigil of Ignaeus
        4468, -- Sheathed Trol'kalar
        4469, -- Rod of Order
        4473, -- Eldritch Shackles
        4482, -- Sealed Folder
        4483, -- Burning Key
        4484, -- Cresting Key
        4485, -- Thundering Key
        4487, -- Maiden's Folly Charts
        4488, -- Spirit of Silverpine Charts
        4489, -- Maiden's Folly Log
        4490, -- Spirit of Silverpine Log
        4491, -- Goggles of Gem Hunting
        4492, -- Elven Gem
        4493, -- Elven Gems
        4494, -- Seahorn's Sealed Letter
        4495, -- Bloodstone Amulet
        4502, -- Sample Elven Gem
        4503, -- Witherbark Tusk
        4506, -- Stromgarde Badge
        4510, -- Befouled Bloodstone Orb
        4512, -- Highland Raptor Eye
        4513, -- Raptor Heart
        4514, -- Sara Balloo's Plea
        4515, -- Marez's Head
        4516, -- Otto's Head
        4517, -- Falconcrest's Head
        4518, -- Torn Scroll Fragment
        4519, -- Crumpled Scroll Fragment
        4520, -- Singed Scroll Fragment
        4522, -- Witherbark Medicine Pouch
        4525, -- Trelane's Wand of Invocation
        4526, -- Raptor Talon Amulet
        4527, -- Azure Agate
        4528, -- Tor'gan's Orb
        4529, -- Enchanted Agate
        4530, -- Trelane's Phylactery
        4531, -- Trelane's Orb
        4532, -- Trelane's Ember Agate
        4533, -- Sealed Letter to Archmage Malin
        4551, -- Or'Kalar's Head
        4610, -- Carved Stone Urn
        4612, -- Black Drake's Heart
        4615, -- Blacklash's Bindings
        4621, -- Ambassador Infernus' Bracer
        4626, -- Small Stone Shard
        4627, -- Large Stone Slab
        4628, -- Bracers of Rock Binding
        4629, -- Supply Crate
        4631, -- Tablet of Ryun'eh
        4635, -- Hammertoe's Amulet
        4640, -- Sign of the Earth
        4644, -- The Legacy Heart
        4645, -- Chains of Hematus
        4647, -- Yagyin's Digest
        4648, -- Sigil of the Hammer
        4649, -- Bonegrip's Note
        4650, -- Bel'dugur's Note
        4654, -- Mysterious Fossil
        4702, -- Prospector's Pick
        4703, -- Broken Tools
        4742, -- Mountain Cougar Pelt
        4751, -- Windfury Talon
        4752, -- Azure Feather
        4753, -- Bronze Feather
        4758, -- Prairie Wolf Paw
        4759, -- Plainstrider Talon
        4769, -- Trophy Swoop Quill
        4783, -- Totem of Hawkwind
        4801, -- Stalker Claws
        4802, -- Cougar Claws
        4803, -- Prairie Alpha Tooth
        4805, -- Flatland Cougar Femur
        4819, -- Fizsprocket's Clipboard
        4823, -- Water of the Seers
        4834, -- Venture Co. Documents
        4843, -- Amethyst Runestone
        4844, -- Opal Runestone
        4845, -- Diamond Runestone
        4846, -- Cog #5
        4850, -- Bristleback Attack Plans
        4859, -- Burning Blade Medallion
        4862, -- Scorpid Worker Tail
        4863, -- Gnomish Tools
        4871, -- Searing Collar
        4883, -- Admiral Proudmoore's Orders
        4887, -- Intact Makrura Eye
        4888, -- Crawler Mucus
        4892, -- Durotar Tiger Fur
        4897, -- Thunderhawk Saliva Gland
        4898, -- Lightning Gland
        4905, -- Sarkoth's Mangled Claw
        4918, -- Sack of Supplies
        4992, -- Recruitment Letter
        4995, -- Signed Recruitment Letter
        5006, -- Khazgorm's Journal
        5012, -- Fungal Spores
        5021, -- Explosive Stick of Gann
        5022, -- Kodobane's Head
        5023, -- Verog's Head
        5025, -- Hezrul's Head
        5026, -- Fire Tar
        5027, -- Rendered Spores
        5030, -- Centaur Bracers
        5038, -- Tear of the Moons
        5050, -- Ignition Key
        5051, -- Dig Rat
        5052, -- Unconscious Dig Rat
        5054, -- Samophlange
        5056, -- Root Sample
        5058, -- Silithid Egg
        5059, -- Digging Claw
        5061, -- Stolen Silver
        5063, -- Kreenig Snarlsnout's Tusk
        5064, -- Witchwing Talon
        5067, -- Serena's Head
        5068, -- Dried Seeds
        5072, -- Lok's Skull
        5073, -- Nak's Skull
        5074, -- Kuz's Skull
        5076, -- Shipment of Boots
        5077, -- Telescopic Lens
        5078, -- Theramore Medal
        5080, -- Gazlowe's Ledger
        5084, -- Cap'n Garvey's Head
        5085, -- Quilboar Tusk
        5086, -- Zhevra Hooves
        5087, -- Plainstrider Beak
        5088, -- Control Console Operating Manual
        5089, -- Console Key
        5096, -- Huntress Claws
        5097, -- Cats Eye Emerald
        5098, -- Altered Snapjaw Shell
        5164, -- Thunderhawk Wings
        5165, -- Sunscale Feather
        5168, -- Timberling Seed
        5169, -- Timberling Sprout
        5170, -- Mossy Tumor
        5184, -- Filled Crystal Phial
        5185, -- Crystal Phial
        5186, -- Partially Filled Vessel
        5188, -- Filled Vessel
        5189, -- Glowing Fruit
        5190, -- Shimmering Frond
        5203, -- Flatland Prowler Claw
        5204, -- Bloodfeather Belt
        5217, -- Tainted Heart
        5218, -- Cleansed Timberling Heart
        5219, -- Inscribed Bark
        5221, -- Melenas' Head
        5233, -- Stone of Relu
        5234, -- Flagongut's Fossil
        5272, -- Insane Scribbles
        5334, -- 99-Year-Old Port
        5336, -- Grell Earring
        5338, -- Ancient Moonstone Seal
        5339, -- Serpentbloom
        5348, -- Worn Parchment
        5354, -- Letter to Delgren
        5359, -- Lorgalis Manuscript
        5360, -- Highborne Relic
        5366, -- Glowing Soul Gem
        5382, -- Anaya's Pendant
        5388, -- Ran Bloodtooth's Skull
        5390, -- Fandral's Message
        5391, -- Rare Earth
        5412, -- Thresher Eye
        5415, -- Thunderhorn Cleansing Totem
        5416, -- Wildmane Cleansing Totem
        5424, -- Ancient Statuette
        5437, -- Bathran's Hair
        5445, -- Ring of Zoram
        5455, -- Divined Scroll
        5456, -- Divining Scroll
        5460, -- Orendil's Cure
        5461, -- Branch of Cenarius
        5463, -- Glowing Gem
        5464, -- Iron Shaft
        5481, -- Satyr Horns
        5490, -- Wrathtail Head
        5493, -- Elune's Tear
        5505, -- Teronis' Journal
        5508, -- Fallen Moonstone
        5519, -- Iron Pommel
        5534, -- Parker's Lunch
        5535, -- Compendium of the Fallen
        5536, -- Mythology of the Titans
        5538, -- Vorrel's Wedding Ring
        5539, -- Letter of Commendation
        5544, -- Bloodclaw's Collection
        5547, -- Reconstructed Rod
        5588, -- Lydon's Toxin
        5594, -- Letter to Jin'Zil
        5619, -- Jade Phial
        5620, -- Vial of Innocent Blood
        5621, -- Tourmaline Phial
        5623, -- Amethyst Phial
        5628, -- Zamah's Note
        5638, -- Toxic Fogger
        5639, -- Filled Jade Phial
        5645, -- Filled Tourmaline Phial
        5646, -- Vial of Blessed Water
        5659, -- Smoldering Embers
        5664, -- Corroded Shrapnel
        5675, -- Crystalized Scales
        5692, -- Remote Detonator (Red)
        5693, -- Remote Detonator (Blue)
        5694, -- NG-5 Explosives (Red)
        5695, -- NG-5 Explosives (Blue)
        5731, -- Scroll of Messaging
        5732, -- NG-5
        5733, -- Unidentified Ore
        5734, -- Super Reaper 6000 Blueprints
        5735, -- Sealed Envelope
        5737, -- Covert Ops Plans: Alpha & Beta
        5790, -- Lonebrow's Journal
        5792, -- Razorflank's Medallion
        5793, -- Razorflank's Heart
        5794, -- Salty Scorpid Venom
        5795, -- Hardened Tortoise Shell
        5797, -- Indurium Flake
        5799, -- Kravel's Parts Order
        5800, -- Kravel's Parts
        5801, -- Kraul Guano
        5802, -- Delicate Car Parts
        5803, -- Speck of Dream Dust
        5804, -- Goblin Rumors
        5807, -- Fool's Stout Report
        5810, -- Fresh Carcass
        5811, -- Frostmaw's Mane
        5824, -- Tablet of Will
        5825, -- Treshala's Pendant
        5826, -- Kravel's Scheme
        5827, -- Fizzle Brassbolts' Letter
        5830, -- Kenata's Head
        5831, -- Fardel's Head
        5832, -- Marcel's Head
        5833, -- Indurium Ore
        5834, -- Mok'Morokk's Snuff
        5835, -- Mok'Morokk's Grog
        5836, -- Mok'Morokk's Strongbox
        5838, -- Kodo Skin Scroll
        5840, -- Searing Tongue
        5841, -- Searing Heart
        5842, -- Unrefined Ore Sample
        5843, -- Grenka's Claw
        5844, -- Fragments of Rok'Alim
        5846, -- Korran's Sealed Note
        5847, -- Mirefin Head
        5848, -- Hollow Vulture Bone
        5849, -- Crate of Crash Helmets
        5850, -- Belgrom's Sealed Note
        5851, -- Cozzle's Key
        5852, -- Fuel Regulator Blueprints
        5853, -- Intact Silithid Carapace
        5854, -- Silithid Talon
        5855, -- Silithid Heart
        5861, -- Beginnings of the Undead Threat
        5865, -- Modified Seaforium Booster
        5866, -- Sample of Indurium Ore
        5867, -- Etched Phial
        5868, -- Filled Etched Phial
        5869, -- Cloven Hoof
        5876, -- Blueleaf Tuber
        5879, -- Twilight Pendant
        5881, -- Head of Kelris
        5882, -- Captain's Documents
        5883, -- Forked Mudrock Tongue
        5884, -- Unpopped Darkmist Eye
        5897, -- Snufflenose Owner's Manual
        5917, -- Spy's Report
        5918, -- Defiant Orc Head
        5919, -- Blackened Iron Shield
        5938, -- Pristine Crawler Leg
        5945, -- Deadmire's Tooth
        5946, -- Sealed Note to Elling
        5948, -- Letter to Jorgen
        5950, -- Reethe's Badge
        5952, -- Corrupted Brain Stem
        5959, -- Acidic Venom Sac
        5960, -- Sealed Note to Watcher Backus
        5998, -- Stormpike's Request
        6016, -- Wolf Heart Sample
        6064, -- Miniature Platinum Discs
        6065, -- Khadgar's Essays on Dimensional Convergence
        6071, -- Draenethyst Crystal
        6072, -- Khan Jehn's Head
        6073, -- Khan Shaka's Head
        6077, -- Maraudine Key Fragment
        6079, -- Crude Charm
        6080, -- Shadow Panther Heart
        6081, -- Mire Lord Fungus
        6082, -- Green Whelp Blood
        6083, -- Broken Tears
        6086, -- Faustin's Truth Serum
        6089, -- Zraedus' Brew
        6091, -- Crate of Power Stones
        6145, -- Clarice's Pendant
        6167, -- Neeka's Report
        6168, -- Sawtooth Snapper Claw
        6169, -- Unprepared Sawtooth Flank
        6178, -- Shipment to Nethergarde
        6181, -- Fetish of Hakkar
        6190, -- Draenethyst Shard
        6212, -- Head of Jammal'an
        6245, -- Karnitol's Satchel
        6253, -- Leftwitch's Package
        6281, -- Rattlecage Skull
        6283, -- The Book of Ur
        6284, -- Runes of Summoning
        6285, -- Egalin's Grimoire
        6286, -- Pure Hearts
        6312, -- Dalin's Heart
        6313, -- Comar's Heart
        6435, -- Infused Burning Gem
        6436, -- Burning Gem
        6443, -- Deviate Hide
        6462, -- Secure Crate
        6479, -- Malem Pendant
        6487, -- Vile Familiar Head
        6488, -- Simple Tablet
        6490, -- Dark Parchment
        6534, -- Forged Steel Bars
        6535, -- Tablet of Verga
        6624, -- Ken'zigla's Draught
        6625, -- Dirt-Caked Pendant
        6626, -- Dogran's Pendant
        6634, -- Ritual Salve
        6640, -- Felstalker Hoof
        6652, -- Reagent Pouch
        6655, -- Glowing Ember
        6656, -- Rough Quartz
        6658, -- Example Collar
        6684, -- Snufflenose Command Stick
        6717, -- Gaffer Jack
        6718, -- Electropeller
        6753, -- Feather Charm
        6767, -- Tyranis' Pendant
        6785, -- Powers of the Void
        6799, -- Vejrek's Head
        6809, -- Elura's Medallion
        6810, -- Surena's Choker
        6838, -- Scorched Spider Fang
        6839, -- Charred Horn
        6840, -- Galvanized Horn
        6843, -- Cask of Scalder
        6844, -- Burning Blood
        6845, -- Burning Rock
        6846, -- Defias Script
        6847, -- Dark Iron Script
        6848, -- Searing Coral
        6849, -- Sunscorched Shell
        6894, -- Whirlwind Heart
        6895, -- Jordan's Smithing Hammer
        6912, -- Heartswood
        6913, -- Heartswood Core
        6914, -- Soran'ruk Fragment
        6915, -- Large Soran'ruk Fragment
        6928, -- Bloodstone Choker
        6930, -- Rod of Channeling
        6931, -- Moldy Tome
        6952, -- Thick Bear Fur
        6989, -- Vial of Hatefury Blood
        6990, -- Lesser Infernal Stone
        6991, -- Smoldering Coal
        6992, -- Jordan's Ore Shipment
        6993, -- Jordan's Refined Ore Shipment
        6994, -- Whitestone Oak Lumber
        6995, -- Corrupted Kor Gem
        6996, -- Jordan's Weapon Notes
        6997, -- Tattered Manuscript
        6999, -- Tome of the Cabal
        7006, -- Reconstructed Tome
        7083, -- Purified Kor Gem
        7119, -- Twitching Antenna
        7126, -- Smoky Iron Ingot
        7128, -- Uncloven Satyr Hoof
        7131, -- Dragonmaw Shinbone
        7134, -- Sturdy Dragonmaw Shinbone
        7206, -- Mirror Lake Water Sample
        7207, -- Jennea's Flask
        7209, -- Tazan's Satchel
        7226, -- Mage-Tastic Gizmonitor
        7231, -- Astor's Letter of Introduction
        7247, -- Chest of Containment Coffers
        7249, -- Charged Rift Gem
        7268, -- Xavian Water Sample
        7269, -- Deino's Flask
        7270, -- Laughing Sister's Hair
        7272, -- Bolt Charged Bramble
        7273, -- Witherbark Totem Stick
        7274, -- Rituals of Power
        7291, -- Infernal Orb
        7292, -- Filled Containment Coffer
        7293, -- Dalaran Mana Gem
        7294, -- Andron's Ledger
        7295, -- Tazan's Logbook
        7306, -- Fenwick's Head
        7308, -- Cantation of Manifestation
        7309, -- Dalaran Status Report
        7365, -- Gnoam Sprecklesprocket
        7442, -- Gyromast's Key
        7464, -- Glyphs of Summoning
        7498, -- Top of Gelkak's Key
        7500, -- Bottom of Gelkak's Key
        7586, -- Tharnariun's Hope
        7587, -- Thun'grim's Instructions
        7626, -- Bundle of Furs
        7628, -- Nondescript Letter
        7646, -- Crate of Inn Supplies
        7667, -- Talvash's Phial of Scrying
        7669, -- Shattered Necklace Ruby
        7670, -- Shattered Necklace Sapphire
        7671, -- Shattered Necklace Topaz
        7672, -- Shattered Necklace Power Source
        7674, -- Delivery to Mathias
        7675, -- Shipping Schedule
        7715, -- Onin's Report
        7733, -- Staff of Prehistoria
        7735, -- Jannok's Rose
        7737, -- Sethir's Journal
        7766, -- Empty Brown Waterskin
        7767, -- Empty Blue Waterskin
        7768, -- Empty Red Waterskin
        7769, -- Filled Brown Waterskin
        7770, -- Filled Blue Waterskin
        7771, -- Filled Red Waterskin
        7810, -- Vial of Purest Water
        7811, -- Remaining Drops of Purest Water
        7812, -- Corrupt Manifestation's Bracers
        7813, -- Shard of Water
        7848, -- Rock Elemental Shard
        7866, -- Empty Thaumaturgy Vessel
        7867, -- Vessel of Dragon's Blood
        7870, -- Thaumaturgy Vessel Lockbox
        7871, -- Token of Thievery
        7886, -- Untranslated Journal
        7887, -- Necklace and Gem Salvage
        7908, -- Klaven Mortwake's Journal
        7968, -- Southsea Treasure
        8009, -- Dentrium Power Stone
        8026, -- Garrett Family Treasure
        8027, -- Krom Stoutarm's Treasure
        8050, -- Tallonkai's Jewel
        8051, -- Flare Gun
        8052, -- An'Alleum Power Stone
        8053, -- Obsidian Power Source
        8070, -- Reward Voucher
        8073, -- Cache of Zanzil's Altered Mixture
        8074, -- Gallywix's Head
        8087, -- Sample of Zanzil's Altered Mixture
        8136, -- Gargantuan Tumor
        8149, -- Voodoo Charm
        8155, -- Sathrah's Sacrifice
        8344, -- Silvery Spinnerets
        8363, -- Shaman Voodoo Charm
        8428, -- Laden Dew Gland
        8431, -- Spool of Light Chartreuse Silk Thread
        8463, -- Warchief's Orders
        8523, -- Field Testing Kit
        8525, -- Zinge's Purchase Order
        8527, -- Sealed Field Testing Kit
        8528, -- Violet Powder
        8564, -- Hippogryph Egg
        8584, -- Untapped Dowsing Widget
        8585, -- Tapped Dowsing Widget
        8594, -- Insect Analysis Report
        8603, -- Thistleshrub Dew
        8643, -- Extraordinary Egg
        8644, -- Fine Egg
        8645, -- Ordinary Egg
        8646, -- Bad Egg
        8684, -- Hinterlands Honey Ripple
        8685, -- Dran's Ripple Delivery
        8686, -- Mithril Pendant
        8687, -- Sealed Description of Thredd's Visitor
        8707, -- Gahz'rilla's Electrified Scale
        8723, -- Caliph Scorpidsting's Head
        8724, -- Rin'ji's Secret
        8973, -- Thick Yeti Hide
        9153, -- Rig Blueprints
        9234, -- Tiara of the Deep
        9235, -- Pratt's Letter
        9236, -- Jangdor's Letter
        9237, -- Woodpaw Gnoll Mane
        9238, -- Uncracked Scarab Shell
        9245, -- Stoley's Bottle
        9246, -- Firebeard's Head
        9247, -- Hatecrest Naga Scale
        9248, -- Mysterious Relic
        9259, -- Troll Tribal Necklace
        9263, -- Troyas' Stave
        9266, -- Woodpaw Battle Plans
        9277, -- Techbot's Memory Core
        9278, -- Essential Artificial
        9283, -- Empty Leaden Collection Phial
        9284, -- Full Leaden Collection Phial
        9306, -- Stave of Equinex
        9308, -- Grime-Encrusted Object
        9309, -- Robo-Mechanical Guts
        9320, -- Witherbark Skull
        9321, -- Venom Bottle
        9322, -- Undamaged Venom Sac
        9328, -- Super Snapper FX
        9329, -- A Short Note
        9330, -- Snapshot of Gammerita
        9331, -- Feralas: A History
        9364, -- Heavy Leaden Collection Phial
        9365, -- High Potency Radioactive Fallout
        9368, -- Jer'kai's Signet Ring
        9369, -- Iridescent Sprite Darter Wing
        9371, -- Gordunni Orb
        9436, -- Faranell's Parcel
        9438, -- Acceptable Scorpid Sample
        9440, -- Acceptable Basilisk Sample
        9441, -- Acceptable Hyena Sample
        9442, -- Untested Scorpid Sample
        9460, -- Grimtotem Horn
        9462, -- Crate of Grimtotem Horns
        9466, -- Orwin's Shovel
        9468, -- Sharpbeak's Feather
        9471, -- Nekrum's Medallion
        9507, -- A Carefully-Packed Crate
        9528, -- Edana's Dark Heart
        9542, -- Simple Letter
        9543, -- Simple Rune
        9544, -- Simple Memorandum
        9545, -- Simple Sigil
        9547, -- Simple Note
        9548, -- Hallowed Letter
        9550, -- Encrypted Rune
        9551, -- Encrypted Sigil
        9552, -- Rune-Inscribed Note
        9553, -- Etched Parchment
        9554, -- Encrypted Tablet
        9555, -- Encrypted Letter
        9556, -- Hallowed Rune
        9557, -- Hallowed Sigil
        9558, -- Encrypted Memorandum
        9559, -- Encrypted Scroll
        9560, -- Encrypted Parchment
        9561, -- Hallowed Tablet
        9562, -- Rune-Inscribed Tablet
        9563, -- Consecrated Rune
        9564, -- Etched Tablet
        9565, -- Etched Note
        9566, -- Etched Rune
        9567, -- Etched Sigil
        9568, -- Rune-Inscribed Parchment
        9570, -- Consecrated Letter
        9571, -- Glyphic Letter
        9573, -- Glyphic Memorandum
        9574, -- Glyphic Scroll
        9575, -- Glyphic Tablet
        9576, -- Tainted Letter
        9577, -- Tainted Rune
        9578, -- Tainted Scroll
        9579, -- Tainted Parchment
        9580, -- Verdant Sigil
        9581, -- Verdant Note
        9593, -- Treant Muisek
        9594, -- Beast Muisek
        9595, -- Hippogryph Muisek
        9596, -- Faerie Dragon Muisek
        9597, -- Mountain Giant Muisek
        9606, -- Treant Muisek Vessel
        9618, -- Beast Muisek Vessel
        9619, -- Hippogryph Muisek Vessel
        9620, -- Faerie Dragon Muisek Vessel
        9621, -- Mountain Giant Muisek Vessel
        9628, -- Xerash's Herb Pouch
        9629, -- A Shrunken Head
        9738, -- Gem of Cobrahn
        9739, -- Gem of Anacondra
        9740, -- Gem of Pythas
        9741, -- Gem of Serpentis
        9978, -- Gahz'ridian Detector
        10005, -- Margol's Gigantic Horn
        10022, -- Proof of Deed
        10283, -- Wolf Heart Samples
        10327, -- Horn of Echeyakee
        10414, -- Sample Snapjaw Shell
        10420, -- Skull of the Coldbringer
        10438, -- Felix's Box
        10439, -- Durnan's Scalding Mornbrew
        10440, -- Nori's Mug
        10445, -- Drawing Kit
        10458, -- Prayer to Elune
        10459, -- Chief Sharptusk Thornmantle's Head
        10464, -- Staff of Command
        10467, -- Trader's Satchel
        10515, -- Torch of Retribution
        10556, -- Stone Circle
        10563, -- Rubbing: Rune of Beth'Amara
        10564, -- Rubbing: Rune of Jin'yael
        10565, -- Rubbing: Rune of Markri
        10566, -- Rubbing: Rune of Sael'hai
        10598, -- Hetaera's Bloodied Head
        10599, -- Hetaera's Beaten Head
        10600, -- Hetaera's Bruised Head
        10610, -- Hetaera's Blood
        10622, -- Kadrak's Flag
        10641, -- Moonpetal Lily
        10642, -- Iverron's Antidote
        10643, -- Sealed Letter to Ag'tor
        10649, -- Nightmare Shard
        10660, -- First Mosh'aru Tablet
        10661, -- Second Mosh'aru Tablet
        10678, -- Magatha's Note
        10679, -- Andron's Note
        10680, -- Jes'rimon's Note
        10681, -- Xylem's Note
        10682, -- Belnistrasz's Oathstone
        10691, -- Filled Vial Labeled #1
        10692, -- Filled Vial Labeled #2
        10693, -- Filled Vial Labeled #3
        10694, -- Filled Vial Labeled #4
        10699, -- Yeh'kinya's Bramble
        10712, -- Cuely's Elixir
        10738, -- Shipment to Galvan
        10753, -- Amulet of Grol
        10754, -- Amulet of Sevine
        10755, -- Amulet of Allistarj
        10759, -- Severed Horn of the Defiler
        10789, -- Manual of Engineering Disciplines
        10792, -- Nixx's Pledge of Secrecy
        10793, -- Overspark's Pledge of Secrecy
        10794, -- Oglethorpe's Pledge of Secrecy
        10831, -- Fel Orb
        10832, -- Fel Tracker Owner's Manual
        10958, -- Nida's Necklace
        10999, -- Ironfel
        11018, -- Un'Goro Soil
        11020, -- Evergreen Pouch
        11022, -- Packet of Tharlendris Seeds
        11024, -- Evergreen Herb Casing
        11102, -- Unhatched Sprite Darter Egg
        11103, -- Seed Voucher
        11112, -- Research Equipment
        11113, -- Crate of Foodstuffs
        11114, -- Dinosaur Bone
        11125, -- Grape Manifest
        11126, -- Tablet of Kurniya
        11129, -- Essence of the Elements
        11131, -- Hive Wall Sample
        11132, -- Unused Scraping Vial
        11133, -- Linken's Training Sword
        11136, -- Linken's Tempered Sword
        11142, -- Broken Samophlange
        11143, -- Nugget Slug
        11146, -- Broken and Battered Samophlange
        11147, -- Samophlange Manual Cover
        11149, -- Samophlange Manual
        11162, -- Linken's Superior Sword
        11169, -- Book of Aquor
        11184, -- Blue Power Crystal
        11185, -- Green Power Crystal
        11186, -- Red Power Crystal
        11188, -- Yellow Power Crystal
        11222, -- Head of Krom'zar
        11227, -- Piece of Krom'zar's Banner
        11230, -- Encased Fiery Essence
        11267, -- Elemental Shard Sample
        11268, -- Head of Argelmach
        11269, -- Intact Elemental Core
        11270, -- Nixx's Signed Pledge
        11282, -- Oglethorpe's Signed Pledge
        11283, -- Overspark's Signed Pledge
        11286, -- Thorium Shackles
        11309, -- The Heart of the Mountain
        11312, -- Lost Thunderbrew Recipe
        11313, -- Ribbly's Head
        11315, -- Bloodpetal Sprout
        11319, -- Unloaded Zapper
        11366, -- Helendis Riverhorn's Letter
        11367, -- Solomon's Plea to King Wrynn
        11368, -- Wrynn's Decree
        11412, -- Nagmara's Vial
        11462, -- Discarded Knife
        11464, -- Marshal Windsor's Lost Information
        11465, -- Marshal Windsor's Lost Information
        11466, -- Raschal's Report
        11468, -- Dark Iron Fanny Pack
        11470, -- Tablet Transcript
        11471, -- Fragile Sprite Darter Egg
        11476, -- U'cha's Pelt
        11477, -- White Ravasaur Claw
        11478, -- Un'Goro Gorilla Pelt
        11479, -- Un'Goro Stomper Pelt
        11480, -- Un'Goro Thunderer Pelt
        11504, -- Piece of Threshadon Carcass
        11509, -- Ravasaur Pheromone Gland
        11510, -- Lar'korwi's Head
        11511, -- Cenarion Beacon
        11512, -- Patch of Tainted Skin
        11513, -- Tainted Vitriol
        11514, -- Fel Creep
        11515, -- Corrupted Soul Shard
        11516, -- Cenarion Plant Salve
        11569, -- Preserved Threshadon Meat
        11570, -- Preserved Pheromone Mixture
        11583, -- Cactus Apple
        11613, -- DEBUG Samophlange Manual Cover
        11617, -- Eridan's Supplies
        11754, -- Black Diamond
        10454, -- Essence of Eranikus
        18258, -- Gordok Ogre Suit
        19803, -- Brownell's Blue Striped Racer
        19805, -- Keefer's Angelfish
        19806, -- Dezian Queenfish
        8623, -- OOX-17/TN Distress Beacon
        8705, -- OOX-22/FE Distress Beacon
        8704, -- OOX-09/HL Distress Beacon
        16408, -- Befouled Water Globe
        16303, -- Ursangous' Paw
        16304, -- Shadumbra's Head
        16305, -- Sharptalon's Claw
        9327, -- Security DELTA Data Access Card
        10593, -- Imperfect Draenethyst Fragment
        12335, -- Gemstone of Smolderthorn
        12336, -- Gemstone of Spirestone
        12337, -- Gemstone of Bloodaxe
        17224, -- Scrying Scope
        17364, -- Scrying Scope
        20858, -- Stone Scarab
        20859, -- Gold Scarab
        20860, -- Silver Scarab
        20861, -- Bronze Scarab
        20862, -- Crystal Scarab
        20863, -- Clay Scarab
        20864, -- Bone Scarab
        20865, -- Ivory Scarab
        18335, -- Pristine Black Diamond
        20866, -- Azure Idol
        20867, -- Onyx Idol
        20868, -- Lambent Idol
        20869, -- Amber Idol
        20870, -- Jasper Idol
        20871, -- Obsidian Idol
        20872, -- Vermillion Idol
        20873, -- Alabaster Idol
        20874, -- Idol of the Sun
        20875, -- Idol of Night
        20876, -- Idol of Death
        20877, -- Idol of the Sage
        20878, -- Idol of Rebirth
        20879, -- Idol of Life
        20881, -- Idol of Strife
        20882, -- Idol of War
        12806, -- Unforged Rune Covered Breastplate
        12812, -- Unfired Plate Gauntlets
        18356, -- Garona: A Study on Stealth and Treachery
        18357, -- Codex of Defense
        18358, -- The Arcanist's Cookbook
        18359, -- The Light and How to Swing It
        18360, -- Harnessing Shadows
        18361, -- The Greatest Race of Hunters
        18362, -- Holy Bologna: What the Light Won't Tell You
        18363, -- Frost Shock and You
        18364, -- The Emerald Dream
        7740, -- Gni'kiv Medallion
        7741, -- The Shaft of Tsol
        8244, -- Flawless Draenethyst Sphere
        12607, -- Brilliant Chromatic Scale
        19227, -- Ace of Beasts
        19230, -- Two of Beasts
        19231, -- Three of Beasts
        19232, -- Four of Beasts
        19233, -- Five of Beasts
        19234, -- Six of Beasts
        19235, -- Seven of Beasts
        19236, -- Eight of Beasts
        19258, -- Ace of Warlords
        19259, -- Two of Warlords
        19260, -- Three of Warlords
        19261, -- Four of Warlords
        19262, -- Five of Warlords
        19263, -- Six of Warlords
        19264, -- Seven of Warlords
        19265, -- Eight of Warlords
        19268, -- Ace of Elementals
        19269, -- Two of Elementals
        19270, -- Three of Elementals
        19271, -- Four of Elementals
        19272, -- Five of Elementals
        19273, -- Six of Elementals
        19274, -- Seven of Elementals
        19275, -- Eight of Elementals
        19276, -- Ace of Portals
        19278, -- Two of Portals
        19279, -- Three of Portals
        19280, -- Four of Portals
        19281, -- Five of Portals
        19282, -- Six of Portals
        19283, -- Seven of Portals
        19284, -- Eight of Portals
        20884, -- Qiraji Magisterial Ring
        20885, -- Qiraji Martial Drape
        20888, -- Qiraji Ceremonial Ring
        20889, -- Qiraji Regal Drape
        18704, -- Mature Blue Dragon Sinew
        18628, -- Thorium Brotherhood Contract
        19002, -- Head of Nefarian
        19003, -- Head of Nefarian
        19228, -- Beasts Deck
        19257, -- Warlords Deck
        19267, -- Elementals Deck
        19277, -- Portals Deck
        21220, -- Head of Ossirian the Unscarred
        21221, -- Eye of C'Thun
        21232, -- Imperial Qiraji Armaments
        21237, -- Imperial Qiraji Regalia
        12731, -- Pristine Hide of the Beast
        20886, -- Qiraji Spiked Hilt
        20890, -- Qiraji Ornate Hilt
        20926, -- Vek'nilash's Circlet
        20927, -- Ouro's Intact Hide
        20928, -- Qiraji Bindings of Command
        20929, -- Carapace of the Old God
        20930, -- Vek'lor's Diadem
        20931, -- Skin of the Great Sandworm
        20932, -- Qiraji Bindings of Dominance
        20933, -- Husk of the Old God
        19018, -- Dormant Wind Kissed Blade
        18563, -- Bindings of the Windseeker
        18564, -- Bindings of the Windseeker
        19016  -- Vessel of Rebirth
    },
    [addon.CONS.RECIPES_ID] = {
        [addon.CONS.R_BOOKS_ID] = {
            8046, -- Kearnen's Journal
            11732, -- Libram of Rumination
            11733, -- Libram of Constitution
            11734, -- Libram of Tenacity
            11736, -- Libram of Resilience
            11737, -- Libram of Voracity
            18332, -- Libram of Rapidity
            18333, -- Libram of Focus
            18334, -- Libram of Protection
            22739  -- Tome of Polymorph: Turtle
        },
        [addon.CONS.R_ALCHEMY_ID] = {
            13517, -- Recipe: Alchemist Stone
            20761, -- Recipe: Transmute Elemental Fire
            20013, -- Recipe: Living Action Potion
            12958, -- Recipe: Transmute Arcanite
            13478, -- Recipe: Elixir of Superior Defense
            9300, -- Recipe: Elixir of Demonslaying
            9302, -- Recipe: Ghost Dye
            9303, -- Recipe: Philosopher's Stone
            9304, -- Recipe: Transmute Iron to Gold
            9305, -- Recipe: Transmute Mithril to Truesilver
            10644, -- Recipe: Goblin Rocket Fuel
            6056, -- Recipe: Frost Protection Potion
            6057, -- Recipe: Nature Protection Potion
            17709, -- Recipe: Elixir of Frost Power
            3830, -- Recipe: Elixir of Fortitude
            5643, -- Recipe: Great Rage Potion
            6055, -- Recipe: Fire Protection Potion
            5642, -- Recipe: Free Action Potion
            6211, -- Recipe: Elixir of Ogre's Strength
            6054, -- Recipe: Shadow Protection Potion
            3394, -- Recipe: Potion of Curing
            6053, -- Recipe: Holy Protection Potion
            5640, -- Recipe: Rage Potion
            13518, -- Recipe: Potion of Petrification
            13520, -- Recipe: Flask of Distilled Wisdom
            13501, -- Recipe: Major Mana Potion
            13494, -- Recipe: Greater Fire Protection Potion
            13495, -- Recipe: Greater Frost Protection Potion
            13496, -- Recipe: Greater Nature Protection Potion
            13497, -- Recipe: Greater Arcane Protection Potion
            13499, -- Recipe: Greater Shadow Protection Potion
            13490, -- Recipe: Greater Stoneshield Potion
            13491, -- Recipe: Elixir of the Mongoose
            13482, -- Recipe: Transmute Air to Fire
            13483, -- Recipe: Transmute Fire to Earth
            13484, -- Recipe: Transmute Earth to Water
            13485, -- Recipe: Transmute Water to Air
            13486, -- Recipe: Transmute Undeath to Water
            13487, -- Recipe: Transmute Water to Undeath
            13488, -- Recipe: Transmute Life to Earth
            13489, -- Recipe: Transmute Earth to Life
            3395, -- Recipe: Limited Invulnerability Potion
            9301, -- Recipe: Elixir of Shadow Power
            21547, -- Recipe: Elixir of Greater Firepower
            9298, -- Recipe: Elixir of Giants
            9296, -- Recipe: Gift of Arthas
            9297, -- Recipe: Elixir of Dream Vision
            9295, -- Recipe: Invisibility Potion
            9294, -- Recipe: Wildvine Potion
            14634, -- Recipe: Frost Oil
            3832, -- Recipe: Elixir of Detect Lesser Invisibility
            3831, -- Recipe: Major Troll's Blood Elixir
            3396, -- Recipe: Elixir of Lesser Agility
            6663, -- Recipe: Elixir of Giant Growth
            2555, -- Recipe: Swiftness Potion
            2553, -- Recipe: Elixir of Minor Agility
            18257 -- Recipe: Major Rejuvenation Potion
        },
        [addon.CONS.R_BLACKSMITHING_ID] = {
            22221, -- Plans: Obsidian Mail Tunic
            19207, -- Plans: Dark Iron Gauntlets
            19210, -- Plans: Ebon Hand
            19211, -- Plans: Blackguard
            19212, -- Plans: Nightfall
            20040, -- Plans: Dark Iron Boots
            22219, -- Plans: Jagged Obsidian Shield
            22766, -- Plans: Ironvine Breastplate
            22767, -- Plans: Ironvine Gloves
            22768, -- Plans: Ironvine Belt
            22209, -- Plans: Heavy Obsidian Belt
            22214, -- Plans: Light Obsidian Belt
            19206, -- Plans: Dark Iron Helm
            19208, -- Plans: Black Amnesty
            19209, -- Plans: Blackfury
            19204, -- Plans: Heavy Timbermaw Boots
            19205, -- Plans: Gloves of the Dawn
            12719, -- Plans: Runic Plate Leggings
            12714, -- Plans: Runic Plate Helm
            12706, -- Plans: Runic Plate Shoulders
            12707, -- Plans: Runic Plate Boots
            12830, -- Plans: Corruption
            19202, -- Plans: Heavy Timbermaw Belt
            19203, -- Plans: Girdle of the Dawn
            12823, -- Plans: Huge Thorium Battleaxe
            8030, -- Plans: Ebon Shiv
            7995, -- Plans: Mithril Scale Bracers
            10713, -- Plans: Inlaid Mithril Cylinder
            6047, -- Plans: Golden Scale Coif
            12164, -- Plans: Massive Iron Axe
            12163, -- Plans: Moonsteel Broadsword
            12162, -- Plans: Hardened Iron Shortsword
            10858, -- Plans: Solid Iron Maul
            12718, -- Plans: Runic Breastplate
            12713, -- Plans: Radiant Leggings
            12704, -- Plans: Thorium Leggings
            12702, -- Plans: Radiant Circlet
            12697, -- Plans: Radiant Boots
            12828, -- Plans: Volcanic Hammer
            12695, -- Plans: Radiant Gloves
            12827, -- Plans: Serenity
            11615, -- Plans: Dark Iron Shoulders
            12693, -- Plans: Thorium Boots
            12694, -- Plans: Thorium Helm
            12692, -- Plans: Thorium Shield Spike
            12819, -- Plans: Ornate Thorium Handaxe
            11614, -- Plans: Dark Iron Mail
            12689, -- Plans: Radiant Breastplate
            12691, -- Plans: Wildthorn Mail
            12685, -- Plans: Radiant Belt
            12684, -- Plans: Thorium Bracers
            12682, -- Plans: Thorium Armor
            12683, -- Plans: Thorium Belt
            7990, -- Plans: Heavy Mithril Helm
            7993, -- Plans: Dazzling Mithril Rapier
            7989, -- Plans: Mithril Spurs
            8029, -- Plans: Wicked Mithril Blade
            7992, -- Plans: Blue Glittering Axe
            7975, -- Plans: Heavy Mithril Pants
            3868, -- Plans: Frost Tiger Blade
            3869, -- Plans: Shadow Crescent Axe
            3873, -- Plans: Golden Scale Cuirass
            6046, -- Plans: Steel Weapon Chain
            12261, -- Plans: Searing Golden Blade
            17706, -- Plans: Edge of Winter
            3874, -- Plans: Polished Steel Boots
            7982, -- Plans: Barbaric Iron Gloves
            7981, -- Plans: Barbaric Iron Boots
            3866, -- Plans: Jade Serpentblade
            7980, -- Plans: Barbaric Iron Helm
            3867, -- Plans: Golden Iron Destroyer
            3872, -- Plans: Golden Scale Leggings
            6045, -- Plans: Iron Counterweight
            3870, -- Plans: Green Iron Shoulders
            7978, -- Plans: Barbaric Iron Shoulders
            7979, -- Plans: Barbaric Iron Breastplate
            10424, -- Plans: Silvered Bronze Leggings
            3612, -- Plans: Green Iron Gauntlets
            6044, -- Plans: Iron Shield Spike
            3608, -- Plans: Mighty Iron Hammer
            3611, -- Plans: Green Iron Boots
            5543, -- Plans: Iridescent Hammer
            5578, -- Plans: Silvered Bronze Breastplate
            2882, -- Plans: Silvered Bronze Shoulders
            2883, -- Plans: Deadly Bronze Poniard
            6735, -- Plans: Ironforge Breastplate
            2881, -- Plans: Runed Copper Breastplate
            3610, -- Plans: Gemmed Copper Gauntlets
            3609, -- Plans: Copper Chain Vest
            22222, -- Plans: Thick Obsidian Breastplate
            22220, -- Plans: Black Grasp of the Destroyer
            17059, -- Plans: Dark Iron Reaver
            17060, -- Plans: Dark Iron Destroyer
            12833, -- Plans: Hammer of the Titans
            12835, -- Plans: Annihilator
            12836, -- Plans: Frostguard
            12838, -- Plans: Arcanite Reaper
            12839, -- Plans: Heartseeker
            20553, -- Plans: Darkrune Gauntlets
            20554, -- Plans: Darkrune Breastplate
            20555, -- Plans: Darkrune Helm
            17053, -- Plans: Fiery Chain Shoulders
            12716, -- Plans: Helm of the Great Chief
            12711, -- Plans: Whitesoul Helm
            17052, -- Plans: Dark Iron Leggings
            18264, -- Plans: Elemental Sharpening Stone
            12703, -- Plans: Storm Gauntlets
            17049, -- Plans: Fiery Chain Girdle
            17051, -- Plans: Dark Iron Bracers
            12698, -- Plans: Dawnbringer Shoulders
            11612, -- Plans: Dark Iron Plate
            11611, -- Plans: Dark Iron Sunderer
            11610, -- Plans: Dark Iron Pulverizer
            8028, -- Plans: Runed Mithril Hammer
            7991, -- Plans: Mithril Scale Shoulders
            7976, -- Plans: Mithril Shield Spike
            3875, -- Plans: Golden Scale Boots
            3871, -- Plans: Golden Scale Shoulders
            22389, -- Plans: Sageblade
            12728, -- Plans: Invulnerable Mail
            18592, -- Plans: Sulfuron Hammer
            22390, -- Plans: Persuader
            12720, -- Plans: Stronghold Gauntlets
            12717, -- Plans: Lionheart Helm
            22388  -- Plans: Titanic Leggings
        },
        [addon.CONS.R_COOKING_ID] = {
            13947, -- Recipe: Lobster Stew
            13948, -- Recipe: Mightfish Steak
            13949, -- Recipe: Baked Salmon
            13945, -- Recipe: Nightfin Soup
            13946, -- Recipe: Poached Sunscale Salmon
            13942, -- Recipe: Grilled Squid
            13943, -- Recipe: Hot Smoked Bass
            13939, -- Recipe: Spotted Yellowtail
            13940, -- Recipe: Cooked Glossy Mightfish
            13941, -- Recipe: Filet of Redgill
            16110, -- Recipe: Monster Omelet
            16111, -- Recipe: Spiced Chili Crab
            16767, -- Recipe: Undermine Clam Chowder
            18046, -- Recipe: Tender Wolf Steak
            12239, -- Recipe: Dragonbreath Chili
            12240, -- Recipe: Heavy Kodo Stew
            4609, -- Recipe: Barbecued Buzzard Wing
            6039, -- Recipe: Giant Clam Scorcho
            12228, -- Recipe: Roast Raptor
            12229, -- Recipe: Hot Wolf Ribs
            12231, -- Recipe: Jungle Stew
            12232, -- Recipe: Carrion Surprise
            12233, -- Recipe: Mystery Stew
            21219, -- Recipe: Sagefish Delight
            6369, -- Recipe: Rockscale Cod
            17062, -- Recipe: Mithril Head Trout
            20075, -- Recipe: Heavy Crocolisk Stew
            2701, -- Recipe: Seasoned Wolf Kabob
            3680, -- Recipe: Murloc Fin Soup
            3681, -- Recipe: Crocolisk Gumbo
            3682, -- Recipe: Curiously Tasty Omelet
            3683, -- Recipe: Gooey Spider Cake
            3735, -- Recipe: Hot Lion Chops
            6330, -- Recipe: Bristle Whisker Catfish
            2699, -- Recipe: Redridge Goulash
            2700, -- Recipe: Succulent Pork Ribs
            5488, -- Recipe: Crispy Lizard Tail
            5528, -- Recipe: Clam Chowder
            728, -- Recipe: Westfall Stew
            2697, -- Recipe: Goretusk Liver Pie
            2698, -- Recipe: Cooked Crab Claw
            3678, -- Recipe: Crocolisk Steak
            3679, -- Recipe: Blood Sausage
            5486, -- Recipe: Strider Stew
            6328, -- Recipe: Longjaw Mud Snapper
            6329, -- Recipe: Loch Frenzy Delight
            6368, -- Recipe: Rainbow Fin Albacore
            21099, -- Recipe: Smoked Sagefish
            6892, -- Recipe: Smoked Bear Meat
            5484, -- Recipe: Roasted Kodo Meat
            17201, -- Recipe: Winter Veil Egg Nog
            2889, -- Recipe: Beer Basted Boar Ribs
            5483, -- Recipe: Scorpid Surprise
            6325, -- Recipe: Brilliant Smallfish
            6326, -- Recipe: Slitherskin Mackerel
            12226, -- Recipe: Crispy Bat Wing
            17200, -- Recipe: Gingerbread Cookie
            18267, -- Recipe: Runn Tum Tuber Surprise
            6661, -- Recipe: Savory Deviate Delight
            18160  -- Recipe: Thistle Tea
        },
        [addon.CONS.R_ENCHANTING_ID] = {
            19449, -- Formula: Enchant Weapon - Mighty Intellect
            19448, -- Formula: Enchant Weapon - Mighty Versatility
            19447, -- Formula: Enchant Bracer - Healing Power
            19444, -- Formula: Enchant Weapon - Strength
            19445, -- Formula: Enchant Weapon - Agility
            19446, -- Formula: Enchant Bracer - Argent Versatility
            22392, -- Formula: Enchant 2H Weapon - Agility
            16224, -- Formula: Enchant Cloak - Superior Defense
            16221, -- Formula: Enchant Chest - Major Health
            20755, -- Formula: Wizard Oil
            16217, -- Formula: Enchant Shield - Greater Stamina
            20754, -- Formula: Lesser Mana Oil
            20753, -- Formula: Lesser Wizard Oil
            20752, -- Formula: Minor Mana Oil
            6349, -- Formula: Enchant 2H Weapon - Lesser Intellect
            20758, -- Formula: Minor Wizard Oil
            16253, -- Formula: Enchant Chest - Greater Stats
            16255, -- Formula: Enchant 2H Weapon - Major Versatility
            16252, -- Formula: Enchant Weapon - Crusader
            16249, -- Formula: Enchant 2H Weapon - Major Intellect
            16250, -- Formula: Enchant Weapon - Superior Striking
            16251, -- Formula: Enchant Bracer - Superior Stamina
            16246, -- Formula: Enchant Bracer - Superior Strength
            16248, -- Formula: Enchant Weapon - Unholy
            16223, -- Formula: Enchant Weapon - Icy Chill
            16220, -- Formula: Enchant Boots - Versatility
            16218, -- Formula: Enchant Bracer - Superior Versatility
            11207, -- Formula: Enchant Weapon - Fiery Weapon
            16215, -- Formula: Enchant Boots - Greater Stamina
            11226, -- Formula: Enchant Gloves - Riding Skill
            11225, -- Formula: Enchant Bracer - Greater Stamina
            11223, -- Formula: Enchant Bracer - Dodge
            11208, -- Formula: Enchant Weapon - Demonslaying
            11204, -- Formula: Enchant Bracer - Greater Versatility
            11203, -- Formula: Enchant Gloves - Advanced Mining
            11202, -- Formula: Enchant Shield - Stamina
            11166, -- Formula: Enchant Gloves - Skinning
            11168, -- Formula: Enchant Shield - Lesser Parry
            11167, -- Formula: Enchant Boots - Lesser Versatility
            17725, -- Formula: Enchant Weapon - Winter's Might
            11164, -- Formula: Enchant Weapon - Lesser Beastslayer
            11165, -- Formula: Enchant Weapon - Lesser Elemental Slayer
            11150, -- Formula: Enchant Gloves - Mining
            11152, -- Formula: Enchant Gloves - Fishing
            11101, -- Formula: Enchant Bracer - Lesser Strength
            6377, -- Formula: Enchant Boots - Minor Agility
            6375, -- Formula: Enchant Bracer - Lesser Versatility
            11038, -- Formula: Enchant 2H Weapon - Lesser Versatility
            11039, -- Formula: Enchant Cloak - Minor Agility
            11081, -- Formula: Enchant Shield - Lesser Protection
            6348, -- Formula: Enchant Weapon - Minor Beastslayer
            6346, -- Formula: Enchant Chest - Lesser Mana
            6347, -- Formula: Enchant Bracer - Minor Strength
            6344, -- Formula: Enchant Bracer - Minor Versatility
            6342, -- Formula: Enchant Chest - Minor Mana
            20726, -- Formula: Enchant Gloves - Threat
            20727, -- Formula: Enchant Gloves - Shadow Power
            20728, -- Formula: Enchant Gloves - Frost Power
            20729, -- Formula: Enchant Gloves - Fire Power
            20730, -- Formula: Enchant Gloves - Healing Power
            20731, -- Formula: Enchant Gloves - Superior Agility
            20734, -- Formula: Enchant Cloak - Stealth
            20735, -- Formula: Enchant Cloak - Subtlety
            20736, -- Formula: Enchant Cloak - Dodge
            18259, -- Formula: Enchant Weapon - Spellpower
            18260  -- Formula: Enchant Weapon - Healing Power
        },
        [addon.CONS.R_ENGINEERING_ID] = {
            16050, -- Schematic: Delicate Arcanite Converter
            16046, -- Schematic: Masterwork Target Dummy
            18656, -- Schematic: Powerful Seaforium Charge
            18652, -- Schematic: Gyrofreeze Ice Reflector
            10609, -- Schematic: Mithril Mechanical Dragonling
            19027, -- Schematic: Snake Burst Firework
            10602, -- Schematic: Deadly Scope
            7742, -- Schematic: Gnomish Cloaking Device
            13311, -- Schematic: Mechanical Dragonling
            18650, -- Schematic: EZ-Thro Dynamite II
            7561, -- Schematic: Goblin Jumper Cables
            18647, -- Schematic: Red Firework
            18648, -- Schematic: Green Firework
            18649, -- Schematic: Blue Firework
            14639, -- Schematic: Minor Recombobulator
            7560, -- Schematic: Gnomish Universal Remote
            16056, -- Schematic: Flawless Arcanite Rifle
            16054, -- Schematic: Arcanite Dragonling
            16055, -- Schematic: Arcane Bomb
            18658, -- Schematic: Ultra-Flash Shadow Reflector
            16052, -- Schematic: Voice Amplification Modulator
            16049, -- Schematic: Dark Iron Bomb
            16048, -- Schematic: Dark Iron Rifle
            18655, -- Schematic: Major Recombobulator
            21733, -- Schematic: Large Blue Rocket Cluster
            21734, -- Schematic: Large Green Rocket Cluster
            21735, -- Schematic: Large Red Rocket Cluster
            16044, -- Schematic: Lifelike Mechanical Toad
            18653, -- Schematic: Goblin Jumper Cables XL
            18654, -- Schematic: Gnomish Alarm-o-Bot
            18661, -- Schematic: World Enlarger
            10606, -- Schematic: Parachute Cloak
            10603, -- Schematic: Catseye Ultra Goggles
            10604, -- Schematic: Mithril Heavy-Bore Rifle
            11828, -- Schematic: Pet Bombling
            4417, -- Schematic: Large Seaforium Charge
            11827, -- Schematic: Lil' Smoky
            4416, -- Schematic: Goblin Land Mine
            6672, -- Schematic: Flash Bomb
            10601, -- Schematic: Bright-Eye Goggles
            21728, -- Schematic: Large Green Rocket
            21729, -- Schematic: Large Red Rocket
            4414, -- Schematic: Portable Bronze Mortar
            4413, -- Schematic: Discombobulator Ray
            4412, -- Schematic: Moonsight Rifle
            4411, -- Schematic: Flame Deflector
            4410, -- Schematic: Shadow Goggles
            4409, -- Schematic: Small Seaforium Charge
            6716, -- Schematic: EZ-Thro Dynamite
            4408, -- Schematic: Mechanical Squirrel Box
            18291, -- Schematic: Force Reactive Disk
            18292, -- Schematic: Core Marksman Rifle
            18290, -- Schematic: Biznicks 247x128 Accurascope
            10608, -- Schematic: Sniper Scope
            17720, -- Schematic: Snowmaster 9000
            4415   -- Schematic: Craftsman's Monocle
        },
        [addon.CONS.R_FIRST_AID_ID] = {
            19442, -- Formula: Powerful Anti-Venom
            6454   -- Manual: Strong Anti-Venom
        },
        [addon.CONS.R_LEATHERWORKING_ID] = {
            19331, -- Pattern: Chromatic Gauntlets
            19332, -- Pattern: Corehound Belt
            19333, -- Pattern: Molten Belt
            22769, -- Pattern: Bramblewood Belt
            22770, -- Pattern: Bramblewood Boots
            22771, -- Pattern: Bramblewood Helm
            20382, -- Pattern: Dreamscale Breastplate
            19330, -- Pattern: Lava Belt
            19327, -- Pattern: Timbermaw Brawlers
            19329, -- Pattern: Golden Mantle of the Dawn
            20506, -- Pattern: Spitfire Bracers
            20507, -- Pattern: Spitfire Gauntlets
            20508, -- Pattern: Spitfire Breastplate
            20509, -- Pattern: Sandstalker Bracers
            20510, -- Pattern: Sandstalker Gauntlets
            20511, -- Pattern: Sandstalker Breastplate
            17023, -- Pattern: Molten Helm
            17025, -- Pattern: Black Dragonscale Boots
            15762, -- Pattern: Heavy Scorpid Helm
            17022, -- Pattern: Corehound Boots
            15758, -- Pattern: Devilsaur Gauntlets
            15759, -- Pattern: Black Dragonscale Breastplate
            19326, -- Pattern: Might of the Timbermaw
            19328, -- Pattern: Dawn Treaders
            20254, -- Pattern: Warbear Woolies
            15740, -- Pattern: Frostsaber Boots
            15741, -- Pattern: Stormshroud Pants
            15742, -- Pattern: Warbear Harness
            20253, -- Pattern: Warbear Harness
            15734, -- Pattern: Living Shoulders
            15735, -- Pattern: Ironfeather Shoulders
            15725, -- Pattern: Wicked Leather Gauntlets
            15724, -- Pattern: Heavy Scorpid Bracers
            8385, -- Pattern: Turtle Scale Gloves
            18239, -- Pattern: Shadowskin Gloves
            14635, -- Pattern: Gem-Studded Leather Belt
            5973, -- Pattern: Barbaric Leggings
            7613, -- Pattern: Green Leather Armor
            18949, -- Pattern: Barbaric Bracers
            18731, -- Pattern: Heavy Leather Ball
            7362, -- Pattern: Earthen Leather Shoulders
            7290, -- Pattern: Red Whelp Gloves
            7289, -- Pattern: Black Whelp Cloak
            20576, -- Pattern: Black Whelp Tunic
            5787, -- Pattern: Murloc Scale Breastplate
            5786, -- Pattern: Murloc Scale Belt
            15779, -- Pattern: Frostsaber Tunic
            15774, -- Pattern: Heavy Scorpid Shoulders
            15775, -- Pattern: Volcanic Shoulders
            15761, -- Pattern: Frostsaber Gloves
            15755, -- Pattern: Chimeric Vest
            15747, -- Pattern: Frostsaber Leggings
            15748, -- Pattern: Heavy Scorpid Leggings
            15749, -- Pattern: Volcanic Breastplate
            15743, -- Pattern: Heavy Scorpid Belt
            15746, -- Pattern: Chimeric Leggings
            15737, -- Pattern: Chimeric Boots
            15738, -- Pattern: Heavy Scorpid Gauntlets
            15732, -- Pattern: Volcanic Leggings
            15727, -- Pattern: Heavy Scorpid Vest
            8402, -- Pattern: Tough Scorpid Helm
            8401, -- Pattern: Tough Scorpid Leggings
            8389, -- Pattern: Big Voodoo Pants
            8390, -- Pattern: Big Voodoo Cloak
            8400, -- Pattern: Tough Scorpid Shoulders
            8399, -- Pattern: Tough Scorpid Boots
            8398, -- Pattern: Tough Scorpid Gloves
            8387, -- Pattern: Big Voodoo Mask
            8395, -- Pattern: Tough Scorpid Breastplate
            8397, -- Pattern: Tough Scorpid Bracers
            8386, -- Pattern: Big Voodoo Robe
            8409, -- Pattern: Nightscape Shoulders
            7453, -- Pattern: Swift Boots
            4300, -- Pattern: Guardian Leather Bracers
            5789, -- Pattern: Murloc Scale Bracers
            7451, -- Pattern: Green Whelp Bracers
            17722, -- Pattern: Gloves of the Greatfather
            5974, -- Pattern: Guardian Cloak
            4299, -- Pattern: Guardian Armor
            7450, -- Pattern: Green Whelp Armor
            5788, -- Pattern: Thick Murloc Armor
            7449, -- Pattern: Dusky Leather Leggings
            13287, -- Pattern: Raptor Hide Harness
            13288, -- Pattern: Raptor Hide Belt
            4297, -- Pattern: Barbaric Gloves
            7364, -- Pattern: Heavy Earthen Gloves
            4296, -- Pattern: Dark Leather Shoulders
            7363, -- Pattern: Pilferer's Gloves
            7361, -- Pattern: Herbalist's Gloves
            7360, -- Pattern: Dark Leather Gloves
            4294, -- Pattern: Hillman's Belt
            5972, -- Pattern: Fine Leather Pants
            2409, -- Pattern: Dark Leather Tunic
            4293, -- Pattern: Hillman's Leather Vest
            6710, -- Pattern: Moonglow Vest
            2406, -- Pattern: Fine Leather Boots
            2408, -- Pattern: Fine Leather Gloves
            2407, -- Pattern: White Leather Jerkin
            5083, -- Pattern: Kodo Hide Bag
            7288, -- Pattern: Rugged Leather Pants
            15781, -- Pattern: Black Dragonscale Leggings
            15730, -- Pattern: Red Dragonscale Breastplate
            15770, -- Pattern: Black Dragonscale Shoulders
            15772, -- Pattern: Devilsaur Leggings
            18252, -- Pattern: Core Armor Kit
            15764, -- Pattern: Stormshroud Shoulders
            15760, -- Pattern: Ironfeather Breastplate
            15752, -- Pattern: Living Leggings
            4301, -- Pattern: Barbaric Belt
            7452, -- Pattern: Dusky Boots
            8384, -- Pattern: Comfortable Leather Hat
            4298  -- Pattern: Guardian Belt
        },
        [addon.CONS.R_TAILORING_ID] = {
            19220, -- Pattern: Flarecore Leggings
            22683, -- Pattern: Gaea's Embrace
            22772, -- Pattern: Sylvan Shoulders
            22773, -- Pattern: Sylvan Crown
            22774, -- Pattern: Sylvan Vest
            19219, -- Pattern: Flarecore Robe
            22312, -- Pattern: Satchel of Cenarius
            19217, -- Pattern: Argent Shoulders
            19218, -- Pattern: Mantle of the Timbermaw
            17018, -- Pattern: Flarecore Gloves
            17017, -- Pattern: Flarecore Mantle
            18487, -- Pattern: Mooncloth Robe
            19215, -- Pattern: Wisdom of the Timbermaw
            19216, -- Pattern: Argent Boots
            14483, -- Pattern: Felcloth Pants
            14526, -- Pattern: Mooncloth
            22308, -- Pattern: Enchanted Runecloth Bag
            22310, -- Pattern: Cenarion Herb Bag
            14468, -- Pattern: Runecloth Bag
            21358, -- Pattern: Soul Pouch
            10325, -- Pattern: White Wedding Dress
            10326, -- Pattern: Tuxedo Jacket
            10323, -- Pattern: Tuxedo Pants
            10318, -- Pattern: Admiral's Hat
            10321, -- Pattern: Tuxedo Shirt
            10317, -- Pattern: Pink Mageweave Shirt
            10314, -- Pattern: Lavender Mageweave Shirt
            22307, -- Pattern: Enchanted Mageweave Pouch
            10311, -- Pattern: Orange Martial Shirt
            7088, -- Pattern: Crimson Silk Robe
            4355, -- Pattern: Icy Cloak
            10728, -- Pattern: Black Swashbuckler's Shirt
            17724, -- Pattern: Green Holiday Shirt
            7087, -- Pattern: Crimson Silk Cloak
            7089, -- Pattern: Azure Silk Cloak
            14630, -- Pattern: Enchanter's Cowl
            6401, -- Pattern: Dark Silk Shirt
            7114, -- Pattern: Azure Silk Gloves
            14627, -- Pattern: Bright Yellow Shirt
            5772, -- Pattern: Red Woolen Bag
            6275, -- Pattern: Greater Adept's Robe
            6274, -- Pattern: Blue Overalls
            5771, -- Pattern: Red Linen Bag
            6272, -- Pattern: Blue Linen Robe
            6270, -- Pattern: Blue Linen Vest
            22309, -- Pattern: Big Bag of Enchantment
            14499, -- Pattern: Mooncloth Bag
            14494, -- Pattern: Brightcloth Pants
            14490, -- Pattern: Cindercloth Pants
            14482, -- Pattern: Cindercloth Cloak
            14476, -- Pattern: Cindercloth Gloves
            14471, -- Pattern: Cindercloth Vest
            14467, -- Pattern: Frostweave Robe
            10463, -- Pattern: Shadoweave Mask
            10320, -- Pattern: Red Mageweave Headband
            10315, -- Pattern: Red Mageweave Shoulders
            10312, -- Pattern: Red Mageweave Gloves
            10300, -- Pattern: Red Mageweave Vest
            10301, -- Pattern: White Bandit Mask
            10302, -- Pattern: Red Mageweave Pants
            4356, -- Pattern: Star Belt
            7086, -- Pattern: Earthen Silk Belt
            7084, -- Pattern: Crimson Silk Shoulders
            7085, -- Pattern: Azure Shoulders
            5775, -- Pattern: Black Silk Pack
            4353, -- Pattern: Spider Belt
            4352, -- Pattern: Boots of the Enchanter
            5774, -- Pattern: Green Silk Pack
            4351, -- Pattern: Shadow Hood
            7090, -- Pattern: Green Silk Armor
            7091, -- Pattern: Truefaith Gloves
            7092, -- Pattern: Hands of Darkness
            4350, -- Pattern: Spider Silk Slippers
            4348, -- Pattern: Phoenix Gloves
            4349, -- Pattern: Phoenix Pants
            4347, -- Pattern: Reinforced Woolen Shoulders
            6390, -- Pattern: Stylish Blue Shirt
            6391, -- Pattern: Stylish Green Shirt
            10316, -- Pattern: Colorful Kilt
            2601, -- Pattern: Gray Woolen Robe
            4346, -- Pattern: Heavy Woolen Cloak
            4292, -- Pattern: Green Woolen Bag
            4345, -- Pattern: Red Woolen Boots
            6271, -- Pattern: Red Linen Vest
            2598, -- Pattern: Red Linen Robe
            18265, -- Pattern: Flarecore Wraps
            14509, -- Pattern: Mooncloth Circlet
            14510, -- Pattern: Bottomless Bag
            20546, -- Pattern: Runed Stygian Leggings
            20547, -- Pattern: Runed Stygian Boots
            20548, -- Pattern: Runed Stygian Belt
            14507, -- Pattern: Mooncloth Shoulders
            14501, -- Pattern: Mooncloth Vest
            21371, -- Pattern: Core Felcloth Bag
            14497, -- Pattern: Mooncloth Leggings
            14493, -- Pattern: Robe of Winter Night
            14486, -- Pattern: Cloak of Fire
            4354, -- Pattern: Rich Purple Silk Shirt
            14511, -- Pattern: Gloves of Spell Mastery
            14512, -- Pattern: Truefaith Vestments
            14513  -- Pattern: Robe of the Archmage
        }
    },
    [addon.CONS.TRADE_GOODS_ID] = {
        10647, -- Engineer's Ink
        [addon.CONS.T_CLOTH_ID] = {
            14048, -- Bolt of Runecloth
            14342, -- Mooncloth
            14047, -- Runecloth
            14227, -- Ironweb Spider Silk
            14256, -- Felcloth
            14341, -- Rune Thread
            4339, -- Bolt of Mageweave
            4338, -- Mageweave Cloth
            8343, -- Heavy Silken Thread
            10285, -- Shadow Silk
            4305, -- Bolt of Silk Cloth
            4337, -- Thick Spider's Silk
            4291, -- Silken Thread
            4306, -- Silk Cloth
            2997, -- Bolt of Woolen Cloth
            2321, -- Fine Thread
            3182, -- Spider's Silk
            2592, -- Wool Cloth
            2996, -- Bolt of Linen Cloth
            2320, -- Coarse Thread
            2589  -- Linen Cloth
        },
        [addon.CONS.T_ELEMENTAL_ID] = {
            7076, -- Essence of Earth
            7080, -- Essence of Water
            7082, -- Essence of Air
            12803, -- Living Essence
            12808, -- Essence of Undeath
            7078, -- Essence of Fire
            7075, -- Core of Earth
            7077, -- Heart of Fire
            7079, -- Globe of Water
            7081, -- Breath of Wind
            7972, -- Ichor of Undeath
            10286, -- Heart of the Wild
            7067, -- Elemental Earth
            7068, -- Elemental Fire
            7069, -- Elemental Air
            7070  -- Elemental Water
        },
        [addon.CONS.T_ENCHANTING_ID] = {
            20725, -- Nexus Crystal
            16207, -- Runed Arcanite Rod
            16203, -- Greater Eternal Essence
            16206, -- Arcanite Rod
            14344, -- Large Brilliant Shard
            16204, -- Illusion Dust
            14343, -- Small Brilliant Shard
            16202, -- Lesser Eternal Essence
            11178, -- Large Radiant Shard
            11176, -- Dream Dust
            11175, -- Greater Nether Essence
            11145, -- Runed Truesilver Rod
            11177, -- Small Radiant Shard
            11174, -- Lesser Nether Essence
            11144, -- Truesilver Rod
            11139, -- Large Glowing Shard
            11135, -- Greater Mystic Essence 
            11137, -- Vision Dust
            11130, -- Runed Golden Rod
            11134, -- Lesser Mystic Essence
            11128, -- Golden Rod
            11138, -- Small Glowing Shard
            11084, -- Large Glimmering Shard
            11082, -- Greater Astral Essence
            11083, -- Soul Dust
            6339, -- Runed Silver Rod
            10978, -- Small Glimmering Shard
            10998, -- Lesser Astral Essence
            6338, -- Silver Rod
            10939, -- Greater Magic Essence 
            10938, -- Lesser Magic Essence
            10940, -- Strange Dust 
            6217, -- Copper Rod
            6218  -- Runed Copper Rod
        },
        [addon.CONS.T_PARTS_ID] = {
            16006, -- Delicate Arcanite Converter
            15994, -- Thorium Widget
            15992, -- Dense Blasting Powder
            10647, -- Engineer's Ink
            18631, -- Truesilver Transformer
            10561, -- Mithril Casing
            9060, -- Inlaid Mithril Cylinder
            9061, -- Goblin Rocket Fuel
            10560, -- Unstable Trigger
            16000, -- Thorium Tube
            10559, -- Mithril Tube
            10505, -- Solid Blasting Powder
            4389, -- Gyrochronatom
            4387, -- Iron Strut
            10558, -- Gold Power Core
            4382, -- Bronze Framework
            7191, -- Fused Wiring
            4375, -- Whirring Bronze Gizmo
            4377, -- Heavy Blasting Powder
            4400, -- Heavy Stock
            4371, -- Bronze Tube
            4404, -- Silver Contact
            4364, -- Coarse Blasting Powder
            4399, -- Wooden Stock
            4359, -- Handful of Copper Bolts
            4357, -- Rough Blasting Powder
            814, -- Flask of Oil
            4611  -- Blue Pearl
        },
        [addon.CONS.T_HERBS_ID] = {
            19727, -- Blood Scythe
            13468, -- Black Lotus
            13467, -- Icecap
            108343, -- Icecap Petal
            13466, -- Sorrowmoss
            108342, -- Sorrowmoss Leaf
            13465, -- Mountain Silversage
            108341, -- Mountain Silversage Stalk
            13463, -- Dreamfoil
            108339, -- Dreamfoil Blade
            13464, -- Golden Sansam
            108340, -- Golden Sansam Leaf
            8846, -- Gromsblood
            108338, -- Gromsblood Leaf
            8839, -- Blindweed
            108336, -- Blindweed Stem
            8845, -- Ghost Mushroom
            108337, -- Ghost Mushroom Cap
            8838, -- Sungrass
            108335, -- Sungrass Stalk
            8836, -- Arthas' Tears
            108334, -- Arthas' Tears Petal
            8831, -- Purple Lotus
            108333, -- Purple Lotus Petal
            4625, -- Firebloom
            108332, -- Firebloom Petal
            8153, -- Wildvine
            3819, -- Dragon's Teeth
            108329, -- Dragon's Teeth Stem
            3358, -- Khadgar's Whisker
            108326, -- Khadgar's Whisker Stem
            3821, -- Goldthorn
            108331, -- Goldthorn Bramble
            3818, -- Fadeleaf
            108328, -- Fadeleaf Petal
            3357, -- Liferoot
            3356, -- Kingsblood
            3355, -- Wild Steelbloom
            3369, -- Grave Moss
            2453, -- Bruiseweed
            3820, -- Stranglekelp
            2450, -- Briarthorn
            2452, -- Swiftthistle
            785, -- Mageroyal
            2449, -- Earthroot
            765, -- Silverleaf
            2447  -- Peacebloom
        },
        [addon.CONS.T_JEWELCRAFTING_ID] = {
            12363, -- Arcane Crystal
            12800, -- Azerothian Diamond
            12364, -- Huge Emerald
            12361, -- Blue Sapphire
            12799, -- Large Opal
            7910, -- Star Ruby
            11382, -- Blood of the Mount
            7909, -- Aquamarine
            3864, -- Citrine
            7971, -- Black Pearl
            13926, -- Golden Pearl
            1529, -- Jade
            1705, -- Lesser Moonstone
            1206, -- Moss Agate
            5500, -- Iridescent Pearl
            1210, -- Shadowgem
            818, -- Tigerseye
            5498, -- Small Lustrous Pearl
            774  -- Malachite
        },
        [addon.CONS.T_LEATHER_ID] = {
            20381, -- Dreamscale
            15410, -- Scale of Onyxia
            17012, -- Core Leather
            19767, -- Primal Bat Leather
            19768, -- Primal Tiger Leather
            15414, -- Red Dragonscale
            15408, -- Heavy Scorpid Scale
            12810, -- Enchanted Leather
            15416, -- Black Dragonscale
            8171, -- Rugged Hide
            15407, -- Cured Rugged Hide
            15412, -- Green Dragonscale
            8170, -- Rugged Leather
            15417, -- Devilsaur Leather
            15419, -- Warbear Leather
            15415, -- Blue Dragonscale
            8154, -- Scorpid Scale
            8165, -- Worn Dragonscale
            8168, -- Jet Black Feather
            4304, -- Thick Leather
            8150, -- Deeprock Salt
            8169, -- Thick Hide
            8172, -- Cured Thick Hide
            5785, -- Thick Murloc Scale
            8167, -- Turtle Scale
            4234, -- Heavy Leather
            4235, -- Heavy Hide
            4236, -- Cured Heavy Hide 
            4461, -- Raptor Hide
            2319, -- Medium Leather
            4232, -- Medium Hide
            4233, -- Cured Medium Hide
            5784, -- Slimy Murloc Scale
            783, -- Light Hide
            2318, -- Light Leather
            4231, -- Cured Light Hide
            4289, -- Salt
            5082, -- Thin Kodo Leather
            2934, -- Ruined Leather Scraps
            6470, -- Deviate Scale
            6471, -- Perfect Deviate Scale
            7286, -- Black Whelp Scale
            7392, -- Green Whelp Scale
            5116, -- Long Tail Feather
            17056, -- Light Feather
            15409  -- Refined Deeprock Salt
        },
        [addon.CONS.T_MEAT_ID] = {
            3404, -- Buzzard Wing
            2251, -- Gooey Spider Leg
            5470,  -- Thunder Lizard Tail
            [addon.CONS.TM_ANIMAL_ID] = {
                2674, -- Crawler Meat
                21024, -- Chimaerok Tenderloin
                20424, -- Sandworm Meat
                7974, -- Zesty Clam Meat
                12206, -- Tender Crab Meat
                12208, -- Tender Wolf Meat
                12205, -- White Spider Meat
                4655, -- Giant Clam Meat
                12204, -- Heavy Kodo Meat
                3712, -- Turtle Meat
                12037, -- Mystery Meat
                12184, -- Raptor Flesh
                12202, -- Tiger Meat
                12203, -- Red Wolf Meat
                3667, -- Tender Crocolisk Meat
                5471, -- Stag Meat
                5504, -- Tangy Clam Meat
                3731, -- Lion Meat
                3730, -- Big Bear Meat
                1080, -- Tough Condor Meat
                5468, -- Soft Frenzy Flesh
                2677, -- Boar Ribs
                5503, -- Clam Meat
                723, -- Goretusk Liver
                2673, -- Coyote Meat
                8959, -- Raw Spinefin Halibut
                13888, -- Darkclaw Lobster
                13889, -- Raw Whitescale Salmon
                4603, -- Raw Spotted Yellowtail
                1015, -- Lean Wolf Flank
                1468, -- Murloc Fin
                2924, -- Crocolisk Meat
                2675, -- Crawler Claw
                3173, -- Bear Meat
                5469, -- Strider Meat
                5466, -- Scorpid Stinger
                769, -- Chunk of Boar Meat
                2672, -- Stringy Wolf Meat
                2886, -- Crag Boar Rib
                5465, -- Small Spider Leg
                12223, -- Meaty Bat Wing
                2665, -- Stormwind Seasoning Herbs
                5467  -- Kodo Meat
            },
            [addon.CONS.TM_EGG_ID] = {
                12207, -- Giant Egg
                6889, -- Small Egg
                3685  -- Raptor Egg
            },
            [addon.CONS.TM_FISH_ID] = {
                13888, -- Darkclaw Lobster
                13889, -- Raw Whitescale Salmon
                13754, -- Raw Glossy Mightfish
                13758, -- Raw Redgill
                13759, -- Raw Nightfin Snapper
                13760, -- Raw Sunscale Salmon
                4603, -- Raw Spotted Yellowtail
                13756, -- Raw Summer Bass
                21153, -- Raw Greater Sagefish
                8365, -- Raw Mithril Head Trout
                6362, -- Raw Rockscale Cod
                6308, -- Raw Bristle Whisker Catfish
                21071, -- Raw Sagefish
                6289, -- Raw Longjaw Mud Snapper
                6317, -- Raw Loch Frenzy
                6361, -- Raw Rainbow Fin Albacore
                6291, -- Raw Brilliant Smallfish
                6303  -- Raw Slitherskin Mackerel
            },
        },
        [addon.CONS.T_METAL_STONE_ID] = {
            17203, -- Sulfuron Ingot
            18562, -- Elementium Ingot
            18567, -- Elemental Flux
            12809, -- Guardian Stone
            12655, -- Enchanted Thorium Bar
            12360, -- Arcanite Bar
            11370, -- Dark Iron Ore
            11371, -- Dark Iron Bar
            12359, -- Thorium Bar
            6037, -- Truesilver Bar
            12644, -- Dense Grinding Stone
            12365, -- Dense Stone
            3858, -- Mithril Ore
            108300, -- Mithril Ore Nugget
            3860, -- Mithril Bar
            10620, -- Thorium Ore
            108298, -- Thorium Ore Nugget
            7911, -- Truesilver Ore
            108299, -- Truesilver Ore Nugget
            3859, -- Steel Bar
            7912, -- Solid Stone
            7966, -- Solid Grinding Stone
            3577, -- Gold Bar
            2772, -- Iron Ore
            108297, -- Iron Ore Nugget
            3575, -- Iron Bar
            3857, -- Coal
            2776, -- Gold Ore
            108296, -- Gold Ore Nugget
            3486, -- Heavy Grinding Stone
            2838, -- Heavy Stone
            2771, -- Tin Ore
            108295, -- Tin Ore Nugget
            2841, -- Bronze Bar
            3478, -- Coarse Grinding Stone
            3576, -- Tin Bar
            2836, -- Coarse Stone
            2775, -- Silver Ore
            108294, -- Silver Ore Nugget
            2842, -- Silver Bar
            2770, -- Copper Ore
            2840, -- Copper Bar
            3470, -- Rough Grinding Stone
            2835, -- Rough Stone
            22202, -- Small Obsidian Shard
            22203  -- Large Obsidian Shard
        },
        [addon.CONS.T_OTHER_ID] = {
            19943, -- Massive Mojo
            12804, -- Powerful Mojo
            4342, -- Purple Dye
            10290, -- Pink Dye
            13423, -- Stonescale Oil
            9210, -- Ghost Dye
            13422, -- Stonescale Eel
            13757, -- Lightning Eel
            2325, -- Black Dye
            7973, -- Big-Mouth Clam
            9262, -- Black Vitriol
            6261, -- Orange Dye
            5637, -- Large Fang
            11291, -- Star Wood
            3466, -- Strong Flux
            4340, -- Gray Dye
            4341, -- Yellow Dye
            4402, -- Small Flame Sac
            6359, -- Firefin Snapper
            6371, -- Fire Oil
            7072, -- Naga Scale
            5524, -- Thick-Shelled Clam
            2605, -- Green Dye
            5635, -- Sharp Claw
            6358, -- Oily Blackmouth
            6370, -- Blackmouth Oil
            2604, -- Red Dye
            5523, -- Small Barnacled Clam
            6260, -- Blue Dye
            2324, -- Bleach
            2678, -- Mild Spices
            2880, -- Weak Flux
            4470, -- Simple Wood
            1288, -- Large Venom Sac
            1475, -- Small Venom Sac
            3164, -- Discolored Worg Heart
            3371, -- Crystal Vial
            15874, -- Soft-Shelled Clam
            19441, -- Huge Venom Sac
            12811, -- Righteous Orb
            20520, -- Dark Rune
            12662, -- Demonic Rune
            18240, -- Ogre Tannin
            17010, -- Fiery Core
            17011, -- Lava Core
            9719, -- Broken Blade of Heroes
            17204  -- Eye of Sulfuras
        }
    }
}