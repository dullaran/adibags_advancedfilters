local _, addon = ...

addon.ITEM_DATABASE[addon.CONS.CATACLYSM_ID] = {
    [addon.CONS.CONTAINERS_ID] = {
        60335, -- Thick Hide Pack
        67390, -- "Carriage - Maddy" High Tech Bag
        67392, -- "Carriage - Exclusive" Gem Studded Clutch
        67387, -- "Carriage" Signature Bag
        67389, -- "Carriage - Exclusive" Enchanting Evening Purse
        67393, -- "Carriage - Going Green" Herb Tote Bag
        67394, -- "Carriage - Xandera" Student's Satchel
        67395, -- "Carriage - Meeya" Leather Bag
        67396, -- "Carriage - Christina" Precious Metal Bag
        46753, -- Melithar's Supply Bag
        56148, -- Bank Robber's Bag
        56149, -- Empty Town-In-A-Box
        57247, -- Grape-Picking Sack
        57251, -- "Collecting" Bag
        57542, -- Coldridge Mountaineer's Pouch
        57549, -- Prospector's Bag
        57790, -- Pumpkin Bag
        57791, -- Headsman's Bag
        57792, -- Bag of Thorns
        57793, -- Kodo Saddlebag
        59053, -- Airfield Courier Bag
        60239, -- Foul Bag
        60240, -- Dream Carrier
        60241, -- Dusty Bag
        60242, -- Kron's New Hunting Bag
        60260, -- Sea Witch's Bag
        60731, -- Message Carrier
        52039, -- Shipwright's Bag
        52040, -- Salvager's Bag
        56147, -- Fatcandle Bag
        54443, -- Embersilk Bag
        67525, -- Bilgewater Satchel
        67526, -- Darnassian Satchel
        67527, -- Exodar Satchel
        67528, -- Ironforge Satchel
        67529, -- Undercity Satchel
        67530, -- Gnomeregan Satchel
        67531, -- Stormwind Satchel
        67532, -- Gilnean Satchel
        67533, -- Orgrimmar Satchel
        67534, -- Thunder Bluff Satchel
        67535, -- Silvermoon Satchel
        67536, -- Darkspear Satchel
        54444, -- Illusionary Bag
        69748, -- Tattered Hexcloth Bag
        54445, -- Otherworldly Bag
        60217, -- Elementium Toolbox
        70138, -- Luxurious Silk Gem Bag
        54446, -- Hyjal Expedition Bag
        70136, -- Royal Scribe's Satchel
        70137, -- Triple-Reinforced Mining Bag
        60218  -- Lure Master Tackle Box
    },
    [addon.CONS.CONSUMABLES_ID] = {
        [addon.CONS.C_BANDAGES_ID] = {
            53051, -- Dense Embersilk Bandage
            63391, -- Baradin's Wardens Bandage
            64995, -- Hellscream's Reach Bandage
            53050, -- Heavy Embersilk Bandage
            53049  -- Embersilk Bandage
        },
        [addon.CONS.C_CONSUMABLES_ID] = {
            63396, -- Big Daddy
            60853, -- Volatile Seaforium Blastpack
            40727, -- Gnomish Gravity Well
            60223, -- High-Powered Bolt Gun
            60854, -- Loot-A-Rang
            67494, -- Electrostatic Condenser
            65665  -- Burgy Blackheart's Handsome Hat
        },
        [addon.CONS.C_ELIXIRS_ID] = {
            58084, -- Ghost Elixir
            58089, -- Elixir of the Naga
            58092, -- Elixir of the Cobra
            58093, -- Elixir of Deep Earth
            58094, -- Elixir of Impossible Accuracy
            58143, -- Prismatic Elixir
            58144, -- Elixir of Mighty Speed
            58148, -- Elixir of the Master
            64640, -- Infectis Puffer Sashimi
            78883  -- Darkmoon Firewater
        },
        [addon.CONS.C_FLASKS_ID] = {
            58085, -- Flask of Steelskin
            58086, -- Flask of the Draconic Mind
            58087, -- Flask of the Winds
            58088, -- Flask of Titanic Strength
            65455, -- Flask of Battle
            67438  -- Flask of Flowing Water
        },
        [addon.CONS.C_FOOD_DRINKS_ID] = {
            58257, -- Highland Spring Water
            58259, -- Highland Sheep Cheese
            58261, -- Buttery Wheat Roll
            58263, -- Grilled Shark
            58265, -- Highland Pomegranate
            58267, -- Scarlet Polypore
            58269, -- Massive Turkey Leg
            63251, -- Mei's Masterful Brew
            65499, -- Conjured Mana Cake
            68140, -- Invigorating Pineapple Punch
            70924, -- Eternal Eye of Elune
            70925, -- Eternal Lunar Pear
            70926, -- Eternal Moonberry
            70927, -- Eternal Sunfruit
            73260, -- Salty Sea Dog
            74822, -- Sasparilla Sinker
            74921, -- Darkmoon Doughnut
            60858, -- Goblin Barbecue
            62289, -- Broiled Dragon Feast
            62290, -- Seafood Magnifique Feast
            62649, -- Fortune Cookie
            62651, -- Lightly Fried Lurker
            62652, -- Seasoned Crab
            62653, -- Salted Eye
            62654, -- Lavascale Fillet
            62655, -- Broiled Mountain Trout
            62656, -- Whitecrest Gumbo
            62657, -- Lurker Lunch
            62658, -- Tender Baked Turtle
            62659, -- Hearty Seafood Soup
            62660, -- Pickled Guppy
            62661, -- Baked Rockfish
            62662, -- Grilled Dragon
            62663, -- Lavascale Minestrone
            62664, -- Crocolisk Au Gratin
            62665, -- Basilisk Liverdog
            62666, -- Delicious Sagefish Tail
            62667, -- Mushroom Sauce Mudfish
            62668, -- Blackbelly Sushi
            62669, -- Skewered Eel
            62670, -- Beer-Basted Crocolisk
            62671, -- Severed Sagefish Head
            62675, -- Starfire Espresso
            62676, -- Blackened Surprise
            58256, -- Sparkling Oasis Water
            58258, -- Smoked String Cheese
            58260, -- Pine Nut Bread
            58262, -- Sliced Raw Billfish
            58264, -- Sour Green Apple
            58266, -- Violet Morel
            58268, -- Roasted Beef
            59029, -- Greasy Whale Milk
            59228, -- Vile Purple Fungus
            59230, -- Fungus Squeezings
            59232, -- Unidentifiable Meat Dish
            62672, -- South Island Iced Tea
            62677, -- Fish Fry
            62680, -- Chocolate Cookie
            68687, -- Scalding Murglesnout
            58274, -- Fresh Water
            58275, -- Hardtack
            58276, -- Gilnean White
            58277, -- Simmered Squid
            58278, -- Tropical Sunfruit
            58279, -- Tasty Puffball
            58280, -- Stewed Rabbit
            59227, -- Rock-Hard Biscuit
            59229, -- Murky Water
            59231, -- Oily Giblets
            69243, -- Ice Cream Cake Slice
            65517, -- Conjured Mana Lollipop
            75027, -- Teldrassil Tenderloin
            75028, -- Stormwind Surprise
            75029, -- Beer-Basted Short Ribs
            75030, -- Gnomeregan Gnuggets
            75031, -- Draenic Dumplings
            75032, -- Mulgore Meat Pie
            75035, -- Troll Tartare
            75036, -- Silvermoon Steak
            65516, -- Conjured Mana Cupcake
            67270, -- Ursius Flank
            67271, -- Hell-Hoot Barbecue
            67272, -- Shy-Rotam Steak
            67273, -- Chillwind Omelet
            65515, -- Conjured Mana Brownie
            63691, -- Brivelthwerp's Crunchy Ice Cream Bar
            69244, -- Ice Cream Bar
            61382, -- Garr's Limeade
            61986, -- Tol Barad Coconut Rum
            63023, -- Sweet Tea
            64639, -- Silversnap Ice
            65500, -- Conjured Mana Cookie
            61381, -- Yance's Special Burger Patty
            63692, -- Frozen "Cream" Custard
            57519, -- Cookie's Special Ramlette
            61383, -- Garr's Key Lime Pie
            61985, -- Banana Cocktail
            64641, -- "Delicious" Worm Steak
            63693, -- Ooey Gooey Gelato
            63694, -- Silithid-Free Sorbet
            46392, -- Venison Steak
            57518, -- Mr. Bubble's Shockingly Delicious Ice Cream
            60375, -- Cheery Cherry Pie
            60377, -- Lots 'o Meat Pie
            60378, -- Plumpkin Pie
            60379, -- Mud Pie
            61983, -- Imported E.K. Ale
            61984, -- Potent Pineapple Punch
            62908, -- Hair of the Dog
            65730, -- Stagwich
            65731, -- Yetichoke Hearts
            58933, -- Westfall Mud Pie
            60267, -- Country Pumpkin
            60268, -- Hobo Surprise
            60269, -- Well Water
            69027, -- Cone of Cold
            69233, -- Cone of Cold
            49365, -- Briaroot Brew
            49397, -- Half-Eaten Rat
            49600, -- Goblin Shortbread
            49601, -- Volcanic Spring Water
            49602, -- Earl Black Tea
            62909, -- "Bear" Steaks
            63530, -- Refreshing Pineapple Punch
            67230, -- Venison Jerky
            69920, -- Thrice-Spiced Crunchy Stew
            49253, -- Slightly Worm-Eaten Hardtack
            49254, -- Tarp Collected Dew
            57543, -- Stormhammer Stout
            57544, -- Leftover Boar Meat
            61384, -- Doublerum
            61982, -- Fizzy Fruit Wine
            62674, -- Highland Spirits
            62790, -- Darkbrew Lager
            63275, -- Gilnean Fortified Brandy
            63291, -- Blood Red Ale
            63292, -- Disgusting Rotgut
            63293, -- Blackheart Grog
            63296, -- Embalming Fluid
            63299, -- Sunkissed Wine
            75033, -- Green Ham & Eggs
            75034  -- Forsaken Foie Gras
        },
        [addon.CONS.C_ITEM_ENHANCEMENTS_ID] = {
            55057, -- Pyrium Weapon Chain
            56517, -- Heavy Savage Armor Kit
            55055, -- Elementium Shield Spike
            55056, -- Pyrium Shield Spike
            59594, -- Gnomish X-Ray Scope
            59595, -- R19 Threatfinder
            59596, -- Safety Catch Removal Kit
            62321, -- Lesser Inscription of Unbreakable Quartz
            62342, -- Lesser Inscription of Charged Lodestone
            62344, -- Lesser Inscription of Jagged Stone
            62347, -- Lesser Inscription of Shattered Crystal
            70139, -- Flintlocke's Woodchucker
            54449, -- Ghostly Spellthread
            56502, -- Scorched Leg Armor
            56503, -- Twilight Leg Armor
            52747, -- Enchant Weapon - Mending
            68784, -- Enchant Bracer - Agility
            68785, -- Enchant Bracer - Major Strength
            68786, -- Enchant Bracer - Mighty Intellect
            54447, -- Enchanted Spellthread
            54450, -- Powerful Ghostly Spellthread
            56550, -- Dragonscale Leg Armor
            56551, -- Charscale Leg Armor
            62333, -- Greater Inscription of Unbreakable Quartz
            62343, -- Greater Inscription of Charged Lodestone
            62345, -- Greater Inscription of Jagged Stone
            62346, -- Greater Inscription of Shattered Crystal
            68772, -- Greater Inscription of Vicious Intellect
            68773, -- Greater Inscription of Vicious Strength
            68774, -- Greater Inscription of Vicious Agility
            71720, -- Drakehide Leg Armor
            54448, -- Powerful Enchanted Spellthread
            68049, -- Heat-Treated Spinning Lure
            62673, -- Feathered Lure
            67404, -- Glass Fishing Bobber
            69907  -- Corpse Worm
        },
        [addon.CONS.C_POTIONS_ID] = {
            57099, -- Mysterious Potion
            57194, -- Potion of Concentration
            58090, -- Earthen Potion
            58091, -- Volcanic Potion
            58145, -- Potion of the Tol'vir
            58146, -- Golemblood Potion
            58488, -- Potion of Treasure Finding
            58489, -- Potion of Illusion
            63144, -- Baradin's Wardens Healing Potion
            63145, -- Baradin's Wardens Mana Potion
            64993, -- Hellscream's Reach Mana Potion
            64994, -- Hellscream's Reach Healing Potion
            57193, -- Mighty Rejuvenation Potion
            57191, -- Mythical Healing Potion
            57192, -- Mythical Mana Potion
            63300, -- Rogue's Draught
            67415, -- Draught of War
            54213  -- Molotov Cocktail
        },
        [addon.CONS.C_OTHER_ID] = {
            58142, -- Deathblood Venom
            63122, -- Lifegiving Seed
            63303, -- Scroll of Agility IX
            63304, -- Scroll of Strength IX
            63305, -- Scroll of Intellect IX
            63306, -- Scroll of Stamina IX
            63307, -- Scroll of Versatility IX
            63308, -- Scroll of Protection IX
            65460, -- Big Cauldron of Battle
            54442, -- Embersilk Net
            63351, -- Tahret Dynasty Mallet
            60816, -- Maziel's Research
            62238, -- Origami Rock
            62239, -- Origami Slime
            62288, -- Cauldron of Battle
            63246, -- Origami Beetle
            60680, -- S.A.F.E. "Parachute"
            46696, -- Panther Figurine
            74142, -- Darkmoon Firework
            60376, -- Very Berry Pie
            52079, -- A Scarlet Letter
            46692, -- Elune's Torch
            46702, -- Ancient Device Fragment
            46819, -- Black Mandrake
            48707, -- Gilnean Mastiff Collar
            49210, -- Goblin Male Mask
            49212, -- Goblin Female Mask
            49215, -- Worgen Female Mask
            49216, -- Worgen Male Mask
            49279, -- Grandma's Good Clothes
            49281, -- Chance the Cat
            49337, -- Shipwright's Tools
            49338, -- Planks of Wood
            49339, -- Coal Tar
            49649, -- Impaling Spine
            49743, -- Sten's First Aid Kit
            49749, -- Forgotten Dwarven Artifact
            49753, -- Coldridge Trail Bag
            49921, -- Unearthed Memento
            49944, -- Belysra's Talisman
            50017, -- Moonleaf
            50086, -- Mysterious Artifact
            50134, -- Horn of Tal'doren
            50218, -- Krennan's Potion of Stealth
            50220, -- Half-Burnt Torch
            50236, -- High Viscosity Coconut Milk
            50237, -- Un'Goro Coconut
            50238, -- Cracked Un'Goro Coconut
            50253, -- Un'Goro Lasher Seed
            50258, -- Tarblossom Blossom
            50334, -- Rapier of the Gilnean Patriots
            50371, -- Silithid Leg
            50405, -- Fossil-Finder 3000
            50407, -- Enormous Dinosaur Talon
            50408, -- Spark's Shovel
            50409, -- Spark's Fossil Finding Kit
            50410, -- Durrin's Archaeological Findings
            50430, -- Scraps of Rotting Meat
            50441, -- Garl's Net
            50443, -- Discarded Supplies
            50742, -- Tara's Tar Scraper
            50746, -- Tara's Tar Scraper
            50770, -- Ithis' Findings
            51546, -- A-Me 01's Thumb Drive
            51780, -- Small Rock
            51794, -- Maximillian's Armor
            51956, -- Blessed Offerings
            52050, -- Elreth's Hand Mirror
            52051, -- Trail-Worn Scroll
            52066, -- Xavren's Thorn
            52067, -- Marrowpetal
            52068, -- Briny Sea Cucumber
            52077, -- Urgent Scarlet Memorandum
            52270, -- Plagued Bruin Hide
            52361, -- Permission Slip
            52482, -- Agamand Memento
            52490, -- Stardust
            52505, -- Poison Extraction Totem
            52514, -- Thonk's Spyglass
            52558, -- Kul Tiras Treasure
            52564, -- Burning Blade Spellscroll
            52574, -- Mageweave Tether
            52580, -- Fizzle's Orb
            52685, -- Twilight Recruitment Papers
            52693, -- Forged Recruitment Papers
            52717, -- Fiery Leash
            52724, -- Twilight Communique
            52725, -- Hyjal Battleplans
            52819, -- Frostgale Crystal
            52828, -- Orb of Ascension
            52974, -- Mack's Deep Sea Grog
            53009, -- Juniper Berries
            53106, -- Amulet of Binding
            53107, -- Flameseer's Staff
            54461, -- Charred Staff Fragment
            54463, -- Flameseer's Staff
            54610, -- Spiked Basilisk Hide
            54788, -- Twilight Pick
            54814, -- Talisman of Flame Ascendancy
            54905, -- Tome of Flame
            54906, -- The Burning Litanies
            54907, -- Ascendant's Codex
            54959, -- Elemental Fire Shard
            54960, -- Elemental Earth Shard
            54961, -- Elemental Water Shard
            54962, -- Elemental Air Shard
            54973, -- Young Twilight Drake Skull
            55122, -- Tholo's Horn
            55137, -- Ogre Disguise
            55153, -- Horn of Cenarius
            55173, -- Young Twilight Drake Skull
            55178, -- Pure Twilight Egg
            55179, -- Drums of the Turtle God
            55238, -- Concentrated Solvent
            55826, -- Snickerfang Hyena Blood
            55827, -- Redstone Basilisk Blood
            55828, -- Ashmane Steak
            55829, -- Loramus' Head
            55836, -- Loramus' Torso
            55989, -- Charred Granite Chips
            55991, -- Vile Demonic Blood
            56012, -- Stone Knife of Sealing
            56017, -- Nethergarde Mine Report
            56019, -- Discordant Rune
            56178, -- Duarn's Rope
            56824, -- Terrapin Oil Sample
            56825, -- Remora Oil Sample
            56826, -- Hammerhead Oil Sample
            56909, -- Earthen Ring Unbinding Totem
            57190, -- Barricade
            58167, -- Spirit Totem
            58292, -- Faded Journal
            58365, -- Horn of the Ancients
            60364, -- Magmadar's Head
            60382, -- Mylra's Knife
            60574, -- The Upper World Pillar Fragment
            60749, -- Ruby Horn
            60760, -- Plague Lurker Sample
            60761, -- Venom Mist Lurker Sample
            60762, -- Hulking Plaguebear Sample
            60763, -- Diseased Wolf Sample
            60814, -- Twilight Research Notes
            60835, -- Depleted Totem
            61971, -- Stone of the Sun
            62533, -- Ancient Tol'vir Armaments
            62534, -- Horn of Ramkahen
            62542, -- Mech Control Scrambler
            62795, -- Silversnap Swim Tonic
            62821, -- Neferset Armor
            63027, -- Brazier Torch
            63256, -- Hardened Crocolisk Hide
            64352, -- Neferset Insignia
            64491, -- Royal Reward
            64642, -- Atulhet's Record Fragment
            64649, -- The Cypher of Keset
            65660, -- Grand Vizier Ertan's Heart
            65734, -- Twilight Documents
            67413, -- War of the Satyr
            69187, -- Murloc Female Mask
            69188, -- Murloc Male Mask
            69189, -- Naga Female Mask
            69190, -- Naga Male Mask
            69192, -- Ogre Female Mask
            69193, -- Ogre Male Mask
            69194, -- Vrykul Female Mask
            69195, -- Vrykul Male Mask
            69724, -- Temple Rat
            70725, -- Hallowed Hunter Wand - Squashling
            71627, -- Throwing Starfish
            74616, -- The Gatewatcher's Talisman
            77158, -- Darkmoon "Tiger"
            77957, -- Urgent Twilight Missive
            55053, -- Obsidium Skeleton Key
            48601, -- Red Rider Air Rifle Ammo
            63359, -- Banner of Cooperation
            64400, -- Banner of Cooperation
            52304, -- Fire Prism
            63141, -- Tol Barad Searchlight
            63269, -- Loaded Gnomish Dice
            63376, -- Hellscream's Reach Battle Standard
            63377, -- Baradin's Wardens Battle Standard
            64398, -- Standard of Unity
            64401, -- Standard of Unity
            64997, -- Tol Barad Searchlight
            69215, -- War Party Hitching Post
            69227, -- Fool's Gold
            69775, -- Vrykul Drinking Horn
            69776, -- Ancient Amber
            69777, -- Haunted War Drum
            71628, -- Sack of Starfish
            64399, -- Battle Standard of Coordination
            64402  -- Battle Standard of Coordination
        }
    },
    [addon.CONS.GEMS_ID] = {
        52129, -- Perfect Sensei's Jasper
        52130, -- Perfect Zen Jasper
        52131, -- Perfect Puissant Jasper
        52132, -- Perfect Lightning Jasper
        52133, -- Perfect Forceful Jasper
        52134, -- Perfect Steady Jasper
        52135, -- Perfect Piercing Jasper
        52136, -- Perfect Jagged Jasper
        52137, -- Perfect Nimble Jasper
        52138, -- Perfect Regal Jasper
        52139, -- Perfect Keen Hessonite
        52140, -- Perfect Artful Hessonite
        52141, -- Perfect Fine Hessonite
        52142, -- Perfect Adept Hessonite
        52143, -- Perfect Skillful Hessonite
        52144, -- Perfect Reckless Hessonite
        52145, -- Perfect Deft Hessonite
        52146, -- Perfect Fierce Hessonite
        52147, -- Perfect Potent Hessonite
        52148, -- Perfect Deadly Hessonite
        52149, -- Perfect Inscribed Hessonite
        52150, -- Perfect Resolute Hessonite
        52151, -- Perfect Polished Hessonite
        52152, -- Perfect Accurate Nightstone
        52153, -- Perfect Veiled Nightstone
        52154, -- Perfect Retaliating Nightstone
        52155, -- Perfect Glinting Nightstone
        52156, -- Perfect Etched Nightstone
        52157, -- Perfect Purified Nightstone
        52158, -- Perfect Guardian's Nightstone
        52159, -- Perfect Timeless Nightstone
        52160, -- Perfect Defender's Nightstone
        52161, -- Perfect Shifting Nightstone
        52162, -- Perfect Sovereign Nightstone
        52163, -- Perfect Fractured Alicite
        52164, -- Perfect Quick Alicite
        52165, -- Perfect Mystic Alicite
        52166, -- Perfect Smooth Alicite
        52167, -- Perfect Subtle Alicite
        52168, -- Perfect Rigid Zephyrite
        52169, -- Perfect Stormy Zephyrite
        52170, -- Perfect Sparkling Zephyrite
        52171, -- Perfect Solid Zephyrite
        52172, -- Perfect Precise Carnelian
        52173, -- Perfect Brilliant Carnelian
        52174, -- Perfect Flashing Carnelian
        52175, -- Perfect Delicate Carnelian
        52176, -- Perfect Bold Carnelian
        52081, -- Bold Carnelian
        52082, -- Delicate Carnelian
        52083, -- Flashing Carnelian
        52084, -- Brilliant Carnelian
        52085, -- Precise Carnelian
        52086, -- Solid Zephyrite
        52087, -- Sparkling Zephyrite
        52088, -- Stormy Zephyrite
        52089, -- Rigid Zephyrite
        52090, -- Subtle Alicite
        52091, -- Smooth Alicite
        52092, -- Mystic Alicite
        52093, -- Quick Alicite
        52094, -- Fractured Alicite
        52095, -- Sovereign Nightstone
        52096, -- Shifting Nightstone
        52097, -- Defender's Nightstone
        52098, -- Timeless Nightstone
        52099, -- Guardian's Nightstone
        52100, -- Purified Nightstone
        52101, -- Etched Nightstone
        52102, -- Glinting Nightstone
        52103, -- Retaliating Nightstone
        52104, -- Veiled Nightstone
        52105, -- Accurate Nightstone
        52106, -- Polished Hessonite
        52107, -- Resolute Hessonite
        52108, -- Inscribed Hessonite
        52109, -- Deadly Hessonite
        52110, -- Potent Hessonite
        52111, -- Fierce Hessonite
        52112, -- Deft Hessonite
        52113, -- Reckless Hessonite
        52114, -- Skillful Hessonite
        52115, -- Adept Hessonite
        52116, -- Fine Hessonite
        52117, -- Artful Hessonite
        52118, -- Keen Hessonite
        52119, -- Regal Jasper
        52120, -- Nimble Jasper
        52121, -- Jagged Jasper
        52122, -- Piercing Jasper
        52123, -- Steady Jasper
        52124, -- Forceful Jasper
        52125, -- Lightning Jasper
        52126, -- Puissant Jasper
        52127, -- Zen Jasper
        52128, -- Sensei's Jasper
        52203, -- Accurate Demonseye
        52204, -- Adept Ember Topaz
        52205, -- Artful Ember Topaz
        52206, -- Bold Inferno Ruby
        52207, -- Brilliant Inferno Ruby
        52208, -- Reckless Ember Topaz
        52209, -- Deadly Ember Topaz
        52210, -- Defender's Demonseye
        52211, -- Deft Ember Topaz
        52212, -- Delicate Inferno Ruby
        52213, -- Etched Demonseye
        52214, -- Fierce Ember Topaz
        52215, -- Fine Ember Topaz
        52216, -- Flashing Inferno Ruby
        52217, -- Veiled Demonseye
        52218, -- Forceful Dream Emerald
        52219, -- Fractured Amberjewel
        52220, -- Glinting Demonseye
        52221, -- Guardian's Demonseye
        52222, -- Inscribed Ember Topaz
        52223, -- Jagged Dream Emerald
        52224, -- Keen Ember Topaz
        52225, -- Lightning Dream Emerald
        52226, -- Mystic Amberjewel
        52227, -- Nimble Dream Emerald
        52228, -- Piercing Dream Emerald
        52229, -- Polished Ember Topaz
        52230, -- Precise Inferno Ruby
        52231, -- Puissant Dream Emerald
        52232, -- Quick Amberjewel
        52233, -- Regal Dream Emerald
        52234, -- Retaliating Demonseye
        52235, -- Rigid Ocean Sapphire
        52236, -- Purified Demonseye
        52237, -- Sensei's Dream Emerald
        52238, -- Shifting Demonseye
        52239, -- Potent Ember Topaz
        52240, -- Skillful Ember Topaz
        52241, -- Smooth Amberjewel
        52242, -- Solid Ocean Sapphire
        52243, -- Sovereign Demonseye
        52244, -- Sparkling Ocean Sapphire
        52245, -- Steady Dream Emerald
        52246, -- Stormy Ocean Sapphire
        52247, -- Subtle Amberjewel
        52248, -- Timeless Demonseye
        52249, -- Resolute Ember Topaz
        52250, -- Zen Dream Emerald
        52289, -- Fleet Shadowspirit Diamond
        52291, -- Chaotic Shadowspirit Diamond
        52292, -- Bracing Shadowspirit Diamond
        52293, -- Eternal Shadowspirit Diamond
        52294, -- Austere Shadowspirit Diamond
        52295, -- Effulgent Shadowspirit Diamond
        52296, -- Ember Shadowspirit Diamond
        52297, -- Revitalizing Shadowspirit Diamond
        52298, -- Destructive Shadowspirit Diamond
        52299, -- Powerful Shadowspirit Diamond
        52300, -- Enigmatic Shadowspirit Diamond
        52301, -- Impassive Shadowspirit Diamond
        52302, -- Forlorn Shadowspirit Diamond
        59477, -- Subtle Cogwheel
        59478, -- Smooth Cogwheel
        59479, -- Quick Cogwheel
        59480, -- Fractured Cogwheel
        59489, -- Precise Cogwheel
        59491, -- Flashing Cogwheel
        59493, -- Rigid Cogwheel
        59496, -- Sparkling Cogwheel
        68356, -- Willful Ember Topaz
        68357, -- Lucent Ember Topaz
        68358, -- Resplendent Ember Topaz
        68660, -- Mystic Cogwheel
        68741, -- Vivid Dream Emerald
        68778, -- Agile Shadowspirit Diamond
        68779, -- Reverberating Shadowspirit Diamond
        68780, -- Burning Shadowspirit Diamond
        52255, -- Bold Chimera's Eye
        52257, -- Brilliant Chimera's Eye
        52258, -- Delicate Chimera's Eye
        52259, -- Flashing Chimera's Eye
        52260, -- Precise Chimera's Eye
        52261, -- Solid Chimera's Eye
        52262, -- Sparkling Chimera's Eye
        52263, -- Stormy Chimera's Eye
        52264, -- Rigid Chimera's Eye
        52265, -- Subtle Chimera's Eye
        52266, -- Smooth Chimera's Eye
        52267, -- Mystic Chimera's Eye
        52268, -- Quick Chimera's Eye
        52269, -- Fractured Chimera's Eye
        71817, -- Rigid Deepholm Iolite
        71818, -- Stormy Deepholm Iolite
        71819, -- Sparkling Deepholm Iolite
        71820, -- Solid Deepholm Iolite
        71822, -- Misty Elven Peridot
        71823, -- Piercing Elven Peridot
        71824, -- Lightning Elven Peridot
        71825, -- Sensei's Elven Peridot
        71826, -- Infused Elven Peridot
        71827, -- Zen Elven Peridot
        71828, -- Balanced Elven Peridot
        71829, -- Vivid Elven Peridot
        71830, -- Turbid Elven Peridot
        71831, -- Radiant Elven Peridot
        71832, -- Shattered Elven Peridot
        71833, -- Energized Elven Peridot
        71834, -- Jagged Elven Peridot
        71835, -- Regal Elven Peridot
        71836, -- Forceful Elven Peridot
        71837, -- Nimble Elven Peridot
        71838, -- Puissant Elven Peridot
        71839, -- Steady Elven Peridot
        71840, -- Deadly Lava Coral
        71841, -- Crafty Lava Coral
        71842, -- Potent Lava Coral
        71843, -- Inscribed Lava Coral
        71844, -- Polished Lava Coral
        71845, -- Resolute Lava Coral
        71846, -- Stalwart Lava Coral
        71847, -- Champion's Lava Coral
        71848, -- Deft Lava Coral
        71849, -- Wicked Lava Coral
        71850, -- Reckless Lava Coral
        71851, -- Fierce Lava Coral
        71852, -- Adept Lava Coral
        71853, -- Keen Lava Coral
        71854, -- Artful Lava Coral
        71855, -- Fine Lava Coral
        71856, -- Skillful Lava Coral
        71857, -- Lucent Lava Coral
        71858, -- Tenuous Lava Coral
        71859, -- Willful Lava Coral
        71860, -- Splendid Lava Coral
        71861, -- Resplendent Lava Coral
        71862, -- Glinting Shadow Spinel
        71863, -- Accurate Shadow Spinel
        71864, -- Veiled Shadow Spinel
        71865, -- Retaliating Shadow Spinel
        71866, -- Etched Shadow Spinel
        71867, -- Mysterious Shadow Spinel
        71868, -- Purified Shadow Spinel
        71869, -- Shifting Shadow Spinel
        71870, -- Guardian's Shadow Spinel
        71871, -- Timeless Shadow Spinel
        71872, -- Defender's Shadow Spinel
        71873, -- Sovereign Shadow Spinel
        71874, -- Smooth Lightstone
        71875, -- Subtle Lightstone
        71876, -- Quick Lightstone
        71877, -- Fractured Lightstone
        71878, -- Mystic Lightstone
        71879, -- Delicate Queen's Garnet
        71880, -- Precise Queen's Garnet
        71881, -- Brilliant Queen's Garnet
        71882, -- Flashing Queen's Garnet
        71883, -- Bold Queen's Garnet
        77130, -- Balanced Elven Peridot
        77131, -- Infused Elven Peridot
        77132, -- Lucent Lava Coral
        77133, -- Mysterious Shadow Spinel
        77134, -- Mystic Lightstone
        77136, -- Resplendent Lava Coral
        77137, -- Shattered Elven Peridot
        77138, -- Splendid Lava Coral
        77139, -- Steady Elven Peridot
        77140, -- Stormy Deepholm Iolite
        77141, -- Tenuous Lava Coral
        77142, -- Turbid Elven Peridot
        77143, -- Vivid Elven Peridot
        77144, -- Willful Lava Coral
        77154  -- Radiant Elven Peridot
    },
    [addon.CONS.GLYPHS_ID] = {
        77101  -- Glyph of Shadow
    },
    [addon.CONS.KEYS_ID] = {
        64663  -- Bile-Etched Brass Key
    },
    [addon.CONS.MISCELLANEOUS_ID] = {
        [addon.CONS.M_ARMOR_TOKENS_ID] = {
            78847, -- Chest of the Corrupted Conqueror
            78848, -- Chest of the Corrupted Protector
            78849, -- Chest of the Corrupted Vanquisher
            78850, -- Crown of the Corrupted Conqueror
            78851, -- Crown of the Corrupted Protector
            78852, -- Crown of the Corrupted Vanquisher
            78853, -- Gauntlets of the Corrupted Conqueror
            78854, -- Gauntlets of the Corrupted Protector
            78855, -- Gauntlets of the Corrupted Vanquisher
            78856, -- Leggings of the Corrupted Conqueror
            78857, -- Leggings of the Corrupted Protector
            78858, -- Leggings of the Corrupted Vanquisher
            78859, -- Shoulders of the Corrupted Conqueror
            78860, -- Shoulders of the Corrupted Protector
            78861, -- Shoulders of the Corrupted Vanquisher
            78170, -- Shoulders of the Corrupted Vanquisher
            78171, -- Leggings of the Corrupted Vanquisher
            78172, -- Crown of the Corrupted Vanquisher
            78173, -- Gauntlets of the Corrupted Vanquisher
            78174, -- Chest of the Corrupted Vanquisher
            78175, -- Shoulders of the Corrupted Protector
            78176, -- Leggings of the Corrupted Protector
            78177, -- Crown of the Corrupted Protector
            78178, -- Gauntlets of the Corrupted Protector
            78179, -- Chest of the Corrupted Protector
            78180, -- Shoulders of the Corrupted Conqueror
            78181, -- Leggings of the Corrupted Conqueror
            78182, -- Crown of the Corrupted Conqueror
            78183, -- Gauntlets of the Corrupted Conqueror
            78184, -- Chest of the Corrupted Conqueror
            71669, -- Gauntlets of the Fiery Vanquisher
            71670, -- Crown of the Fiery Vanquisher
            71671, -- Leggings of the Fiery Vanquisher
            71672, -- Chest of the Fiery Vanquisher
            71673, -- Shoulders of the Fiery Vanquisher
            71676, -- Gauntlets of the Fiery Conqueror
            71677, -- Crown of the Fiery Conqueror
            71678, -- Leggings of the Fiery Conqueror
            71679, -- Chest of the Fiery Conqueror
            71680, -- Shoulders of the Fiery Conqueror
            71683, -- Gauntlets of the Fiery Protector
            71684, -- Crown of the Fiery Protector
            71685, -- Leggings of the Fiery Protector
            71686, -- Chest of the Fiery Protector
            71687, -- Shoulders of the Fiery Protector
            78862, -- Chest of the Corrupted Vanquisher
            78863, -- Chest of the Corrupted Conqueror
            78864, -- Chest of the Corrupted Protector
            78865, -- Gauntlets of the Corrupted Vanquisher
            78866, -- Gauntlets of the Corrupted Conqueror
            78867, -- Gauntlets of the Corrupted Protector
            78868, -- Crown of the Corrupted Vanquisher
            78869, -- Crown of the Corrupted Conqueror
            78870, -- Crown of the Corrupted Protector
            78871, -- Leggings of the Corrupted Vanquisher
            78872, -- Leggings of the Corrupted Conqueror
            78873, -- Leggings of the Corrupted Protector
            78874, -- Shoulders of the Corrupted Vanquisher
            78875, -- Shoulders of the Corrupted Conqueror
            78876, -- Shoulders of the Corrupted Protector
            71668, -- Helm of the Fiery Vanquisher
            71674, -- Mantle of the Fiery Vanquisher
            71675, -- Helm of the Fiery Conqueror
            71681, -- Mantle of the Fiery Conqueror
            71682, -- Helm of the Fiery Protector
            71688, -- Mantle of the Fiery Protector
            63682, -- Helm of the Forlorn Vanquisher
            63683, -- Helm of the Forlorn Conqueror
            63684, -- Helm of the Forlorn Protector
            64314, -- Mantle of the Forlorn Vanquisher
            64315, -- Mantle of the Forlorn Conqueror
            64316, -- Mantle of the Forlorn Protector
            65000, -- Crown of the Forlorn Protector
            65001, -- Crown of the Forlorn Conqueror
            65002, -- Crown of the Forlorn Vanquisher
            65087, -- Shoulders of the Forlorn Protector
            65088, -- Shoulders of the Forlorn Conqueror
            65089, -- Shoulders of the Forlorn Vanquisher
            66998, -- Essence of the Forlorn
            67423, -- Chest of the Forlorn Conqueror
            67424, -- Chest of the Forlorn Protector
            67425, -- Chest of the Forlorn Vanquisher
            67426, -- Leggings of the Forlorn Vanquisher
            67427, -- Leggings of the Forlorn Protector
            67428, -- Leggings of the Forlorn Conqueror
            67429, -- Gauntlets of the Forlorn Conqueror
            67430, -- Gauntlets of the Forlorn Protector
            67431  -- Gauntlets of the Forlorn Vanquisher
        },
        [addon.CONS.M_HOLIDAY_ID] = {
            70931, -- Scrooge's Payoff
            54516, -- Loot-Filled Pumpkin
            73792, -- Stolen Present
            46709, -- MiniZep Controller
            70722  -- Little Wickerman
        },
        [addon.CONS.M_HOLIDAY_ID] = {
            [addon.CONS.MH_DARKMOON_ID] = {
                71083, -- Darkmoon Game Token
                71634  -- Darkmoon Adventurer's Guide
            }
        },
        [addon.CONS.M_REAGENTS_ID] = {
            64670  -- Vanishing Powder
        },
        [addon.CONS.M_MOUNTS_ID] = {
            54465, -- Vashj'ir Seahorse
            62461, -- Goblin Trike Key
            73838, -- Mountain Horse
            65891, -- Vial of the Sands
            69747, -- Amani Battle Bear
            77067, -- Reins of the Blazing Drake
            77068, -- Reins of the Twilight Harbinger
            77069, -- Life-Binder's Handmaiden
            78919, -- Experiment 12-B
            67151, -- Reins of Poseidus
            62900, -- Reins of the Volcanic Stone Drake
            62901, -- Reins of the Drake of the East Wind
            63039, -- Reins of the Drake of the West Wind
            63040, -- Reins of the Drake of the North Wind
            63041, -- Reins of the Drake of the South Wind
            63042, -- Reins of the Phosphorescent Stone Drake
            63043, -- Reins of the Vitreous Stone Drake
            63125, -- Reins of the Dark Phoenix
            64998, -- Reins of the Spectral Steed
            64999, -- Reins of the Spectral Wolf
            65356, -- Reins of the Drake of the West Wind
            69213, -- Flameward Hippogryph
            69224, -- Smoldering Egg of Millagazor
            69226, -- Felfire Hawk
            69230, -- Corrupted Egg of Millagazor
            68008, -- Mottled Drake
            68825, -- Amani Dragonhawk
            72582, -- Corrupted Hippogryph
            74269, -- Blazing Hippogryph
            79771, -- Feldrake
            60954, -- Fossilized Raptor
            62462, -- Goblin Turbo-Trike Key
            63044, -- Reins of the Brown Riding Camel
            63045, -- Reins of the Tan Riding Camel
            63046, -- Reins of the Grey Riding Camel
            64883, -- Scepter of Azj'Aqir
            68823, -- Armored Razzashi Raptor
            68824, -- Swift Zulian Panther
            70909, -- Reins of the Vicious War Steed
            70910, -- Horn of the Vicious War Wolf
            71665, -- Flametalon of Alysrazor
            73839, -- Swift Mountain Horse
            62298, -- Reins of the Golden King
            67107, -- Reins of the Kor'kron Annihilator
            69228, -- Savage Raptor
            69846, -- Winged Guardian
            71718, -- Swift Shorestrider
            72140, -- Swift Forest Strider
            72145, -- Swift Springstrider
            72146, -- Swift Lovebird
            72575, -- White Riding Camel
            73766, -- Darkmoon Dancing Bear
            78924  -- Heart of the Aspects
        },
        [addon.CONS.M_COMPANIONS_ID] = {
            46325, -- Withers
            60869, -- Pebble
            64494, -- Tiny Shale Spider
            65661, -- Blue Mini Jouster
            65662, -- Gold Mini Jouster
            66067, -- Brazie's Sunflower Seeds
            66073, -- Snail Shell
            66076, -- Mr. Grubbs
            66080, -- Tiny Flamefly
            68833, -- Panther Cub
            69239, -- Winterspring Cub
            69251, -- Lashtail Hatchling
            70908, -- Feline Familiar
            71033, -- Lil' Tarecgosa
            71076, -- Creepy Crate
            72042, -- Alliance Balloon
            72045, -- Horde Balloon
            75040, -- Flimsy Darkmoon Balloon
            75041, -- Flimsy Green Balloon
            22780, -- White Murloc Egg
            46892, -- Murkimus' Tiny Spear
            54810, -- Celestial Dragon
            59597, -- Personal World Destroyer
            60216, -- De-Weaponized Mechanical Companion
            60955, -- Fossilized Hatchling
            62540, -- Lil' Deathwing
            63138, -- Dark Phoenix Hatchling
            63355, -- Rustberg Gull
            63398, -- Armadillo Pup
            64372, -- Clockwork Gnome
            64403, -- Fox Kit
            64996, -- Rustberg Gull
            65361, -- Guild Page
            65362, -- Guild Page
            65363, -- Guild Herald
            65364, -- Guild Herald
            67128, -- Landro's Lil' XT
            67274, -- Enchanted Lantern
            67275, -- Magic Lamp
            67282, -- Elementium Geode
            67418, -- Smoldering Murloc Egg
            68384, -- Moonkin Egg
            68385, -- Lil' Ragnaros
            68618, -- Moonkin Hatchling
            68619, -- Moonkin Hatchling
            68673, -- Smolderweb Egg
            68840, -- Landro's Lichling
            68841, -- Nightsaber Cub
            69648, -- Legs
            69821, -- Pterrordax Hatchling
            69824, -- Voodoo Figurine
            69847, -- Guardian Cub
            69991, -- Tiny Sporebat
            69992, -- Shimmering Wyrmling
            70099, -- Cenarion Hatchling
            70140, -- Hyjal Bear Cub
            70160, -- Crimson Lasher
            71137, -- Brewfest Keg Pony
            71140, -- Nuts' Acorn
            71387, -- Brilliant Kaliri
            71624, -- Purple Puffer
            71726, -- Murky's Little Soulstone
            72068, -- Guardian Cub
            72134, -- Grell Moss
            72153, -- Sand Scarab
            73762, -- Darkmoon Balloon
            73764, -- Darkmoon Monkey
            73765, -- Darkmoon Turtle
            73797, -- Lump of Coal
            73903, -- Darkmoon Tonk
            73905, -- Darkmoon Zeppelin
            73953, -- Sea Pony
            74610, -- Lunar Lantern
            74611, -- Festival Lantern
            74981, -- Darkmoon Cub
            75042, -- Flimsy Yellow Balloon
            76062, -- Fetish Shaman's Spear
            78916, -- Soul of the Aspects
            79744, -- Eye of the Legion
            60847  -- Crawling Claw
        },
        [addon.CONS.M_COMPANIONS_ID] = {
            [addon.CONS.MC_CONSUMABLE_ID] = {
                71153  -- Magical Pet Biscuit
            }
        },
        [addon.CONS.M_TELEPORT_ID] = {
            [addon.CONS.MT_HEARTSTONE_ID] = {
                64457  -- The Last Relic of Argus
            },
            [addon.CONS.MT_ARMOR_ID] = {
                63379, -- Baradin's Wardens Tabard
                63378, -- Hellscream's Reach Tabard
                65274, -- Cloak of Coordination: Orgrimmar
                65360, -- Cloak of Coordination: Stormwind
                63353, -- Shroud of Cooperation: Orgrimmar
                63352, -- Shroud of Cooperation: Stormwind
                63207, -- Wrap of Unity: Orgrimmar
                63206  -- Wrap of Unity: Stormwind
            },
            [addon.CONS.MT_QUEST_ID] = {
                68809, -- Veteran's Hearthstone
                61379, -- Gidwin's Hearthstone
                68808  -- Hero's Hearthstone
            },
            [addon.CONS.MT_SCROLLS_ID] = {
                58487  -- Potion of Deepholm
            },
            [addon.CONS.MT_TOYS_ID] = {
                64488  -- Innkeeper's Daughter
            }
        },
        [addon.CONS.M_ARCHAEOLOGY_ID] = {
            [addon.CONS.MA_KEY_STONES_ID] = {
                64397, -- Tol'vir Hieroglyphic
                52843, -- Dwarf Rune Stone
                63128, -- Troll Tablet
                63127, -- Highborne Scroll
                64394, -- Draenei Tome
                64392, -- Orc Blood Text
                64395, -- Vrykul Rune Stick
                64396  -- Nerubian Obelisk
            }
        },
        [addon.CONS.M_OTHER_ID] = {
            67597, -- Sealed Crate
            78930, -- Sealed Crate
            66943, -- Treasures from Grim Batol
            67539, -- Tiny Treasure Chest
            67495, -- Strange Bloated Stomach
            63349, -- Flame-Scarred Junkbox
            69838, -- Chirping Box
            53057, -- Faded Wizard Hat
            67414, -- Bag of Shiny Things
            31167, -- Cracked Gateway Control Stone
            45040, -- Shatterspear Torturer's Cage Key
            49884, -- Kaja'Cola
            52274, -- Earthen Ring Supplies
            52344, -- Earthen Ring Supplies
            57540, -- Coldridge Mountaineer's Pouch
            62062, -- Bulging Sack of Gold
            64466, -- Tiny Totem of Mau'ari
            64657, -- Canopic Jar
            68598, -- Very Fat Sack of Coins
            68795, -- Stendel's Bane
            69986, -- Cub's First Toy
            70994, -- Pyreshell Fragment
            70997, -- Rhyolite Fragment
            70999, -- Obsidian-Flecked Chitin Fragment
            71000, -- Emberstone Fragment
            71950, -- Unbalanced Throwing Ring
            78897, -- Pouch o' Tokens
            78898, -- Sack o' Tokens
            78899, -- Pouch o' Tokens
            78900, -- Pouch o' Tokens
            78901, -- Pouch o' Tokens
            78902, -- Pouch o' Tokens
            78903, -- Pouch o' Tokens
            78904, -- Pouch o' Tokens
            78905, -- Sack o' Tokens
            78906, -- Sack o' Tokens
            78907, -- Sack o' Tokens
            78908, -- Sack o' Tokens
            78909, -- Sack o' Tokens
            78910, -- Sack o' Tokens
            68729, -- Elementium Lockbox
            71631, -- Zen'Vorka's Cache
            63518, -- Hellscream's Reach Commendation
            69886, -- Bag of Coins
            67250, -- Satchel of Helpful Goods
            68813, -- Satchel of Freshly-Picked Herbs
            71259, -- Leyara's Locket
            67248, -- Satchel of Helpful Goods
            64358, -- Highborne Soul Mirror
            64361, -- Druid and Priest Statue Set
            64373, -- Chalice of the Mountain Kings
            64383, -- Kaldorei Wind Chimes
            64456, -- Arrival of the Naaru
            66888, -- Stave of Fur and Claw
            67097, -- Grim Campfire
            68806, -- Kalytha's Haunted Locket
            69817, -- Hive Queen's Honeycomb
            69818, -- Giant Sack
            69822, -- Master Chef's Groceries
            69823, -- Gub's Catch
            69895, -- Green Balloon
            69896, -- Yellow Balloon
            70161, -- Mushroom Chair
            70719, -- Water-Filled Gills
            72159, -- Magical Ogre Idol
            72161, -- Spurious Sarcophagus
            76401, -- Scarab Coffer Key
            76402, -- Greater Scarab Coffer Key
            79769, -- Demon Hunter's Aspect
            71617, -- Crystallized Firestone
            78890, -- Crystalline Geode
            78891, -- Elementium-Coated Geode
            62286, -- Guild Vault Voucher (7th Slot)
            62287, -- Guild Vault Voucher (8th Slot)
            64481, -- Blessing of the Old God
            64482, -- Puzzle Box of Yogg-Saron
            64646, -- Bones of Transformation
            64651, -- Wisp Amulet
            64881, -- Pendant of the Scarab Storm
            68136, -- Guild Vault Voucher (8th Slot)
            -- JUNK
            69903  -- Satchel of Exotic Mysteries
        }
    },
    [addon.CONS.QUEST_ID] = {
        69854, -- Smoke-Stained Locket
        69855, -- Smoke-Stained Locket
        61322, -- Piece of Rope
        62768, -- Efficient Excavations
        45863, -- Decayed Treasure Map
        53053, -- Tattered Treasure Map
        54345, -- Crumpled Treasure Map
        54639, -- Waterlogged Journal
        55186, -- Lady La-La's Necklace
        62138, -- Gnash's Head
        49088, -- Slitherblade Trident
        49092, -- Slitherblade Staff
        52708, -- Charred Basilisk Meat
        62020, -- Itharius' Scroll
        62022, -- Scroll of Hakkar
        64353, -- Empty Firewater Flask
        61387, -- Hidden Stash
        54845, -- Succulent Crab Meat
        54846, -- Captain Taylor's Flare Gun
        55183, -- Coil of Rope
        55184, -- Rusty Harpoon Gun
        55185, -- Pilfered Cannonball
        55187, -- Lady La-La's Medallion
        55212, -- Gnaws' Tooth
        55214, -- Rusty Harpoon
        55215, -- Grimy Grub Guts
        55244, -- Gold Necklace
        55247, -- Gnaws Tooth Necklace
        55805, -- The Pewter Pounder
        55806, -- The Pewter Pounder
        56249, -- Nazgrim's Flare Gun
        52831, -- Gordunni Scroll
        38567, -- Maraudine Prisoner Manifest
        49200, -- Infernal Power Core
        55166, -- Pristine Yeti Hide
        55167, -- Perfect Yeti Hide
        49776, -- Roadway Plans
        49201, -- Dingy Wizard Hat
        49700, -- SFG
        63090, -- Muckgill's Flipper
        60681, -- Cannary's Cache
        60956, -- Korok's Second Head
        57935, -- Harvest Watcher Heart
        47039, -- Scout's Orders
        52286, -- Taming Rod
        52287, -- Taming Rod
        52288, -- Taming Rod
        44830, -- Highborne Relic
        44850, -- Bear's Paw
        44863, -- Corrupted Tide Crawler Flesh
        44864, -- Encrusted Clam Muscle
        44868, -- Frenzied Cyclone Bracers
        44887, -- Blessed Herb Bundle
        44888, -- Bear's Paw Bundle
        44889, -- Blessed Herb Bundle
        44911, -- Foul Bear Carcass Sample
        44913, -- Corrupted Thistle Bear Guts
        44925, -- Corruptor's Master Key
        44927, -- Corruptor's Master Key
        44929, -- Aetherion's Essence
        44942, -- Shatterspear Amulet
        44959, -- Soothing Totem
        44960, -- Slain Wildkin Feather
        44966, -- Foul Ichor
        44968, -- Twilight Plans
        44969, -- Moonstalker Whisker
        44975, -- Orb of Elune
        44976, -- Fuming Toadstool
        44979, -- Overseer's Orders
        44985, -- Shattershield Arrow
        44995, -- Dryad Spear
        44999, -- Sentinel Torch
        45001, -- Medicated Salve
        45004, -- Serviceable Arrow
        45012, -- Vial of Gorat's Blood
        45023, -- Gorat's Imbued Blood
        45025, -- Bag of Bathran's Hair
        45027, -- Tuft of Mottled Doe Hair
        45042, -- Feero's Holy Hammer
        45043, -- The Purifier's Prayer Book
        45051, -- Kadrak's Reins
        45065, -- Unbathed Concoction
        45066, -- Bathed Concoction
        45069, -- Freshly Cut Wood
        45071, -- Natural Oil
        45079, -- Gorick's Stash List
        45476, -- Seeker's Fel Spear
        45477, -- Gorgannon's Flaming Blade
        45478, -- Reinforced Canister
        45545, -- Gnarl's Bough
        45546, -- Hastily Written Note
        45573, -- The Forest Heart
        45576, -- Chunk of Ore
        45598, -- Accursed Ore
        45600, -- Dr. Finger's Keys
        45662, -- Slobberfang Murloc Brain Stem
        45678, -- Blood-Soaked Axe
        45681, -- Filled Slobberfang Trap
        45683, -- Tainted Blood of the Kaldorei
        45684, -- Empty Slobberfang Trap
        45710, -- Secret Signal Powder
        45807, -- Splintertree Axe
        45885, -- Thistle Bear Fur
        45898, -- Apothecary Furrows' Notes
        45911, -- Petrified Root
        45944, -- Narassin's Tome
        46091, -- Splintertree Post Report
        46094, -- Bucket of Water
        46128, -- Troll Charm
        46147, -- Kaldorei Assassin's Head
        46310, -- Troll Charm
        46311, -- Sheelah's Sealed Missive
        46314, -- Bloodcup Braid
        46315, -- Thorned Bloodcup
        46316, -- Orc-Hair Braid
        46317, -- Draaka's Confession
        46318, -- Hellscream's Missive
        46352, -- Gift of the Earth
        46354, -- Seed of the Earth
        46355, -- Seed of the Sky
        46356, -- Seed of the Sea
        46357, -- Sunken Scrap Metal
        46363, -- Lifebringer Sapling
        46365, -- Mystlash Hydra Blubber
        46366, -- Mystlash Hydra Oil
        46370, -- Lifebringer Sapling
        46384, -- Salvageable Greymist Wreckage
        46385, -- Marvelous Mobile Murloc Manor Maker
        46386, -- Mud-Crusted Ancient Disc
        46387, -- Ancient Slotted Device
        46388, -- Buried Artifact Detector
        46389, -- Elune's Tear
        46543, -- Laughing Sister's Corpse
        46546, -- Energized Soothing Totem
        46694, -- Dellylah's Stolen Beans
        46695, -- Emerald Scroll
        46697, -- Engorged Venom Sac
        46698, -- Moon-Kissed Clay
        46699, -- Dellylah's Stolen Water
        46700, -- Melithar's Stolen Bags
        46701, -- Tweedle's Improvised Explosive
        46714, -- Halga's "Street Sweeper"
        46715, -- Sanctified Moonstones
        46716, -- Ireroot Seeds
        46720, -- Shael'dryn's Playing Possum Technique
        46721, -- Dartol's Rod
        46722, -- Grol'dom Net
        46734, -- Battle Triage Kit
        46739, -- Dartol's Rod
        46741, -- Furbolg Ear
        46742, -- Stolen Grain
        46754, -- Tweedle's Tiny Package
        46768, -- Sploder's Head
        46769, -- Tweedle's Pow-Pow Powder
        46770, -- Spiked Wolf Collar
        46771, -- Locking Bolt
        46772, -- Bronze Cog
        46773, -- Copper Plating
        46774, -- Heart of Kaliva
        46776, -- Jinx's Goggles
        46777, -- Bloodtooth Banner
        46781, -- Dartol's Rod
        46782, -- Tonga's Totem
        46789, -- Fungal Cultures
        46794, -- Helbrim's Research
        46795, -- Spiked Wolf Collars
        46798, -- Spiked Wolf Collars
        46801, -- Re-Engineered Samophlange Wreckage
        46827, -- Ship Schematics
        46828, -- Crate of Tools
        46829, -- Limpet Mine
        46832, -- Tattooed Pirate Head
        46833, -- Gazlowe's Treasure Chest
        46834, -- Glomp's Booty
        46836, -- Heavy Dwarven Rifle
        46838, -- Pirate Signal Horn
        46850, -- King Reaperclaw's Horn
        46851, -- Wittle Waptor
        46853, -- Waptor Twap
        46855, -- Bruno's Debt
        46856, -- Keys to the Hot Rod
        46857, -- Centaur Intelligence
        46858, -- Personal Riches
        46867, -- Frankie's Debt
        46868, -- Jack's Debt
        46869, -- Sudsy's Debt
        46896, -- Salvaged Supplies
        47038, -- Slab of Venison
        47040, -- Scout's Orders
        47044, -- Stack of Macaroons
        47045, -- Shiny Bling
        47046, -- Hip New Outfit
        47047, -- Cool Shades
        47050, -- Azshara Lumber
        47530, -- Stolen Loot
        47819, -- Crystal Pendant
        47833, -- Furien's Journal
        48104, -- The Refleshifier
        48110, -- Vortex Gem
        48128, -- Mountainfoot Iron
        48249, -- Raging Vortex Gem
        48525, -- Recovered Artifacts
        48665, -- Surveyor's Beacon
        48766, -- Kaja'mite Chunk
        48768, -- Kablooey Bombs
        48780, -- Ectosplatter Sample
        48835, -- Kalytha's Ring
        48921, -- Sarcen Stone
        48937, -- Maldy's Falcon
        48939, -- The Goblin Lisa
        48941, -- The Ultimate Bomb
        48951, -- Ancient Summoning Ritual
        49008, -- Burning Blade Ear
        49012, -- Abjurer's Manual
        49014, -- Swoop Eggs
        49026, -- Scorpion Stinger
        49028, -- Nitro-Potassium Bananas
        49030, -- Aged Basilisk Tail
        49032, -- Dread Swoop Feather
        49034, -- Element 116
        49036, -- Animate Basalt
        49038, -- Arcane Charge
        49042, -- Artillery Signal
        49056, -- Slitherblade Bones
        49058, -- Slitherblade Scale
        49060, -- Slitherblade Fin
        49062, -- Goblin Mortar Shell
        49064, -- Slitherblade Charm
        49068, -- Pristine Thunderhead Feather
        49082, -- Living Ire Thyme
        49090, -- Field Journal
        49094, -- Keystone Shard
        49102, -- Ancient Tablet Fragment
        49104, -- Ancient Engravings of Neptulon
        49108, -- Weed Whacker
        49132, -- Fireliminator X-21
        49134, -- Korrah's Report
        49136, -- Blood-Filled Leech
        49138, -- Bottle of Leeches
        49140, -- Aloe Thistle
        49142, -- Infrared Heat Focals
        49144, -- Soothing Broth
        49150, -- Cenarion Seeds
        49162, -- Kawphi Bean
        49164, -- Cenarion Supply Crate
        49166, -- Spear of the Kolkar Khan
        49168, -- Enchanted Azshari Sea Sponge
        49170, -- Basilisk Flank
        49172, -- Simmering Water Droplet
        49174, -- Globe of Boiling Water
        49176, -- Engorged Azshari Sea Sponge
        49195, -- Hollow Kodo Horn
        49196, -- Smeed's Harnesses
        49197, -- Maurin's Concoction
        49199, -- Infernal Power Core
        49202, -- Black Gunpowder Keg
        49204, -- Secret Rocket Plans
        49207, -- Azsharite Sample
        49208, -- Mutilated Mistwing Carcass
        49211, -- Mound o' Meat
        49221, -- Brendol's Satchel
        49222, -- Tender Lobstrock Tail
        49226, -- Mysterious Concoction
        49230, -- Giant-Sized Laxative
        49240, -- Mastiff Whistle
        49280, -- Linen-Wrapped Book
        49350, -- Blessed Flaregun
        49356, -- Amberwind's Journal
        49359, -- Attuned Runestone
        49364, -- Blackmaw Intelligence
        49366, -- Ambassador's Robes
        49367, -- Blackmaw Meeting Agenda
        49368, -- Ambassador Disguise
        49377, -- Garrosh's Autograph
        49424, -- Gyrochoppa Keys
        49459, -- Flat Widget
        49460, -- Progressive Widget
        49533, -- Ironwrought Key
        49535, -- Stolen Rifle
        49539, -- Adana's Torch
        49540, -- Grunwald's Head
        49596, -- Cryomatic 16
        49599, -- Military Supplies
        49611, -- Infrared Heat Focals
        49629, -- Gob Squad Flare
        49639, -- Highborne Tablet
        49642, -- Heart of Arkkoroc
        49647, -- Drum of the Soothed Earth
        49651, -- Water of Vision
        49652, -- Water of Vision
        49674, -- The Head of Jarrodenus
        49679, -- Sanctified Flaregun
        49680, -- Borrowed Tabard
        49685, -- Flag of Territorial Claim
        49699, -- Crude Key
        49701, -- Stealth Field Generator
        49705, -- Armed Azsharite Core
        49707, -- Trogg Item
        49742, -- Koroth's Banner
        49744, -- Cask of Stormhammer Stout
        49745, -- Cask of Theramore Pale Ale
        49746, -- Cask of Gnomenbrau
        49747, -- Boar Haunch
        49748, -- Ragged Wolf Hide
        49751, -- Priceless Rockjaw Artifact
        49752, -- Replacement Parts
        49754, -- Coldridge Beer Flagon
        49755, -- Ragged Wolf-Hide Cloak
        49756, -- Leftover Boar Meat
        49758, -- Empowered Rune
        49759, -- Arcane Rune
        49760, -- Old Journal Page
        49769, -- Confiscated Arms
        49771, -- Captain Peake's Eyeballs
        49772, -- Terrortooth Hide
        49782, -- Horn of Challenge
        49815, -- Unusual Flower Bud
        49850, -- Theramore Merit Badge
        49865, -- Snagglebolt's Cloaking Device
        49866, -- Life Savings
        49874, -- Goblin Stickybombs
        49875, -- Enervated Adder
        49876, -- Writhing Seed
        49880, -- Funerary Totem
        49881, -- Slaver's Key
        49882, -- Soothing Seeds
        49883, -- Squirming Heart
        49887, -- KTC Snapflash
        49913, -- Borrowed Silver Covenant Tabard
        49932, -- Carved Boar Idol
        49934, -- Blood Shard Trinket
        49945, -- Severed Extremity
        49946, -- Enormous Kodo Heart
        49947, -- Fresh Brain
        49948, -- Calder's Bonesaw
        50018, -- Naj'tess' Orb of Corruption
        50031, -- Tomusa's Hook
        50044, -- Polished Boar Skull
        50053, -- Bloodtalon Lasso
        50054, -- SI:7 Briefings
        50128, -- Bucket of Burning Pitch
        50162, -- Crawgol's Silithid Field Guide
        50219, -- Side of Stag Meat
        50222, -- Wildmane Cat Pelt
        50223, -- Bael'Dun Fortress Schematics
        50232, -- Wild Clucker Eggs
        50239, -- Spiny Raptor Egg
        50261, -- The Biggest Egg Ever
        50288, -- Blood Elf's Belongings
        50374, -- Unbelievably Sticky Tar
        50381, -- Shark Parts
        50382, -- Mechashark X-Steam
        50383, -- Mahka's Gift
        50385, -- Disturbed Earth Fragment
        50437, -- Intact Naga Hide
        50445, -- Stonetalon Supplies
        50448, -- Kaldorei Artifact
        50465, -- Water Pitcher
        50473, -- Mane of Thornmantle
        50602, -- Irresistible Pool Pony
        50829, -- Steamwheedle Supplies
        51547, -- Burning Rum
        51549, -- Pirate Booty
        51567, -- Meg's Pager
        51778, -- Hyena Chunk
        51781, -- Yngwie's Body
        51793, -- Ocular Crystal
        51957, -- Langridge Shot
        52013, -- Super Booster Rocket Boots
        52014, -- Herb-Soaked Bandages
        52017, -- Paxton's Torch
        52024, -- Rockin' Powder
        52031, -- Bootlegger Bug Bait
        52032, -- Rockin' Powder Infused Rocket Boots
        52035, -- Fire Gland
        52036, -- Rageroar's Helm
        52038, -- Refurbished Ogre Suit
        52041, -- Centipaarts
        52043, -- Bootzooka
        52044, -- Bilgewater Cartel Promotional Delicacy Morsels
        52045, -- Northwatch Supply Crate
        52046, -- Bramblestaff
        52047, -- Sandscraper's Scarab
        52059, -- Murloc Leash
        52061, -- Ancient Hieroglyphs
        52064, -- Fistful of Blood
        52065, -- Territorial Fetish
        52069, -- Sealed Sang'thraze
        52072, -- Darkest Mojo
        52073, -- Bramblestaff
        52080, -- Fresh Crawler Meat
        52189, -- Elemental Sapta
        52198, -- Fresh Stormsnout Steak
        52202, -- Elemental Sapta
        52271, -- Northwatch Manacles
        52272, -- Cleansing Totem
        52275, -- Tablets of the Earth
        52277, -- Taurajo Intelligence
        52279, -- Salt-Crusted Journal
        52280, -- Satchel of Grenades
        52281, -- Meatface's Locked Chest
        52282, -- Turtle-Digested Key
        52283, -- Bloodtalon Whistle
        52284, -- Earthen Jewel
        52285, -- Siege Engine Scrap
        52290, -- Keg of Blast-O Powder
        52305, -- Humming Electrogizard
        52324, -- Frontline Report
        52345, -- Cleansing Totem
        52346, -- Commander Arrington's Head
        52347, -- Darkblade Cyn's Head
        52349, -- Alexi Silenthowl's Head
        52360, -- Bael Modan Artifact
        52469, -- Twinbraid's Tools
        52470, -- The Grand Tablet
        52471, -- Lasher Tail
        52472, -- Pound of Dunestalker Legs
        52473, -- Basilisk Brain
        52474, -- Gazer Guts
        52475, -- Hyena Jaw
        52476, -- Rabid Liver
        52477, -- Scorpitar's Husk
        52478, -- Hellgazer's Fins
        52479, -- Slaverjaw's Skeleton
        52481, -- Blastshadow's Soulstone
        52483, -- Kaja'Cola Zero-One
        52484, -- Kaja'Cola Zero-One
        52491, -- Mug of Ol' Barkerstout
        52504, -- Conch Shell
        52506, -- Elemental Goo
        52507, -- Stardust No. 2
        52513, -- Glyphic Parchment
        52530, -- Spare Shredder Parts
        52531, -- Still-Beating Heart
        52537, -- Flame Blossom
        52559, -- Fickle Heart
        52560, -- Kajaro 0W Grade Oil
        52561, -- Single-Stage Booster Rockets
        52568, -- Twilight Supplies
        52573, -- Stonetear
        52576, -- Ysondre's Tear
        52581, -- SI:7 Emblem
        52582, -- Sealed Package
        52658, -- Polluted Incense
        52682, -- Lycanthoth's Incense
        52683, -- Blackjack
        52707, -- Warning Poster
        52710, -- Enchanted Conch
        52712, -- Remote Control Fireworks
        52713, -- Gnomeregan Assault Battle Speech
        52714, -- Dwarven Slug
        52715, -- Butcherbot Control Gizmo
        52726, -- Stonebloom
        52727, -- Bitterblossom
        52728, -- Darkflame Ember
        52730, -- Cleansing Draught
        52734, -- Broken Shard
        52789, -- Rusted Skull Key
        52832, -- Gordunni Orb
        52833, -- Modified Soul Orb
        52834, -- Charged Condenser Jar
        52853, -- Totem of Goldrinn
        52854, -- Totem of Lo'Gosh
        52973, -- Sunken Cargo
        52975, -- Coilshell Sifter
        53052, -- Sambino's Modified Earthbind Totem
        53054, -- Horde Chest Key
        53059, -- Rent Scrap Metal
        53060, -- Frightened Animal
        53061, -- Case of Crabs
        53073, -- Clacksnap Tail
        53074, -- Adarrah's Keepsake
        53099, -- Head of Skessesh
        53101, -- Tessina's Wisp Call
        53102, -- Bileberry
        53104, -- Tessina's Hippogryph Call
        53105, -- Tessina's Treant Call
        53120, -- Bottled Bileberry Brew
        53135, -- Glowing Soil
        53136, -- Soul Essence
        53139, -- Twilight Overseer's Key
        53454, -- Black Heart of Thol'embaar
        53464, -- Charred Branch
        53635, -- Sharpened Crab Claw
        53636, -- Shark Sinew
        53642, -- Infected Stag Antler
        53794, -- Rendel's Bridle
        54214, -- Sambino's Modified Stoneclaw Totem
        54216, -- Sambino's Modified Strength of Earth Totem
        54217, -- Sambino's Modified Stoneskin Totem
        54344, -- Corroded Key
        54439, -- Hyjal Bear Cub
        54453, -- Six-Pack of Kaja'Cola
        54456, -- Mournful Essence
        54458, -- Eel Fillet
        54459, -- Fibrous Vine
        54460, -- Waterlogged Driftwood
        54462, -- Moanah's Baitstick
        54466, -- Toshe's Hunting Spears
        54574, -- Hyjal Seedling
        54608, -- Sambino's Air Balloon
        54609, -- Core Hound Entrails
        54611, -- Sambino's Air Valve
        54613, -- Shark Bait
        54614, -- Luminescent Pearl
        54615, -- Tender Turtle Meat
        54618, -- Shark Bait Sack
        54640, -- Gilblingle's Map
        54644, -- List of Parts
        54646, -- Puffer Hatchling
        54744, -- Heap of Core Hound Innards
        54745, -- Nemesis Shell Fragment
        54746, -- Bottle of Grog
        54747, -- Bottle of Grog
        54785, -- Globes of Tumultuous Water
        54809, -- Rocket Car Parts
        54821, -- Pirate's Crowbar
        54828, -- Saltwater Starfish
        54829, -- Gold Filling
        54840, -- Anemone Chemical Extraction Device
        54842, -- Stormwind Helm
        54843, -- Stormwind Breastplate
        54844, -- Stormwind Shield
        54851, -- Anemone Chemical Application Device
        54855, -- Gargantapid's Poison Gland
        54856, -- Duneclaw Stinger
        54861, -- Glimmerdeep Clam
        54884, -- Hefty Sack of Clams
        54957, -- Stormwind S.E.A.L. Equipment
        54958, -- Stormwind Spear
        55049, -- Fang of Goldrinn
        55050, -- Fang of Lo'Gosh
        55121, -- River Boat
        55123, -- Smoldering Core
        55136, -- Tome of Openings
        55138, -- Sunken Treasure Chest
        55139, -- Fiasco's Stray Parts
        55140, -- Snapjaw Grouper Meat
        55141, -- Spiralung
        55143, -- Keg of Gunpowder
        55144, -- Filthy Goblin Technology
        55145, -- Pincer Entrails
        55151, -- Pilfered Supplies
        55152, -- Warsong Flame Thrower
        55158, -- Fake Treasure
        55160, -- Rusted Cage Key
        55163, -- Enchanted Coral
        55164, -- Grouper Liver Oil
        55165, -- Enchanted Sea Snack
        55171, -- Blade of the Naz'jar Battlemaiden
        55174, -- Azure Iron Ore
        55175, -- Hippogryph Muisek Vessel
        55177, -- Crab Entrails
        55180, -- Darkmist Amulet
        55181, -- Illegible Orc Letter
        55182, -- Translated Orc Letter
        55188, -- Medallion Fragment
        55189, -- Hyjal Egg
        55190, -- Booby-Trapped Bait
        55196, -- Evil Dolly's Heart
        55197, -- Seadog Fajardo's Lungs
        55199, -- Lilly Landlubber's Liver
        55200, -- Horde Cage Key
        55208, -- Sacred Nectar
        55209, -- Mountaineer's Ale
        55210, -- Ancient Feather
        55211, -- Enormous Bird Call
        55213, -- Huntress Illiona's Cage Key
        55220, -- Budd's Chain
        55221, -- Mechanized Fire
        55222, -- Mechanized Ice
        55223, -- Mechanized Air
        55226, -- Creature Carcass
        55227, -- BD-816 War Apparatus
        55230, -- Soul Stick
        55231, -- Flood Sediment Sample
        55232, -- Threshadon Chunk
        55233, -- Handful of Fenberries
        55234, -- Dumpy Level
        55239, -- Cragjaw's Huge Tooth
        55240, -- Spark-Proof Pick
        55241, -- Incendicite Ore
        55242, -- Thelgen Seismic Record
        55243, -- Floodsurge Core
        55280, -- Deepmoss Venom Sac
        55807, -- Alliance's Proposal
        55808, -- Horde's Proposal
        55809, -- Twilight Armor Plate
        55837, -- Loramus' Legs
        55883, -- Thisalee's Shiv
        55965, -- Bloated Kelp Bulb
        55966, -- Crude Rubbing Kit
        55967, -- Messy Rubbing
        55968, -- Torn Rubbing
        55969, -- Smudged Rubbing
        55970, -- Smeared Rubbing
        55971, -- Eldre'thar Relic
        55972, -- Highborne Prison
        55977, -- Mountaineer's Ale
        55979, -- The Sacred Flame
        55986, -- Splithoof Brand
        55987, -- Splithoof Brand
        55988, -- Glowerglare's Beard
        56002, -- Needles Pyrite Ore Chunk
        56003, -- Thisalee's Signal Rocket
        56005, -- Slabchisel Report
        56006, -- Pallet of "Gold"
        56008, -- Rift Shard
        56009, -- Rune of Fire
        56010, -- Alliance Weapon Crate
        56011, -- Oil Drilling Rig
        56013, -- Meaty Crawler Claw
        56014, -- Infrared Heat Detection Device
        56016, -- Herald's Incense
        56018, -- Dynamite Bundle
        56020, -- Erunak's Scrying Orb
        56024, -- Portal-Sentry's Mining Pick
        56025, -- Wazzik's Package
        56031, -- Fitzsimmons' Mead
        56032, -- Loramus' Body
        56037, -- Barrel of Oil
        56040, -- Ram Haunch
        56041, -- Kalimdor Eagle Egg
        56042, -- Boulderslide Cheese
        56048, -- Signal Flare
        56057, -- Heart of the Forest
        56058, -- Wobbling Raptor Egg
        56059, -- Meaty Offering
        56060, -- Grimtotem Terms of Service
        56061, -- Alliance Weapon Crates
        56069, -- Alliance Weapon Crate
        56080, -- Insignia of the Horde General
        56081, -- Trapper's Key
        56082, -- Archaeologist's Tools
        56083, -- Fossilized Bone
        56085, -- Swiftgear Gizmo
        56087, -- Marshy Crocolisk Hide
        56088, -- Ironforge Ingot
        56089, -- Horrorjaw's Hide
        56091, -- Krom'gar Log Book
        56134, -- Blessed Floodlily
        56139, -- Longhorn's Horn
        56140, -- Goblin Pump Controller
        56167, -- Wiggleweed Sprout
        56168, -- Thal'darah's Hippogryph Whistle
        56169, -- Breathstone
        56176, -- Warden's Arrow
        56177, -- Glyph of Opening
        56181, -- Alliance Survival Kit
        56182, -- Deepseeker Crab
        56183, -- Coil of Kvaldir Rope
        56184, -- Duarn's Net
        56185, -- Purloined Polearm
        56186, -- Elder Sareth'na's Sketch Book
        56187, -- Sentinel's Glaive
        56188, -- Rescue Flare
        56194, -- Mysterious Pearl
        56207, -- Totem of Tortolla
        56216, -- Marsh Lasher Seed
        56221, -- Salsbury's Rocket Pack
        56222, -- Runes of Return
        56223, -- Black Dragon Whelp Filet
        56224, -- Blazing Heart of Fire
        56225, -- Frozen Artifact
        56226, -- Excavator's Pick
        56227, -- Enchanted Conch
        56235, -- Deepfin Plunder
        56243, -- Orgrimmar Axe
        56244, -- Orgrimmar Helm
        56245, -- Orgrimmar Breastplate
        56247, -- Box of Crossbow Bolts
        56248, -- Alliance S.E.A.L. Equipment
        56250, -- Alliance Standard
        56251, -- Horde Survival Kit
        56254, -- Merciless Head
        56255, -- Horde Standard
        56263, -- Land Mine Wrench
        56264, -- Dark Iron Attack Plans
        56469, -- Alliance Attack Plans
        56470, -- Infrared Heat Focals
        56473, -- Krom'gar Flame Thrower
        56474, -- Orders from Base Camp
        56568, -- Underlight Phosphora
        56569, -- Underlight Nibbler
        56570, -- Enormous Eel Egg
        56571, -- Enormous Eel Egg
        56572, -- Phosphora Fedora
        56573, -- Glow-Juice
        56575, -- Spare Part
        56576, -- Orb of Suggestion
        56577, -- Crate of Spare Parts
        56794, -- Subjugator Devo's Whip
        56795, -- Resonite Crystal
        56796, -- Stormer Heart
        56797, -- Stonetalon Ram Horns
        56798, -- Jin'Zil's Voodoo Stick
        56799, -- Kobold War Horn
        56800, -- Jibbly's Gas Bomb
        56801, -- Erunak's Confinement Totem
        56802, -- Digsong's Fish and Tips
        56803, -- Firefighting Gear
        56804, -- Sheathed Trol'kalar
        56808, -- Eel-Splosive Device
        56809, -- Ironforge Banner
        56810, -- Scalding Shroom
        56811, -- Orako's Report
        56812, -- Enormous Eel Egg
        56813, -- Fish Hat
        56814, -- Iron Hammer Bomb
        56815, -- Eel-Splosive Device
        56816, -- Krom'gar General's Insignia
        56818, -- Terrapin Oil
        56819, -- Remora Oil
        56820, -- Hammerhead Oil
        56821, -- Oil Extrusion Pump
        56822, -- The Brain of the Unfathomable
        56823, -- Stonebreaker's Report
        56827, -- Billowing Fuel Sample
        56828, -- Atomic Fuel Sample
        56829, -- Anemic Fuel Sample
        56830, -- Perduring Fuel Sample
        56831, -- Smoky Fuel Sample
        56832, -- Quick-Burning Fuel Sample
        56833, -- Promising Fuel Sample
        56835, -- Seahorse Lasso
        56837, -- Sturdy Manacles
        57096, -- Runestone of Binding
        57102, -- Twilight Cage Key
        57117, -- Buttonwillow's Hand Grenade
        57118, -- Twilight Cage Key
        57119, -- Enohar's Explosive Arrows
        57121, -- Dark Letter
        57122, -- James Clark's Head
        57131, -- Intact Crocolisk Jaw
        57132, -- Dark Iron Dispatch
        57133, -- Stonewrought Schematics
        57134, -- Intact Shadowsworn Spell Focus
        57136, -- A Letter to Kasim Sharim
        57137, -- Stormwind Pumpkin
        57138, -- Grimeo's Mining Pick
        57140, -- Grimeo's Package
        57172, -- Attuned Runestone of Binding
        57177, -- Tainted Hide
        57178, -- Nightstalker Leg
        57179, -- Screecher Brain
        57181, -- Crushed Nightstalker Leg
        57182, -- Screecher Brain Paste
        57183, -- Demoniac Commixture
        57184, -- Demoniac Vessel
        57185, -- Demoniac Vessel
        57196, -- Reliquary Papers
        57197, -- Juicy Apple
        57245, -- Gigantic Catfish
        57246, -- Confectioners' Sugar
        57259, -- Ferilon's Care Package
        57409, -- Valoren's Shrinkage Totem
        57412, -- Boom Boots
        57755, -- Riverpaw Gnoll Clue
        57756, -- Murloc Clue
        57758, -- Swine Belly
        57761, -- Two-Shoed Lou's Old House
        57764, -- Spare Part
        57765, -- Muddy Crawfish
        57766, -- Prickly Pear Fruit
        57786, -- Stringy Fleshripper Meat
        57787, -- Coyote Tail
        57788, -- Goretusk Flank
        57789, -- Fresh Dirt
        57877, -- Barrel of Darkspear Rice
        57878, -- Barrel of Kezan Rice
        57879, -- Horde Infantry Rations
        57911, -- Okra
        57912, -- Wildhammer Totem
        57920, -- Revantusk War Drums
        57954, -- Harvest Watcher Heart
        57987, -- Recovered Possession
        57988, -- Westfall Stew
        57989, -- Ooze-Coated Supply Crate
        57990, -- Green Hills of Stranglethorn - Page 14
        57991, -- Westfall Stew
        58082, -- Direglob Sample
        58083, -- Grom'gol Rations
        58095, -- Kotonga's Totem of Persuasion
        58111, -- Gnoll Attack Orders
        58112, -- Potion of Shrouding
        58113, -- Informational Pamphlet
        58114, -- Issue of the Moonbrook Times
        58115, -- Secret Journal
        58116, -- Mysterious Propaganda
        58117, -- Red Bandana
        58118, -- Red Bandana
        58120, -- Skittering Spiderling
        58141, -- Twilight Highlands Coastal Chart
        58147, -- Incense Burner
        58165, -- Lashtail Raptor Egg Fragment
        58166, -- Message to Stormwind
        58168, -- Irestone Core
        58169, -- Elementium Grapple Line
        58171, -- Zuuldaian Fetish
        58177, -- Earthen Ring Proclamation
        58178, -- Jessup's Report
        58179, -- Gan'zulah's Body
        58200, -- Techno-Grenade
        58201, -- Skullsplitter Mojo
        58202, -- Stolen Powder Keg
        58203, -- Paintinator
        58204, -- Chasm Ooze
        58205, -- Mosh'Ogg Bounty
        58209, -- Tiki Torch
        58224, -- Induction Samophlange
        58225, -- Braddok's Big Brain
        58228, -- Spider Idol
        58236, -- Umboda's Head
        58239, -- Shadra-Spawn Venom Sample
        58241, -- Tablet of Shadra
        58249, -- Raptor Food Pouch
        58250, -- Burning Twig
        58251, -- Gurubashi Skull
        58252, -- Shadraspawn Egg
        58253, -- Orbital Targeting Device
        58254, -- Delicate Chain Smasher
        58271, -- Sample Casks
        58281, -- Fang of Shadra
        58282, -- Eye of Shadra
        58361, -- Blackrock Orc Weapon
        58362, -- Milly's Fire Extinguisher
        58363, -- Voucher of Bravery
        58364, -- Argus' Note
        58366, -- Mark of Bloodhoof
        58490, -- Opened Mosh'Ogg Bounty
        58491, -- Disfigured Mosh'Ogg Hand
        58492, -- Disfigured Mosh'Ogg Hand
        58500, -- Jade Crystal Cluster
        58501, -- Quartzite Resin
        58502, -- Explosive Bonding Compound
        58503, -- Hardened Walleye
        58779, -- Shell of Shadra
        58783, -- Jade Crystal Composite
        58784, -- Idol of Shadra
        58787, -- Crystal Bass
        58788, -- Overgrown Earthworm
        58798, -- Bottle of Whiskey
        58806, -- Spool of Rope
        58809, -- Rock Lobster
        58811, -- The Holy Water of Clarity
        58812, -- Supple Tigress Fur
        58813, -- Velvety Panther Fur
        58845, -- Chalky Crystal Formation
        58856, -- Royal Monkfish
        58860, -- Gurubashi Challenge Flag
        58864, -- Precious Locket
        58867, -- Snapjaw Gizzard
        58877, -- Naga Icon
        58880, -- Sassy's Secret Ingredient
        58882, -- Sassy's Samples
        58883, -- Sassy's Largesse
        58884, -- Stonefather's Banner
        58885, -- Rockslide Reagent
        58886, -- Thunder Stone
        58887, -- Gnoll Battle Plans
        58888, -- Gnoll Orders
        58889, -- Gnoll Strategy Guide
        58890, -- Parker's Report
        58891, -- Tarantula Eyes
        58892, -- Condor Giblets
        58893, -- Goretusk Kidney
        58894, -- Gnomecorder
        58895, -- Ettin Control Orb
        58896, -- Redridge Supply Crate
        58897, -- Redridge Gnoll Collar
        58899, -- Violet Perch
        58901, -- Zanzil's Formulation
        58932, -- Crate of Dragonmaw Bandages
        58934, -- Monstrous Clam Meat
        58935, -- Gryphon Chow
        58936, -- Blackrock Orc Missive
        58937, -- Blackrock Invasion Plans
        58944, -- Catapult Part
        58945, -- Toxic Puddlefish
        58946, -- Sandy Carp
        58949, -- Stag Eye
        58950, -- Messner's Cage Key
        58951, -- Giant Furious Pike
        58952, -- Blackrock Spyglass
        58953, -- Keeshan's Bow
        58954, -- Keeshan's Survival Knife
        58955, -- Razgar's Fillet Knife
        58956, -- Keeshan's Red Headband
        58957, -- Keeshan's Jade Amulet
        58958, -- Drowned Thunder Lizard Tail
        58959, -- Petrified Stone Bat
        58960, -- Golden Stonefish
        58963, -- Gurubashi Incense
        58964, -- Chabal's Fetish
        58965, -- Deepvein's Patch Kit
        58966, -- Jesana's Faerie Dragon Call
        58967, -- Jesana's Giant Call
        58969, -- Jorgensen's Cage Key
        59030, -- Jasper's Jungle Cooler
        59031, -- Bloodsail Ruby
        59032, -- Bloodsail Sapphire
        59033, -- Blackrock Lever Key
        59034, -- Fitztittle's Ratcheting Torque Wrench
        59035, -- Ephram's Jeweled Mirror
        59036, -- Crate of Grade-E Meat
        59037, -- Crate of Pupellyverbos Port
        59054, -- Giant Crate of Giant Food
        59057, -- Poobah's Tiara
        59058, -- Poobah's Scepter
        59059, -- Poobah's Slippers
        59060, -- Poobah's Diary
        59061, -- Keeshan's Possessions
        59062, -- Blood of Neltharion
        59123, -- Verlok Miracle-Grow
        59142, -- Weather-Beaten Coin
        59143, -- Weather-Beaten Coin
        59144, -- The Earthinator's Cudgel
        59145, -- Bloodsail Orders
        59146, -- Head of Fleet Master Seahorn
        59147, -- Cow Head
        59148, -- Oversized Pirate Hat
        59149, -- Yer Papers
        59150, -- Bushel of Limes
        59151, -- Bloodsail Cannonball
        59152, -- Pile of Leaves
        59153, -- Fox Poop
        59156, -- Muckdweller Gland
        59157, -- Brubaker's Report
        59226, -- Dead-Eye's Flare Gun
        59261, -- Blackrock Holding Pen Key
        59323, -- Stonework Mallet
        59345, -- Rot Blossom
        59357, -- A Slashed Bundle of Letters
        59358, -- Broken Shard
        59361, -- A Torn Journal
        59362, -- The Legend of Stalvan
        59363, -- Mistmantle Family Ring
        59522, -- Key of Ilgalar
        59523, -- Doublerum
        59524, -- Narkk's Handbombs
        60203, -- Sassy's Incentive
        60204, -- Corpseweed
        60205, -- Bundle of Corpseweed
        60206, -- Harris's Ampule
        60207, -- Widow Venom Sac
        60212, -- Morbent's Bane
        60213, -- Crystal Spine Basilisk Blood
        60214, -- Kurzen Compound Prison Records
        60215, -- Kurzen Compound Officers' Dossier
        60225, -- Holy Censer
        60263, -- Whispering Blue Stone
        60264, -- Twilight Orders
        60266, -- Stonework Mallet
        60270, -- Icon of Tsul'Kalu
        60271, -- Icon of Mahamba
        60272, -- Icon of Pogeyan
        60273, -- Emerine's Telling-Sphere
        60274, -- Sea Salt
        60291, -- Tkashi Fetish
        60295, -- Bloodscalp Lore Tablet
        60296, -- Sack of Semi-Precious Stones
        60297, -- Side of Basilisk Meat
        60333, -- Whirlpool Core
        60334, -- Black Bear Brain
        60337, -- Verrall River Muck
        60370, -- Gar'Thok's Reports
        60373, -- Linzi's Gift
        60374, -- Maywiki's Fetish
        60380, -- Ironjaw Humour
        60383, -- Twilight Snare
        60384, -- Bravo Company Field Kit
        60385, -- Bravo Company Field Kit
        60386, -- Pure Water
        60387, -- Gurubashi Challenge Flag
        60400, -- Forbidden Sigil
        60401, -- Tender Boar Meat
        60402, -- Mosshide Ear
        60404, -- Foreman Sharpsneer's Head
        60487, -- Elemental Ore
        60490, -- The Axe of Earthly Sundering
        60494, -- South Gate Status Report
        60495, -- Rugelfuss's Status Report
        60496, -- Tender Boar Ribs
        60497, -- Bear Rump
        60499, -- Searing Binding
        60500, -- Foreman Sharpsneer's Head
        60501, -- Stormstone
        60502, -- Clever Plant Disguise Kit
        60503, -- Potent Murloc Pheromones
        60504, -- Painite Chunk
        60511, -- Murloc Scent Gland
        60575, -- The Middle Fragment of the World Pillar
        60615, -- Durotar Crocolisk Tooth
        60616, -- Boots
        60617, -- No Gloves
        60620, -- No Bracers
        60621, -- Sandals
        60622, -- Cool Shades
        60623, -- Cool Shades
        60624, -- Dress
        60643, -- Befouled Water Globe
        58898, -- Dirt-Stained Scroll
        63517, -- Baradin's Wardens Commendation
        64492, -- Ramkahen Badge of Valor
        70145, -- Darnassus Writ of Commendation
        70146, -- Exodar Writ of Commendation
        70147, -- Gnomeregan Writ of Commendation
        70148, -- Ironforge Writ of Commendation
        70149, -- Orgrimmar Writ of Commendation
        70150, -- Sen'jin Writ of Commendation
        70151, -- Silvermoon Writ of Commendation
        70152, -- Stormwind Writ of Commendation
        70153, -- Thunder Bluff Writ of Commendation
        70154, -- Undercity Writ of Commendation
        71087, -- Gilneas Writ of Commendation
        71088, -- Bilgewater Writ of Commendation
        61988, -- Ace of Embers
        61989, -- Two of Embers
        61990, -- Three of Embers
        61991, -- Four of Embers
        61992, -- Five of Embers
        61993, -- Six of Embers
        61994, -- Seven of Embers
        61995, -- Eight of Embers
        61996, -- Ace of Stones
        61997, -- Two of Stones
        61998, -- Three of Stones
        61999, -- Four of Stones
        62000, -- Five of Stones
        62001, -- Six of Stones
        62002, -- Seven of Stones
        62003, -- Eight of Stones
        62004, -- Ace of the Winds
        62005, -- Two of the Winds
        62006, -- Three of the Winds
        62007, -- Four of the Winds
        62008, -- Five of the Winds
        62009, -- Six of the Winds
        62010, -- Seven of the Winds
        62011, -- Eight of the Winds
        62012, -- Ace of Waves
        62013, -- Two of Waves
        62014, -- Three of Waves
        62015, -- Four of Waves
        62016, -- Five of Waves
        62017, -- Six of Waves
        62018, -- Seven of Waves
        62019, -- Eight of Waves
        63700, -- Myzerian's Head
        71716, -- Soothsayer's Runes
        71635, -- Imbued Crystal
        71636, -- Monstrous Egg
        71637, -- Mysterious Grimoire
        71638, -- Ornate Weapon
        71715, -- A Treatise on Strategy
        71951, -- Banner of the Fallen
        71952, -- Captured Insignia
        71953, -- Fallen Adventurer's Journal
        62021, -- Volcanic Deck
        62044, -- Tsunami Deck
        62045, -- Hurricane Deck
        62046, -- Earthquake Deck
        71141, -- Eternal Ember
        69238, -- Timeless Eye
        69245, -- Timeless Eye
        69646, -- Branch of Nordrassil
        69848, -- Heart of Flame
        69815, -- Seething Cinder
        78352  -- Fragment of Deathwing's Jaw
    },
    [addon.CONS.RECIPES_ID] = {
        [addon.CONS.R_BOOKS_ID] = {
            71949  -- Tome of Burning Jewels
        },
        [addon.CONS.R_ALCHEMY_ID] = {
            65498, -- Recipe: Big Cauldron of Battle
            65435, -- Recipe: Cauldron of Battle
            67538  -- Recipe: Vial of the Sands
        },
        [addon.CONS.R_BLACKSMITHING_ID] = {
            66100, -- Plans: Ebonsteel Belt Buckle
            66101, -- Plans: Pyrium Shield Spike
            66122, -- Plans: Vicious Pyrium Legguards
            66123, -- Plans: Vicious Pyrium Helm
            66124, -- Plans: Vicious Pyrium Breastplate
            66130, -- Plans: Vicious Ornate Pyrium Legguards
            66131, -- Plans: Vicious Ornate Pyrium Helm
            66132, -- Plans: Vicious Ornate Pyrium Breastplate
            66110, -- Plans: Elementium Spellblade
            66112, -- Plans: Elementium Poleaxe
            66114, -- Plans: Elementium Shank
            66115, -- Plans: Elementium Earthguard
            66116, -- Plans: Elementium Stormshield
            66121, -- Plans: Vicious Pyrium Shoulders
            66129, -- Plans: Vicious Ornate Pyrium Shoulders
            67606, -- Plans: Forged Elementium Mindcrusher
            66104, -- Plans: Hardened Elementium Hauberk
            66106, -- Plans: Elementium Deathplate
            66108, -- Plans: Light Elementium Chestguard
            66111, -- Plans: Elementium Hammer
            66113, -- Plans: Elementium Bonesplitter
            66120, -- Plans: Vicious Pyrium Boots
            66128, -- Plans: Vicious Ornate Pyrium Boots
            67603, -- Plans: Elementium Gutslicer
            66105, -- Plans: Hardened Elementium Girdle
            66107, -- Plans: Elementium Girdle of Pain
            66109, -- Plans: Light Elementium Belt
            66119, -- Plans: Vicious Pyrium Belt
            66127, -- Plans: Vicious Ornate Pyrium Belt
            66103, -- Plans: Pyrium Weapon Chain
            66117, -- Plans: Vicious Pyrium Bracers
            66118, -- Plans: Vicious Pyrium Gauntlets
            66125, -- Plans: Vicious Ornate Pyrium Bracers
            66126, -- Plans: Vicious Ornate Pyrium Gauntlets
            70166, -- Plans: Brainsplinter
            70167, -- Plans: Masterwork Elementium Spellblade
            70168, -- Plans: Lightforged Elementium Hammer
            70169, -- Plans: Elementium-Edged Scalper
            70170, -- Plans: Pyrium Spellward
            70171, -- Plans: Unbreakable Guardian
            70172, -- Plans: Masterwork Elementium Deathblade
            70173, -- Plans: Witch-Hunter's Harvester
            74274, -- Plans: Phantom Blade
            69957, -- Plans: Fists of Fury
            69958, -- Plans: Eternal Elementium Handguards
            69959, -- Plans: Holy Flame Gauntlets
            69968, -- Plans: Warboots of Mighty Lords
            69969, -- Plans: Mirrored Boots
            69970, -- Plans: Emberforged Elementium Boots
            72001, -- Plans: Pyrium Legplates of Purified Evil
            72012, -- Plans: Unstoppable Destroyer's Legplates
            72013, -- Plans: Foundations of Courage
            72014, -- Plans: Soul Redeemer Bracers
            72015, -- Plans: Bracers of Destructive Strength
            72016  -- Plans: Titanguard Wristplates
        },
        [addon.CONS.R_COOKING_ID] = {
            65432, -- Recipe: Fortune Cookie
            65433, -- Recipe: South Island Iced Tea
            65431, -- Recipe: Chocolate Cookie
            65420, -- Recipe: Mushroom Sauce Mudfish
            65421, -- Recipe: Severed Sagefish Head
            65422, -- Recipe: Delicious Sagefish Tail
            65423, -- Recipe: Fish Fry
            65424, -- Recipe: Blackbelly Sushi
            65425, -- Recipe: Skewered Eel
            65426, -- Recipe: Baked Rockfish
            65427, -- Recipe: Basilisk Liverdog
            65428, -- Recipe: Grilled Dragon
            65429, -- Recipe: Beer-Basted Crocolisk
            65430, -- Recipe: Crocolisk Au Gratin
            68688, -- Recipe: Scalding Murglesnout
            65417, -- Recipe: Pickled Guppy
            65418, -- Recipe: Hearty Seafood Soup
            65419, -- Recipe: Tender Baked Turtle
            65406, -- Recipe: Whitecrest Gumbo
            65407, -- Recipe: Lavascale Fillet
            65408, -- Recipe: Feathered Lure
            65409, -- Recipe: Lavascale Minestrone
            65410, -- Recipe: Salted Eye
            65411, -- Recipe: Broiled Mountain Trout
            65412, -- Recipe: Lightly Fried Lurker
            65413, -- Recipe: Seasoned Crab
            65414, -- Recipe: Starfire Espresso
            65415, -- Recipe: Highland Spirits
            65416, -- Recipe: Lurker Lunch
            78342, -- Plump Dig Rat
            62799, -- Recipe: Broiled Dragon Feast
            62800  -- Recipe: Seafood Magnifique Feast
        },
        [addon.CONS.R_ENCHANTING_ID] = {
            52737, -- Formula: Enchant Cloak - Greater Critical Strike
            52738, -- Formula: Enchant Bracer - Greater Critical Strike
            52739, -- Formula: Enchant Chest - Peerless Stats
            52740, -- Formula: Enchant Chest - Greater Stamina
            64411, -- Formula: Enchant Boots - Assassin's Step
            64412, -- Formula: Enchant Boots - Lavawalker
            64413, -- Formula: Enchant Bracer - Greater Speed
            64414, -- Formula: Enchant Gloves - Greater Mastery
            64415, -- Formula: Enchant Gloves - Mighty Strength
            71714, -- Formula: Enchant Cloak - Lesser Agility
            78343, -- Formula: Enchant Gloves - Herbalism
            52733, -- Formula: Enchant Weapon - Power Torrent
            52735, -- Formula: Enchant Weapon - Windwalk
            52736, -- Formula: Enchant Weapon - Landslide
            68787, -- Formula: Enchant Bracer - Agility
            68788, -- Formula: Enchant Bracer - Major Strength
            68789, -- Formula: Enchant Bracer - Mighty Intellect
            67308, -- Formula: Enchanted Lantern
            67312, -- Formula: Magic Lamp
            78348  -- Formula: Enchant Weapon - Executioner
        },
        [addon.CONS.R_ENGINEERING_ID] = {
            70177, -- Schematic: Flintlocke's Woodchucker
            71078  -- Schematic: Extreme-Impact Hole Puncher
        },
        [addon.CONS.R_INSCRIPTIONS_ID] = {
            65649, -- Technique: Origami Slime
            65650, -- Technique: Origami Rock
            65651  -- Technique: Origami Beetle
        },
        [addon.CONS.R_JEWELCRAFTING_ID] = {
            52363, -- Design: Flashing Carnelian
            52364, -- Design: Stormy Zephyrite
            52365, -- Design: Subtle Alicite
            52366, -- Design: Defender's Nightstone
            52367, -- Design: Guardian's Nightstone
            52368, -- Design: Purified Nightstone
            52369, -- Design: Retaliating Nightstone
            52370, -- Design: Polished Hessonite
            52371, -- Design: Inscribed Hessonite
            52372, -- Design: Deadly Hessonite
            52373, -- Design: Potent Hessonite
            52374, -- Design: Fierce Hessonite
            52375, -- Design: Deft Hessonite
            52376, -- Design: Fine Hessonite
            52377, -- Design: Keen Hessonite
            52378, -- Design: Regal Jasper
            52379, -- Design: Nimble Jasper
            52382, -- Design: Piercing Jasper
            52383, -- Design: Steady Jasper
            52385, -- Design: Forceful Jasper
            52386, -- Design: Lightning Jasper
            52388, -- Design: Zen Jasper
            69820, -- Design: Reckless Hessonite
            71821, -- Design: Rigid Deepholm Iolite
            71884, -- Design: Stormy Deepholm Iolite
            71885, -- Design: Sparkling Deepholm Iolite
            71886, -- Design: Solid Deepholm Iolite
            71887, -- Design: Misty Elven Peridot
            71888, -- Design: Piercing Elven Peridot
            71889, -- Design: Lightning Elven Peridot
            71890, -- Design: Sensei's Elven Peridot
            71891, -- Design: Infused Elven Peridot
            71892, -- Design: Zen Elven Peridot
            71893, -- Design: Balanced Elven Peridot
            71894, -- Design: Vivid Elven Peridot
            71895, -- Design: Turbid Elven Peridot
            71896, -- Design: Radiant Elven Peridot
            71897, -- Design: Shattered Elven Peridot
            71898, -- Design: Energized Elven Peridot
            71899, -- Design: Jagged Elven Peridot
            71900, -- Design: Regal Elven Peridot
            71901, -- Design: Forceful Elven Peridot
            71902, -- Design: Nimble Elven Peridot
            71903, -- Design: Puissant Elven Peridot
            71904, -- Design: Steady Elven Peridot
            71905, -- Design: Deadly Lava Coral
            71906, -- Design: Crafty Lava Coral
            71907, -- Design: Potent Lava Coral
            71908, -- Design: Inscribed Lava Coral
            71909, -- Design: Polished Lava Coral
            71910, -- Design: Resolute Lava Coral
            71911, -- Design: Stalwart Lava Coral
            71912, -- Design: Champion's Lava Coral
            71913, -- Design: Deft Lava Coral
            71914, -- Design: Wicked Lava Coral
            71915, -- Design: Reckless Lava Coral
            71916, -- Design: Fierce Lava Coral
            71917, -- Design: Adept Lava Coral
            71918, -- Design: Keen Lava Coral
            71919, -- Design: Artful Lava Coral
            71920, -- Design: Fine Lava Coral
            71921, -- Design: Skillful Lava Coral
            71922, -- Design: Lucent Lava Coral
            71923, -- Design: Tenuous Lava Coral
            71924, -- Design: Willful Lava Coral
            71925, -- Design: Splendid Lava Coral
            71926, -- Design: Resplendent Lava Coral
            71927, -- Design: Glinting Shadow Spinel
            71928, -- Design: Accurate Shadow Spinel
            71929, -- Design: Veiled Shadow Spinel
            71930, -- Design: Retaliating Shadow Spinel
            71931, -- Design: Etched Shadow Spinel
            71932, -- Design: Mysterious Shadow Spinel
            71933, -- Design: Purified Shadow Spinel
            71934, -- Design: Shifting Shadow Spinel
            71935, -- Design: Guardian's Shadow Spinel
            71936, -- Design: Timeless Shadow Spinel
            71937, -- Design: Defender's Shadow Spinel
            71938, -- Design: Sovereign Shadow Spinel
            71939, -- Design: Delicate Queen's Garnet
            71940, -- Design: Precise Queen's Garnet
            71941, -- Design: Brilliant Queen's Garnet
            71942, -- Design: Flashing Queen's Garnet
            71943, -- Design: Bold Queen's Garnet
            71944, -- Design: Smooth Lightstone
            71945, -- Design: Subtle Lightstone
            71946, -- Design: Quick Lightstone
            71947, -- Design: Fractured Lightstone
            71948, -- Design: Mystic Lightstone
            71965, -- Design: Rhinestone Sunglasses
            52362, -- Design: Bold Inferno Ruby
            52380, -- Design: Delicate Inferno Ruby
            52384, -- Design: Flashing Inferno Ruby
            52387, -- Design: Brilliant Inferno Ruby
            52389, -- Design: Precise Inferno Ruby
            52390, -- Design: Solid Ocean Sapphire
            52391, -- Design: Sparkling Ocean Sapphire
            52392, -- Design: Stormy Ocean Sapphire
            52393, -- Design: Rigid Ocean Sapphire
            52394, -- Design: Subtle Amberjewel
            52395, -- Design: Smooth Amberjewel
            52396, -- Design: Mystic Amberjewel
            52397, -- Design: Quick Amberjewel
            52398, -- Design: Fractured Amberjewel
            52399, -- Design: Sovereign Demonseye
            52400, -- Design: Shifting Demonseye
            52401, -- Design: Defender's Demonseye
            52402, -- Design: Timeless Demonseye
            52403, -- Design: Guardian's Demonseye
            52404, -- Design: Purified Demonseye
            52405, -- Design: Etched Demonseye
            52406, -- Design: Glinting Demonseye
            52407, -- Design: Retaliating Demonseye
            52408, -- Design: Veiled Demonseye
            52409, -- Design: Accurate Demonseye
            52410, -- Design: Polished Ember Topaz
            52411, -- Design: Resolute Ember Topaz
            52412, -- Design: Inscribed Ember Topaz
            52413, -- Design: Deadly Ember Topaz
            52414, -- Design: Potent Ember Topaz
            52415, -- Design: Fierce Ember Topaz
            52416, -- Design: Deft Ember Topaz
            52417, -- Design: Reckless Ember Topaz
            52418, -- Design: Skillful Ember Topaz
            52419, -- Design: Adept Ember Topaz
            52420, -- Design: Fine Ember Topaz
            52421, -- Design: Artful Ember Topaz
            52422, -- Design: Keen Ember Topaz
            52423, -- Design: Regal Dream Emerald
            52424, -- Design: Nimble Dream Emerald
            52425, -- Design: Jagged Dream Emerald
            52426, -- Design: Piercing Dream Emerald
            52427, -- Design: Steady Dream Emerald
            52428, -- Design: Forceful Dream Emerald
            52429, -- Design: Lightning Dream Emerald
            52430, -- Design: Puissant Dream Emerald
            52431, -- Design: Zen Dream Emerald
            52432, -- Design: Sensei's Dream Emerald
            52433, -- Design: Fleet Shadowspirit Diamond
            52434, -- Design: Chaotic Shadowspirit Diamond
            52435, -- Design: Bracing Shadowspirit Diamond
            52436, -- Design: Eternal Shadowspirit Diamond
            52437, -- Design: Austere Shadowspirit Diamond
            52438, -- Design: Effulgent Shadowspirit Diamond
            52439, -- Design: Ember Shadowspirit Diamond
            52440, -- Design: Revitalizing Shadowspirit Diamond
            52441, -- Design: Destructive Shadowspirit Diamond
            52442, -- Design: Powerful Shadowspirit Diamond
            52443, -- Design: Enigmatic Shadowspirit Diamond
            52444, -- Design: Impassive Shadowspirit Diamond
            52445, -- Design: Forlorn Shadowspirit Diamond
            52494, -- Design: Jeweler's Ruby Monocle
            52495, -- Design: Jeweler's Sapphire Monocle
            52496, -- Design: Jeweler's Amber Monocle
            68359, -- Design: Willful Ember Topaz
            68360, -- Design: Lucent Ember Topaz
            68361, -- Design: Resplendent Ember Topaz
            68742, -- Design: Vivid Dream Emerald
            68781, -- Design: Agile Shadowspirit Diamond
            68782, -- Design: Reverberating Shadowspirit Diamond
            68783, -- Design: Burning Shadowspirit Diamond
            52381, -- Design: Bold Chimera's Eye
            52447, -- Design: Delicate Chimera's Eye
            52448, -- Design: Flashing Chimera's Eye
            52449, -- Design: Brilliant Chimera's Eye
            52450, -- Design: Precise Chimera's Eye
            52451, -- Design: Solid Chimera's Eye
            52452, -- Design: Sparkling Chimera's Eye
            52453, -- Design: Stormy Chimera's Eye
            52454, -- Design: Rigid Chimera's Eye
            52455, -- Design: Subtle Chimera's Eye
            52456, -- Design: Smooth Chimera's Eye
            52457, -- Design: Mystic Chimera's Eye
            52458, -- Design: Quick Chimera's Eye
            52459, -- Design: Fractured Chimera's Eye
            52460, -- Design: Elementium Destroyer's Ring
            52461, -- Design: Band of Blades
            52462, -- Design: Ring of Warring Elements
            52463, -- Design: Elementium Moebius Band
            52464, -- Design: Brazen Elementium Medallion
            52465, -- Design: Entwined Elementium Choker
            52466, -- Design: Eye of Many Deaths
            52467, -- Design: Elementium Guardian
            69853  -- Design: Punisher's Band
        },
        [addon.CONS.R_LEATHERWORKING_ID] = {
            67042, -- Pattern: Vicious Wyrmhide Bracers
            67044, -- Pattern: Vicious Wyrmhide Belt
            67046, -- Pattern: Vicious Leather Bracers
            67048, -- Pattern: Vicious Leather Gloves
            67049, -- Pattern: Vicious Charscale Bracers
            67053, -- Pattern: Vicious Charscale Gloves
            67054, -- Pattern: Vicious Dragonscale Bracers
            67055, -- Pattern: Vicious Dragonscale Shoulders
            67056, -- Pattern: Vicious Wyrmhide Gloves
            67058, -- Pattern: Vicious Wyrmhide Boots
            67060, -- Pattern: Vicious Leather Boots
            67062, -- Pattern: Vicious Leather Shoulders
            67063, -- Pattern: Vicious Charscale Boots
            67064, -- Pattern: Vicious Charscale Belt
            67065, -- Pattern: Vicious Dragonscale Boots
            67066, -- Pattern: Vicious Dragonscale Gloves
            67068, -- Pattern: Lightning Lash
            67070, -- Pattern: Belt of Nefarious Whispers
            67072, -- Pattern: Stormleather Sash
            67073, -- Pattern: Corded Viper Belt
            67074, -- Pattern: Vicious Wyrmhide Shoulders
            67075, -- Pattern: Vicious Wyrmhide Chest
            67076, -- Pattern: Vicious Leather Belt
            67077, -- Pattern: Vicious Leather Helm
            67078, -- Pattern: Vicious Charscale Shoulders
            67079, -- Pattern: Vicious Charscale Legs
            67080, -- Pattern: Vicious Dragonscale Belt
            67081, -- Pattern: Vicious Dragonscale Helm
            67082, -- Pattern: Razor-Edged Cloak
            67083, -- Pattern: Twilight Dragonscale Cloak
            67084, -- Pattern: Charscale Leg Armor
            67085, -- Pattern: Vicious Wyrmhide Legs
            67086, -- Pattern: Vicious Wyrmhide Helm
            67087, -- Pattern: Vicious Leather Chest
            67089, -- Pattern: Vicious Leather Legs
            67090, -- Pattern: Vicious Charscale Chest
            67091, -- Pattern: Vicious Charscale Helm
            67092, -- Pattern: Vicious Dragonscale Legs
            67093, -- Pattern: Vicious Dragonscale Chest
            67094, -- Pattern: Chestguard of Nature's Fury
            67095, -- Pattern: Assassin's Chestplate
            67096, -- Pattern: Twilight Scale Chestguard
            67100, -- Pattern: Dragonkiller Tunic
            68193, -- Pattern: Dragonscale Leg Armor
            71721, -- Pattern: Drakehide Leg Armor
            72033, -- Pattern: Tough Scorpid Helm
            72030, -- Pattern: Tough Scorpid Leggings
            72027, -- Pattern: Tough Scorpid Shoulders
            72028, -- Pattern: Tough Scorpid Boots
            72025, -- Pattern: Tough Scorpid Gloves
            72026, -- Pattern: Tough Scorpid Bracers
            72029, -- Pattern: Tough Scorpid Breastplate
            70174, -- Pattern: Royal Scribe's Satchel
            70175, -- Pattern: Triple-Reinforced Mining Bag
            78345, -- Pattern: Green Dragonscale Leggings
            78346, -- Pattern: Green Dragonscale Breastplate
            69960, -- Pattern: Dragonfire Gloves
            69961, -- Pattern: Gloves of Unforgiving Flame
            69962, -- Pattern: Clutches of Evil
            69963, -- Pattern: Heavenly Gloves of the Moon
            69971, -- Pattern: Earthen Scale Sabatons
            69972, -- Pattern: Footwraps of Quenched Fire
            69973, -- Pattern: Treads of the Craft
            69974, -- Pattern: Ethereal Footfalls
            71999, -- Pattern: Leggings of Nature's Champion
            72005, -- Pattern: Deathscale Leggings
            72006, -- Pattern: Bladeshadow Leggings
            72007, -- Pattern: Rended Earth Leggings
            72008, -- Pattern: Bracers of Flowing Serenity
            72009, -- Pattern: Thundering Deathscale Wristguards
            72010, -- Pattern: Bladeshadow Wristguards
            72011  -- Pattern: Bracers of the Hunter-Killer
        },
        [addon.CONS.R_TAILORING_ID] = {
            67541, -- Pattern: High Society Top Hat
            68199, -- Pattern: Black Embersilk Gown
            70176, -- Pattern: Luxurious Silk Gem Bag
            54593, -- Pattern: Vicious Embersilk Cowl
            54594, -- Pattern: Vicious Embersilk Pants
            54595, -- Pattern: Vicious Embersilk Robe
            54596, -- Pattern: Vicious Fireweave Cowl
            54597, -- Pattern: Vicious Fireweave Pants
            54598, -- Pattern: Vicious Fireweave Robe
            54599, -- Pattern: Powerful Enchanted Spellthread
            54600, -- Pattern: Powerful Ghostly Spellthread
            54601, -- Pattern: Belt of the Depths
            54602, -- Pattern: Dreamless Belt
            54603, -- Pattern: Breeches of Mended Nightmares
            54604, -- Pattern: Flame-Ascended Pantaloons
            54605, -- Pattern: Illusionary Bag
            69965, -- Pattern: Grips of Altered Reality
            69966, -- Pattern: Don Tayo's Inferno Mittens
            69975, -- Pattern: Endless Dream Walkers
            69976, -- Pattern: Boots of the Black Flame
            72000, -- Pattern: World Mender's Pants
            72002, -- Pattern: Lavaquake Legwraps
            72003, -- Pattern: Dreamwraps of the Light
            72004  -- Pattern: Bracers of Unconquered Power
        }
    },
    [addon.CONS.TRADE_GOODS_ID] = {
        61981, -- Inferno Ink
        61978, -- Blackfallow Ink
        61979, -- Ashen Pigment
        [addon.CONS.T_CLOTH_ID] = {
            54440, -- Dreamcloth
            53010, -- Embersilk Cloth
            53643  -- Bolt of Embersilk Cloth
        },
        [addon.CONS.T_ELEMENTAL_ID] = {
            52325, -- Volatile Fire
            52326, -- Volatile Water
            52327, -- Volatile Earth
            52328, -- Volatile Air
            52329, -- Volatile Life
            54464  -- Random Volatile Element
        },
        [addon.CONS.T_ENCHANTING_ID] = {
            52722, -- Maelstrom Crystal
            52721, -- Heavenly Shard
            52720, -- Small Heavenly Shard
            52723, -- Runed Elementium Rod
            52719, -- Greater Celestial Essence
            52718, -- Lesser Celestial Essence
            52555  -- Hypnotic Dust
        },
        [addon.CONS.T_PARTS_ID] = {
            60224, -- Handful of Obsidium Bolts
            67749  -- Electrified Ether
        },
        [addon.CONS.T_HERBS_ID] = {
            52987, -- Twilight Jasmine
            108364, -- Twilight Jasmine Petal
            52988, -- Whiptail
            108365, -- Whiptail Stem
            52986, -- Heartblossom
            108363, -- Heartblossom Petal
            52984, -- Stormvine
            108361, -- Stormvine Stalk
            52985, -- Azshara's Veil
            108362, -- Azshara's Veil Stem
            52983, -- Cinderbloom
            108360  -- Cinderbloom Petal
        },
        [addon.CONS.T_JEWELCRAFTING_ID] = {
            77951, -- Shadowy Gem
            77952, -- Elementium Gem Cluster
            71806, -- Lightstone
            71807, -- Deepholm Iolite
            71808, -- Lava Coral
            71805, -- Queen's Garnet
            71810, -- Elven Peridot
            52196, -- Chimera's Eye
            52303, -- Shadowspirit Diamond
            52339, -- Flawless Pearl
            71809, -- Shadow Spinel
            52190, -- Inferno Ruby
            52191, -- Ocean Sapphire
            52192, -- Dream Emerald
            52193, -- Ember Topaz
            52194, -- Demonseye
            52195, -- Amberjewel
            52177, -- Carnelian
            52178, -- Zephyrite
            52179, -- Alicite
            52180, -- Nightstone
            52181, -- Hessonite
            52182, -- Jasper
            52338  -- Darkfathom Pearl
        },
        [addon.CONS.T_LEATHER_ID] = {
            52980, -- Pristine Hide
            52979, -- Blackened Dragonscale
            112156, -- Blackened Dragonscale Fragment
            52982, -- Deepsea Scale
            112155, -- Deepsea Scale Fragment
            52981, -- Scarred Shell Fragment
            56516, -- Heavy Savage Leather
            52976, -- Savage Leather
            52977  -- Savage Leather Scraps
        },
        [addon.CONS.T_MEAT_ID] = {
            62779, -- Monstrous Claw
            62780,  -- Snake Eye
            [addon.CONS.TM_ANIMAL_ID] = {
                62781, -- Giant Turtle Tongue
                62782, -- Dragon Flank
                62783, -- Basilisk "Liver"
                62784, -- Crocolisk Tail
                62785, -- Delicate Wing
                62791, -- Blood Shrimp
                67229  -- Stag Flank
            },
            [addon.CONS.TM_FISH_ID] = {
                53068, -- Lavascale Catfish
                53062, -- Sharptooth
                53064, -- Highland Guppy
                53065, -- Albino Cavefish
                53066, -- Blackbelly Mudfish
                53067, -- Striped Lurker
                53063, -- Mountain Trout
                53069, -- Murglesnout 
                53070, -- Fathom Eel
                53071, -- Algaefin Rockfish
                53072  -- Deepsea Sagefish
            }
        },
        [addon.CONS.T_METAL_STONE_ID] = {
            51950, -- Pyrium Bar
            52183, -- Pyrite Ore
            108309, -- Pyrite Ore Nugget
            58480, -- Truegold
            52185, -- Elementium Ore
            108308, -- Elementium Ore Nugget
            52186, -- Elementium Bar
            53039, -- Hardened Elementium Bar
            53038, -- Obsidium Ore
            108307, -- Obsidium Ore Nugget
            54849, -- Obsidium Bar
            65365  -- Folded Obsidium
        },
        [addon.CONS.T_OTHER_ID] = {
            60838, -- Mysterious Fortune Card
            65892, -- Pyrium-Laced Crystalline Vial
            52340, -- Abyssal Clam
            56850, -- Deepstone Oil
            62786, -- Cocoa Beans
            65513, -- Crate of Tasty Meat
            68689, -- Imported Supplies
            60841, -- Fortune Card
            62602, -- Fortune Card
            62603, -- Fortune Card
            62604, -- Fortune Card
            62605, -- Fortune Card
            60839, -- Fortune Card
            62598, -- Fortune Card
            62599, -- Fortune Card
            62600, -- Fortune Card
            62601, -- Fortune Card
            62246, -- Fortune Card
            62577, -- Fortune Card
            62578, -- Fortune Card
            62579, -- Fortune Card
            62580, -- Fortune Card
            62581, -- Fortune Card
            62582, -- Fortune Card
            62583, -- Fortune Card
            62584, -- Fortune Card
            62585, -- Fortune Card
            62586, -- Fortune Card
            62587, -- Fortune Card
            62588, -- Fortune Card
            62589, -- Fortune Card
            62590, -- Fortune Card
            62591, -- Fortune Card
            62247, -- Fortune Card
            62552, -- Fortune Card
            62553, -- Fortune Card
            62554, -- Fortune Card
            62555, -- Fortune Card
            62556, -- Fortune Card
            62557, -- Fortune Card
            62558, -- Fortune Card
            62559, -- Fortune Card
            62560, -- Fortune Card
            62561, -- Fortune Card
            62562, -- Fortune Card
            62563, -- Fortune Card
            62564, -- Fortune Card
            62565, -- Fortune Card
            62566, -- Fortune Card
            62567, -- Fortune Card
            62568, -- Fortune Card
            62569, -- Fortune Card
            62570, -- Fortune Card
            62571, -- Fortune Card
            62572, -- Fortune Card
            62573, -- Fortune Card
            62574, -- Fortune Card
            62575, -- Fortune Card
            62576, -- Fortune Card
            60842, -- Fortune Card
            52197, -- Figurine - Demon Panther
            65894, -- Figurine - Dream Owl
            65895, -- Figurine - King of Boars
            65896, -- Figurine - Jeweled Serpent
            65897, -- Figurine - Earthen Guardian
            52078, -- Chaos Orb
            65893, -- Sands of Time
            61987, -- Darkmoon Card of Destruction
            69237, -- Living Ember
            60845, -- Fortune Card
            60843, -- Fortune Card
            62606, -- Fortune Card
            60844, -- Fortune Card
            71998, -- Essence of Destruction
            60840  -- Fortune Card
        }
    }
}