local _, addon = ...

addon.ITEM_DATABASE[addon.CONS.MISTS_OF_PANDARIA_ID] = {
    [addon.CONS.CONTAINERS_ID] = {
        88397, -- Grummlepack
        73241, -- Merchant's Satchel
        73242, -- Master's Haversack
        92070, -- Huojin Satchel
        92071, -- Tushui Satchel
        98059, -- Kor'kron Supply Satchel
        103629, -- Lucky Satchel
        82446, -- Royal Satchel
        92748, -- Portable Refrigerator
        92747, -- Advanced Refrigeration Unit
        95536  -- Magnificent Hide Pack
    },
    [addon.CONS.CONSUMABLES_ID] = {
        [addon.CONS.C_BANDAGES_ID] = {
            72986, -- Heavy Windwool Bandage
            72985  -- Windwool Bandage
        },
        [addon.CONS.C_CONSUMABLES_ID] = {
            77589, -- G91 Landshark
            87216, -- Thermal Anvil
            77532, -- Locksmith's Powderkeg
            87567, -- Food
            87568, -- Food
            90458, -- Lesser Charm of Good Fortune
            108438, -- Moonkin Hatchling
            79918, -- Quilen Statuette
            79919, -- Anatomical Dummy
            104038, -- Cursed Swabby Helmet
            87214  -- Blingtron 4000
        },
        [addon.CONS.C_ELIXIRS_ID] = {
            76075, -- Mantid Elixir
            76076, -- Mad Hozen Elixir
            76077, -- Elixir of Weaponry
            76078, -- Elixir of the Rapids
            76079, -- Elixir of Peace
            76080, -- Elixir of Perfection
            76081, -- Elixir of Mirrors
            76083, -- Monk's Elixir
            104111  -- Elixir of Wandering Spirits
        },
        [addon.CONS.C_FLASKS_ID] = {
            76084, -- Flask of Spring Blossoms
            76085, -- Flask of the Warm Sun
            76086, -- Flask of Falling Leaves
            76087, -- Flask of the Earth
            76088, -- Flask of Winter's Bite
            75525, -- Alchemist's Flask
            103557  -- Enduring Elixir of Wisdom
        },
        [addon.CONS.C_FOOD_DRINKS_ID] = {
            74646, -- Black Pepper Ribs and Shrimp
            74648, -- Sea Mist Rice Noodles
            74650, -- Mogu Fish Stew
            74653, -- Steamed Crab Surprise
            74656, -- Chun Tian Spring Rolls
            74919, -- Pandaren Banquet
            75016, -- Great Pandaren Banquet
            75038, -- Mad Brewer's Breakfast
            79320, -- Half a Lovely Apple
            80610, -- Conjured Mana Pudding
            80618, -- Conjured Mana Fritter
            81408, -- Red Bean Bun
            81409, -- Tangy Yogurt
            81410, -- Green Curry Fish
            81411, -- Peach Pie
            81412, -- Blanched Needle Mushrooms
            81413, -- Skewered Peanut Chicken
            81414, -- Pearl Milk Tea
            81415, -- Pandaren Plum Wine
            81916, -- Humongous Fungus
            81918, -- Pickled Pig's Snout
            81920, -- Plump Fig
            81921, -- Chilton Stilton
            81923, -- Cobo Cola
            82344, -- Hearthglen Ambrosia
            82449, -- Barnacle Bouillabaisse
            82451, -- Frybread
            83094, -- Foote Tripel
            86073, -- Spicy Salmon
            86074, -- Spicy Vegetable Chips
            86508, -- Fresh Bread
            87226, -- Banquet of the Grill
            87228, -- Great Banquet of the Grill
            87230, -- Banquet of the Wok
            87232, -- Great Banquet of the Wok
            87234, -- Banquet of the Pot
            87236, -- Great Banquet of the Pot
            87238, -- Banquet of the Steamer
            87240, -- Great Banquet of the Steamer
            87242, -- Banquet of the Oven
            87244, -- Great Banquet of the Oven
            87246, -- Banquet of the Brew
            87248, -- Great Banquet of the Brew
            87253, -- Perpetual Leftovers
            90135, -- Thick-Cut Bacon
            94535, -- Grilled Dinosaur Haunch
            98111, -- K.R.E.
            98116, -- Freeze-Dried Hyena Jerky
            98118, -- Scorpion Crunchies
            98121, -- Amberseed Bun
            98122, -- Camembert du Clefthoof
            98123, -- Whale Shark Caviar
            98124, -- Bloodberry Tart
            98125, -- Shaved Zangar Truffles
            98126, -- Mechanopeep Foie Gras
            98127, -- Dented Can of Kaja'Cola
            101630, -- Noodle Cart Kit
            101661, -- Deluxe Noodle Cart Kit
            101662, -- Pandaren Treasure Noodle Cart Kit
            101745, -- Mango Ice
            101746, -- Seasoned Pomfruit Slices
            101747, -- Farmer's Delight
            101748, -- Spiced Blossom Soup
            101749, -- Stuffed Lushrooms
            101750, -- Fluffy Silkfeather Omelet
            104339, -- Harmonious River Noodles
            104340, -- Crazy Snake Noodles
            104341, -- Steaming Goat Noodles
            104342, -- Spicy Mushan Noodles
            104343, -- Golden Dragon Noodles
            104344, -- Lucky Mushroom Noodles
            105717, -- Rice-Wine Mushrooms
            105719, -- Brew-Curried Whitefish
            105720, -- Candied Apple
            105721, -- Hot Papaya Milk
            105722, -- Nutty Brew-Bun
            105723, -- Peanut Pork Chops
            105724, -- Fried Cheese Dumplings
            108920, -- Lemon Flower Pudding
            74645, -- Eternal Blossom Fish
            74647, -- Valley Stir Fry
            74649, -- Braised Turtle
            74652, -- Fire Spirit Salmon
            74655, -- Twin Fish Platter
            75037, -- Jade Witch Brew
            86069, -- Rice Pudding
            86070, -- Wildfowl Ginseng Soup
            86432, -- Banana Infused Rum
            87264, -- Four Senses Brew
            74636, -- Golden Carp Consomme
            74641, -- Fish Cake
            74642, -- Charbroiled Tiger Steak
            74643, -- Sauteed Carrots
            74644, -- Swirling Mist Soup
            74651, -- Shrimp Dumplings
            74654, -- Wildfowl Roast
            75026, -- Ginseng Tea
            81400, -- Pounded Rice Cake
            81401, -- Yak Cheese Curds
            81402, -- Toasted Fish Jerky
            81403, -- Dried Peaches
            81404, -- Dried Needle Mushrooms
            81405, -- Boiled Silkworm Pupa
            81406, -- Roasted Barley Tea
            85501, -- Viseclaw Soup
            85504, -- Krasarang Fritters
            86026, -- Perfectly Cooked Instant Noodles
            86057, -- Sliced Peaches
            104348, -- Timeless Tea
            105700, -- Kun-Lai Kicker
            105701, -- Greenstone Brew
            105702, -- Boomer Brew
            105703, -- Stormstout Brew
            105704, -- Yan-Zhu's Blazing Brew
            105705, -- Chani's Bitter Brew
            105706, -- Shado-Pan Brew
            105707, -- Unga Brew
            105708, -- Shimmering Amber-Brew
            105711, -- Funky Monkey Brew
            81175, -- Crispy Dojani Eel
            81407, -- Four Wind Soju
            81889, -- Peppered Puffball
            81917, -- Mutton Stew
            81919, -- Red Raspberry
            81922, -- Redridge Roquefort
            81924, -- Carbonated Water
            82343, -- Lordaeron Lambic
            82448, -- Smoked Squid Belly
            82450, -- Cornmeal Biscuit
            83095, -- Lagrave Stout
            83097, -- Tortoise Jerky
            88379, -- Grummlecake
            88382, -- Keenbean Kafa
            88388, -- Squirmy Delight
            88398, -- Root Veggie Stew
            88490, -- Triple-Distilled Brew
            88492, -- Wicked Wikket
            88529, -- Sparkling Water
            88530, -- Bubbling Beverage
            88532, -- Lotus Water
            88578, -- Cup of Kafa
            88586, -- Chao Cookies
            89594, -- Serpent Brew of Serenity
            89601, -- Tiger Brew of Serenity
            89683, -- Hozen Cuervo
            90457, -- Mah's Warm Yak-Tail Stew
            101616, -- Noodle Soup
            101617, -- Deluxe Noodle Soup
            101618, -- Pandaren Treasure Noodle Soup
            103641, -- Singing Crystal
            103642, -- Book of the Ages
            103643, -- Dew of Eternal Morning
            89593, -- Serpent Brew of Fallen Blossoms
            89600, -- Tiger Brew of Fallen Blossoms
            89592, -- Serpent Brew of Meditation
            89599, -- Tiger Brew of Meditation
            89591, -- Serpent Brew of Pilgrimage
            89598, -- Tiger Brew of Pilgrimage
            89590, -- Serpent Brew of Adversity
            89597, -- Tiger Brew of Adversity
            89589, -- Initiate's Serpent Brew
            89596, -- Initiate's Tiger Brew
            89588, -- Novice's Serpent Brew
            89595, -- Novice's Tiger Brew
            90660, -- Black Tea
            90659, -- Jasmine Tea
            77264, -- Small Bamboo Shoot
            77272, -- Small Sugarcane Stalk
            77273, -- Small Rice Cake
            93208, -- Darkmoon P.I.E.
            98157, -- Big Blossom Brew
            104314, -- Dragon P.I.E.
            104316, -- Spectral Grog
            80313, -- Ling-Ting's Favorite Tea
            88531  -- Lao Chin's Last Mug
        },
        [addon.CONS.C_ITEM_ENHANCEMENTS_ID] = {
            82442, -- Pearlescent Spellthread
            82443, -- Cerulean Spellthread
            86597, -- Living Steel Weapon Chain
            85568, -- Brutal Leg Armor
            85569, -- Sha-Touched Leg Armor
            85570, -- Toughened Leg Armor
            77529, -- Lord Blastington's Scope of Doom
            77531, -- Mirror Scope
            82444, -- Greater Pearlescent Spellthread
            82445, -- Greater Cerulean Spellthread
            83763, -- Ironscale Leg Armor
            83764, -- Shadowleather Leg Armor
            83765, -- Angerhide Leg Armor
            87577, -- Ox Horn Inscription
            87578, -- Crane Wing Inscription
            87579, -- Tiger Claw Inscription
            87580, -- Tiger Fang Inscription
            95349, -- Enchant Weapon - Glorious Tyranny
            98163, -- Enchant Weapon - Bloody Dancing Steel
            74723, -- Enchant Weapon - Windsong
            74724, -- Enchant Weapon - Jade Spirit
            74725, -- Enchant Weapon - Elemental Force
            74726, -- Enchant Weapon - Dancing Steel
            74727, -- Enchant Weapon - Colossus
            74728, -- Enchant Weapon - River's Song
            98164, -- Enchant Weapon - Spirit of Conquest
            83006, -- Greater Tiger Fang Inscription
            83007, -- Greater Tiger Claw Inscription
            87559, -- Greater Crane Wing Inscription
            87560, -- Greater Ox Horn Inscription
            87581, -- Secret Ox Horn Inscription
            87582, -- Secret Crane Wing Inscription
            87584, -- Secret Tiger Claw Inscription
            87585  -- Secret Tiger Fang Inscription
        },
        [addon.CONS.C_POTIONS_ID] = {
            76089, -- Virmen's Bite
            76090, -- Potion of the Mountains
            76092, -- Potion of Focus
            76093, -- Potion of the Jade Serpent
            76094, -- Alchemist's Rejuvenation
            76095, -- Potion of Mogu Power
            76096, -- Darkwater Potion
            76097, -- Master Healing Potion
            76098, -- Master Mana Potion
            93742, -- Healing Potion
            95054, -- Potion of Light Steps
            95055, -- Frost Rune Trap
            97156, -- Frost Rune Trap
            97157, -- Potion of Light Steps
            86569  -- Crystal of Insanity
        },
        [addon.CONS.C_OTHER_ID] = {
            83137, -- Hozen Remedy Pouch
            93043, -- Rotten Apple
            93044, -- Rotten Banana
            93045, -- Rotten Watermelon
            93158, -- Expired Blackout Brew
            95052, -- Arcane Propellant
            95053, -- A Common Rock
            95056, -- Polymorphic Key
            95093, -- Sleep Dust
            97154, -- Sleep Dust
            97155, -- Polymorphic Key
            97158, -- A Common Rock
            97159, -- Arcane Propellant
            98558, -- Empty Supply Crate
            98559, -- Empty Supply Crate
            104112, -- Curious Ticking Parcel
            104114, -- Curious Ticking Parcel
            83795, -- Scrying Roguestone
            86607, -- Goblin Dragon Gun, Mark II
            87872, -- Desecrated Oil
            87647, -- Origami Crane
            87648, -- Origami Frog
            88375, -- Turnip Punching Bag
            88384, -- Burlap Ritual Bag
            88487, -- Volatile Orb
            89223, -- Racing Flag
            89224, -- Floating Racing Flag
            89225, -- Finish Line
            89227, -- Floating Finish Line
            89230, -- Restorative Amber
            89301, -- Stack of Wooden Boards
            89302, -- Stack of Bamboo
            89303, -- Stack of Stone Blocks
            85580, -- Empty Polyformic Acid Vial
            85589, -- Nearly Full Vial of Polyformic Acid
            85592, -- Half Full Vial of Polyformic Acid
            85593, -- Nearly Empty Vial of Polyformic Acid
            90918, -- Celebration Package
            98117, -- Moneybrau
            74812, -- Diluted Lime Sulfur
            74813, -- Dogwood Extract
            74815, -- Soothing Shampoo
            75208, -- Rancher's Lariat
            75256, -- Pang's Extra-Spicy Tofu
            75258, -- Ang's Summer Watermelon
            75259, -- Ang's Giant Pink Turnip
            75271, -- Jian
            75272, -- Ling
            75273, -- Smelly
            75275, -- Mushan Shoulder Steak
            75276, -- Turtle Meat Scrap
            76100, -- Wayward Lamb
            76110, -- Crate of Fresh Produce
            76297, -- Stolen Turnip
            76298, -- Stolen Watermelon
            76334, -- Meadow Marigold
            76335, -- Vial of Animal Blood
            76336, -- Nazgrim's Grog
            76337, -- Stolen Sack of Hops
            76350, -- Li Li's Wishing-Stone
            76356, -- Stoppered Vial of Muddy Water
            76362, -- Mudmug's Vial
            76370, -- Orange-Painted Turnip
            76499, -- Jademoon Leaf
            79048, -- Whimsical Skull Mask
            79102, -- Green Cabbage Seeds
            79338, -- Bucket of Meaty Dog Food
            80590, -- Juicycrunch Carrot Seeds
            80591, -- Scallion Seeds
            80592, -- Mogu Pumpkin Seeds
            80593, -- Red Blossom Leek Seeds
            80594, -- Pink Turnip Seeds
            80595, -- White Turnip Seeds
            80809, -- Bag of Green Cabbage Seeds
            81054, -- Kafa'kota Berry
            81177, -- Pandaren Healing Draught
            81901, -- Brilliant Mana Gem
            82392, -- Gumweed
            82467, -- Ruthers' Harness
            82605, -- Corrupted Insignia
            84686, -- Mug of Dreadbrew
            84782, -- Bag of Juicycrunch Carrot Seeds
            84783, -- Bag of Scallion Seeds
            85153, -- Bag of Mogu Pumpkin Seeds
            85158, -- Bag of Red Blossom Leek Seeds
            85162, -- Bag of Pink Turnip Seeds
            85163, -- Bag of White Turnip Seeds
            85164, -- Elegant Scroll
            85215, -- Snakeroot Seed
            85216, -- Enigma Seed
            85217, -- Magebulb Seed
            85869, -- Potion of Mazu's Breath
            85955, -- Dog's Whistle
            86534, -- Shiny Shado-Pan Coin
            86536, -- Wu Kao Dart of Lethargy
            87257, -- Arcane Familiar Stone
            87258, -- Fiery Familiar Stone
            87259, -- Icy Familiar Stone
            87764, -- Serpent's Heart Firework
            88491, -- Grand Celebration Firework
            88493, -- Celestial Firework
            89197, -- Windshear Cactus Seed
            89202, -- Raptorleaf Seed
            89233, -- Songbell Seed
            89326, -- Witchberry Seeds
            89328, -- Jade Squash Seeds
            89329, -- Striped Melon Seeds
            89847, -- Bag of Witchberry Seeds
            89848, -- Bag of Jade Squash Seeds
            89849, -- Bag of Striped Melon Seeds
            89888, -- Jade Blossom Firework
            89893, -- Autumn Flower Firework
            90006, -- Wu Kao Smoke Bomb
            90174, -- Troubles From Without
            90426, -- Brewhelm
            90428, -- Pandaren Brew
            91806, -- Unstable Portal Shard
            91850, -- Orgrimmar Portal Shard
            91860, -- Stormwind Portal Shard
            91861, -- Thunder Bluff Portal Shard
            91862, -- Undercity Portal Shard
            91863, -- Silvermoon Portal Shard
            91864, -- Ironforge Portal Shard
            91865, -- Darnassus Portal Shard
            91866, -- Exodar Portal Shard
            92049, -- Mercenary Contract: Mage
            92050, -- Mercenary Contract: Paladin
            92051, -- Mercenary Contract: Priest
            92052, -- Mercenary Contract: Druid
            92053, -- Mercenary Contract: Warrior
            92054, -- Mercenary Contract: Rogue
            92055, -- Mercenary Contract: Shaman
            92056, -- Portal Fuel: Sparkrocket Outpost
            92057, -- Portal Fuel: Orgrimmar
            92058, -- Portal Fuel: Shrine of Two Moons
            92059, -- Domination Point Banquet
            92421, -- Guard Contract: Graveyard
            92422, -- Guard Contract: Tower
            92427, -- Guard Contract: Tower
            92428, -- Guard Contract: Graveyard
            92429, -- Lion's Landing Banquet
            92430, -- Portal Reagents: Shrine of Seven Stars
            92431, -- Portal Reagents: Stormwind
            92432, -- Portal Reagents: Skyfire
            92433, -- Mercenary Contract: Shaman
            92434, -- Mercenary Contract: Rogue
            92435, -- Mercenary Contract: Warrior
            92436, -- Mercenary Contract: Druid
            92437, -- Mercenary Contract: Priest
            92438, -- Mercenary Contract: Paladin
            92439, -- Mercenary Contract: Mage
            92442, -- Horde Banner
            92443, -- Alliance Banner
            92525, -- Overridden Excavationbot
            92526, -- Crate of Horde Banners
            92527, -- Rodent Crate
            92528, -- Obelisk of Deception
            92529, -- Mind Vision Altar
            92530, -- Box of Overridden Excavationbots
            92531, -- Crate of Alliance Banners
            92532, -- Rodent Crate
            92533, -- Obelisk of Deception
            92534, -- Mind Vision Altar
            92535, -- Box of Overridden Excavationbots
            92663, -- Guard Contract: Outpost
            92664, -- Guard Contract: Outpost
            92718, -- Brawler's Purse
            92763, -- Kor'kron Helmet
            92956, -- Darkmoon "Snow Leopard"
            92958, -- Darkmoon "Nightsaber"
            92959, -- Darkmoon "Cougar"
            92966, -- Darkmoon "Dragon"
            92967, -- Darkmoon "Gryphon"
            92968, -- Darkmoon "Murloc"
            92969, -- Darkmoon "Rocket"
            92970, -- Darkmoon "Wyvern"
            93314, -- Magic Bamboo Shoot
            93730, -- Darkmoon Top Hat
            93823, -- Challenge Card: Bruce
            93824, -- Challenge Card: Vian
            94160, -- Challenge Card: Goredome
            94161, -- Challenge Card: Vishas
            94162, -- Challenge Card: Dippy
            94163, -- Challenge Card: Kirrawk
            94164, -- Challenge Card: Fran & Riddoh
            94165, -- Challenge Card: King Kulaka
            94166, -- Challenge Card: Blat
            94167, -- Challenge Card: Sanoriak
            94168, -- Challenge Card: Ixx
            94169, -- Challenge Card: Mazhareen
            94170, -- Challenge Card: Crush
            94171, -- Challenge Card: Leona
            94172, -- Challenge Card: Dominika
            94173, -- Challenge Card: Deeken
            94174, -- Challenge Card: Millie Watt
            94175, -- Challenge Card: Fjoll
            94176, -- Challenge Card: Proboskus
            94177, -- Challenge Card: Leper Gnomes
            94178, -- Challenge Card: G.G. Engineering
            94179, -- Challenge Card: Dark Summoner
            94180, -- Challenge Card: Battletron
            94181, -- Challenge Card: Meatball
            94182, -- Challenge Card: Epicus Maximus
            94183, -- Challenge Card: Yikkan Izu
            94184, -- Challenge Card: Akama
            94185, -- Challenge Card: Smash Hoofstomp
            94186, -- Challenge Card: Unguloxx
            94187, -- Challenge Card: Disruptron
            94188, -- Challenge Card: Millhouse Manastorm
            94189, -- Challenge Card: Zen'shar
            95408, -- Waterlogged Zandalari Journal
            95409, -- Iron-Bound Zandalari Journal
            95410, -- Blood-Spattered Zandalari Journal
            95411, -- Torn Zandalari Journal
            95412, -- Frayed Zandalari Journal
            95434, -- Bag of Green Cabbage Seeds
            95436, -- Bag of Juicycrunch Carrot Seeds
            95437, -- Bag of Jade Squash Seeds
            95438, -- Bag of Mogu Pumpkin Seeds
            95439, -- Bag of Pink Turnip Seeds
            95440, -- Bag of Red Blossom Leek Seeds
            95441, -- Bag of Scallion Seeds
            95442, -- Bag of Striped Melon Seeds
            95443, -- Bag of White Turnip Seeds
            95444, -- Bag of Witchberry Seeds
            95445, -- Bag of Songbell Seeds
            95446, -- Bag of Songbell Seeds
            95447, -- Bag of Snakeroot Seeds
            95448, -- Bag of Snakeroot Seeds
            95449, -- Bag of Enigma Seeds
            95450, -- Bag of Enigma Seeds
            95451, -- Bag of Magebulb Seeds
            95452, -- Bag of Magebulb Seeds
            95454, -- Bag of Windshear Cactus Seeds
            95456, -- Bag of Windshear Cactus Seeds
            95457, -- Bag of Raptorleaf Seeds
            95458, -- Bag of Raptorleaf Seeds
            95481, -- Blue War Fuel
            95482, -- Red War Fuel
            97278, -- Challenge Card: Ahoo'ru
            97279, -- Challenge Card: Mingus Diggs
            97280, -- Challenge Card: Dippy & Doopy
            97281, -- Challenge Card: Hexos
            97282, -- Challenge Card: Ty'thar
            97283, -- Challenge Card: Nibbleh
            97284, -- Challenge Card: Master Boom Boom
            97285, -- Challenge Card: Grandpa Grumplefloot
            97286, -- Challenge Card: Big Badda Boom
            97287, -- Challenge Card: The Bear and the Lady Fair
            97288, -- Challenge Card: Doctor FIST
            97289, -- Challenge Card: Splat
            97321, -- Challenge Card: Blind Hero
            97445, -- Challenge Card: Blingtron 3000
            97450, -- Challenge Card: Tyson Sanders
            97559, -- Challenge Card: T440 Dual-Mode Robot
            97560, -- Challenge Card: Ro-Shambo
            97563, -- Challenge Card: Mecha-Bruce
            97566, -- Challenge Card: Razorgrin
            97972, -- Challenge Card: Anthracite
            98112, -- Lesser Pet Treat
            103631, -- Lucky Path of Cenarius
            104140, -- Horde Firework
            104141, -- Alliance Firework
            104142, -- Autumn Flower Firework
            104143, -- Jade Blossom Firework
            104144, -- Celestial Firework
            104145, -- Grand Celebration Firework
            104146, -- Serpent's Heart Firework
            104148, -- Snake Burst Firework
            104149, -- Red Firework
            104150, -- Red, White and Blue Firework
            104151, -- Yellow Rose Firework
            104152, -- Red Streaks Firework
            104153, -- Green Firework
            104154, -- Blue Firework
            104155, -- Red Fireworks Rocket
            107499, -- Mulled Alterac Brandy
            102351, -- Drums of Rage
            82960, -- Ghostly Skeleton Key
            94604, -- Burning Seed
            85264, -- Autumn Blossom Tree
            85265, -- Spring Blossom Tree
            85266, -- Winter Blossom Tree
            85267, -- Autumn Blossom Sapling
            85268, -- Spring Blossom Sapling
            85269, -- Winter Blossom Sapling
            89124, -- Celestial Offering
            104287, -- Windfeather Plume
            104288, -- Condensed Jademist
            104289, -- Faintly-Glowing Herb
            104290, -- Sticky Silkworm Goo
            104312, -- Strange Glowing Mushroom
            85973, -- Ancient Pandaren Fishing Charm
            89614, -- Anatomical Dummy
            90395, -- Facets of Research
            90397, -- Facets of Research
            90398, -- Facets of Research
            90399, -- Facets of Research
            90400, -- Facets of Research
            90401, -- Facets of Research
            90406, -- Facets of Research
            90815, -- Relic of Guo-Lai
            94223, -- Stolen Shado-Pan Insignia
            94225, -- Stolen Celestial Insignia
            94226, -- Stolen Klaxxi Insignia
            94227, -- Stolen Golden Lotus Insignia
            95487, -- Sunreaver Onslaught Insignia
            95488, -- Greater Sunreaver Onslaught Insignia
            95489, -- Kirin Tor Offensive Insignia
            95490, -- Greater Kirin Tor Offensive Insignia
            95496, -- Shado-Pan Assault Insignia
            95589, -- Glorious Standard of the Kirin Tor Offensive
            95590, -- Glorious Standard of the Sunreaver Onslaught
            88370, -- Puntable Marmot
            88377, -- Turnip Paint "Gun"
            88381, -- Silversage Incense
            88385, -- Hozen Idol
            88387, -- Shushen's Spittoon
            88579, -- Jin Warmkeg's Brew
            88580, -- Ken-Ken's Mask
            88584, -- Totem of Harmony
            88589, -- Cremating Torch
            88801, -- Flippable Table
            88802, -- Foxicopter Controller
            89222, -- Cloud Ring
            91904, -- Stackable Stag
            89999, -- Everlasting Alliance Firework
            90000, -- Everlasting Horde Firework
            85219, -- Ominous Seed
            86574, -- Elixir of Ancient Knowledge
            86592, -- Hozen Peace Pipe
            88417, -- Gokk'lok's Shell
            89869, -- Pandaren Scarecrow
            90427, -- Pandaren Brewpack
            94295, -- Primal Egg
            94296, -- Cracked Primal Egg
            97994, -- Darkmoon Seesaw
            98549, -- Iron Hitching Post (UNUSED)
            98552, -- Xan'tish's Flute
            101571, -- Moonfang Shroud
            105898, -- Moonfang's Paw
            90816, -- Relic of the Thunder King
            92522, -- Grand Commendation of the Klaxxi
            93215, -- Grand Commendation of the Golden Lotus
            93220, -- Grand Commendation of the Shado-Pan
            93224, -- Grand Commendation of the August Celestials
            93225, -- Grand Commendation of the Anglers
            93226, -- Grand Commendation of the Tillers
            93229, -- Grand Commendation of the Order of the Cloud Serpent
            93230, -- Grand Commendation of the Lorewalkers
            93231, -- Grand Commendation of Operation: Shieldwall
            93232, -- Grand Commendation of the Dominance Offensive
            95545, -- Grand Commendation of the Kirin Tor Offensive
            95548  -- Grand Commendation of the Sunreaver Onslaught
        }
    },
    [addon.CONS.GEMS_ID] = {
        76502, -- Rigid Lapis Lazuli
        76504, -- Stormy Lapis Lazuli
        76505, -- Sparkling Lapis Lazuli
        76506, -- Solid Lapis Lazuli
        76507, -- Misty Alexandrite
        76508, -- Piercing Alexandrite
        76509, -- Lightning Alexandrite
        76510, -- Sensei's Alexandrite
        76511, -- Effulgent Alexandrite
        76512, -- Zen Alexandrite
        76513, -- Balanced Alexandrite
        76514, -- Vivid Alexandrite
        76515, -- Turbid Alexandrite
        76517, -- Radiant Alexandrite
        76518, -- Shattered Alexandrite
        76519, -- Energized Alexandrite
        76520, -- Jagged Alexandrite
        76521, -- Regal Alexandrite
        76522, -- Forceful Alexandrite
        76524, -- Puissant Alexandrite
        76525, -- Steady Alexandrite
        76526, -- Deadly Tiger Opal
        76527, -- Crafty Tiger Opal
        76528, -- Potent Tiger Opal
        76529, -- Inscribed Tiger Opal
        76530, -- Polished Tiger Opal
        76531, -- Resolute Tiger Opal
        76532, -- Stalwart Tiger Opal
        76533, -- Champion's Tiger Opal
        76534, -- Deft Tiger Opal
        76535, -- Wicked Tiger Opal
        76536, -- Reckless Tiger Opal
        76537, -- Fierce Tiger Opal
        76538, -- Adept Tiger Opal
        76539, -- Keen Tiger Opal
        76540, -- Artful Tiger Opal
        76541, -- Fine Tiger Opal
        76542, -- Skillful Tiger Opal
        76543, -- Lucent Tiger Opal
        76544, -- Tenuous Tiger Opal
        76545, -- Willful Tiger Opal
        76546, -- Splendid Tiger Opal
        76547, -- Resplendent Tiger Opal
        76548, -- Glinting Roguestone
        76549, -- Accurate Roguestone
        76550, -- Veiled Roguestone
        76551, -- Retaliating Roguestone
        76552, -- Etched Roguestone
        76553, -- Mysterious Roguestone
        76554, -- Purified Roguestone
        76555, -- Shifting Roguestone
        76556, -- Guardian's Roguestone
        76557, -- Timeless Roguestone
        76558, -- Defender's Roguestone
        76559, -- Sovereign Roguestone
        76560, -- Delicate Pandarian Garnet
        76561, -- Precise Pandarian Garnet
        76562, -- Brilliant Pandarian Garnet
        76563, -- Flashing Pandarian Garnet
        76564, -- Bold Pandarian Garnet
        76565, -- Smooth Sunstone
        76566, -- Subtle Sunstone
        76567, -- Quick Sunstone
        76568, -- Fractured Sunstone
        76569, -- Mystic Sunstone
        89675, -- Tense Roguestone
        89678, -- Assassin's Roguestone
        93706, -- Nimble Alexandrite
        76636, -- Rigid River's Heart
        76637, -- Stormy River's Heart
        76638, -- Sparkling River's Heart
        76639, -- Solid River's Heart
        76640, -- Misty Wild Jade
        76641, -- Piercing Wild Jade
        76642, -- Lightning Wild Jade
        76643, -- Sensei's Wild Jade
        76644, -- Effulgent Wild Jade
        76645, -- Zen Wild Jade
        76646, -- Balanced Wild Jade
        76647, -- Vivid Wild Jade
        76648, -- Turbid Wild Jade
        76649, -- Radiant Wild Jade
        76650, -- Shattered Wild Jade
        76651, -- Energized Wild Jade
        76652, -- Jagged Wild Jade
        76653, -- Regal Wild Jade
        76654, -- Forceful Wild Jade
        76655, -- Confounded Wild Jade
        76656, -- Puissant Wild Jade
        76657, -- Steady Wild Jade
        76658, -- Deadly Vermilion Onyx
        76659, -- Crafty Vermilion Onyx
        76660, -- Potent Vermilion Onyx
        76661, -- Inscribed Vermilion Onyx
        76662, -- Polished Vermilion Onyx
        76663, -- Resolute Vermilion Onyx
        76664, -- Stalwart Vermilion Onyx
        76665, -- Champion's Vermilion Onyx
        76666, -- Deft Vermilion Onyx
        76667, -- Wicked Vermilion Onyx
        76668, -- Reckless Vermilion Onyx
        76669, -- Fierce Vermilion Onyx
        76670, -- Adept Vermilion Onyx
        76671, -- Keen Vermilion Onyx
        76672, -- Artful Vermilion Onyx
        76673, -- Fine Vermilion Onyx
        76674, -- Skillful Vermilion Onyx
        76675, -- Lucent Vermilion Onyx
        76676, -- Tenuous Vermilion Onyx
        76677, -- Willful Vermilion Onyx
        76678, -- Splendid Vermilion Onyx
        76679, -- Resplendent Vermilion Onyx
        76680, -- Glinting Imperial Amethyst
        76681, -- Accurate Imperial Amethyst
        76682, -- Veiled Imperial Amethyst
        76683, -- Retaliating Imperial Amethyst
        76684, -- Etched Imperial Amethyst
        76685, -- Mysterious Imperial Amethyst
        76686, -- Purified Imperial Amethyst
        76687, -- Shifting Imperial Amethyst
        76688, -- Guardian's Imperial Amethyst
        76689, -- Timeless Imperial Amethyst
        76690, -- Defender's Imperial Amethyst
        76691, -- Sovereign Imperial Amethyst
        76692, -- Delicate Primordial Ruby
        76693, -- Precise Primordial Ruby
        76694, -- Brilliant Primordial Ruby
        76695, -- Flashing Primordial Ruby
        76696, -- Bold Primordial Ruby
        76697, -- Smooth Sun's Radiance
        76698, -- Subtle Sun's Radiance
        76699, -- Quick Sun's Radiance
        76700, -- Fractured Sun's Radiance
        76701, -- Mystic Sun's Radiance
        76879, -- Ember Primal Diamond
        76884, -- Agile Primal Diamond
        76885, -- Burning Primal Diamond
        76886, -- Reverberating Primal Diamond
        76887, -- Fleet Primal Diamond
        76888, -- Revitalizing Primal Diamond
        76890, -- Destructive Primal Diamond
        76891, -- Powerful Primal Diamond
        76892, -- Enigmatic Primal Diamond
        76893, -- Impassive Primal Diamond
        76894, -- Forlorn Primal Diamond
        76895, -- Austere Primal Diamond
        76896, -- Eternal Primal Diamond
        76897, -- Effulgent Primal Diamond
        77540, -- Subtle Tinker's Gear
        77541, -- Smooth Tinker's Gear
        77542, -- Quick Tinker's Gear
        77543, -- Precise Tinker's Gear
        77544, -- Flashing Tinker's Gear
        77545, -- Rigid Tinker's Gear
        77546, -- Sparkling Tinker's Gear
        77547, -- Fractured Tinker's Gear
        89674, -- Tense Imperial Amethyst
        89680, -- Assassin's Imperial Amethyst
        93705, -- Nimble Wild Jade
        76570, -- Perfect Rigid Lapis Lazuli
        76571, -- Perfect Stormy Lapis Lazuli
        76572, -- Perfect Sparkling Lapis Lazuli
        76573, -- Perfect Solid Lapis Lazuli
        76574, -- Perfect Misty Alexandrite
        76575, -- Perfect Piercing Alexandrite
        76576, -- Perfect Lightning Alexandrite
        76577, -- Perfect Sensei's Alexandrite
        76578, -- Perfect Effulgent Alexandrite
        76579, -- Perfect Zen Alexandrite
        76580, -- Perfect Balanced Alexandrite
        76581, -- Perfect Vivid Alexandrite
        76582, -- Perfect Turbid Alexandrite
        76583, -- Perfect Radiant Alexandrite
        76584, -- Perfect Shattered Alexandrite
        76585, -- Perfect Energized Alexandrite
        76586, -- Perfect Jagged Alexandrite
        76587, -- Perfect Regal Alexandrite
        76588, -- Perfect Forceful Alexandrite
        76589, -- Perfect Confounded Alexandrite
        76590, -- Perfect Puissant Alexandrite
        76591, -- Perfect Steady Alexandrite
        76592, -- Perfect Deadly Tiger Opal
        76593, -- Perfect Crafty Tiger Opal
        76594, -- Perfect Potent Tiger Opal
        76595, -- Perfect Inscribed Tiger Opal
        76596, -- Perfect Polished Tiger Opal
        76597, -- Perfect Resolute Tiger Opal
        76598, -- Perfect Stalwart Tiger Opal
        76599, -- Perfect Champion's Tiger Opal
        76600, -- Perfect Deft Tiger Opal
        76601, -- Perfect Wicked Tiger Opal
        76602, -- Perfect Reckless Tiger Opal
        76603, -- Perfect Fierce Tiger Opal
        76604, -- Perfect Adept Tiger Opal
        76605, -- Perfect Keen Tiger Opal
        76606, -- Perfect Artful Tiger Opal
        76607, -- Perfect Fine Tiger Opal
        76608, -- Perfect Skillful Tiger Opal
        76609, -- Perfect Lucent Tiger Opal
        76610, -- Perfect Tenuous Tiger Opal
        76611, -- Perfect Willful Tiger Opal
        76612, -- Perfect Splendid Tiger Opal
        76613, -- Perfect Resplendent Tiger Opal
        76614, -- Perfect Glinting Roguestone
        76615, -- Perfect Accurate Roguestone
        76616, -- Perfect Veiled Roguestone
        76617, -- Perfect Retaliating Roguestone
        76618, -- Perfect Etched Roguestone
        76619, -- Perfect Mysterious Roguestone
        76620, -- Perfect Purified Roguestone
        76621, -- Perfect Shifting Roguestone
        76622, -- Perfect Guardian's Roguestone
        76623, -- Perfect Timeless Roguestone
        76624, -- Perfect Defender's Roguestone
        76625, -- Perfect Sovereign Roguestone
        76626, -- Perfect Delicate Pandarian Garnet
        76627, -- Perfect Precise Pandarian Garnet
        76628, -- Perfect Brilliant Pandarian Garnet
        76629, -- Perfect Flashing Pandarian Garnet
        76630, -- Perfect Bold Pandarian Garnet
        76631, -- Perfect Smooth Sunstone
        76632, -- Perfect Subtle Sunstone
        76633, -- Perfect Quick Sunstone
        76634, -- Perfect Fractured Sunstone
        76635, -- Perfect Mystic Sunstone
        89676, -- Perfect Tense Roguestone
        93707, -- Perfect Nimble Alexandrite
        89679, -- Perfect Assassin's Roguestone
        83141, -- Bold Serpent's Eye
        83142, -- Quick Serpent's Eye
        83143, -- Fractured Serpent's Eye
        83144, -- Rigid Serpent's Eye
        83145, -- Subtle Serpent's Eye
        83146, -- Smooth Serpent's Eye
        83147, -- Precise Serpent's Eye
        83148, -- Solid Serpent's Eye
        83149, -- Sparkling Serpent's Eye
        83150, -- Brilliant Serpent's Eye
        83151, -- Delicate Serpent's Eye
        83152, -- Flashing Serpent's Eye
        93404, -- Resplendent Serpent's Eye
        93405, -- Lucent Serpent's Eye
        93406, -- Willful Serpent's Eye
        93408, -- Tense Serpent's Eye
        93409, -- Assassin's Serpent's Eye
        93410, -- Mysterious Serpent's Eye
        95348  -- Tyrannical Primal Diamond
    },
    [addon.CONS.GLYPHS_ID] = {
        79538, -- Glyph of the Heavens
        80587, -- Glyph of Hawk Feast
        80588, -- Glyph of Burning Anger
        85221, -- Glyph of the Blazing Trail
        87277, -- Glyph of the Val'kyr
        87392, -- Glyph of Shadowy Friends
        87881, -- Glyph of Crackling Tiger Lightning
        87883, -- Glyph of Honor
        87885, -- Glyph of Rising Tiger Kick
        87888, -- Glyph of Fighting Pose
        89868, -- Glyph of the Cheetah
        104099, -- Glyph of the Skeleton
        104104, -- Glyph of the Unbound Elemental
        104105, -- Glyph of Evaporation
        104108, -- Glyph of Pillar of Light
        104120, -- Glyph of the Sha
        104122, -- Glyph of Inspired Hymns
        104126, -- Glyph of Spirit Raptors
        104127, -- Glyph of Lingering Ancestors
        104138  -- Glyph of the Weaponmaster
    },
    [addon.CONS.KEYS_ID] = {
        82354  -- Ghost Iron Key
    },
    [addon.CONS.MISCELLANEOUS_ID] = {
        [addon.CONS.M_ARMOR_TOKENS_ID] = {
            99712, -- Leggings of the Cursed Conqueror
            99713, -- Leggings of the Cursed Protector
            99714, -- Chest of the Cursed Vanquisher
            99715, -- Chest of the Cursed Conqueror
            99716, -- Chest of the Cursed Protector
            99717, -- Shoulders of the Cursed Vanquisher
            99718, -- Shoulders of the Cursed Conqueror
            99719, -- Shoulders of the Cursed Protector
            99720, -- Gauntlets of the Cursed Vanquisher
            99721, -- Gauntlets of the Cursed Conqueror
            99722, -- Gauntlets of the Cursed Protector
            99723, -- Helm of the Cursed Vanquisher
            99724, -- Helm of the Cursed Conqueror
            99725, -- Helm of the Cursed Protector
            99726, -- Leggings of the Cursed Vanquisher
            105866, -- Essence of the Cursed Protector
            105867, -- Essence of the Cursed Conqueror
            105868, -- Essence of the Cursed Vanquisher
            99682, -- Gauntlets of the Cursed Vanquisher
            99683, -- Helm of the Cursed Vanquisher
            99684, -- Leggings of the Cursed Vanquisher
            99685, -- Shoulders of the Cursed Vanquisher
            99686, -- Chest of the Cursed Conqueror
            99687, -- Gauntlets of the Cursed Conqueror
            99688, -- Leggings of the Cursed Conqueror
            99689, -- Helm of the Cursed Conqueror
            99690, -- Shoulders of the Cursed Conqueror
            99691, -- Chest of the Cursed Protector
            99692, -- Gauntlets of the Cursed Protector
            99693, -- Leggings of the Cursed Protector
            99694, -- Helm of the Cursed Protector
            99695, -- Shoulders of the Cursed Protector
            99696, -- Chest of the Cursed Vanquisher
            105857, -- Essence of the Cursed Protector
            105858, -- Essence of the Cursed Conqueror
            105859, -- Essence of the Cursed Vanquisher
            99742, -- Chest of the Cursed Vanquisher
            99743, -- Chest of the Cursed Conqueror
            99744, -- Chest of the Cursed Protector
            99745, -- Gauntlets of the Cursed Vanquisher
            99746, -- Gauntlets of the Cursed Conqueror
            99747, -- Gauntlets of the Cursed Protector
            99748, -- Helm of the Cursed Vanquisher
            99749, -- Helm of the Cursed Conqueror
            99750, -- Helm of the Cursed Protector
            99751, -- Leggings of the Cursed Vanquisher
            99752, -- Leggings of the Cursed Conqueror
            99753, -- Leggings of the Cursed Protector
            99754, -- Shoulders of the Cursed Vanquisher
            99755, -- Shoulders of the Cursed Conqueror
            99756, -- Shoulders of the Cursed Protector
            105863, -- Essence of the Cursed Protector
            105864, -- Essence of the Cursed Conqueror
            105865, -- Essence of the Cursed Vanquisher
            96566, -- Chest of the Crackling Vanquisher
            96567, -- Chest of the Crackling Conqueror
            96568, -- Chest of the Crackling Protector
            96599, -- Gauntlets of the Crackling Vanquisher
            96600, -- Gauntlets of the Crackling Conqueror
            96601, -- Gauntlets of the Crackling Protector
            96623, -- Helm of the Crackling Vanquisher
            96624, -- Helm of the Crackling Conqueror
            96625, -- Helm of the Crackling Protector
            96631, -- Leggings of the Crackling Vanquisher
            96632, -- Leggings of the Crackling Conqueror
            96633, -- Leggings of the Crackling Protector
            96699, -- Shoulders of the Crackling Vanquisher
            96700, -- Shoulders of the Crackling Conqueror
            96701, -- Shoulders of the Crackling Protector
            99667, -- Gauntlets of the Cursed Protector
            99668, -- Shoulders of the Cursed Vanquisher
            99669, -- Shoulders of the Cursed Conqueror
            99670, -- Shoulders of the Cursed Protector
            99671, -- Helm of the Cursed Vanquisher
            99672, -- Helm of the Cursed Conqueror
            99673, -- Helm of the Cursed Protector
            99674, -- Leggings of the Cursed Vanquisher
            99675, -- Leggings of the Cursed Conqueror
            99676, -- Leggings of the Cursed Protector
            99677, -- Chest of the Cursed Vanquisher
            99678, -- Chest of the Cursed Conqueror
            99679, -- Chest of the Cursed Protector
            99680, -- Gauntlets of the Cursed Vanquisher
            99681, -- Gauntlets of the Cursed Conqueror
            105860, -- Essence of the Cursed Protector
            105861, -- Essence of the Cursed Conqueror
            105862, -- Essence of the Cursed Vanquisher
            95569, -- Chest of the Crackling Vanquisher
            95570, -- Gauntlets of the Crackling Vanquisher
            95571, -- Helm of the Crackling Vanquisher
            95572, -- Leggings of the Crackling Vanquisher
            95573, -- Shoulders of the Crackling Vanquisher
            95574, -- Chest of the Crackling Conqueror
            95575, -- Gauntlets of the Crackling Conqueror
            95576, -- Leggings of the Crackling Conqueror
            95577, -- Helm of the Crackling Conqueror
            95578, -- Shoulders of the Crackling Conqueror
            95579, -- Chest of the Crackling Protector
            95580, -- Gauntlets of the Crackling Protector
            95581, -- Leggings of the Crackling Protector
            95582, -- Helm of the Crackling Protector
            95583, -- Shoulders of the Crackling Protector
            89249, -- Chest of the Shadowy Vanquisher
            89250, -- Chest of the Shadowy Conqueror
            89251, -- Chest of the Shadowy Protector
            89252, -- Leggings of the Shadowy Vanquisher
            89253, -- Leggings of the Shadowy Conqueror
            89254, -- Leggings of the Shadowy Protector
            89255, -- Gauntlets of the Shadowy Vanquisher
            89256, -- Gauntlets of the Shadowy Conqueror
            89257, -- Gauntlets of the Shadowy Protector
            89258, -- Helm of the Shadowy Vanquisher
            89259, -- Helm of the Shadowy Conqueror
            89260, -- Helm of the Shadowy Protector
            89261, -- Shoulders of the Shadowy Vanquisher
            89262, -- Shoulders of the Shadowy Conqueror
            89263, -- Shoulders of the Shadowy Protector
            95822, -- Chest of the Crackling Vanquisher
            95823, -- Chest of the Crackling Conqueror
            95824, -- Chest of the Crackling Protector
            95855, -- Gauntlets of the Crackling Vanquisher
            95856, -- Gauntlets of the Crackling Conqueror
            95857, -- Gauntlets of the Crackling Protector
            95879, -- Helm of the Crackling Vanquisher
            95880, -- Helm of the Crackling Conqueror
            95881, -- Helm of the Crackling Protector
            95887, -- Leggings of the Crackling Vanquisher
            95888, -- Leggings of the Crackling Conqueror
            95889, -- Leggings of the Crackling Protector
            95955, -- Shoulders of the Crackling Vanquisher
            95956, -- Shoulders of the Crackling Conqueror
            95957, -- Shoulders of the Crackling Protector
            89234, -- Helm of the Shadowy Vanquisher
            89235, -- Helm of the Shadowy Conqueror
            89236, -- Helm of the Shadowy Protector
            89237, -- Chest of the Shadowy Conqueror
            89238, -- Chest of the Shadowy Protector
            89239, -- Chest of the Shadowy Vanquisher
            89240, -- Gauntlets of the Shadowy Conqueror
            89241, -- Gauntlets of the Shadowy Protector
            89242, -- Gauntlets of the Shadowy Vanquisher
            89243, -- Leggings of the Shadowy Conqueror
            89244, -- Leggings of the Shadowy Protector
            89245, -- Leggings of the Shadowy Vanquisher
            89246, -- Shoulders of the Shadowy Conqueror
            89247, -- Shoulders of the Shadowy Protector
            89248, -- Shoulders of the Shadowy Vanquisher
            89264, -- Chest of the Shadowy Vanquisher
            89265, -- Chest of the Shadowy Conqueror
            89266, -- Chest of the Shadowy Protector
            89267, -- Leggings of the Shadowy Vanquisher
            89268, -- Leggings of the Shadowy Conqueror
            89269, -- Leggings of the Shadowy Protector
            89270, -- Gauntlets of the Shadowy Vanquisher
            89271, -- Gauntlets of the Shadowy Conqueror
            89272, -- Gauntlets of the Shadowy Protector
            89273, -- Helm of the Shadowy Vanquisher
            89274, -- Helm of the Shadowy Conqueror
            89275, -- Helm of the Shadowy Protector
            89276, -- Shoulders of the Shadowy Vanquisher
            89277, -- Shoulders of the Shadowy Conqueror
            89278  -- Shoulders of the Shadowy Protector
        },
        [addon.CONS.M_HOLIDAY_ID] = {
            90883, -- The Pigskin
            90888, -- Foot Ball
            104323, -- The Pigskin
            104324, -- Foot Ball
            93626, -- Stolen Present
            104318, -- Crashin' Thrashin' Flyer Controller
            [addon.CONS.MH_DARKMOON_ID] = {
                93724  -- Darkmoon Game Prize
            }
        },
        [addon.CONS.M_MOUNTS_ID] = {
            101675, -- Shimmering Moonstone
            89682, -- Oddly-Shaped Horn
            89697, -- Bag of Kafa Beans
            89770, -- Tuft of Yak Fur
            83087, -- Ruby Panther
            83088, -- Jade Panther
            83089, -- Sunstone Panther
            83090, -- Sapphire Panther
            104329, -- Ash-Covered Horn
            87250, -- Depleted-Kyparium Rocket
            87251, -- Geosynchronous World Spinner
            89363, -- Disc of the Red Flying Cloud
            82765, -- Reins of the Green Dragon Turtle
            87795, -- Reins of the Black Dragon Turtle
            87796, -- Reins of the Blue Dragon Turtle
            87797, -- Reins of the Brown Dragon Turtle
            87799, -- Reins of the Purple Dragon Turtle
            87800, -- Reins of the Red Dragon Turtle
            91004, -- Reins of the Green Dragon Turtle
            91005, -- Reins of the Brown Dragon Turtle
            91006, -- Reins of the Purple Dragon Turtle
            91007, -- Reins of the Red Dragon Turtle
            91008, -- Reins of the Black Dragon Turtle
            91009, -- Reins of the Blue Dragon Turtle
            79802, -- Reins of the Jade Cloud Serpent
            81354, -- Reins of the Azure Water Strider
            82453, -- Jeweled Onyx Panther
            84101, -- Reins of the Grand Expedition Yak
            85429, -- Reins of the Golden Cloud Serpent
            85430, -- Reins of the Azure Cloud Serpent
            87768, -- Reins of the Onyx Cloud Serpent
            87769, -- Reins of the Crimson Cloud Serpent
            87771, -- Reins of the Heavenly Onyx Cloud Serpent
            87773, -- Reins of the Heavenly Crimson Cloud Serpent
            87774, -- Reins of the Heavenly Golden Cloud Serpent
            87775, -- Yu'lei, Daughter of Jade
            87776, -- Reins of the Heavenly Azure Cloud Serpent
            87777, -- Reins of the Astral Cloud Serpent
            87781, -- Reins of the Azure Riding Crane
            87782, -- Reins of the Golden Riding Crane
            87783, -- Reins of the Regal Riding Crane
            87788, -- Reins of the Grey Riding Yak
            87789, -- Reins of the Blonde Riding Yak
            87791, -- Reins of the Crimson Water Strider
            89304, -- Reins of the Thundering August Cloud Serpent
            89305, -- Reins of the Green Shado-Pan Riding Tiger
            89306, -- Reins of the Red Shado-Pan Riding Tiger
            89307, -- Reins of the Blue Shado-Pan Riding Tiger
            89362, -- Reins of the Brown Riding Goat
            89390, -- Reins of the White Riding Goat
            89391, -- Reins of the Black Riding Goat
            89783, -- Son of Galleon's Saddle
            90655, -- Reins of the Thundering Ruby Cloud Serpent
            93666, -- Spawn of Horridon
            95057, -- Reins of the Thundering Cobalt Cloud Serpent
            95059, -- Clutch of Ji-Kun
            95564, -- Reins of the Golden Primal Direhorn
            95565, -- Reins of the Crimson Primal Direhorn
            98405, -- Brawler's Burly Mushan Beast
            103638, -- Reins of the Ashhide Mushan Beast
            104208, -- Reins of Galakras
            104253, -- Kor'kron Juggernaut
            104269, -- Reins of the Thundering Onyx Cloud Serpent
            95416, -- Sky Golem
            93168, -- Grand Armored Gryphon
            93169, -- Grand Armored Wyvern
            93385, -- Grand Gryphon
            93386, -- Grand Wyvern
            93662, -- Reins of the Armored Skyscreamer
            81559, -- Pandaren Kite String
            85262, -- Reins of the Amber Scorpion
            85666, -- Reins of the Thundering Jade Cloud Serpent
            89785, -- Pandaren Kite String
            91802, -- Jade Pandaren Kite String
            98104, -- Armored Red Dragonhawk
            98259, -- Armored Blue Dragonhawk
            83086, -- Heart of the Nightwing
            93671, -- Ghastly Charger's Skull
            106246, -- Emerald Hippogryph
            82811, -- Reins of the Great Red Dragon Turtle
            87801, -- Reins of the Great Green Dragon Turtle
            87802, -- Reins of the Great Black Dragon Turtle
            87803, -- Reins of the Great Blue Dragon Turtle
            87804, -- Reins of the Great Brown Dragon Turtle
            87805, -- Reins of the Great Purple Dragon Turtle
            91010, -- Reins of the Great Red Dragon Turtle
            91011, -- Reins of the Great Black Dragon Turtle
            91012, -- Reins of the Great Green Dragon Turtle
            91013, -- Reins of the Great Blue Dragon Turtle
            91014, -- Reins of the Great Brown Dragon Turtle
            91015, -- Reins of the Great Purple Dragon Turtle
            94228, -- Reins of the Cobalt Primordial Direhorn
            94229, -- Reins of the Slate Primordial Direhorn
            94230, -- Reins of the Amber Primordial Direhorn
            94231, -- Reins of the Jade Primordial Direhorn
            94290, -- Reins of the Bone-White Primal Raptor
            94291, -- Reins of the Red Primal Raptor
            94292, -- Reins of the Black Primal Raptor
            94293, -- Reins of the Green Primal Raptor
            102514, -- Reins of the Vicious Warsaber
            102533, -- Reins of the Vicious Skeletal Warhorse
            85870, -- Imperial Quilen
            92724, -- Swift Windsteed
            95341, -- Armored Bloodwing
            97989, -- Enchanted Fey Dragon
            98618, -- Hearthsteed
            103630, -- Lucky Riding Turtle
            104011, -- Stormcrow
            107951  -- Iron Skyreaver
        },
        [addon.CONS.M_COMPANIONS_ID] = {
            89640, -- Life Spirit
            89641, -- Water Spirit
            84105, -- Fishy
            88148, -- Jade Crane Chick
            89367, -- Yu'lon Kite
            89368, -- Chi-Ji Kite
            91031, -- Darkmoon Glowfly
            93025, -- Clock'em
            94932, -- Tiny Red Carp
            94933, -- Tiny Blue Carp
            94934, -- Tiny Green Carp
            94935, -- Tiny White Carp
            95621, -- Warbot Ignition Key
            90902, -- Imperial Silkworm
            104333, -- Flimsy Sky Lantern
            82774, -- Jade Owl
            82775, -- Sapphire Cub
            85220, -- Terrible Turnip
            85222, -- Red Cricket
            85447, -- Tiny Goldfish
            85513, -- Thundering Serpent Hatchling
            85578, -- Feral Vermling
            85871, -- Lucky Quilen Cub
            86562, -- Hopling
            86563, -- Hollow Reed
            86564, -- Imbued Jade Fragment
            87526, -- Mechanical Pandaren Dragonling
            88147, -- Singing Cricket Cage
            89587, -- Porcupette
            89686, -- Jade Tentacle
            89736, -- Venus
            90173, -- Pandaren Water Spirit
            90177, -- Baneling
            90897, -- Fox Kit
            90898, -- Fox Kit
            90900, -- Imperial Moth
            90953, -- Spectral Cub
            91003, -- Darkmoon Hatchling
            91040, -- Darkmoon Eye
            92707, -- Cinder Kitten
            92798, -- Pandaren Fire Spirit
            92799, -- Pandaren Air Spirit
            92800, -- Pandaren Earth Spirit
            93029, -- Gluth's Bone
            93030, -- Dusty Clutch of Eggs
            93031, -- Mr. Bigglesworth
            93032, -- Blighted Spore
            93033, -- Mark of Flame
            93034, -- Blazing Rune
            93035, -- Core of Hardened Ash
            93036, -- Unscathed Egg
            93037, -- Blackwing Banner
            93038, -- Whistle of Chromatic Bone
            93039, -- Viscidus Globule
            93040, -- Anubisath Idol
            93041, -- Jewel of Maddening Whispers
            93669, -- Gusting Grimoire
            94025, -- Red Panda
            94124, -- Sunreaver Micro-Sentry
            94125, -- Living Sandling
            94126, -- Zandalari Kneebiter
            94152, -- Son of Animus
            94190, -- Spectral Porcupette
            94191, -- Stunted Direhorn
            94208, -- Sunfur Panda
            94209, -- Snowy Panda
            94210, -- Mountain Panda
            94573, -- Direhorn Runt
            94574, -- Pygmy Direhorn
            94595, -- Spawn of G'nathus
            94835, -- Ji-Kun Hatchling
            94903, -- Pierre
            95422, -- Zandalari Anklerender
            95423, -- Zandalari Footslasher
            95424, -- Zandalari Toenibbler
            97548, -- Spiky Collar
            97549, -- Instant Arcane Sanctum Security Kit
            97550, -- Netherspace Portal-Stone
            97551, -- Satyr Charm
            97552, -- Shell of Tide-Calling
            97553, -- Tainted Core
            97554, -- Dripping Strider Egg
            97555, -- Tiny Fel Engine Key
            97556, -- Crystal of the Void
            97557, -- Brilliant Phoenix Hawk Feather
            97558, -- Tito's Basket
            97821, -- Gahz'rooki's Summoning Stone
            97959, -- Quivering Blob
            97960, -- Dark Quivering Blob
            97961, -- Half-Empty Food Container
            98550, -- Blossoming Ancient
            100870, -- Murkimus' Tyrannical Spear
            100905, -- Rascal-Bot
            101570, -- Moon Moon
            101771, -- Xu-Fu, Cub of Xuen
            102145, -- Chi-Chi, Hatchling of Chi-Ji
            102146, -- Zao, Calfling of Niuzao
            102147, -- Yu'la, Broodling of Yu'lon
            103637, -- Vengeful Porcupette
            103670, -- Lil' Bling
            104156, -- Ashleaf Spriteling
            104157, -- Azure Crane Chick
            104158, -- Blackfuse Bombling
            104159, -- Ruby Droplet
            104160, -- Dandelion Frolicker
            104161, -- Death Adder Hatchling
            104162, -- Droplet of Y'Shaarj
            104163, -- Gooey Sha-ling
            104164, -- Jademist Dancer
            104165, -- Kovok
            104166, -- Ominous Flame
            104167, -- Skunky Alemental
            104168, -- Spineclaw Crab
            104169, -- Gulp Froglet
            104202, -- Bonkers
            104291, -- Swarmling of Gu'chi
            104295, -- Harmonious Porcupette
            104307, -- Jadefire Spirit
            104317, -- Rotten Helper Box
            104332, -- Sky Lantern
            106240, -- Alterac Brandy
            106244, -- Murkalot's Flail
            106256, -- Treasure Goblin's Pack
            80008,  -- Darkmoon Rabbit
            [addon.CONS.MC_BATTLE_STONE_ID] = {
                92742, -- Polished Battle-Stone
                98715, -- Marked Flawless Battle-Stone
                92741, -- Flawless Battle-Stone
                92679, -- Flawless Aquatic Battle-Stone
                92675, -- Flawless Beast Battle-Stone
                92676, -- Flawless Critter Battle-Stone
                92683, -- Flawless Dragonkin Battle-Stone
                92665, -- Flawless Elemental Battle-Stone
                92677, -- Flawless Flying Battle-Stone
                92682, -- Flawless Humanoid Battle-Stone
                92681, -- Flawless Undead Battle-Stone
                92678, -- Flawless Magic Battle-Stone
                92680  -- Flawless Mechanical Battle-Stone
            },
            [addon.CONS.MC_CONSUMABLE_ID] = {
                86143, -- Battle Pet Bandage
                98114, -- Pet Treat
                89139, -- Chain Pet Leash
                89906  -- Magical Mini-Treat
            },
            [addon.CONS.MC_SUPPLIES_ID] = {
                91086, -- Darkmoon Pet Supplies
                89125, -- Sack of Pet Supplies
                98095, -- Brawler's Pet Supplies
                93146, -- Pandaren Spirit Pet Supplies
                93147, -- Pandaren Spirit Pet Supplies
                93148, -- Pandaren Spirit Pet Supplies
                93149, -- Pandaren Spirit Pet Supplies
                94207  -- Fabled Pandaren Pet Supplies
            }
        },
        [addon.CONS.M_TELEPORT_ID] = {
            [addon.CONS.MT_JEWELRY_ID] = {
                103678, -- Time-Lost Artifact
                95051, -- The Brassiest Knuckle (Bizmo's Brawlpub)
                95050  -- The Brassiest Knuckle (Brawl'gar Arena)
            },
            [addon.CONS.MT_QUEST_ID] = {
                92510  -- Vol'jin's Hearthstone
            },
            [addon.CONS.MT_TOYS_ID] = {
                93672, -- Dark Portal
                95567, -- Kirin Tor Beacon
                95568, -- Sunreaver Beacon
                87215  -- Wormhole Generator: Pandaria
            }
        },
        [addon.CONS.M_ARCHAEOLOGY_ID] = {
            [addon.CONS.MA_CRATES_ID]= {
                87533, -- Crate of Dwarven Archaeology Fragments
                87534, -- Crate of Draenei Archaeology Fragments
                87535, -- Crate of Fossil Archaeology Fragments
                87536, -- Crate of Night Elf Archaeology Fragments
                87537, -- Crate of Nerubian Archaeology Fragments
                87538, -- Crate of Orc Archaeology Fragments
                87539, -- Crate of Tol'vir Archaeology Fragments
                87540, -- Crate of Troll Archaeology Fragments
                87541  -- Crate of Vrykul Archaeology Fragments
            },
            [addon.CONS.MA_ARTIFACT_ID] = {
                87399  -- Restored Artifact
            },
            [addon.CONS.MA_KEY_STONES_ID] = {
                79869, -- Mogu Statue Piece
                79868, -- Pandaren Pottery Shard
                95373  -- Mantid Amber Sliver
            },
            [addon.CONS.MA_OTHERS_ID] = {
                87548, -- Lorewalker's Lodestone
                87549, -- Lorewalker's Map
                104198  -- Mantid Artifact Hunter's Kit
            }
        },
        [addon.CONS.M_OTHER_ID] = {
            87816, -- Tigersblood Tincture
            87821, -- Coagulated Tiger's Blood
            87828, -- Tigersblood Pigment
            88165, -- Vine-Cracked Junkbox
            94536, -- Intact Direhorn Hide
            95491, -- Tattered Historical Parchments
            87391, -- Plundered Treasure
            72201, -- Plump Intestines
            87701, -- Sack of Raw Tiger Steaks
            87702, -- Sack of Mushan Ribs
            87703, -- Sack of Raw Turtle Meat
            87704, -- Sack of Raw Crab Meat
            87705, -- Sack of Wildfowl Breasts
            87706, -- Sack of Green Cabbages
            87707, -- Sack of Juicycrunch Carrots
            87708, -- Sack of Mogu Pumpkins
            87709, -- Sack of Scallions
            87710, -- Sack of Red Blossom Leeks
            87712, -- Sack of Witchberries
            87713, -- Sack of Jade Squash
            87714, -- Sack of Striped Melons
            87715, -- Sack of Pink Turnips
            87716, -- Sack of White Turnips
            87721, -- Sack of Jade Lungfish
            87722, -- Sack of Giant Mantis Shrimp
            87723, -- Sack of Emperor Salmon
            87724, -- Sack of Redbelly Mandarin
            87725, -- Sack of Tiger Gourami
            87726, -- Sack of Jewel Danio
            87727, -- Sack of Reef Octopus
            87728, -- Sack of Krasarang Paddlefish
            87729, -- Sack of Golden Carp
            87730, -- Sack of Crocolisk Belly
            88496, -- Sealed Crate
            93198, -- Tome of the Tiger
            93199, -- Tome of the Crane
            93200, -- Tome of the Serpent
            76761, -- Pandaren Scroll
            77501, -- Blue Blizzcon Bag
            79104, -- Rusty Watering Can
            79261, -- Arcane Shackles Key
            79896, -- Pandaren Tea Set
            79897, -- Pandaren Game Board
            79898, -- Twin Stein Set
            79899, -- Walking Cane
            79900, -- Empty Keg
            79901, -- Carved Bronze Mirror
            79902, -- Gold-Inlaid Figurine
            79903, -- Apothecary Tins
            79904, -- Pearl of Yu'lon
            79905, -- Standard of Niuzao
            79908, -- Manacles of Rebellion
            79909, -- Cracked Mogu Runestone
            79910, -- Terracotta Arm
            79911, -- Petrified Bone Whip
            79912, -- Thunder King Insignia
            79913, -- Edicts of the Thunder King
            79914, -- Iron Amulet
            79915, -- Warlord's Branding Iron
            79916, -- Mogu Coin
            79917, -- Worn Monument Ledger
            80302, -- EZ-Gro Green Cabbage Seeds
            80513, -- Vintage Bug Sprayer
            80546, -- Tap Tool
            81055, -- Darkmoon Ride Ticket
            83078, -- Legacy of the Masters (Part 1)
            83128, -- Brew Defender Ball
            85477, -- Pristine Mogu Coin
            85557, -- Pristine Pandaren Tea Set
            85558, -- Pristine Game Board
            85582, -- Shao-Tien Cage Key
            86067, -- Horde Missive
            86068, -- Alliance Missive
            87779, -- Ancient Guo-Lai Cache Key
            87806, -- Ancient Mogu Key
            89155, -- Onyx Egg
            89170, -- Pristine Mogu Runestone
            89171, -- Pristine Terracotta Arm
            89172, -- Pristine Petrified Bone Whip
            89173, -- Pristine Thunder King Insignia
            89174, -- Pristine Edicts of the Thunder King
            89175, -- Pristine Iron Amulet
            89176, -- Pristine Branding Iron
            89178, -- Pristine Twin Stein Set
            89179, -- Pristine Walking Cane
            89180, -- Pristine Empty Keg
            89181, -- Pristine Carved Bronze Mirror
            89182, -- Pristine Gold-Inlaid Figurine
            89183, -- Pristine Apothecary Tins
            89184, -- Pristine Pearl of Yu'lon
            89185, -- Pristine Standard of Niuzao
            89209, -- Pristine Monument Ledger
            89607, -- Crate of Leather
            89608, -- Crate of Ore
            89609, -- Crate of Dust
            89610, -- Pandaria Herbs
            89815, -- Master Plow
            89880, -- Dented Shovel
            89991, -- Pandaria Fireworks
            90621, -- Hero's Purse
            90622, -- Hero's Purse
            90623, -- Hero's Purse
            90624, -- Hero's Purse
            90627, -- Hero's Purse
            90628, -- Hero's Purse
            90629, -- Hero's Purse
            90630, -- Hero's Purse
            90631, -- Hero's Purse
            90632, -- Hero's Purse
            90633, -- Hero's Purse
            90634, -- Hero's Purse
            90635, -- Hero's Purse
            90735, -- Goodies from Nomi
            91906, -- Brittle Root
            91971, -- Battle Rations
            92035, -- Adorned Sceptre
            92061, -- Laborer's Tool
            92440, -- Reliquary Facsimile
            92444, -- Meaty Haunch
            92470, -- Snake Oil
            92471, -- Jubeka's Journal
            92472, -- Soggy Journal
            92538, -- Unexploded Cannonball
            92739, -- Misplaced Keg
            92743, -- Krasari Iron
            92744, -- Heavy Sack of Gold
            92745, -- Liquid Fire
            92750, -- Jungle Hops
            92751, -- Spirit-Kissed Water
            92788, -- Ride Ticket Book
            92789, -- Ride Ticket Book
            92790, -- Ride Ticket Book
            92791, -- Ride Ticket Book
            92792, -- Ride Ticket Book
            92793, -- Ride Ticket Book
            92794, -- Ride Ticket Book
            93738, -- Rusty Prison Key
            93962, -- Heavy Explosives
            94130, -- Incantation of Haqin
            94219, -- Arcane Trove
            94220, -- Sunreaver Bounty
            94233, -- Incantation of Deng
            94918, -- Gatekeeper's Orb
            95350, -- Incantation of Vu
            95375, -- Banner of the Mantid Empire
            95376, -- Ancient Sap Feeder
            95377, -- The Praying Mantid
            95378, -- Inert Sound Beacon
            95379, -- Remains of a Paragon
            95380, -- Mantid Lamp
            95381, -- Pollen Collector
            95382, -- Kypari Sap Container
            95497, -- Burial Trove Key
            95622, -- Arcane Trove
            95623, -- Sunreaver Bounty
            97948, -- Surplus Supplies
            97949, -- Surplus Supplies
            97950, -- Surplus Supplies
            97951, -- Surplus Supplies
            97952, -- Surplus Supplies
            97953, -- Surplus Supplies
            97954, -- Surplus Supplies
            97955, -- Surplus Supplies
            97956, -- Surplus Supplies
            97957, -- Surplus Supplies
            98096, -- Large Sack of Coins
            98097, -- Huge Sack of Coins
            98098, -- Bulging Sack of Coins
            98099, -- Giant Sack of Coins
            98100, -- Humongous Sack of Coins
            98101, -- Enormous Sack of Coins
            98102, -- Overflowing Sack of Coins
            98103, -- Gigantic Sack of Coins
            98560, -- Arcane Trove
            98562, -- Sunreaver Bounty
            100948, -- Skeleton Key
            102144, -- Kor'kron Cage Key
            103786, -- "Dapper Gentleman" Costume
            103789, -- "Little Princess" Costume
            103795, -- "Dread Pirate" Costume
            103797, -- Big Pink Bow
            103977, -- Time-Worn Journal
            104015, -- Barnacle Encrusted Key
            104034, -- Purse of Timeless Coins
            104035, -- Giant Purse of Timeless Coins
            104115, -- Mist-Filled Spirit Lantern
            104286, -- Quivering Firestorm Egg
            88567, -- Ghost Iron Lockbox
            104334, -- Misty Pi'jiu Brew
            104335, -- Thick Pi'jiu Brew
            104336, -- Bubbling Pi'jiu Brew
            86623, -- Blingtron 4000 Gift Package
            80822, -- The Golden Banana
            85223, -- Enigma Seed Pack
            85224, -- Basic Seed Pack
            85225, -- Basic Seed Pack
            85226, -- Basic Seed Pack
            85227, -- Special Seed Pack
            85271, -- Secret Stash
            85272, -- Tree Seed Pack
            85274, -- Gro-Pack
            85275, -- Chee Chee's Goodie Bag
            85276, -- Celebration Gift
            85277, -- Nicely Packed Lunch
            85497, -- Chirping Package
            85498, -- Songbell Seed Pack
            86595, -- Bag of Helpful Things
            87217, -- Small Bag of Goods
            89365, -- Feverbite Egg Sack
            89373, -- Scotty's Lucky Coin
            89427, -- Ancient Mogu Treasure
            89428, -- Ancient Mogu Treasure
            90078, -- Cracked Talisman
            90166, -- Akkalou's Clamshell
            90167, -- Akkalar's Clamshell
            90168, -- Kishak's Clamshell
            90169, -- Damlak's Clamshell
            90170, -- Clamstok's Clamshell
            90171, -- Odd'nirok's Clamshell
            90172, -- Clamshell Band
            93360, -- Serpent's Cache
            94159, -- Small Bag of Zandalari Supplies
            94566, -- Fortuitous Coffer
            95509, -- Mantid Artifact Sonic Locator
            102464, -- Black Ash
            104293, -- Scuttler's Shell
            104297, -- Blazing Sigil of Ordos
            104320, -- Cursed Talisman
            104330, -- Warped Warning Sign
            104346, -- Golden Glider
            85500, -- Anglers Fishing Raft
            86565, -- Battle Horn
            86578, -- Eternal Warrior's Sigil
            86582, -- Aqua Jewel
            86583, -- Salyin Battle Banner
            86584, -- Hardened Shell
            86590, -- Essence of the Breeze
            86594, -- Helpful Wikky's Whistle
            87528, -- Honorary Brewmaster Keg
            89613, -- Cache of Treasures
            90537, -- Winner's Reward
            92813, -- Greater Cache of Treasures
            94154, -- Survivor's Bag of Coins
            94553, -- Notes on Lightning Steel
            97268, -- Tome of Valor
            98133, -- Greater Cache of Treasures
            102467, -- Censer of Eternal Agony
            79264, -- Ruby Shard
            79265, -- Blue Feather
            79266, -- Jade Cat
            79267, -- Lovely Apple
            79268, -- Marsh Lily
            79906, -- Umbrella of Chi-Ji
            79907, -- Spear of Xuen
            80914, -- Mourning Glory
            86425, -- Cooking School Bell
            86428, -- Old Man Thistle's Treasure
            86568, -- Mr. Smite's Brass Compass
            86571, -- Kang's Bindstone
            86573, -- Shard of Archstone
            86575, -- Chalice of Secrets
            86577, -- Rod of Ambershaping
            86581, -- Farwater Conch
            86586, -- Panflute of Pandaria
            86588, -- Pandaren Firework Launcher
            86589, -- Ai-Li's Skymirror
            86591, -- Magic Banana
            86593, -- Hozen Beach Ball
            86596, -- Nat's Fishing Chair
            87219, -- Huge Bag of Herbs
            87220, -- Big Bag of Mysteries
            87221, -- Big Bag of Jewels
            87222, -- Big Bag of Linens
            87223, -- Big Bag of Skins
            87224, -- Big Bag of Wonders
            87225, -- Big Bag of Food
            88566, -- Krastinov's Bag of Horrors
            90067, -- B. F. F. Necklace
            90175, -- Gin-Ji Knife Set
            90626, -- Hero's Purse
            90840, -- Marauder's Gleaming Sack of Gold
            90899, -- Darkmoon Whistle
            92738, -- Safari Hat
            94158, -- Big Bag of Zandalari Supplies
            95343, -- Treasures of the Thunder King
            95469, -- Serpent's Heart
            95566, -- Ra'sha's Sacrificial Dagger
            95617, -- Dividends of the Everlasting Spring
            95618, -- Cache of Mogu Riches
            95619, -- Amber Encased Treasure Pouch
            97153, -- Spoils of the Thunder King
            97919, -- Whole-Body Shrinka'
            97921, -- Bom'bay's Color-Seein' Sauce
            97942, -- Sen'jin Spirit Drum
            98132, -- Shado-Pan Geyser Gun
            98136, -- Gastropod Shell
            101529, -- Celestial Coin
            101538, -- Kukuru's Cache Key
            102463, -- Fire-Watcher's Oath
            103624, -- Treasures of the Vale
            103679, -- Mask of Fear
            103680, -- Mask of Hatred
            103681, -- Mask of Doubt
            103682, -- Mask of Violence
            103683, -- Mask of Anger
            103684, -- Scroll of Challenge
            103685, -- Celestial Defender's Medallion
            104258, -- Glowing Green Ash
            104261, -- Glowing Blue Ash
            104262, -- Odd Polished Stone
            104263, -- Glinting Pile of Stone
            104268, -- Pristine Stalker Hide
            104292, -- Partially-Digested Meal
            104294, -- Rime of the Time-Lost Mariner
            104296, -- Ordon Ceremonial Robes
            104299, -- Falling Flame
            104302, -- Blackflame Daggers
            104303, -- Rain Stone
            104304, -- Blizzard Stone
            104305, -- Ashen Stone
            104306, -- Sunset Stone
            104309, -- Eternal Kiln
            104328, -- Cauterizing Core
            104331, -- Warning Sign
            106130, -- Big Bag of Herbs
            107950, -- Bipsi's Bobbing Berg
            104009, -- Timeless Plate Armor Cache
            104010, -- Timeless Mail Armor Cache
            104012, -- Timeless Leather Armor Cache
            104013, -- Timeless Cloth Armor Cache
            86546, -- Sky Crystal
            86547, -- Skyshard
            98134, -- Heroic Cache of Treasures
            98546, -- Bulging Heroic Cache of Treasures
            103533, -- Vicious Saddle
            103535, -- Bulging Bag of Charms
            105751, -- Kor'kron Shaman's Treasure
            87218, -- Big Bag of Arms
            89810, -- Bounty of a Sundered Land
            90839, -- Cache of Sha-Touched Gold
            94222, -- Key to the Palace of Lei Shen
            95601, -- Shiny Pile of Refuse
            95602, -- Stormtouched Cache
            97565, -- Unclaimed Black Market Container
            102137, -- Unclaimed Black Market Container
            103632, -- Lucky Box of Greatness
            104271, -- Coalesced Turmoil
            104272, -- Celestial Treasure Box
            104273, -- Flame-Scarred Cache of Offerings
            104275, -- Twisted Treasures of the Vale
            105713, -- Twisted Treasures of the Vale
            105714, -- Coalesced Turmoil
            105911, -- Pouch of Enduring Wisdom
            105912, -- Pouch of Enduring Wisdom (5)
            90818   -- Misty Satchel of Exotic Mysteries
        }
    },
    [addon.CONS.QUEST_ID] = {
        93195, -- Brawler's Pass
        93228, -- Brawler's Pass
        87557, -- Bundle of Groceries
        87811, -- Commissioned Painting
        87812, -- Jade Serpent Commission
        87814, -- Engraved Jade Disk
        87815, -- Jade Disk
        87817, -- Incarnadine Ink
        88806, -- Portrait of Madam Goya
        88807, -- Uninscribed Monument
        88808, -- Fine Canvas
        101776, -- Fatty Turtle Steak
        103651, -- Slosh of Brew
        104110, -- Curious Bronze Timepiece
        104113, -- Curious Bronze Timepiece
        80071, -- Cho Family Heirloom
        85783, -- Captain Jack's Head
        87831, -- Inscribed Monument
        72071, -- Stolen Training Supplies
        72109, -- Wind Stone
        72111, -- Dry Dogwood Root
        72112, -- Fluttering Breeze
        72133, -- Orchard Tool
        72578, -- Nectarbreeze Cider
        72583, -- Huo's Offerings
        72589, -- Ripe Orange
        72926, -- Rattan Switch
        72954, -- Black Walnut Extract
        72979, -- Triple-Bittered Ale
        73178, -- Hard Tearwood Reed
        73183, -- Snowblossom Petals
        73184, -- Sun Pearl
        73193, -- Blushleaf Extract
        73368, -- Tortoise Flank
        73369, -- Monastery Fireworks
        73791, -- Sun Pearl
        74030, -- Scroll of Introduction
        74033, -- Ancient Hozen Skull
        74160, -- Zin'Jun's Rifle
        74161, -- Zin'Jun's Left Eye
        74162, -- Zin'Jun's Right Eye
        74163, -- Snuff's Corpse
        74258, -- Staff of Pei-Zhi
        74260, -- Bamboo Key
        74295, -- Uprooted Turnip
        74296, -- Stolen Carrot
        74297, -- Pilfered Pumpkin
        74298, -- Dai-Lo Recess Mallet
        74301, -- Discarded Wood Plank
        74615, -- Paint Soaked Brush
        74621, -- Viscous Chlorophyll
        74623, -- Emergency Supplies
        74624, -- Abandoned Stone Block
        74631, -- Stolen Firework Bundle
        74634, -- Kun-Pai Ritual Charm
        74760, -- Chipped Ritual Bowl
        74761, -- Pungent Ritual Candle
        74762, -- Jade Cong
        74763, -- Spirit Bottle
        74771, -- Staff of Pei-Zhi
        74808, -- Spirit Bottles
        74955, -- Packed Explosion Charge
        74958, -- Alliance Medical Supplies
        75000, -- Torch of Prismatic Flame
        75008, -- Unlit Challenger's Torch
        75023, -- Pristine Silk Strand
        75202, -- Speckled Trout
        75214, -- Tidemist Cap
        75219, -- Freshly Fallen Petal
        75221, -- Wasp Stinger
        76107, -- Pristine Crocolisk Eye
        76115, -- Amberfly Wing
        76128, -- Delicate Shearing Knife
        76129, -- Mist Horror Heart
        76167, -- Dream Brew
        76173, -- Bug Leg
        76174, -- Jade Tiger Pillar
        76209, -- Chunk of Jade
        76225, -- Fistful of Bird Guts
        76260, -- Exploded Slicky
        76262, -- Gut Bomb
        76305, -- Hellscream's Fist Signal Flare
        76333, -- Green Branch
        76420, -- Snapper Steak
        76483, -- Scavenged Jade
        76501, -- Emperor Tern Egg
        76503, -- Whitefisher Crane Egg
        76516, -- Hornbill Strider Egg
        76725, -- Scryer's Staff
        76727, -- Slimy Bottle
        76973, -- Sprig of Dreamleaf
        77033, -- Sack of Grain
        77034, -- Malted Cave Barley
        77281, -- Fran's Watering Can
        77379, -- Virmen Tooth
        77419, -- Mask of Doubt
        77432, -- Ancient Sutra
        77452, -- Defender's Arrow
        77455, -- Mulberry Leaves
        77456, -- Raw Silk
        77471, -- Spritewater Essence
        77475, -- Stack of Mantras
        78877, -- Slick Mudfish
        78880, -- Salty Core
        78881, -- Chunk of Honeycomb
        78911, -- Intact Skitterer Glands
        78914, -- Vial of Tiger Blood
        78917, -- Dojani Orders
        78918, -- Imperial Lotus Leaves
        78928, -- Flame of Zhu's Watch
        78934, -- The Water of Youth
        78941, -- Huge Panther Fang
        78942, -- Jar of Pigment
        78947, -- Silken Rope
        78958, -- Pillaged Jinyu Loot
        78959, -- Serpent Egg
        78960, -- Green Serpent Egg
        78961, -- Yellow Serpent Egg
        78962, -- Blue Serpent Egg
        79021, -- Ken-Ken's Mask
        79025, -- Slitherscale Harpoon
        79027, -- Saltback Meat
        79028, -- Saltback Meat Scrap
        79030, -- Honeycomb
        79043, -- Bouncy Ball
        79046, -- Sugar Minnow
        79049, -- Serpentrider Relic
        79057, -- Ken-Ken's Mask
        79058, -- Darkhide's Head
        79059, -- Intact Tortoise Shell
        79067, -- Stolen Egg
        79120, -- Mogu Artifact
        79163, -- Imperial Lotus Poultice
        79197, -- Glade Glimmer
        79198, -- Spindly Bloodfeather
        79199, -- Murkscale Head
        79237, -- Enormous Crocolisk Tail
        79238, -- Enormous Crocolisk Tail
        79252, -- Mogu Poisoned Blade
        79332, -- Sentinel Scout's Report
        79713, -- Dynastic Tablet
        79745, -- Sunwalker Scout's Report
        79753, -- Slingtail Key
        79806, -- Arrow for Commander Hsieh
        79807, -- Waterspeaker's Staff
        79808, -- Ceremonial Robes
        79809, -- Jade Crown
        79810, -- Rosewood Beads
        79811, -- Glassfin Heirloom
        79819, -- Dit Da Jow
        79824, -- Stolen Vegetable
        79825, -- Zhu's Watch Supplies
        79827, -- Authentic Valley Stir Fry
        79828, -- Yak Statuette
        79833, -- Shadelight Truffle
        79864, -- Cindergut Pepper
        79866, -- Kunzen Legend-Book
        79867, -- Fatty Goatsteak
        79870, -- Yu-Ping Soup
        79871, -- Spicy Shrimp Dumplings
        79875, -- Song of the Vale
        79880, -- Stolen Supplies
        79884, -- Bucket of Slicky Water
        79885, -- Barrel of Fireworks
        79894, -- Mushan Tail Stew
        79895, -- Master's Pot
        79952, -- Pungent Sprite Needles
        80013, -- Shademaster Kiryn's Report
        80014, -- Rivett Clutchpop's Report
        80015, -- Shokia's Report
        80061, -- Riko's Report
        80074, -- Celestial Jade
        80116, -- Partially Chewed Carrot
        80122, -- Spideroot
        80127, -- Shadelight Truffle Spores
        80133, -- Preserved Vegetables
        80134, -- Uncut Chrysoberyl
        80136, -- Waxed Plank
        80137, -- Ransacked Ring
        80138, -- Stolen Circlet
        80139, -- Burglarized Bracelet
        80140, -- Bloodbloom
        80141, -- Cave Lily
        80142, -- Ghostcap
        80143, -- Violet Lichen
        80144, -- Tasty T-Bone
        80176, -- Water-Damaged Gear
        80177, -- Rusty Locking Bolt
        80212, -- The Master's Flame
        80213, -- Spicemaster Jin Jao's Payment
        80214, -- Trader Jambeezi's Payment
        80215, -- Innkeeper Lei Lan's Payment
        80216, -- Lolo Lio's Payment
        80223, -- Old Hillpaw's Prize Chicken
        80227, -- Root Vegetable
        80228, -- Enormous Cattail Grouper Tooth
        80229, -- Blue Freshwater Pearl
        80230, -- Cast Iron Pot
        80231, -- Goldenfire Orchid
        80232, -- Bloody Plainshawk Leg
        80233, -- Grilled Plainshawk Leg
        80234, -- Yoon's Apple
        80235, -- Yoon's Craneberry
        80236, -- Apple-Berry Hooch
        80241, -- Muskpaw's Keepsake
        80245, -- Kun-Lai Meaty Bits
        80260, -- Dojani Eel
        80262, -- Beloved Ring
        80277, -- Jagged Abalone Meat
        80294, -- Mogu Relic
        80295, -- Packet of Green Cabbage Seeds
        80303, -- Pristine Crane Egg
        80307, -- Grummlepack
        80308, -- Fire Lotus Incense
        80310, -- Silver Goby
        80311, -- Filled Oil Vial
        80312, -- Empty Oil Vial
        80314, -- EZ-Gro Green Cabbage
        80315, -- Stolen Supplies
        80316, -- Lucky Virmen's Foot
        80317, -- Lucky Yak Shoe
        80318, -- Lucky Four Winds Clover
        80319, -- Lucky "Gold" Coin
        80337, -- Ken-Ken's Mask
        80403, -- Angler's Fishing Spear
        80428, -- Corpse of Dak Dak
        80429, -- Corpse of Ko Ko
        80430, -- Corpse of Tak Tak
        80437, -- Armored Carp
        80528, -- Explosives Barrel
        80529, -- Prickly Puffer Spine
        80535, -- Yeti Shackle Key
        80598, -- Zhen's Banner
        80599, -- Goblin Fishing Bomb
        80600, -- Stinger
        80677, -- Emerald Tailfeather
        80678, -- Crimson Tailfeather
        80679, -- Dusky Tailfeather
        80685, -- Spare Plank
        80804, -- Tough Kelp
        80806, -- Broken Bamboo Stalk
        80810, -- Shark Fillet
        80817, -- Buried Hozen Treasure
        80827, -- Confusing Treasure Map
        80828, -- "Scrutiny"
        80830, -- Rusty Shipwreck Debris
        80831, -- Snapclaw's Claw
        80832, -- Viseclaw Fisher Eye
        80907, -- Opalescent Blue Crab Shell
        80938, -- Gift of the Great Crane
        80944, -- Bundle of Kafa'kota Berries
        81116, -- Suncrawler
        81122, -- Wolf Piranha
        81137, -- Rabbitsfoot's Luckydo
        81174, -- Dark Pitch
        81176, -- Mist-Shaman's Torch
        81178, -- Stone Key
        81183, -- Flask of Kafa
        81193, -- Ban's Explosives
        81250, -- Snarlvine
        81260, -- Clotted Rodent's Blood
        81261, -- Stolen Pandaren Spices
        81269, -- Waterfall-Polished Stone
        81293, -- Stolen Luckydos
        81319, -- Stack of Torches
        81355, -- Palewind Totem
        81356, -- Shado-Pan Torch
        81385, -- Stolen Inkgill Ritual Staff
        81393, -- Chen's Full Keg
        81417, -- Totem of Harmony
        81430, -- Totem of Harmony
        81712, -- The Tongue of Ba-Shon
        81713, -- Blind Rage Essence
        81741, -- Blinding Rage Trap
        81890, -- Blood-Revealed Map
        81891, -- Gunpowder Casks
        81892, -- Torn Page
        81925, -- Shado-Pan Flare
        82298, -- Handful of Volatile Blooms
        82299, -- Blood-Stained Blade
        82332, -- Father's Crossbow
        82342, -- Violet Citron
        82346, -- Pot of Fire
        82353, -- Sra'thik Weapon
        82381, -- Yak's Milk Flask
        82387, -- Full Mushan Bladder
        82388, -- Rankbite Shell Fragment
        82389, -- Mao-Willow
        82393, -- Shen Dynasty Rubbing
        82394, -- Qiang Dynasty Rubbing
        82395, -- Wai Dynasty Rubbing
        82468, -- Yak Lasso
        82722, -- Krik'thik Limb
        82723, -- Volatile Dread Orb
        82764, -- Bottom Fragment of Lei Shen's Tablet
        82783, -- Initiate Chao's Sword
        82787, -- Citron-Infused Bandages
        82799, -- Yaungol Oil Barrel
        82807, -- Shado-Pan Dragon Gun
        82808, -- Call of the Lorewalkers
        82864, -- Living Amber
        82867, -- Mantid Relic
        82869, -- Meaty Turtle Haunch
        82870, -- Strange Relic
        83023, -- Shado-Pan Crossbow Bolt Bundle
        83024, -- Shado-Pan Fire Arrows
        83062, -- Klaxxi Tuning Fork
        83075, -- Sapfly Bits
        83076, -- Between a Saurok and a Hard Place
        83129, -- Cloudrunner Egg
        83130, -- Shan'ze Tablet
        83134, -- Bronze Claws
        83135, -- Amber Blade
        83136, -- Quiver of Shado-Pan Fire Arrows
        83138, -- Onyx Heart
        83153, -- Bronze Claw
        83276, -- Klaxxi Resonating Crystal
        83767, -- Krosh's Back
        83768, -- Wu Kao Torch
        83770, -- Hozen in the Mist
        83771, -- Fish Tales
        83772, -- The Dark Heart of the Mogu
        83773, -- Heart of the Mantid Swarm
        83774, -- What Is Worth Fighting For
        83777, -- The Song of the Yaungol
        83779, -- The Seven Burdens of Shaohao
        83780, -- The Ballad of Liu Lang
        83781, -- Lost Keg
        83782, -- Lost Mugs
        83783, -- Lost Picnic Supplies
        84102, -- Ancient Arcane Powder
        84107, -- Large Mushan Tooth
        84111, -- Blade of Kz'Kzik
        84112, -- Blade of Ilikkax
        84118, -- Fragrant Corewood
        84119, -- Klaxxi Tuning Fork
        84157, -- Rivett's Rocket Jumpers
        84239, -- Flitterling Dust
        84267, -- Rikkitun Bell
        84586, -- Scroll of Auspice
        84655, -- Mogu Artifact
        84727, -- Ancient Spirit Dust
        84759, -- Ciphered Scroll
        84762, -- Highly Explosive Yaungol Oil
        84771, -- Ruining Fork
        84779, -- Chunk of Solidified Amber
        85159, -- Amber-Filled Jar
        85160, -- Calligraphed Letter
        85174, -- Elegant Rune
        85196, -- Calligraphed Parchment
        85203, -- Calligraphed Note
        85205, -- Calligraphed Sigil
        85211, -- Starfish Meat
        85212, -- Clacker Tail
        85229, -- Volatile Blood
        85230, -- Sea Monarch Chunks
        85231, -- Bag of Clams
        85282, -- Tiger Flank
        85507, -- Alliance Orders
        85571, -- Venomous Stinger
        85572, -- Scarab Wing
        85573, -- Dreadshade
        85634, -- Gurthani Tablet
        85635, -- Pristine Mire Beast Eye
        85664, -- Amber Sap
        85665, -- Mushan Tongue
        85688, -- A Missive from Lorewalker Cho
        85694, -- A Missive from Lorewalker Cho
        85774, -- Ancient Tiger's Blood
        85784, -- Alliance Service Medallion
        85854, -- The Needlebeak
        85875, -- Pure Amber
        85884, -- Sonic Emitter
        85885, -- Amber-Encrusted Brain
        85886, -- Sealed Charter Tube
        85972, -- Mist-Hopper Emergency Buoy
        85981, -- Black Market Merchandise
        85998, -- Thresher Jaw
        85999, -- Thresher Teeth
        86009, -- Resonating Crystal
        86392, -- Letter to Sungshin Ironpaw
        86404, -- Old Map
        86421, -- Old Man Thistle's Almanac
        86431, -- Stormstout Secrets
        86433, -- Nice Necklace
        86434, -- Tasteful Tiara
        86435, -- Exquisite Earring
        86436, -- Beautiful Brooch
        86446, -- Sheepie
        86465, -- Old Sheepskin
        86467, -- Cho's Fireworks
        86476, -- Lantern of the Sorcerer King
        86477, -- Mad King Meng's Balance
        86478, -- Qiang's "The Science of War"
        86479, -- Subetai's Bow of the Swift
        86489, -- Succulent Turtle Filet
        86511, -- Cho's Fireworks
        86532, -- Bag of Shado-Pan Gas Bombs
        86533, -- Glowing Amber
        86598, -- Vor'thik Eggs
        86616, -- Dread Amber Focus
        87202, -- Klaxxi Tuning Fork
        87263, -- Venom-Coated Mandible
        87267, -- Codex of the Crusade
        87268, -- Codex of the Crusade
        87269, -- Kypari Ik Resonating Crystal
        87282, -- Blade of the Anointed
        87388, -- Blades of the Anointed
        87389, -- Blade of the Anointed
        87390, -- Blades of the Anointed
        87394, -- Sonic Disruption Fork
        87400, -- Sap Jar
        87401, -- Needler Wings
        87553, -- Red Radish
        87554, -- Sweet Lakemelon
        87555, -- Fuzzy Peach
        87556, -- Black Cherries
        87558, -- Ella's Brew
        87763, -- Ella's Brew
        87790, -- Ancient Guo-Lai Artifact
        87813, -- Zan'thik Shackles
        87841, -- Korven's Experimental Grenades
        87871, -- Massive Kyparite Core
        87874, -- Kyparite Shards
        87878, -- Enormous Kunchong Mandibles
        87903, -- Dread Amber Shards
        88538, -- Sha-Haunted Crystal
        88604, -- Nat's Fishing Journal
        88715, -- Ashes of Warlord Gurthan
        88855, -- Stolen Sri-La Stout
        88894, -- Rescued Serpent
        88895, -- Serpent's Scale
        88907, -- Tiny Spider Eye
        88966, -- Sha Attunement Device
        89052, -- Tiny Bag of Poop
        89053, -- Big Bag of Poop
        89054, -- Stolen Boots
        89113, -- Golden Honey
        89163, -- Requisitioned Firework Launcher
        89169, -- Pristine Manacles of Rebellion
        89602, -- Alliance Flare
        89603, -- Encoded Captain's Log
        89605, -- Nazgrim's Flare Gun
        89612, -- Sully's Flaregun
        89624, -- Sully's Flaregun
        89769, -- Nazgrim's Flare Gun
        89812, -- "Jinyu Princess" Irrigation System
        89813, -- "Thunder King" Pest Repellers
        89814, -- "Earth-Slasher" Master Plow
        89902, -- Empty Pitcher
        89903, -- Thousand-Year Water
        91814, -- Pristine Golden Crownfeather
        91815, -- Pristine Jet Crownfeather
        91816, -- Unbruised Yak Haunch
        91817, -- Funky Rotten Fish
        91819, -- Sturdy Crane Snare
        91821, -- Sturdy Tiger Trap
        91822, -- Sturdy Crab Crate
        91823, -- Animatable Stone
        91834, -- Ancient Bloodcrown Crane
        91835, -- Krasari Elder
        91836, -- Colossal Viseclaw
        91838, -- Lion's Landing Commission
        91846, -- "Distilled" Fuel Barrel
        91848, -- Energized Iron Ore Chunk
        91854, -- Sturdy Crane Snare
        91855, -- Sturdy Tiger Trap
        91856, -- Sturdy Crab Crate
        91869, -- Polluted Viseclaw Meat
        91874, -- Chunk of Mystery Meat
        91875, -- Buzzsaw's Private Label
        91877, -- Domination Point Commission
        91902, -- Universal Remote
        91907, -- Lion's Landing Lumber
        92019, -- The Bilgewater Molotov
        92036, -- Staff of the Monkey King
        92072, -- Korune Codex
        92425, -- Ancient Korune Tablet
        92474, -- Extra-Waxy Alliance Ears
        92475, -- "New" and "Improved" Infrared Heat Focals
        92493, -- Bilgewater Blasting Cap
        92494, -- Hellfire Fragment
        92495, -- Netherstorm Fragment
        92496, -- Blade's Edge Fragment
        92497, -- Shadowmoon Fragment
        92499, -- Sturdy Needle
        92539, -- Untamed Amber
        92557, -- The Metal Brew
        92558, -- Energized Seeds
        92560, -- Mallet Head
        92561, -- Mallet Handle
        92562, -- Harmonic Ointment
        92704, -- Ancient Mogu Essence
        92708, -- Spirit Trap
        92735, -- Meng-do's Essence
        92736, -- Gen-Li's Essence
        92753, -- Spectral Residue
        92756, -- Memory Wine
        92764, -- Kor'kron Armor
        92765, -- Kor'kron Boots
        92766, -- Kor'kron Disguise
        92801, -- Korune Orders
        92802, -- Legacy of the Korune
        92803, -- Sealed Korune Artifact
        92804, -- Weathered Journal
        92950, -- Grummle Disguise Kit
        92975, -- The Pink Marmot
        92976, -- The Rusty Dagger
        92977, -- The Mogu Melon Twist
        92978, -- Bizmo's Boom Room Invitation
        93009, -- Shieldwall Soldier Dog Tag
        93019, -- Sealed Note
        93022, -- Animatable Stone
        93026, -- Hand-Dandy Bug Off Sprayer
        93124, -- Tear of the Sin'dorei
        93159, -- Enchanted Sleeping Dust
        93179, -- Scrap Metal
        93180, -- Re-Configured Remote
        93187, -- Unstable Explosive
        93189, -- Korune Artifact
        93190, -- Sha-Touched Claw
        93191, -- Rusty Valve
        93212, -- Spirit Essence of Varatus
        93362, -- Alliance Flare Gun
        93389, -- Edged Throwing Axe
        93668, -- Saur Fetish
        93731, -- Mogu Codex Fragment
        93733, -- Mask of the Spirit-Caller
        93734, -- Mask of the Dark Mystic
        93735, -- Ritual Artifact
        93736, -- Loa-Infused Blade
        93751, -- Blessed Torch
        93761, -- Arcane Emancipator
        93792, -- Head of the Chamberlain
        93793, -- Shoulder of the Chamberlain
        93794, -- Staff of the Chamberlain
        93795, -- Hair of the Chamberlain
        93796, -- Torso of the Chamberlain
        93803, -- Incantation of Gura
        93805, -- Sunreaver Scout Report
        93806, -- Resonance Siphon
        94116, -- Remnants of the Animus
        94123, -- Attuned Crystal
        94153, -- Storm-Stave of Antonidas
        94155, -- Staff of Antonidas
        94197, -- The Zandalari Prophecy
        94198, -- Rumbles of Thunder
        94199, -- Gods and Monsters
        94288, -- Giant Dinosaur Bone
        94605, -- Wild Pterrorwing Hatchling
        94905, -- Case of Tactical Mana Bombs
        95094, -- Rommath's Book of Incantations
        95340, -- Jaina's Weathered Spellbook
        95352, -- Sunreaver Mana Crystal
        95360, -- Kirin Tor Mana Crystal
        95371, -- Workshop Orders
        95372, -- Choker of Storms
        95374, -- Hoard-Keeper's Key
        95383, -- Pristine Banner of the Mantid Empire
        95384, -- Pristine Ancient Sap Feeder
        95385, -- Pristine Praying Mantid
        95386, -- Pristine Sound Beacon
        95387, -- Pristine Remains of a Paragon
        95388, -- Pristine Mantid Lamp
        95389, -- Pristine Pollen Collector
        95390, -- Pristine Kypari Sap Container
        102215, -- Brew Gathering Bowl
        102225, -- Rolo's Riddle
        102372, -- Grain Catching Sack
        104257, -- Pristine Firestorm Egg
        104264, -- Meaty Crane Leg
        104265, -- Great Turtle Meat
        104266, -- Heavy Yak Flank
        104267, -- Thick Tiger Haunch
        105715, -- Epoch Stone
        86542, -- Flying Tiger Gourami
        86544, -- Spinefish Alpha
        86545, -- Mimic Octopus
        79283, -- Ace of Tigers
        79284, -- Two of Tigers
        79285, -- Three of Tigers
        79286, -- Four of Tigers
        79287, -- Five of Tigers
        79288, -- Six of Tigers
        79289, -- Seven of Tigers
        79290, -- Eight of Tigers
        79291, -- Ace of Oxen
        79292, -- Two of Oxen
        79293, -- Three of Oxen
        79294, -- Four of Oxen
        79295, -- Five of Oxen
        79296, -- Six of Oxen
        79297, -- Seven of Oxen
        79298, -- Eight of Oxen
        79299, -- Ace of Cranes
        79300, -- Two of Cranes
        79301, -- Three of Cranes
        79302, -- Four of Cranes
        79303, -- Five of Cranes
        79304, -- Six of Cranes
        79305, -- Seven of Cranes
        79306, -- Eight of Cranes
        79307, -- Ace of Serpents
        79308, -- Two of Serpents
        79309, -- Three of Serpents
        79310, -- Four of Serpents
        79311, -- Five of Serpents
        79312, -- Six of Serpents
        79313, -- Seven of Serpents
        79314, -- Eight of Serpents
        94221, -- Shan'ze Ritual Stone
        94721, -- Strange Metal Ingot
        105891, -- Moonfang's Pelt
        79269, -- Marsh Lily
        79323, -- Tiger Deck
        79324, -- Ox Deck
        79325, -- Crane Deck
        79326, -- Serpent Deck
        89317, -- Claw of Anger
        92426, -- Sealed Tome of the Lost Legion
        92441, -- The Codex of Xerrath
        92556, -- Empowered Soulcore
        105930, -- Vision of Time
        105931, -- Vision of Time
        105932, -- Vision of Time
        105933, -- Vision of Time
        105934, -- Vision of Time
        105935, -- Vision of Time
        87262, -- Mysterious Note
        143935, -- Commendation of The Klaxxi
        143936, -- Commendation of the Shado-Pan
        143937, -- Commendation of the Golden Lotus
        143938, -- Commendation of The August Celestials
        143939, -- Commendation of the Sunreaver Onslaught
        143940, -- Commendation of the Kirin Tor Offensive
        143941, -- Commendation of The Tillers
        143942, -- Commendation of the Order of the Cloud Serpent
        143943, -- Commendation of the Dominance Offensive
        143944, -- Commendation of Operation: Shieldwall
        143945, -- Commendation of the Shado-Pan Assault
        143946, -- Commendation of The Anglers
        143947, -- Commendation of Emperor Shaohao
        88563   -- Nat's Fishing Journal
    },
    [addon.CONS.RECIPES_ID] = {
        [addon.CONS.R_BOOKS_ID] = {
            82469, -- Ancient Tome of Teleport: Dalaran
            82470, -- Ancient Tome of Portal: Dalaran
            94232  -- Ancient Tome of Dinomancy
        },
        [addon.CONS.R_BLACKSMITHING_ID] = {
            83787, -- Plans: Ghost Reaver's Breastplate
            83788, -- Plans: Ghost Reaver's Gauntlets
            83789, -- Plans: Living Steel Breastplate
            83790, -- Plans: Living Steel Gauntlets
            83791, -- Plans: Breastplate of Ancient Steel
            83792, -- Plans: Gauntlets of Ancient Steel
            84158, -- Plans: Contender's Revenant Belt
            84159, -- Plans: Contender's Revenant Boots
            84160, -- Plans: Contender's Revenant Bracers
            84161, -- Plans: Contender's Revenant Breastplate
            84162, -- Plans: Contender's Revenant Gauntlets
            84163, -- Plans: Contender's Revenant Helm
            84164, -- Plans: Contender's Revenant Legplates
            84165, -- Plans: Contender's Revenant Shoulders
            84166, -- Plans: Contender's Spirit Belt
            84167, -- Plans: Contender's Spirit Boots
            84168, -- Plans: Contender's Spirit Bracers
            84169, -- Plans: Contender's Spirit Breastplate
            84170, -- Plans: Contender's Spirit Gauntlets
            84171, -- Plans: Contender's Spirit Helm
            84172, -- Plans: Contender's Spirit Legplates
            84173, -- Plans: Contender's Spirit Shoulders
            84196, -- Plans: Living Steel Belt Buckle
            84197, -- Plans: Masterwork Forgewire Axe
            84198, -- Plans: Masterwork Ghost Shard
            84208, -- Plans: Masterwork Lightsteel Shield
            84217, -- Plans: Masterwork Phantasmal Hammer
            84218, -- Plans: Masterwork Spiritblade Decimator
            84219, -- Plans: Masterwork Spiritguard Belt
            84220, -- Plans: Masterwork Spiritguard Boots
            84221, -- Plans: Masterwork Spiritguard Bracers
            84222, -- Plans: Masterwork Spiritguard Breastplate
            84223, -- Plans: Masterwork Spiritguard Gauntlets
            84224, -- Plans: Masterwork Spiritguard Helm
            84225, -- Plans: Masterwork Spiritguard Legplates
            84226, -- Plans: Masterwork Spiritguard Shield
            84227, -- Plans: Masterwork Spiritguard Shoulders
            90531, -- Plans: Ghost Iron Shield Spike
            90532, -- Plans: Living Steel Weapon Chain
            94552, -- Plans: Lightning Steel Ingot
            94567, -- Plans: Thunder, Reborn
            94568, -- Plans: Drakefist Hammer, Reborn
            94569, -- Plans: Lunar Crescent, Reborn
            94570, -- Plans: Planar Edge, Reborn
            94571, -- Plans: Lionheart Blade, Reborn
            94572, -- Plans: Fireguard, Reborn
            100865, -- Plans: Balanced Trillium Ingot and Its Uses
            87408, -- Plans: Unyielding Bloodplate
            87409, -- Plans: Gauntlets of Battle Command
            87410, -- Plans: Ornate Battleplate of the Master
            87411, -- Plans: Bloodforged Warfists
            87412, -- Plans: Chestplate of Limitless Faith
            87413  -- Plans: Gauntlets of Unbound Devotion
        },
        [addon.CONS.R_COOKING_ID] = {
            86393, -- Tablet of Ren Yun
            87266, -- Recipe: Banana Infused Rum
            74657, -- Recipe: Spicy Salmon
            74658, -- Recipe: Spicy Vegetable Chips
            85502, -- Recipe: Viseclaw Soup
            85505, -- Recipe: Krasarang Fritters
            101765, -- Recipe: Seasoned Pomfruit Slices
            101766, -- Recipe: Spiced Blossom Soup
            101767, -- Recipe: Mango Ice
            101768, -- Recipe: Farmer's Delight
            101769, -- Recipe: Stuffed Lushrooms
            101770, -- Recipe: Fluffy Silkfeather Omelet
            75013, -- Recipe: Pandaren Banquet
            75017  -- Recipe: Great Pandaren Banquet
        },
        [addon.CONS.R_ENCHANTING_ID] = {
            138877, -- Formula: Tome of Illusions: Secrets of the Shado-Pan
            84557, -- Formula: Enchant Bracer - Greater Agility
            84559, -- Formula: Enchant Bracer - Super Intellect
            84561, -- Formula: Enchant Bracer - Exceptional Strength
            84580, -- Formula: Enchant Weapon - River's Song
            84583, -- Formula: Enchant Weapon - Jade Spirit
            84584 -- Formula: Enchant Weapon - Dancing Steel
        },
        [addon.CONS.R_ENGINEERING_ID] = {
            89992, -- Schematic: Serpent's Heart Firework
            89993, -- Schematic: Grand Celebration Firework
            89994, -- Schematic: Celestial Firework
            89996, -- Schematic: Autumn Flower Firework
            89997, -- Schematic: Jade Blossom Firework
            100910  -- Schematic: Chief Engineer Jard's Journal
        },
        [addon.CONS.R_INSCRIPTIONS_ID] = {
            102534, -- Technique: Crafted Malevolent Gladiator's Medallion of Tenacity
            104219, -- Technique: Glyph of Skeleton
            104223, -- Technique: Glyph of the Unbound Elemental
            104224, -- Technique: Glyph of Evaporation
            104227, -- Technique: Glyph of Pillar of Light
            104228, -- Technique: Glyph of Angels
            104229, -- Technique: Glyph of the Sha
            104231, -- Technique: Glyph of Inspired Hymns
            104234, -- Technique: Glyph of Spirit Raptors
            104235, -- Technique: Glyph of Lingering Ancestors
            104245  -- Technique: Glyph of the Weaponmaster
        },
        [addon.CONS.R_JEWELCRAFTING_ID] = {
            83830, -- Design: Sunstone Panther
            83845, -- Design: Jade Panther
            83877, -- Design: Jeweled Onyx Panther
            83931, -- Design: Ruby Panther
            83932, -- Design: Sapphire Panther
            83811, -- Design: Agile Primal Diamond
            83815, -- Design: Austere Primal Diamond
            83825, -- Design: Burning Primal Diamond
            83840, -- Design: Destructive Primal Diamond
            83842, -- Design: Effulgent Primal Diamond
            83844, -- Design: Ember Primal Diamond
            83848, -- Design: Enigmatic Primal Diamond
            83851, -- Design: Eternal Primal Diamond
            83859, -- Design: Fleet Primal Diamond
            83862, -- Design: Forlorn Primal Diamond
            83872, -- Design: Impassive Primal Diamond
            83901, -- Design: Powerful Primal Diamond
            83925, -- Design: Reverberating Primal Diamond
            83926, -- Design: Revitalizing Primal Diamond
            90470, -- Design: Jade Owl
            90471, -- Design: Sapphire Cub
            95470, -- Design: Serpent's Heart
            95471  -- Design: Primal Diamond
        },
        [addon.CONS.R_LEATHERWORKING_ID] = {
            86235, -- Pattern: Angerhide Leg Armor
            86237, -- Pattern: Chestguard of Earthen Harmony
            86240, -- Pattern: Contender's Dragonscale Belt
            86241, -- Pattern: Contender's Dragonscale Boots
            86242, -- Pattern: Contender's Dragonscale Bracers
            86243, -- Pattern: Contender's Dragonscale Chestguard
            86244, -- Pattern: Contender's Dragonscale Gloves
            86245, -- Pattern: Contender's Dragonscale Helm
            86246, -- Pattern: Contender's Dragonscale Leggings
            86247, -- Pattern: Contender's Dragonscale Shoulders
            86248, -- Pattern: Contender's Leather Belt
            86249, -- Pattern: Contender's Leather Boots
            86250, -- Pattern: Contender's Leather Bracers
            86251, -- Pattern: Contender's Leather Chestguard
            86252, -- Pattern: Contender's Leather Gloves
            86253, -- Pattern: Contender's Leather Helm
            86254, -- Pattern: Contender's Leather Leggings
            86255, -- Pattern: Contender's Leather Shoulders
            86256, -- Pattern: Contender's Scale Belt
            86257, -- Pattern: Contender's Scale Boots
            86258, -- Pattern: Contender's Scale Bracers
            86259, -- Pattern: Contender's Scale Chestguard
            86260, -- Pattern: Contender's Scale Gloves
            86261, -- Pattern: Contender's Scale Helm
            86262, -- Pattern: Contender's Scale Leggings
            86263, -- Pattern: Contender's Scale Shoulders
            86264, -- Pattern: Contender's Wyrmhide Belt
            86265, -- Pattern: Contender's Wyrmhide Boots
            86266, -- Pattern: Contender's Wyrmhide Bracers
            86267, -- Pattern: Contender's Wyrmhide Chestguard
            86268, -- Pattern: Contender's Wyrmhide Gloves
            86269, -- Pattern: Contender's Wyrmhide Helm
            86270, -- Pattern: Contender's Wyrmhide Leggings
            86271, -- Pattern: Contender's Wyrmhide Shoulders
            86273, -- Pattern: Gloves of Earthen Harmony
            86274, -- Pattern: Greyshadow Chestguard
            86275, -- Pattern: Greyshadow Gloves
            86276, -- Pattern: Ironscale Leg Armor
            86277, -- Pattern: Lifekeeper's Gloves
            86278, -- Pattern: Lifekeeper's Robe
            86295, -- Pattern: Shadowleather Leg Armor
            86308, -- Pattern: Wildblood Gloves
            86309, -- Pattern: Wildblood Vest
            102513, -- Pattern: Drums of Rage
            95467, -- Pattern: Magnificence of Leather
            95468, -- Pattern: Magnificence of Scales
            100864, -- Pattern: Hardened Magnificent Hide and Its Uses
            86238, -- Pattern: Chestguard of Nemeses
            86272, -- Pattern: Fists of Lightning
            86279, -- Pattern: Liferuned Leather Gloves
            86280, -- Pattern: Murderer's Gloves
            86281, -- Pattern: Nightfire Robe
            86283, -- Pattern: Raiment of Blood and Bone
            86284, -- Pattern: Raven Lord's Gloves
            86297  -- Pattern: Stormbreaker Chestguard
        },
        [addon.CONS.R_TAILORING_ID] = {
            86352, -- Pattern: Contender's Silk Cowl
            86353, -- Pattern: Contender's Silk Amice
            86354, -- Pattern: Contender's Silk Raiment
            86355, -- Pattern: Contender's Silk Handwraps
            86356, -- Pattern: Contender's Silk Pants
            86357, -- Pattern: Contender's Silk Cuffs
            86358, -- Pattern: Contender's Silk Footwraps
            86359, -- Pattern: Contender's Silk Belt
            86360, -- Pattern: Contender's Satin Cowl
            86361, -- Pattern: Contender's Satin Amice
            86362, -- Pattern: Contender's Satin Raiment
            86363, -- Pattern: Contender's Satin Handwraps
            86364, -- Pattern: Contender's Satin Pants
            86365, -- Pattern: Contender's Satin Cuffs
            86366, -- Pattern: Contender's Satin Footwraps
            86367, -- Pattern: Contender's Satin Belt
            86368, -- Pattern: Spelltwister's Grand Robe
            86369, -- Pattern: Spelltwister's Gloves
            86370, -- Pattern: Robes of Creation
            86371, -- Pattern: Gloves of Creation
            86375, -- Pattern: Greater Pearlescent Spellthread
            86376, -- Pattern: Greater Cerulean Spellthread
            86377, -- Pattern: Royal Satchel
            100863, -- Pattern: Celestial Cloth and Its Uses
            86379, -- Pattern: Robe of Eternal Rule
            86380, -- Pattern: Imperial Silk Gloves
            86381, -- Pattern: Legacy of the Emperor
            86382  -- Pattern: Touch of the Light
        },
    },
    [addon.CONS.TRADE_GOODS_ID] = {
        79255, -- Starlight Ink
        79254, -- Ink of Dreams
        79253, -- Misty Pigment
        79251, -- Shadow Pigment
        [addon.CONS.T_CLOTH_ID] = {
            82447, -- Imperial Silk
            92960, -- Silkworm Cocoon
            98619, -- Celestial Cloth
            82441, -- Bolt of Windwool Cloth
            72988  -- Windwool Cloth
        },
        [addon.CONS.T_ELEMENTAL_ID] = {
            76061, -- Spirit of Harmony
            89112  -- Mote of Harmony
        },
        [addon.CONS.T_ENCHANTING_ID] = {
            74248, -- Sha Crystal
            105718, -- Sha Crystal Fragment
            74247, -- Ethereal Shard
            74252, -- Small Ethereal Shard
            80433, -- Blood Spirit
            94289, -- Haunting Spirit
            102218, -- Spirit of War
            74250, -- Mysterious Essence
            74249, -- Spirit Dust
            89738  -- Essence or Dust
        },
        [addon.CONS.T_PARTS_ID] = {
            94111, -- Lightning Steel Ingot
            94113, -- Jard's Peculiar Energy Source
            98717, -- Balanced Trillium Ingot
            77467, -- Ghost Iron Bolts
            77468, -- High-Explosive Gunpowder
            90146  -- Tinker's Kit
        },
        [addon.CONS.T_HERBS_ID] = {
            72238, -- Golden Lotus
            79011, -- Fool's Cap
            97623, -- Fool's Cap Spores
            79010, -- Snow Lily
            97622, -- Snow Lily Petal
            72235, -- Silkweed
            97621, -- Silkweed Stem
            72234, -- Green Tea Leaf
            97619, -- Torn Green Tea Leaf
            72237, -- Rain Poppy
            97620, -- Rain Poppy Petal
            89639, -- Desecrated Herb
            97624  -- Desecrated Herb Pod
        },
        [addon.CONS.T_JEWELCRAFTING_ID] = {
            76734, -- Serpent's Eye
            76131, -- Primordial Ruby
            76138, -- River's Heart
            76139, -- Wild Jade
            76140, -- Vermilion Onyx
            76141, -- Imperial Amethyst
            76142, -- Sun's Radiance
            76132, -- Primal Diamond
            76130, -- Tiger Opal
            76133, -- Lapis Lazuli
            76134, -- Sunstone
            76135, -- Roguestone
            76136, -- Pandarian Garnet
            76137  -- Alexandrite
        },
        [addon.CONS.T_LEATHER_ID] = {
            72163, -- Magnificent Hide
            98617, -- Hardened Magnificent Hide
            79101, -- Prismatic Scale
            112157, -- Prismatic Scale Fragment
            72120, -- Exotic Leather
            72162  -- Sha-Touched Leather
        },
        [addon.CONS.T_MEAT_ID] = {
            102540, -- Fresh Mangos
            102542, -- Ancient Pandaren Spices
            102543, -- Aged Mogu'shan Cheese
            74659, -- Farm Chicken
            74660, -- Pandaren Peach
            74661, -- Black Pepper
            74662, -- Rice Flour
            74832, -- Barley
            74840, -- Green Cabbage
            74841, -- Juicycrunch Carrot
            74842, -- Mogu Pumpkin
            74843, -- Scallions
            74844, -- Red Blossom Leek
            74845, -- Ginseng
            102541, -- Aged Balsamic Vinegar
            74847, -- Jade Squash
            74848, -- Striped Melon
            74849, -- Pink Turnip
            74850, -- White Turnip
            74851, -- Rice
            74852, -- Yak Milk
            74853, -- 100 Year Soy Sauce
            74854, -- Instant Noodles
            79246, -- Delicate Blossom Petals
            79250, -- Fresh Pomfruit
            85583, -- Needle Mushrooms
            85584, -- Silkworm Pupa
            85585, -- Red Beans
            102536, -- Fresh Lushroom
            102538, -- Fresh Shao-Tien Rice
            102539, -- Fresh Strawberries
            74846,  -- Witchberries
            [addon.CONS.TM_ANIMAL_ID] = {
                85506, -- Viseclaw Meat
                74833, -- Raw Tiger Steak
                74834, -- Mushan Ribs
                74837, -- Raw Turtle Meat
                74838, -- Raw Crab Meat
                74839, -- Wildfowl Breast
                75014  -- Raw Crocolisk Belly
            },
            [addon.CONS.TM_EGG_ID] = {
                102537  -- Fresh Silkfeather Hawk Eggs
            },
            [addon.CONS.TM_FISH_ID] = {
                74856, -- Jade Lungfish
                74857, -- Giant Mantis Shrimp
                74859, -- Emperor Salmon
                74860, -- Redbelly Mandarin
                74861, -- Tiger Gourami
                74863, -- Jewel Danio
                74864, -- Reef Octopus
                74865, -- Krasarang Paddlefish
                74866  -- Golden Carp
            }
        },
        [addon.CONS.T_METAL_STONE_ID] = {
            72094, -- Black Trillium Ore
            72095, -- Trillium Bar
            72103, -- White Trillium Ore
            72104, -- Living Steel
            72093, -- Kyparite
            97546, -- Kyparite Fragment
            72092, -- Ghost Iron Ore
            97512, -- Ghost Iron Nugget
            72096  -- Ghost Iron Bar
        },
        [addon.CONS.T_OTHER_ID] = {
            83064, -- Spinefish
            90407, -- Sparkling Shard
            87658, -- Empty Raw Tiger Steak Container
            87659, -- Empty Mushan Ribs Container
            87660, -- Empty Raw Turtle Meat Container
            87661, -- Empty Raw Crab Meat Container
            87662, -- Empty Wildfowl Breast Container
            87663, -- Empty Green Cabbage Container
            87664, -- Empty Juicycrunch Carrot Container
            87665, -- Empty Mogu Pumpkin Container
            87666, -- Empty Scallions Container
            87667, -- Empty Red Blossom Leek Container
            87669, -- Empty Witchberries Container
            87670, -- Empty Jade Squash Container
            87671, -- Empty Striped Melon Container
            87672, -- Empty Pink Turnip Container
            87673, -- Empty White Turnip Container
            87678, -- Empty Jade Lungfish Container
            87679, -- Empty Giant Mantis Shrimp Container
            87680, -- Empty Emperor Salmon Container
            87681, -- Empty Redbelly Mandarin Container
            87682, -- Empty Tiger Gourami Container
            87683, -- Empty Jewel Danio Container
            87684, -- Empty Reef Octopus Container
            87685, -- Empty Krasarang Paddlefish Container
            87686, -- Empty Golden Carp Container
            87687, -- Empty Crocolisk Belly Container
            78912, -- Silver Filigree Flask
            79318, -- Darkmoon Card of Mists
            80240, -- Strange Spherical Stone
            83092  -- Orb of Mystery
        }
    }
}