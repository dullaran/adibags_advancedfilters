local _, addon = ...

addon.ITEM_DATABASE[addon.CONS.THE_BURNING_CRUSADE_ID] = {
    [addon.CONS.CONTAINERS_ID] = {
        30745, -- Heavy Toolbox
        23389, -- Empty Draenei Supply Pouch
        22571, -- Courier's Bag
        22976, -- Magister's Pouch
        37606, -- Penny Pouch
        21843, -- Imbued Netherweave Bag
        21841, -- Netherweave Bag
        30744, -- Draenic Leather Pack
        23852, -- Nolkai's Bag
        21872, -- Ebon Shadowbag
        21876, -- Primal Mooncloth Bag
        34105, -- Quiver of a Thousand Feathers
        35516, -- Sun Touched Satchel
        27680, -- Halaani Bag
        33117, -- Jack-o'-Lantern
        34845, -- Pit Lord's Satchel
        38082, -- "Gigantique" Bag
        30748, -- Enchanter's Satchel
        21858, -- Spellfire Bag
        23774, -- Fel Iron Toolbox
        23775, -- Titanium Toolbox
        24270, -- Bag of Jewels
        30747, -- Gem Pouch
        38225, -- Mycah's Botanical Bag
        34482, -- Leatherworker's Satchel
        34490, -- Bag of Many Hides
        29540, -- Reinforced Mining Bag
        30746  -- Mining Sack
    },
    [addon.CONS.CONSUMABLES_ID] = {
        [addon.CONS.C_BANDAGES_ID] = {
            21991, -- Heavy Netherweave Bandage
            21990  -- Netherweave Bandage
        },
        [addon.CONS.C_CONSUMABLES_ID] = {
            22536, -- Formula: Enchant Ring - Minor Intellect
            23819, -- Elemental Seaforium Charge
            23841, -- Gnomish Flame Turret
            34113, -- Field Repair Bot 110G
            23827, -- Super Sapper Charge
            23737, -- Adamantite Grenade
            23826, -- The Bigger One
            32413, -- Frost Grenade
            23821, -- Zapthrottle Mote Extractor
            23736, -- Fel Iron Bomb
            22728, -- Steam Tonk Controller
            31666, -- Battered Steam Tonk Controller
            30847, -- X-52 Rocket Helmet
            22541, -- Formula: Enchant Shield - Resistance
            29483, -- Shadow Armor Kit
            29485, -- Flame Armor Kit
            29486, -- Frost Armor Kit
            29487, -- Nature Armor Kit
            29488, -- Arcane Armor Kit
            29669, -- Pattern: Shadow Armor Kit
            29672, -- Pattern: Flame Armor Kit
            29673, -- Pattern: Frost Armor Kit
            29674, -- Pattern: Nature Armor Kit
            29675, -- Pattern: Arcane Armor Kit
            22548, -- Formula: Enchant Cloak - Major Resistance
            23767, -- Crashin' Thrashin' Robot
            38506, -- Don Carlos' Famous Hat
            28276, -- Formula: Enchant Cloak - Greater Arcane Resistance
            28277, -- Formula: Enchant Cloak - Greater Shadow Resistance
            30542, -- Dimensional Ripper - Area 52
            30544, -- Ultrasafe Transporter: Toshley's Station
            33176  -- Flying Broom
        },
        [addon.CONS.C_ELIXIRS_ID] = {
            22835, -- Elixir of Major Shadow Power
            22840, -- Elixir of Major Mageblood
            22848, -- Elixir of Empowerment
            34537, -- Bloodberry Elixir
            22834, -- Elixir of Major Defense
            31679, -- Fel Strength Elixir
            22831, -- Elixir of Major Agility
            32068, -- Elixir of Ironskin
            22830, -- Elixir of the Searching Eye
            22827, -- Elixir of Major Frost Power
            22833, -- Elixir of Major Firepower
            32067, -- Elixir of Draenic Wisdom
            28104, -- Elixir of Mastery
            32063, -- Earthen Elixir
            22823, -- Elixir of Camouflage
            22824, -- Elixir of Major Strength
            22825, -- Elixir of Healing Power
            32062, -- Elixir of Major Fortitude
            25539, -- Potion of Water Breathing
            28102, -- Onslaught Elixir
            28103, -- Adept's Elixir
            34130, -- Recovery Diver's Potion
            23444  -- Goldenmist Special Brew
        },
        [addon.CONS.C_FLASKS_ID] = {
            22851, -- Flask of Fortification
            22853, -- Flask of Mighty Versatility
            22854, -- Flask of Relentless Assault
            22861, -- Flask of Blinding Light
            22866, -- Flask of Pure Death
            32596, -- Unstable Flask of the Elder
            32597, -- Unstable Flask of the Soldier
            32598, -- Unstable Flask of the Beast
            32599, -- Unstable Flask of the Bandit
            32600, -- Unstable Flask of the Physician
            32601, -- Unstable Flask of the Sorcerer
            32898, -- Shattrath Flask of Fortification
            32899, -- Shattrath Flask of Mighty Restoration
            32900, -- Shattrath Flask of Supreme Power
            32901, -- Shattrath Flask of Relentless Assault
            35716, -- Shattrath Flask of Pure Death
            35717  -- Shattrath Flask of Blinding Light
        },
        [addon.CONS.C_FOOD_DRINKS_ID] = {
            28112, -- Underspore Pod
            22018, -- Conjured Glacier Water
            22019, -- Conjured Croissant
            27860, -- Purified Draenic Water
            29394, -- Lyribread
            29395, -- Ethermead
            29401, -- Sparkling Southshore Cider
            29448, -- Mag'har Mild Cheese
            29449, -- Bladespire Bagel
            29450, -- Telaari Grapes
            29451, -- Clefthoof Ribs
            29452, -- Zangar Trout
            29453, -- Sporeggar Mushroom
            30355, -- Grilled Shadowmoon Tuber
            30357, -- Oronok's Tuber of Healing
            30358, -- Oronok's Tuber of Agility
            30359, -- Oronok's Tuber of Strength
            30361, -- Oronok's Tuber of Spell Power
            30457, -- Gilneas Sparkling Water
            32453, -- Star's Tears
            32667, -- Bash Ale
            32668, -- Dos Ogris
            32685, -- Ogri'la Chicken Fingers
            32686, -- Mingo's Fortune Giblets
            32722, -- Enriched Terocone Juice
            33042, -- Black Coffee
            33048, -- Stewed Trout
            33052, -- Fisherman's Feast
            33053, -- Hot Buttered Trout
            33825, -- Skullfish Soup
            33872, -- Spicy Hot Talbuk
            34062, -- Conjured Mana Biscuit
            34411, -- Hot Apple Cider
            34780, -- Naaru Ration
            38428, -- Rock-Salted Pretzel
            38431, -- Blackrock Fortified Water
            28399, -- Filtered Draenic Water
            29454, -- Silverwine
            30703, -- Conjured Mountain Spring Water
            38430, -- Blackrock Mineral Water
            24008, -- Dried Mushroom Rations
            24009, -- Dried Fruit Rations
            24338, -- Hellfire Spineleaf
            24539, -- Marsh Lichen
            27651, -- Buzzard Bites
            27655, -- Ravager Dog
            27657, -- Blackened Basilisk
            27658, -- Roasted Clefthoof
            27659, -- Warp Burger
            27660, -- Talbuk Steak
            27661, -- Blackened Trout
            27662, -- Feltail Delight
            27663, -- Blackened Sporefish
            27664, -- Grilled Mudfish
            27665, -- Poached Bluefish
            27666, -- Golden Fish Sticks
            27667, -- Spicy Crawdad
            27854, -- Smoked Talbuk Venison
            27855, -- Mag'har Grainbread
            27856, -- Skethyl Berries
            27857, -- Garadar Sharp
            27858, -- Sunspring Carp
            27859, -- Zangar Caps
            28486, -- Moser's Magnificent Muffin
            28501, -- Ravager Egg Omelet
            29292, -- Helboar Bacon
            29393, -- Diamond Berries
            29412, -- Jessen's Special Slop
            30155, -- Clam Bar
            30458, -- Stromgarde Muenster
            30610, -- Smoked Black Bear Meat
            31672, -- Mok'Nathal Shortribs
            31673, -- Crunchy Serpent
            32455, -- Star's Lament
            32721, -- Skyguard Rations
            33866, -- Stormchops
            33867, -- Broiled Bloodfin
            38427, -- Pickled Egg
            35563, -- Charred Bear Kabobs
            35565, -- Juicy Bear Burger
            38429, -- Blackrock Spring Water
            35720, -- Lord of Frost's Private Label
            23848, -- Nethergarde Bitter
            38466, -- Sulfuron Slammer
            38432, -- Plugger's Blackrock Ale
            24105, -- Roasted Moongraze Tenderloin
            27635, -- Lynx Steak
            22645, -- Crunchy Spider Surprise
            24072, -- Sand Pear Pie
            27636, -- Bat Bites
            20857, -- Honey Bread
            23495, -- Springpaw Appetizer
            23756, -- Cookie's Jumbo Gumbo
            28284, -- Don Carlos Tequila
            29112, -- Cenarion Spirits
            30816, -- Spice Bread
            33874, -- Kibler's Bits
            33924, -- Delicious Chocolate Cake
            34832  -- Captain Rumsey's Lager
        },
        [addon.CONS.C_ITEM_ENHANCEMENTS_ID] = {
            23530, -- Felsteel Shield Spike
            29187, -- Inscription of Endurance
            34207, -- Glove Reinforcements
            34836, -- Spun Truesilver Fishing Line
            23764, -- Adamantite Scope
            28878, -- Inscription of Faith
            28881, -- Inscription of Discipline
            28882, -- Inscription of Warding
            28885, -- Inscription of Vengeance
            28903, -- Inscription of the Orb
            28904, -- Inscription of the Oracle
            28907, -- Inscription of the Blade
            28908, -- Inscription of the Knight
            33185, -- Adamantite Weapon Chain
            23766, -- Stabilized Eternium Scope
            23765, -- Khorium Scope
            28886, -- Greater Inscription of Discipline
            28887, -- Greater Inscription of Faith
            28888, -- Greater Inscription of Vengeance
            28889, -- Greater Inscription of Warding
            28909, -- Greater Inscription of the Orb
            28910, -- Greater Inscription of the Blade
            28911, -- Greater Inscription of the Knight
            28912, -- Greater Inscription of the Oracle
            24273, -- Mystic Spellthread
            24275, -- Silver Spellthread
            29533, -- Cobrahide Leg Armor
            29534, -- Clefthide Leg Armor
            24274, -- Runic Spellthread
            24276, -- Golden Spellthread
            29535, -- Nethercobra Leg Armor
            29536, -- Nethercleft Leg Armor
            31535, -- Bloodboil Poison
            23576, -- Greater Ward of Shielding
            25521, -- Greater Rune of Warding
            34538, -- Blessed Weapon Coating
            34539, -- Righteous Weapon Coating
            22522, -- Superior Wizard Oil
            23559, -- Lesser Rune of Warding
            23575, -- Lesser Ward of Shielding
            22521, -- Superior Mana Oil
            23528, -- Fel Sharpening Stone
            25679, -- Comfortable Insoles
            28420, -- Fel Weightstone
            34861, -- Sharpened Fish Hook
            23529, -- Adamantite Sharpening Stone
            28421  -- Adamantite Weightstone
        },
        [addon.CONS.C_POTIONS_ID] = {
            22850, -- Super Rejuvenation Potion
            34440, -- Mad Alchemist's Potion
            22849, -- Ironshield Potion
            31677, -- Fel Mana Potion
            22836, -- Major Dreamless Sleep Potion
            22837, -- Heroic Potion
            22838, -- Haste Potion
            22839, -- Destruction Potion
            22841, -- Major Fire Protection Potion
            22842, -- Major Frost Protection Potion
            22844, -- Major Nature Protection Potion
            22845, -- Major Arcane Protection Potion
            22846, -- Major Shadow Protection Potion
            22847, -- Major Holy Protection Potion
            31853, -- Major Combat Healing Potion
            31855, -- Major Combat Mana Potion
            32783, -- Blue Ogre Brew
            32784, -- Red Ogre Brew
            32840, -- Major Arcane Protection Potion
            32844, -- Major Nature Protection Potion
            32845, -- Major Shadow Protection Potion
            32846, -- Major Fire Protection Potion
            32847, -- Major Frost Protection Potion
            32909, -- Blue Ogre Brew Special
            32910, -- Red Ogre Brew Special
            35287, -- Luminous Bluetail
            31676, -- Fel Regeneration Potion
            33093, -- Mana Potion Injector
            22832, -- Super Mana Potion
            32902, -- Bottled Nethergon Energy
            32903, -- Cenarion Mana Salve
            32948, -- Auchenai Mana Potion
            22871, -- Shrouding Potion
            33092, -- Healing Potion Injector
            22829, -- Super Healing Potion
            32904, -- Cenarion Healing Salve
            32905, -- Bottled Nethergon Vapor
            32947, -- Auchenai Healing Potion
            22828, -- Insane Strength Potion
            22826, -- Sneaking Potion
            28101, -- Unstable Mana Potion
            33935, -- Crystal Mana Potion
            28100, -- Volatile Healing Potion
            33934  -- Crystal Healing Potion
        },
        [addon.CONS.C_OTHER_ID] = {
            31449, -- Distilled Stalker Sight
            31450, -- Stealth of the Stalker
            31451, -- Pure Energy
            33026, -- The Golden Link
            33035, -- Ogre Mead
            33036, -- Mudder's Milk
            33254, -- Forest Strider Drumstick
            34410, -- Honeyed Holiday Ham
            23862, -- Redemption of the Fallen
            29778, -- Phase Disruptor
            31337, -- Orb of the Blackwhelp
            32839, -- Cauldron of Major Arcane Protection
            32849, -- Cauldron of Major Fire Protection
            32850, -- Cauldron of Major Frost Protection
            32851, -- Cauldron of Major Nature Protection
            32852, -- Cauldron of Major Shadow Protection
            33236, -- Fizzy Faire Drink "Classic"
            34475, -- Arcane Charges
            22044, -- Mana Emerald
            22795, -- Fel Blossom
            29482, -- Ethereum Essence
            33025, -- Spicy Smoked Sausage
            33034, -- Gordok Grog
            33246, -- Funnel Cake
            24429, -- Expedition Flare
            24268, -- Netherweave Net
            24520, -- Honor Hold Favor
            24522, -- Thrallmar Favor
            24579, -- Mark of Honor Hold
            24581, -- Mark of Thrallmar
            28038, -- Seaforium PU-36 Explosive Nether Modulator
            28132, -- Area 52 Special
            23585, -- Stouthammer Lite
            24006, -- Grunt's Waterskin
            24007, -- Footman's Waterskin
            33024, -- Pickled Sausage
            33033, -- Thunderbrew Stout
            33234, -- Iced Berry Slush
            34022, -- Stout Shrunken Head
            25548, -- Tallstalk Mushroom
            25550, -- Redcap Toadstool
            25883, -- Dense Stone Statue
            27498, -- Scroll of Agility V
            27499, -- Scroll of Intellect V
            27500, -- Scroll of Protection V
            27501, -- Scroll of Versatility V
            27502, -- Scroll of Stamina V
            27503, -- Scroll of Strength V
            33023, -- Savory Sausage
            33032, -- Thunderbrew Ale
            34021, -- Brewdoo Magic
            34412, -- Sparkling Apple Cider
            25882, -- Solid Stone Statue
            33031, -- Thunder 45
            34020, -- Jungle River Water
            34064, -- Succulent Sausage
            25881, -- Heavy Stone Statue
            30309, -- Stonebreaker Brew
            30499, -- Brightsong Wine
            33029, -- Barleybrew Dark
            34019, -- Path of Brew
            34063, -- Dried Sausage
            25880, -- Coarse Stone Statue
            37750, -- Fresh Brewfest Hops
            39476, -- Fresh Goblin Brewfest Hops
            39477, -- Fresh Dwarven Brewfest Hops
            27553, -- Crimson Steer Energy Drink
            33028, -- Barleybrew Light
            34018, -- Long Stride Brew
            34065, -- Spiced Onion Cheese
            22778, -- Scourgebane Infusion
            22779, -- Scourgebane Draught
            25498, -- Rough Stone Statue
            21038, -- Hardpacked Snowball
            23329, -- Enriched Lasher Root
            33030, -- Barleybrew Clear
            33043, -- The Essential Brewfest Pretzel
            33083, -- Orcish Grog
            33218, -- Goblin Gumbo
            34017, -- Small Step Brew
            37496, -- Binary Brew
            37497, -- Autumnal Acorn Ale
            37902, -- Springtime Stout
            37903, -- Blackrock Lager
            37904, -- Stranglethorn Brew
            37905, -- Draenic Pale Ale
            38518, -- Cro's Apple
            23215, -- Bag of Smorc Ingredients
            23334, -- Cracked Power Core
            23354, -- Crystalized Mana Residue
            23361, -- Cleansing Vial
            23381, -- Chipped Power Core
            23386, -- Condensed Mana Powder
            23417, -- Sanctified Crystal
            23492, -- Suntouched Special Reserve
            23584, -- Loch Modan Lager
            23586, -- Aerie Peak Pale Ale
            23645, -- Seer's Relic
            23768, -- White Smoke Flare
            23769, -- Red Smoke Flare
            23771, -- Green Smoke Flare
            23857, -- Legacy of the Mountain King
            23864, -- Torment of the Worgen
            23865, -- Wrath of the Titans
            23985, -- Crystal of Vitality
            23986, -- Crystal of Insight
            23989, -- Crystal of Ferocity
            24289, -- Chrono-Beacon
            24330, -- Drain Schematics
            24355, -- Ironvine Seeds
            24407, -- Uncatalogued Species
            24408, -- Edible Stalks
            24421, -- Nagrand Cherry
            24474, -- Violet Scrying Crystal
            24494, -- Tears of the Goddess
            24538, -- Fire Bomb
            24540, -- Edible Fern
            25465, -- Stormcrow Amulet
            25886, -- Purple Smoke Flare
            27317, -- Elemental Sapta
            28513, -- Demonic Rune Stone
            28554, -- Shredder Spare Parts
            28571, -- Blank Scroll
            28580, -- B'naar Console Transcription
            28607, -- Sunfury Disguise
            28635, -- Sunfury Arcanist Robes
            28636, -- Sunfury Researcher Gloves
            28637, -- Sunfury Guardsman Medallion
            28784, -- Unyielding Banner Scrap
            29162, -- Galaxis Soul Shard
            29324, -- Warp-Attuned Orb
            29443, -- Bloodmaul Brutebane Brew
            29624, -- First Half of Socrethar's Stone
            29625, -- Second Half of Socrethar's Stone
            29699, -- Socrethar's Teleportation Stone
            29735, -- Holy Dust
            29736, -- Arcane Rune
            29905, -- Kael's Vial Remnant
            29906, -- Vashj's Vial Remnant
            30260, -- Voren'thal's Package
            30540, -- Tally's Waiver (Unsigned)
            30615, -- Halaani Whiskey
            30811, -- Scroll of Demonic Unbanishing
            30858, -- Peon Sleep Potion
            31121, -- Costume Scraps
            31122, -- Overseer Disguise
            31495, -- Grishnath Orb
            31517, -- Dire Pinfeather
            31518, -- Exorcism Feather
            31702, -- Challenge From the Horde
            31795, -- Draenei Prayer Beads
            32406, -- Skyguard Blasting Charges
            32408, -- Naj'entus Spine
            32576, -- Depleted Crystal Focus
            32848, -- Explosives Package
            32971, -- Water Bucket
            33081, -- Voodoo Skull
            33226, -- Tricky Treat
            33277, -- Tome of Thomas Thomson
            33797, -- Portable Brewfest Keg
            33929, -- Brewfest Brew
            33956, -- Echo Isles Pale Ale
            34068, -- Weighted Jack-o'-Lantern
            34077, -- Crudely Wrapped Gift
            34258, -- Love Rocket
            34583, -- Aldor Supplies Package
            34584, -- Scryer Supplies Package
            34585, -- Scryer Supplies Package
            34587, -- Aldor Supplies Package
            34592, -- Aldor Supplies Package
            34593, -- Scryer Supplies Package
            34594, -- Scryer Supplies Package
            34595, -- Aldor Supplies Package
            34599, -- Juggling Torch
            34850, -- Midsummer Ground Flower
            35232, -- Shattered Sun Supplies
            35512, -- Pocket Full of Snow
            36748, -- Dark Brewmaiden's Brew
            36799, -- Mana Gem
            36877, -- Folded Letter
            37582, -- Pyroblast Cinnamon Ball
            37583, -- G.N.E.R.D.S.
            37584, -- Soothing Spearmint Candy
            37585, -- Chewy Fel Taffy
            37604, -- Tooth Pick
            38186, -- Ethereal Credit
            38233, -- Path of Illidan
            38294, -- Ethereal Liqueur
            38308, -- Ethereal Essence Sphere
            38320, -- Dire Brew
            38577, -- Party G.R.E.N.A.D.E.
            31437, -- Medicinal Drake Essence
            29532, -- Drums of Panic
            29529, -- Drums of Battle
            29531, -- Drums of Restoration
            33930, -- Amani Charm of the Bloodletter
            33931, -- Amani Charm of Mighty Mojo
            33932, -- Amani Charm of the Witch Doctor
            33933, -- Amani Charm of the Raging Defender
            29530, -- Drums of Speed
            29528, -- Drums of War
            33865, -- Amani Hex Stick
            30690, -- Power Converter
            27388, -- Mr. Pinchy
            35945, -- Brilliant Glass
            32542, -- Imp in a Ball
            33079, -- Murloc Costume
            33219, -- Goblin Gumbo Kettle
            33926, -- Sealed Scroll Case
            33927, -- Brewfest Pony Keg
            33928, -- Hollowed Bone Decanter
            34686, -- Brazier of Dancing Flames
            35223, -- Papa Hummel's Old-Fashioned Pet Biscuit
            38291, -- Ethereal Mutagen
            38300  -- Diluted Ethereum Essence
        }
    },
    [addon.CONS.CURRENCY_ID] = {
        26044, -- Halaa Research Token
        26045  -- Halaa Battle Token
    },
    [addon.CONS.GEMS_ID] = {
        28458, -- Bold Tourmaline
        28459, -- Delicate Tourmaline
        28461, -- Brilliant Tourmaline
        28463, -- Solid Zircon
        28464, -- Sparkling Zircon
        28467, -- Smooth Amber
        28468, -- Rigid Zircon
        28470, -- Subtle Amber
        23094, -- Brilliant Blood Garnet
        23095, -- Bold Blood Garnet
        23098, -- Inscribed Flame Spessarite
        23099, -- Reckless Flame Spessarite
        23100, -- Glinting Shadow Draenite
        23101, -- Potent Flame Spessarite
        23103, -- Radiant Deep Peridot
        23104, -- Jagged Deep Peridot
        23105, -- Regal Deep Peridot
        23108, -- Timeless Shadow Draenite
        23109, -- Purified Shadow Draenite
        23110, -- Shifting Shadow Draenite
        23111, -- Sovereign Shadow Draenite
        23114, -- Smooth Golden Draenite
        23115, -- Subtle Golden Draenite
        23116, -- Rigid Azure Moonstone
        23118, -- Solid Azure Moonstone
        23119, -- Sparkling Azure Moonstone
        23120, -- Stormy Azure Moonstone
        28290, -- Smooth Golden Draenite
        28595, -- Delicate Blood Garnet
        31866, -- Veiled Shadow Draenite
        31869, -- Deadly Flame Spessarite
        32833, -- Purified Jaggal Pearl
        22460, -- Prismatic Sphere
        24027, -- Bold Living Ruby
        24028, -- Delicate Living Ruby
        24030, -- Brilliant Living Ruby
        24032, -- Subtle Dawnstone
        24033, -- Solid Star of Elune
        24035, -- Sparkling Star of Elune
        24036, -- Flashing Living Ruby
        24039, -- Stormy Star of Elune
        24048, -- Smooth Dawnstone
        24051, -- Rigid Star of Elune
        24053, -- Mystic Dawnstone
        24054, -- Sovereign Nightseye
        24055, -- Shifting Nightseye
        24056, -- Timeless Nightseye
        24058, -- Inscribed Noble Topaz
        24059, -- Potent Noble Topaz
        24060, -- Reckless Noble Topaz
        24061, -- Glinting Nightseye
        24065, -- Purified Nightseye
        24066, -- Radiant Talasite
        24067, -- Jagged Talasite
        25890, -- Destructive Skyfire Diamond
        25893, -- Mystical Skyfire Diamond
        25894, -- Swift Skyfire Diamond
        25895, -- Enigmatic Skyfire Diamond
        25896, -- Powerful Earthstorm Diamond
        25897, -- Bracing Earthstorm Diamond
        25898, -- Tenacious Earthstorm Diamond
        25899, -- Brutal Earthstorm Diamond
        25901, -- Insightful Earthstorm Diamond
        28556, -- Swift Windfire Diamond
        28557, -- Quickened Starfire Diamond
        31867, -- Veiled Nightseye
        31868, -- Deadly Noble Topaz
        32409, -- Relentless Earthstorm Diamond
        32410, -- Thundering Skyfire Diamond
        32634, -- Shifting Amethyst
        32635, -- Timeless Amethyst
        32636, -- Purified Amethyst
        32637, -- Deadly Citrine
        32638, -- Reckless Citrine
        32639, -- Jagged Mossjewel
        32640, -- Tense Unstable Diamond
        32641, -- Imbued Unstable Diamond
        33782, -- Steady Talasite
        34220, -- Chaotic Skyfire Diamond
        34831, -- Eye of the Sea
        35315, -- Quick Dawnstone
        35318, -- Forceful Talasite
        35501, -- Eternal Earthstorm Diamond
        35503, -- Ember Skyfire Diamond
        35707, -- Regal Talasite
        27777, -- Brilliant Blood Garnet
        27786, -- Jagged Deep Peridot
        27809, -- Jagged Deep Peridot
        27812, -- Brilliant Blood Garnet
        28360, -- Delicate Blood Garnet
        28361, -- Delicate Blood Garnet
        30571, -- Don Rodrigo's Heart
        30598, -- Don Amancio's Heart
        32836, -- Purified Shadow Pearl
        22459, -- Void Sphere
        30546, -- Sovereign Tanzanite
        30547, -- Reckless Fire Opal
        30548, -- Jagged Chrysoprase
        30549, -- Shifting Tanzanite
        30550, -- Misty Chrysoprase
        30551, -- Reckless Fire Opal
        30552, -- Timeless Tanzanite
        30553, -- Glinting Tanzanite
        30554, -- Stalwart Fire Opal
        30555, -- Timeless Tanzanite
        30556, -- Glinting Tanzanite
        30558, -- Stalwart Fire Opal
        30559, -- Etched Tanzanite
        30560, -- Misty Chrysoprase
        30563, -- Regal Chrysoprase
        30564, -- Veiled Tanzanite
        30565, -- Jagged Chrysoprase
        30566, -- Defender's Tanzanite
        30572, -- Purified Tanzanite
        30573, -- Mysterious Tanzanite
        30574, -- Shifting Tanzanite
        30575, -- Nimble Chrysoprase
        30581, -- Willful Fire Opal
        30582, -- Deadly Fire Opal
        30583, -- Timeless Tanzanite
        30584, -- Inscribed Fire Opal
        30585, -- Polished Fire Opal
        30586, -- Purified Tanzanite
        30587, -- Champion's Fire Opal
        30588, -- Potent Fire Opal
        30589, -- Purified Tanzanite
        30590, -- Regal Chrysoprase
        30591, -- Lucent Fire Opal
        30592, -- Steady Chrysoprase
        30593, -- Potent Fire Opal
        30594, -- Regal Chrysoprase
        30600, -- Purified Tanzanite
        30601, -- Steady Chrysoprase
        30602, -- Jagged Chrysoprase
        30603, -- Purified Tanzanite
        30604, -- Resplendent Fire Opal
        30605, -- Nimble Chrysoprase
        30606, -- Lightning Chrysoprase
        30607, -- Splendid Fire Opal
        30608, -- Radiant Chrysoprase
        31116, -- Timeless Amethyst
        31117, -- Tireless Soothing Amethyst
        31118, -- Sovereign Amethyst
        32193, -- Bold Crimson Spinel
        32194, -- Delicate Crimson Spinel
        32196, -- Brilliant Crimson Spinel
        32198, -- Subtle Lionseye
        32199, -- Flashing Crimson Spinel
        32200, -- Solid Empyrean Sapphire
        32201, -- Sparkling Empyrean Sapphire
        32203, -- Stormy Empyrean Sapphire
        32205, -- Smooth Lionseye
        32206, -- Rigid Empyrean Sapphire
        32209, -- Mystic Lionseye
        32211, -- Sovereign Shadowsong Amethyst
        32212, -- Shifting Shadowsong Amethyst
        32215, -- Timeless Shadowsong Amethyst
        32217, -- Inscribed Pyrestone
        32218, -- Potent Pyrestone
        32220, -- Glinting Shadowsong Amethyst
        32221, -- Veiled Shadowsong Amethyst
        32222, -- Deadly Pyrestone
        32223, -- Regal Seaspray Emerald
        32224, -- Radiant Seaspray Emerald
        32225, -- Purified Shadowsong Amethyst
        32226, -- Jagged Seaspray Emerald
        33131, -- Crimson Sun
        33133, -- Don Julio's Heart
        33134, -- Kailee's Rose
        33135, -- Falling Star
        33140, -- Blood of Amber
        33143, -- Stone of Blades
        33144, -- Facet of Eternity
        35487, -- Delicate Crimson Spinel
        35488, -- Brilliant Crimson Spinel
        35758, -- Steady Seaspray Emerald
        35759, -- Forceful Seaspray Emerald
        35760, -- Reckless Pyrestone
        35761, -- Quick Lionseye
        27679  -- Mystic Dawnstone
    },
    [addon.CONS.KEYS_ID] = {
        27808  -- Jump-a-Tron 4000 Key
    },
    [addon.CONS.MISCELLANEOUS_ID] = {
        [addon.CONS.M_ARMOR_TOKENS_ID] = {
            29753, -- Chestguard of the Fallen Defender
            29754, -- Chestguard of the Fallen Champion
            29755, -- Chestguard of the Fallen Hero
            29756, -- Gloves of the Fallen Hero
            29757, -- Gloves of the Fallen Champion
            29758, -- Gloves of the Fallen Defender
            29759, -- Helm of the Fallen Hero
            29760, -- Helm of the Fallen Champion
            29761, -- Helm of the Fallen Defender
            29762, -- Pauldrons of the Fallen Hero
            29763, -- Pauldrons of the Fallen Champion
            29764, -- Pauldrons of the Fallen Defender
            29765, -- Leggings of the Fallen Hero
            29766, -- Leggings of the Fallen Champion
            29767, -- Leggings of the Fallen Defender
            30236, -- Chestguard of the Vanquished Champion
            30237, -- Chestguard of the Vanquished Defender
            30238, -- Chestguard of the Vanquished Hero
            30239, -- Gloves of the Vanquished Champion
            30240, -- Gloves of the Vanquished Defender
            30241, -- Gloves of the Vanquished Hero
            30242, -- Helm of the Vanquished Champion
            30243, -- Helm of the Vanquished Defender
            30244, -- Helm of the Vanquished Hero
            30245, -- Leggings of the Vanquished Champion
            30246, -- Leggings of the Vanquished Defender
            30247, -- Leggings of the Vanquished Hero
            30248, -- Pauldrons of the Vanquished Champion
            30249, -- Pauldrons of the Vanquished Defender
            30250, -- Pauldrons of the Vanquished Hero
            31089, -- Chestguard of the Forgotten Conqueror
            31090, -- Chestguard of the Forgotten Vanquisher
            31091, -- Chestguard of the Forgotten Protector
            31092, -- Gloves of the Forgotten Conqueror
            31093, -- Gloves of the Forgotten Vanquisher
            31094, -- Gloves of the Forgotten Protector
            31095, -- Helm of the Forgotten Protector
            31096, -- Helm of the Forgotten Vanquisher
            31097, -- Helm of the Forgotten Conqueror
            31098, -- Leggings of the Forgotten Conqueror
            31099, -- Leggings of the Forgotten Vanquisher
            31100, -- Leggings of the Forgotten Protector
            31101, -- Pauldrons of the Forgotten Conqueror
            31102, -- Pauldrons of the Forgotten Vanquisher
            31103, -- Pauldrons of the Forgotten Protector
            34848, -- Bracers of the Forgotten Conqueror
            34851, -- Bracers of the Forgotten Protector
            34852, -- Bracers of the Forgotten Vanquisher
            34853, -- Belt of the Forgotten Conqueror
            34854, -- Belt of the Forgotten Protector
            34855, -- Belt of the Forgotten Vanquisher
            34856, -- Boots of the Forgotten Conqueror
            34857, -- Boots of the Forgotten Protector
            34858  -- Boots of the Forgotten Vanquisher
        },
        [addon.CONS.M_HOLIDAY_ID] = {
            35557, -- Huge Snowball
            21560, -- Small Purple Rocket
            21591, -- Large Purple Rocket
            31880, -- Blood Elf Orphan Whistle
            31881, -- Draenei Orphan Whistle
            34191, -- Handful of Snowflakes
            34684, -- Handful of Summer Petals
            37586, -- Handful of Treats
            37710, -- Crashin' Thrashin' Racer Controller
            37816, -- Preserved Brewfest Hops
            34480  -- Romantic Picnic Basket
        },
        [addon.CONS.M_MOUNTS_ID] = {
            25470, -- Golden Gryphon
            25471, -- Ebon Gryphon
            25472, -- Snowy Gryphon
            25474, -- Tawny Wind Rider
            25475, -- Blue Wind Rider
            25476, -- Green Wind Rider
            34060, -- Flying Machine
            28481, -- Brown Elekk
            28927, -- Red Hawkstrider
            29220, -- Blue Hawkstrider
            29221, -- Black Hawkstrider
            29222, -- Purple Hawkstrider
            29743, -- Purple Elekk
            29744, -- Gray Elekk
            33976, -- Brewfest Ram
            43599, -- Big Blizzard Bear
            37011, -- Magic Broom
            25473, -- Swift Blue Gryphon
            25477, -- Swift Red Wind Rider
            25527, -- Swift Red Gryphon
            25528, -- Swift Green Gryphon
            25529, -- Swift Purple Gryphon
            25531, -- Swift Green Wind Rider
            25532, -- Swift Yellow Wind Rider
            25533, -- Swift Purple Wind Rider
            32314, -- Green Riding Nether Ray
            32316, -- Purple Riding Nether Ray
            32317, -- Red Riding Nether Ray
            32318, -- Silver Riding Nether Ray
            32319, -- Blue Riding Nether Ray
            32458, -- Ashes of Al'ar
            32857, -- Reins of the Onyx Netherwing Drake
            32858, -- Reins of the Azure Netherwing Drake
            32859, -- Reins of the Cobalt Netherwing Drake
            32860, -- Reins of the Purple Netherwing Drake
            32861, -- Reins of the Veridian Netherwing Drake
            32862, -- Reins of the Violet Netherwing Drake
            33999, -- Cenarion War Hippogryph
            34061, -- Turbo-Charged Flying Machine
            28915, -- Reins of the Dark Riding Talbuk
            28936, -- Swift Pink Hawkstrider
            29102, -- Reins of the Cobalt War Talbuk
            29103, -- Reins of the White War Talbuk
            29104, -- Reins of the Silver War Talbuk
            29105, -- Reins of the Tan War Talbuk
            29223, -- Swift Green Hawkstrider
            29224, -- Swift Purple Hawkstrider
            29227, -- Reins of the Cobalt War Talbuk
            29228, -- Reins of the Dark War Talbuk
            29229, -- Reins of the Silver War Talbuk
            29230, -- Reins of the Tan War Talbuk
            29231, -- Reins of the White War Talbuk
            29465, -- Black Battlestrider
            29466, -- Black War Kodo
            29467, -- Black War Ram
            29468, -- Black War Steed Bridle
            29469, -- Horn of the Black War Wolf
            29470, -- Red Skeletal Warhorse
            29471, -- Reins of the Black War Tiger
            29472, -- Whistle of the Black War Raptor
            29745, -- Great Blue Elekk
            29746, -- Great Green Elekk
            29747, -- Great Purple Elekk
            30480, -- Fiery Warhorse's Reins
            31829, -- Reins of the Cobalt Riding Talbuk
            31830, -- Reins of the Cobalt Riding Talbuk
            31831, -- Reins of the Silver Riding Talbuk
            31832, -- Reins of the Silver Riding Talbuk
            31833, -- Reins of the Tan Riding Talbuk
            31834, -- Reins of the Tan Riding Talbuk
            31835, -- Reins of the White Riding Talbuk
            31836, -- Reins of the White Riding Talbuk
            32768, -- Reins of the Raven Lord
            33977, -- Swift Brewfest Ram
            34129, -- Swift Warstrider
            35513, -- Swift White Hawkstrider
            35906, -- Reins of the Black War Elekk
            37719, -- Swift Zhevra
            37828, -- Great Brewfest Kodo
            23720, -- Riding Turtle
            37012  -- The Horseman's Reins
        },
        [addon.CONS.M_COMPANIONS_ID] = {
            29363, -- Mana Wyrmling
            29364, -- Brown Rabbit Crate
            29901, -- Blue Moth Egg
            29902, -- Red Moth Egg
            29903, -- Yellow Moth Egg
            29904, -- White Moth Egg
            29953, -- Golden Dragonhawk Hatchling
            29956, -- Red Dragonhawk Hatchling
            29957, -- Silver Dragonhawk Hatchling
            29958, -- Blue Dragonhawk Hatchling
            29960, -- Captured Firefly
            31760, -- Miniwing
            34535, -- Azure Whelpling
            32233, -- Wolpertinger's Tankard
            38050, -- Soul-Trader Beacon
            25535, -- Netherwhelp's Collar
            27445, -- Magical Crawdad Box
            30360, -- Lurky's Egg
            32588, -- Banana Charm
            32616, -- Egbert's Egg
            32617, -- Sleepy Willy
            32622, -- Elekk Training Collar
            33154, -- Sinister Squashling
            33816, -- Toothy's Bucket
            33818, -- Muckbreath's Bucket
            33993, -- Mojo
            34425, -- Clockwork Rocket Bot
            34478, -- Tiny Sporebat
            34492, -- Rocket Chicken
            34955, -- Scorched Stone
            35349, -- Snarly's Bucket
            35350, -- Chuck's Bucket
            35504, -- Phoenix Hatchling
            38628, -- Nether Ray Fry
            39656, -- Tyrael's Hilt
            23712, -- Ash'ana
            23713, -- Hippogryph Hatchling
            34493, -- Dragon Kite
            35227  -- Goblin Weather Machine - Prototype 01-B
        },
        [addon.CONS.M_TELEPORT_ID] = {
            [addon.CONS.MT_HEARTSTONE_ID] = {
                37863, -- Direbrew's Remote
                29796  -- Socrethar's Teleportation Stone
            },
            [addon.CONS.MT_ARMOR_ID] = {
                28585  -- Ruby Slippers
            },
            [addon.CONS.MT_JEWELRY_ID] = {
                32757  -- Blessed Medallion of Karabor
            },
            [addon.CONS.MT_SCROLLS_ID] = {
                35230  -- Darnarian's Scroll of Teleportation
            }
        },
        [addon.CONS.M_OTHER_ID] = {
            32835, -- Ogri'la Care Package
            34863, -- Bag of Fishing Treasures
            35286, -- Bloated Giant Sunfish
            35348, -- Bag of Fishing Treasures
            31800, -- Outcast's Cache
            33844, -- Barrel of Fish
            27481, -- Heavy Supply Crate
            29569, -- Strong Junkbox
            23846, -- Nolkai's Box
            23501, -- Bloodthistle Petal
            33096, -- Alcohol-Free Brewfest Sampler
            23500, -- Saltheril's Haven Party Invitation
            23858, -- Singed Page
            23866, -- Singed Page
            23867, -- Singed Page
            23868, -- Singed Page
            24084, -- Draenei Banner
            24335, -- Orb of Returning
            24336, -- Fireproof Satchel
            24402, -- Package of Identified Plants
            25419, -- Unmarked Bag of Gems
            25422, -- Bulging Sack of Gems
            25423, -- Bag of Premium Gems
            25424, -- Gem-Stuffed Envelope
            25853, -- Pack of Incendiary Bombs
            25866, -- Greatmother's List of Herbs
            30426, -- Coilskar Chest Key
            30632, -- Lament of the Highborne
            30712, -- The Doctor's Key
            31088, -- Tainted Core
            31408, -- Offering of the Sha'tar
            31698, -- Letter from Shattrath
            32454, -- Arthorn's Research
            32578, -- Charged Crystal Focus
            32624, -- Large Iron Metamorphosis Geode 
            32625, -- Small Iron Metamorphosis Geode
            32626, -- Large Copper Metamorphosis Geode
            32627, -- Small Copper Metamorphosis Geode
            32628, -- Large Silver Metamorphosis Geode
            32629, -- Large Gold Metamorphosis Geode
            32630, -- Small Gold Metamorphosis Geode
            32631, -- Small Silver Metamorphosis Geode
            32688, -- Bloodstained Fortune
            32689, -- Bloodstained Fortune
            32690, -- Bloodstained Fortune
            32691, -- Bloodstained Fortune
            32692, -- Bloodstained Fortune
            32693, -- Bloodstained Fortune
            32700, -- Bloodstained Fortune
            32701, -- Bloodstained Fortune
            32702, -- Bloodstained Fortune
            32703, -- Bloodstained Fortune
            32704, -- Bloodstained Fortune
            32705, -- Bloodstained Fortune
            32706, -- Bloodstained Fortune
            32707, -- Bloodstained Fortune
            32708, -- Bloodstained Fortune
            32709, -- Bloodstained Fortune
            32710, -- Bloodstained Fortune
            32711, -- Bloodstained Fortune
            32712, -- Bloodstained Fortune
            32713, -- Bloodstained Fortune
            32724, -- Sludge-Covered Object
            32759, -- Accelerator Module
            32777, -- Kronk's Grab Bag
            32911, -- Empty Brewfest Stein
            33857, -- Crate of Meat
            34497, -- Paper Flying Machine
            37148, -- Loose Singed Page
            32897, -- Mark of the Illidari
            31952, -- Khorium Lockbox
            29750, -- Ethereum Stasis Chamber Key
            32064, -- Protectorate Treasure Cache
            32079, -- Shaffar's Stasis Chamber Key
            32773, -- Bash'ir's Skeleton Key
            34846, -- Black Sack of Gems
            32670, -- Depleted Two-Handed Axe
            32671, -- Depleted Mace
            32672, -- Depleted Badge
            32673, -- Depleted Dagger
            32674, -- Depleted Sword
            32677, -- Depleted Cloak
            32678, -- Depleted Ring
            32679, -- Depleted Staff
            27446, -- Mr. Pinchy's Gift
            32092, -- The Eye of Haramad
            32566, -- Picnic Basket
            32572, -- Apexis Crystal
            32681, -- Onyx Scale of Rivendark
            32682, -- Obsidia Scale
            32683, -- Jet Scale of Furywing
            32684, -- Insidion's Ebony Scale
            32782, -- Time-Lost Figurine
            33223, -- Fishing Chair
            34499, -- Paper Flying Machine Kit
            38301, -- D.I.S.C.O.
            32675, -- Depleted Mail Gauntlets
            32676  -- Depleted Cloth Bracers
        }
    },
    [addon.CONS.QUEST_ID] = {
        31345, -- The Journal of Val'zareq
        32464, -- Nethercite Ore
        32468, -- Netherdust Pollen
        32470, -- Nethermine Flayer Hide
        34253, -- Sizzling Embers
        34254, -- Razorthorn Root
        34368, -- Attuned Crystal Cores
        34469, -- Strange Engine Part
        34474, -- Strange Engine Part
        34489, -- Flaming Oil
        34502, -- Bloodberry
        34864, -- Baby Crocolisk
        34865, -- Blackfin Darter
        34866, -- Giant Freshwater Shrimp
        34867, -- Monstrous Felblood Snapper
        34868, -- World's Largest Mudfish
        29738, -- Vial of Void Horror Ooze
        24558, -- Murkblood Invasion Plans
        24559, -- Murkblood Invasion Plans
        24504, -- Howling Wind
        26042, -- Oshu'gun Crystal Powder Sample
        26043, -- Oshu'gun Crystal Powder Sample
        31707, -- Cabal Orders
        33837, -- Cooking Pot
        33851, -- Cooking Pot
        33852, -- Cooking Pot
        35313, -- Bloated Barbed Gill Trout
        23338, -- Eroded Leather Case
        28552, -- A Mysterious Tome
        33978, -- "Honorary Brewer" Hand Stamp
        34028, -- "Honorary Brewer" Hand Stamp
        35237, -- Orb of the Crawler
        24132, -- A Letter from the Admiral
        24399, -- Sunhawk Missive
        23870, -- Red Crystal Pendant
        23926, -- Tome of Divinity
        23850, -- Gurf's Dignity
        23697, -- Taming Rod
        23702, -- Taming Rod
        23703, -- Taming Rod
        23896, -- Taming Totem
        23897, -- Taming Totem
        23898, -- Taming Totem
        23910, -- Blood Elf Communication
        23759, -- Rune Covered Tablet
        23678, -- Faintly Glowing Crystal
        20765, -- Incriminating Documents
        20604, -- Stink Bomb Cleaner
        20605, -- Rotten Eggs
        20743, -- Unstable Mana Crystal
        20759, -- Otembe's Hammer
        20760, -- Chieftain Zul'Marosh's Head
        20764, -- Prospector Anvilward's Head
        20771, -- Tainted Soil Sample
        20772, -- Springpaw Pelt
        20797, -- Lynx Collar
        20799, -- Felendren's Head
        20804, -- Erona's Package
        20934, -- Wraith Essence
        20935, -- Tainted Wraith Essence
        20938, -- Falconwing Square Gift Voucher
        21757, -- Grimscale Murloc Head
        21770, -- Ring of Mmmrrrggglll
        21771, -- Captain Kelisendra's Cargo
        21776, -- Captain Kelisendra's Lost Rutters
        21781, -- Thaelis' Head
        21783, -- Magister Duskwither's Journal
        21807, -- Unmarked Letter
        21808, -- Arcane Core
        21925, -- Immaculate Letter
        22413, -- Sin'dorei Armaments
        22414, -- Antheol's Elemental Grimoire
        22473, -- Antheol's Disciplinary Rod
        22474, -- Meledor's Apprentice Badge
        22475, -- Ralen's Apprentice Badge
        22487, -- Aldaron's Head
        22549, -- Sathiel's Request
        22550, -- Sathiel's Goods
        22566, -- Phantasmal Substance
        22567, -- Gargoyle Fragment
        22570, -- Plagued Blood Sample
        22579, -- Plagued Murloc Spine
        22580, -- Crystallized Mana Essence
        22583, -- Rathis Tomber's Supplies
        22590, -- Night Elf Plans: An'daroth
        22591, -- Night Elf Plans: An'owyn
        22592, -- Night Elf Plans: Scrying on the Sin'dorei
        22594, -- Night Elf Plans
        22597, -- The Lady's Necklace
        22598, -- Stone of Light
        22599, -- Stone of Flame
        22627, -- The Lady's Necklace
        22628, -- Renzithen's Restorative Draught
        22629, -- Sealed Sin'dorei Orders
        22633, -- Troll Juju
        22634, -- Underlight Ore
        22639, -- Zeb'Sora Troll Ear
        22640, -- Head of Kel'gash the Wicked
        22641, -- Rotting Heart
        22642, -- Spinal Dust
        22653, -- Dar'Khan's Head
        22674, -- Wavefront Medallion
        22675, -- Bundle of Medallions
        22677, -- Catlord Claws
        22693, -- Infused Crystal
        22706, -- Dar'Khan's Journal
        22717, -- Letter from Silvermoon
        22735, -- Research Notes
        22755, -- Blazing Torch
        22775, -- Suntouched Special Reserve
        22776, -- Springpaw Appetizers
        22777, -- Bundle of Fireworks
        22796, -- Apothecary's Poison
        22888, -- Azure Watch Gift Voucher
        22889, -- Vial of Moth Blood
        22893, -- Luzran's Head
        22894, -- Knucklerot's Head
        22934, -- Lasher Sample
        22955, -- Neutralizing Agent
        22962, -- Inoculating Crystal
        22978, -- Emitter Spare Part
        23003, -- Blood Elf Plans
        23165, -- Headhunter Axe
        23166, -- Hexxer Stave
        23167, -- Shadowcaster Mace
        23191, -- Crystal Controlling Orb
        23205, -- Hellfire Spineleaf
        23217, -- Ravager Egg
        23218, -- Condensed Voidwalker Essence
        23227, -- iCoke Gift Box Voucher
        23228, -- Old Whitebark's Pendant
        23239, -- Plump Buzzard Wing
        23248, -- Purified Helboar Meat
        23249, -- Amani Invasion Plans
        23268, -- Purification Mixture
        23269, -- Felblood Sample
        23270, -- Tainted Helboar Meat
        23336, -- Helboar Blood Sample
        23337, -- Cenarion Antidote
        23339, -- Arelion's Journal
        23343, -- Torn Pilgrim's Pack
        23358, -- Signaling Gem
        23387, -- Bonestripper Tail Feather
        23394, -- Healing Salve
        23442, -- Glowing Sanctified Crystal
        23480, -- Lit Torch
        23483, -- Haal'eshi Scroll
        23485, -- Empty Birdcage
        23486, -- Caged Female Kaliri Hatchling
        23550, -- Heavy Stone Axe
        23551, -- Azure Phial
        23552, -- Filled Azure Phial
        23566, -- Azure Phial
        23568, -- Bundle of Vials
        23569, -- Letter from the Mag'har
        23580, -- Avruu's Orb
        23588, -- Kaliri Feather
        23589, -- Mag'har Ancestral Beads
        23614, -- Red Snapper
        23642, -- Sha'naar Relic
        23643, -- Purifying Earth
        23644, -- Crate of Reagents
        23654, -- Draenei Fishing Net
        23657, -- Dawn Runner Cargo
        23658, -- Advisor's Pack
        23659, -- Fel-Tainted Morsels
        23660, -- Advisor's Rapier
        23662, -- Letter from Nazgrel
        23669, -- Ancestral Spirit Wolf Totem
        23670, -- Thalanaar Moonwell Water
        23671, -- Earth Crystal
        23672, -- Crate of Red Snapper
        23675, -- Robotron Control Unit
        23677, -- Moongraze Buck Hide
        23679, -- Bloodscalp Totem
        23680, -- Gift of Naias
        23681, -- Heart of Naias
        23682, -- Ritual Torch
        23685, -- Root Trapper Vine
        23686, -- Lacy Handkerchief
        23687, -- Blacktalon's Claws
        23688, -- Hauteur's Ashes
        23692, -- Azure Snapdragon Bulb
        23693, -- Carinda's Scroll of Retribution
        23706, -- Arcane Fragment
        23707, -- Spindleweb Silk Gland
        23723, -- Warchief Kargath's Fist
        23726, -- Fel Ember
        23732, -- Voidstone
        23733, -- Ritual Torch
        23735, -- Grand Warlock's Amulet
        23738, -- Nautical Compass
        23739, -- Nautical Map
        23744, -- Foul Essence
        23749, -- Empty Bota Bag
        23750, -- Filled Bota Bag
        23751, -- Skin of Purest Water
        23752, -- Flask of Purest Water
        23753, -- Drycap Mushroom
        23757, -- Skittering Crawler Meat
        23760, -- Chalice of Elune
        23776, -- Warsong Lumber
        23777, -- Diabolical Plans
        23778, -- Sentinel Melyria's Report
        23779, -- Ancient Relic
        23780, -- Diabolical Plans
        23788, -- Tree Seedlings
        23789, -- Remains of Cowlen's Family
        23790, -- Hollowed Out Tree
        23791, -- Pile of Leaves
        23792, -- Tree Disguise Kit
        23797, -- Diabolical Plans
        23798, -- Diabolical Plans
        23801, -- Bristlelimb Key
        23818, -- Stillpine Furbolg Language Primer
        23830, -- Clopper's Equipment
        23833, -- Crude Murloc Knife
        23834, -- Crude Murloc Idol
        23837, -- Weathered Treasure Map
        23843, -- Whorl of Air
        23845, -- Ravager Hide
        23849, -- Stillpine Grain
        23851, -- Battered Journal
        23859, -- Nazzivus Monument Glyph
        23860, -- The Kurken's Hide
        23863, -- Corrupted Crystal
        23869, -- Crystallized Bark
        23873, -- Galaen's Amulet
        23875, -- Crystal Mining Pick
        23876, -- Crystal Mining Pick
        23877, -- Crystal Mining Pick
        23878, -- Impact Site Crystal Sample
        23879, -- Altered Crystal Sample
        23880, -- Axxarien Crystal Sample
        23881, -- Gargolmar's Hand
        23886, -- Omor's Hoof
        23894, -- Fel Orc Blood Vial
        23899, -- Traitor's Communication
        23900, -- Tzerak's Armor Plate
        23901, -- Nazan's Head
        23902, -- Chellan's List
        23903, -- Nurguni's Supplies
        23925, -- Ravager Cage Key
        23927, -- Sand Pear
        23928, -- The Exarch's Orders
        23929, -- Letter from Lor'themar Theron
        23930, -- Letter Sealed by Sylvanas
        23932, -- Survey Data Crystal
        23933, -- Medivh's Journal
        23934, -- Medivh's Journal
        23937, -- Letter of Introduction
        23981, -- Steam Pump Part
        23984, -- Irradiated Crystal Shard
        23990, -- Completed Star Chart
        23994, -- Thorny Constrictor Vine
        23995, -- Murloc Tagger
        23997, -- Head of Tel'athion
        24025, -- Deathclaw's Paw
        24026, -- Elder Brown Bear Flank
        24040, -- Blood Mushroom
        24041, -- Aquatic Stinkhorn
        24042, -- Ruinous Polyspore
        24043, -- Fel Cone Fungus
        24049, -- Ysera's Tear
        24081, -- Satyrnaar Fel Wood
        24099, -- The High Chief's Key
        24139, -- Faint Arcane Essence
        24148, -- Partial Star Chart
        24149, -- Unfinished Star Chart
        24152, -- Charred Bone Fragment
        24153, -- Bloodcursed Soul
        24156, -- Filled Shimmering Vessel
        24157, -- Shimmering Vessel
        24184, -- Filled Shimmering Vessel
        24185, -- Dragon Bone
        24221, -- Bundle of Dragon Bones
        24224, -- Crate of Bloodforged Ingots
        24225, -- Blood of the Wrathful
        24226, -- Blood Knight Insignia
        24230, -- Velen's Orders
        24233, -- Discarded Nutriment
        24236, -- Medical Supplies
        24237, -- Galaen's Journal
        24238, -- Mushroom Sample
        24239, -- Crate of Materials
        24240, -- Box of Mushrooms
        24245, -- Glowcap
        24246, -- Sanguine Hibiscus
        24247, -- Underspore Frond
        24248, -- Brain of the Black Stalker
        24278, -- Flare Gun
        24279, -- Vicious Teromoth Sample
        24280, -- Naga Claws
        24284, -- Tyr's Hand Holy Water
        24285, -- Crepuscular Powder
        24286, -- Arcane Catalyst
        24290, -- Mature Spore Sac
        24291, -- Bog Lord Tendril
        24317, -- Bloodmyst Water Sample
        24318, -- Water Sample Flask
        24323, -- Translated Sunhawk Missive
        24337, -- Deactivating Jewel
        24372, -- Diaphanous Wing
        24373, -- Scout Jyoba's Report
        24374, -- Eel Filet
        24375, -- Thick Hydra Scale
        24382, -- Zurai's Report
        24383, -- Fulgor Spore
        24400, -- Dead Mire Soil Sample
        24401, -- Unidentified Plant Parts
        24411, -- Ikeyen's Belongings
        24414, -- Blood Elf Plans
        24415, -- Vindicator Idaar's Letter
        24416, -- Corrupted Flower
        24419, -- Digested Caracoli
        24422, -- Feralfen Idol
        24426, -- Sporebat Eye
        24427, -- Fen Strider Tentacle
        24428, -- Ahuurn's Elixir
        24449, -- Fertile Spores
        24467, -- Living Fire
        24468, -- Burstcap Mushroom
        24469, -- Muck-Ridden Core
        24470, -- Murloc Cage
        24471, -- Ango'rosh Attack Plans
        24472, -- Boss Grog'ak's Head
        24473, -- Enraged Crusher Core
        24480, -- Ghostly Essence
        24482, -- Alturus' Report
        24483, -- Withered Basidium
        24484, -- Withered Basidium
        24485, -- Marshlight Bleeder Venom
        24486, -- Fenclaw Hide
        24487, -- Second Key Fragment
        24488, -- Third Key Fragment
        24489, -- Restored Apprentice's Key
        24492, -- Keanna's Log
        24493, -- Marshfang Slicer Blade
        24496, -- Horn of Banthar
        24497, -- Feralfen Protection Totem
        24498, -- Feralfen Totem
        24499, -- Daggerfen Poison Manual
        24500, -- Daggerfen Poison Vial
        24501, -- Gordawg's Boulder
        24502, -- Warmaul Skull
        24503, -- Gurok's Earthen Head
        24505, -- Heart of Tusker
        24513, -- Eye of Gutripper
        24514, -- First Key Fragment
        24523, -- Hoof of Bach'lor
        24542, -- Murkblood Idol
        24543, -- Head of Ortor of Murkblood
        24560, -- Torch of Liquid Fire
        24573, -- Elder Kuruti's Response
        25416, -- Oshu'gun Crystal Fragment
        25433, -- Obsidian Warbeads
        25448, -- Blacksting's Stinger
        25449, -- Bundle of Skins
        25458, -- Mag'har Battle Standard
        25459, -- "Count" Ungula's Mandible
        25460, -- Bleeding Hollow Supply Crate
        25461, -- Book of Forgotten Names
        25462, -- Tome of Dusk
        25463, -- Pair of Ivory Tusks
        25468, -- Boulderfist Plans
        25490, -- Boulderfist Key
        25491, -- Salvaged Spore Sacs
        25509, -- Northwind Cleft Key
        25552, -- Warmaul Ogre Banner
        25554, -- Kil'sorrow Armaments
        25555, -- Kil'sorrow Banner
        25586, -- Burning Blade Peace Offering
        25590, -- Head of Cho'war
        25604, -- Warmaul Prison Key
        25638, -- Eye of Veil Reskk
        25642, -- Eye of Veil Shienor
        25647, -- Telaar Supply Crate
        25648, -- Cho'war's Key
        25658, -- Damp Woolen Blanket
        25672, -- Teromoth Sample
        25719, -- Arakkoa Feather
        25727, -- Sealed Box
        25744, -- Dampscale Basilisk Eye
        25745, -- Olemba Seed
        25746, -- Box of Parts
        25751, -- The Master Planner's Blueprints
        25765, -- Fel Orc Plans
        25767, -- Raliq's Debt
        25768, -- Coosh'coosh's Debt
        25769, -- Floon's Debt
        25770, -- Fel Cannon Activator
        25771, -- Fel Cannon Activator
        25802, -- Dreadfang Venom Sac
        25807, -- Timber Worg Tail
        25812, -- Timber Worg Pelt
        25815, -- Stonegazer's Blood
        25817, -- Blessed Vial
        25837, -- Ironjaw's Pelt
        25840, -- Extract of the Afterlife
        25841, -- Draenei Vessel
        25842, -- Restless Bones
        25852, -- Tail Feather of Torgos
        25862, -- Marshberry
        25863, -- Olemba Root
        25864, -- Telaari Frond
        25865, -- Dragonspine
        25889, -- Draenei Holy Water
        25891, -- Pristine Shimmerscale Eel
        25911, -- Salvaged Wood
        25912, -- Salvaged Metal
        25938, -- Mysteries of the Light
        26002, -- Flaming Torch
        26048, -- Letter to Kialon
        27479, -- Flaming Torch
        27480, -- Soul Device
        27632, -- Terokk's Quill
        27633, -- Terokk's Mask
        27634, -- The Saga of Terokk
        27807, -- Air Elemental Gas
        27841, -- Severed Talon of the Matriarch
        27861, -- Lathrai's Stolen Goods
        27943, -- Chieftain Mummaki's Totem
        28024, -- Orion's Report
        28048, -- Crude Explosives
        28099, -- Drillmaster Zurok's Orders
        28105, -- Duron's Report
        28106, -- Kingston's Primers
        28116, -- Zeppelin Debris
        28209, -- Old Whitebark's Pendant
        28283, -- Soul Mirror
        28287, -- Vial of Sedative Serum
        28292, -- Archmage Vargoth's Staff
        28336, -- Belmara's Tome
        28351, -- Dathric's Blade
        28352, -- Luminrath's Mantle
        28353, -- Cohlien's Cap
        28359, -- Netherologist's Notes
        28364, -- Etherlithium Matrix Crystal
        28368, -- Sigil of Krasus
        28369, -- Battery Recharging Blaster
        28376, -- B'naar Personnel Roster
        28417, -- Nether Ray Stinger
        28452, -- Bloodgem Shard
        28455, -- Archmage Vargoth's Staff
        28457, -- Ethereal Technology
        28472, -- Krasus' Compendium - Chapter 1
        28473, -- Krasus' Compendium - Chapter 2
        28474, -- Krasus' Compendium - Chapter 3
        28475, -- Heliotrope Oculus
        28478, -- To'arch's Primers
        28479, -- Stone of Glacius
        28490, -- Shaffar's Wrappings
        28500, -- Fossil Oil
        28526, -- Etherlithium Matrix Crystals
        28527, -- Mana Wraith Essence
        28547, -- Elemental Power Extractor
        28548, -- Elemental Power
        28550, -- Flaming Torch
        28551, -- Fel Reaver Part
        28558, -- Spirit Shard
        28562, -- Unyielding Battle Horn
        28563, -- Doomclaw's Hand
        28564, -- Energy Isolation Cube
        28634, -- Scrap Reaver X6000 Controller
        28651, -- Unyielding Battle Horn
        28665, -- Mountain Gronn Eyeball
        28667, -- Flawless Greater Windroc Beak
        28668, -- Aged Clefthoof Blubber
        28677, -- The Book of the Dead
        28725, -- Rune Activation Device
        28769, -- The Keystone
        28786, -- Apex's Crystal Focus
        28787, -- Annihilator Servo
        28829, -- Arklon Crystal Artifact
        28913, -- Box of Surveying Equipment
        28934, -- Surveying Equipment
        28962, -- Triangulation Device
        28969, -- Teleporter Power Pack
        28970, -- Nether Dragon Essence
        28971, -- Nether Dragonkin Egg
        29018, -- Triangulation Device
        29026, -- Ata'mal Crystal
        29027, -- Unstable Warp Rift Generator
        29051, -- Warp Nether
        29101, -- Challenge of the Blue Flight
        29106, -- Ata'mal Crystal
        29113, -- Demonic Essence
        29161, -- Void Ridge Soul Shard
        29163, -- Raw Farahlite
        29164, -- Farahlite Core
        29207, -- Conjuring Powder
        29209, -- Zaxxis Insignia
        29216, -- Flawless Crystal Shard
        29226, -- Warp Rift Generator
        29233, -- Dathric's Blade
        29234, -- Belmara's Tome
        29235, -- Luminrath's Mantle
        29236, -- Cohlien's Cap
        29260, -- Heart of the Fel Reaver
        29331, -- Annals of Kirin'Var
        29338, -- Loathsome Remnant
        29361, -- Naberius' Phylactery
        29365, -- Smithing Hammer
        29366, -- B'naar Access Crystal
        29396, -- Coruu Access Crystal
        29397, -- Duro Access Crystal
        29411, -- Ara Access Crystal
        29425, -- Mark of Kil'jaeden
        29426, -- Firewing Signet
        29428, -- Bessy's Bell
        29429, -- Boom's Doom
        29445, -- Surveying Markers
        29447, -- Fel Zapper
        29459, -- Ethereum Relay Data
        29461, -- Mana Bomb Fragment
        29464, -- Shaleskin Shale
        29473, -- Protectorate Igniter
        29474, -- Ivory Bell
        29475, -- Pacifying Dust
        29476, -- Crimson Crystal Shard
        29477, -- Crimson Crystal Shard
        29478, -- Seed of Revitalization
        29480, -- Parched Hydra Sample
        29481, -- Withered Bog Lord Sample
        29501, -- Sha'naar Key
        29513, -- Staff of the Dreghood Elders
        29545, -- Sunfury Military Briefing
        29546, -- Sunfury Arcane Briefing
        29582, -- Ethereum Data Cell
        29586, -- Head of Forgefiend Razorsaw
        29588, -- Burning Legion Missive
        29589, -- Burning Legion Missive
        29590, -- Burning Legion Missive
        29591, -- Prepared Ethereum Wrapping
        29618, -- Protectorate Disruptor
        29737, -- Navuud's Concoction
        29741, -- Diagnostic Results
        29742, -- The Warden's Key
        29768, -- Hulking Hydra Heart
        29769, -- Diagnostic Results
        29770, -- Experimental Repair Apparatus
        29795, -- Burning Legion Gate Key
        29797, -- Orders From Kael'thas
        29798, -- Dome Generator Segment
        29801, -- Ripfang Lynx Pelt
        29803, -- Diagnostic Device
        29817, -- Talbuk Tagger
        29818, -- Energy Field Modulator
        29822, -- Fragment of Dimensius
        29912, -- The Final Code
        29952, -- Rina's Bough
        30094, -- Totem of Spirits
        30157, -- Cursed Talisman
        30158, -- Morkh's Shattered Armor
        30174, -- Dust of the Fey Drake
        30175, -- Gor'drek's Ointment
        30177, -- Stronglimb Deeproot's Trunk
        30184, -- Thunderlord Dire Wolf Tail
        30251, -- Rina's Diminution Powder
        30259, -- Voren'thal's Presence
        30315, -- Draenethyst Mine Crystal
        30325, -- Ravenous Ravager Egg
        30326, -- Bonechewer Blood Samples
        30327, -- Bonechewer Blood
        30353, -- Bloodmaul Brutebane Keg
        30354, -- Ultra Deconsolodation Zapper
        30356, -- Shadowmoon Tuber
        30404, -- Bleeding Hollow Blood Sample
        30413, -- Vindicator Vuuleen's Blade
        30415, -- Vindicator Vuuleen's Shield
        30416, -- Bladespire Clan Banner
        30417, -- Helm of Gurn Grubnosh
        30425, -- Bleeding Hollow Blood
        30428, -- First Fragment of the Cipher of Damnation
        30429, -- Grom'tor's Lockbox
        30430, -- Boiled Blood
        30431, -- Thunderlord Clan Artifact
        30432, -- Thunderlord Clan Drum
        30433, -- Thunderlord Clan Arrow
        30434, -- Thunderlord Clan Tablet
        30435, -- The Thunderspike
        30442, -- Crystalline Key
        30451, -- Lohn'goron, Bow of the Torn-Heart
        30453, -- Second Fragment of the Cipher of Damnation
        30454, -- Ar'tor's Lockbox
        30462, -- Oronok's Boar Whistle
        30468, -- T'chali's Hookah
        30479, -- Wicked Strong Fetish
        30481, -- Fiery Soul Fragment
        30500, -- Rotten Arakkoa Egg
        30501, -- Bundle of Bloodthistle
        30503, -- Archeologist's Shrunken Head
        30529, -- Plucked Lashh'an Feather
        30530, -- Fistful of Feathers
        30539, -- Tally's Waiver (Signed)
        30561, -- Vekh'nir Crystal
        30567, -- Charged Vekh'nir Crystal
        30579, -- Illidari-Bane Shard
        30596, -- Baa'ri Tablet Fragment
        30614, -- Fel Bomb
        30616, -- Bundle of Bloodthistle
        30617, -- Stormrage Missive
        30618, -- Trachela's Carcass
        30628, -- Fel Reaver Power Core
        30631, -- Fel Reaver Armor Plate
        30638, -- Box o' Tricks
        30639, -- Blood Elf Disguise
        30640, -- Eclipsion Armor
        30645, -- Third Fragment of the Cipher of Damnation
        30646, -- Borak's Lockbox
        30649, -- Orders From Akama
        30650, -- Dertrok's Wand Case
        30651, -- Dertrok's First Wand
        30652, -- Dertrok's Second Wand
        30653, -- Dertrok's Third Wand
        30654, -- Dertrok's Fourth Wand
        30655, -- Infused Vekh'nir Crystal
        30656, -- Protovoltaic Magneto Collector
        30657, -- The Cipher of Damnation
        30658, -- Flanis' Pack
        30659, -- Kagrosh's Pack
        30672, -- Elemental Displacer
        30679, -- Sunfury Glaive
        30688, -- Deathforge Key
        30689, -- Razuun's Orders
        30691, -- Haalum's Medallion Fragment
        30692, -- Eykenen's Medallion Fragment
        30693, -- Lakaan's Medallion Fragment
        30694, -- Uylaru's Medallion Fragment
        30695, -- Legion Teleporter Control
        30700, -- Scourgestone Fragments
        30701, -- Oscillating Frequency Scanners
        30704, -- Ruuan'ok Claw
        30706, -- Harbinger's Pendant
        30713, -- The Art of Fel Reaver Maintenance
        30716, -- Ever-Burning Ash
        30742, -- Temporal Phase Modulator
        30743, -- Proto-Nether Drake Essence
        30756, -- Illidari-Bane Shard
        30782, -- Adolescent Nether Drake Essence
        30783, -- Mature Nether Drake Essence
        30785, -- Morgroron's Glaive
        30786, -- Makazradon's Glaive
        30791, -- Silkwing Cocoon
        30792, -- Iridescent Wing
        30794, -- Shredder Keys
        30797, -- Gorefiend's Armor
        30798, -- Extra Sharp Daggermaw Tooth
        30799, -- Gorefiend's Cloak
        30800, -- Gorefiend's Truncheon
        30803, -- Felhound Whistle
        30807, -- Uvuros' Fiery Mane
        30808, -- Book of Fel Names
        30809, -- Mark of Sargeras
        30810, -- Sunfury Signet
        30818, -- Repolarized Magneto Sphere
        30819, -- Felfire Spleen
        30822, -- Box of Ingots
        30823, -- Demon Warding Totem
        30824, -- Overcharged Manacell
        30827, -- Lexicon Demonica
        30828, -- Vial of Underworld Loam
        30829, -- Tear of the Earthmother
        30840, -- Ether-Energized Flesh
        30849, -- Scalewing Lightning Gland
        30850, -- Freshly Drawn Blood
        30851, -- Felspine's Hide
        30852, -- Multi-Spectrum Light Trap
        30853, -- Imbued Silver Spear
        30854, -- Book of Fel Names
        30867, -- Overdeveloped Felfire Gizzard
        30875, -- Forged Illidari-Bane Blade
        30876, -- Quenched Illidari-Bane Blade
        30890, -- Collection of Souls
        31085, -- Top Shard of the Arcatraz Key
        31086, -- Bottom Shard of the Arcatraz Key
        31108, -- Kor'kron Flare Gun
        31119, -- Wyrmcult Net
        31120, -- Meeting Note
        31123, -- Spinning Nether-Weather Vane
        31124, -- Nether-Weather Vane
        31128, -- Rexxar's Whistle
        31129, -- Blackwhelp Net
        31130, -- Wyrmcult Blackwhelp
        31132, -- Crust Burster Venom Gland
        31135, -- Baron Sablemane's Poison
        31141, -- Kodohide Drum
        31144, -- Spirit's Whistle
        31146, -- Rexxar's Battle Horn
        31169, -- Sketh'lon War Totem
        31239, -- Primed Key Mold
        31241, -- Primed Key Mold
        31245, -- Primed Key Mold
        31251, -- Unfired Key Mold
        31252, -- Charred Key Mold
        31260, -- Sketh'lon Commander's Journal - Page 1
        31261, -- Sketh'lon Commander's Journal - Page 2
        31262, -- Sketh'lon Commander's Journal - Page 3
        31271, -- Illidan's Command
        31278, -- Illidari Tabard
        31300, -- Ironroot Seeds
        31307, -- Heart of Fury
        31310, -- Wildhammer Flare Gun
        31316, -- Lianthe's Key
        31317, -- Rod of Lianthe
        31324, -- Sketh'lon Feather
        31344, -- Ceremonial Incense
        31346, -- Burning Bleeding Hollow Torch
        31347, -- Bleeding Hollow Torch
        31349, -- Grulloc's Sack
        31350, -- Huffer's Whistle
        31351, -- Dragonfire Trap
        31360, -- Unfinished Headpiece
        31363, -- Gorgrom's Favor
        31365, -- Energized Headpiece
        31366, -- Felsworn Gas Mask
        31372, -- Rocknail Flayer Carcass
        31373, -- Rocknail Flayer Giblets
        31374, -- Worg Master's Head
        31384, -- Damaged Mask
        31386, -- Staff of Parshah
        31387, -- Mystery Mask
        31403, -- Sablemane's Sleeping Powder
        31463, -- Zezzak's Shard
        31489, -- Orb of the Grishna
        31504, -- Nethervine Crystal
        31522, -- Primal Mooncloth Supplies
        31524, -- Square of Imbued Netherweave
        31525, -- Vial of Primal Reagents
        31529, -- Grillok's Eyepatch
        31530, -- Sample of Primal Mooncloth
        31536, -- Camp Anger Key
        31550, -- Albreck's Findings
        31606, -- Demoniac Scryer
        31607, -- Demoniac Scryer Reading
        31610, -- Rod of Purification
        31651, -- Bladespire Totem
        31652, -- Enchanted Nethervine Crystal
        31653, -- Condensed Nether Gas
        31655, -- Veil Skith Prison Key
        31656, -- Lesser Nether Drake Spirit
        31662, -- Rilak's Missive
        31663, -- Spirit Calling Totems
        31664, -- Zuluhed's Key
        31668, -- Orb Collecting Totem
        31678, -- Mental Interference Rod
        31697, -- Dread Relic
        31705, -- Derelict Caravan Chest Key
        31706, -- The Head of the Hand of Kargath
        31708, -- Scroll of Atalor
        31709, -- Drape of Arunen
        31710, -- Gavel of K'alen
        31716, -- Unused Axe of the Executioner
        31721, -- Kalithresh's Trident
        31722, -- Murmur's Essence
        31736, -- Crystal of Deep Shadows
        31739, -- Smoke Beacon
        31740, -- Meeting Note
        31741, -- Nether-Wraith Essence
        31742, -- Nether-Wraith Beacon
        31744, -- Botanist's Field Guide
        31750, -- Earthen Signet
        31751, -- Blazing Signet
        31752, -- Sablemane's Trap
        31753, -- Essence of Infinity
        31754, -- Grisly Totem
        31755, -- Wyvern Cage Key
        31757, -- Fel Cannonball
        31763, -- Druid Signal
        31769, -- Sha'tari Torch
        31772, -- Anchorite Relic
        31799, -- Fei Fei Doggy Treat
        31807, -- Naturalized Ammunition
        31808, -- Sablemane's Signet
        31809, -- Evergrove Wand
        31810, -- Fumper
        31811, -- Dread Relic
        31812, -- Doom Skull
        31813, -- Warp Chaser Blood
        31814, -- Mature Bone Sifter Carcass
        31815, -- Zeppit's Crystal
        31825, -- Fumper
        31826, -- Enormous Bone Worm Organs
        31827, -- Sablemane's Trap
        31828, -- Ritual Prayer Beads
        31946, -- Ashtongue Cowl
        31950, -- Bogblossom
        31951, -- Toy Dragon
        31953, -- Ward of Waking
        31955, -- Arelion's Knapsack
        31956, -- Salvaged Ethereum Prison Key
        31994, -- Ethereum Key Tablet - Alpha
        32061, -- Evidence from Alpha
        32069, -- Mana-Tombs Stasis Chamber Key
        32074, -- Relics of Aviana
        32244, -- Seer's Stone
        32313, -- Raven Stone
        32315, -- Cenarion Sparrowhawk Whistle
        32320, -- Captive Sparrowhawk
        32321, -- Sparrowhawk Net
        32355, -- Essence of the Eagle
        32356, -- Essence of the Hawk
        32357, -- Essence of the Falcon
        32358, -- Vim'gol's Vile Grimoire
        32359, -- Arthorn's Package
        32364, -- Southfury Moonstone
        32379, -- Grulloc's Dragon Skull
        32380, -- Maggoc's Treasure Chest
        32382, -- Slaag's Standard
        32383, -- Skulloc's Soul
        32388, -- Shadow Dust
        32427, -- Netherwing Crystal
        32446, -- Elixir of Shadows
        32456, -- Skyguard Bombs
        32459, -- Time-Phased Phylactery
        32462, -- Morthis' Materials
        32467, -- Vim'gol's Grimoire
        32469, -- Illidari Service Papers
        32502, -- Fel Gland
        32503, -- Yarzill's Mutton
        32509, -- Netherwing Relic
        32523, -- Ishaal's Almanac
        32564, -- Ishaal's Almanac
        32567, -- Aether Ray Eye
        32569, -- Apexis Shard
        32619, -- Preserved Fruit
        32620, -- Time-Lost Scroll
        32621, -- Partially Digested Hand
        32623, -- Bossi's Spare Parts
        32646, -- Medallion of Karabor
        32657, -- Arthorn's Sparrowhawk Whistle
        32666, -- Hardened Hide of Tyrantus
        32680, -- Booterang
        32687, -- Hazzik's Package
        32696, -- Banishing Crystal
        32697, -- Apexis Guardian's Head
        32698, -- Wrangling Rope
        32720, -- Time-Lost Offering
        32723, -- Nethermine Cargo
        32726, -- Murkblood Escape Plans
        32732, -- Dragon Teeth
        32733, -- Fel Whip
        32734, -- Hand of the Overseer
        32741, -- Shabby Arakkoa Disguise
        32742, -- Adversarial Bloodlines
        32758, -- Brute Cologne
        32822, -- Flawless Arcane Essence
        32825, -- Soul Cannon
        32834, -- Nether Ray Cage
        32842, -- Dragonmaw Flare Gun
        32843, -- Scryer Medals
        32853, -- Aldor Medals
        32906, -- Stunned Wolpertinger
        32907, -- Wolpertinger Net
        32960, -- Elekk Dispersion Ray
        33007, -- Grimbooze's Secret Recipe
        33008, -- Deserter Propaganda
        33010, -- Griftah's Note
        33013, -- Budd's Map of Zul'Aman
        33015, -- Altered Leaflets
        33037, -- Defias Orders
        33038, -- Damaged Diving Gear
        33039, -- Tool Kit
        33040, -- Repaired Diving Gear
        33041, -- Salvaged Strongbox
        33044, -- Salvage Kit
        33045, -- Renn's Supplies
        33050, -- Grimtotem Note
        33051, -- Grimtotem Battle Plan
        33061, -- Grimtotem Key
        33069, -- Sturdy Rope
        33070, -- Raptor Bait
        33071, -- Blackhoof Armaments
        33072, -- Tabetha's Torch
        33082, -- Wreath
        33085, -- Bloodfen Feather
        33086, -- Stonemaul Banner
        33087, -- Black Dragonkin Essence
        33088, -- Brogg's Totem
        33091, -- Energized Totem
        33095, -- Stonemaul Banner
        33101, -- Captured Totem
        33103, -- Marsh Venom
        33106, -- Forest Troll Tusk
        33108, -- Ooze Buster
        33110, -- Razorspine's Sword
        33112, -- Witchbane
        33113, -- Witchbane Torch
        33114, -- Sealed Letter
        33115, -- Sealed Letter
        33126, -- Thresher Oil
        33127, -- Dastardly Denizens of the Deep
        33163, -- Zeppelin Cargo
        33166, -- Pagle's Fish Paste, Extra Strength
        33175, -- Wyrmtail
        33202, -- Marsh Frog Leg
        33306, -- Ram Racing Reins
        33599, -- Dragonflayer Blood Sample
        33814, -- Keli'dan's Feathered Stave
        33815, -- Bladefist's Seal
        33821, -- The Heart of Quagmirran
        33826, -- Black Stalker Egg
        33827, -- The Warlord's Treatise
        33833, -- Nazan's Riding Crop
        33834, -- The Headfeathers of Ikiss
        33835, -- Shaffar's Wondrous Amulet
        33836, -- The Exarch's Soul Gem
        33838, -- Giant Kaliri Wing
        33839, -- Kaliri Stew
        33840, -- Murmur's Whisper
        33847, -- Epoch Hunter's Head
        33848, -- Demon Broiled Surprise
        33849, -- Mana Berry
        33850, -- Spiritual Soup
        33858, -- Aeonus' Hourglass
        33859, -- Warp Splinter Clipping
        33860, -- Pathaleon's Projector
        33861, -- The Scroll of Skyriss
        33985, -- Dreary Candle
        34089, -- Alicia's Poem
        34141, -- Dark Iron Ale Keg
        34157, -- Head of Kael'thas
        34160, -- The Signet Ring of Prince Kael'thas
        34246, -- Smuggled Mana Cell
        34248, -- Bash'ir Phasing Device
        34255, -- Razorthorn Flayer Gland
        34257, -- Fel Siphon
        34259, -- Demonic Blood
        34338, -- Mana Remnants
        34414, -- Shattered Sun Banner
        34420, -- Captured Legion Scroll
        34477, -- Darkspine Chest Key
        34479, -- Darkspine Iron Ore
        34483, -- Orb of Murloc Control
        34500, -- Ata'mal Armament
        34501, -- Cleansed Ata'mal Metal
        34533, -- Astromancer's Crystal
        34833, -- Unlit Torches
        34862, -- Practice Torches
        34953, -- Earthen Ring Magma Totem
        35229, -- Nether Residue
        35231, -- Sunfury Attack Plans
        35233, -- Multiphase Spectrographic Goggles
        35277, -- Twilight Correspondence
        35568, -- Flame of Silvermoon
        35569, -- Flame of the Exodar
        35723, -- Shards of Ahune
        35725, -- Summer Incense
        35828, -- Totemic Beacon
        36876, -- Scorched Holy Symbol
        37571, -- "Brew of the Month" Club Membership Form
        37599, -- "Brew of the Month" Club Membership Form
        38280, -- Direbrew's Dire Brew
        38329, -- Don Carlos' Hat
        38606, -- Battle-Worn Axe
        38630, -- Runebladed Axe
        29052, -- Warp Nether Extractor
        32457, -- Arakkoa Fetish
        32506, -- Netherwing Egg
        29460, -- Ethereum Prison Key
        29739, -- Arcane Tome
        29740, -- Fel Armament
        31941, -- Mark of the Nexus-King
        31957, -- Ethereum Prisoner I.D. Tag
        32715, -- Akkarai's Talons
        32716, -- Gezzarak's Claws
        32717, -- Karrog's Spine
        32718, -- Vakkiz's Scale
        33107, -- Tattered Voodoo Doll
        33955, -- Brewfest Stein Voucher
        37829, -- Brewfest Prize Token
        31882, -- Ace of Blessings
        31883, -- Eight of Blessings
        31884, -- Five of Blessings
        31885, -- Four of Blessings
        31886, -- Seven of Blessings
        31887, -- Six of Blessings
        31888, -- Three of Blessings
        31889, -- Two of Blessings
        31892, -- Ace of Storms
        31893, -- Eight of Storms
        31894, -- Five of Storms
        31895, -- Four of Storms
        31896, -- Seven of Storms
        31898, -- Six of Storms
        31899, -- Three of Storms
        31900, -- Two of Storms
        31901, -- Ace of Furies
        31902, -- Eight of Furies
        31903, -- Five of Furies
        31904, -- Four of Furies
        31905, -- Seven of Furies
        31906, -- Six of Furies
        31908, -- Three of Furies
        31909, -- Two of Furies
        31910, -- Ace of Lunacy
        31911, -- Eight of Lunacy
        31912, -- Five of Lunacy
        31913, -- Four of Lunacy
        31915, -- Seven of Lunacy
        31916, -- Six of Lunacy
        31917, -- Three of Lunacy
        31918, -- Two of Lunacy
        31890, -- Blessings Deck
        31891, -- Storms Deck
        31907, -- Furies Deck
        31914, -- Lunacy Deck
        32385, -- Magtheridon's Head
        32386, -- Magtheridon's Head
        32405, -- Verdant Sphere
        129950, -- Commendation of the Keepers of Time
        129945, -- Commendation of The Consortium
        129946, -- Commendation of The Sha'tar
        129947, -- Commendation of Thrallmar
        129948, -- Commendation of Honor Hold
        129949, -- Commendation of the Cenarion Expedition
        129951  -- Commendation of Lower City
    },
    [addon.CONS.RECIPES_ID] = {
        [addon.CONS.R_ALCHEMY_ID] = {
            22922, -- Recipe: Major Nature Protection Potion
            22911, -- Recipe: Major Dreamless Sleep Potion
            25869, -- Recipe: Transmute Earthstorm Diamond
            25870, -- Recipe: Transmute Skyfire Diamond
            29232, -- Recipe: Transmute Skyfire Diamond
            35752, -- Recipe: Guardian's Alchemist Stone
            35753, -- Recipe: Sorcerer's Alchemist Stone
            35754, -- Recipe: Redeemer's Alchemist Stone
            35755, -- Recipe: Assassin's Alchemist Stone
            22909, -- Recipe: Elixir of Major Defense
            22907, -- Recipe: Super Mana Potion
            22905, -- Recipe: Elixir of Major Agility
            24001, -- Recipe: Elixir of Major Agility
            32071, -- Recipe: Elixir of Ironskin
            22902, -- Recipe: Elixir of Major Frost Power
            32070, -- Recipe: Earthen Elixir
            22901, -- Recipe: Sneaking Potion
            22900, -- Recipe: Elixir of Camouflage
            22926, -- Recipe: Elixir of Empowerment
            22927, -- Recipe: Ironshield Potion
            35294, -- Recipe: Elixir of Empowerment
            22920, -- Recipe: Major Fire Protection Potion
            22921, -- Recipe: Major Frost Protection Potion
            22923, -- Recipe: Major Arcane Protection Potion
            22924, -- Recipe: Major Shadow Protection Potion
            22925, -- Recipe: Major Holy Protection Potion
            22919, -- Recipe: Elixir of Major Mageblood
            22910, -- Recipe: Elixir of Major Shadow Power
            22912, -- Recipe: Heroic Potion
            22913, -- Recipe: Haste Potion
            22914, -- Recipe: Destruction Potion
            22915, -- Recipe: Transmute Primal Air to Fire
            22916, -- Recipe: Transmute Primal Earth to Water
            22917, -- Recipe: Transmute Primal Fire to Earth
            22918, -- Recipe: Transmute Primal Water to Air
            23574, -- Recipe: Transmute Primal Might
            30443, -- Recipe: Transmute Primal Fire to Earth
            35295, -- Recipe: Haste Potion
            22908, -- Recipe: Elixir of Major Firepower
            22906, -- Recipe: Shrouding Potion
            22904, -- Recipe: Elixir of the Searching Eye
            22903, -- Recipe: Insane Strength Potion
            31354, -- Recipe: Flask of the Titans
            31355, -- Recipe: Flask of Supreme Power
            31356, -- Recipe: Flask of Distilled Wisdom
            31682, -- Recipe: Fel Mana Potion
            31681, -- Recipe: Fel Regeneration Potion
            31680  -- Recipe: Fel Strength Elixir
        },
        [addon.CONS.R_BLACKSMITHING_ID] = {
            32441, -- Plans: Shadesteel Sabots
            32442, -- Plans: Shadesteel Bracers
            32443, -- Plans: Shadesteel Greaves
            32444, -- Plans: Shadesteel Girdle
            23600, -- Plans: Enchanted Adamantite Leggings
            23604, -- Plans: Flamebane Breastplate
            23599, -- Plans: Enchanted Adamantite Breastplate
            23603, -- Plans: Flamebane Gloves
            23619, -- Plans: Felsteel Shield Spike
            24002, -- Plans: Felsteel Shield Spike
            23597, -- Plans: Enchanted Adamantite Belt
            23598, -- Plans: Enchanted Adamantite Boots
            23602, -- Plans: Flamebane Helm
            23601, -- Plans: Flamebane Bracers
            23618, -- Plans: Adamantite Sharpening Stone
            25526, -- Plans: Greater Rune of Warding
            28632, -- Plans: Adamantite Weightstone
            23596, -- Plans: Adamantite Breastplate
            23638, -- Plans: Lesser Ward of Shielding
            23593, -- Plans: Adamantite Rapier
            23594, -- Plans: Adamantite Plate Bracers
            23595, -- Plans: Adamantite Plate Gloves
            23591, -- Plans: Adamantite Cleaver
            23592, -- Plans: Adamantite Dagger
            23590, -- Plans: Adamantite Maul
            33186, -- Plans: Adamantite Weapon Chain
            35296, -- Plans: Adamantite Weapon Chain
            33792, -- Plans: Heavy Copper Longsword
            23639, -- Plans: Greater Ward of Shielding
            23613, -- Plans: Ragesteel Breastplate
            23615, -- Plans: Swiftsteel Gloves
            23617, -- Plans: Earthpeace Breastplate
            23607, -- Plans: Felsteel Helm
            23610, -- Plans: Khorium Boots
            23611, -- Plans: Ragesteel Gloves
            23612, -- Plans: Ragesteel Helm
            33174, -- Plans: Ragesteel Shoulders
            23605, -- Plans: Felsteel Gloves
            23606, -- Plans: Felsteel Leggings
            23608, -- Plans: Khorium Belt
            23609, -- Plans: Khorium Pants
            30321, -- Plans: Belt of the Guardian
            30322, -- Plans: Red Belt of Battle
            30323, -- Plans: Boots of the Protector
            30324, -- Plans: Red Havoc Boots
            31390, -- Plans: Wildguard Breastplate
            31391, -- Plans: Wildguard Leggings
            31392, -- Plans: Wildguard Helm
            31393, -- Plans: Iceguard Breastplate
            31394, -- Plans: Iceguard Leggings
            31395, -- Plans: Iceguard Helm
            23620, -- Plans: Felfury Gauntlets
            23621, -- Plans: Gauntlets of the Iron Tower
            23622, -- Plans: Steelgrip Gauntlets
            23623, -- Plans: Storm Helm
            23624, -- Plans: Helm of the Stalwart Defender
            23625, -- Plans: Oathkeeper's Helm
            23626, -- Plans: Black Felsteel Bracers
            23627, -- Plans: Bracers of the Green Fortress
            23628, -- Plans: Blessed Bracers
            23629, -- Plans: Felsteel Longblade
            23630, -- Plans: Khorium Champion
            23631, -- Plans: Fel Edged Battleaxe
            23632, -- Plans: Felsteel Reaper
            23633, -- Plans: Runic Hammer
            23634, -- Plans: Fel Hardened Maul
            23635, -- Plans: Eternium Runed Blade
            23636, -- Plans: Dirge
            23637, -- Plans: Hand of Eternity
            33954, -- Plans: Hammer of Righteous Might
            32736, -- Plans: Swiftsteel Bracers
            32737, -- Plans: Swiftsteel Shoulders
            32738, -- Plans: Dawnsteel Bracers
            32739, -- Plans: Dawnsteel Shoulders
            35208, -- Plans: Sunblessed Gauntlets
            35209, -- Plans: Hard Khorium Battlefists
            35210, -- Plans: Sunblessed Breastplate
            35211  -- Plans: Hard Khorium Battleplate
        },
        [addon.CONS.R_COOKING_ID] = {
            31674, -- Recipe: Crunchy Serpent
            31675, -- Recipe: Mok'Nathal Shortribs
            27691, -- Recipe: Roasted Clefthoof
            27692, -- Recipe: Warp Burger
            27693, -- Recipe: Talbuk Steak
            27699, -- Recipe: Golden Fish Sticks
            27700, -- Recipe: Spicy Crawdad
            27697, -- Recipe: Grilled Mudfish
            27698, -- Recipe: Poached Bluefish
            27690, -- Recipe: Blackened Basilisk
            27696, -- Recipe: Blackened Sporefish
            27684, -- Recipe: Buzzard Bites
            27688, -- Recipe: Ravager Dog
            27694, -- Recipe: Blackened Trout
            27695, -- Recipe: Feltail Delight
            30156, -- Recipe: Clam Bar
            34413, -- Recipe: Hot Apple Cider
            22647, -- Recipe: Crunchy Spider Surprise
            27687, -- Recipe: Bat Bites
            27685, -- Recipe: Lynx Steak
            27686, -- Recipe: Roasted Moongraze Tenderloin
            33869, -- Recipe: Broiled Bloodfin
            33870, -- Recipe: Skullfish Soup
            33873, -- Recipe: Spicy Hot Talbuk
            33875, -- Recipe: Kibler's Bits
            34834, -- Recipe: Captain Rumsey's Lager
            33871, -- Recipe: Stormchops
            33925  -- Recipe: Delicious Chocolate Cake
        },
        [addon.CONS.R_ENCHANTING_ID] = {
            22538, -- Formula: Enchant Ring - Stats
            34872, -- Formula: Void Shatter
            33165, -- Formula: Enchant Weapon - Greater Agility
            35500, -- Formula: Enchant Chest - Dodge
            22547, -- Formula: Enchant Chest - Exceptional Stats
            24003, -- Formula: Enchant Chest - Exceptional Stats
            22552, -- Formula: Enchant Weapon - Major Striking
            22563, -- Formula: Superior Wizard Oil
            22565, -- Formula: Large Prismatic Shard
            22531, -- Formula: Enchant Bracer - Superior Healing
            22539, -- Formula: Enchant Shield - Intellect
            24000, -- Formula: Enchant Bracer - Superior Healing
            28282, -- Formula: Enchant Shield - Major Stamina
            22562, -- Formula: Superior Mana Oil
            22545, -- Formula: Enchant Boots - Surefooted
            22534, -- Formula: Enchant Bracer - Spellpower
            22556, -- Formula: Enchant 2H Weapon - Major Agility
            22557, -- Formula: Enchant Weapon - Battlemaster
            22558, -- Formula: Enchant Weapon - Spellsurge
            28271, -- Formula: Enchant Gloves - Precise Strikes
            28272, -- Formula: Enchant Gloves - Major Spellpower
            22533, -- Formula: Enchant Bracer - Fortitude
            22553, -- Formula: Enchant Weapon - Potency
            22554, -- Formula: Enchant 2H Weapon - Savagery
            22555, -- Formula: Enchant Weapon - Major Spellpower
            28273, -- Formula: Enchant Gloves - Major Healing
            28281, -- Formula: Enchant Weapon - Major Healing
            28270, -- Formula: Enchant Chest - Major Resilience
            22540, -- Formula: Enchant Shield - Parry
            22544, -- Formula: Enchant Boots - Dexterity
            22551, -- Formula: Enchant Weapon - Major Intellect
            22532, -- Formula: Enchant Bracer - Versatility Prime
            28274, -- Formula: Enchant Cloak - PvP Power
            22530, -- Formula: Enchant Bracer - Greater Dodge
            22543, -- Formula: Enchant Boots - Fortitude
            22542, -- Formula: Enchant Boots - Vitality
            35298, -- Formula: Enchant Boots - Vitality
            22559, -- Formula: Enchant Weapon - Mongoose
            22560, -- Formula: Enchant Weapon - Sunfire
            22561, -- Formula: Enchant Weapon - Soulfrost
            35498, -- Formula: Enchant Weapon - Deathfrost
            28279, -- Formula: Enchant Boots - Cat's Swiftness
            28280, -- Formula: Enchant Boots - Boar's Speed
            35297, -- Formula: Enchant Boots - Boar's Speed
            35299, -- Formula: Enchant Boots - Cat's Swiftness
            33148, -- Formula: Enchant Cloak - Dodge
            33149, -- Formula: Enchant Cloak - Stealth
            33150, -- Formula: Enchant Cloak - Subtlety
            33151, -- Formula: Enchant Cloak - Subtlety
            33152, -- Formula: Enchant Gloves - Superior Agility
            33153, -- Formula: Enchant Gloves - Threat
            35756  -- Formula: Enchant Cloak - Greater Dodge
        },
        [addon.CONS.R_ENGINEERING_ID] = {
            23799, -- Schematic: Adamantite Rifle
            23805, -- Schematic: Ultra-Spectropic Detection Goggles
            23874, -- Schematic: Elemental Seaforium Charge
            23803, -- Schematic: Cogspinner Goggles
            23807, -- Schematic: Adamantite Scope
            23811, -- Schematic: White Smoke Flare
            23814, -- Schematic: Green Smoke Flare
            23816, -- Schematic: Fel Iron Toolbox
            23888, -- Schematic: Zapthrottle Mote Extractor
            22729, -- Schematic: Steam Tonk Controller
            34114, -- Schematic: Field Repair Bot 110G
            25887, -- Schematic: Purple Smoke Flare
            23810, -- Schematic: Crashin' Thrashin' Robot
            21730, -- Schematic: Blue Rocket Cluster
            21731, -- Schematic: Green Rocket Cluster
            21732, -- Schematic: Red Rocket Cluster
            21727, -- Schematic: Large Blue Rocket
            21724, -- Schematic: Small Blue Rocket
            21725, -- Schematic: Small Green Rocket
            21726, -- Schematic: Small Red Rocket
            23802, -- Schematic: Ornate Khorium Rifle
            23809, -- Schematic: Stabilized Eternium Scope
            23800, -- Schematic: Felsteel Boomstick
            23806, -- Schematic: Hyper-Vision Goggles
            23808, -- Schematic: Khorium Scope
            23887, -- Schematic: Rocket Boots Xtreme
            35582, -- Schematic: Rocket Boots Xtreme Lite
            23884, -- Schematic: Mana Potion Injector
            35311, -- Schematic: Mana Potion Injector
            23804, -- Schematic: Power Amplification Goggles
            23883, -- Schematic: Healing Potion Injector
            35310, -- Schematic: Healing Potion Injector
            35186, -- Schematic: Annihilator Holo-Gogs
            35187, -- Schematic: Justicebringer 3000 Specs
            35189, -- Schematic: Powerheal 9000 Lens
            35190, -- Schematic: Hyper-Magnified Moon Specs
            35191, -- Schematic: Wonderheal XT68 Shades
            35192, -- Schematic: Primal-Attuned Goggles
            35193, -- Schematic: Lightning Etched Specs
            35194, -- Schematic: Surestrike Goggles v3.0
            35195, -- Schematic: Mayhem Projection Goggles
            35196, -- Schematic: Hard Khorium Goggles
            35197  -- Schematic: Quad Deathblow X44 Goggles
        },
        [addon.CONS.R_FISHING_ID] = {
            34109  -- Weather-Beaten Journal
        },
        [addon.CONS.R_JEWELCRAFTING_ID] = {
            35239, -- Design: Timeless Shadowsong Amethyst
            35242, -- Design: Shifting Shadowsong Amethyst
            35243, -- Design: Sovereign Shadowsong Amethyst
            35244, -- Design: Bold Crimson Spinel
            35246, -- Design: Delicate Crimson Spinel
            35247, -- Design: Flashing Crimson Spinel
            35248, -- Design: Brilliant Crimson Spinel
            35249, -- Design: Subtle Lionseye
            35251, -- Design: Purified Shadowsong Amethyst
            35252, -- Design: Regal Seaspray Emerald
            35253, -- Design: Jagged Seaspray Emerald
            35254, -- Design: Radiant Seaspray Emerald
            35258, -- Design: Mystic Lionseye
            35259, -- Design: Rigid Empyrean Sapphire
            35260, -- Design: Smooth Lionseye
            35263, -- Design: Solid Empyrean Sapphire
            35264, -- Design: Sparkling Empyrean Sapphire
            35265, -- Design: Stormy Empyrean Sapphire
            35266, -- Design: Glinting Shadowsong Amethyst
            35267, -- Design: Inscribed Pyrestone
            35269, -- Design: Potent Pyrestone
            35270, -- Design: Veiled Shadowsong Amethyst
            35271, -- Design: Deadly Pyrestone
            35695, -- Design: Figurine - Empyrean Tortoise
            35696, -- Design: Figurine - Khorium Boar
            35697, -- Design: Figurine - Crimson Serpent
            35698, -- Design: Figurine - Shadowsong Panther
            35699, -- Design: Figurine - Seaspray Albatross
            35502, -- Design: Eternal Earthstorm Diamond
            35505, -- Design: Ember Skyfire Diamond
            25902, -- Design: Powerful Earthstorm Diamond
            25903, -- Design: Bracing Earthstorm Diamond
            25904, -- Design: Insightful Earthstorm Diamond
            25908, -- Design: Swift Skyfire Diamond
            25910, -- Design: Enigmatic Skyfire Diamond
            33155, -- Design: Kailee's Rose
            33156, -- Design: Crimson Sun
            33157, -- Design: Falling Star
            33158, -- Design: Stone of Blades
            33159, -- Design: Blood of Amber
            33160, -- Design: Facet of Eternity
            33305, -- Design: Don Julio's Heart
            35708, -- Design: Regal Talasite
            21957, -- Design: Necklace of the Diamond Tower
            21954, -- Design: Ring of Bitter Shadows
            21952, -- Design: Emerald Crown of Destruction
            21948, -- Design: Opal Necklace of Impact
            21942, -- Design: Ruby Crown of Restoration
            21943, -- Design: Truesilver Crab
            21941, -- Design: Black Pearl Panther
            20975, -- Design: The Jade Eye
            20856, -- Design: Heavy Golden Necklace of Battle
            20855, -- Design: Wicked Moonstone Ring
            20854, -- Design: Amulet of the Moon
            23134, -- Design: Delicate Blood Garnet
            23138, -- Design: Potent Flame Spessarite
            23155, -- Design: Sparkling Azure Moonstone
            31870, -- Design: Rigid Azure Moonstone
            31873, -- Design: Veiled Shadow Draenite
            31874, -- Design: Deadly Flame Spessarite
            23133, -- Design: Brilliant Blood Garnet
            23142, -- Design: Regal Deep Peridot
            23146, -- Design: Shifting Shadow Draenite
            23150, -- Design: Subtle Golden Draenite
            23154, -- Design: Stormy Azure Moonstone
            31359, -- Design: Regal Deep Peridot
            23136, -- Design: Reckless Flame Spessarite
            23145, -- Design: Purified Shadow Draenite
            23149, -- Design: Smooth Golden Draenite
            21955, -- Design: Black Diamond Crab
            21956, -- Design: Dark Iron Scorpid
            21953, -- Design: Emerald Owl
            21949, -- Design: Ruby Serpent
            21947, -- Design: Gem Studded Band
            21945, -- Design: The Aquamarine Ward
            21944, -- Design: Truesilver Boar
            21940, -- Design: Golden Hare
            20976, -- Design: Citrine Pendant of Golden Healing
            20974, -- Design: Jade Pendant of Blasting
            24179, -- Design: Felsteel Boar
            24180, -- Design: Dawnstone Crab
            24181, -- Design: Living Ruby Serpent
            24182, -- Design: Talasite Owl
            24183, -- Design: Nightseye Panther
            31358, -- Design: Dawnstone Crab
            24162, -- Design: Arcane Khorium Band
            24165, -- Design: Blazing Eternium Band
            24170, -- Design: Embrace of the Dawn
            24171, -- Design: Chain of the Twilight Owl
            25905, -- Design: Tenacious Earthstorm Diamond
            25906, -- Design: Brutal Earthstorm Diamond
            25907, -- Design: Destructive Skyfire Diamond
            25909, -- Design: Mystical Skyfire Diamond
            32411, -- Design: Thundering Skyfire Diamond
            33622, -- Design: Relentless Earthstorm Diamond
            34689, -- Design: Chaotic Skyfire Diamond
            24161, -- Design: Khorium Band of Leaves
            24168, -- Design: Braided Eternium Chain
            24169, -- Design: Eye of the Night
            24174, -- Design: Pendant of Frozen Flame
            24175, -- Design: Pendant of Thawing
            24176, -- Design: Pendant of Withering
            24177, -- Design: Pendant of Shadow's End
            24178, -- Design: Pendant of the Null Rune
            30826, -- Design: Ring of Arcane Shielding
            24159, -- Design: Khorium Band of Frost
            24160, -- Design: Khorium Inferno Band
            24164, -- Design: Delicate Eternium Ring
            24166, -- Design: Thick Felsteel Necklace
            24167, -- Design: Living Ruby Pendant
            24158, -- Design: Khorium Band of Shadows
            24192, -- Design: Delicate Living Ruby
            24193, -- Design: Bold Living Ruby
            24194, -- Design: Delicate Living Ruby
            24196, -- Design: Brilliant Living Ruby
            24197, -- Design: Subtle Dawnstone
            24198, -- Design: Flashing Living Ruby
            24199, -- Design: Solid Star of Elune
            24200, -- Design: Sparkling Star of Elune
            24201, -- Design: Sparkling Star of Elune
            24202, -- Design: Stormy Star of Elune
            24203, -- Design: Brilliant Living Ruby
            24204, -- Design: Smooth Dawnstone
            24205, -- Design: Rigid Star of Elune
            24206, -- Design: Smooth Dawnstone
            24208, -- Design: Mystic Dawnstone
            24209, -- Design: Sovereign Nightseye
            24210, -- Design: Shifting Nightseye
            24211, -- Design: Timeless Nightseye
            24212, -- Design: Purified Nightseye
            24213, -- Design: Inscribed Noble Topaz
            24214, -- Design: Potent Noble Topaz
            24215, -- Design: Reckless Noble Topaz
            24216, -- Design: Glinting Nightseye
            24217, -- Design: Regal Talasite
            24218, -- Design: Radiant Talasite
            24219, -- Design: Purified Nightseye
            24220, -- Design: Jagged Talasite
            31875, -- Design: Rigid Star of Elune
            31876, -- Design: Shifting Nightseye
            31877, -- Design: Glinting Nightseye
            31878, -- Design: Veiled Nightseye
            31879, -- Design: Deadly Noble Topaz
            33783, -- Design: Steady Talasite
            35304, -- Design: Solid Star of Elune
            35305, -- Design: Brilliant Living Ruby
            35307, -- Design: Rigid Star of Elune
            35322, -- Design: Quick Dawnstone
            35323, -- Design: Reckless Noble Topaz
            35325, -- Design: Forceful Talasite
            24163, -- Design: Heavy Felsteel Ring
            31401, -- Design: The Frozen Eye
            31402, -- Design: The Natural Ward
            32274, -- Design: Bold Crimson Spinel
            32277, -- Design: Delicate Crimson Spinel
            32282, -- Design: Brilliant Crimson Spinel
            32284, -- Design: Subtle Lionseye
            32285, -- Design: Flashing Crimson Spinel
            32286, -- Design: Solid Empyrean Sapphire
            32287, -- Design: Sparkling Empyrean Sapphire
            32289, -- Design: Stormy Empyrean Sapphire
            32291, -- Design: Smooth Lionseye
            32292, -- Design: Rigid Empyrean Sapphire
            32295, -- Design: Mystic Lionseye
            32297, -- Design: Sovereign Shadowsong Amethyst
            32298, -- Design: Shifting Shadowsong Amethyst
            32301, -- Design: Timeless Shadowsong Amethyst
            32303, -- Design: Inscribed Pyrestone
            32304, -- Design: Potent Pyrestone
            32306, -- Design: Glinting Shadowsong Amethyst
            32307, -- Design: Veiled Shadowsong Amethyst
            32308, -- Design: Deadly Pyrestone
            32309, -- Design: Regal Seaspray Emerald
            32310, -- Design: Radiant Seaspray Emerald
            32311, -- Design: Purified Shadowsong Amethyst
            32312, -- Design: Jagged Seaspray Emerald
            35762, -- Design: Reckless Pyrestone
            35763, -- Design: Quick Lionseye
            35764, -- Design: Steady Seaspray Emerald
            35765, -- Design: Forceful Seaspray Emerald
            35766, -- Design: Steady Seaspray Emerald
            35767, -- Design: Reckless Pyrestone
            35768, -- Design: Quick Lionseye
            35769, -- Design: Forceful Seaspray Emerald
            24172, -- Design: Coronet of Verdant Flame
            24173, -- Design: Circlet of Arcane Might
            35198, -- Design: Loop of Forged Power
            35199, -- Design: Ring of Flowing Life
            35200, -- Design: Hard Khorium Band
            35201, -- Design: Pendant of Sunfire
            35202, -- Design: Amulet of Flowing Life
            35203  -- Design: Hard Khorium Choker
        },
        [addon.CONS.R_LEATHERWORKING_ID] = {
            32429, -- Pattern: Boots of Shackled Souls
            32430, -- Pattern: Bracers of Shackled Souls
            32431, -- Pattern: Greaves of Shackled Souls
            32432, -- Pattern: Waistguard of Shackled Souls
            32433, -- Pattern: Redeemed Soul Moccasins
            32434, -- Pattern: Redeemed Soul Wristguards
            32435, -- Pattern: Redeemed Soul Legguards
            32436, -- Pattern: Redeemed Soul Cinch
            29713, -- Pattern: Drums of Panic
            29717, -- Pattern: Drums of Battle
            29721, -- Pattern: Nethercleft Leg Armor
            29722, -- Pattern: Nethercobra Leg Armor
            31362, -- Pattern: Nethercobra Leg Armor
            25725, -- Pattern: Riding Crop
            29677, -- Pattern: Enchanted Felscale Leggings
            29682, -- Pattern: Enchanted Felscale Gloves
            29684, -- Pattern: Enchanted Felscale Boots
            29689, -- Pattern: Flamescale Leggings
            29691, -- Pattern: Flamescale Boots
            29693, -- Pattern: Flamescale Belt
            29698, -- Pattern: Enchanted Clefthoof Leggings
            29700, -- Pattern: Enchanted Clefthoof Gloves
            29701, -- Pattern: Enchanted Clefthoof Boots
            29702, -- Pattern: Blastguard Pants
            29703, -- Pattern: Blastguard Boots
            29704, -- Pattern: Blastguard Belt
            34174, -- Pattern: Drums of Restoration
            34175, -- Pattern: Drums of Restoration
            34172, -- Pattern: Drums of Speed
            34173, -- Pattern: Drums of Speed
            29719, -- Pattern: Cobrahide Leg Armor
            29720, -- Pattern: Clefthide Leg Armor
            31361, -- Pattern: Cobrahide Leg Armor
            25721, -- Pattern: Vindicator's Armor Kit
            25722, -- Pattern: Magister's Armor Kit
            29664, -- Pattern: Reinforced Mining Bag
            30444, -- Pattern: Reinforced Mining Bag
            25726, -- Pattern: Comfortable Insoles
            34491, -- Pattern: Bag of Many Hides
            34262, -- Pattern: Winter Boots
            25735, -- Pattern: Heavy Clefthoof Vest
            25739, -- Pattern: Felstalker Bracers
            25740, -- Pattern: Felstalker Breastplate
            29214, -- Pattern: Felstalker Bracers
            29215, -- Pattern: Felstalker Breastplate
            25736, -- Pattern: Heavy Clefthoof Leggings
            25737, -- Pattern: Heavy Clefthoof Boots
            25728, -- Pattern: Stylin' Purple Hat
            25729, -- Pattern: Stylin' Adventure Hat
            25730, -- Pattern: Stylin' Jungle Hat
            25731, -- Pattern: Stylin' Crimson Hat
            25733, -- Pattern: Fel Leather Boots
            25734, -- Pattern: Fel Leather Leggings
            25738, -- Pattern: Felstalker Belt
            25743, -- Pattern: Netherfury Boots
            29213, -- Pattern: Felstalker Belt
            29218, -- Pattern: Netherfury Boots
            33124, -- Pattern: Cloak of Darkness
            25732, -- Pattern: Fel Leather Gloves
            25741, -- Pattern: Netherfury Belt
            25742, -- Pattern: Netherfury Leggings
            29217, -- Pattern: Netherfury Belt
            29219, -- Pattern: Netherfury Leggings
            30301, -- Pattern: Belt of Natural Power
            30302, -- Pattern: Belt of Deep Shadow
            30303, -- Pattern: Belt of the Black Eagle
            30304, -- Pattern: Monsoon Belt
            30305, -- Pattern: Boots of Natural Grace
            30306, -- Pattern: Boots of Utter Darkness
            30307, -- Pattern: Boots of the Crimson Hawk
            30308, -- Pattern: Hurricane Boots
            35212, -- Pattern: Leather Gauntlets of the Sun
            35213, -- Pattern: Fletcher's Gloves of the Phoenix
            35214, -- Pattern: Gloves of Immortal Dusk
            35215, -- Pattern: Sun-Drenched Scale Gloves
            35216, -- Pattern: Leather Chestguard of the Sun
            35217, -- Pattern: Embrace of the Phoenix
            35218, -- Pattern: Carapace of Sun and Shadow
            35219, -- Pattern: Sun-Drenched Scale Chestguard
            29723, -- Pattern: Cobrascale Hood
            29724, -- Pattern: Cobrascale Gloves
            29725, -- Pattern: Windscale Hood
            29726, -- Pattern: Hood of Primal Life
            29727, -- Pattern: Gloves of the Living Touch
            29728, -- Pattern: Windslayer Wraps
            29729, -- Pattern: Living Dragonscale Helm
            29730, -- Pattern: Earthen Netherscale Boots
            29731, -- Pattern: Windstrike Gloves
            29732, -- Pattern: Netherdrake Helm
            29733, -- Pattern: Netherdrake Gloves
            29734, -- Pattern: Thick Netherscale Breastplate
            33205, -- Pattern: Shadowprowler's Chestguard
            35300, -- Pattern: Windstrike Gloves
            35301, -- Pattern: Netherdrake Gloves
            35302, -- Pattern: Cobrascale Gloves
            35303, -- Pattern: Gloves of the Living Touch
            32744, -- Pattern: Bracers of Renewed Life
            32745, -- Pattern: Shoulderpads of Renewed Life
            32746, -- Pattern: Swiftstrike Bracers
            32747, -- Pattern: Swiftstrike Shoulders
            32748, -- Pattern: Bindings of Lightning Reflexes
            32749, -- Pattern: Shoulders of Lightning Reflexes
            32750, -- Pattern: Living Earth Bindings
            32751  -- Pattern: Living Earth Shoulders
        },
        [addon.CONS.R_MINING_ID] = {
            35273  -- Study of Advanced Smelting
        },
        [addon.CONS.R_TAILORING_ID] = {
            21910, -- Pattern: Spellfire Robe
            21913, -- Pattern: Frozen Shadoweave Robe
            21917, -- Pattern: Primal Mooncloth Robe
            24294, -- Pattern: Runic Spellthread
            24295, -- Pattern: Golden Spellthread
            32437, -- Pattern: Soulguard Slippers
            32438, -- Pattern: Soulguard Bracers
            32439, -- Pattern: Soulguard Leggings
            32440, -- Pattern: Soulguard Girdle
            32447, -- Pattern: Night's End
            38229, -- Pattern: Mycah's Botanical Bag
            21909, -- Pattern: Spellfire Gloves
            21914, -- Pattern: Frozen Shadoweave Boots
            21918, -- Pattern: Primal Mooncloth Shoulders
            21900, -- Pattern: Imbued Netherweave Robe
            21901, -- Pattern: Imbued Netherweave Tunic
            21902, -- Pattern: Soulcloth Gloves
            21908, -- Pattern: Spellfire Belt
            21912, -- Pattern: Frozen Shadoweave Shoulders
            21916, -- Pattern: Primal Mooncloth Belt
            21895, -- Pattern: Primal Mooncloth
            21899, -- Pattern: Imbued Netherweave Boots
            24316, -- Pattern: Spellcloth
            30483, -- Pattern: Shadowcloth
            30833, -- Pattern: Cloak of Arcane Evasion
            30842, -- Pattern: Flameheart Bracers
            30843, -- Pattern: Flameheart Gloves
            30844, -- Pattern: Flameheart Vest
            21894, -- Pattern: Bolt of Soulcloth
            21897, -- Pattern: Netherweave Tunic
            21893, -- Pattern: Imbued Netherweave Bag
            21896, -- Pattern: Netherweave Robe
            21898, -- Pattern: Imbued Netherweave Pants
            24314, -- Pattern: Bag of Jewels
            24292, -- Pattern: Mystic Spellthread
            24293, -- Pattern: Silver Spellthread
            21892, -- Pattern: Bolt of Imbued Netherweave
            37915, -- Pattern: Dress Shoes
            38327, -- Pattern: Haliscan Jacket
            38328, -- Pattern: Haliscan Pantaloons
            21907, -- Pattern: Arcanoweave Robe
            21906, -- Pattern: Arcanoweave Boots
            21905, -- Pattern: Arcanoweave Bracers
            34261, -- Pattern: Green Winter Clothes
            34319, -- Pattern: Red Winter Clothes
            21911, -- Pattern: Spellfire Bag
            21915, -- Pattern: Ebon Shadowbag
            21919, -- Pattern: Primal Mooncloth Bag
            24296, -- Pattern: Unyielding Bracers
            24297, -- Pattern: Bracers of Havok
            24298, -- Pattern: Blackstrike Bracers
            24299, -- Pattern: Cloak of the Black Void
            24300, -- Pattern: Cloak of Eternity
            24301, -- Pattern: White Remedy Cape
            35308, -- Pattern: Unyielding Bracers
            21904, -- Pattern: Soulcloth Vest
            24308, -- Pattern: Whitemend Pants
            24309, -- Pattern: Spellstrike Pants
            24310, -- Pattern: Battlecast Pants
            24311, -- Pattern: Whitemend Hood
            24312, -- Pattern: Spellstrike Hood
            24313, -- Pattern: Battlecast Hood
            30280, -- Pattern: Belt of Blasting
            30281, -- Pattern: Belt of the Long Road
            30282, -- Pattern: Boots of Blasting
            30283, -- Pattern: Boots of the Long Road
            21903, -- Pattern: Soulcloth Shoulders
            24302, -- Pattern: Unyielding Girdle
            24303, -- Pattern: Girdle of Ruination
            24304, -- Pattern: Black Belt of Knowledge
            24305, -- Pattern: Resolute Cape
            24306, -- Pattern: Vengeance Wrap
            24307, -- Pattern: Manaweave Cloak
            35309, -- Pattern: Unyielding Girdle
            32752, -- Pattern: Swiftheal Wraps
            32753, -- Pattern: Swiftheal Mantle
            32754, -- Pattern: Bracers of Nimble Thought
            32755, -- Pattern: Mantle of Nimble Thought
            35204, -- Pattern: Sunfire Handwraps
            35205, -- Pattern: Hands of Eternal Light
            35206, -- Pattern: Sunfire Robe
            35207  -- Pattern: Robe of Eternal Light
        }
    },
    [addon.CONS.TRADE_GOODS_ID] = {
        [addon.CONS.T_CLOTH_ID] = {
            21845, -- Primal Mooncloth
            24271, -- Spellcloth
            24272, -- Shadowcloth
            21844, -- Bolt of Soulcloth
            21842, -- Bolt of Imbued Netherweave
            21840, -- Bolt of Netherweave
            21877, -- Netherweave Cloth
            21881  -- Netherweb Spider Silk
        },
        [addon.CONS.T_ELEMENTAL_ID] = {
            30183, -- Nether Vortex
            22578, -- Mote of Water
            23572, -- Primal Nether
            21884, -- Primal Fire
            21885, -- Primal Water
            21886, -- Primal Life
            22451, -- Primal Air
            22452, -- Primal Earth
            23571, -- Primal Might
            22457, -- Primal Mana
            22572, -- Mote of Air
            22573, -- Mote of Earth
            22574, -- Mote of Fire
            22575, -- Mote of Life
            22576, -- Mote of Mana
            22577, -- Mote of Shadow
            22456  -- Primal Shadow
        },
        [addon.CONS.T_ENCHANTING_ID] = {
            25845, -- Eternium Rod
            22449, -- Large Prismatic Shard
            25844, -- Adamantite Rod
            22450, -- Void Crystal
            22448, -- Small Prismatic Shard
            22446, -- Greater Planar Essence
            22461, -- Runed Fel Iron Rod
            22462, -- Runed Adamantite Rod
            22463, -- Runed Eternium Rod
            22447, -- Lesser Planar Essence
            22445, -- Arcane Dust
            25843  -- Fel Iron Rod
        },
        [addon.CONS.T_PARTS_ID] = {
            23785, -- Hardened Adamantite Tube
            23786, -- Khorium Power Core
            23787, -- Felsteel Stabilizer
            32423, -- Icy Blasting Primers
            23784, -- Adamantite Frame
            23781, -- Elemental Blasting Powder
            23782, -- Fel Iron Casing
            23783  -- Handful of Fel Iron Bolts
        },
        [addon.CONS.T_FISHING_ID] = {
            35285  -- Giant Sunfish
        },
        [addon.CONS.T_HERBS_ID] = {
            22794, -- Fel Lotus
            22792, -- Nightmare Vine
            108350, -- Nightmare Vine Stem
            22793, -- Mana Thistle
            108351, -- Mana Thistle Leaf
            22797, -- Nightmare Seed
            22791, -- Netherbloom
            108349, -- Netherbloom Leaf
            22790, -- Ancient Lichen
            108348, -- Ancient Lichen Petal
            22788, -- Flame Cap
            22787, -- Ragveil
            108346, -- Ragveil Cap
            22785, -- Felweed
            108344, -- Felweed Stalk
            22786, -- Dreaming Glory
            108345, -- Dreaming Glory Petal
            22789, -- Terocone
            108347, -- Terocone Leaf
            22710   -- Bloodthistle
        },
        [addon.CONS.T_JEWELCRAFTING_ID] = {
            24243, -- Adamantite Powder
            32227, -- Crimson Spinel
            32229, -- Lionseye
            32230, -- Shadowsong Amethyst
            32231, -- Pyrestone
            32249, -- Seaspray Emerald
            23436, -- Living Ruby
            23437, -- Talasite
            23438, -- Star of Elune
            23439, -- Noble Topaz
            23440, -- Dawnstone
            23441, -- Nightseye
            25867, -- Earthstorm Diamond
            25868, -- Skyfire Diamond
            32228, -- Empyrean Sapphire
            21929, -- Flame Spessarite
            23079, -- Deep Peridot
            23107, -- Shadow Draenite
            23112, -- Golden Draenite
            23117, -- Azure Moonstone
            23077, -- Blood Garnet
            31079, -- Mercurial Adamantite
            21752, -- Thorium Setting
            24478, -- Jaggal Pearl
            24479, -- Shadow Pearl
            20963, -- Mithril Filigree
            20817, -- Bronze Setting
            20816  -- Delicate Copper Wire
        },
        [addon.CONS.T_LEATHER_ID] = {
            25707, -- Fel Hide
            112182, -- Patch of Fel Hide
            23793, -- Heavy Knothide Leather
            21887, -- Knothide Leather
            25649, -- Knothide Leather Scraps
            25699, -- Crystal Infused Leather
            112180, -- Patch of Crystal Infused Leather
            25700, -- Fel Scales
            112181, -- Fel Scale Fragment
            25708, -- Thick Clefthoof Leather
            112179, -- Patch of Thick Clefthoof Leather
            29539, -- Cobra Scales
            112184, -- Cobra Scale Fragment
            29547, -- Wind Scales
            112185, -- Wind Scale Fragment
            29548, -- Nether Dragonscales
            112183  -- Nether Dragonscale Fragment
        },
        [addon.CONS.T_MEAT_ID] = {
            [addon.CONS.TM_ANIMAL_ID] = {
                24477, -- Jaggal Clam Meat
                27681, -- Warped Flesh
                27674, -- Ravager Flesh
                27677, -- Chunk o' Basilisk
                27678, -- Clefthoof Meat
                27671, -- Buzzard Meat
                27682, -- Talbuk Venison
                31670, -- Raptor Ribs
                31671, -- Serpent Flesh 
                35562, -- Bear Flank
                27669  -- Bat Flesh
            },
            [addon.CONS.TM_FISH_ID] = {
                33823, -- Bloodfin Catfish
                33824, -- Crescent-Tail Skullfish
                27422, -- Barbed Gill Trout
                27425, -- Spotted Feltail
                27429, -- Zangarian Sporefish
                27435, -- Figluster's Mudfish
                27437, -- Icefin Bluefish
                27438, -- Golden Darter
                27439  -- Furious Crawdad
            }
        },
        [addon.CONS.T_METAL_STONE_ID] = {
            23426, -- Khorium Ore
            108304, -- Khorium Ore Nugget
            23447, -- Eternium Bar
            23449, -- Khorium Bar
            35128, -- Hardened Khorium
            23427, -- Eternium Ore
            108303, -- Eternium Ore Nugget
            23425, -- Adamantite Ore
            108302, -- Adamantite Ore Nugget
            23446, -- Adamantite Bar
            23573, -- Hardened Adamantite Bar
            23448, -- Felsteel Bar
            23424, -- Fel Iron Ore
            108301, -- Fel Iron Ore Nugget
            23445  -- Fel Iron Bar
        },
        [addon.CONS.T_OTHER_ID] = {
            24476, -- Jaggal Clam
            21882, -- Soul Essence
            27511, -- Inscribed Scrollcase
            27513, -- Curious Crate
            20815, -- Jeweler's Kit
            30817, -- Simple Flour
            34664, -- Sunmote
            32428  -- Heart of Darkness
        }
    }
}