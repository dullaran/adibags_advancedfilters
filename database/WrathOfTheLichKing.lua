local _, addon = ...

addon.ITEM_DATABASE[addon.CONS.WRATH_OF_THE_LICH_KING_ID] = {
    [addon.CONS.CONTAINERS_ID] = {
        38145, -- Deathweave Bag
        41599, -- Frostweave Bag
        41597, -- Abyssal Bag
        41600, -- Glacial Bag
        43345, -- Dragon Hide Bag
        50316, -- Papa's Brand New Bag
        50317, -- Papa's New Bag
        51809, -- Portable Hole
        49295, -- Enlarged Onyxia Hide
        41598, -- Mysterious Bag
        45773, -- Emerald Bag
        44446, -- Pack of Endless Pockets
        39489, -- Scribe's Satchel
        38399, -- Trapper's Traveling Pack
        38347  -- Mammoth Mining Bag
    },
    [addon.CONS.CONSUMABLES_ID] = {
        [addon.CONS.C_BANDAGES_ID] = {
            34722, -- Heavy Frostweave Bandage
            34721  -- Frostweave Bandage
        },
        [addon.CONS.C_CONSUMABLES_ID] = {
            37567, -- Healing Injector Kit
            40536, -- Explosive Decoy
            40769, -- Scrapbot Construction Kit
            42546, -- Mana Injector Kit
            47828, -- Goblin Beam Welder
            41119, -- Saronite Bomb
            42641, -- Global Thermal Sapper Charge
            44951, -- Box of Bombs
            40771, -- Cobalt Frag Bomb
            40772, -- Gnomish Army Knife
            37161, -- Five of Swords
            40892, -- Hammer Pick
            40893, -- Bladed Pickaxe
            37330, -- Formula: Enchant Cloak - Superior Arcane Resistance
            37331, -- Formula: Enchant Cloak - Superior Fire Resistance
            37332, -- Formula: Enchant Cloak - Superior Frost Resistance
            37333, -- Formula: Enchant Cloak - Superior Nature Resistance
            37334, -- Formula: Enchant Cloak - Superior Shadow Resistance
            54343, -- Blue Crashin' Thrashin' Racer Controller
            40768, -- MOLL-E
            40895, -- Gnomish X-Ray Specs
            44559, -- Pattern: Fur Lining - Fire Resist
            44560, -- Pattern: Fur Lining - Frost Resist
            44561, -- Pattern: Fur Lining - Shadow Resist
            44562, -- Pattern: Fur Lining - Nature Resist
            44563, -- Pattern: Fur Lining - Arcane Resist
            46349, -- Chef's Hat
            49040  -- Jeeves
        },
        [addon.CONS.C_ELIXIRS_ID] = {
            39666, -- Elixir of Mighty Agility
            40068, -- Wrath Elixir
            40070, -- Spellpower Elixir
            40072, -- Elixir of Versatility
            40073, -- Elixir of Mighty Strength
            40076, -- Guru's Elixir
            40078, -- Elixir of Mighty Fortitude
            40097, -- Elixir of Protection
            40109, -- Elixir of Mighty Mageblood
            44325, -- Elixir of Accuracy
            44327, -- Elixir of Deadly Strikes
            44328, -- Elixir of Mighty Defense
            44329, -- Elixir of Expertise
            44330, -- Elixir of Armor Piercing
            44331, -- Elixir of Lightning Speed
            44332, -- Elixir of Mighty Thoughts
            44012, -- Underbelly Elixir
            37449, -- Breath of Murloc
            45621  -- Elixir of Minor Accuracy
        },
        [addon.CONS.C_FLASKS_ID] = {
            40079, -- Lesser Flask of Toughness
            40082, -- Mixture of the Frost Wyrm
            40083, -- Mixture of Stoneblood
            40084, -- Mixture of Endless Rage
            40404, -- Mixture of Pure Mojo
            46376, -- Flask of the Frost Wyrm
            46377, -- Flask of Endless Rage
            46378, -- Flask of Pure Mojo
            46379  -- Flask of Stoneblood
        },
        [addon.CONS.C_FOOD_DRINKS_ID] = {
            43488, -- Last Week's Mammoth
            43490, -- Tasty Cupcake
            43491, -- Bad Clams
            43492, -- Haunted Herring
            43523, -- Conjured Mana Strudel
            33445, -- Honeymint Tea
            35947, -- Sparkling Frostcap
            35948, -- Savory Snowplum
            35950, -- Sweet Potato Bread
            35951, -- Poached Emperor Salmon
            35952, -- Briny Hardcheese
            35953, -- Mead Basted Caribou
            38706, -- Bowels 'n' Brains
            39520, -- Kungaloosh
            40202, -- Sizzling Grizzly Flank
            41729, -- Stewed Drakeflesh
            41731, -- Yeti Milk
            42429, -- Red Velvet Cupcake
            42431, -- Dalaran Brownie
            42434, -- Lovely Cake Slice
            42777, -- Crusader's Waterskin
            42778, -- Crusader's Rations
            42779, -- Steaming Chicken Soup
            43087, -- Crisp Dalaran Apple
            43236, -- Star's Sorrow
            44049, -- Freshly-Speared Emperor Salmon
            44071, -- Slow-Roasted Eel
            44072, -- Roasted Mystery Beast
            44607, -- Aged Dalaran Sharp
            44722, -- Aged Yolk
            44940, -- Corn-Breaded Sausage
            45932, -- Black Jelly
            43518, -- Conjured Mana Pie
            33444, -- Pungent Seal Whey
            34125, -- Shoveltusk Soup
            34747, -- Northern Stew
            34748, -- Mammoth Meal
            34749, -- Shoveltusk Steak
            34750, -- Worm Delight
            34751, -- Roasted Worg
            34752, -- Rhino Dogs
            34753, -- Great Feast
            34754, -- Mega Mammoth Meal
            34755, -- Tender Shoveltusk Steak
            34756, -- Spiced Worm Burger
            34757, -- Very Burnt Worg
            34758, -- Mighty Rhino Dogs
            34759, -- Smoked Rockfin
            34760, -- Grilled Bonescale
            34761, -- Sauteed Goby
            34762, -- Grilled Sculpin
            34763, -- Smoked Salmon
            34764, -- Poached Nettlefish
            34765, -- Pickled Fangtooth
            34766, -- Poached Northern Sculpin
            34767, -- Firecracker Salmon
            34768, -- Spicy Blue Nettlefish
            34769, -- Imperial Manta Steak
            38698, -- Bitter Plasma
            39691, -- Succulent Orca Stew
            42428, -- Carrot Cupcake
            42430, -- Dalaran Doughnut
            42432, -- Berry Pie Slice
            42433, -- Chocolate Cake Slice
            42942, -- Baked Manta Ray
            42993, -- Spicy Fried Herring
            42994, -- Rhinolicious Wormsteak
            42995, -- Hearty Rhino
            42996, -- Snapper Extreme
            42997, -- Blackened Worg Steak
            42998, -- Cuttlesteak
            42999, -- Blackened Dragonfin
            43000, -- Dragonfin Filet
            43001, -- Tracker Snacks
            43004, -- Critter Bites
            43005, -- Spiced Mammoth Treats
            43015, -- Fish Feast
            43086, -- Fresh Apple Juice
            43268, -- Dalaran Clam Chowder
            43478, -- Gigantic Feast
            43480, -- Small Feast
            44616, -- Glass of Dalaran White
            44941, -- Fresh-Squeezed Limeade
            44953, -- Worg Tartare
            33443, -- Sour Goat Cheese
            33449, -- Crusty Flatbread
            33451, -- Fillet of Icefin
            33452, -- Honey-Spiced Lichen
            33454, -- Salted Venison
            35949, -- Tundra Berries
            35954, -- Sweetened Goat's Milk
            37252, -- Frostberries
            37253, -- Frostberry Juice
            37452, -- Fatty Bluefin
            40356, -- Grizzleberries
            40357, -- Grizzleberry Juice
            40358, -- Raw Tallhorn Chunk
            40359, -- Fresh Eagle Meat
            44608, -- Dalaran Swiss
            44609, -- Fresh Dalaran Bread Slice
            44749, -- Salted Yeti Cheese
            44750, -- Mountain Water
            46887, -- Bountiful Feast
            41751, -- Black Mushroom
            44791, -- Noblegarden Chocolate
            46691, -- Bread of the Dead
            33004, -- Clamlette Magnifique
            44574, -- Skin of Mulgore Firewater
            40042, -- Caraway Burnwine
            44573, -- Cup of Frog Venom Brew
            40036, -- Snowplum Brandy
            44571, -- Bottle of Silvermoon Port
            44575, -- Flask of Bitter Cactus Cider
            38350, -- Winterfin "Depth Charge"
            40035, -- Northrend Honey Mead
            43695, -- Half Full Bottle of Prison Moonshine
            43696, -- Half Empty Bottle of Prison Moonshine
            44114, -- Old Spices
            44228, -- Baby Spice
            44570, -- Glass of Eversong Wine
            44617, -- Glass of Dalaran Red
            44716, -- Mysterious Fermented Liquid
            44836, -- Pumpkin Pie
            44837, -- Spice Bread Stuffing
            44838, -- Slow-Roasted Turkey
            44839, -- Candied Sweet Potato
            44840, -- Cranberry Chutney
            44854, -- Tangy Wetland Cranberries
            44855, -- Teldrassil Sweet Potato
            46690, -- Candy Skull
            46784, -- Ripe Elwynn Pumpkin
            46793, -- Tangy Southfury Cranberries
            46796, -- Ripe Tirisfal Pumpkin
            46797, -- Mulgore Sweet Potato
            44618, -- Glass of Aged Dalaran Red
            44619  -- Glass of Peaked Dalaran Red
        },
        [addon.CONS.C_ITEM_ENHANCEMENTS_ID] = {
            38376, -- Heavy Borean Armor Kit
            41976, -- Titanium Weapon Chain
            42500, -- Titanium Shield Spike
            41167, -- Heartseeker Scope
            41601, -- Shining Spellthread
            41603, -- Azure Spellthread
            44067, -- Inscription of Triumph
            44068, -- Inscription of Dominance
            44129, -- Lesser Inscription of the Storm 
            44130, -- Lesser Inscription of the Crag
            44131, -- Lesser Inscription of the Axe
            44132, -- Lesser Inscription of the Pinnacle
            44936, -- Titanium Plating
            38371, -- Jormungar Leg Armor
            38372, -- Nerubian Leg Armor
            41146, -- Sun Scope
            38948, -- Enchant Weapon - Executioner
            38963, -- Enchant Weapon - Exceptional Versatility
            38965, -- Enchant Weapon - Icebreaker
            38972, -- Enchant Weapon - Lifeward
            38981, -- Enchant 2H Weapon - Scourgebane
            38988, -- Enchant Weapon - Giant Slayer
            38991, -- Enchant Weapon - Exceptional Spellpower
            38992, -- Enchant 2H Weapon - Greater Savagery
            38995, -- Enchant Weapon - Exceptional Agility
            38998, -- Enchant Weapon - Deathfrost
            43987, -- Enchant Weapon - Black Magic
            44453, -- Enchant Weapon - Greater Potency
            44463, -- Enchant 2H Weapon - Massacre
            44466, -- Enchant Weapon - Superior Potency
            44467, -- Enchant Weapon - Mighty Spellpower
            44493, -- Enchant Weapon - Berserking
            44497, -- Enchant Weapon - Accuracy
            45056, -- Enchant Staff - Greater Spellpower
            45060, -- Enchant Staff - Spellpower
            38917, -- Enchant Weapon - Major Striking
            38918, -- Enchant Weapon - Major Intellect
            38919, -- Enchant 2H Weapon - Savagery
            38920, -- Enchant Weapon - Potency
            38921, -- Enchant Weapon - Major Spellpower
            38922, -- Enchant 2H Weapon - Major Agility
            38923, -- Enchant Weapon - Sunfire
            38924, -- Enchant Weapon - Soulfrost
            38925, -- Enchant Weapon - Mongoose
            38926, -- Enchant Weapon - Spellsurge
            38927, -- Enchant Weapon - Battlemaster
            38946, -- Enchant Weapon - Major Healing
            38947, -- Enchant Weapon - Greater Agility
            38772, -- Enchant 2H Weapon - Minor Impact
            38779, -- Enchant Weapon - Minor Beastslayer
            38780, -- Enchant Weapon - Minor Striking
            38781, -- Enchant 2H Weapon - Lesser Intellect
            38788, -- Enchant 2H Weapon - Lesser Versatility
            38794, -- Enchant Weapon - Lesser Striking
            38796, -- Enchant 2H Weapon - Lesser Impact
            38813, -- Enchant Weapon - Lesser Beastslayer
            38814, -- Enchant Weapon - Lesser Elemental Slayer
            38821, -- Enchant Weapon - Striking
            38822, -- Enchant 2H Weapon - Impact
            38838, -- Enchant Weapon - Fiery Weapon
            38840, -- Enchant Weapon - Demonslaying
            38845, -- Enchant 2H Weapon - Greater Impact
            38848, -- Enchant Weapon - Greater Striking
            38868, -- Enchant Weapon - Icy Chill
            38869, -- Enchant 2H Weapon - Superior Impact
            38870, -- Enchant Weapon - Superior Striking
            38871, -- Enchant Weapon - Lifestealing
            38872, -- Enchant Weapon - Unholy Weapon
            38873, -- Enchant Weapon - Crusader
            38874, -- Enchant 2H Weapon - Major Versatility
            38875, -- Enchant 2H Weapon - Major Intellect
            38876, -- Enchant Weapon - Winter's Might
            38877, -- Enchant Weapon - Spellpower
            38878, -- Enchant Weapon - Healing Power
            38879, -- Enchant Weapon - Strength
            38880, -- Enchant Weapon - Agility
            38883, -- Enchant Weapon - Mighty Versatility
            38884, -- Enchant Weapon - Mighty Intellect
            38896, -- Enchant 2H Weapon - Agility
            46026, -- Enchant Weapon - Blade Ward
            46098, -- Enchant Weapon - Blood Draining
            38373, -- Frosthide Leg Armor
            38374, -- Icescale Leg Armor
            41602, -- Brilliant Spellthread
            41604, -- Sapphire Spellthread
            44957, -- Greater Inscription of the Gladiator
            44963, -- Earthen Leg Armor
            44133, -- Greater Inscription of the Axe
            44134, -- Greater Inscription of the Crag
            44135, -- Greater Inscription of the Storm
            44136, -- Greater Inscription of the Pinnacle
            50335, -- Greater Inscription of the Axe
            50336, -- Greater Inscription of the Crag
            50337, -- Greater Inscription of the Pinnacle
            50338, -- Greater Inscription of the Storm
            46006  -- Glow Worm
        },
        [addon.CONS.C_POTIONS_ID] = {
            33447, -- Runic Healing Potion
            33448, -- Runic Mana Potion
            40077, -- Crazy Alchemist's Potion
            40081, -- Potion of Nightmares
            40087, -- Powerful Rejuvenation Potion
            40093, -- Indestructible Potion
            40211, -- Potion of Speed
            40212, -- Potion of Wild Magic
            40213, -- Mighty Arcane Protection Potion
            40214, -- Mighty Fire Protection Potion
            40215, -- Mighty Frost Protection Potion
            40216, -- Mighty Nature Protection Potion
            40217, -- Mighty Shadow Protection Potion
            41166, -- Runic Healing Injector
            42545, -- Runic Mana Injector
            39671, -- Resurgent Healing Potion
            40067, -- Icy Mana Potion
            38351, -- Murliver Oil
            39327, -- Noth's Special Brew
            36770, -- Zort's Protective Elixir
            43569, -- Endless Healing Potion
            43570  -- Endless Mana Potion
        },
        [addon.CONS.C_OTHER_ID] = {
            37464, -- Winterfin Horn of Distress
            42438, -- Lovely Cake
            43088, -- Dalaran Apple Bowl
            44613, -- Aged Dalaran Sharp Wheel
            44698, -- Intravenous Healing Potion
            46399, -- Thunder's Plunder
            46400, -- Barleybrew Gold
            46401, -- Crimson Stripe
            46402, -- Promise of the Pandaren
            46403, -- Chuganpug's Delight
            37092, -- Scroll of Intellect VIII
            37094, -- Scroll of Stamina VIII
            37098, -- Scroll of Versatility VIII
            43135, -- Fate Rune of Fleet Feet
            43464, -- Scroll of Agility VIII
            43466, -- Scroll of Strength VIII
            33312, -- Mana Sapphire
            39738, -- Thunderbrew's Hard Ale
            42436, -- Chocolate Celebration Cake
            42439, -- Big Berry Pie
            44610, -- Fresh Dalaran Bread
            44612, -- Dalaran Swiss Wheel
            41509, -- Frostweave Net
            37091, -- Scroll of Intellect VII
            37093, -- Scroll of Stamina VII
            37097, -- Scroll of Versatility VII
            39213, -- Massive Seaforium Charge
            42986, -- The RP-GG
            43002, -- Inflatable Land Mines
            43463, -- Scroll of Agility VII
            43465, -- Scroll of Strength VII
            43467, -- Scroll of Protection VII
            44315, -- Scroll of Recall III
            44621, -- Bottle of Dalaran White
            44622, -- Cask of Dalaran White
            44623, -- Bottle of Dalaran Red
            44632, -- Cask of Dalaran Red
            46847, -- Seaforium Bombs
            47030, -- Huge Seaforium Bombs
            33457, -- Scroll of Agility VI
            33458, -- Scroll of Intellect VI
            33459, -- Scroll of Protection VI
            33460, -- Scroll of Versatility VI
            33461, -- Scroll of Stamina VI
            33462, -- Scroll of Strength VI
            43850, -- Certificate of Ownership
            44314, -- Scroll of Recall II
            35946, -- Fizzcrank Practice Parachute
            37198, -- Prototype Neural Needler
            41427, -- Dalaran Firework
            37118, -- Scroll of Recall
            37488, -- Wild Winter Pilsner
            37489, -- Izzard's Ever Flavor
            37490, -- Aromatic Honey Brew
            37491, -- Metok's Bubble Bock
            37492, -- Springtime Stout
            37493, -- Blackrock Lager
            37494, -- Stranglethorn Brew
            37495, -- Draenic Pale Ale
            37498, -- Bartlett's Bitter Brew
            37499, -- Lord of Frost's Private Label
            37898, -- Wild Winter Pilsner
            37899, -- Izzard's Ever Flavor
            37900, -- Aromatic Honey Brew
            37901, -- Metok's Bubble Bock
            37906, -- Binary Brew
            37907, -- Autumnal Acorn Ale
            37908, -- Bartlett's Bitter Brew
            37909, -- Lord of Frost's Private Label
            33099, -- Intact Plague Container
            33349, -- Plague Vials
            33614, -- Empty Apothecary's Flask
            33615, -- Flask of Vrykul Blood
            33616, -- Unstable Mix
            33617, -- Balanced Concoction
            33619, -- Lysander's Strain
            33621, -- Plague Spray
            34000, -- Blood Elf Female Mask
            34001, -- Draenei Female Mask
            34002, -- Blood Elf Male Mask
            34003, -- Draenei Male Mask
            34023, -- Empty Apothecary's Flask
            34044, -- B-Ball
            34076, -- Fish Bladder
            35704, -- Incendiary Explosives
            36771, -- Sturdy Crates
            37173, -- Geomancer's Orb
            37265, -- Tua'kea's Breathing Bladder
            37661, -- Gossamer Potion
            37815, -- Emerald Essence
            37859, -- Amber Essence
            37860, -- Ruby Essence
            37877, -- Silver Feather
            37925, -- Experimental Mixture
            38266, -- Rotund Relic
            38629, -- Orders from the Lich King
            38657, -- Freya's Ward
            39698, -- Light-Infused Artifact
            39878, -- Mysterious Egg
            39883, -- Cracked Egg
            40110, -- Haunted Memento
            40390, -- Vic's Emergency Air Tank
            40482, -- Dual-Plagued Brain
            41426, -- Magically Wrapped Gift
            43462, -- Airy Pale Ale
            43470, -- Worg Tooth Oatmeal Stout
            43472, -- Snowfall Lager
            43473, -- Drakefire Chile Ale
            44064, -- Nepeta Leaf
            44065, -- Oracle Secret Solution
            44113, -- Small Spice Bag
            44481, -- Grindgear Toy Gorilla
            44482, -- Trusty Copper Racer
            44576, -- Bright Flare
            44599, -- Zippy Copper Racer
            44601, -- Heavy Copper Racer
            44717, -- Disgusting Jar
            44718, -- Ripe Disgusting Jar
            44751, -- Hyldnir Spoils
            44792, -- Blossoming Branch
            44806, -- Brightly Colored Shell Fragment
            44812, -- Turkey Shooter
            44818, -- Noblegarden Egg
            44844, -- Turkey Caller
            45784, -- Thorim's Sigil
            45786, -- Hodir's Sigil
            45787, -- Mimiron's Sigil
            45788, -- Freya's Sigil
            45791, -- Sigils of the Watchers
            45798, -- Heroic Celestial Planetarium Key
            45814, -- Freya's Sigil
            45815, -- Hodir's Sigil
            45816, -- Mimiron's Sigil
            45817, -- Thorim's Sigil
            45855, -- Sigils of the Watchers
            46029, -- Magnetic Core
            46711, -- Spirit Candle
            46718, -- Orange Marigold
            46765, -- Blue War Fuel
            46766, -- Red War Fuel
            46779, -- Path of Cenarius
            46783, -- Pink Gumball
            49631, -- Standard Apothecary Serving Kit
            49856, -- "VICTORY" Perfume
            49857, -- "Enchantress" Perfume
            49858, -- "Forever" Perfume
            49859, -- "Bravado" Cologne
            49860, -- "Wizardry" Cologne
            49861, -- "STALWART" Cologne
            49936, -- Lovely Stormwind Card
            49937, -- Lovely Undercity Card
            49938, -- Lovely Darnassus Card
            49939, -- Lovely Orgrimmar Card
            49940, -- Lovely Ironforge Card
            49941, -- Lovely Thunder Bluff Card
            49942, -- Lovely Exodar Card
            49943, -- Lovely Silvermoon City Card
            50163, -- Lovely Rose
            50164, -- Fras Siabi's Barely Bigger Beer
            50307, -- Infernal Spear
            54455, -- Paint Bomb
            43853, -- Titanium Skeleton Key
            41367, -- Dark Jade Focusing Lens
            42420, -- Shadow Crystal Focusing Lens
            42421, -- Shadow Jade Focusing Lens
            43854, -- Cobalt Skeleton Key
            44625, -- Bottle of Aged Dalaran Red
            44626, -- Cask of Aged Dalaran Red
            43950, -- Kirin Tor Commendation Badge
            44435, -- Windle's Lighter
            44710, -- Wyrmrest Commendation Badge
            44711, -- Argent Crusade Commendation Badge
            44713, -- Ebon Blade Commendation Badge
            44817, -- The Mischief Maker
            45714, -- Darnassus Commendation Badge
            45715, -- Exodar Commendation Badge
            45716, -- Gnomeregan Commendation Badge
            45717, -- Ironforge Commendation Badge
            45718, -- Stormwind Commendation Badge
            45719, -- Orgrimmar Commendation Badge
            45720, -- Sen'jin Commendation Badge
            45721, -- Silvermoon Commendation Badge
            45722, -- Thunder Bluff Commendation Badge
            45723, -- Undercity Commendation Badge
            46725, -- Red Rider Air Rifle
            49702, -- Sons of Hodir Commendation Badge
            54437, -- Tiny Green Ragdoll
            54438, -- Tiny Blue Ragdoll
            44943, -- Icy Prism
            47541, -- Argent Pony Bridle
            44627, -- Bottle of Peaked Dalaran Red
            44629, -- Cask of Peaked Dalaran Red
            36862, -- Worn Troll Dice
            36863, -- Decahedral Dwarven Dice
            44606, -- Toy Train Set
            45011, -- Stormwind Banner
            45013, -- Thunder Bluff Banner
            45014, -- Orgrimmar Banner
            45015, -- Sen'jin Banner
            45016, -- Undercity Banner
            45017, -- Silvermoon City Banner
            45018, -- Ironforge Banner
            45019, -- Gnomeregan Banner
            45020, -- Exodar Banner
            45021, -- Darnassus Banner
            45047, -- Sandbox Tiger
            45063, -- Foam Sword Rack
            46780, -- Ogre Pinata
            46843, -- Argent Crusader's Banner
            50471, -- The Heartbreaker
            54212, -- Instant Statue Pedestal
            45038, -- Fragment of Val'anyr
            45896, -- Unbound Fragments of Val'anyr
            45897  -- Reforged Hammer of Ancient Kings
        }
    },
    [addon.CONS.GEMS_ID] = {
        39900, -- Bold Bloodstone
        39905, -- Delicate Bloodstone
        39907, -- Subtle Sun Crystal
        39908, -- Flashing Bloodstone
        39909, -- Smooth Sun Crystal
        39910, -- Precise Bloodstone
        39912, -- Brilliant Bloodstone
        39915, -- Rigid Chalcedony
        39917, -- Mystic Sun Crystal
        39918, -- Quick Sun Crystal
        39919, -- Solid Chalcedony
        39927, -- Sparkling Chalcedony
        39932, -- Stormy Chalcedony
        39933, -- Jagged Dark Jade
        39934, -- Sovereign Shadow Crystal
        39935, -- Shifting Shadow Crystal
        39939, -- Defender's Shadow Crystal
        39940, -- Guardian's Shadow Crystal
        39942, -- Glinting Shadow Crystal
        39945, -- Mysterious Shadow Crystal
        39947, -- Inscribed Huge Citrine
        39948, -- Etched Shadow Crystal
        39949, -- Champion's Huge Citrine
        39950, -- Resplendent Huge Citrine
        39951, -- Fierce Huge Citrine
        39952, -- Deadly Huge Citrine
        39954, -- Lucent Huge Citrine
        39955, -- Deft Huge Citrine
        39956, -- Potent Huge Citrine
        39957, -- Veiled Shadow Crystal
        39958, -- Willful Huge Citrine
        39959, -- Reckless Huge Citrine
        39965, -- Stalwart Huge Citrine
        39966, -- Accurate Shadow Crystal
        39967, -- Resolute Huge Citrine
        39968, -- Timeless Shadow Crystal
        39975, -- Nimble Dark Jade
        39976, -- Regal Dark Jade
        39977, -- Steady Dark Jade
        39978, -- Forceful Dark Jade
        39979, -- Purified Shadow Crystal
        39980, -- Misty Dark Jade
        39981, -- Lightning Dark Jade
        39982, -- Turbid Dark Jade
        39983, -- Energized Dark Jade
        39991, -- Radiant Dark Jade
        39992, -- Shattered Dark Jade
        41432, -- Perfect Bold Bloodstone
        41434, -- Perfect Delicate Bloodstone
        41435, -- Perfect Flashing Bloodstone
        41436, -- Perfect Smooth Sun Crystal
        41437, -- Perfect Precise Bloodstone
        41439, -- Perfect Subtle Sun Crystal
        41440, -- Perfect Sparkling Chalcedony
        41441, -- Perfect Solid Chalcedony
        41443, -- Perfect Stormy Chalcedony
        41444, -- Perfect Brilliant Bloodstone
        41445, -- Perfect Mystic Sun Crystal
        41446, -- Perfect Quick Sun Crystal
        41447, -- Perfect Rigid Chalcedony
        41450, -- Perfect Shifting Shadow Crystal
        41451, -- Perfect Defender's Shadow Crystal
        41452, -- Perfect Timeless Shadow Crystal
        41453, -- Perfect Guardian's Shadow Crystal
        41455, -- Perfect Mysterious Shadow Crystal
        41461, -- Perfect Sovereign Shadow Crystal
        41462, -- Perfect Glinting Shadow Crystal
        41464, -- Perfect Regal Dark Jade
        41466, -- Perfect Forceful Dark Jade
        41467, -- Perfect Energized Dark Jade
        41468, -- Perfect Jagged Dark Jade
        41470, -- Perfect Misty Dark Jade
        41473, -- Perfect Purified Shadow Crystal
        41474, -- Perfect Shattered Dark Jade
        41475, -- Perfect Lightning Dark Jade
        41476, -- Perfect Steady Dark Jade
        41478, -- Perfect Radiant Dark Jade
        41480, -- Perfect Turbid Dark Jade
        41481, -- Perfect Nimble Dark Jade
        41482, -- Perfect Accurate Shadow Crystal
        41483, -- Perfect Champion's Huge Citrine
        41484, -- Perfect Deadly Huge Citrine
        41485, -- Perfect Deft Huge Citrine
        41486, -- Perfect Willful Huge Citrine
        41488, -- Perfect Etched Shadow Crystal
        41489, -- Perfect Fierce Huge Citrine
        41490, -- Perfect Stalwart Huge Citrine
        41492, -- Perfect Inscribed Huge Citrine
        41493, -- Perfect Lucent Huge Citrine
        41495, -- Perfect Potent Huge Citrine
        41497, -- Perfect Reckless Huge Citrine
        41498, -- Perfect Resolute Huge Citrine
        41499, -- Perfect Resplendent Huge Citrine
        41502, -- Perfect Veiled Shadow Crystal
        42701, -- Enchanted Pearl
        39996, -- Bold Scarlet Ruby
        39997, -- Delicate Scarlet Ruby
        39998, -- Brilliant Scarlet Ruby
        40000, -- Subtle Autumn's Glow
        40001, -- Flashing Scarlet Ruby
        40003, -- Precise Scarlet Ruby
        40008, -- Solid Sky Sapphire
        40010, -- Sparkling Sky Sapphire
        40011, -- Stormy Sky Sapphire
        40013, -- Smooth Autumn's Glow
        40014, -- Rigid Sky Sapphire
        40016, -- Mystic Autumn's Glow
        40017, -- Quick Autumn's Glow
        40022, -- Sovereign Twilight Opal
        40023, -- Shifting Twilight Opal
        40025, -- Timeless Twilight Opal
        40026, -- Purified Twilight Opal
        40028, -- Mysterious Twilight Opal
        40032, -- Defender's Twilight Opal
        40034, -- Guardian's Twilight Opal
        40037, -- Inscribed Monarch Topaz
        40038, -- Etched Twilight Opal
        40039, -- Champion's Monarch Topaz
        40040, -- Resplendent Monarch Topaz
        40041, -- Fierce Monarch Topaz
        40044, -- Glinting Twilight Opal
        40045, -- Lucent Monarch Topaz
        40048, -- Potent Monarch Topaz
        40049, -- Veiled Twilight Opal
        40050, -- Willful Monarch Topaz
        40051, -- Reckless Monarch Topaz
        40052, -- Deadly Monarch Topaz
        40055, -- Deft Monarch Topaz
        40057, -- Stalwart Monarch Topaz
        40058, -- Accurate Twilight Opal
        40059, -- Resolute Monarch Topaz
        40086, -- Jagged Forest Emerald
        40088, -- Nimble Forest Emerald
        40089, -- Regal Forest Emerald
        40090, -- Steady Forest Emerald
        40091, -- Forceful Forest Emerald
        40095, -- Misty Forest Emerald
        40098, -- Radiant Forest Emerald
        40100, -- Lightning Forest Emerald
        40102, -- Turbid Forest Emerald
        40105, -- Energized Forest Emerald
        40106, -- Shattered Forest Emerald
        41285, -- Chaotic Skyflare Diamond
        41307, -- Destructive Skyflare Diamond
        41333, -- Ember Skyflare Diamond
        41335, -- Enigmatic Skyflare Diamond
        41339, -- Swift Skyflare Diamond
        41375, -- Tireless Skyflare Diamond
        41376, -- Revitalizing Skyflare Diamond
        41377, -- Shielded Skyflare Diamond
        41378, -- Forlorn Skyflare Diamond
        41379, -- Impassive Skyflare Diamond
        41380, -- Austere Earthsiege Diamond
        41381, -- Persistent Earthsiege Diamond
        41382, -- Trenchant Earthsiege Diamond
        41385, -- Invigorating Earthsiege Diamond
        41389, -- Beaming Earthsiege Diamond
        41395, -- Bracing Earthsiege Diamond
        41396, -- Eternal Earthsiege Diamond
        41397, -- Powerful Earthsiege Diamond
        41398, -- Relentless Earthsiege Diamond
        41400, -- Thundering Skyflare Diamond
        41401, -- Insightful Earthsiege Diamond
        42702, -- Enchanted Tear
        44076, -- Swift Starflare Diamond
        44078, -- Tireless Starflare Diamond
        44081, -- Enigmatic Starflare Diamond
        44082, -- Impassive Starflare Diamond
        44084, -- Forlorn Starflare Diamond
        44087, -- Persistent Earthshatter Diamond
        44088, -- Powerful Earthshatter Diamond
        44089, -- Trenchant Earthshatter Diamond
        36767, -- Solid Dragon's Eye
        40111, -- Bold Cardinal Ruby
        40112, -- Delicate Cardinal Ruby
        40113, -- Brilliant Cardinal Ruby
        40115, -- Subtle King's Amber
        40116, -- Flashing Cardinal Ruby
        40118, -- Precise Cardinal Ruby
        40119, -- Solid Majestic Zircon
        40120, -- Sparkling Majestic Zircon
        40122, -- Stormy Majestic Zircon
        40124, -- Smooth King's Amber
        40125, -- Rigid Majestic Zircon
        40127, -- Mystic King's Amber
        40128, -- Quick King's Amber
        40129, -- Sovereign Dreadstone
        40130, -- Shifting Dreadstone
        40133, -- Purified Dreadstone
        40135, -- Mysterious Dreadstone
        40139, -- Defender's Dreadstone
        40141, -- Guardian's Dreadstone
        40142, -- Inscribed Ametrine
        40143, -- Etched Dreadstone
        40144, -- Champion's Ametrine
        40145, -- Resplendent Ametrine
        40146, -- Fierce Ametrine
        40147, -- Deadly Ametrine
        40149, -- Lucent Ametrine
        40150, -- Deft Ametrine
        40152, -- Potent Ametrine
        40153, -- Veiled Dreadstone
        40154, -- Willful Ametrine
        40155, -- Reckless Ametrine
        40157, -- Glinting Dreadstone
        40160, -- Stalwart Ametrine
        40162, -- Accurate Dreadstone
        40163, -- Resolute Ametrine
        40164, -- Timeless Dreadstone
        40165, -- Jagged Eye of Zul
        40166, -- Nimble Eye of Zul
        40167, -- Regal Eye of Zul
        40168, -- Steady Eye of Zul
        40169, -- Forceful Eye of Zul
        40171, -- Misty Eye of Zul
        40173, -- Turbid Eye of Zul
        40177, -- Lightning Eye of Zul
        40179, -- Energized Eye of Zul
        40180, -- Radiant Eye of Zul
        40182, -- Shattered Eye of Zul
        42142, -- Bold Dragon's Eye
        42143, -- Delicate Dragon's Eye
        42144, -- Brilliant Dragon's Eye
        42145, -- Sparkling Dragon's Eye
        42149, -- Smooth Dragon's Eye
        42150, -- Quick Dragon's Eye
        42151, -- Subtle Dragon's Eye
        42152, -- Flashing Dragon's Eye
        42154, -- Precise Dragon's Eye
        42155, -- Stormy Dragon's Eye
        42156, -- Rigid Dragon's Eye
        42158, -- Mystic Dragon's Eye
        44066, -- Kharmaa's Grace
        45862, -- Bold Stormjewel
        45879, -- Delicate Stormjewel
        45880, -- Solid Stormjewel
        45881, -- Sparkling Stormjewel
        45882, -- Brilliant Stormjewel
        45883, -- Brilliant Stormjewel
        45987, -- Rigid Stormjewel
        49110  -- Nightmare Tear
    },
    [addon.CONS.GLYPHS_ID] = {
        40919, -- Glyph of the Orca
        41100, -- Glyph of the Luminous Charger
        42459, -- Glyph of Felguard
        42751, -- Glyph of Crittermorph
        43334, -- Glyph of the Ursol Chameleon
        43350, -- Glyph of Lesser Proportion
        43366, -- Glyph of Winged Vengeance
        43369, -- Glyph of Fire From the Heavens
        43373, -- Glyph of Shackle Undead
        43386, -- Glyph of the Spectral Wolf
        43394, -- Glyph of Soulwell
        43398, -- Glyph of Gushing Wound
        43400, -- Glyph of Mighty Victory
        43535, -- Glyph of the Geist
        43551, -- Glyph of Foul Menagerie
        44922, -- Glyph of Stars
        45768, -- Glyph of Disguise
        45775, -- Glyph of Deluge
        45789, -- Glyph of Crimson Banish
        49084  -- Glyph of Thunder Strike
    },
    [addon.CONS.KEYS_ID] = {
        43650  -- Rusty Prison Key
    },
    [addon.CONS.MISCELLANEOUS_ID] = {
        [addon.CONS.M_ARMOR_TOKENS_ID] = {
            40610, -- Chestguard of the Lost Conqueror
            40611, -- Chestguard of the Lost Protector
            40612, -- Chestguard of the Lost Vanquisher
            40613, -- Gloves of the Lost Conqueror
            40614, -- Gloves of the Lost Protector
            40615, -- Gloves of the Lost Vanquisher
            40616, -- Helm of the Lost Conqueror
            40617, -- Helm of the Lost Protector
            40618, -- Helm of the Lost Vanquisher
            40619, -- Leggings of the Lost Conqueror
            40620, -- Leggings of the Lost Protector
            40621, -- Leggings of the Lost Vanquisher
            40622, -- Spaulders of the Lost Conqueror
            40623, -- Spaulders of the Lost Protector
            40624, -- Spaulders of the Lost Vanquisher
            40625, -- Breastplate of the Lost Conqueror
            40626, -- Breastplate of the Lost Protector
            40627, -- Breastplate of the Lost Vanquisher
            40628, -- Gauntlets of the Lost Conqueror
            40629, -- Gauntlets of the Lost Protector
            40630, -- Gauntlets of the Lost Vanquisher
            40631, -- Crown of the Lost Conqueror
            40632, -- Crown of the Lost Protector
            40633, -- Crown of the Lost Vanquisher
            40634, -- Legplates of the Lost Conqueror
            40635, -- Legplates of the Lost Protector
            40636, -- Legplates of the Lost Vanquisher
            40637, -- Mantle of the Lost Conqueror
            40638, -- Mantle of the Lost Protector
            40639, -- Mantle of the Lost Vanquisher
            45632, -- Breastplate of the Wayward Conqueror
            45633, -- Breastplate of the Wayward Protector
            45634, -- Breastplate of the Wayward Vanquisher
            45635, -- Chestguard of the Wayward Conqueror
            45636, -- Chestguard of the Wayward Protector
            45637, -- Chestguard of the Wayward Vanquisher
            45638, -- Crown of the Wayward Conqueror
            45639, -- Crown of the Wayward Protector
            45640, -- Crown of the Wayward Vanquisher
            45641, -- Gauntlets of the Wayward Conqueror
            45642, -- Gauntlets of the Wayward Protector
            45643, -- Gauntlets of the Wayward Vanquisher
            45644, -- Gloves of the Wayward Conqueror
            45645, -- Gloves of the Wayward Protector
            45646, -- Gloves of the Wayward Vanquisher
            45647, -- Helm of the Wayward Conqueror
            45648, -- Helm of the Wayward Protector
            45649, -- Helm of the Wayward Vanquisher
            45650, -- Leggings of the Wayward Conqueror
            45651, -- Leggings of the Wayward Protector
            45652, -- Leggings of the Wayward Vanquisher
            45653, -- Legplates of the Wayward Conqueror
            45654, -- Legplates of the Wayward Protector
            45655, -- Legplates of the Wayward Vanquisher
            45656, -- Mantle of the Wayward Conqueror
            45657, -- Mantle of the Wayward Protector
            45658, -- Mantle of the Wayward Vanquisher
            45659, -- Spaulders of the Wayward Conqueror
            45660, -- Spaulders of the Wayward Protector
            45661, -- Spaulders of the Wayward Vanquisher
            47242, -- Trophy of the Crusade
            52025, -- Vanquisher's Mark of Sanctification
            52026, -- Protector's Mark of Sanctification
            52027, -- Conqueror's Mark of Sanctification
            52028, -- Vanquisher's Mark of Sanctification
            52029, -- Protector's Mark of Sanctification
            52030  -- Conqueror's Mark of Sanctification
        },
        [addon.CONS.M_HOLIDAY_ID] = {
            45072, -- Brightly Colored Egg
            49909, -- Box of Chocolates
            49927, -- Love Token
            50160, -- Lovely Dress Box
            50161, -- Dinner Suit Box
            54535, -- Keg-Shaped Treasure Chest
            54537  -- Heart-Shaped Box
        },
        [addon.CONS.M_MOUNTS_ID] = {
            44221, -- Loaned Gryphon Reins
            44229, -- Loaned Wind Rider Reins
            49288, -- Little Ivory Raptor Whistle
            49289, -- Little White Stallion Bridle
            46109, -- Sea Turtle
            44554, -- Flying Carpet
            49285, -- X-51 Nether-Rocket
            46099, -- Horn of the Black Wolf
            46100, -- White Kodo
            46308, -- Black Skeletal Horse
            47100, -- Reins of the Striped Dawnsaber
            49283, -- Reins of the Spectral Tiger
            41508, -- Mechano-Hog
            44413, -- Mekgineer's Chopper
            43951, -- Reins of the Bronze Drake
            43952, -- Reins of the Azure Drake
            43953, -- Reins of the Blue Drake
            43954, -- Reins of the Twilight Drake
            43955, -- Reins of the Red Drake
            43986, -- Reins of the Black Drake
            44151, -- Reins of the Blue Proto-Drake
            44160, -- Reins of the Red Proto-Drake
            44168, -- Reins of the Time-Lost Proto-Drake
            44175, -- Reins of the Plagued Proto-Drake
            44177, -- Reins of the Violet Proto-Drake
            44178, -- Reins of the Albino Drake
            44558, -- Magnificent Flying Carpet
            44689, -- Armored Snowy Gryphon
            44690, -- Armored Blue Wind Rider
            44707, -- Reins of the Green Proto-Drake
            44842, -- Red Dragonhawk
            44843, -- Blue Dragonhawk
            45693, -- Mimiron's Head
            45725, -- Argent Hippogryph
            45801, -- Reins of the Ironbound Proto-Drake
            45802, -- Reins of the Rusted Proto-Drake
            46813, -- Silver Covenant Hippogryph
            46814, -- Sunreaver Dragonhawk
            49286, -- X-51 Nether-Rocket X-TREME
            49636, -- Reins of the Onyxian Drake
            51954, -- Reins of the Bloodbathed Frostbrood Vanquisher
            51955, -- Reins of the Icebound Frostbrood Vanquisher
            54797, -- Frosty Flying Carpet
            40775, -- Winged Steed of the Ebon Blade
            54069, -- Blazing Hippogryph
            54860, -- X-53 Touring Rocket
            43956, -- Reins of the Black War Mammoth
            43958, -- Reins of the Ice Mammoth
            43959, -- Reins of the Grand Black War Mammoth
            43961, -- Reins of the Grand Ice Mammoth
            43962, -- Reins of the White Polar Bear
            44077, -- Reins of the Black War Mammoth
            44080, -- Reins of the Ice Mammoth
            44083, -- Reins of the Grand Black War Mammoth
            44086, -- Reins of the Grand Ice Mammoth
            44223, -- Reins of the Black War Bear
            44224, -- Reins of the Black War Bear
            44225, -- Reins of the Armored Brown Bear
            44226, -- Reins of the Armored Brown Bear
            44230, -- Reins of the Wooly Mammoth
            44231, -- Reins of the Wooly Mammoth
            44234, -- Reins of the Traveler's Tundra Mammoth
            44235, -- Reins of the Traveler's Tundra Mammoth
            45125, -- Stormwind Steed
            45586, -- Ironforge Ram
            45589, -- Gnomeregan Mechanostrider
            45590, -- Exodar Elekk
            45591, -- Darnassian Nightsaber
            45592, -- Thunder Bluff Kodo
            45593, -- Darkspear Raptor
            45595, -- Orgrimmar Wolf
            45596, -- Silvermoon Hawkstrider
            45597, -- Forsaken Warhorse
            46102, -- Whistle of the Venomhide Ravasaur
            46743, -- Swift Purple Raptor
            46744, -- Swift Moonsaber
            46745, -- Great Red Elekk
            46746, -- White Skeletal Warhorse
            46747, -- Turbostrider
            46748, -- Swift Violet Ram
            46749, -- Swift Burgundy Wolf
            46750, -- Great Golden Kodo
            46751, -- Swift Red Hawkstrider
            46752, -- Swift Gray Steed
            46815, -- Quel'dorei Steed
            46816, -- Sunreaver Hawkstrider
            47101, -- Ochre Skeletal Warhorse
            47179, -- Argent Charger
            47180, -- Argent Warhorse
            49282, -- Big Battle Bear
            49284, -- Reins of the Swift Spectral Tiger
            49290, -- Magic Rooster Egg
            54068, -- Wooly White Rhino
            50250, -- Big Love Rocket
            50818, -- Invincible's Reins
            52200, -- Reins of the Crimson Deathcharger
            54811, -- Celestial Steed
            49177  -- Tome of Cold Weather Flight
        },
        [addon.CONS.M_COMPANIONS_ID] = {
            39896, -- Tickbird Hatchling
            39898, -- Cobra Hatchling
            39899, -- White Tickbird Hatchling
            44721, -- Proto-Drake Whelp
            44822, -- Albino Snake
            46396, -- Wolvar Orphan Whistle
            46397, -- Oracle Orphan Whistle
            46398, -- Cat Carrier (Calico Cat)
            46767, -- Warbot Ignition Key
            46831, -- Macabre Marionette
            48112, -- Darting Hatchling
            48114, -- Deviate Hatchling
            48116, -- Gundrak Hatchling
            48118, -- Leaping Hatchling
            48120, -- Obsidian Hatchling
            48122, -- Ravasaur Hatchling
            48124, -- Razormaw Hatchling
            48126, -- Razzashi Hatchling
            50301, -- Landro's Pet Box
            46707, -- Pint-Sized Pink Pachyderm
            53641, -- Ice Chip
            34518, -- Golden Pig Coin
            34519, -- Silver Pig Coin
            39286, -- Frosty's Collar
            39973, -- Ghostly Skull
            40653, -- Reeking Pet Carrier
            43698, -- Giant Sewer Rat
            44723, -- Nurtured Penguin Egg
            44738, -- Kirin Tor Familiar
            44794, -- Spring Rabbit's Foot
            44810, -- Turkey Cage
            44841, -- Little Fawn's Salt Lick
            44965, -- Teldrassil Sproutling
            44970, -- Dun Morogh Cub
            44971, -- Tirisfal Batling
            44973, -- Durotar Scorpion
            44974, -- Elwynn Lamb
            44980, -- Mulgore Hatchling
            44982, -- Enchanted Broom
            44983, -- Strand Crawler
            44984, -- Ammen Vale Lashling
            44998, -- Argent Squire
            45002, -- Mechanopeep
            45022, -- Argent Gruntling
            45057, -- Wind-Up Train Wrecker
            45606, -- Sen'jin Fetish
            46544, -- Curious Wolvar Pup
            46545, -- Curious Oracle Hatchling
            46802, -- Heavy Murloc Egg
            46820, -- Shimmering Wyrmling
            46821, -- Shimmering Wyrmling
            49343, -- Spectral Tiger Cub
            49646, -- Core Hound Pup
            49662, -- Gryphon Hatchling
            49663, -- Wind Rider Cub
            49665, -- Pandaren Monk
            49693, -- Lil' Phylactery
            49912, -- Perky Pug
            50446, -- Toxic Wasteling
            54436, -- Blue Clockwork Rocket Bot
            54847, -- Lil' XT
            56806, -- Mini Thor
            49287  -- Tuskarr Kite
        },
        [addon.CONS.M_COMPANIONS_ID] = {
            [addon.CONS.MC_CONSUMABLE_ID] = {
                43626, -- Happy Pet Snack
                43352, -- Pet Grooming Kit
                37431, -- Fetch Ball
                37460, -- Rope Pet Leash
                44820  -- Red Ribbon Pet Leash
            }
        },
        [addon.CONS.M_TELEPORT_ID] = {
            [addon.CONS.MT_HEARTSTONE_ID] = {
                52251  -- Jaina's Locket
            },
            [addon.CONS.MT_ARMOR_ID] = {
                46874, -- Argent Crusader's Tabard
                50287  -- Boots of the Bay
            },
            [addon.CONS.MT_JEWELRY_ID] = {
                40586, -- Band of the Kirin Tor
                48956, -- Etched Ring of the Kirin Tor
                45690, -- Inscribed Ring of the Kirin Tor
                44935, -- Ring of the Kirin Tor
                51559, -- Runed Ring of the Kirin Tor
                51557, -- Runed Signet of the Kirin Tor
                45691, -- Inscribed Signet of the Kirin Tor
                40585, -- Signet of the Kirin Tor
                48957, -- Etched Signet of the Kirin Tor
                44934, -- Loop of the Kirin Tor
                51558, -- Runed Loop of the Kirin Tor
                45689, -- Inscribed Loop of the Kirin Tor 
                48955  -- Etched Loop of the Kirin Tor
            },
            [addon.CONS.MT_TOYS_ID] = {
                54452, -- Ethereal Portal
                43824, -- The Schools of Arcane Magic - Mastery
                48933  -- Wormhole Generator: Northrend
            }
        },
        [addon.CONS.M_OTHER_ID] = {
            46007, -- Bag of Fishing Treasures
            43575, -- Reinforced Junkbox
            33634, -- Orehammer's Precision Bombs
            34119, -- Black Conrad's Treasure
            34494, -- Paper Zeppelin
            34498, -- Paper Zeppelin Kit
            34871, -- Crafty's Sack
            35792, -- Mage Hunter Personal Effects
            37372, -- Harpoon
            43627, -- Thrall's Gold Coin
            43628, -- Lady Jaina Proudmoore's Gold Coin
            43629, -- Uther Lightbringer's Gold Coin
            43630, -- Tirion Fordring's Gold Coin
            43631, -- Teron's Gold Coin
            43632, -- Sylvanas Windrunner's Gold Coin
            43633, -- Prince Kael'thas Sunstrider's Gold Coin
            43634, -- Lady Katrana Prestor's Gold Coin
            43635, -- Kel'Thuzad's Gold Coin
            43636, -- Chromie's Gold Coin
            43637, -- Brann Bronzebeard's Gold Coin
            43638, -- Arugal's Gold Coin
            43639, -- Arthas' Gold Coin
            43640, -- Archimonde's Gold Coin
            43641, -- Anduin Wrynn's Gold Coin
            44663, -- Abandoned Adventurer's Satchel
            44680, -- Assorted Writings
            45724, -- Champion's Purse
            46114, -- Champion's Writ
            49918, -- Brazie's Guide to Getting Good with Gnomish Girls
            49922, -- Brazie's Dictionary of Devilish Draenei Damsels
            49923, -- Brazie's Document on Dwarven Dates in Dun Morogh
            49924, -- Brazie's Notes on Naughty Night Elves
            49925, -- Brazie's Handbook to Handling Human Hunnies
            54218, -- Landro's Gift Box
            43556, -- Patroller's Pack
            43622, -- Froststeel Lockbox
            37888, -- Arcane Disruptor
            52676, -- Cache of the Ley-Guardian
            45984, -- Unusual Compass
            45986, -- Tiny Titanium Lockbox
            46110, -- Alchemist's Cache
            54536, -- Satchel of Chilled Goods
            43624, -- Titanium Lockbox
            52005, -- Satchel of Helpful Goods
            52004, -- Satchel of Helpful Goods
            52003, -- Satchel of Helpful Goods
            52002, -- Satchel of Helpful Goods
            52001, -- Satchel of Helpful Goods
            52000, -- Satchel of Helpful Goods
            38578, -- The Flag of Ownership
            44430, -- Titanium Seal of Dalaran
            51999, -- Satchel of Helpful Goods
            52201, -- Muradin's Favor
            52253, -- Sylvanas' Music Box
            49703, -- Perpetual Purple Firework
            37254, -- Super Simian Sphere
            43346, -- Large Satchel of Spoils
            43347, -- Satchel of Spoils
            45875, -- Sack of Ulduar Spoils
            45878, -- Large Sack of Ulduar Spoils
            49294, -- Ashen Sack of Gems
            49704, -- Carved Ogre Idol
            49926, -- Brazie's Black Book of Secrets
            51316  -- Unsealed Chest
        }
    },
    [addon.CONS.QUEST_ID] = {
        43090, -- Fate Rune of Baneful Intent
        43094, -- Fate Rune of Nigh Invincibility
        43134, -- Fate Rune of Primal Energy
        43141, -- Fate Rune of Unsurpassed Vigor
        46004, -- Sealed Vial of Poison
        46005, -- Sealed Vial of Poison
        48679, -- Waterlogged Recipe
        48681, -- Waterlogged Recipe
        49667, -- Waterlogged Recipe
        50851, -- Pulsing Life Crystal
        51026, -- Crystalline Essence of Sindragosa
        51027, -- Crystalline Essence of Sindragosa
        44725, -- Everfrost Chip
        33120, -- Shoveltusk Meat
        38660, -- Unliving Choker
        38678, -- Unliving Choker
        43512, -- Ooze-Covered Fungus
        36744, -- Flesh-Bound Tome
        33330, -- Ingvar's Head
        34618, -- Succulent Orca Blubber
        34897, -- Beryl Shield Detonator
        34909, -- Salrand's Broken Key
        38673, -- Writhing Choker
        38680, -- Writhing Choker
        38699, -- Ensorcelled Choker
        38701, -- Bowels and Brains Bowl
        39165, -- Explosive Charges
        39238, -- Scepter of Command
        39319, -- Scepter of Domination
        39664, -- Scepter of Domination
        43099, -- Infused Mushroom Meatloaf
        43100, -- Infused Mushroom
        43101, -- Meatloaf Pan
        43128, -- Jug of Wine
        43136, -- Wine and Cheese Platter
        43137, -- Aged Dalaran Limburger
        43138, -- Half Full Dalaran Wine Glass
        43139, -- Empty Cheese Serving Platter
        43142, -- Empty Picnic Basket
        43143, -- Wild Mustard
        43144, -- Mustard Dog Basket
        43147, -- Stew Cookpot
        43148, -- Crystalsong Carrot
        43149, -- Vegetable Stew
        43269, -- Blood Jade Amulet
        43270, -- Glowing Ivory Figurine
        43272, -- Wicked Sun Brooch
        43274, -- Intricate Bone Figurine
        43275, -- Bright Armor Relic
        43276, -- Shifting Sun Curio
        35648, -- Scintillating Fragment
        33221, -- Plaguehound Cage
        33238, -- Crow Meat
        33613, -- Abomination Assembly Kit
        34777, -- Ith'rix's Hardened Carapace
        44143, -- Ace of Demons
        44154, -- Two of Demons
        44155, -- Three of Demons
        44156, -- Four of Demons
        44157, -- Five of Demons
        44144, -- Two of Mages
        44145, -- Three of Mages
        44146, -- Four of Mages
        44147, -- Five of Mages
        44165, -- Ace of Mages
        37145, -- Ace of Swords
        37147, -- Two of Swords
        37159, -- Three of Swords
        37160, -- Four of Swords
        37140, -- Ace of Rogues
        37143, -- Two of Rogues
        37156, -- Three of Rogues
        29205, -- Inquisitor's Crest - Top Half
        29206, -- Inquisitor's Crest - Bottom Half
        33084, -- Darkclaw Guano
        33098, -- Petrov's Cluster Bombs
        33109, -- Steel Gate Artifact
        33111, -- Pristine Mistsaber Catgut
        33119, -- Malister's Frost Wand
        33123, -- Westguard Cannonball
        33129, -- Feknut's Firecrackers
        33164, -- Ever-Burning Torches
        33187, -- Whisper Gulch Gem
        33188, -- Whisper Gulch Ore Fragment
        33190, -- Steelring's Foolproof Dynamite
        33278, -- Burning Torch
        33282, -- Wyrmcaller's Horn
        33284, -- Gjalerbron Cage Key
        33289, -- Gjalerbron Attack Plans
        33290, -- Large Gjalerbron Cage Key
        33308, -- Dragonflayer Cage Key
        33310, -- The Sergeant's Machete
        33311, -- Westguard Command Insignia
        33314, -- Vrykul Scroll of Ascension
        33321, -- Sergeant's Torch
        33323, -- Sergeant's Flare
        33335, -- Cannoneer's Smoke Flare
        33336, -- Sergeant's Spyglass
        33337, -- Giant Toxin Gland
        33339, -- Vrykul Scroll of Ascension
        33340, -- Winterhoof Emblem
        33341, -- Brave's Spyglass
        33342, -- The Brave's Machete
        33343, -- Brave's Torch
        33344, -- Brave's Flare
        33345, -- Vrykul Scroll of Ascension
        33346, -- Vrykul Scroll of Ascension
        33347, -- Gjalerbron Attack Plans
        33348, -- Spotted Hippogryph Down
        33351, -- Undamaged Ram Horn
        33352, -- Tough Ram Meat
        33355, -- Wyrmskull Tablet
        33387, -- Glorenfeld's Package
        33411, -- Dragonskin Map
        33418, -- Tillinghast's Plague Canister
        33420, -- Plagued Proto-Whelp Specimen
        33441, -- Tillinghast's Plagued Meat
        33450, -- Carved Horn
        33472, -- Gorth's Torch
        33477, -- Giant Yeti Meal
        33485, -- Sacred Artifact
        33486, -- Plaguehound Leash
        33487, -- Trapped Prey
        33488, -- Dragonflayer Battle Plans
        33541, -- Dwarven Keg
        33545, -- Ancient Cipher
        33554, -- Grick's Bonesaw
        33558, -- Deranged Explorer Brain
        33563, -- Forsaken Banner
        33581, -- Vrykul Insult
        33605, -- Icy Core
        33606, -- Lurielle's Pendant
        33607, -- Enchanted Ice Core
        33611, -- Shoveltusk Ligament
        33612, -- Fresh Pound of Flesh
        33618, -- Worg Disguise
        33620, -- Apothecary's Package
        33627, -- Peppy's Special Mix
        33628, -- Northern Barbfish
        33635, -- Reagent Pouch
        33637, -- Incense Burner
        33774, -- Incense Burner
        33778, -- Book of Runes - Chapter 1
        33779, -- Book of Runes - Chapter 2
        33780, -- Book of Runes - Chapter 3
        33781, -- The Book of Runes
        33794, -- Iron Rune Carving Tools
        33796, -- Rune of Command
        33806, -- Runeseeking Pick
        33819, -- Rune Sample
        33960, -- Scourging Crystal Controller
        33961, -- Scourge Device
        33962, -- Scourge Device
        34013, -- Fresh Barbfish Bait
        34026, -- Feathered Charm
        34027, -- Eyes of the Eagle
        34031, -- Harpoon Operation Manual
        34032, -- Harpoon Control Mechanism
        34035, -- Rotgill's Trident
        34040, -- Saga of the Twins
        34041, -- Saga of the Winter Curse
        34042, -- Saga of the Val'kyr
        34043, -- Ancient Vrykul Bone
        34051, -- Alliance Banner
        34069, -- Amani Vase
        34070, -- Eagle Figurine
        34078, -- Icehammer's Harpoon Controller
        34081, -- Valgarde Supply Crate
        34082, -- Diving Helm
        34083, -- Awakening Rod
        34084, -- Bear Musk
        34088, -- McSorf's Bundle
        34090, -- Mezhen's Writings
        34091, -- Mezhen's Writings
        34101, -- Chimaera Horn
        34102, -- Fjord Grub
        34111, -- Trained Rock Falcon
        34112, -- Fjord Turkey
        34115, -- "Silvermoon" Harry's Debt
        34116, -- Jack Adams' Debt
        34118, -- Black Conrad's Treasure
        34120, -- Fjord Hawk
        34121, -- Trained Rock Falcon
        34122, -- Big Roy's Blubber
        34123, -- Fjord Hawk Egg
        34124, -- Trained Rock Falcon
        34127, -- Tasty Reef Fish
        34128, -- Jonah Sterling's Spyglass
        34131, -- Building Tools
        34132, -- Donny's Letter
        34133, -- Large Barrel
        34134, -- Industrial Strength Rope
        34135, -- Dark Iron Ingots
        34136, -- Pristine Shoveltusk Hide
        34137, -- Steel Ribbing
        34222, -- Fengir's Clue
        34223, -- Rodin's Clue
        34224, -- Isuldof's Clue
        34225, -- Windan's Clue
        34226, -- Orfus' Bundle
        34235, -- Sin'dorei Scrying Crystal
        34236, -- The Staff of Storm's Fury
        34237, -- The Frozen Heart of Isuldof
        34238, -- The Shield of the Aesirites
        34239, -- The Ancient Armor of the Kvaldir
        34387, -- Barrel of Blasting Powder
        34468, -- Sorlof's Booty
        34597, -- Winterfin Clam
        34598, -- The King's Empty Conch
        34600, -- Urmgrgl's Key
        34617, -- Glrggl's Head
        34619, -- King Mrgl-Mrgl's Spare Suit
        34620, -- King Mrgl-Mrgl's Spare Suit
        34621, -- Claw of Claximus
        34623, -- The King's Filled Conch
        34624, -- Bundle of Vrykul Artifacts
        34669, -- Arcanometer
        34688, -- Beryl Prison Key
        34690, -- Warsong Banner
        34691, -- Arcane Binder
        34692, -- Ragefist's Torch
        34695, -- Enlistment Card
        34709, -- Warsong Munitions
        34710, -- Seaforium Depth Charge Bundle
        34711, -- Core of Malice
        34713, -- Tuskarr Ritual Object
        34714, -- Kul Tiras Wine
        34715, -- Tuskarr Ritual Object
        34719, -- Luther's Journal
        34720, -- Barthus' Note
        34772, -- Gnomish Grenade
        34773, -- Farseer Grimwalker's Remains
        34774, -- Scourged Earth
        34775, -- Scourged Mammoth Pelt
        34778, -- Imperean's Primal
        34779, -- Imperean's Primal
        34781, -- Kaganishu's Fetish
        34785, -- Pneumatic Tank Transjigamarig
        34786, -- Super Strong Metal Plate
        34787, -- Crafty's Stuff
        34800, -- Tempest Mote
        34801, -- Crafty's Shopping List
        34802, -- Crafty's Tools
        34804, -- A Handful of Rocknar's Grit
        34806, -- Sage Aeire's Totem
        34811, -- Neural Needler
        34812, -- Crafty's Ultra-Advanced Proto-Typical Shortening Blaster
        34813, -- Horn of the Ancient Mariner
        34814, -- Tuskarr Relic
        34815, -- Vial of Fresh Blood
        34830, -- Tuskarr Torch
        34842, -- Warsong Outfit
        34844, -- Horn of the Ancient Mariner
        34869, -- Warsong Banner
        34870, -- Warsong Orc Disguise
        34908, -- Scourge Cage Key
        34913, -- Highmesa's Cleansing Seeds
        34915, -- Bixie's Inhibiting Powder
        34920, -- Map of the Geyser Fields
        34948, -- Salrand's Key
        34954, -- Torp's Kodo Snaffle
        34956, -- Elemental Heart
        34957, -- Engine-Core Crystal
        34958, -- Magical Gyroscope
        34959, -- Piloting Scourgestone
        34960, -- The Legend of the Horn
        34961, -- Burblegobble's Key
        34962, -- Gurgleboggle's Key
        34963, -- Lower Horn Half
        34964, -- Upper Horn Half
        34968, -- The Horn of Elemental Fury
        34971, -- Warsong Flare Gun
        34972, -- Fizzcrank Spare Parts
        34973, -- Re-Cursive Transmatter Injection
        34974, -- Bloodspore Carpel
        34975, -- Portable Oil Collector
        34976, -- Bloodspore Moth Pollen
        34977, -- Thick Mammoth Hide
        34978, -- Pollinated Bloodspore Flower
        34979, -- Pouch of Crushed Bloodspore
        34980, -- Head of Gammothra
        34981, -- Shake-n-Quake 5000 Control Unit
        34982, -- Massive Glowing Egg
        34983, -- Gorloc Spice Pouch
        34984, -- The Ultrasonic Screwdriver
        35116, -- The Ultrasonic Screwdriver
        35119, -- Hawthorn's Anti-Venom
        35121, -- Wolf Bait
        35122, -- Cultist Communique
        35123, -- Microfilm
        35125, -- Oculus of the Exorcist
        35126, -- Fizzcrank Pilot's Insignia
        35127, -- Pile of Fake Furs
        35188, -- Nesingwary Lackey Ear
        35222, -- Shipment of Animal Parts
        35224, -- Emergency Torch
        35228, -- D.E.H.T.A. Trap Smasher
        35234, -- Kaw's War Halberd
        35272, -- Jenny's Whistle
        35276, -- Gnomish Emergency Toolkit
        35278, -- Reinforced Net
        35281, -- Windsoul Totem
        35288, -- Uncured Caribou Hide
        35289, -- Steam Cured Hide
        35293, -- Cenarion Horn
        35352, -- Sage's Lightning Rod
        35353, -- High Priest Naferset's Scroll
        35354, -- High Priest Talet-Kha's Scroll
        35355, -- High Priest Andorath's Scroll
        35401, -- The Greatmother's Soulcatcher
        35479, -- Interdimensional Refabricator
        35481, -- Fields, Factories and Workshops
        35483, -- Glacial Splinter
        35484, -- Magic-Bound Splinter
        35486, -- Mechazod's Head
        35490, -- Arcane Splinter
        35491, -- Wendy's Torch
        35492, -- Frostberry
        35493, -- Nexus Mana Essence
        35506, -- Raelorasz's Spear
        35586, -- Frozen Axe
        35628, -- Azure Codex
        35629, -- Shimmering Rune
        35668, -- Prison Casing
        35669, -- Energy Core
        35671, -- Augmented Arcane Prison
        35685, -- Crystallized Mana Shard
        35686, -- Stolen Moa'ki Goods
        35687, -- Tanathal's Phylactery
        35688, -- Blood of Loguhn
        35690, -- Arcane Power Focus
        35692, -- Snowfall Glade Pup
        35701, -- Issliruk's Totem
        35705, -- Cart Release Key
        35706, -- Crate of Farshire Ore
        35709, -- Saragosa's Corpse
        35711, -- Kaskala Supplies
        35718, -- Raelorasz' Flare
        35726, -- Horde Armaments
        35734, -- Boulder
        35736, -- Bounty Hunter's Cage
        35737, -- Missing Journal Page
        35738, -- Brann Bronzebeard's Journal
        35739, -- Incomplete Journal
        35746, -- Runic Keystone
        35747, -- Runic Keystone Fragment
        35774, -- Trident of Naz'jan
        35782, -- Shimmering Snowcap
        35783, -- Moonrest Gardens Plans
        35784, -- Blood Oath of the Horde
        35795, -- Waterweed Frond
        35796, -- Howlin' Good Moonshine
        35797, -- Drakuru's Elixir
        35798, -- Ice Serpent Eye
        35799, -- Frozen Mojo
        35800, -- Wind Trader Mu'fah's Remains
        35801, -- The Scales of Goramosh
        35802, -- Tua'kea Crab Trap
        35803, -- Scalawag Frog
        35806, -- Eye of the Prophets
        35813, -- Shiny Knife
        35819, -- Thor Modan Stability Profile
        35831, -- The Flesh of "Two Huge Pincers"
        35836, -- Zim'bo's Mojo
        35837, -- Portable Seismograph
        35838, -- Tu'u'gwar's Bait
        35850, -- Trident of Naz'jan
        35907, -- Toalu'u's Spiritual Incense
        35908, -- Mack's Dark Grog
        35941, -- Letter from Saurfang
        35943, -- Jeremiah's Tools
        35944, -- Lurid's Bones
        36725, -- Black Blood of Yogg-Saron Sample
        36726, -- Ironbender's Mining Pick
        36727, -- Composite Ore
        36728, -- Ice Shard Cluster
        36729, -- Thin Animal Hide
        36730, -- Splintered Bone Chunk
        36731, -- Seared Jormungar Meat
        36732, -- Potent Explosive Charges
        36733, -- Coldwind Lumber
        36734, -- Xink's Shredder Control Device
        36735, -- Kilix's Battle Plan
        36736, -- Scourge Armament
        36738, -- Warsong Battle Standard
        36739, -- Heart of the Ancients
        36740, -- Spiritsbreath
        36741, -- Head of High Cultist Zangus
        36742, -- Goramosh's Strange Device
        36743, -- Desperate Mojo
        36746, -- Goramosh's Strange Device
        36747, -- Surge Needle Teleporter
        36751, -- Ley Line Focus Control Ring
        36752, -- Anok'ra's Key Fragment
        36753, -- Tivax's Key Fragment
        36754, -- Sinok's Key Fragment
        36756, -- Captain Malin's Letter
        36757, -- Drakkari Tablets
        36758, -- Sacred Mojo
        36759, -- Fragment of Anub'et'kan's Husk
        36760, -- Anub'ar Prison Key
        36764, -- Shard of the Earth
        36765, -- Sample of Rockflesh
        36768, -- Vial of Corrosive Spit
        36769, -- Zort's Protective Elixir
        36772, -- Captured Jormungar Spawn
        36774, -- Valnok's Flare Gun
        36775, -- Zort's Scraper
        36776, -- Island Shoveltusk Meat
        36777, -- Horn of Kamagua
        36779, -- Ley Line Focus Control Amulet
        36780, -- Lieutenant Ta'zinni's Letter
        36786, -- Bark of the Walkers
        36787, -- Shard of Gavrock
        36793, -- Sarathstra's Frozen Heart
        36796, -- Gavrock's Runebreaker
        36800, -- Rot Resistant Organ
        36803, -- Ruby Lilac
        36807, -- Sintar's Vaccine
        36815, -- Ley Line Focus Control Talisman
        36818, -- Pack of Vaccine
        36819, -- Fibrous Worg Meat
        36820, -- Flesh-Bound Tome
        36825, -- Drakkari Canopic Jar
        36826, -- Drakil'jin Mallet
        36827, -- Blood Gem
        36828, -- Filled Blood Gem
        36832, -- Letter of Introduction to Wyrmrest Temple
        36833, -- Letter of Introduction to Wyrmrest Temple
        36834, -- Charged Drakil'jin Mallet
        36835, -- Unholy Gem
        36836, -- Filled Unholy Gem
        36846, -- Filled Frost Gem
        36847, -- Frost Gem
        36848, -- War Golem Blueprint
        36849, -- Golem Blueprint Section 1
        36850, -- Golem Blueprint Section 2
        36851, -- Golem Blueprint Section 3
        36852, -- War Golem Part
        36853, -- Grom'thar's Head
        36854, -- Emblazoned Battle Horn
        36855, -- Emblazoned Battle Horn
        36856, -- Emblazoned Battle Horn
        36857, -- Durar's Power Cell
        36858, -- Kathorn's Power Cell
        36859, -- Snow of Eternal Slumber
        36861, -- Translated Flesh-Bound Tome
        36864, -- Emblazoned Battle Horn
        36865, -- Golem Control Unit
        36868, -- Drakkari Spirit Particles
        36870, -- Sacred Drakkari Offering
        36873, -- Drakkari Spirit Dust
        36875, -- Shovelhorn Steak
        36935, -- Raegar's Explosives
        36936, -- Golem Control Unit
        36940, -- Mikhail's Journal
        36956, -- Liquid Fire of Elune
        36957, -- Functional Cultist Suit
        36958, -- The Favor of Zangus
        37003, -- Alliance Missive
        37006, -- Ley Line Attunement Crystal
        37010, -- Gray Worg Hide
        37013, -- Dun Argol Cage Key
        37020, -- Grizzly Hide
        37027, -- Blight Specimen
        37035, -- Overseer's Uniform
        37045, -- Kilian's Camera
        37063, -- Infused Drakkari Offering
        37071, -- Overseer Disguise Kit
        37085, -- Haze Leaf
        37087, -- Sweetroot
        37104, -- Mature Stag Horn
        37121, -- Ectoplasmic Residue
        37124, -- Emerald Dragon Tear
        37125, -- Rokar's Camera
        37129, -- Flask of Blight
        37136, -- Scarlet Onslaught Armor
        37137, -- Scarlet Onslaught Weapon
        37185, -- Succulent Venison
        37187, -- Container of Rats
        37199, -- Slime Sample
        37200, -- Grizzly Flank
        37202, -- Onslaught Riding Crop
        37229, -- Flame-Imbued Talisman
        37233, -- The Forsaken Blight
        37246, -- Blackroot Stalk
        37247, -- Anderhol's Slider Cider
        37248, -- Siegesmith Bomb
        37250, -- Partially Processed Amberseeds
        37251, -- Crazed Furbolg Blood
        37259, -- Siegesmith Bombs
        37267, -- Scarlet Onslaught Daily Orders: Barracks
        37268, -- Scarlet Onslaught Daily Orders: Abbey
        37269, -- Scarlet Onslaught Daily Orders: Beach
        37287, -- Wintergarde Gryphon Whistle
        37299, -- Scarlet Onslaught Daily Orders
        37300, -- Levine Family Termites
        37302, -- Vordrassil's Seed
        37303, -- Vordrassil's Ashes
        37304, -- Apothecary's Burning Water
        37305, -- Captain Shely's Rutters
        37306, -- Verdant Torch
        37307, -- Purified Ashes of Vordrassil
        37314, -- High Executor's Branding Iron
        37350, -- Bishop Street's Prayer Book
        37358, -- Quarterflash's Mining Pick
        37359, -- Strange Ore
        37381, -- Banshee's Magic Mirror
        37411, -- Wintergarde Miner's Card
        37412, -- Grooved Cog
        37413, -- Notched Sprocket
        37416, -- High Tension Spring
        37432, -- Torturer's Rod
        37438, -- Rod of Compulsion
        37445, -- Destructive Wards
        37459, -- Quarterflash's Homing Bot
        37465, -- Wintergarde Mine Bomb
        37500, -- Key to Refurbished Shredder
        37501, -- Northern Salmon
        37502, -- Quarterflash's Package
        37538, -- Scrying Orb
        37539, -- Neltharion's Flame
        37540, -- The Diary of High General Abbendis
        37542, -- Fishing Net
        37565, -- The Head of the High General
        37568, -- Renewing Tourniquet
        37569, -- Murkweed
        37570, -- Murkweed Elixir
        37572, -- Cedar Chest
        37576, -- Renewing Bandage
        37577, -- Orik's Crystalline Orb
        37580, -- Forgotten Treasure
        37581, -- Bloodied Scalping Knife
        37601, -- Flesh-Bound Tome
        37607, -- Flesh-Bound Tome
        37621, -- Smoke Bomb
        37662, -- Gossamer Dust
        37665, -- Tranquilizer Dart
        37707, -- Wild Carrot
        37716, -- Flashbang Grenade
        37727, -- Ruby Acorn
        37736, -- "Brew of the Month" Club Membership Form
        37737, -- "Brew of the Month" Club Membership Form
        37741, -- Letter of Amnesty
        37830, -- Mikhail's Journal
        37831, -- Mikhail's Journal
        37833, -- Ruby Brooch
        37879, -- Wintergarde Munitions
        37880, -- The Plume of Alystros
        37881, -- Skytalon Molts
        37882, -- Lasher Seed
        37887, -- Seeds of Nature's Wrath
        37910, -- Page 4 of Plunderbeard's Journal
        37911, -- Page 5 of Plunderbeard's Journal
        37912, -- Page 6 of Plunderbeard's Journal
        37913, -- Page 7 of Plunderbeard's Journal
        37920, -- Thel'zan's Phylactery
        37922, -- Pack of Nozzlerust Explosives
        37923, -- Hourglass of Eternity
        37930, -- Onslaught Map
        37931, -- The Path of Redemption
        37932, -- Miner's Lantern
        37933, -- Zelig's Scrying Orb
        38083, -- Dull Carving Knife
        38098, -- Dragonflayer Patriarch's Blood
        38144, -- Harkor's Ingredients
        38149, -- Scourged Troll Mummy
        38281, -- Direbrew's Dire Brew
        38302, -- Ruby Beacon of the Dragon Queen
        38303, -- Enduring Mojo
        38305, -- Scythe of Antiok
        38319, -- New Deployment Orders
        38321, -- Strange Mojo
        38323, -- Water Elemental Link
        38324, -- Tether to the Plane of Water
        38325, -- Precious Elemental Fluids
        38326, -- Muddlecap Fungus
        38330, -- Crusader's Bandage
        38332, -- Modified Mojo
        38333, -- Drakkari Medallion
        38334, -- Flying Machine Engine
        38335, -- Ancient Ectoplasm
        38336, -- Crystallized Hogsnot
        38337, -- Speckled Guano
        38338, -- Knotroot
        38339, -- Withered Batwing
        38340, -- Amberseed
        38341, -- Pickled Eagle Egg
        38342, -- Trollbane
        38343, -- Prismatic Mojo
        38344, -- Shrunken Dragon's Claw
        38345, -- Frozen Spider Ichor
        38346, -- Chilled Serpent Mucus
        38349, -- Venture Co. Spare Parts
        38369, -- Wasp's Wings
        38370, -- Raptor Claw
        38379, -- Crushed Basilisk Crystals
        38380, -- Zul'Drak Rat
        38381, -- Seasoned Slider Cider
        38382, -- Basilisk Crystals
        38384, -- Pulverized Gargoyle Teeth
        38386, -- Muddy Mire Maggot
        38393, -- Spiky Spider Egg
        38396, -- Hairy Herring Head
        38397, -- Putrid Pirate Perspiration
        38398, -- Icecrown Bottled Water
        38467, -- Softknuckle Poker
        38473, -- Claw of Serfex
        38477, -- Stinger of the Sapphire Queen
        38483, -- Captured Chicken
        38504, -- Skyreach Crystal Cluster
        38505, -- Intact Cobra Fang
        38510, -- Skyreach Crystal Clusters
        38512, -- Zepik's Hunting Horn
        38514, -- Intact Skimmer Spinneret
        38515, -- Tangled Skein Thrower
        38519, -- Soo-Rahm's Incense
        38521, -- Head of the Corrupter
        38522, -- Farunn's Horn
        38523, -- Shango's Pelt
        38544, -- Argent Crusade Banner
        38551, -- Drakkari Offerings
        38552, -- Mature Water-Poppy
        38553, -- Sandfern
        38556, -- Incinerating Oil
        38559, -- Bushwhacker's Jaw
        38560, -- Plague Sprayer Parts
        38562, -- Chunk of Saronite
        38563, -- Dead Thornwood
        38564, -- Sandfern Disguise
        38566, -- Steel Spade
        38573, -- RJR Rifle
        38574, -- High Impact Grenade
        38575, -- Shiny Treasures
        38600, -- Primordial Hatchling
        38601, -- Mistwhisper Treasure
        38607, -- Battle-Worn Sword
        38610, -- Fresh Spider Ichor
        38619, -- Goregek's Shackles
        38620, -- Unblemished Bat Wing
        38621, -- Dajik's Worn Chalk
        38622, -- Lafoo's Bug Bag
        38623, -- Jaloot's Favorite Crystal
        38624, -- Moodle's Stress Ball
        38627, -- Mammoth Harness
        38631, -- Runebladed Sword
        38637, -- Treasure of Kutube'sa
        38638, -- Treasure of Chulo the Mad
        38639, -- Treasure of Gawanil
        38642, -- Golden Engagement Ring
        38653, -- Banana Bunch
        38654, -- Corvus' Report
        38655, -- Papaya
        38656, -- Orange
        38659, -- Stefan's Steel Toed Boot
        38676, -- Whisker of Har'koa
        38677, -- Har'koan Relic
        38681, -- Essence of the Frozen Earth
        38684, -- Freya's Horn
        38686, -- Putrid Abomination Guts
        38687, -- Gooey Ghoul Drool
        38688, -- Thunderbrew's Jungle Punch
        38689, -- Chicken Net
        38695, -- Arctic Bear God Mojo
        38696, -- Tormentor's Incense
        38697, -- Jungle Punch Sample
        38700, -- Rhunokian Artifact
        38703, -- Pitch's Remains
        38705, -- Roc Egg
        38708, -- Omega Rune
        38709, -- Omega Rune
        38731, -- Ahunae's Knife
        39041, -- Bat Net
        39063, -- Lifeblood Shard
        39150, -- Sacred Adornment
        39154, -- Diluting Additive
        39156, -- Underworld Power Fragment
        39157, -- Scepter of Suggestion
        39158, -- Quetz'lun's Hexxing Stick
        39159, -- Harvested Blight Crystal
        39160, -- Saronite Arrow
        39161, -- Twisted Roc Talon
        39162, -- Broodmother Slivina's Skull
        39164, -- Sample Container
        39166, -- Underworld Power Fragments
        39167, -- Blood of Mam'toth
        39187, -- Quetz'lun's Ritual
        39206, -- Scepter of Empowerment
        39227, -- Huge Stone Key
        39253, -- Gift of the Harvester
        39264, -- Vic's Keys
        39265, -- Oracle Blood
        39266, -- Tainted Crystal
        39268, -- Medallion of Mam'toth
        39269, -- Prince Valanar's Report
        39301, -- Zol'Maz Stronghold Cache
        39305, -- Tiki Hex Remover
        39313, -- Yara's Sword
        39314, -- Tiki Dervish Ceremony
        39315, -- Drek'Maz's Tiki
        39316, -- Tiri's Magical Incantation
        39318, -- Key of Warlord Zol'Maz
        39324, -- Empty Cauldron
        39326, -- Iron Chain
        39328, -- Crusader Skull
        39329, -- Pile of Crusader Skulls
        39362, -- New Avalon Registry
        39418, -- Ornately Jeweled Box
        39434, -- Key of Warlord Zol'Maz
        39504, -- New Avalon Patrol Schedule
        39506, -- Depleted Element 115
        39510, -- Valroth's Head
        39540, -- Unblemished Heart of the Guardian
        39541, -- Gusty Essence of the Warden
        39566, -- Prophet of Akali Convocation
        39571, -- Drums of the Tempest
        39572, -- Chime of Cleansing
        39573, -- Matriarch's Heartblood
        39574, -- Rejek's Vial
        39575, -- Suntouched Heartblood
        39576, -- Suntouched Water
        39577, -- Rejek's Blade
        39598, -- Didgeridoo of Contemplation
        39599, -- Horn of Fecundity
        39614, -- True Power of the Tempest
        39615, -- Crusader Parachute
        39616, -- Essence of the Monsoon
        39643, -- Essence of the Storm
        39645, -- Makeshift Cover
        39646, -- Scarlet Courier's Belongings
        39647, -- Scarlet Courier's Message
        39651, -- Venture Co. Explosives
        39654, -- The Path of Redemption
        39665, -- Bundle of Hides
        39667, -- Stormwatcher's Head
        39668, -- Abomination Guts
        39669, -- Ghoul Drool
        39670, -- Blight Crystal
        39689, -- Crystal of the Frozen Grip
        39693, -- Crystal of Unstable Energy
        39694, -- Crystal of the Violent Storm
        39695, -- Ensnaring Trap
        39696, -- Volatile Trap
        39697, -- Spike Bomb
        39700, -- Horn of the Frostbrood
        39713, -- Ebon Hold Gift Voucher
        39737, -- Secret Strength of the Frenzyheart
        39739, -- Wolvar Berries
        39740, -- Kirin Tor Signet
        39747, -- Dormant Polished Crystal
        39748, -- Energized Polished Crystal
        40066, -- Ancient Ectoplasm
        40364, -- Rainspeaker Peace Offering
        40394, -- Longneck Grazer Steak
        40397, -- Lifeblood Gem
        40425, -- Drakuru's Skull
        40551, -- Gore Bladder
        40587, -- Darkmender's Tincture
        40600, -- Bone Gryphon
        40603, -- Charred Wreckage
        40640, -- Onslaught Intel Documents
        40641, -- Cold Iron Key
        40642, -- Sparksocket's Tools
        40645, -- Dried Gnoll Rations
        40652, -- Scarlet Onslaught Trunk Key
        40666, -- Note from the Grand Admiral
        40676, -- Improved Land Mines
        40686, -- U.D.E.D.
        40690, -- Runes of the Yrkvinn
        40726, -- K3 Equipment
        40728, -- Hearty Mammoth Meat
        40730, -- Arete's Gate
        40731, -- Transporter Power Cell
        40732, -- Acherus Shackle Key
        40744, -- Impure Saronite Ore
        40917, -- Lord-Commander's Nullifier
        40944, -- Icetip Venom Sac
        40946, -- Anuniaq's Net
        40947, -- Burlap-Wrapped Note
        40969, -- Improved Land Mines
        40970, -- Onslaught Gryphon Reins
        40971, -- Brann's Communicator
        41058, -- Hyldnir Harpoon
        41115, -- Ragemane's Flipper
        41130, -- Inventor's Disk Fragment
        41131, -- Rageclaw Fire Extinguisher
        41132, -- The Inventor's Disk
        41161, -- Drakuru "Lock Opener"
        41179, -- The Inventor's Disk
        41197, -- The Inventor's Disk
        41258, -- Norgannon's Shell
        41260, -- Norgannon's Core
        41262, -- Orders From Drakuru
        41265, -- Eyesore Blaster
        41267, -- SCRAP-E Access Card
        41336, -- Medical Supply Crate
        41340, -- Fresh Ice Rhino Meat
        41341, -- Stormcrest Eagle Egg
        41359, -- Cultist Rod
        41361, -- Abomination Hook
        41362, -- Geist Rope
        41363, -- Scourge Essence
        41366, -- Sovereign Rod
        41372, -- Challenge Flag
        41390, -- Stefan's Horn
        41393, -- Frostgrip's Signet Ring
        41399, -- Scourge Scrap Metal
        41424, -- Icemane Yeti Hide
        41428, -- Mildred's Key
        41430, -- Frosthound's Collar
        41431, -- Hardpacked Explosive Bundle
        41503, -- Diatomaceous Earth
        41504, -- Banshee Essence
        41505, -- Thorim's Charm of Earth
        41506, -- Granite Boulder
        41507, -- Jumbo Seaforium Charge
        41514, -- Voice of the Wind
        41556, -- Slag Covered Metal
        41557, -- Refined Gleaming Ore
        41558, -- Furious Spark
        41585, -- Slag Covered Metal
        41612, -- Vial of Frost Oil
        41614, -- Enchanted Earth
        41615, -- Earthen Mining Pick
        41776, -- Shadow Vault Decree
        41843, -- Key to Vaelen's Chains
        41988, -- Telluric Poultice
        41989, -- Vrykul Amulet
        42104, -- Northern Ivory
        42105, -- Iron Dwarf Brooch
        42106, -- Proto Dragon Bone
        42107, -- Elemental Armor Scrap
        42108, -- Scourge Curio
        42109, -- Dark Ore Sample
        42159, -- Storm Hammer
        42160, -- Incendiary Harpoons
        42162, -- Horn Fragment
        42163, -- Horn Fragments
        42164, -- Hodir's Horn
        42203, -- Dark Armor Plate
        42204, -- Dark Armor Sample
        42246, -- Essence of Ice
        42252, -- Frozen Iron Scrap
        42394, -- Dark Armor Plate
        42419, -- Bouldercrag's War Horn
        42422, -- Jotunheim Cage Key
        42423, -- Stormforged Eye
        42424, -- Diamond Tipped Pick
        42441, -- Bouldercrag's Bomb
        42442, -- Tablets of Pronouncement
        42475, -- Colossus Attack Specs
        42476, -- Colossus Defense Specs
        42479, -- Ethereal Worg's Fang
        42480, -- Ebon Blade Banner
        42481, -- Reins of the Warbear Matriarch
        42499, -- Reins of the Warbear Matriarch
        42510, -- Worg Fur
        42541, -- Everfrost Shard
        42542, -- Stoic Mammoth Hide
        42624, -- Battered Storm Hammer
        42640, -- Viscous Oil
        42679, -- Creteus' Mobile Databank
        42700, -- Reforged Armor of the Stormlord
        42732, -- Everfrost Razor
        42733, -- Icemaw Bear Flank
        42769, -- Spear of Hodir
        42770, -- Forgotten Depths Venom Sac
        42772, -- Dr. Terrible's "Building a Better Flesh Giant"
        42774, -- Arngrim's Tooth
        42780, -- Relic of Ulduar
        42781, -- The Chieftain's Totem
        42782, -- Stormhoof's Spear
        42783, -- Stormhoof's Mail
        42784, -- Small Proto-Drake Egg
        42797, -- Stolen Proto-Dragon Eggs
        42837, -- Disciplining Rod
        42838, -- Runed Harness
        42839, -- The Lorehammer
        42840, -- Horn of the Peaks
        42894, -- Horn of Elemental Fury
        42918, -- The Lorehammer
        42926, -- Cave Mushroom
        42927, -- Toxin Gland
        42928, -- Bethod's Sword
        43006, -- Emerald Acorn
        43059, -- Drakuru's Last Wish
        43084, -- Dahlia's Tears
        43089, -- Vrykul Bones
        43095, -- Berinand's Research
        43096, -- The Breath of Alexstrasza
        43140, -- Drakkari History Tablet
        43151, -- Loken's Tongue
        43153, -- Holy Water
        43158, -- Drakkari Colossus Fragment
        43159, -- Master Summoner's Staff
        43166, -- The Bone Witch's Amulet
        43169, -- Scourgestone
        43206, -- War Horn of Acherus
        43214, -- Kurzel's Blouse Scrap
        43215, -- Ichor-Stained Cloth
        43217, -- Crystalline Heartwood
        43218, -- Ancient Elven Masonry
        43225, -- Crystallized Energy
        43229, -- Death's Gaze Orb
        43238, -- Untarnished Silver Bar
        43239, -- Shiny Bauble
        43240, -- Golden Goblet
        43241, -- Jade Statue
        43242, -- Jagged Shard
        43243, -- Blessed Banner of the Crusade
        43259, -- Jagged Shard
        43288, -- Smelted Metal Bar
        43289, -- Bag of Jagged Shards
        43290, -- Father Gustav's Report
        43291, -- Runed Saronite Plate
        43314, -- Eternal Ember
        43315, -- Sigil of the Ebon Blade
        43322, -- Enchanted Alliance Breastplate
        43323, -- Quiver of Dragonbone Arrows
        43324, -- Alliance Herb Pouch
        43411, -- Anub'arak's Broken Husk
        43440, -- To King Anduin Wrynn of the Alliance
        43441, -- To Saurfang of the Horde
        43493, -- Watcher's Corpse Dust
        43494, -- Ahn'kahar Watcher's Corpse
        43511, -- Grotesque Fungus
        43513, -- Vrykul Weapon
        43524, -- Olakin's Torch
        43526, -- Olakin's Torso
        43527, -- Olakin's Legs
        43528, -- Olakin's Left Arm
        43529, -- Olakin's Right Arm
        43564, -- Crusader Olakin's Remains
        43567, -- Spool of Thread
        43568, -- The Doctor's Cleaver
        43608, -- Copperclaw's Volatile Oil
        43609, -- Pile of Bones
        43610, -- Abandoned Helm
        43615, -- Saurfang's Battle Armor
        43616, -- Abandoned Armor
        43662, -- Axe of the Plunderer
        43665, -- Keristrasza's Broken Heart
        43668, -- Ley Line Tuner
        43669, -- Locket of the Deceased Queen
        43670, -- Prophet's Enchanted Tiki
        43688, -- Weeping Quarry Document
        43689, -- Weeping Quarry Ledger
        43690, -- Weeping Quarry Schedule
        43691, -- Weeping Quarry Map
        43693, -- Mojo Remnant of Akali
        43697, -- Artifact from the Nathrezim Homeworld
        43699, -- The Curse of Flesh Disc
        43724, -- Celestial Ruby Ring
        43726, -- The Idle Crown of Anub'arak
        43821, -- Faceless One's Withered Brain
        43823, -- Head of Cyanigosa
        43966, -- Chilled Abomination Guts
        43968, -- Abomination Reanimation Kit
        43983, -- Prospector Soren's Maps
        43984, -- Prospector Khrona's Notes
        43997, -- Pustulant Spine
        44001, -- Faceless One's Blood
        44009, -- Flesh Giant Spine
        44010, -- Pustulant Spinal Fluid
        44047, -- Norgannon's Core
        44048, -- Smuggled Solution
        44127, -- Barricade Construction Kit
        44153, -- Foreman's Key
        44186, -- Rune of Distortion
        44212, -- SGM-3
        44220, -- Orgrim's Hammer Dispatch
        44222, -- Dart Gun
        44246, -- Orb of Illusion
        44251, -- Partitioned Flask
        44301, -- Tainted Essence
        44304, -- Writhing Mass
        44307, -- Diluted Cult Tonic
        44319, -- Broken Shard of Horror
        44320, -- Broken Shard of Despair
        44321, -- Broken Shard of Suffering
        44433, -- Rod of Siphoning
        44434, -- Dark Matter
        44450, -- Hourglass of Eternity
        44459, -- Cult of the Damned Research - Page 1
        44460, -- Cult of the Damned Research - Page 2
        44461, -- Cult of the Damned Research - Page 3
        44462, -- Cult of the Damned Thesis
        44474, -- Fordragon's Shield
        44476, -- Alumeth's Skull
        44477, -- Alumeth's Heart
        44478, -- Alumeth's Scepter
        44479, -- Alumeth's Robes
        44480, -- Alumeth's Remains
        44529, -- Demolisher Parts
        44631, -- Whelp Bone Dust
        44650, -- Heart of Magic
        44651, -- Heart of Magic
        44653, -- Volatile Acid
        44656, -- Cultist Acolyte Robes
        44704, -- Charged Disk
        43297, -- Damaged Necklace
        43298, -- Beautiful Chalcedony Necklace
        43299, -- Damaged Necklace
        37163, -- Rogues Deck
        43039, -- Rogues Deck
        37664, -- Element 115
        45127, -- Mark of the Valiant
        45500, -- Mark of the Champion
        44158, -- Demons Deck
        44185, -- Demons Deck
        44148, -- Mages Deck
        44184, -- Mages Deck
        37164, -- Swords Deck
        42922, -- Swords Deck
        44260, -- Ace of Prisms
        44261, -- Two of Prisms
        44262, -- Three of Prisms
        44263, -- Four of Prisms
        44264, -- Five of Prisms
        44265, -- Six of Prisms
        44266, -- Seven of Prisms
        44267, -- Eight of Prisms
        44268, -- Ace of Nobles
        44269, -- Two of Nobles
        44270, -- Three of Nobles
        44271, -- Four of Nobles
        44272, -- Five of Nobles
        44273, -- Six of Nobles
        44274, -- Seven of Nobles
        44275, -- Eight of Nobles
        44277, -- Ace of Chaos
        44278, -- Two of Chaos
        44279, -- Three of Chaos
        44280, -- Four of Chaos
        44281, -- Five of Chaos
        44282, -- Six of Chaos
        44284, -- Seven of Chaos
        44285, -- Eight of Chaos
        44286, -- Ace of Undeath
        44287, -- Two of Undeath
        44288, -- Three of Undeath
        44289, -- Four of Undeath
        44290, -- Five of Undeath
        44291, -- Six of Undeath
        44292, -- Seven of Undeath
        44293, -- Eight of Undeath
        44259, -- Prisms Deck
        44276, -- Chaos Deck
        44294, -- Undeath Deck
        44326, -- Nobles Deck
        46052, -- Reply-Code Alpha
        46053, -- Reply-Code Alpha
        49643, -- Head of Onyxia
        49644, -- Head of Onyxia
        44569, -- Key to the Focusing Iris
        44577, -- Heroic Key to the Focusing Iris
        45506, -- Archivum Data Disc
        45857, -- Archivum Data Disc
        49869, -- Light's Vengeance
        50226, -- Festergut's Acidic Blood
        50231, -- Rotface's Acidic Blood
        50379, -- Battered Hilt
        50380, -- Battered Hilt
        51315, -- Sealed Chest
        51317, -- Alexandros' Soul Shard
        51318, -- Jaina's Locket
        51319, -- Arthas' Training Sword
        51320, -- Badge of the Silver Hand
        51321, -- Blood of Sylvanas
        45039, -- Shattered Fragments of Val'anyr
        50274, -- Shadowfrost Shard
        129940, -- Commendation of the Kirin Tor
        129942, -- Commendation of the Argent Crusade
        129943, -- Commendation of the Sons of Hodir
        129944, -- Commendation of the Wyrmrest Accord
        129941, -- Commendation of the Ebon Blade
        129954, -- Commendation of the Horde Expedition
        129955  -- Commendation of the Alliance Vanguard
    },
    [addon.CONS.RECIPES_ID] = {
        [addon.CONS.R_BOOKS_ID] = {
            44714, -- Tome of Dalaran Brilliance
            44709, -- Tome of Polymorph: Black Cat
            44793  -- Tome of Polymorph: Rabbit
        },
        [addon.CONS.R_ALCHEMY_ID] = {
            44564, -- Recipe: Mighty Arcane Protection Potion
            44565, -- Recipe: Mighty Fire Protection Potion
            44566, -- Recipe: Mighty Frost Protection Potion
            44567, -- Recipe: Mighty Nature Protection Potion
            44568  -- Recipe: Mighty Shadow Protection Potion
        },
        [addon.CONS.R_BLACKSMITHING_ID] = {
            41120, -- Plans: Reinforced Cobalt Legplates
            41122, -- Plans: Reinforced Cobalt Chestpiece
            41123, -- Plans: Reinforced Cobalt Helm
            41124, -- Plans: Reinforced Cobalt Shoulders
            44937, -- Plans: Titanium Plating
            44938, -- Plans: Titanium Plating
            45088, -- Plans: Belt of the Titans
            45089, -- Plans: Battlelord's Plate Boots
            45090, -- Plans: Plate Girdle of Righteousness
            45091, -- Plans: Treads of Destiny
            45092, -- Plans: Indestructible Plate Girdle
            45093, -- Plans: Spiked Deathdealers
            47622, -- Plans: Breastplate of the White Knight
            47623, -- Plans: Saronite Swordbreakers
            47624, -- Plans: Titanium Razorplate
            47625, -- Plans: Titanium Spikeguards
            47626, -- Plans: Sunforged Breastplate
            47627, -- Plans: Sunforged Bracers
            47640, -- Plans: Breastplate of the White Knight
            47641, -- Plans: Saronite Swordbreakers
            47642, -- Plans: Sunforged Bracers
            47643, -- Plans: Sunforged Breastplate
            47644, -- Plans: Titanium Razorplate
            47645, -- Plans: Titanium Spikeguards
            49969, -- Plans: Puresteel Legplates
            49970, -- Plans: Protectors of Life
            49971, -- Plans: Legplates of Painful Death
            49972, -- Plans: Hellfrozen Bonegrinders
            49973, -- Plans: Pillars of Might
            49974  -- Plans: Boots of Kingly Upheaval
        },
        [addon.CONS.R_COOKING_ID] = {
            44861, -- Recipe: Slow-Roasted Turkey
            46807, -- Recipe: Slow-Roasted Turkey
            44859, -- Recipe: Candied Sweet Potato
            46806, -- Recipe: Candied Sweet Potato
            44858, -- Recipe: Cranberry Chutney
            46805, -- Recipe: Cranberry Chutney
            44862, -- Recipe: Pumpkin Pie
            46804, -- Recipe: Pumpkin Pie
            46710, -- Recipe: Bread of the Dead
            44860, -- Recipe: Spice Bread Stuffing
            46803, -- Recipe: Spice Bread Stuffing
            43017, -- Recipe: Fish Feast
            43018, -- Recipe: Mega Mammoth Meal
            43019, -- Recipe: Tender Shoveltusk Steak
            43020, -- Recipe: Spiced Worm Burger
            43021, -- Recipe: Very Burnt Worg
            43022, -- Recipe: Mighty Rhino Dogs
            43023, -- Recipe: Poached Northern Sculpin
            43024, -- Recipe: Firecracker Salmon
            43025, -- Recipe: Spicy Blue Nettlefish
            43026, -- Recipe: Imperial Manta Steak
            43027, -- Recipe: Spicy Fried Herring
            43028, -- Recipe: Rhinolicious Wormsteak
            43029, -- Recipe: Critter Bites
            43030, -- Recipe: Hearty Rhino
            43031, -- Recipe: Snapper Extreme
            43032, -- Recipe: Blackened Worg Steak
            43033, -- Recipe: Cuttlesteak
            43034, -- Recipe: Spiced Mammoth Treats
            43035, -- Recipe: Blackened Dragonfin
            43036, -- Recipe: Dragonfin Filet
            43037, -- Recipe: Tracker Snacks
            43505, -- Recipe: Gigantic Feast
            43506, -- Recipe: Small Feast
            43507, -- Recipe: Tasty Cupcake
            43508, -- Recipe: Last Week's Mammoth
            43509, -- Recipe: Bad Clams
            43510, -- Recipe: Haunted Herring
            44954, -- Recipe: Worg Tartare
            46809, -- Bountiful Cookbook
            46810  -- Bountiful Cookbook
        },
        [addon.CONS.R_ENCHANTING_ID] = {
            37347, -- Formula: Enchant Cloak - Superior Dodge
            37349, -- Formula: Enchant Cloak - Shadow Armor
            44485, -- Formula: Enchant Gloves - Armsman
            44488, -- Formula: Enchant Cloak - Wisdom
            44489, -- Formula: Enchant Chest - Powerful Stats
            44490, -- Formula: Enchant Boots - Greater Assault
            44491, -- Formula: Enchant Boots - Tuskarr's Vitality
            44498, -- Formula: Enchant Bracer - Superior Spellpower
            37340, -- Formula: Enchant Chest - Exceptional Resilience
            44484, -- Formula: Enchant Bracer - Greater Assault
            50406, -- Formula: Enchant Gloves - Angler
            44471, -- Formula: Enchant Cloak - Mighty Stamina
            44472, -- Formula: Enchant Cloak - Greater Speed
            45050, -- Formula: Smoking Heart of the Mountain
            44944, -- Formula: Enchant Bracer - Major Stamina
            45059, -- Formula: Enchant Staff - Greater Spellpower
            37344, -- Formula: Enchant Weapon - Icebreaker
            37339, -- Formula: Enchant Weapon - Giant Slayer
            44483, -- Formula: Enchant 2H Weapon - Massacre
            44486, -- Formula: Enchant Weapon - Superior Potency
            44487, -- Formula: Enchant Weapon - Mighty Spellpower
            44492, -- Formula: Enchant Weapon - Berserking
            44494, -- Formula: Enchant Weapon - Lifeward
            44495, -- Formula: Enchant Weapon - Black Magic
            44496, -- Formula: Enchant Weapon - Accuracy
            44473, -- Formula: Enchant Weapon - Scourgebane
            46027, -- Formula: Enchant Weapon - Blade Ward
            46348  -- Formula: Enchant Weapon - Blood Draining
        },
        [addon.CONS.R_ENGINEERING_ID] = {
            23817, -- Schematic: Titanium Toolbox
            44918, -- Schematic: Cluster Launcher
            44919, -- Schematic: Firework Launcher
            49050, -- Schematic: Jeeves
            44502, -- Schematic: Mechano-Hog
            44503  -- Schematic: Mekgineer's Chopper
        },
        [addon.CONS.R_FIRST_AID_ID] = {
            39152  -- Manual: Heavy Frostweave Bandage
        },
        [addon.CONS.R_INSCRIPTIONS_ID] = {
            45912, -- Book of Glyph Mastery
            46108  -- Technique: Rituals of the New Moon
        },
        [addon.CONS.R_JEWELCRAFTING_ID] = {
            41559, -- Design: Mystic Sun Crystal
            41560, -- Design: Stormy Chalcedony
            41561, -- Design: Reckless Huge Citrine
            41562, -- Design: Deadly Huge Citrine
            41563, -- Design: Willful Huge Citrine
            41565, -- Design: Lucent Huge Citrine
            41566, -- Design: Resplendent Huge Citrine
            41567, -- Design: Nimble Dark Jade
            41568, -- Design: Purified Shadow Crystal
            41569, -- Design: Shattered Dark Jade
            41570, -- Design: Radiant Dark Jade
            41571, -- Design: Turbid Dark Jade
            41572, -- Design: Steady Dark Jade
            41574, -- Design: Defender's Shadow Crystal
            41575, -- Design: Mysterious Shadow Crystal
            41576, -- Design: Bold Scarlet Ruby
            41577, -- Design: Delicate Scarlet Ruby
            41578, -- Design: Flashing Scarlet Ruby
            41579, -- Design: Quick Autumn's Glow
            41580, -- Design: Rigid Sky Sapphire
            41581, -- Design: Sparkling Sky Sapphire
            41582, -- Design: Glinting Twilight Opal
            41686, -- Design: Potent Monarch Topaz
            41687, -- Design: Deft Monarch Topaz
            41688, -- Design: Veiled Twilight Opal
            41690, -- Design: Reckless Monarch Topaz
            41692, -- Design: Energized Forest Emerald
            41693, -- Design: Forceful Forest Emerald
            41696, -- Design: Lightning Forest Emerald
            41697, -- Design: Regal Forest Emerald
            41698, -- Design: Nimble Forest Emerald
            41702, -- Design: Jagged Forest Emerald
            41704, -- Design: Chaotic Skyflare Diamond
            41705, -- Design: Shielded Skyflare Diamond
            41706, -- Design: Ember Skyflare Diamond
            41707, -- Design: Revitalizing Skyflare Diamond
            41708, -- Design: Insightful Earthsiege Diamond
            41709, -- Design: Invigorating Earthsiege Diamond
            41710, -- Design: Relentless Earthsiege Diamond
            41711, -- Design: Trenchant Earthsiege Diamond
            41718, -- Design: Brilliant Scarlet Ruby
            41719, -- Design: Subtle Autumn's Glow
            41720, -- Design: Smooth Autumn's Glow
            41721, -- Design: Deadly Monarch Topaz
            41722, -- Design: Stalwart Monarch Topaz
            41723, -- Design: Jagged Forest Emerald
            41724, -- Design: Misty Forest Emerald
            41725, -- Design: Timeless Twilight Opal
            41726, -- Design: Guardian's Twilight Opal
            41727, -- Design: Mystic Autumn's Glow
            41728, -- Design: Stormy Sky Sapphire
            41730, -- Design: Willful Monarch Topaz
            41733, -- Design: Lucent Monarch Topaz
            41734, -- Design: Resplendent Monarch Topaz
            41735, -- Design: Shattered Forest Emerald
            41737, -- Design: Turbid Forest Emerald
            41738, -- Design: Steady Forest Emerald
            41740, -- Design: Mysterious Twilight Opal
            41742, -- Design: Enigmatic Skyflare Diamond
            41743, -- Design: Forlorn Skyflare Diamond
            41744, -- Design: Impassive Skyflare Diamond
            41747, -- Design: Shifting Twilight Opal
            41777, -- Design: Etched Twilight Opal
            41778, -- Design: Resolute Monarch Topaz
            41780, -- Design: Champion's Monarch Topaz
            41781, -- Design: Misty Forest Emerald
            41782, -- Design: Lightning Forest Emerald
            41783, -- Design: Purified Twilight Opal
            41784, -- Design: Sovereign Twilight Opal
            41786, -- Design: Destructive Skyflare Diamond
            41787, -- Design: Thundering Skyflare Diamond
            41788, -- Design: Beaming Earthsiege Diamond
            41789, -- Design: Inscribed Monarch Topaz
            41790, -- Design: Precise Scarlet Ruby
            41793, -- Design: Fierce Monarch Topaz
            41797, -- Design: Austere Earthsiege Diamond
            41798, -- Design: Bracing Earthsiege Diamond
            41799, -- Design: Eternal Earthsiege Diamond
            41818, -- Design: Accurate Twilight Opal
            41819, -- Design: Radiant Forest Emerald
            41820, -- Design: Defender's Twilight Opal
            42138, -- Design: Solid Sky Sapphire
            43317, -- Design: Ring of Earthen Might
            43318, -- Design: Ring of Scarlet Shadows
            43319, -- Design: Windfire Band
            43320, -- Design: Ring of Northern Tears
            43485, -- Design: Savage Titanium Ring
            43497, -- Design: Savage Titanium Band
            46897, -- Design: Regal Eye of Zul
            46898, -- Design: Steady Eye of Zul
            46899, -- Design: Nimble Eye of Zul
            46901, -- Design: Jagged Eye of Zul
            46902, -- Design: Timeless Dreadstone
            46904, -- Design: Forceful Eye of Zul
            46905, -- Design: Misty Eye of Zul
            46909, -- Design: Lightning Eye of Zul
            46911, -- Design: Radiant Eye of Zul
            46912, -- Design: Energized Eye of Zul
            46913, -- Design: Shattered Eye of Zul
            46915, -- Design: Turbid Eye of Zul
            46916, -- Design: Brilliant Cardinal Ruby
            46917, -- Design: Bold Cardinal Ruby
            46918, -- Design: Delicate Cardinal Ruby
            46920, -- Design: Precise Cardinal Ruby
            46922, -- Design: Subtle King's Amber
            46923, -- Design: Flashing Cardinal Ruby
            46924, -- Design: Solid Majestic Zircon
            46925, -- Design: Sparkling Majestic Zircon
            46926, -- Design: Stormy Majestic Zircon
            46928, -- Design: Rigid Majestic Zircon
            46929, -- Design: Smooth King's Amber
            46932, -- Design: Mystic King's Amber
            46933, -- Design: Quick King's Amber
            46935, -- Design: Sovereign Dreadstone
            46937, -- Design: Purified Dreadstone
            46938, -- Design: Shifting Dreadstone
            46941, -- Design: Defender's Dreadstone
            46942, -- Design: Guardian's Dreadstone
            46943, -- Design: Mysterious Dreadstone
            46948, -- Design: Inscribed Ametrine
            46949, -- Design: Deadly Ametrine
            46950, -- Design: Potent Ametrine
            46951, -- Design: Veiled Dreadstone
            46952, -- Design: Willful Ametrine
            46953, -- Design: Etched Dreadstone
            46956, -- Design: Glinting Dreadstone
            47007, -- Design: Reckless Ametrine
            47010, -- Design: Accurate Dreadstone
            47015, -- Design: Champion's Ametrine
            47017, -- Design: Stalwart Ametrine
            47018, -- Design: Resplendent Ametrine
            47019, -- Design: Fierce Ametrine
            47020, -- Design: Deft Ametrine
            47021, -- Design: Lucent Ametrine
            47022, -- Design: Resolute Ametrine
            49112, -- Design: Nightmare Tear
            42298, -- Design: Bold Dragon's Eye
            42301, -- Design: Delicate Dragon's Eye
            42302, -- Design: Flashing Dragon's Eye
            42305, -- Design: Mystic Dragon's Eye
            42306, -- Design: Precise Dragon's Eye
            42307, -- Design: Quick Dragon's Eye
            42308, -- Design: Rigid Dragon's Eye
            42309, -- Design: Brilliant Dragon's Eye
            42310, -- Design: Smooth Dragon's Eye
            42311, -- Design: Solid Dragon's Eye
            42312, -- Design: Sparkling Dragon's Eye
            42313, -- Design: Stormy Dragon's Eye
            42314, -- Design: Subtle Dragon's Eye
            42648, -- Design: Titanium Impact Band
            42649, -- Design: Titanium Earthguard Ring
            42650, -- Design: Titanium Spellshock Ring
            42651, -- Design: Titanium Impact Choker
            42652, -- Design: Titanium Earthguard Chain
            42653, -- Design: Titanium Spellshock Necklace
            43597  -- Design: Titanium Frostguard Ring
        },
        [addon.CONS.R_LEATHERWORKING_ID] = {
            44513, -- Pattern: Eviscerator's Facemask
            44514, -- Pattern: Eviscerator's Shoulderpads
            44515, -- Pattern: Eviscerator's Chestguard
            44516, -- Pattern: Eviscerator's Bindings
            44517, -- Pattern: Eviscerator's Gauntlets
            44518, -- Pattern: Eviscerator's Waistguard
            44519, -- Pattern: Eviscerator's Legguards
            44520, -- Pattern: Eviscerator's Treads
            44521, -- Pattern: Overcast Headguard
            44522, -- Pattern: Overcast Spaulders
            44523, -- Pattern: Overcast Chestguard
            44524, -- Pattern: Overcast Bracers
            44525, -- Pattern: Overcast Handwraps
            44526, -- Pattern: Overcast Belt
            44527, -- Pattern: Overcast Leggings
            44528, -- Pattern: Overcast Boots
            44530, -- Pattern: Swiftarrow Helm
            44531, -- Pattern: Swiftarrow Shoulderguards
            44532, -- Pattern: Swiftarrow Hauberk
            44533, -- Pattern: Swiftarrow Bracers
            44534, -- Pattern: Swiftarrow Gauntlets
            44535, -- Pattern: Swiftarrow Belt
            44536, -- Pattern: Swiftarrow Leggings
            44537, -- Pattern: Swiftarrow Boots
            44538, -- Pattern: Stormhide Crown
            44539, -- Pattern: Stormhide Shoulders
            44540, -- Pattern: Stormhide Hauberk
            44541, -- Pattern: Stormhide Wristguards
            44542, -- Pattern: Stormhide Grips
            44543, -- Pattern: Stormhide Belt
            44544, -- Pattern: Stormhide Legguards
            44545, -- Pattern: Stormhide Stompers
            44509, -- Pattern: Trapper's Traveling Pack
            44510, -- Pattern: Mammoth Mining Bag
            45094, -- Pattern: Belt of Dragons
            45095, -- Pattern: Boots of Living Scale
            45096, -- Pattern: Blue Belt of Chaos
            45097, -- Pattern: Lightning Grounded Boots
            45098, -- Pattern: Death-Warmed Belt
            45099, -- Pattern: Footpads of Silence
            45100, -- Pattern: Belt of Arctic Life
            45101, -- Pattern: Boots of Wintry Endurance
            47628, -- Pattern: Ensorcelled Nerubian Breastplate
            47629, -- Pattern: Black Chitin Bracers
            47630, -- Pattern: Crusader's Dragonscale Breastplate
            47631, -- Pattern: Crusader's Dragonscale Bracers
            47632, -- Pattern: Lunar Eclipse Robes
            47633, -- Pattern: Moonshadow Armguards
            47634, -- Pattern: Knightbane Carapace
            47635, -- Pattern: Bracers of Swift Death
            47646, -- Pattern: Black Chitin Bracers
            47647, -- Pattern: Bracers of Swift Death
            47648, -- Pattern: Crusader's Dragonscale Bracers
            47649, -- Pattern: Crusader's Dragonscale Breastplate
            47650, -- Pattern: Ensorcelled Nerubian Breastplate
            47651, -- Pattern: Knightbane Carapace
            47652, -- Pattern: Lunar Eclipse Robes
            47653, -- Pattern: Moonshadow Armguards
            49957, -- Pattern: Legwraps of Unleashed Nature
            49958, -- Pattern: Blessed Cenarion Boots
            49959, -- Pattern: Bladeborn Leggings
            49961, -- Pattern: Footpads of Impending Death
            49962, -- Pattern: Lightning-Infused Leggings
            49963, -- Pattern: Earthsoul Boots
            49965, -- Pattern: Draconic Bonesplinter Legguards
            49966, -- Pattern: Rock-Steady Treads
            44546, -- Pattern: Giantmaim Legguards
            44547, -- Pattern: Giantmaim Bracers
            44548, -- Pattern: Revenant's Breastplate
            44549, -- Pattern: Revenant's Treads
            44550, -- Pattern: Trollwoven Spaulders
            44551, -- Pattern: Trollwoven Girdle
            44552, -- Pattern: Earthgiving Legguards
            44553, -- Pattern: Earthgiving Boots
            44584, -- Pattern: Polar Vest
            44585, -- Pattern: Polar Cord
            44586, -- Pattern: Polar Boots
            44587, -- Pattern: Icy Scale Chestguard
            44588, -- Pattern: Icy Scale Belt
            44589, -- Pattern: Icy Scale Boots
            44932, -- Pattern: Windripper Boots
            44933  -- Pattern: Windripper Leggings
        },
        [addon.CONS.R_MINING_ID] = {
            44956  -- Goblin's Guide to Elementium
        },
        [addon.CONS.R_TAILORING_ID] = {
            42183, -- Pattern: Abyssal Bag
            42184, -- Pattern: Glacial Bag
            42185, -- Pattern: Mysterious Bag
            42187, -- Pattern: Brilliant Spellthread
            42188, -- Pattern: Sapphire Spellthread
            45774, -- Pattern: Emerald Bag
            42172, -- Pattern: Red Lumberjack Shirt
            42173, -- Pattern: Blue Lumberjack Shirt
            42175, -- Pattern: Green Lumberjack Shirt
            42176, -- Pattern: Blue Workman's Shirt
            42177, -- Pattern: Red Workman's Shirt
            42178, -- Pattern: Rustic Workman's Shirt
            44916, -- Pattern: Festival Dress
            44917, -- Pattern: Festival Suit
            43876, -- A Guide to Northern Cloth Scavenging
            54798, -- Pattern: Frosty Flying Carpet
            45102, -- Pattern: Sash of Ancient Power
            45103, -- Pattern: Spellslinger's Slippers
            45104, -- Pattern: Cord of the White Dawn
            45105, -- Pattern: Savior's Slippers
            47636, -- Pattern: Royal Moonshroud Robe
            47637, -- Pattern: Royal Moonshroud Bracers
            47638, -- Pattern: Merlin's Robe
            47639, -- Pattern: Bejeweled Wizard's Bracers
            47654, -- Pattern: Bejeweled Wizard's Bracers
            47655, -- Pattern: Merlin's Robe
            47656, -- Pattern: Royal Moonshroud Bracers
            47657, -- Pattern: Royal Moonshroud Robe
            49953, -- Pattern: Leggings of Woven Death
            49954, -- Pattern: Deathfrost Boots
            49955, -- Pattern: Lightweave Leggings
            49956  -- Pattern: Sandals of Consecration
        }
    },
    [addon.CONS.TRADE_GOODS_ID] = {
        43127, -- Snowfall Ink
        43126, -- Ink of the Sea
        43125, -- Darkflame Ink
        43124, -- Ethereal Ink
        43123, -- Ink of the Sky
        43122, -- Shimmering Ink
        43121, -- Fiery Ink
        43120, -- Celestial Ink
        43119, -- Royal Ink
        43118, -- Jadefire Ink
        43117, -- Dawnstar Ink
        43116, -- Lion's Ink
        43115, -- Hunter's Ink
        39774, -- Midnight Ink
        39469, -- Moonglow Ink
        43109, -- Icy Pigment
        43108, -- Ebon Pigment
        39343, -- Azure Pigment
        43107, -- Sapphire Pigment
        39342, -- Nether Pigment
        43106, -- Ruby Pigment
        39341, -- Silvery Pigment
        43105, -- Indigo Pigment
        39340, -- Violet Pigment
        43104, -- Burnt Pigment
        39339, -- Emerald Pigment
        43103, -- Verdant Pigment
        39338, -- Golden Pigment
        39334, -- Dusky Pigment
        39151, -- Alabaster Pigment
        [addon.CONS.T_CLOTH_ID] = {
            41593, -- Ebonweave
            41594, -- Moonshroud
            41595, -- Spellweave
            41511, -- Bolt of Imbued Frostweave
            42253, -- Iceweb Spider Silk
            33470, -- Frostweave Cloth
            38426, -- Eternium Thread
            41510  -- Bolt of Frostweave
        },
        [addon.CONS.T_ELEMENTAL_ID] = {
            37700, -- Crystallized Air
            35622, -- Eternal Water
            35624, -- Eternal Earth
            35625, -- Eternal Life
            35627, -- Eternal Shadow
            36860, -- Eternal Fire
            35623, -- Eternal Air
            37701, -- Crystallized Earth
            37702, -- Crystallized Fire
            37703, -- Crystallized Shadow
            37704, -- Crystallized Life
            37705  -- Crystallized Water
        },
        [addon.CONS.T_ENCHANTING_ID] = {
            38682, -- Enchanting Vellum
            44452, -- Runed Titanium Rod
            34057, -- Abyss Crystal
            34052, -- Dream Shard
            34053, -- Small Dream Shard
            34055, -- Greater Cosmic Essence
            34056, -- Lesser Cosmic Essence
            34054, -- Infinite Dust
            41741, -- Cobalt Rod
            41745  -- Titanium Rod
        },
        [addon.CONS.T_PARTS_ID] = {
            44499, -- Salvaged Iron Golem Parts
            44501, -- Goblin-Machined Piston
            40533, -- Walnut Stock
            44500, -- Elementium-Plated Exhaust Pipe
            39690, -- Volatile Blasting Trigger
            39681, -- Handful of Cobalt Bolts
            39684, -- Hair Trigger
            39682, -- Overcharged Capacitor
            39683  -- Froststeel Tube
        },
        [addon.CONS.T_FISHING_ID] = {
            43571, -- Sewer Carp
            43572, -- Magic Eater
            43647, -- Shimmering Minnow
            43652, -- Slippery Eel
            43646  -- Fountain Goldfish
        },
        [addon.CONS.T_HERBS_ID] = {
            36908, -- Frost Lotus
            36905, -- Lichbloom
            108355, -- Lichbloom Stalk
            36906, -- Icethorn
            108356, -- Icethorn Bramble
            36903, -- Adder's Tongue
            108353, -- Adder's Tongue Stem
            39970, -- Fire Leaf
            108359, -- Fire Leaf Bramble
            36901, -- Goldclover
            108352, -- Goldclover Leaf
            36904, -- Tiger Lily
            108354, -- Tiger Lily Petal
            36907, -- Talandra's Rose
            108357, -- Talandra's Rose Petal
            37921, -- Deadnettle
            108358  -- Deadnettle Bramble
        },
        [addon.CONS.T_JEWELCRAFTING_ID] = {
            41266, -- Skyflare Diamond
            36919, -- Cardinal Ruby
            36925, -- Majestic Zircon
            36928, -- Dreadstone
            36931, -- Ametrine
            36934, -- Eye of Zul
            36918, -- Scarlet Ruby
            36921, -- Autumn's Glow
            36924, -- Sky Sapphire
            36927, -- Twilight Opal
            36930, -- Monarch Topaz
            36933, -- Forest Emerald
            36922, -- King's Amber
            41334, -- Earthsiege Diamond
            42225, -- Dragon's Eye
            46849, -- Titanium Powder
            36784, -- Siren's Tear
            36783, -- Northsea Pearl
            36917, -- Bloodstone
            36920, -- Sun Crystal
            36923, -- Chalcedony
            36926, -- Shadow Crystal
            36929, -- Huge Citrine
            36932, -- Dark Jade
            45054  -- Prismatic Black Diamond
        },
        [addon.CONS.T_LEATHER_ID] = {
            38425, -- Heavy Borean Leather
            44128, -- Arctic Fur
            38557, -- Icy Dragonscale
            112158, -- Icy Dragonscale Fragment
            38561, -- Jormungar Scale
            112178, -- Jormungar Scale Fragment
            33567, -- Borean Leather Scraps
            33568, -- Borean Leather
            38558, -- Nerubian Chitin
            112177  -- Nerubian Chitin Fragment
        },
        [addon.CONS.T_MEAT_ID] = {
            [addon.CONS.TM_ANIMAL_ID] = {
                36782, -- Succulent Clam Meat
                34736, -- Chunk o' Mammoth
                43009, -- Shoveltusk Flank
                43010, -- Worm Meat
                43011, -- Worg Haunch
                43013, -- Chilled Meat
                44834  -- Wild Turkey
            },
            [addon.CONS.TM_EGG_ID] = {
                43501  -- Northern Egg
            },
            [addon.CONS.TM_FISH_ID] = {
                41808, -- Bonescale Snapper 
                41800, -- Deep Sea Monsterbelly
                41802, -- Imperial Manta Ray
                41803, -- Rockfin Grouper
                41805, -- Borean Man O' War
                41806, -- Musselback Sculpin
                41807, -- Dragonfin Angelfish
                41801, -- Moonglow Cuttlefish
                41809, -- Glacial Salmon
                41810, -- Fangtooth Herring
                41812, -- Barrelhead Goby
                41813, -- Nettlefish
                41814, -- Glassfin Minnow
                43012  -- Rhino Meat
            }
        },
        [addon.CONS.T_METAL_STONE_ID] = {
            36910, -- Titanium Ore
            108391, -- Titanium Ore Nugget
            37663, -- Titansteel Bar
            41163, -- Titanium Bar
            36913, -- Saronite Bar
            36912, -- Saronite Ore
            108306, -- Saronite Ore Nugget
            36909, -- Cobalt Ore
            108305, -- Cobalt Ore Nugget
            36916   -- Cobalt Bar
        },
        [addon.CONS.T_OTHER_ID] = {
            43007, -- Northern Spices
            45909, -- Giant Darkwater Clam
            36781, -- Darkwater Clam
            40195, -- Pygmy Oil
            40199, -- Pygmy Suckerfish
            44700, -- Brooding Darkwater Clam
            44958, -- Ethereal Oil
            44475, -- Reinforced Crate
            44163, -- Shadowy Tarot
            44161, -- Arcane Tarot
            44142, -- Strange Tarot
            37168, -- Mysterious Tarot
            44835, -- Autumnal Herbs
            39354, -- Light Parchment
            44853, -- Honey
            46888, -- Bountiful Basket
            43102, -- Frozen Orb
            44318, -- Darkmoon Card of the North
            45087, -- Runed Orb
            47556, -- Crusader Orb
            49908, -- Primordial Saronite
            44317, -- Greater Darkmoon Card
            44316  -- Darkmoon Card
        }
    }
}