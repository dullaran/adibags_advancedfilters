local _, addon = ...
local AdiBags = LibStub("AceAddon-3.0"):GetAddon("AdiBags")
local L = addon.L

local ItemsIDs = nil
local ItemsIDsExpansion = nil

local function concatTable(table1, table2)
    local result = {}
    if table1 then 
        for key, value in pairs(table1) do
            result[key] = value
        end
    end
    if table2 then 
        for key, value in pairs(table2) do
            result[key] = value
        end
    end
    return result
end

local function incorporeTable(table1, table2)
    if table2 then
        for key, value in pairs(table2) do
            table1[key] = value
        end
    end
end

local function copyArray(array1)
    local result = {}
    for index, value in ipairs(array1) do
        result[index] = value
    end
    return result
end

local function InitItemsID(filter)

    local function AddToSet(filter, set, list, label)
        if list then
            for _, v in ipairs(list) do
                if set[v] then
                    if filter.db.profile.flagDebug['DEBUG_CLASSIFICATION'] then print("Item ID \'" .. v .. "\' from category \'" .. set[v] .. "\' is changing to \'" .. label .. "\'") end
                end
                set[v] = label
            end
        end
    end

    local function classifyCategory(filter, response, categoryTree, database, expansion, loopNumber, categoryHistory)
        for category, subcategories in pairs(categoryTree) do

            local IDCategory = nil
            local IDExpansionOfCategory = false
            local IDSplit = nil
            -- if (category == addon.CONS.ASHRAN_ID) then
            --     -- print(category)
            --     print('xflagSplit'..addon.LABEL_TEXT[category])
            --     print(filter.db.profile['flagSplit'..addon.LABEL_TEXT[category]])
            --     -- print('flagSplit' .. addon.LABEL_TEXT[category] .. ": " .. filter.db.profile['flagSplit'..addon.LABEL_TEXT[category]])
    
            --     -- if filter.db.profile['flagSplit'..addon.LABEL_TEXT[category]] then
            --     --     IDCategory = category
            -- end

            actualCategoryHistory = copyArray(categoryHistory)
            actualCategoryHistory[loopNumber] = category
            for index, historyCategory in ipairs(actualCategoryHistory) do
                if (table.getn(actualCategoryHistory) ~= index) then
                    if filter.db.profile['flagSplit' .. addon.LABEL_TEXT[historyCategory]][actualCategoryHistory[index + 1]] then
                        IDSplit = actualCategoryHistory[index + 1]
                    end
                end
            end

            if IDSplit ~= nil then
                IDCategory = IDSplit
            elseif filter.db.profile.flagOrganizeByExpansion[expansion] then
                IDCategory = expansion
            elseif filter.db.profile.flagOrganizeInOldExpansions[expansion] then
                IDCategory = addon.CONS.OLD_EXPANSIONS_ID
            else
                IDCategory = category
            end

            if filter.db.profile.flagDebug['DEBUG_CLASSIFICATION'] then print("Expansion: \'" .. addon.LABEL_TEXT_SHORT[expansion] .. "\', category: \'" .. addon.LABEL_TEXT[category] .. "\', adicionada em: \'" .. addon.LABEL_TEXT[IDCategory] .. "\'") end

            if (database[category]) then
                AddToSet(filter, response, database[category], IDCategory)
                if (type(subcategories) == 'table') then
                    classifyCategory(filter, response, subcategories, database[category], expansion, (loopNumber + 1), actualCategoryHistory)
                end
            end
        end
    end

    if filter.db.profile.flagDebug['DEBUG_CLASSIFICATION'] then print(" #---- Starting item classification ----#") end
    local result = {}

    for _, expansion in ipairs(addon.RULES.EXPANSIONS_LIST) do
        classifyCategory(filter, result, concatTable(addon.RULES.CATEGORY_TREE, addon.RULES.EXPANSION_SPECIFIC_SUBCATEGORY[expansion]), addon.ITEM_DATABASE[expansion], expansion, 1, {[0] = L['Categories']})
    end

    return result
end

local generalFilter = AdiBags:RegisterFilter(addon.FILTER_NAME, 92)
generalFilter.uiName = addon.FILTER_NAME
generalFilter.uiDesc = addon.FILTER_DESCRIPTION

function generalFilter:Update()
    ItemsIDs = nil
    ItemsIDsExpansion = nil
    self:SendMessage('AdiBags_FiltersChanged')
end

function generalFilter:OnEnable()
    AdiBags:UpdateFilters()
end

function generalFilter:OnDisable()
    AdiBags:UpdateFilters()
end

function generalFilter:Filter(slotData)
    ItemsIDs = ItemsIDs or InitItemsID(self)

    if ItemsIDs[slotData.itemId] then
        -- if self.db.profile.flagExpansionPrefix and Label_Expansions_Text[ItemsExpansionIDs[slotData.itemId]] then
        --     if self.db.profile.flagDebug['DEBUG_FILTER'] then print("Item de nome \'" .. slotData.name .. "\' classificado como: \'" .. Label_Expansions_Text[ItemsExpansionIDs[slotData.itemId]]  .. ": " .. Label_Text[ItemsIDs[slotData.itemId]] .. "\'") end
        --     return Label_Expansions_Text[ItemsExpansionIDs[slotData.itemId]]  .. ": " .. Label_Text[ItemsIDs[slotData.itemId]]
        -- else
            return addon.LABEL_TEXT[ItemsIDs[slotData.itemId]]
        -- end
    -- else
    --     if self.db.profile.flagDebug['DEBUG_CATEGORIZATION'] then
    --         local equipSlot = slotData.equipSlot
    --         if equipSlot and equipSlot ~= "" then
    --         else
    --             if self.db.profile.flagDebug['DEBUG_FILTER'] then print("Item de nome \'" .. slotData.name .. "\' sem categoria:. ID: \'" .. slotData.itemId .. "\'") end
    --             return "Junk"
    --         end
    --     end
    end
    if self.db.profile.flagDebug['DEBUG_FILTER'] then
        if (slotData.equipSlot ~= nil) and (slotData.equipSlot == "") and (slotData.quality ~= 0) then
            print("Item \'" .. slotData.name .. "\', ID: \'" .. slotData.itemId .. "\', Rarity: " .. slotData.quality .. "\'")
        end
    end
        
end

function generalFilter:OnInitialize()

    local function generateProfile(profile, categoryTree, category)
        flagName = 'flagSplit'..category
        profile[flagName] = {false}

        for key, value in pairs(categoryTree) do
            if (type(value) == 'table') then
                generateProfile(profile, value, addon.LABEL_TEXT[key])
            end
        end
    end

    profile = {
        ['flagOrganizeByExpansion'] = {false},
        ['flagOrganizeInOldExpansions'] = {false},
        ['flagDebug'] = {false}
    }

    fullCategoryTree = concatTable(addon.RULES.CATEGORY_TREE)

    for index, expansion in ipairs(addon.RULES.EXPANSIONS_LIST) do
        fullCategoryTree = concatTable(fullCategoryTree, addon.RULES.EXPANSION_SPECIFIC_SUBCATEGORY[expansion])
    end

    generateProfile(profile, fullCategoryTree, L['Categories'])

    self.db = AdiBags.db:RegisterNamespace(addon.FILTER_NAME, {
        profile = profile,
        char = {},
    })
end

function generalFilter:GetOptions()
    
    local function generateOptions(result, categoryTree, category, loopNumber, childNumber)
        flagName = 'flagSplit'..category
        result[flagName] = {
            ['name'] = addon.FILTER_TEXT.SplitText .. category,
            ['desc'] = addon.FILTER_TEXT.SplitDesc,
            ['type'] = 'multiselect',
            ['order'] = 10 * loopNumber + childNumber,
            ['values'] = {}
        }
        child = 0
        toDo = {}
        for key, value in pairs(categoryTree) do
            child = child + 0.25
            result[flagName].values[key] = addon.LABEL_TEXT[key]
            if (type(value) == 'table') then
                table.insert(toDo, {['key'] = key, ['value'] = value})
            end
        end

        for _, nodeChild in ipairs(toDo) do
            generateOptions(result, nodeChild.value, addon.LABEL_TEXT[nodeChild.key], (loopNumber + 1), child)
        end

    end

    local result = {
        ['flagOrganizeByExpansion'] = {
            ['name'] = addon.FILTER_TEXT.ORGANIZE_BY_EXPANSION_TEXT,
            ['desc'] = addon.FILTER_TEXT.ORGANIZE_BY_EXPANSION_DESC,
            ['type'] = 'multiselect',
            ['order'] = 06,
            ['values'] = {
            }
        },
        ['flagOrganizeInOldExpansions'] = {
            ['name'] = addon.FILTER_TEXT.OLD_EXPANSION_TEXT,
            ['desc'] = addon.FILTER_TEXT.OLD_EXPANSION_DESC,
            ['type'] = 'multiselect',
            ['order'] = 08,
            ['values'] = {
            }
        },
        ['flagDebug'] = {
            ['name'] = addon.FILTER_TEXT.DEBUG_TEXT,
            ['desc'] = addon.FILTER_TEXT.DEBUG_DESC,
            ['type'] = 'multiselect',
            ['order'] = 99,
            ['values'] = {
                ['DEBUG_FILTER'] = L['Filter'],
                ['DEBUG_CLASSIFICATION'] = L['Classification'],
                ['DEBUG_INITIALIZATION'] = L['Initialization'],
                ['DEBUG_CATEGORIZATION'] = L['Categorization']
            }
        }
    }

    fullCategoryTree = concatTable(addon.RULES.CATEGORY_TREE)

    for index, expansion in ipairs(addon.RULES.EXPANSIONS_LIST) do
        result.flagOrganizeByExpansion.values[expansion] = addon.LABEL_TEXT[expansion]
        result.flagOrganizeInOldExpansions.values[expansion] = addon.LABEL_TEXT[expansion]
        fullCategoryTree = concatTable(fullCategoryTree, addon.RULES.EXPANSION_SPECIFIC_SUBCATEGORY[expansion])
    end

    generateOptions(result, fullCategoryTree, L['Categories'], 1, 0)
    return result, AdiBags:GetOptionHandler(self, false, function() return self:Update() end)
end