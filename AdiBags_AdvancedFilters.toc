## Interface: 80200

## Title: AdiBags Extended Filters
## Notes: Adds an extra filters to AdiBags.
## Author: Dullaran
## Version: 0.0
## Dependencies: AdiBags

Localization.lua
Constants.lua
database\Classic.lua
database\TheBurningCrusade.lua
database\WrathOfTheLichKing.lua
database\Cataclysm.lua
database\MistsOfPandaria.lua
database\WarlordsOfDraenor.lua
database\Legion.lua
database\BattleForAzeroth.lua
AdiBags_AdvancedFilters.lua