local _, addon = ...
local L = addon.L

addon.FILTER_NAME = L['Advanced Item Filter']
addon.FILTER_DESCRIPTION = L['Advanced Item Filter']

addon.ITEM_DATABASE = {}

addon.CONS = {
    ['CLASSIC_ID'] = '0',
    ['THE_BURNING_CRUSADE_ID'] = '1',
    ['WRATH_OF_THE_LICH_KING_ID'] = '2',
    ['CATACLYSM_ID'] = '3',
    ['MISTS_OF_PANDARIA_ID'] = '4',
    ['WARLORDS_OF_DRAENOR_ID'] = '5',
    ['LEGION_ID'] = '6',
    ['BATTLE_FOR_AZEROTH_ID'] = '7',
    ['OTHER_ID'] = '20',
    ['OLD_EXPANSIONS_ID'] = '21',
    
    ['WEAPONS_ID'] = '30',
    ['ARMOR_ID'] = '31',
    ['CONTAINERS_ID'] = '32',
    ['CONSUMABLES_ID'] = '33',
    ['CURRENCY_ID'] = '34',
    ['GEMS_ID'] = '35',
    ['GLYPHS_ID'] = '36',
    ['KEYS_ID'] = '37',
    ['MISCELLANEOUS_ID'] = '38',
    ['QUEST_ID'] = '39',
    ['RECIPES_ID'] = '40',
    ['TRADE_GOODS_ID'] = '41',

    -- Consumable
    ['C_BANDAGES_ID'] = '42',
    ['C_CONSUMABLES_ID'] = '43',
    ['C_ELIXIRS_ID'] = '44',
    ['C_FLASKS_ID'] = '45',
    ['C_FOOD_DRINKS_ID'] = '46',
    ['C_ITEM_ENHANCEMENTS_ID'] = '47',
    ['C_POTIONS_ID'] = '48',
    ['C_SCROLLS_ID'] = '49',
    ['C_OTHER_ID'] = '50',

    ['G_COLOR_ID'] = '51',
    ['G_RELIC_ID'] = '52',
    ['G_SIMPLE_ID'] = '53',

    ['GC_META_ID'] = '54',
    ['GC_RED_ID'] = '55',
    ['GC_BLUE_ID'] = '56',
    ['GC_YELLOW_ID'] = '57',
    ['GC_PURPLE_ID'] = '58',
    ['GC_GREEN_ID'] = '59',
    ['GC_ORANGE_ID'] = '60',
    ['GC_PRISMATIC_ID'] = '61',
    ['GC_SHA_TOUCHED_ID'] = '62',
    ['GC_COGWHEEL_ID'] = '63',

    ['GR_Arcane_ID'] = '64',
    ['GR_Blood_ID'] = '65',
    ['GR_Fel_ID'] = '66',
    ['GR_Fire_ID'] = '67',
    ['GR_Frot_ID'] = '68',
    ['GR_Holy_ID'] = '69',
    ['GR_Iron_ID'] = '70',
    ['GR_Life_ID'] = '71',
    ['GR_Shadow_ID'] = '72',
    ['GR_Water_ID'] = '73',
    ['GR_Storm_ID'] = '74',

    ['M_ARMOR_TOKENS_ID'] = '75',
    ['M_HOLIDAY_ID'] = '76',
    ['M_REAGENTS_ID'] = '77',
    ['M_MOUNTS_ID'] = '78',
    ['M_COMPANIONS_ID'] = '79',
    ['M_OTHER_ID'] = '80',

    -- Recipes
    ['R_BOOKS_ID'] = '81',
    ['R_ALCHEMY_ID'] = '82',
    ['R_BLACKSMITHING_ID'] = '83',
    ['R_COOKING_ID'] = '84',
    ['R_ENCHANTING_ID'] = '85',
    ['R_ENGINEERING_ID'] = '86',
    ['R_FIRST_AID_ID'] = '87',
    ['R_FISHING_ID'] = '88',
    ['R_INSCRIPTIONS_ID'] = '89',
    ['R_JEWELCRAFTING_ID'] = '90',
    ['R_LEATHERWORKING_ID'] = '91',
    ['R_MINING_ID'] = '92',
    ['R_TAILORING_ID'] = '93',

    -- Trade goods
    ['T_ARMOR_ENCHANTMENTS_ID'] = '94',
    ['T_CLOTH_ID'] = '95',
    ['T_DEVICES_ID'] = '96',
    ['T_ELEMENTAL_ID'] = '97',
    ['T_ENCHANTING_ID'] = '98',
    ['T_EXPLOSIVES_ID'] = '99',
    ['T_HERBS_ID'] = '100',
    ['T_JEWELCRAFTING_ID'] = '101',
    ['T_LEATHER_ID'] = '102',
    ['T_MATERIALS_ID'] = '103',
    ['T_MEAT_ID'] = '104',
    ['T_METAL_STONE_ID'] = '105',
    ['T_PARTS_ID'] = '106',
    ['T_WEAPON_ENCHANTMENTS_ID'] = '107',
    ['T_OTHER_ID'] = '108',

    -- Professions
    ['P_ALCHEMY_ID'] = '109',
    ['P_BLACKSMITHING_ID'] = '110',
    ['P_ENCHANTING_ID'] = '111',
    ['P_ENGINEERING_ID'] = '112',
    ['P_HERBALISM_ID'] = '113',
    ['P_INSCRIPTION_ID'] = '114',
    ['P_JEWELCRAFTING_ID'] = '115',
    ['P_LEATHERWORKING_ID'] = '116',
    ['P_MINING_ID'] = '117',
    ['P_SKINNING_ID'] = '118',
    ['P_TAILORING_ID'] = '119',
    ['P_ARCHAEOLOGY_ID'] = '120',
    ['P_COOKING_ID'] = '121',
    ['P_FIRST_AID_ID'] = '122',
    ['P_FISHING_ID'] = '123',
    ['P_RIDING_ID'] = '124',


    -- Custom
    ['M_ARCHAEOLOGY_ID'] = '125',
    ['MA_ARTIFACT_ID'] = '145',
    ['MA_CRATES_ID'] = '144',
    ['MA_KEY_STONES_ID'] = '126',
    ['MA_QUEST_ID'] = '127',
    ['MA_OTHERS_ID'] = '128',
    ['M_TELEPORT_ID'] = '129',
    ['MT_HEARTSTONE_ID'] = '130',
    ['MT_ARMOR_ID'] = '131',
    ['MT_JEWELRY_ID'] = '132',
    ['MT_QUEST_ID'] = '133',
    ['MT_SCROLLS_ID'] = '134',
    ['MT_TOYS_ID'] = '135',
    ['MT_WHISTLE_ID'] = '136',

    ['MH_DARKMOON_ID'] = '146',

    ['T_FISHING_ID'] = '137',
    ['TF_POLES_ID'] = '138',
    ['TF_HATS_ID'] = '139',
    ['TF_OTHERS_ID'] = '140',
    ['TM_ANIMAL_ID'] = '141',
    ['TM_EGG_ID'] = '142',
    ['TM_FISH_ID'] = '143',

    -- All expansions
    ['MC_BATTLE_STONE_ID'] = '148',
    ['MC_CONSUMABLE_ID'] = '149',
    ['MC_SUPPLIES_ID'] = '150',
    ['MC_TRAINING_STONE_ID'] = '151',

    -- Legion
    ['ORDER_HALL_ID'] = '161',
    ['O_CHAMPION_ARMOR_ID'] = '162',
    ['O_CHAMPION_EQUIPMENT_ID'] = '163',
    ['O_CHESTS_ID'] = '164',
    ['O_CONSUMABLES_ID'] = '165',
    ['O_TROOPS_ID'] = '166',

    -- Warlords of Draenor
    ['GARRISON_ID'] = '152',
    ['G_BLUEPRINTS_ID'] = '153',
    ['G_FOLLOWERS_ID'] = '154',
    ['G_IRONHORDE_ID'] = '155',
    ['G_MINING_ID'] = '156',
    ['G_SHIPYARD_ID'] = '157',
    ['G_WORKORDERS_ID'] = '158',

    ['ASHRAN_ID'] = '159',
    ['A_BOOKS_ID'] = '160',
}

addon.RULES = {
    ['EXPANSIONS_LIST'] = {
        addon.CONS.CLASSIC_ID,
        addon.CONS.THE_BURNING_CRUSADE_ID, 
        addon.CONS.WRATH_OF_THE_LICH_KING_ID, 
        addon.CONS.CATACLYSM_ID, 
        addon.CONS.MISTS_OF_PANDARIA_ID, 
        addon.CONS.WARLORDS_OF_DRAENOR_ID, 
        addon.CONS.LEGION_ID
    },
    ['CATEGORY_TREE'] = {
        [addon.CONS.CONTAINERS_ID] = true,
        [addon.CONS.CONSUMABLES_ID] = {
            [addon.CONS.C_BANDAGES_ID] = true,
            [addon.CONS.C_CONSUMABLES_ID] = true,
            [addon.CONS.C_ELIXIRS_ID] = true,
            [addon.CONS.C_FLASKS_ID] = true,
            [addon.CONS.C_FOOD_DRINKS_ID] = true,
            [addon.CONS.C_ITEM_ENHANCEMENTS_ID] = true,
            [addon.CONS.C_POTIONS_ID] = true,
            [addon.CONS.C_SCROLLS_ID] = true,
            [addon.CONS.C_OTHER_ID] = true
        }, 
        [addon.CONS.CURRENCY_ID] = true,
        [addon.CONS.GEMS_ID] = {
            [addon.CONS.G_RELIC_ID] = true
        },
        [addon.CONS.GLYPHS_ID] = true,
        [addon.CONS.KEYS_ID] = true,
        [addon.CONS.MISCELLANEOUS_ID] = {
            [addon.CONS.M_ARMOR_TOKENS_ID] = true,
            [addon.CONS.M_HOLIDAY_ID] = {
                [addon.CONS.MH_DARKMOON_ID] = true
            },
            [addon.CONS.M_REAGENTS_ID] = true,
            [addon.CONS.M_MOUNTS_ID] = true,
            [addon.CONS.M_COMPANIONS_ID] = {
                [addon.CONS.MC_BATTLE_STONE_ID] = true, 
                [addon.CONS.MC_CONSUMABLE_ID] = true, 
                [addon.CONS.MC_SUPPLIES_ID] = true, 
                [addon.CONS.MC_TRAINING_STONE_ID] = true
            },
            [addon.CONS.M_TELEPORT_ID] = {
                [addon.CONS.MT_HEARTSTONE_ID] = true,
                [addon.CONS.MT_ARMOR_ID] = true,
                [addon.CONS.MT_JEWELRY_ID] = true,
                [addon.CONS.MT_QUEST_ID] = true,
                [addon.CONS.MT_SCROLLS_ID] = true,
                [addon.CONS.MT_TOYS_ID] = true,
                [addon.CONS.MT_WHISTLE_ID] = true
            },
            [addon.CONS.M_ARCHAEOLOGY_ID] = {
                [addon.CONS.MA_CRATES_ID] = true,
                [addon.CONS.MA_ARTIFACT_ID] = true,
                [addon.CONS.MA_KEY_STONES_ID] = true,
                [addon.CONS.MA_QUEST_ID] = true,
                [addon.CONS.MA_OTHERS_ID] = true
            },
            [addon.CONS.M_OTHER_ID] = true
        },
        [addon.CONS.QUEST_ID] = true, 
        [addon.CONS.RECIPES_ID] = {
            [addon.CONS.R_BOOKS_ID] = true,
            [addon.CONS.R_ALCHEMY_ID] = true,
            [addon.CONS.R_BLACKSMITHING_ID] = true,
            [addon.CONS.R_COOKING_ID] = true,
            [addon.CONS.R_ENCHANTING_ID] = true,
            [addon.CONS.R_ENGINEERING_ID] = true,
            [addon.CONS.R_FIRST_AID_ID] = true,
            [addon.CONS.R_FISHING_ID] = true,
            [addon.CONS.R_INSCRIPTIONS_ID] = true,
            [addon.CONS.R_JEWELCRAFTING_ID] = true,
            [addon.CONS.R_LEATHERWORKING_ID] = true,
            [addon.CONS.R_MINING_ID] = true,
            [addon.CONS.R_TAILORING_ID] = true
        },
        [addon.CONS.TRADE_GOODS_ID] = {
            [addon.CONS.T_ARMOR_ENCHANTMENTS_ID] = true, 
            [addon.CONS.T_CLOTH_ID] = true, 
            [addon.CONS.T_DEVICES_ID] = true, 
            [addon.CONS.T_ELEMENTAL_ID] = true, 
            [addon.CONS.T_ENCHANTING_ID] = true, 
            [addon.CONS.T_EXPLOSIVES_ID] = true, 
            [addon.CONS.T_HERBS_ID] = true, 
            [addon.CONS.T_JEWELCRAFTING_ID] = true,
            [addon.CONS.T_LEATHER_ID] = true,
            [addon.CONS.T_MATERIALS_ID] = true,
            [addon.CONS.T_MEAT_ID] = {
                [addon.CONS.TM_ANIMAL_ID] = true, 
                [addon.CONS.TM_EGG_ID] = true, 
                [addon.CONS.TM_FISH_ID] = true
            },    
            [addon.CONS.T_METAL_STONE_ID] = true,
            [addon.CONS.T_PARTS_ID] = true,
            [addon.CONS.T_WEAPON_ENCHANTMENTS_ID] = true,
            [addon.CONS.T_OTHER_ID] = true,
            [addon.CONS.T_FISHING_ID] = {
                [addon.CONS.TF_POLES_ID] = true, 
                [addon.CONS.TF_HATS_ID] = true, 
                [addon.CONS.TF_OTHERS_ID] = true
            }
        }
    },
    ['EXPANSION_SPECIFIC_SUBCATEGORY'] = {
        [addon.CONS.LEGION_ID] = {
            [addon.CONS.ORDER_HALL_ID] = {
                [addon.CONS.G_BLUEPRINTS_ID] = true,
                [addon.CONS.G_FOLLOWERS_ID] = true,
                [addon.CONS.G_IRONHORDE_ID] = true,
                [addon.CONS.G_MINING_ID] = true,
                [addon.CONS.G_SHIPYARD_ID] = true,
                [addon.CONS.G_WORKORDERS_ID] = true
            }
        },
        [addon.CONS.WARLORDS_OF_DRAENOR_ID] = {
            [addon.CONS.GARRISON_ID] = {
                [addon.CONS.G_BLUEPRINTS_ID] = true,
                [addon.CONS.G_FOLLOWERS_ID] = true,
                [addon.CONS.G_IRONHORDE_ID] = true,
                [addon.CONS.G_MINING_ID] = true,
                [addon.CONS.G_SHIPYARD_ID] = true,
                [addon.CONS.G_WORKORDERS_ID] = true
            }, 
            [addon.CONS.ASHRAN_ID] = {
                [addon.CONS.A_BOOKS_ID] = true
            }
        }
        
    }
}

addon.LABEL_TEXT_SHORT = {
    [addon.CONS.CLASSIC_ID] = L['Classic'],
    [addon.CONS.THE_BURNING_CRUSADE_ID] = L['TBC'],
    [addon.CONS.WRATH_OF_THE_LICH_KING_ID] = L['WotLK'],
    [addon.CONS.CATACLYSM_ID] = L['CTC'],
    [addon.CONS.MISTS_OF_PANDARIA_ID] = L['MoP'],
    [addon.CONS.WARLORDS_OF_DRAENOR_ID] = L['WoD'],
    [addon.CONS.LEGION_ID] = L['LGN'],
    [addon.CONS.BATTLE_FOR_AZEROTH_ID] = L['BfA']
}

addon.LABEL_TEXT = {
    [addon.CONS.CLASSIC_ID] = L['Classic'],
    [addon.CONS.THE_BURNING_CRUSADE_ID] = L['The Burning Crusade'],
    [addon.CONS.WRATH_OF_THE_LICH_KING_ID] = L['Wrath of the Lich King'],
    [addon.CONS.CATACLYSM_ID] = L['Cataclysm'],
    [addon.CONS.MISTS_OF_PANDARIA_ID] = L['Mists of Pandaria'],
    [addon.CONS.WARLORDS_OF_DRAENOR_ID] = L['Warlords of Draenor'],
    [addon.CONS.LEGION_ID] = L['Legion'],
    [addon.CONS.BATTLE_FOR_AZEROTH_ID] = L['Battle for Azeroth'],
    [addon.CONS.OTHER_ID] = L['Other'],
    [addon.CONS.OLD_EXPANSIONS_ID] = L['Old Expansions'],

    -- Items
    [addon.CONS.WEAPONS_ID] = L['Weapons'],
    [addon.CONS.ARMOR_ID] = L['Armors'],
    [addon.CONS.CONTAINERS_ID] = L['Containers'],
    [addon.CONS.CONSUMABLES_ID] = L['Consumables'],
    [addon.CONS.CURRENCY_ID] = L['Currency'],
    [addon.CONS.GEMS_ID] = L['Gems'],
    [addon.CONS.GLYPHS_ID] = L['Glyphs'],
    [addon.CONS.KEYS_ID] = L['Keys'],
    [addon.CONS.MISCELLANEOUS_ID] = L['Miscellaneous'],
    [addon.CONS.QUEST_ID] = L['Quest'],
    [addon.CONS.RECIPES_ID] = L['Recipes'],
    [addon.CONS.TRADE_GOODS_ID] = L['Trade Goods'],

    -- Consumable category
    [addon.CONS.C_BANDAGES_ID] = L['Bandages'],
    [addon.CONS.C_CONSUMABLES_ID] = L['Consumables'],
    [addon.CONS.C_ELIXIRS_ID] = L['Elixirs'],
    [addon.CONS.C_FLASKS_ID] = L['Flasks'],
    [addon.CONS.C_FOOD_DRINKS_ID] = L['Food & Drinks'],
    [addon.CONS.C_ITEM_ENHANCEMENTS_ID] = L['Item Enchantments'],
    [addon.CONS.C_POTIONS_ID] = L['Potions'],
    [addon.CONS.C_SCROLLS_ID] = L['Scrolls'],
    [addon.CONS.C_OTHER_ID] = L['Other'],

    [addon.CONS.G_COLOR_ID] = L['Colored Gems'],
    [addon.CONS.G_RELIC_ID] = L['Relic'],
    [addon.CONS.G_SIMPLE_ID] = L['Simple Gems'],

    [addon.CONS.GC_META_ID] = L['Meta'],
    [addon.CONS.GC_RED_ID] = L['Red'],
    [addon.CONS.GC_BLUE_ID] = L['Blue'],
    [addon.CONS.GC_YELLOW_ID] = L['Yellow'],
    [addon.CONS.GC_PURPLE_ID] = L['Purple'],
    [addon.CONS.GC_GREEN_ID] = L['Green'],
    [addon.CONS.GC_ORANGE_ID] = L['Orange'],
    [addon.CONS.GC_PRISMATIC_ID] = L['Prismatic'],
    [addon.CONS.GC_SHA_TOUCHED_ID] = L['Sha-Touched'],
    [addon.CONS.GC_COGWHEEL_ID] = L['Cogwheel'],

    [addon.CONS.GR_Arcane_ID] = L['Arcane Relic'],
    [addon.CONS.GR_Blood_ID] = L['Blood Relic'],
    [addon.CONS.GR_Fel_ID] = L['Fel Relic'],
    [addon.CONS.GR_Fire_ID] = L['Fire Relic'],
    [addon.CONS.GR_Frot_ID] = L['Frost Relic'],
    [addon.CONS.GR_Holy_ID] = L['Holy Relic'],
    [addon.CONS.GR_Iron_ID] = L['Iron Relic'],
    [addon.CONS.GR_Life_ID] = L['Life Relic'],
    [addon.CONS.GR_Shadow_ID] = L['Shadow Relic'],
    [addon.CONS.GR_Water_ID] = L['Water Relic'],
    [addon.CONS.GR_Storm_ID] = L['Storm Relic'],

    [addon.CONS.M_ARMOR_TOKENS_ID] = L['Armor Tokens'],
    [addon.CONS.M_HOLIDAY_ID] = L['Holiday'],
    [addon.CONS.M_REAGENTS_ID] = L['Reagents'],
    [addon.CONS.M_MOUNTS_ID] = L['Mounts'],
    [addon.CONS.M_COMPANIONS_ID] = L['Companions'],
    [addon.CONS.M_OTHER_ID] = L['Miscellaneous'],

    -- Recipes category
    [addon.CONS.R_BOOKS_ID] = L['Books'],
    [addon.CONS.R_ALCHEMY_ID] = L['Alchemy'],
    [addon.CONS.R_BLACKSMITHING_ID] = L['Blacksmithing'],
    [addon.CONS.R_COOKING_ID] = L['Cooking'],
    [addon.CONS.R_ENCHANTING_ID] = L['Enchanting'],
    [addon.CONS.R_ENGINEERING_ID] = L['Engineering'],
    [addon.CONS.R_FIRST_AID_ID] = L['First Aid'],
    [addon.CONS.R_FISHING_ID] = L['Fishing'],
    [addon.CONS.R_INSCRIPTIONS_ID] = L['Inscriptions'],
    [addon.CONS.R_JEWELCRAFTING_ID] = L['Jewelcrafting'],
    [addon.CONS.R_LEATHERWORKING_ID] = L['Leatherworking'],
    [addon.CONS.R_MINING_ID] = L['Mining'],
    [addon.CONS.R_TAILORING_ID] = L['Tailoring'],

    -- Trade goods category
    [addon.CONS.T_ARMOR_ENCHANTMENTS_ID] = L['Armor Enchantments'],
    [addon.CONS.T_CLOTH_ID] = L['Cloth'],
    [addon.CONS.T_DEVICES_ID] = L['Devices'],
    [addon.CONS.T_ELEMENTAL_ID] = L['Elemental'],
    [addon.CONS.T_ENCHANTING_ID] = L['Enchanting'],
    [addon.CONS.T_EXPLOSIVES_ID] = L['Explosives'],
    [addon.CONS.T_HERBS_ID] = L['Herbs'],
    [addon.CONS.T_JEWELCRAFTING_ID] = L['Jewelcrafting'],
    [addon.CONS.T_LEATHER_ID] = L['Leather'],
    [addon.CONS.T_MATERIALS_ID] = L['Materials'],
    [addon.CONS.T_MEAT_ID] = L['Cooking'],
    [addon.CONS.T_METAL_STONE_ID] = L['Metal & Stone'],
    [addon.CONS.T_PARTS_ID] = L['Engineering'],
    [addon.CONS.T_WEAPON_ENCHANTMENTS_ID] = L['Weapon Enchantments'],
    [addon.CONS.T_OTHER_ID] = L['Others'],

    -- Professions category
    [addon.CONS.P_ALCHEMY_ID] = L['Alchemy'],
    [addon.CONS.P_BLACKSMITHING_ID] = L['Blacksmithing'],
    [addon.CONS.P_ENCHANTING_ID] = L['Enchanting'],
    [addon.CONS.P_ENGINEERING_ID] = L['Engineering'],
    [addon.CONS.P_HERBALISM_ID] = L['Herbalism'],
    [addon.CONS.P_INSCRIPTION_ID] = L['Inscription'],
    [addon.CONS.P_JEWELCRAFTING_ID] = L['Jewelcrafting'],
    [addon.CONS.P_LEATHERWORKING_ID] = L['Leatherworking'],
    [addon.CONS.P_MINING_ID] = L['Mining'],
    [addon.CONS.P_SKINNING_ID] = L['Skinning'],
    [addon.CONS.P_TAILORING_ID] = L['Tailoring'],
    [addon.CONS.P_ARCHAEOLOGY_ID] = L['Archaeology'],
    [addon.CONS.P_COOKING_ID] = L['Cooking'],
    [addon.CONS.P_FIRST_AID_ID] = L['First Aid'],
    [addon.CONS.P_FISHING_ID] = L['Fishing'],
    [addon.CONS.P_RIDING_ID] = L['Riding'],

    -- Custom
    [addon.CONS.M_ARCHAEOLOGY_ID] = L['Archaeology'],
    [addon.CONS.MA_ARTIFACT_ID] = L['Archaeology Artifact'],
    [addon.CONS.MA_CRATES_ID] = L['Crates'],
    [addon.CONS.MA_KEY_STONES_ID] = L['Key Stones'],
    [addon.CONS.MA_QUEST_ID] = L['Archaeology Quest Items'],
    [addon.CONS.MA_OTHERS_ID] = L['Other Archaeology Items'],
    
    [addon.CONS.MH_DARKMOON_ID] = L['Darkmoon Faire'],

    [addon.CONS.M_TELEPORT_ID] = L['Teleport'],
    [addon.CONS.MT_HEARTSTONE_ID] = L['Heartstones'],
    [addon.CONS.MT_ARMOR_ID] = L['Armor with Teleport'],
    [addon.CONS.MT_JEWELRY_ID] = L['Teleport Jewelry'],
    [addon.CONS.MT_QUEST_ID] = L['Teleport Quests'],
    [addon.CONS.MT_SCROLLS_ID] = L['Scrolls of Teleport'],
    [addon.CONS.MT_TOYS_ID] = L['Toys with Teleport'],
    [addon.CONS.MT_WHISTLE_ID] = L['Whistle with Teleport'],

    [addon.CONS.T_FISHING_ID] = L['Fishing'],
    [addon.CONS.TF_POLES_ID] = L['Poles'],
    [addon.CONS.TF_HATS_ID] = L['Fishing Hats'],
    [addon.CONS.TF_OTHERS_ID] = L['Other Fishing Items'],
    [addon.CONS.TM_ANIMAL_ID] = L['Animal Meat'],
    [addon.CONS.TM_EGG_ID] = L['Eggs'],
    [addon.CONS.TM_FISH_ID] = L['Fish Meat'],

    -- All expansions category

    [addon.CONS.MC_BATTLE_STONE_ID] = L['Battle-Stone'],
    [addon.CONS.MC_CONSUMABLE_ID] = L['Pet Consumable'],
    [addon.CONS.MC_SUPPLIES_ID] = L['Pet Supplies'],
    [addon.CONS.MC_TRAINING_STONE_ID] = L['Training-Stone'],

    -- Legion

    [addon.CONS.ORDER_HALL_ID] = L['Order Hall'],
    [addon.CONS.O_CHAMPION_ARMOR_ID] = L['Champion Armor'],
    [addon.CONS.O_CHAMPION_EQUIPMENT_ID] = L['Champion Equipment'],
    [addon.CONS.O_CHESTS_ID] = L['Champion Chest'],
    [addon.CONS.O_CONSUMABLES_ID] = L['Order Hall Consumables'],
    [addon.CONS.O_TROOPS_ID] = L['Troops Items'],

    -- Warlords of Draenor
    [addon.CONS.GARRISON_ID] = L['Garrison'],
    [addon.CONS.G_BLUEPRINTS_ID] = L['Blueprints'],
    [addon.CONS.G_FOLLOWERS_ID] = L['Followers'],
    [addon.CONS.G_IRONHORDE_ID] = L['Iron Horde'],
    [addon.CONS.G_MINING_ID] = L['Mining'],
    [addon.CONS.G_SHIPYARD_ID] = L['Shipyard'],
    [addon.CONS.G_WORKORDERS_ID] = L['Workorders'],

    [addon.CONS.ASHRAN_ID] = L['Ashran'],
    [addon.CONS.A_BOOKS_ID] = L['Ashran Books'],

    ['Categories'] = L['Categories']
}

addon.FILTER_TEXT = {
    ['SplitText'] = L['Split: '],
    ['SplitDesc'] = L['Divides the following subcategories into specific filters'],
    ['DEBUG_TEXT'] = L['Debug'],
    ['DEBUG_DESC'] = L['Show debug messages'],
    ['ORGANIZE_BY_EXPANSION_TEXT'] = L['Organize by expansion'],
    ['ORGANIZE_BY_EXPANSION_DESC'] = L['Group items according to the expansion that have been released'],
    ['OLD_EXPANSION_TEXT'] = L['Set as: Old expansion'],
    ['OLD_EXPANSION_DESC'] = L['Groups items from the following expansions as: Old expansion']
}